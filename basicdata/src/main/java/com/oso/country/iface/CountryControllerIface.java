/**
 * create by liyi
 * create date 2015-04-17
 * description 国家的维护
 */
package com.oso.country.iface;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;

public interface CountryControllerIface {

	public DBRow countryList(String searchConditions,int pageNo, int pageSize,
			HttpServletResponse response) throws Exception;

	public DBRow insertCountry(Map<String,String> map,HttpServletResponse response) throws Exception;
	
	public DBRow getCountry(long ccid,HttpServletResponse response) throws Exception;
	
	public DBRow deleteCountry(long ccid,HttpServletResponse response) throws Exception;
	
	public DBRow updateCountry(Map<String,String> map,HttpServletResponse response) throws Exception;
	
	public DBRow checkCountry(String c_country,String c_code,String c_code2,long ccid,HttpServletResponse response) throws Exception;
}
