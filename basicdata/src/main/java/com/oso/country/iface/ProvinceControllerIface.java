/**
 * create by liyi
 * create date 2015-04-17
 * description 国家的维护
 */
package com.oso.country.iface;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;

public interface ProvinceControllerIface {
	
	//添加省份
	public DBRow insertProvince(Map<String,String> map,HttpServletResponse response) throws Exception;
	//获得省份
	public DBRow getProvince(long proId,HttpServletResponse response) throws Exception;
	//删除省份
	public DBRow deleteProvince(long proId,HttpServletResponse response) throws Exception;
	//更新省份
	public DBRow updateProvince(Map<String,String> map,HttpServletResponse response) throws Exception;
	//获取某国家下的所有省份
	public DBRow getAllProvinceById(long ccid,int pageNo, int pageSize) throws Exception;
	//检查重复
	public DBRow checkProvince(String pro_name,String p_code,long proId,long ccid,HttpServletResponse response) throws Exception;
}
