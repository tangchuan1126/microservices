package com.oso.country.controller;

import java.util.Map;



import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.fa.country.FloorProvinceMgrLiy;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.country.iface.ProvinceControllerIface;
/**
 * 
 * @author liyi
 *
 */
@RestController
@RequestMapping(value="/provinces")
public class ProvinceController implements ProvinceControllerIface {
	@Autowired
	private FloorProvinceMgrLiy provinceMgr;
	
	/**
	 * 添加身份信息
	 */
	@RequestMapping(value = "/addProvince", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public @ResponseBody DBRow insertProvince(
			@RequestBody Map<String,String> map,
			HttpServletResponse response) throws Exception {
		DBRow province = new DBRow();
		province.add("pro_name",map.get("pro_name"));
		province.add("p_code",map.get("p_code").toUpperCase());
		province.add("nation_id",map.get("nation_id"));
		long id = provinceMgr.insertProvince(province);
		boolean flag = false;
		if (id>0) {
			flag=true;
		} 
		return getResultInfo(flag);
		
	}
	/**
	 * 返回成功信息
	 * @param flag
	 * @return
	 */
	private DBRow getResultInfo(boolean flag){
		DBRow result = new DBRow();
		result.add("success",flag);
		return result;
	}

	@Override
	@RequestMapping(value = "/getProvince", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public DBRow getProvince(@RequestParam(value = "pro_id")long pro_id, HttpServletResponse response)
			throws Exception {
		if(pro_id>0)
			return provinceMgr.getProvinceById(pro_id);
		return null;
	}
	/**
	 * 删除省份
	 */
	@RequestMapping(value = "/deleteProvince", produces = "application/json;charset=UTF-8", method = RequestMethod.DELETE)
	public @ResponseBody DBRow deleteProvince(@RequestParam(value = "pro_id")long pro_id, HttpServletResponse response)
			throws Exception {
		int delId =provinceMgr.deltetProvinceById(pro_id);
		boolean flag = false;
		if (delId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	/**
	 * 跟新省份信息
	 * 
	 */
	@RequestMapping(value = "/updateProvince", produces = "application/json;charset=UTF-8", method = RequestMethod.PUT)
	public @ResponseBody DBRow updateProvince(@RequestBody Map<String,String> map,
			HttpServletResponse response) throws Exception {
		DBRow province = new DBRow();
		province.add("pro_name",map.get("pro_name"));
		province.add("p_code",map.get("p_code").toUpperCase());
		long pro_id = Long.parseLong(map.get("pro_id"));
		int id = provinceMgr.updateProvince(pro_id, province);
		boolean flag = false;
		if (id>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	/**
	 * 根据国家id获取省份信息
	 */
	@RequestMapping(value = "/provinceList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getAllProvinceById(
			@RequestParam(value = "ccid") long ccid,
			@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize)
			throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageSize(pageSize==0 ? 10 : pageSize);
		pc.setPageNo(pageNo==0 ? 1 : pageNo);
		DBRow[] rows = provinceMgr.getAllProvinceByProvinceId(ccid, pc);
		
		DBRow result = new DBRow();
		result.add("provinces",rows);
		result.add("pagectrl", pc);
		return result;
		
	}

	/**
	 * 省份名称，code的重复验证
	 */
	@RequestMapping(value = "/checkProvince", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public DBRow checkProvince(@RequestParam(value = "pro_name",required=false)String pro_name, @RequestParam(value = "p_code",required=false)String p_code, @RequestParam(value = "pro_id",required=false,defaultValue="0")long pro_id,
			@RequestParam(value = "ccid",required=false)long ccid,
			HttpServletResponse response) throws Exception {
		DBRow para = new DBRow();
		para.add("pro_name", pro_name);
		para.add("p_code", p_code);
		DBRow result = new DBRow();
		boolean pro_Name_isExist = false;
		boolean p_code_isExist = false;
		
		if(pro_id>0) {
			boolean[] isExists = provinceMgr.checkProvince(pro_id, para,ccid);
			p_code_isExist = isExists[0];
			p_code_isExist = isExists[1];
		} else {
			pro_Name_isExist = provinceMgr.checkProvinceField(" and pro_name='"+pro_name+"'",ccid);
			p_code_isExist = provinceMgr.checkProvinceField(" and p_code='"+p_code+"'",ccid);
		}
		result.add("pro_name",pro_Name_isExist);
		result.add("p_code",p_code_isExist);
		if(p_code_isExist||pro_Name_isExist) {
			result.add("success",false);
		} else {
			result.add("success",true);
		}
		return result;
	}

}
