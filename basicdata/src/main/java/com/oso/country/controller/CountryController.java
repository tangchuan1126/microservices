package com.oso.country.controller;

import java.util.Map;



import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.fa.country.FloorCountryMgrLiy;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.country.iface.CountryControllerIface;
/**
 * 
 * @author liyi
 *
 */
@RestController
@RequestMapping(value="/countrys")
public class CountryController implements CountryControllerIface {
	@Autowired
	private FloorCountryMgrLiy countryMgr;
	
	@Override
	@RequestMapping(value = "/countryList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow countryList(
			@RequestParam(value = "searchConditions",defaultValue="") String searchConditions,
			@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "10") int pageSize,
			HttpServletResponse response) throws Exception {
		PageCtrl pc = new PageCtrl();
		pc.setPageSize(pageSize==0 ? 10 : pageSize);
		pc.setPageNo(pageNo==0 ? 1 : pageNo);
		DBRow par = new DBRow();
		par.add("searchConditions", searchConditions);
		
		DBRow[] rows = countryMgr.getAllCountry(par, pc);
		DBRow result = new DBRow();
		result.add("countrys",rows);
		result.add("pagectrl", pc);
		return result;
	}

	@Override
	@RequestMapping(value = "/addCountry", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public @ResponseBody DBRow insertCountry(@RequestBody Map<String,String> map,
			HttpServletResponse response) throws Exception {
		DBRow country = new DBRow();
		country.add("c_country",map.get("c_country"));
		country.add("c_code",map.get("c_code").toUpperCase());
		country.add("c_code2",map.get("c_code2").toUpperCase());
		long insertId = countryMgr.insertCountry(country);
		boolean flag = false;
		if (insertId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	@Override
	@RequestMapping(value = "/getCountry", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getCountry(@RequestParam(value="ccid") long ccid, HttpServletResponse response)
			throws Exception {
		if(ccid>0)
			return countryMgr.getCountryById(ccid);
		return null;
	}

	@Override
	@RequestMapping(value = "/deleteCountry", produces = "application/json;charset=UTF-8", method = RequestMethod.DELETE)
	public @ResponseBody DBRow deleteCountry(@RequestParam(value="ccid") long ccid, HttpServletResponse response)
			throws Exception {
		int delId = countryMgr.deleteCountryById(ccid);

		boolean flag = false;
		if (delId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	private DBRow getResultInfo(boolean flag){
		DBRow result = new DBRow();
		result.add("success",flag);
		return result;
	}
	
	@Override
	@RequestMapping(value = "/updateCountry", produces = "application/json;charset=UTF-8", method = RequestMethod.PUT)
	public @ResponseBody DBRow updateCountry(@RequestBody Map<String,String> map,HttpServletResponse response)
			throws Exception {
		DBRow country = new DBRow();
		country.add("c_country",map.get("c_country"));
		country.add("c_code",map.get("c_code").toUpperCase());
		country.add("c_code2",map.get("c_code2").toUpperCase());
		long ccid = Long.parseLong(map.get("ccid"));
		int id = countryMgr.updateCountry(ccid, country);
		DBRow result = new DBRow();
		if(id>0) {
			result.add("success",true);
		} else {
			result.add("success",false);
		}
		return result;
	}

	@Override
	@RequestMapping(value = "/checkCountry", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public DBRow checkCountry(
			@RequestParam(value="c_country",required=false) String c_country,
			@RequestParam(value="c_code",required=false) String c_code,
			@RequestParam(value="c_code2",required=false) String c_code2,
			@RequestParam(value="ccid",required=false,defaultValue="0") long ccid,
			HttpServletResponse response) throws Exception {
		DBRow para = new DBRow();
		para.add("c_country", c_country);
		para.add("c_code", c_code);
		para.add("c_code2", c_code2);
		Map<String,Boolean> isExists = countryMgr.checkCountry(ccid,para);	
		DBRow result = new DBRow();
		result.add("c_code",isExists.get("c_code_isExist"));
		result.add("c_code2",isExists.get("c_code2_isExist"));
		result.add("c_country",isExists.get("c_country_isExist"));
		if((isExists.get("c_code_isExist")||isExists.get("c_code2_isExist"))||isExists.get("c_country_isExist")) {
			result.add("success",false);
		} else {
			result.add("success",true);
		}
		
		
		return result;
	}
}
