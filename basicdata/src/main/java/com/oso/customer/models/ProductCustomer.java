package com.oso.customer.models;

import java.util.ArrayList;
import java.util.List;

import com.oso.basicdata.beans.Customer;

/**
 * 表示产品与品牌商的关系
 * @author lujintao
 *
 */
public class ProductCustomer {
	//产品ID
	private Integer productId;
	//品牌商ID
	private Title title = new Title();
	private List<Customer> customers = new ArrayList<Customer>();
	
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Title getTitle() {
		return title;
	}
	public void setTitle(Title title) {
		this.title = title;
	}
	public List<Customer> getCustomers() {
		if(customers == null){
			customers = new ArrayList<Customer>();
		}
		return customers;
	}
	public void setCustomers(List<Customer> customers) {
		this.customers = customers;
	}
}
