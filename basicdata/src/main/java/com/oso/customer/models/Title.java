package com.oso.customer.models;

/**
 * 生产商
 * @author lujintao
 *
 */
public class Title {
	private int id;
	//名称
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
}
