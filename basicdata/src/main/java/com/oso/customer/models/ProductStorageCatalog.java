package com.oso.customer.models;

public class ProductStorageCatalog {
	private String address;
	private String send_address1;
	private String send_address2;
	private String city;
	private String send_zip_code;
	private String province;
	private String nation;
	private String contact;
	private String phone;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSend_address1() {
		return send_address1;
	}

	public void setSend_address1(String send_address1) {
		this.send_address1 = send_address1;
	}

	public String getSend_address2() {
		return send_address2;
	}

	public void setSend_address2(String send_address2) {
		this.send_address2 = send_address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSend_zip_code() {
		return send_zip_code;
	}

	public void setSend_zip_code(String send_zip_code) {
		this.send_zip_code = send_zip_code;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getNation() {
		return nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

}
