package com.oso.customer.iface;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author Zhangchangjiang
 *
 */
public interface BrandControllerIFace {
	public void brandList(int pageNo, int pageSize, long customerKey,
			HttpServletResponse response) throws Exception;

	public void insertBrand(HttpServletResponse response,HttpSession session,
			@RequestBody Map<String, String> map) throws Exception;

	public void modifyActive(HttpServletResponse response ,HttpSession session, long bId,
			String style) throws Exception;
}
