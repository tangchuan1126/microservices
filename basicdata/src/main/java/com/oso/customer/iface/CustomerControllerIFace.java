package com.oso.customer.iface;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.oso.basicdata.beans.Customer;
import com.oso.customer.models.Title;

/**
 * @author Zhangchangjiang
 *
 */
public interface CustomerControllerIFace {
	public void customerList(int pageNo, int pageSize,String searchCustomerId,String cmd,String searchCustomerName,String searchCountryValue,String searchStateValue,String searchConditions, long active,
			HttpServletResponse response)
			throws Exception;

	public void listAllCountries(HttpServletResponse response) throws Exception;

	public void getProvinceByCcid(int ccid, HttpServletResponse response)
			throws Exception;

	public void checkCustomerId(String customer_id, HttpServletResponse response)
			throws Exception;

	public void checkCustomerName(String customer_name,
			HttpServletResponse response) throws Exception;

	public void uncheckCustomerId(String customer_id, String input_customer_id,
			HttpServletResponse response) throws Exception;

	public void uncheckCustomerName(String customer_name,
			String inputCustomerName, HttpServletResponse response)
			throws Exception;

	public void insertCustomer(HttpServletResponse response,
			Map<String, String> map) throws Exception;

	public void modifyCustomer(HttpServletResponse response,

	Map<String, String> map) throws Exception;

	public void modifyActive(HttpServletResponse response, long customer_key,
			String style) throws Exception;
	
	public List<Customer> getByTitle(@PathVariable("titleId") int titleId) throws Exception;
}
