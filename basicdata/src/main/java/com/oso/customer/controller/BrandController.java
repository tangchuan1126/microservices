package com.oso.customer.controller;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.fa.FloorBrandMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.customer.iface.BrandControllerIFace;

/**
 * @author Zhangchangjiang
 *
 */
@RestController
public class BrandController implements BrandControllerIFace {
	@Autowired
	private FloorBrandMgr brandMgr;
	
	/**
	 * 品牌列表显示
	 * 
	 * @param 分页参数 ,customer_key
	 * @throws Exception
	 */
	@RequestMapping(value = "/brandList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public void brandList(
			@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "5") int pageSize,
			@RequestParam(value = "customer_key") long customerKey,
			HttpServletResponse response) throws Exception {
		DBRow dbRow = new DBRow();
		dbRow.add("customer_key", customerKey);
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		DBRow rows[] = brandMgr.getAllBrandList(dbRow, pc);
		JSONObject output = new JSONObject();
		output.put("pageCtrl", new JSONObject(pc));
		output.put("brands", DBRowUtils.dbRowArrayAsJSON(rows));
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().print(output);
	}
	
	/**
	 * 插入品牌
	 * 
	 * @param 品牌信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/brandInsert", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public void insertBrand(HttpServletResponse response,HttpSession session,
			@RequestBody Map<String, String> map) throws Exception {
		String bName = map.get("brand_name");
		String bNumber = map.get("brand_number");
		String customerKey = map.get("customer_key");
		// 获取adminLoginBean
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);

		DBRow dbRow = new DBRow();
		dbRow.add("brand_name", bName);
		dbRow.add("brand_number", bNumber);
		dbRow.add("customer_key", customerKey);
		dbRow.add("creator", adminBean.get("adid"));
		dbRow.add("create_time", DateUtil.NowStr());
		long brand = brandMgr.insertBrand(dbRow);
		JSONObject output = new JSONObject();
		if (brand > 0) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}
	
	/**
	 * 启用/禁用品牌
	 * 
	 * @param style->启用禁用参数,bId->品牌id
	 * @throws Exception
	 */
	@RequestMapping(value = "/brandActive", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	@ResponseBody
	public void modifyActive(HttpServletResponse response,HttpSession session,
			@RequestParam(value = "cb_id") long bId,
			@RequestParam(value = "style") String style) throws Exception {
		
		// 获取adminLoginBean
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);
		DBRow dbRow = new DBRow();
		dbRow.add("cb_id", bId);
		dbRow.add("creator", adminBean.get("adid"));
		dbRow.add("create_time", DateUtil.NowStr());
		if (style.equals("active")) {
			dbRow.add("active", 1);
		} else {
			dbRow.add("active", 0);
		}
		int brand = brandMgr.updateActive(dbRow);
		JSONObject output = new JSONObject();
		if (brand > 0) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}
}
