package com.oso.customer.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.fa.FloorCustomerMgr;
import com.cwc.app.key.PaymentTermKey;
import com.cwc.app.key.StorageTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.beans.Customer;
import com.oso.basicdata.service.CustomerService;
import com.oso.customer.iface.CustomerControllerIFace;
import com.oso.customer.models.CustomerId;
import com.oso.customer.models.Title;

/**
 * @author Zhangchangjiang
 *
 */
@RestController
public class CustomerController implements CustomerControllerIFace {

	@Autowired
	private FloorCustomerMgr customerMgr;
	@Autowired
	private CustomerService customerService;
	/**
	 * 品牌商列表显示
	 * 
	 * @param 分页参数
	 * @throws Exception
	 */
	@RequestMapping(value = "/customer", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public void customerList(
			@RequestParam(value = "pageNo", defaultValue = "1") int pageNo,
			@RequestParam(value = "pageSize", defaultValue = "15") int pageSize,
			@RequestParam(value = "cmd") String cmd,
			@RequestParam(value = "searchCustomerId") String searchCustomerId,
			@RequestParam(value = "searchCustomerName") String searchCustomerName,
			@RequestParam(value = "searchCountryValue") String searchCountryValue,
			@RequestParam(value = "searchStateValue") String searchStateValue,
			@RequestParam(value = "searchConditions") String searchConditions,
			@RequestParam(value = "active", defaultValue="-1") long active,
			HttpServletResponse response)
			throws Exception {

		DBRow rows[] = new DBRow[0];
		DBRow dbRow = new DBRow();
		dbRow.add("storage_type", StorageTypeKey.CUSTOMER);
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		if ("search".equals(cmd)) {
			if (!"".equals(searchConditions)) {
				dbRow.add("custom.customer_id", searchConditions);
				dbRow.add("custom.customer_name", searchConditions);
//				dbRow.add("searchConditions", searchConditions);
			}
			
			rows = customerMgr.getAllCustomerList(dbRow, pc);
		} else{
			// 高级搜索
			if(active > -1){
				dbRow.add("active", active);
			}
			if (!"".equals(searchCustomerId.trim())) {
				dbRow.add("customer_id", searchCustomerId);
			}
			if (!"".equals(searchCustomerName.trim())) {
				dbRow.add("customer_name", searchCustomerName);
			}
			if (!"".equals(searchCountryValue.trim())) {
				dbRow.add("c_country", searchCountryValue);
			}
			if (!"".equals(searchStateValue.trim())) {
				dbRow.add("pro_name", searchStateValue);
			}
			
			
			rows = customerMgr.getAllCustomerList(dbRow, pc);
		}
		
		for(DBRow one : rows){
			
			DBRow[] titles = customerMgr.getTitleProductByCustomer(one.get("customer_key",0));
			
			one.put("titles", titles);
			one.put("titleCnt", titles.length);
		}
		
//		else {
//			// 列表显示
//			rows = customerMgr.getAllCustomerList(dbRow, pc);
//		}
		JSONObject output = new JSONObject();
		output.put("pageCtrl", new JSONObject(pc));
		output.put("customers", DBRowUtils.dbRowArrayAsJSON(rows));
		response.setCharacterEncoding("UTF-8");
		response.setContentType("application/json");
		response.getWriter().print(output.toString());
	}

	/**
	 * 获取所有国家
	 */
	@RequestMapping(value = "/country", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void listAllCountries(HttpServletResponse response) throws Exception {
		DBRow countrys[] = customerMgr.getAllCountryCode();
		PaymentTermKey payment = new PaymentTermKey();
		DBRow[] enPaymentTerm = payment.getEnPaymentTerm();
		JSONObject output = new JSONObject();
		output.put("countrys", DBRowUtils.dbRowArrayAsJSON(countrys));
		output.put("payments", DBRowUtils.dbRowArrayAsJSON(enPaymentTerm));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output.toString());
	}

	/**
	 * 根据国家id，获取所有省份
	 * 
	 * @param ccid
	 *            ->国家id
	 * @throws Exception
	 */
	@RequestMapping(value = "/province", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void getProvinceByCcid(@RequestParam(value = "ccid") int ccid,
			HttpServletResponse response) throws Exception {
		DBRow provinces[] = customerMgr.getStorageProvinceByCcid(ccid);
		JSONObject output = new JSONObject();
		output.put("provinces", DBRowUtils.dbRowArrayAsJSON(provinces));
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * 增加customer_id时的唯一效验
	 * 
	 * @param customer_id
	 * @throws Exception
	 */
	@RequestMapping(value = "/customerId", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void checkCustomerId(
			@RequestParam(value = "customer_id") String customer_id,
			HttpServletResponse response) throws Exception {
		JSONObject output = new JSONObject();
		if (customerMgr.CheckCustomerIdUnique(customer_id) == true) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * 增加customer_name时的唯一效验
	 * 
	 * @param customer_name
	 * @throws Exception
	 */
	@RequestMapping(value = "/customerName", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void checkCustomerName(
			@RequestParam(value = "customer_name") String customer_name,
			HttpServletResponse response) throws Exception {
		JSONObject output = new JSONObject();
		if (customerMgr.CheckCustomerNameUnique(customer_name) == true) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * 修改customer_id时唯一效验(排除自身)
	 * 
	 * @param customer_id
	 *            ->原有值,input_customer_id->录入值
	 * @throws Exception
	 */
	@RequestMapping(value = "/customerIdByMod", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void uncheckCustomerId(
			@RequestParam(value = "customer_id") String customer_id,
			@RequestParam(value = "input_customer_id") String input_customer_id,
			HttpServletResponse response) throws Exception {
		JSONObject output = new JSONObject();
		if (customer_id.equals(input_customer_id)) {
			output.put("success", true);
		} else {
			if (customerMgr.CheckCustomerIdUnique(input_customer_id) == true) {
				output.put("success", true);
			} else {
				output.put("success", false);
			}
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * 修改customer_name时唯一效验(排除自身)
	 * 
	 * @param customer_name
	 *            ->原有值,input_customer_name->录入值
	 * @throws Exception
	 */
	@RequestMapping(value = "/customerNameByMod", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	@ResponseBody
	public void uncheckCustomerName(
			@RequestParam(value = "customer_name") String customer_name,
			@RequestParam(value = "input_customer_name") String inputCustomerName,
			HttpServletResponse response) throws Exception {
		JSONObject output = new JSONObject();
		if (customer_name.equals(inputCustomerName)) {
			output.put("success", true);
		} else {
			if (customerMgr.CheckCustomerNameUnique(inputCustomerName) == true) {
				output.put("success", true);
			} else {
				output.put("success", false);
			}
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * add-->Customer
	 * 
	 * @param customer基本信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/customer", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public void insertCustomer(HttpServletResponse response,
			@RequestBody Map<String, String> map) throws Exception {
		String customer_id = map.get("customer_id");
		String customer_name = map.get("customer_name");
		String name = map.get("warehouse_name");
		String house_address = map.get("house_address");
		String street_address = map.get("street_address");
		String city = map.get("city");
		String country1 = map.get("country");
		String zipcode = map.get("zipcode");
		String state_dd1 = map.get("state_dd");
		long state_dd = Long.parseLong(state_dd1);
		String text_state = map.get("is_text_state");
		String state = map.get("state");
		long country = Long.parseLong(country1);
		String contact = map.get("contact");
		String phone = map.get("phone");
		String email = map.get("email");
		String fax = map.get("fax");
		String payment_term = map.get("payment_term");
		payment_term = (payment_term==null || payment_term.trim().length()==0) ? "0" : payment_term;
		
		DBRow dbRow = new DBRow();
		dbRow.add("customer_id", customer_id);
		dbRow.add("customer_name", customer_name);
		dbRow.add("payment_term", payment_term);
		long customer = customerMgr.insertCustomer(dbRow);

		DBRow dbRow2 = new DBRow();
		dbRow2.add("title", name);
		dbRow2.add("send_house_number", house_address);
		dbRow2.add("deliver_house_number", house_address);
		dbRow2.add("send_street", street_address);
		dbRow2.add("deliver_street", street_address);
		dbRow2.add("send_zip_code", zipcode);
		dbRow2.add("deliver_zip_code", zipcode);
		dbRow2.put("contact", contact);
		dbRow2.put("send_contact", contact);
		dbRow2.put("deliver_contact", contact);
		dbRow2.put("phone", phone);
		dbRow2.put("send_phone", phone);
		dbRow2.put("deliver_phone", phone);
		dbRow2.put("city", city);
		dbRow2.put("send_city", city);
		dbRow2.put("deliver_city", city);
		dbRow2.put("native", country);
		dbRow2.put("send_nation", country);
		dbRow2.put("deliver_nation", country);
		dbRow2.put("storage_type_id", customer);
		dbRow2.add("storage_type", StorageTypeKey.CUSTOMER);
		dbRow2.add("active", 1);
		dbRow2.add("email", email);
		dbRow2.add("fax", fax);
		if (text_state.equalsIgnoreCase("true")) {
			dbRow2.put("send_pro_input", state);
			dbRow2.put("deliver_pro_input", state);
		} else {
			dbRow2.put("pro_id", state_dd);
			dbRow2.put("send_pro_id", state_dd);
			dbRow2.put("deliver_pro_id", state_dd);
			DBRow dbRow3 = customerMgr.getProvinceByProId(state_dd);
			String provName = dbRow3.getString("pro_name");
			dbRow2.put("send_pro_input", provName);
			dbRow2.put("deliver_pro_input", provName);
		}
		long psc = customerMgr.insertProductStorageCatalog(dbRow2);
		JSONObject output = new JSONObject();
		if (customer > 0 && psc > 0) {
			output.put("success", true);
			output.put("customer_key", customer);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * modify-->Customer
	 * 
	 * @param customer基本信息
	 * @throws Exception
	 */
	@RequestMapping(value = "/modify", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	@ResponseBody
	public void modifyCustomer(HttpServletResponse response,

	@RequestBody Map<String, String> map) throws Exception {
		String customer_id = map.get("customer_id");
		String customer_name = map.get("customer_name");
		String name = map.get("warehouse_name");
		String house_address = map.get("house_address");
		String street_address = map.get("street_address");
		String city = map.get("city");
		String country1 = map.get("country");
		String zipcode = map.get("zipcode");
		String state_dd1 = map.get("state_dd");
		long state_dd = Long.parseLong(state_dd1);
		String text_state = map.get("is_text_state");
		String state = map.get("state");
		long country = Long.parseLong(country1);
		String customer_key1 = map.get("customer_key");
		long customer_key = Long.parseLong(customer_key1);
		String contact = map.get("contact");
		String phone = map.get("phone");
		String email = map.get("email");
		String fax = map.get("fax");
		String payment_term = map.get("payment_term");
		payment_term = (payment_term==null || payment_term.trim().length()==0) ? "0" : payment_term;
		
		DBRow dbRow = new DBRow();
		dbRow.add("customer_key", customer_key);
		dbRow.add("customer_id", customer_id);
		dbRow.add("customer_name", customer_name);
		dbRow.add("payment_term", payment_term);
		int customer = customerMgr.updateCustomerID(dbRow);

		DBRow dbRow2 = new DBRow();
		dbRow2.add("title", name);
		dbRow2.add("send_house_number", house_address);
		dbRow2.add("deliver_house_number", house_address);
		dbRow2.add("send_street", street_address);
		dbRow2.add("deliver_street", street_address);
		dbRow2.add("send_zip_code", zipcode);
		dbRow2.add("deliver_zip_code", zipcode);
		dbRow2.put("contact", contact);
		dbRow2.put("send_contact", contact);
		dbRow2.put("deliver_contact", contact);
		dbRow2.put("phone", phone);
		dbRow2.put("send_phone", phone);
		dbRow2.put("deliver_phone", phone);
		dbRow2.put("city", city);
		dbRow2.put("send_city", city);
		dbRow2.put("deliver_city", city);
		dbRow2.put("native", country);
		dbRow2.put("send_nation", country);
		dbRow2.put("deliver_nation", country);
		dbRow2.put("storage_type_id", customer_key);
		dbRow2.put("active", 1);
		dbRow2.add("email", email);
		dbRow2.add("fax", fax);
		dbRow2.add("storage_type", StorageTypeKey.CUSTOMER);
		if (text_state.equalsIgnoreCase("true")) {
			dbRow2.put("send_pro_input", state);
			dbRow2.put("deliver_pro_input", state);
			dbRow2.put("pro_id", -1);
			dbRow2.put("send_pro_id", -1);
			dbRow2.put("deliver_pro_id", -1);

		} else {
			dbRow2.put("pro_id", state_dd);
			dbRow2.put("send_pro_id", state_dd);
			dbRow2.put("deliver_pro_id", state_dd);
			DBRow dbRow3 = customerMgr.getProvinceByProId(state_dd);
			String provName = dbRow3.getString("pro_name");
			dbRow2.put("send_pro_input", provName);
			dbRow2.put("deliver_pro_input", provName);
		}
		DBRow getpsc = new DBRow();
		getpsc.add("storage_type_id", customer_key);
		getpsc.add("storage_type", StorageTypeKey.CUSTOMER);
		DBRow pscInfo = customerMgr.getCustomerByCustomerKey(getpsc);
		int psc = 0;
		if(pscInfo != null && pscInfo.size() > 0){
			psc = customerMgr.updateProductStorageCatalog(dbRow2);
		}else{
			long insId = customerMgr.insertProductStorageCatalog(dbRow2);
			psc = (int)insId;
		}
		
		JSONObject output = new JSONObject();
		if (customer > 0 || psc > 0) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}

	/**
	 * 启用/禁用-->Customer
	 * 
	 * @param customer_key
	 *            ,style(启用/禁用类型)
	 * @throws Exception
	 */
	@RequestMapping(value = "/active", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	@ResponseBody
	public void modifyActive(HttpServletResponse response,
			@RequestParam(value = "customer_key") long customer_key,
			@RequestParam(value = "style") String style) throws Exception {
		DBRow dbRow = new DBRow();
		dbRow.add("storage_type_id", customer_key);
		dbRow.add("storage_type", StorageTypeKey.CUSTOMER);
		if (style.equals("active")) {
			dbRow.add("active", 1);
		} else {
			dbRow.add("active", 0);
		}
		DBRow pscDb = new DBRow();
		pscDb.add("storage_type_id", customer_key);
		pscDb.add("storage_type", StorageTypeKey.CUSTOMER);
		DBRow pscInfo = customerMgr.getCustomerByCustomerKey(pscDb);
		
		if(pscInfo == null){
			DBRow insDb = new DBRow();
			insDb.add("storage_type_id", customer_key);
			insDb.add("storage_type", StorageTypeKey.CUSTOMER);
			insDb.add("active", 0);
			
			customerMgr.insertProductStorageCatalog(insDb);
		}
		int psc = customerMgr.updateProductStorageCatalog(dbRow);
		
		JSONObject output = new JSONObject();
		if (psc > 0) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}
	
	/**
	 * 获得所有品牌商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/customer/all", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Customer> get() throws Exception{
		return this.customerService.getAll();
	}

	@Override
	@RequestMapping(value = "/customer/title/{titleId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Customer> getByTitle(@PathVariable("titleId") int titleId) throws Exception{
		return this.customerService.getCustomersByTitle(titleId);
	}
}
