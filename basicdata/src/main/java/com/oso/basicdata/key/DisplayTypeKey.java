package com.oso.basicdata.key;

import java.util.ArrayList;

import com.cwc.app.key.ProcessKey;
import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.key.DispalyType.java] 
 * @ClassName:    [DispalyType]  
 * @Description:  [配置显示类型]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月28日 下午5:51:34]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月28日 下午5:51:34]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class DisplayTypeKey {
    DBRow row;
    
    public DBRow getRow() {
        return row;
    }

    public final static  int TEXTFIELD = 1 ;                    
     public final static  int TEXTAREA = 2 ;                     
     public final static  int RADIO = 3;         
    public final static  int CHECKBOX = 4;                        
    public final static  int DROPLIST = 5 ;                     
    public final static  int DATE = 6; 
    
    public DisplayTypeKey(){
        row = new DBRow();
        row.add(String.valueOf(DisplayTypeKey.TEXTFIELD), "TextField");
        row.add(String.valueOf(DisplayTypeKey.TEXTAREA), "TEXTAREA");
        row.add(String.valueOf(DisplayTypeKey.RADIO), "Radio");
        row.add(String.valueOf(DisplayTypeKey.CHECKBOX), "Checkbox");
        row.add(String.valueOf(DisplayTypeKey.DROPLIST), "DropList");
        row.add(String.valueOf(DisplayTypeKey.DATE), "Date");
    }
    
    public ArrayList<String> getStatus()
    {
        return((ArrayList<String>)row.getFieldNames());
    }

	public String getStatusById(int id)
    {
        return(row.getString(String.valueOf(id)));
    }
    public String getStatusById(String id)
    {
        return(row.getString(String.valueOf(id)));
    }
}
