package com.oso.basicdata.key;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import com.cwc.db.DBRow;
import com.oso.basicdata.beans.TitleShipTo;

/**
 * 功能：处理流程定义
 * 
 * @author lixf      2015年5月12日
 *
 * vvme.com All rights reserved.
 */
public class ProcessStepKey {
	private DBRow row;
	public static int STEP_RECEVING = 1;
	public static int STEP_PUTAWAY = 2;
	public static int STEP_SCAN = 3;
	public static int STEP_CC = 4;
	public static int STEP_ROUTING = 5;
	public static int STEP_INVENTARYCONTROL = 6;
	public static int STEP_PICKING = 7;
	public static int STEP_CONSOLIDATE = 8;
	public static int STEP_LOADING = 9;

	public ProcessStepKey() {
		this.row = new DBRow();
		this.row.add(String.valueOf(STEP_RECEVING), "Receving");
		this.row.add(String.valueOf(STEP_PUTAWAY), "Put Away");
		this.row.add(String.valueOf(STEP_SCAN), "Scan");
		this.row.add(String.valueOf(STEP_CC), "CC");
		this.row.add(String.valueOf(STEP_ROUTING), "Routing");
		this.row.add(String.valueOf(STEP_INVENTARYCONTROL), "Inventary Control");
		this.row.add(String.valueOf(STEP_PICKING), "Picking");
		this.row.add(String.valueOf(STEP_CONSOLIDATE), "Consolidate");
		this.row.add(String.valueOf(STEP_LOADING), "Loading");
	}

	public ArrayList getProcessStepKeys() {
		return this.row.getFieldNames();
	}

	public String getProcessStepKeyValue(int id) {
		return this.row.getString(String.valueOf(id));
	}
	
	public String getProcessStepKeyValue(String id) {
		return this.row.getString(id);
	}
	
	public DBRow getAll(){
		return row;
	}
	
	public DBRow[] toDBRows(){
		DBRow[] kvs = new DBRow[0];
		
		if(row!=null && row.size()>0){
			kvs = new DBRow[row.size()];
			int i = 0;
			for(String key : row.keySet()){
				DBRow kv = new DBRow();
				
				kv.add(TitleShipTo.PROCESS_STEP, key);
				kv.add(TitleShipTo.PROCESS_STEP+"_name", row.getString(key));
				
				kvs[i] = kv;
				
				i++;
			}
			
			Arrays.sort(kvs, new Comparator<DBRow>(){

				@Override
				public int compare(DBRow o1, DBRow o2) {
					int step1 = o1.get(TitleShipTo.PROCESS_STEP, 0);
					int step2 = o2.get(TitleShipTo.PROCESS_STEP, 0);
					return  (step1>step2) ? 1 : (step1==step2) ? 0 : -1;
				}
				
			});
		}
		
		return kvs;
	}
}
