package com.oso.basicdata.utils;

import java.util.ArrayList;
import java.util.List;

import com.cwc.app.util.Md5;
import com.cwc.app.util.StrUtil;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.utils.ParseStrToArray.java] 
 * @ClassName:    [ParseStrToArray]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月23日 下午3:35:41]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月23日 下午3:35:41]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */ 
public class ParseStrToArray {
	private final static String DEFAULT_SEPARARER =",";
	
	public static Long[] toLongArray(String str) throws NumberFormatException, Exception{
		List<Long> longList = new ArrayList<Long>();
		if(!StrUtil.isBlank(str))
		{
			String[] arr = str.split(DEFAULT_SEPARARER);
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StrUtil.isBlankAndCanParseLong(arr[i]))
				{
					if(!"-1".equals(arr[i]))
					{
						longList.add(Long.valueOf(arr[i]));
					}
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	public static Long[] toLongArray(String str,String  separarer) throws NumberFormatException, Exception{
		List<Long> longList = new ArrayList<Long>();
		if(!StrUtil.isBlank(str))
		{
			String[] arr = str.split(separarer);
			for (int i = 0; i < arr.length; i++) 
			{
				if(!StrUtil.isBlankAndCanParseLong(arr[i]))
				{
					if(!"-1".equals(arr[i]))
					{
						longList.add(Long.valueOf(arr[i]));
					}
				}
			}
		}
		return longList.toArray(new Long[0]);
	}
	
	public static final String getMD5(String s) {

		if (s == null) {
			return null;
		}
		
		if (s.trim().length() == 0) {
			return s;
		}

		Md5 md5 = new Md5();

		return md5.getMD5ofStr(s);
	}
}
