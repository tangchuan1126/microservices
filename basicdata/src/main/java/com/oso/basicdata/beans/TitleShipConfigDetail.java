package com.oso.basicdata.beans;

import com.cwc.app.floor.api.fa.basicdata.BasicBean;
import com.cwc.db.DBRow;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.beans.TitleShipConfigDetail.java]
 * @ClassName: [TitleShipConfigDetail]
 * @Description: [一句话描述该类的功能]
 * @Author: [赵永亚]
 * @CreateDate: [2015年4月2日 上午10:32:55]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年4月2日 上午10:32:55]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class TitleShipConfigDetail implements BasicBean {

	public static String TSC_ID = "tsc_id";
	public static String REQUIREMENT_ITEM_ID = "ri_id";
	public static String REQUIREMENT_VALUE_INT = "requirement_value_int";
	public static String REQUIREMENT_VALUE_STR = "requirement_value_str";

	private int tsc_id;
	private int ri_id;
	private int requirement_value_int;
	private String requirement_value_str;

	@Override
	public DBRow toDBRow(boolean flag) {
		DBRow row = new DBRow();
		row.add(TSC_ID, getTsc_id());
		row.add(REQUIREMENT_ITEM_ID, ri_id);
		row.add(REQUIREMENT_VALUE_INT, requirement_value_int);
		row.add(REQUIREMENT_VALUE_STR, requirement_value_str);
		return row;
	}

	public int getTsc_id() {
		return tsc_id;
	}

	public void setTsc_id(int tsc_id) {
		this.tsc_id = tsc_id;
	}

	public int getRi_id() {
		return ri_id;
	}

	public void setRi_id(int ri_id) {
		this.ri_id = ri_id;
	}

	public int getRequirement_value_int() {
		return requirement_value_int;
	}

	public void setRequirement_value_int(int requirement_value_int) {
		this.requirement_value_int = requirement_value_int;
	}

	public String getRequirement_value_str() {
		return requirement_value_str;
	}

	public void setRequirement_value_str(String requirement_value_str) {
		this.requirement_value_str = requirement_value_str;
	}

}
