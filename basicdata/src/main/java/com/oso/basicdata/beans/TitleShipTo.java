package com.oso.basicdata.beans;


import com.cwc.app.floor.api.fa.basicdata.BasicBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.beans.TitleShipTo.java] 
 * @ClassName:    [TitleShipTo]  
 * @Description:  [实体类]  
 * @Author:       [赵永亚]
 * @CreateDate:   [2015年3月24日 上午10:26:25]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年3月24日 上午10:26:25]  
 * @UpdateRemark: [创建TitleShipTo类] 
 * @Version:      [v1.0]
 *   
 */

public class TitleShipTo implements BasicBean{
	
	public final static String TST_ID="tsc_id";
	public final static String TITLE_ID="title_id";
	public final static String SHIP_TO_ID="ship_to_id";
	public final static String CONFIG_TYPE="config_type";
	public final static String CONFIG_ID="config_id";
	public final static String CUSTOMER_KEY = "customer_key";
	public final static String PROCESS_STEP = "process_step";
	
	
	private int tsc_id;
	private int title_id;
	private int ship_to_id;
	private int config_type;
	private int config_id;
	private int customer_key;
	
	public int getTitle_id() {
		return title_id;
	}
	public void setTitle_id(int title_id) {
		this.title_id = title_id;
	}
	public int getShip_to_id() {
		return ship_to_id;
	}
	public void setShip_to_id(int ship_to_id) {
		this.ship_to_id = ship_to_id;
	}
	public int getConfig_type() {
		return config_type;
	}
	public void setConfig_type(int config_type) {
		this.config_type = config_type;
	}
	public int getConfig_id() {
		return config_id;
	}
	public void setConfig_id(int config_id) {
		this.config_id = config_id;
	}
	
	public int getTsc_id() {
		return tsc_id;
	}
	public void setTsc_id(int tsc_id) {
		this.tsc_id = tsc_id;
	}
	
	public int getCustomer_key() {
		return customer_key;
	}
	public void setCustomer_key(int customer_key) {
		this.customer_key = customer_key;
	}
	@Override
	public DBRow toDBRow(boolean flag) {
		DBRow row = new DBRow();
		if(getTsc_id()!=0||flag){
			row.add(TITLE_ID, getTsc_id());
		}
		row.add(TITLE_ID, getTitle_id());
		row.add(CUSTOMER_KEY, getCustomer_key());
		row.add(SHIP_TO_ID, getShip_to_id());
		row.add(CONFIG_TYPE, getConfig_type());
		row.add(CONFIG_ID, getConfig_id());
		return row;
	}
	
}
