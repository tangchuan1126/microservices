package com.oso.basicdata.beans;

import java.util.List;

import com.cwc.app.floor.api.fa.basicdata.BasicBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.beans.ProductCatagorys.java] 
 * @ClassName:    [ProductCatagorys]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年3月27日 上午9:30:04]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年3月27日 上午9:30:04]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class ProductCatagorys  implements BasicBean,Comparable<ProductCatagorys>{
	
	public static String ID="id";
	public static String TITLE="title";
	public static String PRODUCT_LINE_ID="product_line_id";
	public static String SORT="sort";
	public static String TAX="tax";
	public static String PARENTID="parentid";
	public static String CHILDREN="children";
	
	private int id;
	private int parentid;
	private String title;
	private int product_line_id;
	private int sort;
	private double tax;
	
	public  ProductCatagorys() {}
	public  ProductCatagorys(DBRow rows) {
		this.id = rows.get(ID, 0);
		this.title = rows.get(TITLE, "");
		this.product_line_id = rows.get(PRODUCT_LINE_ID, 0);
		this.sort = rows.get(SORT, 0);
		this.tax = rows.get(TAX, 0.0);
		this.parentid = rows.get(PARENTID, 0);
	}
	@Override
	public DBRow toDBRow(boolean flag) {
		DBRow row = new DBRow();
		if(getId()!=0||flag){
			row.add(ID, getId());
		}
		row.add(PRODUCT_LINE_ID, getProduct_line_id());
		row.add(SORT, getSort());
		row.add(TAX, getTax());
		row.add(PARENTID, getParentid());
		if(!StrUtil.isBlank(getTitle())||flag){
			row.add(TITLE,StrUtil.dealNull(getTitle()));
		}
		if(null!=this.children && this.children.size()>0){
			DBRow[] rows = new DBRow[this.children.size()];
			int i=1;
			for(ProductCatagorys pc : this.children){
				rows[i]=pc.toDBRow(flag);
			}
			row.add(CHILDREN, rows);
		}
		return row;
	}
	
	public DBRow toDBRow() {
		DBRow row = new DBRow();
		row.add(ID, getId());
		row.add(PRODUCT_LINE_ID, getProduct_line_id());
		row.add(SORT, getSort());
		row.add(TAX, getTax());
		row.add(PARENTID, getParentid());
		row.add("text",StrUtil.dealNull(getTitle()));
		
		if(null!=this.children && this.children.size()>0){
			DBRow[] rows = new DBRow[this.children.size()];
			int i=0;
			for(ProductCatagorys pc : this.children){
				rows[i]=pc.toDBRow();
				i++;
			}
			row.add(CHILDREN, rows);
		}
		return row;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getProduct_line_id() {
		return product_line_id;
	}
	public void setProduct_line_id(int product_line_id) {
		this.product_line_id = product_line_id;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public double getTax() {
		return tax;
	}
	public void setTax(double tax) {
		this.tax = tax;
	}
	
	private List<ProductCatagorys> children;

	public List<ProductCatagorys> getChildren() {
		return children;
	}
	public void setChildren(List<ProductCatagorys> children) {
		this.children = children;
	}
	@Override
	public int compareTo(ProductCatagorys pc) {
		return this.sort-pc.getSort();
	}
	
}
