package com.cwc.app.floor.api.sbb;

import com.cwc.app.key.ProductFileTypeKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

public class FloorProduct {
	
	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	//根据条件查询商品
	public DBRow[] getProduct(DBRow queryPro, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT p.pc_id, p.p_name,p.catalog_id, p.weight, p.length, p.width, p.heigth, p.length_uom, p.weight_uom, pc.title category_name ");
			sql.append(" FROM product p LEFT JOIN product_catalog pc on p.catalog_id = pc.id ");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProduct()"+e);
		}
	}
	
	//根据商品查询maincode
	public DBRow getProductCode(long id, int type) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT p_code ");
			sql.append(" FROM product_code ");
			sql.append(" WHERE code_type = ");
			sql.append(type);
			sql.append(" AND pc_id = ");
			sql.append(id);
			
			return dbUtilAutoTran.selectSingle(sql.toString());
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductCode()"+e);
		}
	}
	
	//根据商品查询上传图片
	public DBRow[] getProductPicture(long id, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT * ");
			sql.append(" FROM product_file ");
			sql.append(" where pc_id = ");
			sql.append(id);
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductPicture()"+e);
		}
	}
	
	//根据商品查询商品与title,customer关系
	public DBRow[] getProductTitleCustomer(long id, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT tp.*,t.title_name,ci.customer_id as customer_name ");
			sql.append(" FROM title_product tp ");
			sql.append(" LEFT JOIN title t ON tp.tp_title_id = t.title_id ");
			sql.append(" LEFT JOIN customer_id ci ON tp.tp_customer_id = ci.customer_key ");
			sql.append(" WHERE tp_pc_id = ");
			sql.append(id);
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductTitleCustomer()"+e);
		}
	}
	
	//根据商品查询商品与title关系
	public DBRow[] getProductTitle(long id, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT tp.tp_pc_id pc_id,tp.tp_title_id title_id,t.title_name ");
			sql.append(" FROM title_product tp ");
			sql.append(" LEFT JOIN title t ON tp.tp_title_id = t.title_id ");
			sql.append(" WHERE tp_pc_id = ");
			sql.append(id);
			sql.append(" GROUP BY tp_title_id ");
			sql.append(" ORDER BY t.title_name ");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductTitle()"+e);
		}
	}
	
	//根据商品和titleID 查询商品与title,customer关系
	public DBRow[] getProductTitleCustomer(long id, long titleId, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT tp.tp_customer_id customer_id, ci.customer_id customer_name ");
			sql.append(" FROM title_product tp  ");
			sql.append(" LEFT JOIN customer_id ci ON tp.tp_customer_id = ci.customer_key ");
			sql.append("  WHERE tp.tp_pc_id = ");
			
			sql.append(id);
			
			sql.append(" AND tp.tp_title_id = ");
			sql.append(titleId);
			
			sql.append(" ORDER BY ci.customer_id ");
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductTitleCustomer()"+e);
		}
	}
	
	//根据商品查询clp type
	public DBRow[] getProductClpType(long id, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT * ");
			sql.append(" FROM license_plate_type lpt ");
			sql.append(" WHERE pc_id = ");
			sql.append(id);
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductClpType()"+e);
		}
	}
	
	//根据ClpType查询title custoemr shipto
	public DBRow[] getClpTypeTitleCustomerShipTo(long id, PageCtrl pc) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT ltst.customer_id,ltst.title_id,ltst.ship_to_id,IFNULL(t.title_name,'*') title_name,IFNULL(ci.customer_key,'*') customer_name,IFNULL(st.ship_to_name,'*') ship_to_name  ");
			sql.append(" FROM lp_title_ship_to ltst ");
			sql.append(" LEFT JOIN title t on ltst.title_id = t.title_id ");
			sql.append(" LEFT JOIN customer_id ci on ltst.customer_id = ci.customer_key ");
			sql.append(" LEFT JOIN ship_to st on ltst.ship_to_id = st.ship_to_id ");
			sql.append(" WHERE ltst.lp_type_id = ");
			sql.append(id);
			
			return dbUtilAutoTran.selectMutliple(sql.toString(), pc);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getClpTypeTitleCustomerShipTo()"+e);
		}
	}
	
	//根据商品查询各个类型的文件的总数
	public DBRow[] getProductFileCnt(long id) throws Exception{
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT file_with_class id, count(*) cnt ");
			sql.append(" FROM file  ");
			sql.append(" WHERE file_with_id = ");
			sql.append(id);
			sql.append(" AND file_with_type = 7 AND expire_in_secs = '0' GROUP BY file_with_class ");
			
			DBRow[] fileCnt = dbUtilAutoTran.selectMutliple(sql.toString());
			
			DBRow[] fileType = new ProductFileTypeKey().getKey();
			
			//组装键值对 组装结果为[id:1,name:pkg,cnt:2]
			for(DBRow one : fileType){
				
				one.put("cnt", 0);
				
				for(DBRow two : fileCnt){
					
					if(one.get("id", 0) == two.get("id", 0)){
						
						one.put("cnt", two.get("cnt", 0));
					}
				}
			}
			
			return fileType;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductFileCnt()"+e);
		}
	}
	
	//根据商品查询各个类型的文件
	public DBRow[] getProductFile(long id, int type) throws Exception {
		
		try {
			
			StringBuilder sql = new StringBuilder();
			
			sql.append(" SELECT file_id, file_with_class ,file_is_convert file_cover ");
			sql.append(" FROM file ");
			sql.append(" WHERE file_with_id = ");
			sql.append(id);
			sql.append(" AND file_with_type = ");
			sql.append(type);
			sql.append(" AND expire_in_secs = 0 ");
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql.toString());
			
			return result;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProductFile()"+e);
		}
	}
	
	//更新file表
	public int updateFile(long fileId, DBRow row) throws Exception {
		
		try {
			
			String sql = " where file_id = " + fileId;
			
			return dbUtilAutoTran.update(sql, "file", row);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.updateFile()"+e);
		}
	}
	
	//根据pc_id查询商品  -- yuanxinyu
	public DBRow getProduct(long pc_id) throws Exception{
		
		try {
			DBRow product = new DBRow();
			
			if(pc_id > 0)
			{
				StringBuilder sql = new StringBuilder();
				
				sql.append(" SELECT p.pc_id, p.p_name,p.catalog_id, p.weight, p.length, p.width, p.heigth, p.volume, p.length_uom, p.weight_uom, pc.title category_name, pcode.p_code as main_code ");
				
				sql.append(" FROM product p LEFT JOIN product_catalog pc on p.catalog_id = pc.id ");
				
				sql.append(" LEFT JOIN product_code pcode ON p.pc_id = pcode.pc_id and code_type = 1 ");
			
				sql.append(" WHERE p.pc_id = " + pc_id + " ");
				
				product = dbUtilAutoTran.selectSingle(sql.toString());
			}
			
			return product;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			throw new Exception("FloorProduct.getProduct(long pc_id)"+e);
		}
	}
}
