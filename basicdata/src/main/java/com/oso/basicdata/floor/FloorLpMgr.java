package com.oso.basicdata.floor;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**  
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.floor.FloorPackagingTypeMgr.java] 
 * @ClassName:    [FloorPackagingTypeMgr]  
 * @Description:  [PackagingType]  
 * @Author:       [zyj]  
 * @CreateDate:   [2015年6月24日 下午3:20:08]  
 */
public class FloorLpMgr {
	
	
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	
	/**
	 * 创建lp
	 * @param insertRow
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月26日 下午2:48:17
	 */
	public DBRow addLp(DBRow insertRow) throws Exception
	   {
	     try
	     {
	       String tableName = ConfigBean.getStringValue("container");
	       String containerName = insertRow.getString("container");
	       if (!com.cwc.app.util.StrUtil.isBlank(containerName)) {
	         insertRow.add("container", containerName);
	       }
	       long containerId = dbUtilAutoTran.insertReturnId(tableName, insertRow);
	       if (com.cwc.app.util.StrUtil.isBlank(containerName)) {
	         containerName = ""+containerId;
	         DBRow updateData = new DBRow();
	         updateData.add("container", containerName);
	         
	         updateLp(containerId, updateData);
	         
	         insertRow.add("container", containerName);
	       }
	       insertRow.add("con_id", containerId);
	       return insertRow;
	     }
	     catch (Exception e)
	     {
	       throw new Exception("FloorLpMgr.addLp(insertRow):" + e);
	     }
	   }
	
	
	
	/**
	 * 更新容器
	 * @param lp_id
	 * @param updateRow
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月26日 下午2:54:49
	 */
	public void updateLp(long lp_id, DBRow updateRow) throws Exception 
	{
		try
		{
			String tableName = ConfigBean.getStringValue("container");
			this.dbUtilAutoTran.update(" where con_id=" + lp_id, tableName, updateRow);
		}
		catch (Exception e) 
		{
			throw new Exception( "FloorLpMgr.updateLp(lp_id , updateRow):" + e);
		}
	}
	
	
	/**
	 * 查询LP
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getLpsLikeName(PageCtrl pc, String searchKey) throws Exception
	{
		try 
		{
			
			String sql = "select * from container where 1=1 ";
			
			if(searchKey != null && !searchKey.equals("")){
				
				sql += " and container like '%"+searchKey.trim()+"%' ";
			}
			
			sql += " order by con_id desc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
			
		} catch (Exception e) {
			
			throw new Exception("FloorLpMgr getContainerTypesLikeName error:" +e);
		}
	}
	
	
	
	/**
	 * 验证硬件编号是否存在
	 * @param hardwareId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月27日 上午10:49:21
	 */
	public boolean isExitsHardwareId(String hardwareId ) throws Exception
	{
		boolean flag = false ;
		try{
			String tableName = ConfigBean.getStringValue("container");
			StringBuffer sql = new StringBuffer();
			sql.append("select count(*) as count_sum from ").append(tableName).append(" where hardwareId= ? ");
			DBRow para = new DBRow();
			para.add("hardwareId", hardwareId);
			DBRow data = dbUtilAutoTran.selectPreSingle(sql.toString(), para);
			if(data != null && data.get("count_sum", 0) > 0){
				flag = true ;
			}
			return flag;
		}catch (Exception e) {
			throw new Exception("FloorLpMgr isExitsHardwareId(hardwareId):"+e);
		}
	}
	
	
	/**
	 * 查询lp
	 * @param key
	 * @param type
	 * @param container_type
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月27日 下午12:04:00
	 */
	public DBRow[] findContainerByTypeContainerTypeName(String key,long type ,int container_type, PageCtrl pc) throws Exception 
	{
		
		try {
			
			StringBuffer sb = new StringBuffer();
			
			if( ContainerTypeKey.CLP == container_type ) {
				
				sb.append(" SELECT c.*, ct.type_name,ctt.pc_id FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id ");
				sb.append(" LEFT JOIN container_type ct ON ct.type_id = ctt.basic_type_id WHERE 1=1");
				if(container_type > 0)
				{
					sb.append(" AND c.container_type = " + container_type);
				}
				if(!StrUtil.isBlank(key))
				{
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				if(type > 0)
				{
					sb.append(" AND ct.type_id = " + type);
				}
				
				sb.append(" ORDER BY c.con_id DESC ");
				
			} else if(ContainerTypeKey.TLP == container_type) {
				
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id ");
				sb.append(" WHERE 1=1");
				
				if(container_type > 0) {
					sb.append(" AND c.container_type = " + container_type);
				}
				
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				
				sb.append(" ORDER BY c.con_id DESC ");
				
			} else {
				
				sb.append(" select * from ( ");
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id ");
				//sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN license_plate_type ctt ON c.type_id = ctt.lpt_id AND c.container_type = " + ContainerTypeKey.CLP);
				sb.append(" LEFT JOIN container_type ct ON ct.type_id = ctt.basic_type_id ");
				//sb.append(" WHERE 1=1");
				sb.append(" where c.container_type = ").append(ContainerTypeKey.CLP);
				
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				sb.append(" UNION");
				
				sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id ");
				//sb.append(" SELECT c.*, ct.type_name FROM container c left JOIN container_type ct ON c.type_id = ct.type_id AND c.container_type = " + ContainerTypeKey.TLP);
				//sb.append(" WHERE 1=1 ");
				sb.append(" where c.container_type = ").append(ContainerTypeKey.TLP);
				if(type > 0) {
					sb.append(" AND ct.type_id = " + type);
				}
				if(!StrUtil.isBlank(key)) {
					sb.append(" AND c.container LIKE '%" + key + "%'");
				}
				
				sb.append(" ) as container ORDER BY con_id DESC ");
			}
			
			if(null != pc) {
				return dbUtilAutoTran.selectMutliple(sb.toString(), pc);
				
			} else {
				
				return dbUtilAutoTran.selectMutliple(sb.toString());
			}
		} catch (Exception e) {
			throw new Exception("FloorContainerMgrZyj.findContainerByTypeContainerTypeName:"+e);
		}
	}
	
	
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	
	
	
}
