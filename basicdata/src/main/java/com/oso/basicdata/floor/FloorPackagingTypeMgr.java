package com.oso.basicdata.floor;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**  
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.floor.FloorPackagingTypeMgr.java] 
 * @ClassName:    [FloorPackagingTypeMgr]  
 * @Description:  [PackagingType]  
 * @Author:       [zyj]  
 * @CreateDate:   [2015年6月24日 下午3:20:08]  
 */
public class FloorPackagingTypeMgr {
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	
	
	/**
	 * 根据类型名查类型信息
	 * @param typeName
	 * @return
	 * @throws Exception
	 * @date: 2015年6月25日 下午3:56:11
	 */
	public DBRow getPackagingTypeByName(String typeName)throws Exception
	{
		try 
		{
			String sql = "select * from "+ConfigBean.getStringValue("container_type")+" where type_name='"+typeName+"'";
			return dbUtilAutoTran.selectSingle(sql);
			
		} catch (Exception e) {
			throw new Exception("FloorPackagingTypeMgr getPackagingTypeByName error:" +e);
		}
	}
	
	
	/**
	 * 查询PackagingType
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContainerTypesLikeName(PageCtrl pc, String searchKey) throws Exception
	{
		try 
		{
			
			String sql = "select * from container_type where 1=1 ";
			
			if(searchKey != null && !searchKey.equals("")){
				
				sql += " and type_name like '%"+searchKey.trim()+"%' ";
			}
			
			sql += " order by type_id desc ";
			
			return dbUtilAutoTran.selectMutliple(sql, pc);
			
		} catch (Exception e) {
			
			throw new Exception("FloorPackagingTypeMgr getContainerTypesLikeName error:" +e);
		}
	}
	
	
	/**
	 * 添加PackagingType
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPackagingType(DBRow row)throws Exception
	{
		try
		{
			return dbUtilAutoTran.insertReturnId(ConfigBean.getStringValue("container_type"), row);
		} 
		catch (Exception e)
		{
			throw new Exception("FloorPackagingTypeMgr addPackagingType error:" +e);
		}
	}
	
	
	public DBUtilAutoTran getDbUtilAutoTran() {
		return dbUtilAutoTran;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	
	
	
}
