package com.oso.basicdata.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.key.BCSKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.MoneyUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.service.impl.PackagingTypeMgr;

/**  
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.PackagingTypeController.java] 
 * @ClassName:    [PackagingTypeController]  
 * @Description:  [PackagingType]  
 * @Author:       [zyj]  
 * @CreateDate:   [2015年6月24日 下午3:20:08]  
 */
@RestController
public class PackagingTypeController{
	
	@Autowired
	private PackagingTypeMgr packagingTypeMgr;
	
	
	@Transactional
	@RequestMapping(value = "/packaging/add", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addPackagingType(@RequestBody MultiValueMap<String, String> param, HttpSession session) throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		JSONObject jsonData = new JSONObject(param.get("json"));
		DBRow row = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		row.put("creator", Long.valueOf(adminBean.get("adid")+""));
		row.put("create_time", DateUtil.NowStr());
		
		float length = row.get("length", 0F);
		float width = row.get("width", 0F);
		float height = row.get("height", 0F);
		float weight = row.get("weight", 0F);
		float max_load = row.get("max_load", 0F);
		float max_height = row.get("max_height", 0F);
		length = MoneyUtil.round(length, 2);
		width = MoneyUtil.round(width, 2);
		height = MoneyUtil.round(height, 2);
		weight = MoneyUtil.round(weight, 2);
		max_load = MoneyUtil.round(max_load, 2);
		max_height = MoneyUtil.round(max_height, 2);
		
		DBRow isExistContainerType = packagingTypeMgr.getPackagingTypeByName(row.getString("type_name"));
		
		int check_result = YesOrNotKey.YES;
		int err = 0;
		if (isExistContainerType != null) {
			check_result = YesOrNotKey.NO;
			err = BCSKey.PackagingTypeExist;
		}
		
		row.add("length", length);
		row.add("width", width);
		row.add("height", height);
		row.add("weight", weight);
		row.add("max_load", max_load);
		row.add("max_height", max_height);
		
		packagingTypeMgr.addPackagingType(row);
		
		Map<String,Object> result = new HashMap<String, Object>();
		
		if(check_result == YesOrNotKey.YES){
			result.put("ret",BCSKey.SUCCESS);
			result.put("err",err);
		} else {
			result.put("ret",BCSKey.FAIL);
			result.put("err",err);
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/packaging/search", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> searchPackagingType(
			@RequestParam("page_no") int page ,
			@RequestParam("type_name") String type_name,
			HttpSession session) throws Exception {
		
		//分页
		PageCtrl pc = null;
		if(page > 0)
		{
			pc = new PageCtrl();
			pc.setPageNo(page);
			pc.setPageSize(10);
		}
		
		DBRow[] rows = packagingTypeMgr.getContainerTypesLikeName(pc, type_name);
		
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("packagings", rows);
		
		return result;
	}
	
}
