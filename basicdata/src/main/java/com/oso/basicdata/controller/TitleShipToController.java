package com.oso.basicdata.controller;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.fa.basicdata.FloorTitleShipToMgrZYY;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.mongodb.util.JSON;
import com.oso.basicdata.beans.TitleShipTo;
import com.oso.basicdata.key.ProcessStepKey;
import com.oso.basicdata.service.iface.TitleShiptoRequirementConfigMgrIFace;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.controller.TitleShipToController.java]
 * @ClassName: [TitleShipToController]
 * @Description: [一句话描述该类的功能]
 * @Author: [赵永亚]
 * @CreateDate: [2015年3月24日 上午10:30:58]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年3月24日 上午10:30:58]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
@RestController
@RequestMapping(value = "/tstc")
public class TitleShipToController {

	private final static String SUCCESS="success";
	private final static String DES="des";
	
	@Autowired
	private FloorTitleShipToMgrZYY titleShipToMgr;
	@Autowired
	private FloorProprietaryMgrZyj proprietaryMgrZyj;
	
	@Autowired
	private FloorShipToMgrZJ shipToMgrZJ;
	
	@Autowired
	private TitleShiptoRequirementConfigMgrIFace titleShiptoRequirementConfigMgr;
	
		@RequestMapping(value = "/params", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow params(HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow row = new DBRow();
		//获取Title 
		DBRow[] titles = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(0l, null, null);
		DBRow[] shipTo = shipToMgrZJ.getAllShipTo();
		DBRow[] customers = titleShipToMgr.getAllCustomer();
		DBRow[] processSteps = new ProcessStepKey().toDBRows();
		//Requirement
		row.put("titles", titles);
		row.put("shipTo", shipTo);
		row.put("customers", customers);
		row.put("processSteps", processSteps);
		
		return row;
	}

	@RequestMapping(value = "/handleTst", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public DBRow create( @RequestBody String data, HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map map = (Map) JSON.parse(new String(data.getBytes("iso-8859-1"), "utf-8"));
		DBRow row = new DBRow();
		
		String _titleId= map.get(TitleShipTo.TITLE_ID)==null?"0":map.get(TitleShipTo.TITLE_ID).toString();
		String _config_type= map.get(TitleShipTo.CONFIG_TYPE)==null?"0":map.get(TitleShipTo.CONFIG_TYPE).toString();
		String _config_id= map.get(TitleShipTo.CONFIG_ID)==null?"0":map.get(TitleShipTo.CONFIG_ID).toString();
		String _ship_to_id= map.get(TitleShipTo.SHIP_TO_ID)==null?"0":map.get(TitleShipTo.SHIP_TO_ID).toString();
		String _customer_key= map.get(TitleShipTo.CUSTOMER_KEY)==null?"0":map.get(TitleShipTo.CUSTOMER_KEY).toString();
		int process_step = (map.get(TitleShipTo.PROCESS_STEP)==null) ? 0 : Integer.valueOf(map.get(TitleShipTo.PROCESS_STEP).toString());
		
		_titleId = StrUtil.isBlank(_titleId)?"0":_titleId;
		_config_type = StrUtil.isBlank(_config_type)?"0":_config_type;
		_config_id = StrUtil.isBlank(_config_id)?"0":_config_id;
		_ship_to_id = StrUtil.isBlank(_ship_to_id)?"0":_ship_to_id;
		_customer_key = StrUtil.isBlank(_customer_key)?"0":_customer_key;

		row.put(TitleShipTo.TITLE_ID, URLDecoder.decode(_titleId, "UTF-8"));
		row.put(TitleShipTo.CONFIG_TYPE, URLDecoder.decode(_config_type, "UTF-8"));
		row.put(TitleShipTo.CONFIG_ID, URLDecoder.decode(_config_id, "UTF-8"));
		row.put(TitleShipTo.SHIP_TO_ID, URLDecoder.decode(_ship_to_id, "UTF-8"));
		row.put(TitleShipTo.CUSTOMER_KEY,URLDecoder.decode(_customer_key, "UTF-8"));
		row.put(TitleShipTo.PROCESS_STEP, process_step);

		row.put("customer_key",URLDecoder.decode(map.get("customer_key").toString(), "UTF-8"));
		Object items = map.get("items");
		
		List<Map> valMap =  (List<Map>) JSON.parse(items.toString());
		int size = valMap.size();
		DBRow[] subRows = new DBRow[size];
		
		for(int i =0;i<size;i++){
			Map _map = valMap.get(i);
			DBRow _row = new DBRow();
			_row.add("ri_id",_map.get("ri_id"));
			//_map.get("requirement_value_int") 为null 或者“”时  值设置为-1;
			if(!StrUtil.isBlank(_map.get("requirement_value_int").toString())){
				_row.add("requirement_value_int",_map.get("requirement_value_int"));
			}
			_row.add("requirement_value_str",_map.get("requirement_value_str"));
			subRows[i]=_row;
		}
		
		int key =titleShipToMgr.addTst(row,subRows);
		DBRow dbrow = null;
		switch (key) {
		case 0:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 0);
			dbrow.add(DES, "Add faile");
			break;
		case 1:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 1);
			dbrow.add(DES, "Add success");
		break;case 2:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 2);
			dbrow.add(DES, " Configuration already exists");
			break;
		default:
			break;
		}
		return dbrow;
	}

	
	
	@RequestMapping(value = "/handleTst/{tstid}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public DBRow modify(@PathVariable("tstid")int tstid ,@RequestBody String data, HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		Map map = (Map) JSON.parse(new String(data.getBytes("iso-8859-1"), "utf-8"));
		DBRow row = new DBRow();
		row.put(TitleShipTo.TST_ID, tstid);
		String _titleId= map.get(TitleShipTo.TITLE_ID)==null?"0":map.get(TitleShipTo.TITLE_ID).toString();
		String _config_type= map.get(TitleShipTo.CONFIG_TYPE)==null?"0":map.get(TitleShipTo.CONFIG_TYPE).toString();
		String _config_id= map.get(TitleShipTo.CONFIG_ID)==null?"0":map.get(TitleShipTo.CONFIG_ID).toString();
		String _ship_to_id= map.get(TitleShipTo.SHIP_TO_ID)==null?"0":map.get(TitleShipTo.SHIP_TO_ID).toString();
		String _customer_key= map.get(TitleShipTo.CUSTOMER_KEY)==null?"0":map.get(TitleShipTo.CUSTOMER_KEY).toString();
		int process_step = (map.get(TitleShipTo.PROCESS_STEP)==null) ? 0 : Integer.valueOf(map.get(TitleShipTo.PROCESS_STEP).toString());
		
		_titleId = StrUtil.isBlank(_titleId)?"0":_titleId;
		_config_type = StrUtil.isBlank(_config_type)?"0":_config_type;
		_config_id = StrUtil.isBlank(_config_id)?"0":_config_id;
		_ship_to_id = StrUtil.isBlank(_ship_to_id)?"0":_ship_to_id;
		_customer_key = StrUtil.isBlank(_customer_key)?"0":_customer_key;

		row.put(TitleShipTo.TITLE_ID, URLDecoder.decode(_titleId, "UTF-8"));
		row.put(TitleShipTo.CONFIG_TYPE, URLDecoder.decode(_config_type, "UTF-8"));
		row.put(TitleShipTo.CONFIG_ID, URLDecoder.decode(_config_id, "UTF-8"));
		row.put(TitleShipTo.SHIP_TO_ID, URLDecoder.decode(_ship_to_id, "UTF-8"));
		row.put(TitleShipTo.CUSTOMER_KEY,URLDecoder.decode(_customer_key, "UTF-8"));
		row.put(TitleShipTo.PROCESS_STEP, process_step);
		
		Object items = map.get("items");
		
		List<Map> valMap =  (List<Map>) JSON.parse(items.toString());
		int size = valMap.size();
		DBRow[] subRows = new DBRow[size];
		
		for(int i =0;i<size;i++){
			Map _map = valMap.get(i);
			DBRow _row = new DBRow();
			_row.add("ri_id",_map.get("ri_id"));
			//_map.get("requirement_value_int") 为null 或者“”时  值设置为-1;
			if(!StrUtil.isBlank(_map.get("requirement_value_int").toString())){
				_row.add("requirement_value_int",_map.get("requirement_value_int"));
			}
			_row.add("requirement_value_str",_map.get("requirement_value_str"));
			subRows[i]=_row;
		}
		
		
		int key =titleShipToMgr.updateTst(row,subRows);
		DBRow dbrow = null;
		switch (key) {
		case 0:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 0);
			dbrow.add(DES, "Update faile");
			break;
		case 1:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 1);
			dbrow.add(DES, "Update success");
		break;case 2:
			dbrow = new DBRow();
			dbrow.add(SUCCESS, 2);
			dbrow.add(DES, " Configuration already exists");
			break;
		default:
			break;
		}
		return dbrow;
	}
	
	@RequestMapping(value = "/handleTst", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public DBRow del(@RequestParam(value="tstid") Integer tstid,HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow row = new DBRow();
		try{
			titleShipToMgr.deleteTstById(tstid);
			row.put("success", "true");
		}catch(Exception e){
			row.put("success", "false");
			throw new Exception("TitleShipToController del >>"+e);
		}
		return row;
	}
	@RequestMapping(value = "/handleTst/{tstid}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow fetch(@PathVariable(value="tstid") Integer tstid,HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try{
			DBRow row  = titleShipToMgr.findTstById(tstid);
			return row;
		}catch(Exception e){
			throw new Exception("TitleShipToController fetch >>"+e);
		}
		
	}
	@RequestMapping(value = "/handleTst", produces = "application/json; charset=UTF-8",method = RequestMethod.GET)
	public DBRow list(
			@RequestParam(value="title_id",defaultValue="0") int title_id,
			@RequestParam(value="customer_key",defaultValue="0") int customer_key,
			@RequestParam(value="ship_to_id",defaultValue="0") int ship_to_id,
			@RequestParam(value="process_step",defaultValue="0") int process_step,
			@RequestParam(value="config_id",defaultValue="0") int config_id,
			@RequestParam(value="item_value",defaultValue="") String item_value,
			@RequestParam(value="accurately",defaultValue="0") boolean accurately,
			@RequestParam(value="config_type",defaultValue="0") int config_type,
			@RequestParam(value="config",defaultValue="0") int config,
			@RequestParam(value="tsc_id",defaultValue="0") int tsc_id,
			@RequestParam(value="pageSize" ,required=false) Integer rows,
			@RequestParam(value="pageNo" ,required=false) Integer page,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		PageCtrl pc = new PageCtrl();
		if(null==rows){
			rows=10;
		}
		if(null==page){
			page=1;
		}
		pc.setPageNo(page);
		pc.setPageSize(rows);
		
		//DBRow[] dataRows = titleShipToMgr.findTstByTitleIdAndShipTo(customer_key,title_id, ship_to_id,config_id,item_value,pc,accurately, config_type, config, tsc_id);
		DBRow[] dataRows = titleShiptoRequirementConfigMgr.searchConfigByTitleShipto(config_type, config, title_id, ship_to_id, customer_key, tsc_id, config_id, item_value, pc, accurately,process_step);
		
		DBRow DBRows = new DBRow();
		DBRows.put("list", dataRows);
		DBRows.put("pageCtrl", pc);
		
		//TODO 拼SQL
		return DBRows;
	}

}
