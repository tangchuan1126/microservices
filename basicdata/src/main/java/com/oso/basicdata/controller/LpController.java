package com.oso.basicdata.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.key.BCSKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.service.impl.LpMgr;

/**  
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.LpController.java] 
 * @ClassName:    [LpController]  
 * @Description:  [lp]  
 * @Author:       [zyj]  
 * @CreateDate:   [2015年6月24日 下午3:20:08]  
 */
@RestController
public class LpController{
	
	@Autowired
	private LpMgr lpMgr;
	
	
	@Transactional
	@RequestMapping(value = "/lp/add", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addContainer(@RequestBody MultiValueMap<String, String> param, HttpSession session) throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		JSONObject jsonData = new JSONObject(param.get("json"));
		DBRow row = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		row.put("create_user", Long.valueOf(adminBean.get("adid")+""));
		row.put("create_date", DateUtil.NowStr());
		
	
		String hardwareId = row.getString( "hardwareId");
		int check_result = YesOrNotKey.YES;
		long lp_id = 0;
		int err = 0;
		if(!StrUtil.isBlank(hardwareId))
		{
			check_result = YesOrNotKey.NO;
			err = BCSKey.RFIDExist;
		}
		else
		{
			lp_id = lpMgr.addLp(row).get("con_id", 0L);
		}
		
		Map<String,Object> result = new HashMap<String, Object>();
		
		if(check_result == YesOrNotKey.YES){
			result.put("ret",BCSKey.SUCCESS);
			result.put("lp_id", lp_id);
			result.put("err",err);
		} else {
			result.put("ret",BCSKey.FAIL);
			result.put("err",err);
		}
		
		return result;
	}
	
	
	
	@RequestMapping(value = "/lp/search", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> searchContainer(
			@RequestParam("page_no") int page ,
			@RequestParam("container") String key,
			@RequestParam("type_id") long typeId ,
			@RequestParam("container_type") int container_type,
			HttpSession session) throws Exception {
		
		//分页
		PageCtrl pc = null;
		if(page > 0)
		{
			pc = new PageCtrl();
			pc.setPageNo(page);
			pc.setPageSize(10);
		}
		DBRow[] rows = lpMgr.findContainerByTypeContainerTypeName(key,typeId,container_type,pc);;
		
		Map<String,Object> result = new HashMap<String, Object>();
		result.put("containers", rows);
		
		return result;
	}



	public LpMgr getLpMgr() {
		return lpMgr;
	}



	public void setLpMgr(LpMgr lpMgr) {
		this.lpMgr = lpMgr;
	}
	
}
