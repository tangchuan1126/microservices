package com.oso.basicdata.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.fa.basicdata.FloorRequirementItemsMgrZYY;
import com.cwc.app.floor.api.fa.basicdata.FloorTitleShipToMgrZYY;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.oso.basicdata.controller.iface.TitleControllerIFace;
import com.oso.basicdata.key.ProcessStepKey;
import com.oso.basicdata.service.TitleService;
import com.oso.customer.models.Title;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.controller.TitleController.java]
 * @ClassName: [TitleController]
 * @Description: [Title 的控制类 ]
 * @Author: [赵永亚]
 * @CreateDate: [2015年3月24日 下午5:31:21]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年3月24日 下午5:31:21]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
@RestController
public class TitleController implements TitleControllerIFace{
	@Autowired
	private FloorProprietaryMgrZyj proprietaryMgrZyj;
	@Autowired
	private FloorRequirementItemsMgrZYY requirementItemsMgrZYY;
	@Autowired
	private FloorShipToMgrZJ shipToMgrZJ;
	@Autowired
	private FloorTitleShipToMgrZYY titleShipToMgr;
	@Autowired
	private TitleService titleService;
	
	@Override
	public @ResponseBody void getTitle(HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try{
			DBRow req = new DBRow();
			req.add("searchIsactive", 1);
			DBRow[] requ = requirementItemsMgrZYY.getAllRequirement(false, null,req);
			DBRow[] ship = shipToMgrZJ.getAllShipTo();
			DBRow[] title = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(0l, null, null);
			DBRow[] customers = titleShipToMgr.getAllCustomer();
			DBRow[] processSteps = new ProcessStepKey().toDBRows();
			
			JSONObject output = new JSONObject();
			output.put("requirements", DBRowUtils.dbRowArrayAsJSON(requ));
			output.put("shiptos", DBRowUtils.dbRowArrayAsJSON(ship));
			output.put("titles", DBRowUtils.dbRowArrayAsJSON(title));
			output.put("customers", DBRowUtils.dbRowArrayAsJSON(customers));
			output.put("processsteps", DBRowUtils.dbRowArrayAsJSON(processSteps));
			
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(output.toString());
			//return  output;
		}catch(Exception e){
			throw new Exception("TitleController->getTitle"+e.getMessage());
		}
	}

	@Override
	public @ResponseBody void getAll(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		try{
			DBRow[] title = proprietaryMgrZyj.findProprietaryAllOrByAdidTitleId(0l, null, null);
			response.setCharacterEncoding("UTF-8");
			response.setContentType("application/json");
			response.getWriter().print(DBRowUtils.dbRowArrayAsJSON(title));
			//return  output;
		}catch(Exception e){
			throw new Exception("TitleController->getTitle"+e.getMessage());
		}
	}
	
	@Override
	@RequestMapping(value = "/customer/{customerId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Title> getByCustomer(@PathVariable("customerId") int customerId) throws Exception{
		return this.titleService.getTitlesByCustomerId(customerId);
	}
}
