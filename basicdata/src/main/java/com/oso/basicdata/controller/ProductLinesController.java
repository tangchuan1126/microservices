package com.oso.basicdata.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.db.DBRow;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.controller.ProductLinesController.java]
 * @ClassName: [ProductLinesController]
 * @Description: [产品线控制]
 * @Author: [赵永亚]
 * @CreateDate: [2015年3月26日 上午9:08:23]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年3月26日 上午9:08:23]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
@RestController
@RequestMapping(value = "productLine")
public class ProductLinesController {

	@Autowired
	private FloorProprietaryMgrZyj proprietaryMgrZyj;

	@RequestMapping(value = "/getByTitleId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] findProductLinesIdAndNameByTitleId(
			@RequestParam(value = "title_id", required = false) Long title_id,
			@RequestParam(value = "customer_id", required = false) String customer_id_str,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try {
			Long[] title_ids = null; 
			long customer_id = 0L;
			if(customer_id_str != null && customer_id_str.trim().length() > 0){
				customer_id = Long.parseLong(customer_id_str);
			}
			
			if(null!=title_id){
				title_ids =new Long[1];
				title_ids[0] = title_id;
			}
			DBRow[] rows = proprietaryMgrZyj.findProductLinesIdAndNameByTitleId( null,customer_id, title_ids, 0, 0,null);
			// TODO 处理成 children 的形式
			return rows;
		} catch (Exception e) {
			throw new Exception("ProductLinesController->findProductLinesIdAndNameByTitleId"+ e.getMessage());
		}
	}

}
