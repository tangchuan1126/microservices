package com.oso.basicdata.controller.iface;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.DBRow;

@RestController
public interface RequirementControllerIFace {
	
	@RequestMapping(value = "/tstc/requirements", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow getAllRequirements(@RequestParam(value="titleId", defaultValue="-1") long titleId, @RequestParam(value="shipToId", defaultValue="-1") long shipToId, @RequestParam(value="riId", defaultValue="-1") long riId, @RequestParam(value="pageNo", defaultValue="1") int pageNo, @RequestParam(value="pageSize", defaultValue="10") int pageSize, @RequestParam(value="pageSize", defaultValue="") String searchConditions);
	
	@RequestMapping(value = "/tstc/requirements/{riId}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public DBRow delRequirement(@PathVariable(value="riId") long riId);
}
