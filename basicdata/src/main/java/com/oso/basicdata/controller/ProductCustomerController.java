package com.oso.basicdata.controller;

import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.PageCtrl;
import com.oso.basicdata.beans.Customer;
import com.oso.basicdata.service.ProductCustomerSerivce;
import com.oso.basicdata.service.TitleService;
import com.oso.customer.models.ProductCustomer;
import com.oso.customer.models.Title;

/**
 * 
 * @author lujintao
 *
 */
@RestController
public class ProductCustomerController  {
	@Autowired
	private ProductCustomerSerivce productCustomerSerivce;
	@Autowired
	private TitleService titleService;
	//表示用户身份是Admin
	static final String ADMIN = "admin";
	//表示用户身份是Customer
	static final String CUSTOMER = "customer";
	//表示用户身份是Title
	static final String TITLE = "title";
	
	/**
	 * 添加
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> add(@RequestBody  ProductCustomer productCustomer,HttpServletRequest request) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> error_map = this.validateInput(productCustomer,false,this.isCustomer(request));
		boolean flag = false;
		
		if(error_map.size() > 0){
			map.put("errors", error_map);
		}else{
		    flag = this.productCustomerSerivce.add(productCustomer);
			if(!flag){
				error_map.put("server", "Error ocurs in server");
				map.put("errors", error_map);
			}
		}
		map.put("success", flag);	
		return map;
	}
	
	/**
	 * 修改菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{titleId}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> update(@RequestBody  ProductCustomer productCustomer,@PathVariable("productId")int productId,@PathVariable("titleId")int titleId,HttpServletRequest request) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		productCustomer.setProductId(productId);
		Title title = new Title();
		title.setId(titleId);
		productCustomer.setTitle(title);
		Map<String,Object> error_map = this.validateInput(productCustomer,true,this.isCustomer(request));
		boolean flag = false;
		if(error_map.size() == 0){
			try{
				flag = this.productCustomerSerivce.update(productCustomer);
			}catch(Exception ex){
				flag = false;
			}
		    if(!flag){
		    	error_map.put("server", "Error occured in server");
		    }
		}
		map.put("errors", error_map);
		map.put("success", flag);
		return map;
	}


	
	/**
	 * 删除品牌商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{titleId}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public Map<String,Object> delete(@PathVariable("productId") int productId,@PathVariable("titleId") int titleId) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,String> errors = new HashMap<String,String>();
		boolean flag = true;
		if(this.productCustomerSerivce.isAllowDelete(productId, titleId)){
			 flag = this.productCustomerSerivce.deleteByTitle(productId, titleId);
			 if(!flag){
				 errors.put("server","Failed");
			 }
		}else{
			flag = false;
			errors.put("data_error","NOT_ALLOWED");
		}
		
		result.put("success", flag);
		result.put("errors", errors);
		return result;
	}

	/**
	 * 为某种产品的品牌商移除生产商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{customerId}/{titleId}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public Map<String,Object> delete(@PathVariable("productId") int productId,@PathVariable("customerId") int customerId
			,@PathVariable("titleId") int titleId) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		Map<String,String> errors = new HashMap<String,String>();
		boolean flag = true;
		if(this.productCustomerSerivce.isAllowDelete(productId, titleId,customerId)){
			 flag = this.productCustomerSerivce.deleteByTitle(productId,customerId,titleId);
			 if(!flag){
				 errors.put("server","Failed");
			 }
		}else{
			flag = false;
			errors.put("data_error","NOT_ALLOWED");
		}
		
		result.put("success", flag);
		result.put("errors", errors);
		return result;
	}
	
	/**
	 * 判断指定的品牌商是否已和特定产品建立关系
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{titleId}/exist", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> exist(@PathVariable("productId") int productId,@PathVariable("titleId") int titleId) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", !this.productCustomerSerivce.exist(productId, titleId));
		return result;
	}
	
	/**
	 * 判断指定的生产商、品牌商是否已和特定产品建立关系
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{titleId}/{customerId}/exist", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> exist(@PathVariable("productId") int productId,@PathVariable("titleId") int titleId,@PathVariable("customerId") int customerId) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", !this.productCustomerSerivce.exist(productId, titleId,customerId));
		return result;
	}
	
	/**
	 * 获得与指定产品相关的品牌商和 生产商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<ProductCustomer> get(@PathVariable("productId") int productId) throws Exception{
		return this.productCustomerSerivce.getByProduct(productId);
	}
	
	/**
	 * 获得与指定产品相关的所有品牌商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/customer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Customer> getCustomers(@PathVariable("productId") int productId) throws Exception{
		return this.productCustomerSerivce.getCustomers(productId);
	}
	
	/**
	 * 获得与指定产品相关的所有生产商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/title", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Title> getTitles(@PathVariable("productId") int productId) throws Exception{
		return this.productCustomerSerivce.getTitles(productId);
	}	


	/**
	 * 获得与指定产品相关的所有生产商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{clpTypeId}/{customerId}/title", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Title> getTitles(@PathVariable("productId") int productId,@PathVariable("clpTypeId") int clpTypeId,@PathVariable("customerId") int customerId) throws Exception{
		List<Title> titles = this.titleService.getTitlesByProdAndCLP(productId, clpTypeId, customerId);
		if(titles.size() == 0){
			titles = this.titleService.getTitles(productId, customerId);
		}
		return titles;
	}	
	
	
	/**
	 * 获得指定产品和指定品牌商 相关的生产商
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{titleId}/customer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Customer> getCustomres(@PathVariable("productId") int productId,@PathVariable("titleId") int titleId) throws Exception{
		return this.productCustomerSerivce.getCustomers(productId, titleId);
	}
	
	/**
	 * 获得与指定产品和指定角色的客户相关的ProductCustomer
	 * @param productId  产品ID
	 * @param role   角色  是 customer ,还是title
	 * @param roleId 即CustomerId或 TitleId
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/{role}/{roleId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<ProductCustomer> get(@PathVariable("productId") int productId,@PathVariable("role")String role,@PathVariable("roleId") int roleId) throws Exception{
		List<ProductCustomer>  result = new ArrayList<ProductCustomer>();
		if(role.equals(CUSTOMER)){
			result = this.productCustomerSerivce.getByProductAndCustomer(productId,roleId);
		}else if(role.equals(TITLE)){
			result = this.productCustomerSerivce.getByProductAndTitle(productId, roleId);
		}			

		return result;
	}
	
	/**
	 * 获得与指定产品和指定角色的客户相关的ProductCustomer
	 * @param productId
	 * @param titleId
	 * @param customerId 
	 * @throws Exception
	 */
	@RequestMapping(value = "/productCustomer/{productId}/title/{titleId}/customer/{customerId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<ProductCustomer> get(@PathVariable("productId") int productId,@PathVariable("titleId") int titleId,@PathVariable("customerId") int customerId) throws Exception{
		List<ProductCustomer>  result = new ArrayList<ProductCustomer>();
		if(titleId == 0){
			result = this.productCustomerSerivce.getByProductAndCustomer(productId, customerId);
		}else if(customerId == 0){
			result = this.productCustomerSerivce.getByProductAndTitle(productId, titleId);
		}else{
			result = this.productCustomerSerivce.get(productId, titleId, customerId);
		}
		
		return result;
	}
	
	/**
	 * 对输入数据进行校验
	 * @param productCustomer
	 * @param isUpdate
	 * @param isCustomer  当前是否是Customer用户
	 * @return
	 */
	private Map<String,Object> validateInput(ProductCustomer productCustomer,boolean isUpdate,boolean isCustomer){
		Map<String,Object> error_map = new HashMap<String,Object>();
		if(productCustomer.getProductId() == null || productCustomer.getTitle().getId() == 0 || productCustomer.getCustomers().size() == 0 ){
			error_map.put("data_error", "Some input data is invalid");
		}
		if(!isUpdate){

			if(productCustomer.getCustomers() != null && isCustomer){
				int customerId = productCustomer.getCustomers().get(0).getId();
				if(this.productCustomerSerivce.exist(productCustomer.getProductId(), productCustomer.getTitle().getId(),customerId)){
					error_map.put("data_error", "Title already exists");
				}
			}else{
				if(this.productCustomerSerivce.exist(productCustomer.getProductId(), productCustomer.getTitle().getId())){
					error_map.put("data_error", "Title already exists");
				}				
			}

		}else{
			List<Customer> invalid_customer_list = this.validateUpdate(productCustomer);
			if(invalid_customer_list.size() > 0){
				error_map.put("data_error", "NOT_ALLOWED");
				error_map.put("data", invalid_customer_list);
			}
		}
		
		return error_map;
	}
	
	/**
	 * 判断当前登录用户是否是Customer 用户
	 * @param request
	 * @return
	 */
	private boolean isCustomer(HttpServletRequest request){
		boolean isCustomer = false;
		HttpSession sess = request.getSession(true);
		Map<String,Object> loginUser = (Map<String,Object>) sess.getAttribute("adminSesion");
		int corporationType = (Integer)loginUser.get("corporationType");
		int customerId = 0;
		//存放登录用户类型，表示用户是admin,是Customer,还是Title或者其它
		String userType = "";
		if(corporationType == 1){
			isCustomer = true;
		}
		return isCustomer;
	}

	/**
	 * 对要更新的ProductCustomer 进行验证,返回有问题的Customer ID 的集合
	 * @param productCustomer
	 * @return
	 */
	private List<Customer> validateUpdate(ProductCustomer productCustomer){
		List<Customer> list = new ArrayList<Customer>();
		if(productCustomer != null && productCustomer.getCustomers() != null && productCustomer.getCustomers().size() > 0){
			List<Customer> customers = this.productCustomerSerivce.getCustomers(productCustomer.getProductId(), productCustomer.getTitle().getId());
			
			List<Customer> removeList = new ArrayList<Customer>();
			//如果品牌商 在之前的产品品牌关系中存在，但在新的产品品牌关系中不存在
			for(Customer item : customers){
				if(!this.contain(productCustomer.getCustomers(), item)){
					removeList.add(item);
				}
			}
			if(removeList.size() > 0){
				for(Customer item : removeList){
					if(!this.productCustomerSerivce.isAllowDelete(productCustomer.getProductId(), productCustomer.getTitle().getId(), item.getId())){
						list.add(item);
					}
				}
			}
		}
		return list;
	}
	
	/**
	 * 判断list中是否包含指定的Customer
	 * @param list
	 * @param customer
	 * @return
	 */
	private boolean contain(List<Customer> list,Customer customer){
		boolean flag = false;
		for(Customer item : list){
			if(item.getId().equals(customer.getId())){
				flag = true;
				break;
			}
		}
		return flag;
	}
}
