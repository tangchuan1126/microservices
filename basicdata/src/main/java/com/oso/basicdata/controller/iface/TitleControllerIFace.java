package com.oso.basicdata.controller.iface;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.db.DBRow;
import com.oso.customer.models.Title;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.iface.TitleController.java] 
 * @ClassName:    [TitleController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月9日 下午3:00:49]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月9日 下午3:00:49]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
@RestController
@RequestMapping("/title")
public interface TitleControllerIFace {
	@RequestMapping(value = "/getTitle", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody void getTitle(HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
	
	/**
	 * 获得所有的Title
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAll", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody void getAll(HttpServletRequest request,HttpServletResponse response) throws Exception;	
	
	/**
	 * 获得与指定品牌商相关的Title
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/customer/{customerId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Title>  getByCustomer(@PathVariable("customerId") int customerId) throws Exception;	
}
