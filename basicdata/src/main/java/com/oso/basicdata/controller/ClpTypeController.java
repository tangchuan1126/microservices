package com.oso.basicdata.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.key.BCSKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.oso.basicdata.service.impl.ClpTypeMgr;

@RestController
public class ClpTypeController {
	
	@Autowired
	private ClpTypeMgr clpTypeMgr;
	/**
	 * 根据pc_id获取product基本信息
	 * 
	 * @param pc_id
	 * @throws Exception
	 */
	@RequestMapping(value = "/productInfo", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> getProduct(@RequestParam(value = "pc_id") long pc_id) throws Exception {
		
		Map<String,Object> result = new HashMap<String, Object>();
		//查询商品基本寄信
		DBRow product = clpTypeMgr.getProductInfo(pc_id);
		
		result.put("product", product);
		
		return result;
	}
	
	@RequestMapping(value = "/clpList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> clpList(@RequestParam(value = "basic_container_type" , defaultValue = "0") long basic_container_type,
					@RequestParam(value = "pc_id") long pc_id,
					@RequestParam(value = "cmd") String cmd,
					@RequestParam(value = "title_value" , defaultValue = "0") long title_value,
					@RequestParam(value = "ship_to_value" , defaultValue = "0") long ship_to_value,
					@RequestParam(value = "customer_value" , defaultValue = "0") long customer_value,
					@RequestParam(value = "active" , defaultValue = "1") long active, HttpSession session) throws Exception {
		
		DBRow param_row = new DBRow();
		param_row.add("basic_container_type", basic_container_type);
		param_row.add("pc_id", pc_id);
		param_row.add("cmd", cmd);
		param_row.add("title_value", title_value);
		param_row.add("ship_to_value", ship_to_value);
		param_row.add("customer_value", customer_value);
		param_row.add("active", active);
		
		Map<String,Object> result = new HashMap<String, Object>();
		
		DBRow[] clps = clpTypeMgr.getClpTypeList(param_row, session);
		// packaging type
		DBRow[] containers = clpTypeMgr.getAllPackagingType();
		// title
		List<Title> titles = clpTypeMgr.getAllTitle(param_row);
		// customer
		List<Customer> customers = clpTypeMgr.getAllCustomer(param_row, session);
	    // ship to
	    DBRow[] shipTos = clpTypeMgr.getAllShipTo();
		// active
	    DBRow[] actives = clpTypeMgr.getActives();
	    
	    result.put("clps", clps);
	    result.put("containers", containers);
	    result.put("titles", titles);
	    result.put("customers", customers);
	    result.put("shipTos", shipTos);
	    result.put("actives", actives);
		
	    return result;
	}
	
	
	/**
	 * 手持端 创建clp时，获取添加数据的接口
	 * @return Map<String,Object>
	 * @throws Exception
	 */
	@RequestMapping(value = "/clp/context", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> clpContext(@RequestParam(value = "pc_id") long pc_id) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		DBRow[] packagingTypes = clpTypeMgr.getAllPackagingType();
		DBRow product = clpTypeMgr.getProductInfo(pc_id);
		DBRow[] innerClps = clpTypeMgr.getInnerClps(pc_id);
		
		
		result.put("product", product);
		result.put("inner_clps", innerClps);
		result.put("packaging_types", packagingTypes);
		result.put("ret",BCSKey.SUCCESS);
		result.put("err",0);
		
		return result;
	}
	/**
	 * 手持端 创建clp时，添加数据的接口
	 * @return Map<String,Object>
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/clptype/add", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addClpType(@RequestBody MultiValueMap<String, String> param, HttpSession session) throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		JSONObject jsonData = new JSONObject(param.get("json").get(0));
		DBRow row = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		row.put("adid", Long.valueOf(adminBean.get("adid")+""));
		row.put("sysDate", DateUtil.NowStr());
		
		Map<String, Object> result = clpTypeMgr.vaildateAddClpType(row);
		
		if((boolean)result.get("ret")){
			result = new HashMap<String, Object>();
			result = clpTypeMgr.addClpType(row);
		}
		
		return result;
	}
	
}
