package com.oso.basicdata.controller;

import java.net.URLDecoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.fa.basicdata.FloorRequirementItemsDetailsMgrZYY;
import com.cwc.app.floor.api.fa.basicdata.FloorRequirementItemsMgrZYY;
import com.cwc.app.floor.api.fa.basicdata.RequirementItem;
import com.cwc.app.floor.api.fa.basicdata.RequirementItemDetails;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.mongodb.util.JSON;
import com.oso.basicdata.key.DisplayTypeKey;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.RequirementItemsContrller.java] 
 * @ClassName:    [RequirementItemsContrller]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年4月2日 下午5:44:49]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年4月2日 下午5:44:49]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
@RestController
@RequestMapping("/requirement")
public class RequirementItemsContrller {
	private final static String SUCCESS="success";
	private final static String DES="des";
	
	@RequestMapping(value = "/all", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow[] getAll() throws Exception{
		DBRow param = new DBRow();
		param.add("searchIsactive", 2);
		return requirementItemsMgrZYY.getAllRequirement(false, null,param);
	}
	
	@RequestMapping(value = "/check_name", produces = "application/json; charset=UTF-8")
	public DBRow checkName(@RequestParam(value= "requirement_name") String name ,
			@RequestParam(value= "ri_id",defaultValue="0") int ri_id ,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		boolean flag = requirementItemsMgrZYY.checkExistsByName(name, ri_id);
		DBRow dbrow = new DBRow();
		dbrow.add("success", !flag);
		return dbrow;
	}
	/**
	 * 获取详情列表 
	 * @param searchConditions
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/handleRiDetails", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getList(@RequestParam(value= "searchConditions",required=false ) String searchConditions ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		
		if(StrUtil.isBlankNullUndefined(searchConditions)){
			DBRow[] rows = new DBRow[0];
			DBRow dbRows = new DBRow();
			dbRows.add("list", rows);
			return dbRows;
		}
		
		try{
			String[] ids = searchConditions.split(",");
			int[] idis = new int[ids.length];
			int i = 0;
			for(String id:ids){
				idis[i]= Integer.valueOf(id);
				i++;
			}
			DBRow[] rows =  requirementItemsDetailsMgrZYY.getByRiId(idis);
			DBRow dbRows = new DBRow();
			dbRows.add("list", rows);
			return  dbRows;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->getList"+e.getMessage());
		}
	}
	/**
	 * 获取详情列表 
	 * @param searchConditions
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/handleRiDetails/{ids}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] getListBySearch(@PathVariable(value="ids") String searchConditions ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		
		if(StrUtil.isBlankNullUndefined(searchConditions)){
			DBRow[] rows = new DBRow[0];
			return rows;
		}
		
		try{
			String[] ids = searchConditions.split(",");
			int[] idis = new int[ids.length];
			int i = 0;
			for(String id:ids){
				idis[i]= Integer.valueOf(id);
				i++;
			}
			DBRow[] rows =  requirementItemsDetailsMgrZYY.getByRiId(idis);
			return rows;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->getList"+e.getMessage());
		}
	}
	@RequestMapping(value = "/handleRiDetails/{rid_id}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public DBRow del(@PathVariable(value="rid_id") Integer rid_id,HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		DBRow row = new DBRow();
		try{
			//titleShipToMgr.deleteTstById(tstid);
			requirementItemsDetailsMgrZYY.deleteById(rid_id.intValue());
			row.put("success", "true");
		}catch(Exception e){
			row.put("success", "false");
			throw new Exception("TitleShipToController del >>"+e);
		}
		return row;
	}
	
	@RequestMapping(value = "/handleRItems/{ri_id}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public DBRow modify(@PathVariable("ri_id")int ri_id ,@RequestBody String data, HttpSession session,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		try{
			Map map = (Map) JSON.parse(new String(data.getBytes("iso-8859-1"), "utf-8"));
			DBRow row = new DBRow();
			row.add(RequirementItem.REQUIREMENT_NAME,URLDecoder.decode(
					 map.get(RequirementItem.REQUIREMENT_NAME).toString(), "UTF-8"));
			row.add(RequirementItem.DISPLAY_TYPE,URLDecoder.decode(
					map.get(RequirementItem.DISPLAY_TYPE).toString(), "UTF-8"));
			row.add(RequirementItem.DESCRIPTION,URLDecoder.decode(
					map.get(RequirementItem.DESCRIPTION).toString(), "UTF-8"));
			row.add(RequirementItem.REQUIREMENT_ID,URLDecoder.decode(
					map.get(RequirementItem.REQUIREMENT_ID).toString(), "UTF-8"));
			DBRow[] children=  new DBRow[0];
			Object items = map.get("children");
			if(null!=items&& items.toString().length()>3){
				List<Map> cmap =  (List<Map>) JSON.parse(items.toString());
				int size = cmap.size();
				if(size>0){
					children = new DBRow[size];
					for(int i=0;i<size;i++){
						DBRow r = new DBRow();
						if(cmap.get(i).containsKey(RequirementItemDetails.REQUIREMENT_DETAILS_ID)){
							r.add(RequirementItemDetails.REQUIREMENT_DETAILS_ID, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.REQUIREMENT_DETAILS_ID).toString(), "UTF-8"));
						}
//						r.add(RequirementItemDetails.REQUIREMENT_ID, URLDecoder.decode(cmap.get(i).get(RequirementItemDetails.REQUIREMENT_ID).toString(), "UTF-8"));
						r.add(RequirementItemDetails.ITEM_NAME, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.ITEM_NAME).toString(), "UTF-8"));
						r.add(RequirementItemDetails.ITEM_VALUE, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.ITEM_VALUE).toString(), "UTF-8"));
						r.add(RequirementItemDetails.SORT, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.SORT).toString(), "UTF-8"));
						r.add(RequirementItemDetails.REQUIREMENT_ID, ri_id);
						children[i]=r;
					}
				}
			}
			
			int result = requirementItemsMgrZYY.mod(row, children);
			DBRow dbrow = null;
			switch (result) {
			case 0:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 0);
				dbrow.add(DES, "Update faile");
				break;
			case 1:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 1);
				dbrow.add(DES, "Update success");
			break;case 2:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 2);
				dbrow.add(DES, " Configuration already exists");
				break;
			default:
				break;
			}
			return dbrow;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->modify"+e.getMessage());
		}
	}
	/**
	 * 保存
	 * @param searchConditions
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/handleRItems", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public @ResponseBody DBRow create(@RequestBody String data  ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try{
			Map map = (Map) JSON.parse(new String(data.getBytes("iso-8859-1"), "utf-8"));
			DBRow row = new DBRow();
			row.add(RequirementItem.REQUIREMENT_NAME,URLDecoder.decode(
					 map.get(RequirementItem.REQUIREMENT_NAME).toString(), "UTF-8"));
			row.add(RequirementItem.DISPLAY_TYPE,URLDecoder.decode(
					map.get(RequirementItem.DISPLAY_TYPE).toString(), "UTF-8"));
			row.add(RequirementItem.DESCRIPTION,URLDecoder.decode(
					map.get(RequirementItem.DESCRIPTION).toString(), "UTF-8"));
			row.add("active",1);
			DBRow[] children=  new DBRow[0];
			Object items = map.get("children");
			if(null!=items&& items.toString().length()>3){
				List<Map> cmap =  (List<Map>) JSON.parse(items.toString());
				int size = cmap.size();
				if(size>0){
					children = new DBRow[size];
					for(int i=0;i<size;i++){
						DBRow r = new DBRow();
//						r.add(RequirementItemDetails.REQUIREMENT_ID, URLDecoder.decode(cmap.get(i).get(RequirementItemDetails.REQUIREMENT_ID).toString(), "UTF-8"));
						r.add(RequirementItemDetails.ITEM_NAME, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.ITEM_NAME).toString(), "UTF-8"));
						r.add(RequirementItemDetails.ITEM_VALUE, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.ITEM_VALUE).toString(), "UTF-8"));
						r.add(RequirementItemDetails.SORT, URLDecoder.decode(
								cmap.get(i).get(RequirementItemDetails.SORT).toString(), "UTF-8"));
						children[i]=r;
					}
				}
			}
			
			int result = requirementItemsMgrZYY.add(row, children);
			DBRow dbrow = null;
			switch (result) {
			case 0:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 0);
				dbrow.add(DES, "Add Faile");
				break;
			case 1:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 1);
				dbrow.add(DES, "Add success");
			break;case 2:
				dbrow = new DBRow();
				dbrow.add(SUCCESS, 2);
				dbrow.add(DES, " Configuration already exists");
				break;
			default:
				break;
			}
			return dbrow;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->create"+e.getMessage());
		}
		
	}
	
	/**
	 * 保存
	 * @param searchConditions
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/handleRItems/{ri_id}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getById(@PathVariable("ri_id") int ri_id  ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		DisplayTypeKey dsk= new DisplayTypeKey();
		try{
			DBRow row = requirementItemsMgrZYY.getById(ri_id, true);
			if(null!=row){
				row.add("display_type_txt", dsk.getStatusById(row.get("display_type", 0)));
			}
			return row;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->getById"+e);
		}
	}

	@RequestMapping(value = "/handleRItems/{ri_id}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public DBRow delRequirement(@PathVariable(value="ri_id") long ri_id){
		DBRow result = new DBRow();
		
		
		int execRs = -1;
		try {
			execRs = requirementItemsMgrZYY.delItem(ri_id);
		} catch (Exception e) {
			e.printStackTrace();
			execRs = 0;
		}
		
		result.add("ret", execRs==1 ? 0 : 1 );
		if(execRs == 0){
			result.add("error", "fail to delete ");
		} else if (execRs ==-1){
			result.add("error", "is used");
		}else {
			result.add("success", "true");
		}
		
		return result;
	}
	
	@RequestMapping(value = "/handleRItems", produces = "application/json; charset=UTF-8",method = RequestMethod.GET)
	public DBRow list(
			@RequestParam(value="pageSize" ,defaultValue="10") Integer rows,
			@RequestParam(value="pageNo" ,defaultValue="1") Integer page,
			@RequestParam(value="searchRname" ,required=false,defaultValue="") String searchRname,
			@RequestParam(value="searchDisplaytype" ,required=false) Integer searchDisplaytype,
			@RequestParam(value="searchIsactive" ,required=false,defaultValue="2") Integer searchIsactive,
			@RequestParam(value="cmd" ,required=false) String cmd,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		try{
			searchRname = new String(searchRname.getBytes("iso-8859-1"), "utf-8");
			PageCtrl pc = new PageCtrl();
			pc.setPageNo(page);
			pc.setPageSize(rows);
			DBRow sqlRow = new DBRow();
			if(!"".equals(cmd) && "advance".equals(cmd)){
				if(searchDisplaytype != null){
					sqlRow.put("searchDisplaytype", searchDisplaytype);
				}
				
				if(!"".equals(searchRname)){
					sqlRow.put("searchRname", searchRname);
				}
			}
			if(searchIsactive != null){
				sqlRow.put("searchIsactive",searchIsactive);
			}
			DBRow[]  requirements =requirementItemsMgrZYY.getAllRequirement(true, pc,sqlRow);
			DBRow row = new DBRow();
			DisplayTypeKey dsk= new DisplayTypeKey();
			if(requirements != null){
				for(DBRow x : requirements){
					String displayType = dsk.getStatusById(x.get("display_type", 0));
//					x.remove("display_type");
					x.add("display_type_txt", displayType);
					long ri_id = x.get("ri_id", 0l);
					x.add("is_used", requirementItemsMgrZYY.checkUsed(ri_id));
					
					//String required = Required.getRequired(x.get("requirement_value", 0));
					/*String requirement_value = x.getString("requirement_value");
					x.remove("requirement_value");*/
					
					//x.add("requirement_value", getDisplayValue(displayType, requirement_value));
				}
			}
			row.put("list", requirements);
			row.put("pagectrl", pc);
			return row;
		}catch(Exception e){
			throw new Exception("RequirementItemsContrller->list"+e);
		}
		
	}
	
	/**
	 * 启用/禁用--> RequirementItems
	 * 
	 * @param ri_id,style(启用/禁用类型)
	 * @throws Exception
	 */
	@RequestMapping(value = "/active", produces = "application/json;charset=UTF-8", method = RequestMethod.PUT)
	@ResponseBody
	public void modifyActive(HttpServletResponse response,
			@RequestParam(value = "ri_id") long ri_id,
			@RequestParam(value = "style") String style) throws Exception {
		DBRow dbRow = new DBRow();
		dbRow.add("ri_id", ri_id);
		if (style.equals("active")) {
			dbRow.add("active", 1);
		} else {
			dbRow.add("active", 0);
		}
		int psc = requirementItemsMgrZYY.updateActive(dbRow);
		JSONObject output = new JSONObject();
		if (psc > 0) {
			output.put("success", true);
		} else {
			output.put("success", false);
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().print(output);
	}
	
	@Autowired
	private FloorRequirementItemsMgrZYY requirementItemsMgrZYY;
	@Autowired
	private FloorRequirementItemsDetailsMgrZYY requirementItemsDetailsMgrZYY;
	
}
