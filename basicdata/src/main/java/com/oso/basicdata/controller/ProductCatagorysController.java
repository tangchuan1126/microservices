package com.oso.basicdata.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.beans.ProductCatagorys;
import com.oso.basicdata.controller.iface.ProductCatagorysControllerIFace;
import com.oso.basicdata.utils.ParseStrToArray;

/**
 * 
 * @ProjectName: [basicdata]
 * @Package: [com.oso.basicdata.controller.ProductCatagorysController.java]
 * @ClassName: [ProductCatagorysController]
 * @Description: [一句话描述该类的功能]
 * @Author: [赵永亚]
 * @CreateDate: [2015年3月26日 下午2:21:56]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年4月22日 10:55:23]
 * @UpdateRemark: [添加按Catagory父ID获取 Catagory的方法]
 * @Version: [v1.1]
 * 
 */
@RestController
public class ProductCatagorysController implements
		ProductCatagorysControllerIFace {
	@Autowired
	private FloorProprietaryMgrZyj proprietaryMgrZyj;
	@Autowired
	private FloorCatalogMgr fcm;
	
//	@Autowired
//	private Fl

	@Override
	public @ResponseBody DBRow[] findParentProductCatagorysByTitleId(
			@RequestParam(value = "title_id", required = false) Long title_id,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try {
			Long[] title_ids = null;
			if (null != title_id) {
				title_ids = new Long[1];
				title_ids[0] = title_id;
			} else {
				title_ids = new Long[0];
			}
			DBRow[] rows = proprietaryMgrZyj
					.findParentProductCatagorysByTitleId(0l, null, title_ids,
							null);
			List<DBRow> treeData = getjstreelist(rows, 0);
			DBRow[] treeRows= new DBRow[treeData.size()];
			treeData.toArray(treeRows);
			return treeRows;
		} catch (Exception e) {
			throw new Exception(
					"ProductLinesController->findProductLinesIdAndNameByTitleId"
							+ e.getMessage());
		}
	}

	
	private List<DBRow> getjstreelist(DBRow[] rows, long parid) {
		List<DBRow> dbrowlist = new ArrayList<DBRow>();
		if (null != rows && rows.length > 0) {
			for (int i = 0; i < rows.length; i++) {
				DBRow row = rows[i];
				long id = row.get("id",0l);
				String title = row.get("title","");
				long parentid = row.get("parentid",0l);
				if (parentid == parid) {
					DBRow newdbrow = new DBRow();
					newdbrow.add("id", id);
					newdbrow.add("text", title);
					newdbrow.add("children", getjstreelist(rows, id));
					dbrowlist.add(newdbrow);
				}
			}
		}
		return dbrowlist;
	}


	@Override
	public DBRow[] findParentProductCatagorysByPId(
			@RequestParam(value = "pid", required = false) String pid,
			@RequestParam(value = "title_id", required = false) String title_id,
			@RequestParam(value = "product_line_id", required = false) String product_line_id,
			HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		Long[] pc_line_id =ParseStrToArray.toLongArray(product_line_id);
		Long[] category_parent_id = ParseStrToArray.toLongArray(pid);
		Long[] title_ids = ParseStrToArray.toLongArray(title_id);
		return proprietaryMgrZyj.findProductCatagoryParentsIdAndNameByTitleId(0l,pc_line_id,category_parent_id,null,title_ids,0,0,null);
//		
		
//		if(pid==""){
//			return fcm.getProductCatalogByParentId(0l, null);
//		}else{
//			return fcm.getProductCatalogByParentId(Long.valueOf(pid), null);
//		}
	}
	
	@Override
	public @ResponseBody DBRow[] findByTitleAndCustomer(@RequestParam(value="title_id",required=false) Long title_id ,@RequestParam(value="customer_id",required=false) Long customer_id ,
			HttpSession session, HttpServletRequest request,HttpServletResponse response) throws Exception{
		try {
			Long[] title_ids = null;
			if (null != title_id) {
				title_ids = new Long[1];
				title_ids[0] = title_id;
			} else {
				title_ids = new Long[0];
			}
			DBRow[] rows = proprietaryMgrZyj.findProductCatagorysByTitleIdAndCustomer(null,customer_id, title_ids,null);
			List<DBRow> treeData = getjstreelist(rows, 0);
			DBRow[] treeRows= new DBRow[treeData.size()];
			treeData.toArray(treeRows);
			return treeRows;
		} catch (Exception e) {
			throw new Exception(
					"ProductLinesController->findProductLinesIdAndNameByTitleId"
							+ e.getMessage());
		}		
	}
	
	/**
	 * 根据产品线  获取相关 Catagorys
	 * @param pid
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public @ResponseBody DBRow[] findProductCatagorysByPId(
			@RequestParam(value = "pid", required = false) String pid,
			@RequestParam(value = "title_id", required = false) String title_id,
			@RequestParam(value = "customer_id", required = false) long customerId,
			@RequestParam(value = "product_line_id", required = false) String product_line_id,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception{
		Long[] pc_line_id =ParseStrToArray.toLongArray(product_line_id);
		Long[] category_parent_id = ParseStrToArray.toLongArray(pid);
		Long[] title_ids = ParseStrToArray.toLongArray(title_id);
		return proprietaryMgrZyj.findProductCatagoryByLine(0l,pc_line_id,category_parent_id,title_ids,customerId,0,0,null);		
	}
}
