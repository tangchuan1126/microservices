package com.oso.basicdata.controller.iface;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.db.DBRow;
import com.oso.basicdata.beans.ProductCatagorys;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.ProductCatagorysController.java] 
 * @ClassName:    [ProductCatagorysController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年3月26日 下午2:21:56]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年3月26日 下午2:21:56]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
@RestController
@RequestMapping(value="/productCatagorys")
public interface ProductCatagorysControllerIFace {
	@RequestMapping(value = "/getByTitleId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] findParentProductCatagorysByTitleId(@RequestParam(value="title_id",required=false) Long title_id ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception;

	@RequestMapping(value = "/getByTitleAndCustomer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] findByTitleAndCustomer(@RequestParam(value="title_id",required=false) Long title_id ,@RequestParam(value="customer_id",required=false) Long customer_id ,
			HttpSession session, HttpServletRequest request,HttpServletResponse response) throws Exception;	
	
	/**
	 * 根据父ID获取  其子 Catagorys
	 * @param pid
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getByPid", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] findParentProductCatagorysByPId(
			@RequestParam(value = "pid", required = false) String pid,
			@RequestParam(value = "title_id", required = false) String title_id,
			@RequestParam(value = "product_line_id", required = false) String product_line_id,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
	
	/**
	 * 根据产品线  获取相关 Catagorys
	 * @param pid
	 * @param session
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getByLine", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] findProductCatagorysByPId(
			@RequestParam(value = "pid", required = false) String pid,
			@RequestParam(value = "title_id", required = false) String title_id,
			@RequestParam(value = "customer_id", required = false) long customerId,
			@RequestParam(value = "product_line_id", required = false) String product_line_id,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception;	
	
}
