package com.oso.basicdata.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import seamoonotp.seamoonapi;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorCatalogMgr;
import com.cwc.app.floor.api.FloorProductMgr;
import com.cwc.app.floor.api.gql.FlooCustomSeachConditionsGql;
import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.floor.api.zj.FloorProductCodeMgrZJ;
import com.cwc.app.floor.api.zj.FloorProductLogsMgrZJ;
import com.cwc.app.floor.api.zwb.FloorProductLableTemplateMgrWFH;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.CodeTypeKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.LableTemplateProductKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.key.ProductEditReasonKey;
import com.cwc.app.key.ProductEditTypeKey;
import com.cwc.app.key.ProductFileTypeKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.utils.ParseStrToArray;

/**  
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.ProductController.java] 
 * @ClassName:    [ProductController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年3月27日 下午3:51:08]  
 * @UpdateUser:   [赵永亚]  product
 * @UpdateDate:   [2015年3月27日 下午3:51:08]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 */
@RestController
public class ProductController{
	
	@Autowired
	private FloorProprietaryMgrZyj proprietaryMgrZyj;
	
	@Autowired
	private FloorProductMgr floorProductMgr;
	
	@Autowired
	private FloorProductCodeMgrZJ floorProductCodeMgrZj;
	
	@Autowired
	private FloorAccountMgrSbb floorAccountMgr;
	
	@Autowired
	private FloorCatalogMgr floorCatalogMgr;
	
	@Autowired
	private FloorProductLogsMgrZJ floorProductLogsMgr;
	
	@Autowired
	private FloorProductLableTemplateMgrWFH productLableTemplate;
	
	@Autowired
	private FlooCustomSeachConditionsGql flooCustomSeachConditions;
	
	@RequestMapping(value = "/product/context", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> productContext() throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		result.put("lengthUom", new LengthUOMKey().getKey());
		result.put("weightUom", new WeightUOMKey().getKey());
		result.put("priceUom", new PriceUOMKey().getKey());
		result.put("fileType", new ProductFileTypeKey().getKey());
		
		//商品分类数据
		DBRow[] category = floorCatalogMgr.getProductCategoryTree(0);
		DBRow[] title = floorCatalogMgr.getTitle();
		DBRow[] customer = floorCatalogMgr.getCustomer();
		
		result.put("category", category);
		result.put("title", title);
		result.put("customer", customer);
		
		result.put("ret",BCSKey.SUCCESS);
		result.put("err",0);
		
		return result;
	}
	
	@Transactional
	@RequestMapping(value = "/product/add", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addProduct(@RequestBody MultiValueMap<String, String> param, HttpSession session) throws Exception {
		
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		JSONObject jsonData = new JSONObject(param.get("json").get(0));
		DBRow row = DBRowUtils.convertToMultipleDBRow(jsonData);
		
		row.put("adid", Long.valueOf(adminBean.get("adid")+""));
		row.put("sysDate", DateUtil.NowStr());
		
		//数据处理
		row = this.formatParameters(row);
		
		//验证
		Map<String,Object> result = this.validateProduct(row);
		
		if((boolean) result.get("success")){
			
			//添加商品
			long id = this.insertProduct(row);
			row.put("pc_id", id);
			
			//添加Code
			this.insertProductCode(row);
			
			//添加关系
			this.insertProductTitle(row);
			
			//文件上传
			this.insertProductFile(row);
			
			//维护SN
			this.insertProductSn(row);
			
			//记录日志
			this.insertProductLog(row);
			
			//添加基础模板
			this.addLableTempByPcId(id,0L);
			
			//维护索引
			
			//维护商品分类与title customer关系
			this.updateRelation(row);
			
			result.put("ret",BCSKey.SUCCESS);
			result.put("err",0);
			result.put("id",id);
			
		} else {
			
			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
		}
		
		return result;
	}
	
	@Transactional
	@RequestMapping(value = "/public/product/add/edi", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addProduct(@RequestBody Map<String, Object> param) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		//记录添加的ID
		List<Map<String, Long>> list = new ArrayList<Map<String, Long>>();
		
		//HttpSession session
		//Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		DBRow row = DBRowUtils.convertToMultipleDBRow(new JSONObject(param));
		
		//绑定账号Account, 使用令牌验证
		String account = "EDI001";
		DBRow admin = floorAccountMgr.getAccountByAccount(account);
		
		if(admin != null && admin.get("pwd","").equals(ParseStrToArray.getMD5(row.get("token").toString()))){
		//if(admin != null && this.validateToken(admin.getString("register_token"),row.get("token").toString())){
			
			//row.put("adid", Long.valueOf(adminBean.get("adid")+""));
			row.put("adid", admin.get("adid",0L));
			row.put("sysDate", DateUtil.NowStr());
			
			DBRow[] productArray = row.get("product") != null ? (DBRow[]) row.get("product") : null;
			
			for(DBRow oneResult : productArray){
				
				DBRow[] suitItem = oneResult.get("suit_items") != null ? (DBRow[]) oneResult.get("suit_items") : null;
				
				oneResult.put("union_flag", suitItem != null && suitItem.length > 0 ? 1 : 0 );
				oneResult.put("adid", row.get("adid",0L));
				oneResult.put("sysDate", row.get("sysDate",""));
				
				//验证商品是否存在
				long id = this.existsProductEdi(oneResult);
				
				//如果商品不存在
				if( id == 0 ){
					
					//添加套装或者商品
					id = this.insertProductEdi(oneResult);
					oneResult.put("pc_id", id);
					
					//记录ID
					Map<String, Long> map = new HashMap<String, Long>();
					map.put("id", id);
					
					list.add(map);
					
					//添加关系//处理
					this.updateProductTitleEdi(oneResult);
					
					//添加基础模板
					this.addLableTempByPcId(id,0L);
					
					//记录日志
					this.insertProductLog(oneResult);
					
					//维护索引
					
					//维护商品分类与title customer关系
					this.updateRelation(oneResult);
					
					if(suitItem != null){
						
						for(DBRow suitResult : suitItem){
							
							suitResult.put("union_flag", 0);
							suitResult.put("adid", row.get("adid",0L));
							suitResult.put("sysDate", row.get("sysDate",""));
							suitResult.put("id", id);
							
							//验证商品是否存在
							long suitId = this.existsProductEdi(suitResult);
							
							//如果商品不存在
							if( suitId == 0 ){
								
								//添加商品//添加Code//处理
								suitId = this.insertProductEdi(suitResult);
								suitResult.put("pc_id", suitId);
								
								//保存商品ID
								Map<String, Long> suitMap = new HashMap<String, Long>();
								suitMap.put("id", suitId);
								
								list.add(suitMap);
								
								//添加关系//处理
								this.updateProductTitleEdi(suitResult);
								
								//添加基础模板
								this.addLableTempByPcId(suitId,0L);
								
								//记录日志
								this.insertProductLog(suitResult);
								
								//维护索引
								
								//维护商品分类与title customer关系
								this.updateRelation(suitResult);
								
							}else {
								
								suitResult.put("pc_id", suitId);
							}
							
							//添加套装关系
							this.insertproductUnion(suitResult);
						}
					}
				}
			}
			
			result.put("ids", list);
			result.put("token", row.get("token").toString());
			
		}else{
			
			result.put("error", "Login failed");
		}
		
		return result;
	}
	
	/**
	 * 令牌验证
	 * @param 令牌ID,令牌口令
	 * @return boolean
	 * @since Sync10-ui 1.0
	 * @author subin
	 **/
	private boolean validateToken(String tokenId,String tokenSn) throws Exception {
		
		try {
			boolean result = false;
			
			if(tokenId != null && tokenSn != null){
				
				DBRow tokeRow = floorAccountMgr.getTokenById(tokenId);
				
				if(tokeRow != null){
					
					seamoonapi seamoon = new seamoonapi();
					
					String validateResult = seamoon.checkpassword(tokeRow.getString("sninfo"),tokenSn);
					
					if(validateResult.length()>3){
						
						DBRow updateTokeRow = new DBRow();
						updateTokeRow.add("sn", tokenId);
						updateTokeRow.add("sninfo", validateResult);
						//更新SNINFO
						floorAccountMgr.updateSninfo(updateTokeRow);
						
						result = true;
					}
				}
				
			}
			
			return result;
			
		} catch (Exception e) {
			
			throw new Exception("AccountMgrSbb.validateToken(String tokenId,String tokenSn) error:"+ e);
		}
	}
	
	//格式化请求参数
	public DBRow formatParameters(DBRow param) throws Exception{
		
		List<DBRow> list = new ArrayList<DBRow>();
		
		if(param.get("tcArray") != null){
			
			for(DBRow oneResult : (DBRow[]) param.get("tcArray")){
				
				long title = oneResult.get("title",0);
				String customer = oneResult.get("customer","");
				
				if(title != 0 && !customer.equals("")){
					
					String[] array = customer.split(",");
					
					for(String one : array){
						
						DBRow row = new DBRow();
						row.put("title", title);
						row.put("customer", Long.valueOf(one));
						
						list.add(row);
					}
				}
			}
		}
		
		DBRow[] result = new DBRow[list.size()];
		
		if(list.size() > 0){
			
			list.toArray(result);
		}
		
		param.put("tcArray", result);
		
		return param;
	}
	
	//添加套装关系
	public void insertproductUnion(DBRow row) throws Exception{
		
		DBRow union = floorProductMgr.getProductUnion(row.get("id",0),row.get("pc_id",0));
		
		if( union == null){
			
			DBRow param = new DBRow();
			param.put("set_pid", row.get("id",0));
			param.put("pid", row.get("pc_id",0));
			param.put("quantity", row.get("qty",0));
			
			floorProductMgr.addProductUnion(param);
		}
	}
	
	//添加product-title-customer关系
	public void updateProductTitleEdi(DBRow row) throws Exception {
		
		if( row.get("title_customer") != null ){
			
			for(DBRow oneResult : (DBRow[]) row.get("title_customer")){
				
				/**是否存在此title*/
				DBRow title = proprietaryMgrZyj.findProprietaryByTitleName(oneResult.get("title",""));
				
				/**是否存在此customer*/
				DBRow customer = proprietaryMgrZyj.getCustomerByName(oneResult.get("customer",""));
				
				if( title != null && customer != null){
					
					DBRow titleProduct = proprietaryMgrZyj.getDetailTilteProductByPCTitle(row.get("pc_id",0L), title.get("title_id",0l), customer.get("customer_key", 0l));
					
					if(titleProduct == null ){
						
						DBRow param = new DBRow();
						param.put("tp_pc_id", row.get("pc_id",0L));
						param.put("tp_title_id", title.get("title_id",0l));
						param.put("tp_customer_id", customer.get("customer_key", 0l));
						
						//添加product-title-customer关系
						floorProductMgr.addProductTitle(param);
					}
				}
			}
		}
	}
	
	//验证是否存在商品
	public long existsProductEdi(DBRow row) throws Exception {
		
		long id = 0;
		
		String name = row.get("customer_material","")+"/"+row.get("material_number","");
		
		DBRow product = floorProductMgr.getDetailProductByPname(name);
		
		if( product == null ){
			
			DBRow mainCode = floorProductMgr.getProductCode(name);
			
			if(mainCode != null){
				
				id = mainCode.get("pc_id",0);
			}
			
		} else {
			
			id = product.get("pc_id",0);
		}
		
		return id;
	}
	
	//添加商品
	public long insertProductEdi(DBRow row) throws Exception {
		
		String name = row.get("customer_material","")+"/"+row.get("material_number","");
		
		DBRow product = new DBRow();
		product.put("p_name", name);
		
		//处理分类名
		DBRow catalog = floorCatalogMgr.getProductCatalogByName(row.get("item_category",""));
		
		long category = catalog == null ? 0 : catalog.get("id",0);
		
		if(category != 0){
			
			//判断分类是否是最后一级
			DBRow[] subRow = floorCatalogMgr.getProductCatalogByParentId(category, null);
			
			if(subRow != null && subRow.length > 0){
				
				category = 0;
			}
		}
		
		product.put("catalog_id", category);
		
		product.put("nmfc_code", row.get("nmfc",""));
		product.put("freight_class", row.get("freight_class",0));
		
		product.put("length", row.get("unit_length",0F));
		product.put("width", row.get("unit_width",0F));
		product.put("heigth", row.get("unit_height",0F));
		product.put("weight", row.get("unit_weight",0F));
		
		long weight_uom = new WeightUOMKey().getWeightUOMKeyByValue(row.get("weight_unit_code",""));
		long length_uom = new LengthUOMKey().getLengthUOMKeyByValue(row.get("linear_unit_code",""));
		//处理Code 对应的ID [KG,LBS][CM,DM,MM,INCH]
		product.put("weight_uom", weight_uom == 0 ? WeightUOMKey.LBS : weight_uom);
		product.put("length_uom", length_uom == 0 ? LengthUOMKey.INCH : length_uom);
		
		//非页面参数
		product.put("unit_price", 0);
		product.add("device", ProductDeviceTypeKey.EDI);
		product.add("unit_name","ITEM");
		product.add("union_flag", row.get("union_flag",0));
		product.add("volume", row.get("unit_length",0F)*row.get("unit_width",0F)*row.get("unit_height",0F));
		product.add("alive", 1);
		product.add("creator", row.get("adid",0L));
		product.add("create_time", row.get("sysDate",""));
		
		//添加商品
		long id = floorProductMgr.addProduct(product);
		
		//商品Code表 [MainCode]
		DBRow productCode1 = new DBRow();
		productCode1.add("p_code", name);
		productCode1.add("create_adid", row.get("adid",0L));
		productCode1.add("create_time", row.get("sysDate",""));
		productCode1.add("pc_id", id);
		productCode1.add("code_type", CodeTypeKey.Main);
		
		//添加商品CODE
		floorProductCodeMgrZj.addProductCode(productCode1);
		
		return id;
	}
	
	//商品验证
	public Map<String,Object> validateProductEdi(DBRow row) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		boolean flag = true;
		String error = "";
		
		//必填验证
		if(flag && row.get("productName", "").equals("")){
			
			error = "please enter product name";
			flag = false;
		}
		
		if(flag && row.get("mainCode", "").equals("")){
			
			error = "please enter product code";
			flag = false;
		}
		
		if(flag && row.get("length",0F) == 0F){
			
			error = "please enter length";
			flag = false;
		}
		
		if(flag && row.get("width",0F) == 0F){
			
			error = "please enter width";
			flag = false;
		}

		if(flag && row.get("height",0F) == 0F){
			
			error = "please enter height";
			flag = false;
		}
		
		if(flag && row.get("weight",0F) == 0F){
			
			error = "please enter weight";
			flag = false;
		}
		
		if(flag && row.get("price","").equals("")){
			
			error = "please enter unit price";
			flag = false;
		}
		
		if(flag && row.get("snLength","").equals("")){
			
			error = "please enter Serial No";
			flag = false;
		}
		
		//验证商品名称[productName]
		DBRow product = floorProductMgr.getDetailProductByPname(row.get("productName", ""));
		
		if(flag && product != null ){
			
			error = "product exists";
			flag = false;
		}
		
		//验证商品条码[mainCode]
		product = floorProductMgr.getProductCode(row.get("mainCode", ""));
		
		if(flag && product != null ){
			
			error = "product exists";
			flag = false;
		}
		
		//验证分类是否存在
		if(flag && row.get("categoryId",0) != 0){
			
			DBRow category = floorCatalogMgr.getProductCategory(row.get("categoryId",0));
			
			if(category == null){
				
				error = "category not exists";
				flag = false;
				
			} else{
				
				//判断分类是否是最后一级
				DBRow[] subRow = floorCatalogMgr.getProductCatalogByParentId(row.get("categoryId",0), null);
				
				if(subRow != null && subRow.length > 0){
					
					error = "there are sub category";
					flag = false;
				}
			}
		}
		
		if(flag){
			//title是否存在//customer是否存在
			for(DBRow oneResult : (DBRow[]) row.get("tcArray")){
				
				if(oneResult.get("title",0) == 0 || oneResult.get("customer", 0) == 0){
					
					error = "please enter title and customer";
					flag = false;
					break;
					
				} else {
					
					DBRow title = floorCatalogMgr.getTitle(oneResult.get("title",0));
					DBRow customer = floorCatalogMgr.getCustomer(oneResult.get("customer", 0));
					
					if(title == null || customer == null){
						
						error = "title or customer not exists";
						flag = false;
						break;
					}
				}
			}
		}
		
		result.put("data", error);
		result.put("success",flag);
		
		return result;
	}
		
	//添加商品是，取基础模板第一个商品标签和clp标签，建立商品和标签模板的关系
	public void addLableTempByPcId(long pc_id,long title_id) throws Exception{
		
		try {
			
			DBRow[] lableTempRows =productLableTemplate.findAllLable("", "", 0, null);
			
			DBRow pcTemp = null;
			DBRow clpTemp = null;
			//取基础标签模板中第一个商品模板和第一个clp模板
			for(int n=0;n<lableTempRows.length;n++){
				lableTempRows[n].remove("lable_id");//去掉基础模板的id
				//如果是商品标签
				if(lableTempRows[n].get("lable_template_type", 0)==LableTemplateProductKey.FIRST){
					if(pcTemp==null){
						pcTemp=lableTempRows[n];
					}else if(clpTemp!=null){
						break;
					}else{
						continue;
					}
				}else if(lableTempRows[n].get("type", 0)==ContainerTypeKey.CLP){
					if(clpTemp==null){
						clpTemp=lableTempRows[n];
					}else if(pcTemp!=null){
						break;
					}else{
						continue;
					}
					
				}
			}
			
			//如果基础模板存在商品模板，给该新增商品添加第一个商品模板
			if(pcTemp!=null){
				pcTemp.add("title_id", title_id);
				long pcTempId=this.flooCustomSeachConditions.addProductDetailLable(pcTemp);
				DBRow row = new DBRow();
				row.add("pc_id", pc_id);
				row.add("lable_template_detail_id", pcTempId);
				this.flooCustomSeachConditions.addLableTemplateRelation(row);
			}
			//如果基础模板存在商品模板，给该新增商品添加第一个clp模板
			if(clpTemp!=null){
				clpTemp.add("title_id", title_id);
				long clpTempId=this.flooCustomSeachConditions.addProductDetailLable(clpTemp);
				DBRow row = new DBRow();
				row.add("pc_id", pc_id);
				row.add("lable_template_detail_id", clpTempId);
				this.flooCustomSeachConditions.addLableTemplateRelation(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	//维护商品分类与title customer关系
	public void updateRelation(DBRow row) throws Exception {
		
		floorCatalogMgr.updateCustomerTitleAndCategoryLineRelation(row.get("pc_id", 0L), 0);
	}
	
	//记录添加商品日志
	public void insertProductLog(DBRow row) throws Exception{
		
		//记录商品添加日志
		DBRow product = floorProductMgr.getDetailProductByPcid(row.get("pc_id",0L));
		
		product.put("account", row.get("adid",0L));
		product.put("edit_type", ProductEditTypeKey.ADD);
		product.put("edit_reason", ProductEditReasonKey.SELF);
		product.put("post_date", row.get("sysDate",""));
		
		floorProductLogsMgr.addProductLogs(product);
	}
	
	//维护SN
	public void insertProductSn(DBRow row) throws Exception {
			
		DBRow param = new DBRow();
		param.put("pc_id", row.get("pc_id",0L));
		param.put("sn_size", row.get("snLength",""));
		param.put("default_value", "1");
		param.put("creator", row.get("adid",0L));
		param.put("create_time", row.get("sysDate",""));
		
		floorProductMgr.addProductSn(param);
	}
	
	//添加商品文件
	public void insertProductFile(DBRow row) throws Exception {
		
		if(row.get("file") != null){
			for(DBRow oneResult : (DBRow[]) row.get("file")){
				
				DBRow param = new DBRow();
				param.put("pc_id", row.get("pc_id",0L));
				param.put("file_id", oneResult.get("id",0L));
				param.put("product_file_type", oneResult.get("type",0L));
				param.put("upload_adid", row.get("adid",0L));
				param.put("upload_time", row.get("sysDate",""));
				param.put("file_name", "");
				
				floorProductMgr.addProductFile(param);
			}
		}
	}
	
	//添加product-title-customer关系
	public void insertProductTitle(DBRow row) throws Exception {
		
		for(DBRow oneResult : (DBRow[]) row.get("tcArray")){
			
			if(oneResult.get("title",0) != 0 && oneResult.get("customer",0) != 0){
				
				DBRow param = new DBRow();
				param.put("tp_pc_id", row.get("pc_id",0L));
				param.put("tp_title_id", oneResult.get("title",0));
				param.put("tp_customer_id", oneResult.get("customer",0));
				
				//添加product-title-customer关系
				floorProductMgr.addProductTitle(param);
			}
		}
	}
		
	//添加商品CODE
	public void insertProductCode(DBRow row) throws Exception {
		
		//商品Code表 [MainCode]
		DBRow productCode1 = new DBRow();
		productCode1.add("p_code", row.get("mainCode",""));
		productCode1.add("create_adid", row.get("adid",0L));
		productCode1.add("create_time", row.get("sysDate",""));
		productCode1.add("pc_id", row.get("pc_id",0L));
		productCode1.add("code_type", CodeTypeKey.Main);
		
		//添加商品CODE
		floorProductCodeMgrZj.addProductCode(productCode1);
		
		//商品Code表 [UPC]
		DBRow productCode2 = new DBRow();
		productCode2.add("p_code",row.get("upcCode",""));
		productCode2.add("create_adid", row.get("adid",0L));
		productCode2.add("create_time", row.get("sysDate",""));
		productCode2.add("pc_id", row.get("pc_id",0L));
		productCode2.add("code_type", CodeTypeKey.UPC);
		
		//添加商品CODE
		floorProductCodeMgrZj.addProductCode(productCode2);
	}
	
	//添加商品
	public long insertProduct(DBRow row) throws Exception {
		
		DBRow product = new DBRow();
		//页面参数
		product.add("p_name", row.get("productName",""));
		product.add("length", row.get("length",0F));
		product.add("width", row.get("width",0F));
		product.add("heigth", row.get("height",0F));
		product.add("length_uom", row.get("lengthUom",0F));
		product.add("weight", row.get("weight",0F));
		product.add("weight_uom", row.get("weightUom",0F));
		product.add("unit_price",row.get("price",""));
		product.add("price_uom", row.get("priceUom",0F));
		product.add("catalog_id",row.get("categoryId",0));
		product.add("sn_size",row.get("snLength",0F));
		product.add("device", row.get("device",""));
		
		//非页面参数
		product.add("unit_name","ITEM");
		product.add("union_flag",0);
		product.add("volume", row.get("length",0F)*row.get("width",0F)*row.get("height",0F));
		product.add("alive", 1);
		product.add("creator", row.get("adid",0L));
		product.add("create_time", row.get("sysDate",""));
		
		//添加商品
		return floorProductMgr.addProduct(product);
	}
	
	//商品验证
	public Map<String,Object> validateProduct(DBRow row) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		boolean flag = true;
		String error = "";
		
		float LENGTH = 9999999F;
		
		//必填验证
		if(flag && row.get("productName", "").equals("")){
			
			error = "please enter product name";
			flag = false;
		}
		
		if(flag && row.get("mainCode", "").equals("")){
			
			error = "please enter main code";
			flag = false;
		}
		
		if(flag && row.get("length","").equals("")){
			
			error = "please enter length";
			flag = false;
		}
		
		if(flag && row.get("width",0F) == 0F){
			
			error = "please enter width";
			flag = false;
		}

		if(flag && row.get("height",0F) == 0F){
			
			error = "please enter height";
			flag = false;
		}
		
		if(flag && row.get("weight",0F) == 0F){
			
			error = "please enter weight";
			flag = false;
		}
		
		if(flag && row.get("price",0F) == 0F){
			
			error = "please enter unit price";
			flag = false;
		}
		
		if(flag && row.get("snLength",0F) == 0F){
			
			error = "please enter Serial No";
			flag = false;
		}
		
		//长度验证
		if(flag && row.get("productName", "").length() > 100){
			
			error = "Product name length should not exceed 100";
			flag = false;
		}
		
		if(flag && row.get("mainCode", "").length() > 30){
			
			error = "Main code length should not exceed 30";
			flag = false;
		}
		
		if(flag && row.get("upcCode","").length() > 30){
			
			error = "UPC code length should not exceed 30";
			flag = false;
		}
		
		if(flag && row.get("length",0F) > LENGTH ){
			
			error = " Length maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}

		if(flag && row.get("width",0F) > LENGTH ){
			
			error = " Width maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}

		if(flag && row.get("height",0F)  > LENGTH ){
			
			error = " Height maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}
		
		if(flag && row.get("length",0F)*row.get("width",0F)*row.get("height",0F) > LENGTH ){
			
			error = " Length*Width*Height maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}
		
		if(flag && row.get("weight",0F)  > LENGTH ){
			
			error = " Weight maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}
		
		if(flag && row.get("price",0F)  > LENGTH){
			
			error = " Price maximum 7 digits of integer and 3 digits of decimal, as : 1234567.123 ";
			flag = false;
		}
		
		if(flag && row.get("snLength",0F) > 9999999999F ){
			
			error = " Serial No maximum 10 digits of integer";
			flag = false;
		}
		
		//验证商品名称[productName]
		DBRow product = floorProductMgr.getDetailProductByPname(row.get("productName", ""));
		
		if(flag && product != null ){
			
			error = "product exists";
			flag = false;
		}
		
		//验证商品条码[mainCode]
		/*
		product = floorProductMgr.getProductCode(row.get("mainCode", ""));
		
		if(flag && product != null ){
			
			error = "product exists";
			flag = false;
		}*/
		
		//验证分类是否存在
		if(flag && row.get("categoryId",0) != 0){
			
			DBRow category = floorCatalogMgr.getProductCategory(row.get("categoryId",0));
			
			if(category == null){
				
				error = "category not exists";
				flag = false;
				
			} else{
				
				//判断分类是否是最后一级
				DBRow[] subRow = floorCatalogMgr.getProductCatalogByParentId(row.get("categoryId",0), null);
				
				if(subRow != null && subRow.length > 0){
					
					error = "there are sub category";
					flag = false;
				}
			}
		}
		
		if(flag && row.get("tcArray") != null){
			
			//title是否存在//customer是否存在
			for(DBRow oneResult : (DBRow[]) row.get("tcArray")){
					
				DBRow title = floorCatalogMgr.getTitle(oneResult.get("title",0));
				DBRow customer = floorCatalogMgr.getCustomer(oneResult.get("customer", 0));
				
				if(title == null || customer == null){
					
					error = "title or customer not exists";
					flag = false;
					break;
				}
			}
		}
		
		result.put("data", error);
		result.put("success",flag);
		
		return result;
	}

	@RequestMapping(value = "/product/getByTitleId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getProductByTitleId(
			@RequestParam(value="title_id",required=false) String title_id ,
			@RequestParam(value="product_line_id",required=false) String product_line_id ,
			@RequestParam(value="product_catagorys_id",required=false) String product_catagorys_id ,
			@RequestParam(value="keys",required=false) String keys ,
			@RequestParam("pageSize") int  rows,
			@RequestParam("pageNo") int page ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try{
			Long[] title_ids = ParseStrToArray.toLongArray(title_id);
			Long[] product_line_ids = ParseStrToArray.toLongArray(product_line_id);
			Long[] product_catagorys_ids = ParseStrToArray.toLongArray(product_catagorys_id);
			
			PageCtrl pc = new PageCtrl();
			if(rows==0){
				rows=10;
			}
			if(page ==0){
				page=1;
			}
			pc.setPageNo(page);
			pc.setPageSize(rows);
			DBRow[] dbrows = null;
			if(!StrUtil.isBlank(keys)){
				dbrows = proprietaryMgrZyj.findDetailProductLikeSearch(keys,title_ids,0l,0,0,0,pc,-1);
			}else{
			
				dbrows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(product_catagorys_ids,product_line_ids,0,0,0,title_ids,0l,0,0l, pc,-1);
			}

			//TODO 处理成 children 的形式
			DBRow row = new DBRow();
			row.put("list", dbrows);
			row.put("pagectrl", pc);
			return  row;
			//return null;
		}catch(Exception e){
			throw new Exception("ProductLinesController->findProductLinesIdAndNameByTitleId"+e.getMessage());
		}
		
	}
	
	@RequestMapping(value = "/product/getByTitleAndCustomer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow getProductByTitleId(
			@RequestParam(value="title_id",required=false) String title_id ,
			@RequestParam(value="customer_id",required=false) String customer_id_str ,
			@RequestParam(value="product_line_id",required=false) String product_line_id ,
			@RequestParam(value="product_catagorys_id",required=false) String product_catagorys_id ,
			@RequestParam(value="keys",required=false) String keys ,
			@RequestParam("pageSize") int  rows,
			@RequestParam("pageNo") int page ,HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try{
			Long[] title_ids = ParseStrToArray.toLongArray(title_id);
			Long[] product_line_ids = ParseStrToArray.toLongArray(product_line_id);
			Long[] product_catagorys_ids = ParseStrToArray.toLongArray(product_catagorys_id);
			
			PageCtrl pc = new PageCtrl();
			if(rows==0){
				rows=10;
			}
			if(page ==0){
				page=1;
			}
			long customerId = 0L;
			if(customer_id_str != null && customer_id_str.trim().length() > 0){
				customerId = Long.parseLong(customer_id_str);
			}
			pc.setPageNo(page);
			pc.setPageSize(rows);
			DBRow[] dbrows = null;
			if(!StrUtil.isBlank(keys)){
				
				dbrows = proprietaryMgrZyj.findDetailProductLikeSearch(keys,customerId,title_ids,pc);
			}else{
				dbrows = proprietaryMgrZyj.filterProductByPcLineCategoryTitleId(product_catagorys_ids,product_line_ids,customerId,title_ids, pc);
			}

			//TODO 处理成 children 的形式
			DBRow row = new DBRow();
			row.put("list", dbrows);
			row.put("pagectrl", pc);
			return  row;
			//return null;
		}catch(Exception e){
			throw new Exception("ProductLinesController->findProductLinesIdAndNameByTitleId"+e.getMessage());
		}
		
	}
}
