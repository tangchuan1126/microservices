package com.oso.basicdata.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [basicdata]
 * @Package:      [com.oso.basicdata.controller.ShipToController.java] 
 * @ClassName:    [ShipToController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年3月25日 下午2:21:27]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年3月25日 下午2:21:27]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
@RestController
@RequestMapping("/shipTo")
public class ShipToController {
	@Autowired
	private FloorShipToMgrZJ shipToMgrZJ;
	@RequestMapping(value = "/all", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] getAllShipTo(HttpSession session, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO 处理相关业务信息
		try{
			DBRow[] rows = shipToMgrZJ.getAllShipTo();
			return  rows;
		}catch(Exception e){
			throw new Exception("ShipToController->getShipTo"+e.getMessage());
		}
	}
}
