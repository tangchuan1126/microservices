package com.oso.basicdata.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.floor.FloorPackagingTypeMgr;

@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.READ_COMMITTED)
@Service
public class PackagingTypeMgr{
	
	@Autowired
	private FloorPackagingTypeMgr floorPackagingTypeMgr;
	
	
	/**
	 * 查询PackagingType
	 * @param pc
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getContainerTypesLikeName(PageCtrl pc, String searchKey) throws Exception
	{
		return floorPackagingTypeMgr.getContainerTypesLikeName(pc, searchKey);
	}
	
	/**
	 * 根据类型名查类型信息
	 * @param typeName
	 * @return
	 * @throws Exception
	 * @date: 2015年6月25日 下午3:56:11
	 */
	public DBRow getPackagingTypeByName(String typeName)throws Exception
	{
		return floorPackagingTypeMgr.getPackagingTypeByName(typeName);
	}
	
	
	
	
	/**
	 * 添加PackagingType
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addPackagingType(DBRow row)throws Exception
	{
		return floorPackagingTypeMgr.addPackagingType(row);
	}
	

	public FloorPackagingTypeMgr getFloorPackagingTypeMgr() {
		return floorPackagingTypeMgr;
	}

	public void setFloorPackagingTypeMgr(FloorPackagingTypeMgr floorPackagingTypeMgr) {
		this.floorPackagingTypeMgr = floorPackagingTypeMgr;
	}
	
	
	
	
	
	
}
