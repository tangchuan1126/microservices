package com.oso.basicdata.service.impl;

import javax.servlet.http.HttpServletRequest;

import org.omg.CORBA.SystemException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.floor.FloorLpMgr;

@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.READ_COMMITTED)
@Service
public class LpMgr{
	
	@Autowired
	private FloorLpMgr floorLpMgr;
	
	
	/**
	 * 添加LP
	 * @param lp
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月27日 上午10:50:16
	 */
	public DBRow addLp(DBRow lp) throws Exception {
		DBRow result = new DBRow();
		String hardwareId = lp.getString( "hardwareId");
		
		if(!StrUtil.isBlank(hardwareId))
		{
			if(floorLpMgr.isExitsHardwareId(hardwareId)){
				result.add("flag", "error");
				result.add("message", "hardwareIdExits");
				return result;
 			}
		}
		long id = floorLpMgr.addLp(lp).get("con_id", 0L);
		result.add("flag", "success");
		result.add("id", id);
		return result; 
 	}
	
	
	/**
	 * 更新lp
	 * @param lp_id
	 * @param updateRow
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月26日 下午3:06:53
	 */
	public void updateLp(long lp_id, DBRow updateRow) throws Exception 
	{
		floorLpMgr.updateLp(lp_id, updateRow);
	}

	
	
	/**
	 * 验证硬件编号是否存在
	 * @param hardwareId
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月27日 上午10:49:21
	 */
	public boolean isExitsHardwareId(String hardwareId ) throws Exception
	{
		return floorLpMgr.isExitsHardwareId(hardwareId);
	}
	
	
	
	/**
	 * 查询lp
	 * @param key
	 * @param type
	 * @param container_type
	 * @param pc
	 * @return
	 * @throws Exception
	 * @author:zyj
	 * @date: 2015年6月27日 下午12:04:00
	 */
	public DBRow[] findContainerByTypeContainerTypeName(String key,long type ,int container_type, PageCtrl pc) throws Exception 
	{
		return floorLpMgr.findContainerByTypeContainerTypeName(key, type, container_type, pc);
	}
	
	
	

	public FloorLpMgr getFloorLpMgr() {
		return floorLpMgr;
	}


	public void setFloorLpMgr(FloorLpMgr floorLpMgr) {
		this.floorLpMgr = floorLpMgr;
	}
	


	
}
