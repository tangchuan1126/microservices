package com.oso.basicdata.service;

import java.util.Stack;

import org.springframework.beans.factory.annotation.Autowired;

import com.cwc.app.floor.api.fa.basicdata.FloorTitleShipToMgrZYY;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.basicdata.beans.TitleShipTo;
import com.oso.basicdata.key.ProcessStepKey;
import com.oso.basicdata.service.iface.TitleShiptoRequirementConfigMgrIFace;

public class TitleShiptoRequirementConfigMgr implements TitleShiptoRequirementConfigMgrIFace {
	
	@Autowired
	private FloorTitleShipToMgrZYY titleShipToMgr;
	
	private static final long CONFIG_TYPE_PRODUCT = 1L;
	private static final long CONFIG_TYPE_CATALOG = 2L;
	private static final long CONFIG_TYPE_PRODUCTLINE = 3L;
	

	/* (non-Javadoc)
	 * @see com.oso.basicdata.service.iface.TitleShiptoRequirementConfigIFace#searchConfigByTitleShipto(long, long, long, long, long, long, long, java.lang.String, com.cwc.db.PageCtrl, boolean)
	 */
	@Override
	public DBRow[] searchConfigByTitleShipto(long config_type, long config_id, long title_id, long ship_to_id, 
			long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception {
		
		Stack<Long> priority = new Stack<Long>();
		priority.push(config_id);
		priority.push(title_id);
		priority.push(ship_to_id);
		priority.push(customer_key);
		
		DBRow[] searchResult = searchConfigByTitleShipto(config_type, tsc_id, ri_id, val, pc, accurate, priority, priority.size(), process_step);
		
		ProcessStepKey psk = new ProcessStepKey();
		
		if(searchResult!=null && searchResult.length>0){
			for(DBRow x : searchResult){
				String key = x.getString(TitleShipTo.PROCESS_STEP);
				x.add(TitleShipTo.PROCESS_STEP+"_name", psk.getProcessStepKeyValue(key));
			}
		}
		
		return searchResult;
	}
	
	/**
	 * 功能：通过查询条件搜索配置信息
	 * @param config_type
	 * @param tsc_id
	 * @param ri_id
	 * @param val
	 * @param pc
	 * @param accurate
	 * @param priority
	 * @param recursionTimes
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年4月29日
	 */
	private DBRow[] searchConfigByTitleShipto(long config_type, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, Stack<Long> priority, int recursionTimes, long process_step) throws Exception {
		DBRow[] searchResult = null;
		
		//先按条件精确查询
		searchResult = findTstByTitleIdAndShipTo(config_type, priority.get(0).intValue(), priority.get(1).intValue(), priority.get(2).intValue(), priority.get(3).intValue(), tsc_id, ri_id, val, pc, accurate, process_step);
		
		if(searchResult==null || searchResult.length<1){
			
			//若配置选择产品，精确查询无结果，按产品分类、产品线搜索
			if(config_type == CONFIG_TYPE_PRODUCT){
				searchResult = searchProductRelativedRequirementConfig(config_type, priority.get(0).intValue(), priority.get(1).intValue(), priority.get(2).intValue(), priority.get(3).intValue(), tsc_id, ri_id, val, pc, accurate, process_step);
			}
			
			//去掉优先级低的查询条件
			priority.set(recursionTimes-1, 0L);
			recursionTimes--;
			
			if((searchResult==null || searchResult.length<1) && recursionTimes>1){
				searchResult = searchConfigByTitleShipto(config_type, tsc_id, ri_id, val, pc, accurate, priority, recursionTimes, process_step);
			}
		}
		
		return searchResult;
	}
	
	/**
	 * 功能：查询产品相关配置
	 * @param config_type
	 * @param config_id
	 * @param title_id
	 * @param ship_to_id
	 * @param customer_key
	 * @param tsc_id
	 * @param ri_id
	 * @param val
	 * @param pc
	 * @param accurate
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年4月29日
	 */
	private DBRow[] searchProductRelativedRequirementConfig(long config_type, long config_id, long title_id, long ship_to_id, 
			long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception {
		DBRow[] searchResult = null;
			
			DBRow product = titleShipToMgr.getProductInfoByPcid(config_id);
			long catalog_id = (product==null) ? 0L : product.get("catalog_id", 0L);
			
			//根据商品分类查询
			if(catalog_id != 0){
				searchResult = searchProductRelativedRequirementConfigViaCatalog(CONFIG_TYPE_CATALOG, catalog_id, title_id, ship_to_id, customer_key, tsc_id, ri_id, val, pc, accurate, process_step);
			}
			
			DBRow productCatalog = titleShipToMgr.getCatalogInfoByCatalog(catalog_id);
			long product_line_id = (productCatalog==null) ? 0L : productCatalog.get("product_line_id", 0L);
			if((searchResult==null || searchResult.length<1) && product_line_id!=0){
				searchResult = searchProductRelativedRequirementConfigViaProductLine(CONFIG_TYPE_PRODUCTLINE, product_line_id, title_id, ship_to_id, customer_key, tsc_id, ri_id, val, pc, accurate, process_step);
			}
		
		return searchResult;
	}
	
	/**
	 * 功能：根据商品分类搜索配置信息
	 * @param config_type
	 * @param catalog_id
	 * @param title_id
	 * @param ship_to_id
	 * @param customer_key
	 * @param tsc_id
	 * @param ri_id
	 * @param val
	 * @param pc
	 * @param accurate
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年4月28日
	 */
	private DBRow[] searchProductRelativedRequirementConfigViaCatalog(long config_type, long catalog_id, long title_id, long ship_to_id, 
			long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception {
		DBRow[] categoryConfig = null;
		
		categoryConfig = findTstByTitleIdAndShipTo(config_type, catalog_id, title_id, ship_to_id, customer_key, tsc_id, ri_id, val, pc, accurate, process_step);
		
		DBRow productCatalog = titleShipToMgr.getCatalogInfoByCatalog(catalog_id);
		long parentCatalogId = (productCatalog==null) ? 0L : productCatalog.get("parentid", 0L);
		
		if((categoryConfig==null || categoryConfig.length<1) && parentCatalogId!=0){
			categoryConfig = findTstByTitleIdAndShipTo(config_type, parentCatalogId, title_id, ship_to_id, customer_key, tsc_id, ri_id, val, pc, accurate, process_step);
		}
		
		return categoryConfig;
	}
	
	/**
	 * 功能：根据产品线搜索配置信息
	 * @param config_type
	 * @param product_line_id
	 * @param title_id
	 * @param ship_to_id
	 * @param customer_key
	 * @param tsc_id
	 * @param ri_id
	 * @param val
	 * @param pc
	 * @param accurate
	 * @return DBRow[]
	 * @throws Exception
	 * @author lixf  2015年4月28日
	 */
	private DBRow[] searchProductRelativedRequirementConfigViaProductLine(long config_type, long product_line_id, long title_id, long ship_to_id, 
			long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception {
		DBRow[] productLineConfig = null;
		
		productLineConfig = this.findTstByTitleIdAndShipTo(config_type, product_line_id, title_id, ship_to_id, customer_key, tsc_id, ri_id, val, pc, accurate, process_step);
		
		return productLineConfig;
	}
	
	private DBRow[] findTstByTitleIdAndShipTo(long config_type, long config_id, long title_id, long ship_to_id, 
			long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception {
		return titleShipToMgr.findTstByTitleIdAndShipTo((int)customer_key, (int)title_id, (int)ship_to_id, (int)ri_id, val, pc, accurate, (int)config_type, (int)config_id, (int)tsc_id, (int)process_step);
	}
}
