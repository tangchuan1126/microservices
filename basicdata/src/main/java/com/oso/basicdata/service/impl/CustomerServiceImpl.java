package com.oso.basicdata.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.oso.basicdata.beans.Customer;
import com.oso.basicdata.service.CustomerService;
import com.oso.customer.models.Title;

/**
 * 提供了针对品牌商的数据访问方法
 * @author lujintao
 *
 */
@Transactional(readOnly=true)
@Service
public class CustomerServiceImpl implements CustomerService{
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 获得所有的品牌商
	 * @return 如果有，返回所有的品牌商;如果没有，返回空集合
	 */
	public List<Customer> getAll(){
		List<Customer> list = new ArrayList<Customer>();
		try {
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple("SELECT a.customer_key customer_id,a.customer_id customer_name  from customer_id a");
			if(rows != null && rows.length > 0){
				for(DBRow row : rows){
					list.add(this.convertToCustomer(row));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 获得与某个Title 相关的所有Customer
	 * @param titleId
	 * @return
	 */
	public List<Customer> getCustomersByTitle(int titleId){
		// select DISTINCT c.customer_key customer_id,c.customer_id customer_name from customer_id c inner join  title_product t on c.customer_key=t.tp_customer_id  where t.tp_title_id=12;
		List<Customer> list = new ArrayList<Customer>();
		if(titleId == 0){
			list = this.getAll();
		}else{
			try {
				DBRow param_row = new DBRow();
				param_row.add("tp_title_id", titleId);
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple("select DISTINCT c.customer_key customer_id,c.customer_id customer_name from customer_id c inner join  title_product t on c.customer_key=t.tp_customer_id  where t.tp_title_id=?",param_row);
				
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						list.add(this.convertToCustomer(rows[i]));
					}
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}			
		}
		return list;	
	}
	
	/**
	 * 将DBRow 对象转变为对应的 CustomerId对象
	 * @param row
	 * @return
	 */
	private Customer convertToCustomer(DBRow row){
		Assert.notNull(row);
		Customer customer = new Customer();
		customer.setId(row.get("customer_id", 1));
		customer.setName(row.get("customer_name", ""));
		return customer;
	}


	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	
}
