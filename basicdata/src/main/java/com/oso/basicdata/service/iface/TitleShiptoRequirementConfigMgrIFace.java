package com.oso.basicdata.service.iface;

import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

/**
 * 功能：title、shipto相关要求配置
 * 
 * @author lixf      2015年4月28日
 *
 * vvme.com All rights reserved.
 */
public interface TitleShiptoRequirementConfigMgrIFace {
	
	/**
	 * 功能：根据title、shipto搜索相应配置
	 * @param config_type
	 * @param config_id
	 * @param title_id
	 * @param ship_to_id
	 * @param customer_key
	 * @param tsc_id
	 * @param ri_id
	 * @param val
	 * @param accurate {true:精确查询, false:模糊查询}
	 * @param process_step 流程步骤
	 * @return DBRow[]
	 * @author lixf  2015年4月28日
	 */
	public DBRow[] searchConfigByTitleShipto(long config_type, long config_id, long title_id, long ship_to_id, long customer_key, long tsc_id, long ri_id, String val, PageCtrl pc, boolean accurate, long process_step) throws Exception;
	
}
