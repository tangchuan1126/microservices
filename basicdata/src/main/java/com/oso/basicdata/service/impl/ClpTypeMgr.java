package com.oso.basicdata.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zr.FloorBoxTypeZr;
import com.cwc.app.floor.api.zr.FloorClpTypeMgrZr;
import com.cwc.app.floor.api.zyj.FloorContainerMgrZyj;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.floor.api.zyj.model.Customer;
import com.cwc.app.floor.api.zyj.model.Title;
import com.cwc.app.floor.api.zyj.service.CustomerService;
import com.cwc.app.floor.api.zyj.service.ProductCustomerSerivce;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.oso.basicdata.floor.FloorProduct;

@Transactional(readOnly=true)
@Service
public class ClpTypeMgr {
	@Autowired
	private FloorProduct floorProduct;
	@Autowired
	private FloorBoxTypeZr boxTypeMgrZr;
	@Autowired
	private CustomerService customerService;
	@Autowired
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;
	@Autowired
	private ProductCustomerSerivce productCustomerSerivce;
	@Autowired
	private FloorShipToMgrZJ floorShipToMgrZJ;
	@Autowired
	private FloorContainerMgrZyj floorContainerMgrZyj;
	@Autowired
	private FloorClpTypeMgrZr floorClpTypeMgrZr;
	@Autowired
	private FloorProductStoreMgr productStoreMgr;
	
	// clp type list方法
	public DBRow[] getClpTypeList(DBRow param, HttpSession session)throws Exception{
		
		// 从dbrow中获取前台传入的字段
		long pc_id = param.get("pc_id", 0l);
		int basic_container_type = param.get("basic_container_type", 0);
		int title_value = param.get("title_value", 0);
		int ship_to_value = param.get("ship_to_value", 0);
		int customer_value = param.get("customer_id", 0);
		long active = param.get("active", 0l);
		
		// 根据session获取adminBean
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		
		// 获取登陆信息，判断是否为customer登陆
		long corporationId = Long.valueOf(adminBean.get("corporationId") + "");
		// 1:customer 2:carrier
		long corporationType = Long.valueOf(adminBean.get("corporationType") + "");
		long customer_id = 0;
		if(corporationType == 1){
			if(corporationId > 0 ){
				customer_id = corporationId;
			}else{
				customer_id = param.get("customer_value",0l);
			}
		}
		
		// 查询clp type的列表
		DBRow[] lpRows = boxTypeMgrZr.getBoxTypesInfo(pc_id,(int)basic_container_type,(int)title_value,(int)ship_to_value,(int)customer_id, active);
		for(DBRow clp : lpRows)
		{
			//is_has_sn
			clp.put("is_has_sn_en", new YesOrNotKey().getYesOrNotKeyName(clp.getString("is_has_sn","2")));
			//重量单位
			clp.put("weight_uom_en",new WeightUOMKey().getWeightUOMKey(clp.get("weight_uom",0)));
			
			//Configuration 树形数据 
			if(clp.get("inner_pc_or_lp",0l) > 0)
			{
				DBRow cascade = this.getCascadeClpType(clp.get("inner_pc_or_lp",0l));
				
				int j = 0;
				
				clp.put("jsTreeModel", cascade);
				
				String html = "";
				if(cascade.get("inner_pc_or_lp", 0l)==0)
				{
					html = "";
					html = "<a id='inner_clp_"+ cascade.get("lpt_id",0) +"' class='inner_clp underline' _lpt_id='" + cascade.get("lpt_id",0) + "' href='javascript:void(0);'>" + cascade.get("lp_name","") + "</a>";
					clp.put("inner_clp_html", html);
				}
				else
				{
					html = "";
					html = "<div name='inner_clp_html'>";
					do {
						j++;
						String lpName = cascade.get("lp_name","");
						
						html += "<ul><li>";
						
						html += "<a id='inner_clp_"+ cascade.get("lpt_id",0) +"' class='inner_clp underline' _lpt_id='" + cascade.get("lpt_id",0) + "' href='javascript:void(0);'>" + lpName + "</a>";
						
						cascade = cascade.get("subPlateType")==null?null:(DBRow)cascade.get("subPlateType");
					} 
					while (cascade!=null);
					for(;j>0;j--){
	  					html += "</li></ul>";
					}
					html += "</div>";
					clp.put("inner_clp_html", html);
				}
			}
//			title customer ship to
      		String titleIds = clp.getString("title_ids");
      		String shipToIds = clp.getString("ship_to_ids");
      		String customerIds = clp.getString("customer_ids");
      		if(titleIds != null && titleIds.length() > 0){
    			String[] titleIdArr = titleIds.split(",");
    			String[] shipToIdArr = shipToIds.split(",");
    			String[] customerIdArr = customerIds.split(",");
    			int index = 0;
    			if(title_value > 0 && ship_to_value > 0 && customer_value > 0){
    				for (int t=0; t<titleIdArr.length; t++) {
    					if(String.valueOf(title_value).equals(titleIdArr[t])){
    						if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
	    						if(String.valueOf(customer_value).equals(customerIdArr[t])){
	    							index = t;
	    						}	    							
    						}
    					}
    				}
    			} else if(title_value > 0 && ship_to_value<=0 && customer_value == 0){
    				for (int t=0; t<titleIdArr.length; t++) {
    					if(String.valueOf(title_value).equals(titleIdArr[t])){
    						index = t;
    					}
    				}
    			} else if(title_value <= 0 && ship_to_value > 0 && customer_value == 0){
    				for (int t=0; t<shipToIdArr.length; t++) {
    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
    						index = t;
    					}
    				}
    			} else if(title_value >= 0 && ship_to_value > 0){
    				for (int t=0; t<shipToIdArr.length; t++) {
    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
	    					if(String.valueOf(title_value).equals(titleIdArr[t])){
	    						index = t;
	    					}	    						
    					}
    				}
    			} else if(customer_value >= 0 && ship_to_value > 0){
    				for (int t=0; t<shipToIdArr.length; t++) {
    					if(String.valueOf(ship_to_value).equals(shipToIdArr[t])){
	    					if(String.valueOf(customer_value).equals(customerIdArr[t])){
	    						index = t;
	    					}	    						
    					}
    				}
    			} else if(title_value >= 0 && customer_value > 0){
    				for (int t=0; t<shipToIdArr.length; t++) {
    					if(String.valueOf(title_value).equals(titleIdArr[t])){
	    					if(String.valueOf(customer_value).equals(customerIdArr[t])){
	    						index = t;
	    					}	   						
    					}
    				}
    			}
    			
    			if(index > 3){
    				String oldTitle = titleIdArr[0];
    				String oldCustomer = customerIdArr[0];
    				String oldShipTo = shipToIdArr[0];
    				titleIdArr[0]=titleIdArr[index];
    				customerIdArr[0]=customerIdArr[index];
    				shipToIdArr[0]=shipToIdArr[index];
    				titleIdArr[index]=oldTitle;
    				customerIdArr[index]=oldCustomer;
    				shipToIdArr[index]=oldShipTo;
    			}
    			
                 DBRow[] tit_cus_shi = new DBRow[titleIdArr.length];
                 for (int k=0;k<titleIdArr.length;k++) {
	    				DBRow title = floorProprietaryMgrZyj.findProprietaryByTitleId(Long.valueOf(titleIdArr[k]));
	    				if(customer_id > 0 && Integer.parseInt(customerIdArr[k]) > 0 && customer_id != Integer.parseInt(customerIdArr[k])){
	    					continue;
	    				}
	    				Customer customer = null;
	    				
	    				if(customerIdArr[k] != null  && customerIdArr[k].trim().length() > 0 && Integer.parseInt(customerIdArr[k]) > 0){
	    					customer = customerService.getCustomer(Integer.parseInt(customerIdArr[k]));
	    				}
	    				DBRow shipTo = floorShipToMgrZJ.getShipToById(Long.valueOf(shipToIdArr[k]));
	    				
	    				String customerName = customer == null ? "*" : customer.getName();
	    				String titleName = title == null ? "*" : title.getString("title_name");
	    				String shipToName = shipTo == null ? "*" : shipTo.getString("ship_to_name");
	    				
	    				DBRow tcs = new DBRow();
	    				tcs.add("title_id", titleIdArr[k]);
	    				tcs.add("title_name", titleName);
	    				
	    				tcs.add("ship_to_id", shipToIdArr[k]);
	    				tcs.add("ship_to_name", shipToName);
	    				
	    				tcs.add("customer_key", customerIdArr[k]);
	    				tcs.add("customer_name", customerName);
	    				
	    				tcs.add("sub_lpt_id", clp.get("lpt_id", 0));
	    				
	    				tit_cus_shi[k] = tcs;
                 }
    			clp.add("title_customer_ship", tit_cus_shi);
      		}
		}
		
		return lpRows;
	}
// clp type search -> select -> packaging type 
public DBRow[] getAllPackagingType() throws Exception {
	
	return boxTypeMgrZr.getBoxContainerType(ContainerTypeKey.CLP);
}
//clp type search -> select -> customer
public List<Customer> getAllCustomer(DBRow param, HttpSession session) throws Exception {
	
	// 从dbrow中获取前台传入的字段
	int pc_id = param.get("pc_id", 0);
	int title_value = param.get("title_value", 0);
	
	// 根据session获取adminBean
	@SuppressWarnings("unchecked")
	Map<String, Object> adminBean = (Map<String, Object>) session.getAttribute(Config.adminSesion);
	
	// 获取登陆信息，判断是否为customer登陆
	long corporationId = Long.valueOf(adminBean.get("corporationId") + "");
	// 1:customer 2:carrier
	long corporationType = Long.valueOf(adminBean.get("corporationType") + "");
	long customer_id = 0;
	String userType = "";
	if(corporationType == 1){
		if(corporationId > 0 ){
			customer_id = corporationId;
		}else{
			customer_id = param.get("customer_value",0l);
		}
		userType = "Customer";
	}else if(Boolean.TRUE.equals(adminBean.get("administrator"))){
		userType = "Admin";
	}
	
	List<Customer> customers = null;
	if(userType.equals("Admin")){
		if(title_value > 0){
			customers = productCustomerSerivce.getCustomers(pc_id, title_value);
		}else{
			customers = productCustomerSerivce.getCustomers(pc_id);
		}
    }else{
		customers = new ArrayList<Customer>();
		Customer customer = customerService.getCustomer((int)customer_id);
		if(customer != null){
			customers.add( customer ); 
		}
    }
	return customers;
}
//clp type search -> select -> title
public List<Title> getAllTitle(DBRow param) throws Exception {
	
	return productCustomerSerivce.getTitles(param.get("pc_id", 0));
}
//clp type search -> select -> ship to
public DBRow[] getAllShipTo() throws Exception {
	
	return floorShipToMgrZJ.getAllShipTo();
}
//clp type search -> select -> active
public DBRow[] getActives() throws Exception {
	 DBRow active = new DBRow();
	 DBRow inactive = new DBRow();
	 active.add("active", 1L); 
	 active.add("active_name", "Active");
	 inactive.add("active", 2L); 
	 inactive.add("active_name", "Inactive");
	 DBRow[] actives = new DBRow[]{active,inactive};
	 return actives;
}
// 商品详细信息
public DBRow getProductInfo(long pc_id) throws Exception {
	
	DBRow product = floorProduct.getProduct(pc_id);
	
	//键值对转换
	product.put("length_uom_name", new LengthUOMKey().getLengthUOMKey(product.get("length_uom", LengthUOMKey.INCH)));
	product.put("weight_uom_name", new WeightUOMKey().getWeightUOMKey(product.get("weight_uom", WeightUOMKey.LBS)));
	
	return product;
}
//递归查询clp
public DBRow getCascadeClpType(long id) throws Exception {
	try{
		
		DBRow result = boxTypeMgrZr.findIlpByIlpId(id);
		
		if(result != null && result.get("inner_pc_or_lp", 0L) != 0){
			
			DBRow subResult = this.getCascadeClpType(result.get("inner_pc_or_lp", 0L));
			
			if(subResult != null){
				
				result.put("subPlateType", subResult);
			}
		}
		
		return result;
		
	}catch(Exception e){
		throw new Exception("ClpTypeController->getCascadeClpType " + e.getMessage());
	}
}

// 手持端 接口 -> packaging type
public DBRow[] getInnerClps(long pc_id) throws Exception {
	DBRow[] innerClps = floorContainerMgrZyj.getBoxSkuByPcId(pc_id,0);
	
	for(DBRow one : innerClps){
		//获取基础容器类型
		long basic_type_id = one.get("basic_type_id", 0l);
		// 查询基础容器的属性
		DBRow basic_container_type = floorContainerMgrZyj.findTlpTypeRelateInfoByTypeId(basic_type_id);
		one.put("basic_length", basic_container_type.get("length"));
		one.put("basic_width", basic_container_type.get("width"));
		one.put("basic_height", basic_container_type.get("height"));
		one.put("basic_weight", basic_container_type.get("weight"));
		one.put("basic_length_uom", basic_container_type.get("length_uom"));
		one.put("basic_weight_uom", basic_container_type.get("weight_uom"));
	}
	
	return innerClps;
}
// 手持端 接口 -> 添加clp type -> 验证
public Map<String,Object> vaildateAddClpType(DBRow param) throws Exception {
	Map<String,Object> result = new HashMap<String,Object>();
	long sku_lp_box_type = param.get("sku_lp_box_type",0l);	//盒子类型
	
	int sku_lp_box_length = param.get("sku_lp_box_length",0); //容器的摆放形式
	int sku_lp_box_width = param.get("sku_lp_box_width",0);
	int sku_lp_box_height = param.get("sku_lp_box_height",0);
	int length_uom = param.get("length_uom",0);
	int weight_uom = param.get("weight_uom",0);
	double box_length = param.get("box_length",0d);
	double box_width = param.get("box_width",0d);
	double box_height = param.get("box_height",0d);
	double box_weight = param.get("box_weight",0d);
	double basic_length = param.get("basic_length",0d);
	double basic_width = param.get("basic_width",0d);
	double basic_height = param.get("basic_height",0d);
	double basic_weight = param.get("basic_weight",0d);
	int basic_length_uom = param.get("basic_length_uom",0);
	int basic_weight_uom = param.get("basic_weight_uom",0);
	
	double pc_length = param.get("pc_length",0d);
	double pc_width = param.get("pc_width",0d);
	double pc_heigth = param.get("pc_heigth",0d);
	double pc_weight = param.get("pc_weight",0d);
	
	int pc_weight_uom = param.get("pc_weight_uom",0);
	int pc_length_uom = param.get("pc_length_uom",0);
	
	if(sku_lp_box_type==0)
	{
		double lengthTo =	this.lengthUnitConverter(pc_length,pc_length_uom,length_uom);
		double lengthFrom = this.lengthUnitConverter(box_length,length_uom,length_uom);
 		 
		double widthTo =	this.lengthUnitConverter(pc_width,pc_length_uom,length_uom);
		double widthFrom =  this.lengthUnitConverter(box_width,length_uom,length_uom);
 		 
		double heigthTo =	this.lengthUnitConverter(pc_heigth,pc_length_uom,length_uom);
		double heigthFrom =	this.lengthUnitConverter(box_height,length_uom,length_uom);
 		 
		double weightTo =	this.lengthUnitConverter(pc_weight,pc_weight_uom,weight_uom);
		double weightFrom =	this.lengthUnitConverter(box_weight,weight_uom,weight_uom);
		
		double lengthSum = lengthTo*sku_lp_box_length ;
 		 
		double widthSum =  widthTo*sku_lp_box_width ;
 		
		double heigthSum = heigthTo*sku_lp_box_height ;
   	 
		double weightSum = weightTo*sku_lp_box_length*sku_lp_box_width*sku_lp_box_height;
		
		if(lengthSum > lengthFrom){
			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_length", "Inner length too long.");
  		 }
  		 
  		 if(widthSum > widthFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_width", "Inner width too long.");
  		 }
  		 
  		 if(heigthSum > heigthFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_height", "Inner height too long.");
  		 }
  		 if(weightSum > weightFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_weight", "Inner weight too heavy.");
  		 }
		
		
	}
	else if(sku_lp_box_type!=0)
	{
		double lengthTo =	this.lengthUnitConverter(basic_length,basic_length_uom,length_uom);
		double lengthFrom =	this.lengthUnitConverter(box_length,length_uom,length_uom);
	  		
		double widthTo =	this.lengthUnitConverter(basic_width,basic_length_uom,length_uom);
		double widthFrom =  this.lengthUnitConverter(box_width,length_uom,length_uom);
	  	
		double heigthTo =	this.lengthUnitConverter(basic_height,basic_length_uom,length_uom);
		double heigthFrom =	this.lengthUnitConverter(box_height,length_uom,length_uom);
	  		
		double weightTo =	this.lengthUnitConverter(basic_weight,basic_weight_uom,weight_uom);
		double weightFrom =	this.lengthUnitConverter(box_weight,weight_uom,weight_uom);
		
		double lengthSum = lengthTo*sku_lp_box_length ;
 		 
		double widthSum =  widthTo*sku_lp_box_width ;
 		
		double heigthSum = heigthTo*sku_lp_box_height ;
   	 
		double weightSum = weightTo*sku_lp_box_length*sku_lp_box_width*sku_lp_box_height;
		
		if(lengthSum > lengthFrom){
			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_length", "Inner length too long.");
  		 }
  		 
  		 if(widthSum > widthFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_width", "Inner width too long.");
  		 }
  		 
  		 if(heigthSum > heigthFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_height", "Inner height too long.");
  		 }
  		 if(weightSum > weightFrom){
  			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("err_msg_weight", "Inner weight too heavy.");
  		 }
		
	}
	else
	{
		result.put("ret",BCSKey.SUCCESS);
		result.put("err",0);
	}
	
	return result;
}

public Map<String,Object> addClpType(DBRow param) throws Exception{
	Map<String,Object> result = new HashMap<String,Object>();
	try{
		DBRow row = new DBRow();
		long sku_lp_pc_id = param.get("sku_lp_pc_id",0l);
		long sku_lp_title_id = param.get("sku_lp_title_id",0l);
		long sku_lp_to_ps_id = param.get("sku_lp_to_ps_id",0l);
		long lp_type_id = param.get("lp_type_id",0l);
		long sku_lp_box_type = param.get("sku_lp_box_type",0l);	//盒子类型
		
		int sku_lp_total_box = 0;
		int sku_lp_total_piece = 0 ;
		
		int sku_lp_box_length = param.get("sku_lp_box_length",0); //容器的摆放形式
		int sku_lp_box_width = param.get("sku_lp_box_width",0);
		int sku_lp_box_height = param.get("sku_lp_box_height",0);
		int is_has_sn = param.get("is_has_sn",0);
		String length_uom = param.getString("length_uom");
		String weight_uom = param.getString("weight_uom");
		String lp_name = param.getString("lp_name");
		double box_length = param.get("box_length",0d);
		double box_width = param.get("box_width",0d);
		double box_height = param.get("box_height",0d);
		double box_weight = param.get("box_weight",0d);
		double weight = param.get("weight",0d);
		int level = 0;
		
		if(sku_lp_box_type  == 0l)
		{
			sku_lp_total_box = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ;//商品的数量
			sku_lp_total_piece = sku_lp_total_box;	//商品的数量
			level = 1;
		}
		else
		{
			
			DBRow boxSkuType = boxTypeMgrZr.getBoxTypeById(sku_lp_box_type);
			if(boxSkuType.get("active", 0) != 1 )
			{
				result.put("ret",BCSKey.FAIL);
				result.put("err",90);
				result.put("flag_msg", "The InnerType you select is inactive now.");
				return result;
			}
			else
			{
				int totalBoxSkuType = boxSkuType.get("inner_total_pc", 0);
				sku_lp_total_box  = sku_lp_box_length * sku_lp_box_width * sku_lp_box_height ; //盒子的数量
				sku_lp_total_piece = sku_lp_total_box * totalBoxSkuType ; //商品的数量                  
				
				if(boxSkuType.get("pc_id", 0l) != sku_lp_pc_id)
				{
					//盒子上面的商品 和 页面传递过来的pc_id 不等.W
					result.put("ret",BCSKey.FAIL);
					result.put("err",90);
					result.put("flag_msg", "Product and Box not match.");
					return result;
				}
				level = boxSkuType.get("level", 0)+1;					
			}
		}
		
		if(sku_lp_total_box == 0)
		{
			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("flag_msg", "The box quantity on CLP is wrong.");
			return result;
		}
		if(sku_lp_total_piece == 0 )
		{
			result.put("ret",BCSKey.FAIL);
			result.put("err",90);
			result.put("flag_msg", "The Product quantity on CLP is wrong.");
			return result;
		}
		
		int cn = floorClpTypeMgrZr.findClpTypeByBasicLWW(sku_lp_pc_id, lp_type_id,sku_lp_box_type, sku_lp_box_length, sku_lp_box_width, sku_lp_box_height);
		if(cn == 0){
			/*set DBRow*/
			DBRow sortRow = floorClpTypeMgrZr.findMaxSort(sku_lp_to_ps_id, sku_lp_pc_id,sku_lp_title_id,0);
			int sort=sortRow.get("sort", 0);
			row.add("sort",sort+1);
			
			row.add("pc_id", sku_lp_pc_id);
		//	row.add("lp_title_id", sku_lp_title_id);
			row.add("basic_type_id", lp_type_id);
			row.add("inner_pc_or_lp", sku_lp_box_type);		
			row.add("stack_length_qty", sku_lp_box_length);
			row.add("stack_width_qty", sku_lp_box_width);
			row.add("stack_height_qty", sku_lp_box_height);
			row.add("is_has_sn", is_has_sn);
			row.add("inner_total_lp", sku_lp_total_box);
			row.add("inner_total_pc", sku_lp_total_piece);
			row.add("length_uom", length_uom);
			row.add("weight_uom", weight_uom);
			row.add("container_type", 1);
			row.add("lp_name",lp_name);
			row.add("active",1);
			row.add("ibt_length",box_length);
			row.add("ibt_width",box_width);
			row.add("ibt_height",box_height);
			row.add("ibt_weight",box_weight);
			row.add("total_weight",weight);
			row.add("level", level);
			long id = addClpTypeDBRow(row);
		
			//图数据库
		//	DBRow con=this.floorClpTypeMgrZr.findContainerProperty(lp_type_id);
			Map<String,Object> containerNode = new HashMap<String, Object>();
			containerNode.put("con_id",id);
			containerNode.put("type_id",id);			
			containerNode.put("pc_id",sku_lp_pc_id);
			containerNode.put("sort",sort+1);
			containerNode.put("basic_type_id",lp_type_id);
			containerNode.put("inner_pc_or_lp",sku_lp_box_type);
			containerNode.put("stack_length_qty", sku_lp_box_length);
			containerNode.put("stack_width_qty", sku_lp_box_width);
			containerNode.put("stack_height_qty", sku_lp_box_height);			
			containerNode.put("is_has_sn",is_has_sn);
			containerNode.put("inner_total_lp",sku_lp_total_box);
			containerNode.put("inner_total_pc",sku_lp_total_piece);
			containerNode.put("length_uom",length_uom);
			containerNode.put("weight_uom",weight_uom);		
			containerNode.put("container_type",1);
			containerNode.put("lp_name",lp_name);
			containerNode.put("active",1);
			containerNode.put("ibt_length",box_length);
			containerNode.put("ibt_width",box_width);
			containerNode.put("ibt_height",box_height);
			containerNode.put("ibt_weight",box_weight);
			containerNode.put("total_weight",weight); 
			containerNode.put("level", level);
		
		//	con_id, container, container_type, is_full, is_has_sn, type_id
		
			if(sku_lp_box_type  == 0l){
				DBRow[] containerProduct = floorClpTypeMgrZr.findProductNode(sku_lp_pc_id);
				 DBRow[] hasTitle = floorProprietaryMgrZyj.getProudctHasTitleList(sku_lp_pc_id);
				 
				for(int i = 0; i < containerProduct.length; i++){
					
					Map<String,Object> productNode = new HashMap<String, Object>();
					productNode.put("pc_id",sku_lp_pc_id);
					
					String title_id = "";
					
					for(int j = 0; j<hasTitle.length; j++){
						
						title_id += ","+hasTitle[j].get("title_id",0l);
					}
					
					productNode.put("title_id",title_id.equals("")?title_id:title_id.substring(1));
				//	productNode.put("product_line",containerProductLine.get("product_line_id",0l));
					productNode.put("p_name",containerProduct[i].getString("p_name"));
				//	productNode.put("catalogs",containerProduct[i].get("catalog_id",0l));
					
					productNode.put("unit_name",containerProduct[i].getString("unit_name"));
					productNode.put("unit_price",containerProduct[i].getString("unit_price"));
					productNode.put("catalog_id",containerProduct[i].getString("catalog_id"));
					productNode.put("orignal_pc_id", containerProduct[i].get("orignal_pc_id",0l));
					productNode.put("width",containerProduct[i].getString("width"));
					productNode.put("weight",containerProduct[i].getString("weight"));
					productNode.put("heigth", containerProduct[i].getString("heigth"));
					productNode.put("length", containerProduct[i].getString("length"));
					productNode.put("length_nom",containerProduct[i].get("length_uom",0l));
					productNode.put("weight_nom",containerProduct[i].get("weight_uom",0l));
					productNode.put("price_uom",containerProduct[i].getString("price_uom"));
					//productNode.put("p_code", containerProduct[i].getString("p_code"));
					productNode.put("union_flag",containerProduct[i].get("union_flag",0));
					productNode.put("alive",containerProduct[i].get("alive",0));
					
					Map<String,Object> relProps = new HashMap<String, Object>();
					relProps.put("quantity",sku_lp_total_piece);
				//	relProps.put("locked_quantity",sku_lp_total_piece);
					relProps.put("stack_length_qty",sku_lp_box_length);
					relProps.put("stack_width_qty",sku_lp_box_width);
					relProps.put("stack_height_qty",sku_lp_box_height);
					relProps.put("total_weight",weight);
					relProps.put("weight_nom",weight_uom);
					
					
					Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
					searchContainerNodes.put("con_id", id);
					Map<String,Object> searchProductNodes = new HashMap<String, Object>();
					searchProductNodes.put("pc_id", sku_lp_pc_id);
					DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
					DBRow[]  existProductNode =	productStoreMgr.searchNodes(1, NodeType.Product,searchProductNodes);
					
					if(existContainerNode.length>0){
						productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加商品节点
					}else{
						productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
					}
					if(existProductNode.length>0){
						productStoreMgr.updateNode(1,NodeType.Product,searchProductNodes,productNode);//添加商品节点
					}else{
						productStoreMgr.addNode(1,NodeType.Product,productNode);//添加商品节点
					}
					productStoreMgr.addRelation(1,NodeType.Container,NodeType.Product,containerNode,productNode, relProps);//添加容易与商品的关系
					
				}
			}else{
			/*	Map<String,Object> fromContainerNode = new HashMap<String, Object>();
				fromContainerNode.put("con_id",lp_type_id);
				
				Map<String,Object> toContainerNode = new HashMap<String, Object>();
				toContainerNode.put("con_id",sku_lp_total_box);
				*/
				
				DBRow tocon=floorClpTypeMgrZr.findContainerProperty(sku_lp_box_type);
				
				Map<String,Object> toContainerNode = new HashMap<String, Object>();
				toContainerNode.put("con_id",sku_lp_box_type);
				toContainerNode.put("type_id",sku_lp_box_type);	
				
			    toContainerNode.put("pc_id",tocon.get("pc_id",01));			  
		        //toContainerNode.put("sort",tocon.get("sort",01));
			    toContainerNode.put("basic_type_id",tocon.get("basic_type_id",01));
				toContainerNode.put("inner_pc_or_lp",tocon.get("inner_pc_or_lp",01));
				toContainerNode.put("is_has_sn",tocon.get("is_has_sn",01));
				toContainerNode.put("inner_total_lp",tocon.get("inner_total_lp",01));
				toContainerNode.put("inner_total_pc",tocon.get("inner_total_pc",01));
				//toContainerNode.put("length_uom",tocon.get("length_uom",01));
				//toContainerNode.put("weight_uom",tocon.get("weight_uom",01));		
				toContainerNode.put("container_type",tocon.get("container_type",01));
			    toContainerNode.put("lp_name",tocon.getString("lp_name"));
				toContainerNode.put("active",tocon.get("active",01));
				toContainerNode.put("ibt_length",tocon.get("ibt_length",01));
				toContainerNode.put("ibt_width",tocon.get("ibt_width",01));
				toContainerNode.put("ibt_height",tocon.get("ibt_height",01));
				//	toContainerNode.put("ibt_weight",tocon.get("ibt_weight",01));
				//toContainerNode.put("total_weight",tocon.get("total_weight",01));
				toContainerNode.put("stack_length_qty", tocon.get("stack_length_qty",01));
				toContainerNode.put("stack_width_qty", tocon.get("stack_width_qty",01));
				toContainerNode.put("stack_height_qty", tocon.get("stack_height_qty",01)); 
				
				
				//
				Map<String,Object> ContainerNodeConcern = new HashMap<String, Object>();
				ContainerNodeConcern.put("quantity", sku_lp_total_piece);
				ContainerNodeConcern.put("stack_length_qty", sku_lp_box_length);
				ContainerNodeConcern.put("stack_width_qty", sku_lp_box_width);
				ContainerNodeConcern.put("stack_height_qty", sku_lp_box_height);
				ContainerNodeConcern.put("total_weight", weight);
				
				Map<String,Object> searchContainerNodes = new HashMap<String, Object>();
				searchContainerNodes.put("con_id", id);
				
				Map<String,Object> searchToContainerNodes = new HashMap<String, Object>();
				searchToContainerNodes.put("con_id", sku_lp_box_type);
				DBRow[]  existContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
				DBRow[]  existToContainerNode =	productStoreMgr.searchNodes(1, NodeType.Container,searchContainerNodes);
		
				if(existContainerNode.length>0){
					productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,containerNode);//添加商品节点
				}else{
					productStoreMgr.addNode(1,NodeType.Container,containerNode);//添加容器节点
				}
				
				if(existToContainerNode.length>0){
					productStoreMgr.updateNode(1,NodeType.Container,searchContainerNodes,toContainerNode);//添加商品节点
				}else{
					productStoreMgr.addNode(1,NodeType.Container,toContainerNode);//添加容器节点
				}
				
				
				productStoreMgr.addRelation(1,NodeType.Container,NodeType.Container,containerNode,toContainerNode,ContainerNodeConcern);
			}
			
			result.put("ret",BCSKey.SUCCESS);
			result.put("err",0);
		}else{
			result.put("ret",BCSKey.SUCCESS);
			result.put("err",0);
			result.put("message", "This clp type is exists.");
		}
		
		return result;
	
	}catch (Exception e) {
		throw new Exception(e.getMessage()+"addclp");
	}
}

// 添加clp
private long addClpTypeDBRow(DBRow row) throws Exception {
	try{
		return floorClpTypeMgrZr.addClpType(row);
	}catch (Exception e) {
		throw new Exception(e.getMessage()+"addClpTypeDBRow");
	}
}

// 宽度转换
public double weightUnitConverter(double weight, int fromUom, int toUom)
{	
	double result = 0;
	
	switch(fromUom){
		
		case WeightUOMKey.LBS : 
			result = this.widthLbsConverterOthers(weight, toUom);
			break;
		case WeightUOMKey.KG :
			result = this.widthKgConverterOthers(weight, toUom);
			break;
	}
	
	//保留三位
	BigDecimal f = new BigDecimal(result);
	return f.setScale(3, BigDecimal.ROUND_HALF_UP).doubleValue();
}

//宽度转换
public double lengthUnitConverter(double length, int fromUom, int toUom)
{	
	double result = 0;
	
	switch (fromUom){
	 
 		case LengthUOMKey.DM:
 			result = this.lengthDmConverterOthers(length, toUom);
 			break;
 		case LengthUOMKey.CM:
	 		result = this.lengthCmConverterOthers(length, toUom);
	 		break;
 		case LengthUOMKey.MM:
	 		result = this.lengthMmConverterOthers(length, toUom);
	 		break;
 		case LengthUOMKey.INCH:
	 		result = this.lengthInchConverterOthers(length, toUom);
	 		break;
	}
	
	//保留三位
	BigDecimal f = new BigDecimal(result);
	return f.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
}





//LBS宽度转换
public double widthLbsConverterOthers(double weight, int toUom)
{
	double result = 0;
	 
	 switch (toUom) {
	 
		case WeightUOMKey.LBS :
			result = weight;
			break;
		case WeightUOMKey.KG :
			result = 0.4536 * weight;
			break;
	}
	 
	 return result;
}
//KG宽度转换
public double widthKgConverterOthers(double weight, int toUom)
{
	double result = 0;
	 
	switch (toUom) {
	 
		case WeightUOMKey.KG :
			result = weight;
			break;
		case WeightUOMKey.LBS :
			result = 2.2046 * weight;
			break;
	}
	 
	 return result;
}

//DM长度转换
public double lengthDmConverterOthers(double length, int toUom)
{
	double result = 0;
	 
	 switch (toUom) {
	 	case LengthUOMKey.DM:
			result = length;
			break;
		case LengthUOMKey.CM:
			result = length * 10;
			break;
		case LengthUOMKey.MM:
			result = length * 100;
			break;
		case LengthUOMKey.INCH:
			result = length * 10 / 2.54;
			break;
		default:
			break;
	}
	 
	 return result;
}

//CM长度转换
public double lengthCmConverterOthers(double length, int toUom)
{
	double result = 0;
	 
	switch (toUom) {
	 	case LengthUOMKey.CM:
			result = length;
			break;
		case LengthUOMKey.DM:
			result = length / 10;
			break;
		case LengthUOMKey.MM:
			result = length * 10;
			break;
		case LengthUOMKey.INCH:
			result = length / 2.54;
			break;
	}
	 
	 return result;
}

//MM长度转换
public double lengthMmConverterOthers(double length, int toUom)
{
	double result = 0;
	 
	switch (toUom) {
	 	case LengthUOMKey.MM:
			result = length;
			break;
		case LengthUOMKey.DM:
			result = length / 100;
			break;
		case LengthUOMKey.CM:
			result = length / 10;
			break;
		case LengthUOMKey.INCH:
			result = length / 10 / 2.54;
			break;
	}
	 
	 return result;
}

//inch长度转换
public double lengthInchConverterOthers(double length, int toUom)
{
	double result = 0;
	 
	switch (toUom) {
	 	case LengthUOMKey.INCH:
			result = length;
			break;
		case LengthUOMKey.DM:
			result = length / 10 * 2.54;
			break;
		case LengthUOMKey.CM:
			result = length * 2.54;
			break;
		case LengthUOMKey.MM:
			result = length * 10 * 2.54;
			break;
	}
	 
	 return result;
}











}
