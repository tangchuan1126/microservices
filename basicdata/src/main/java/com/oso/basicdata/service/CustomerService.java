package com.oso.basicdata.service;

import java.util.List;
import com.oso.basicdata.beans.Customer;


/**
 * 定义针对品牌商的数据访问接口
 * @author lujintao
 *
 */
public interface CustomerService {

	/**
	 * 获得所有的品牌商
	 * @return 如果有，返回所有的品牌商;如果没有，返回空集合
	 */
	public List<Customer> getAll();
	
	/**
	 * 获得与某个Title 相关的所有Customer
	 * @param titleId
	 * @return
	 */
	public List<Customer> getCustomersByTitle(int titleId);
}
