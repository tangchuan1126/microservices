package com.oso.product.controller;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorSetupLogMgr;
import com.cwc.app.floor.api.fa.product.FloorProductCodeMgrLiy;
import com.cwc.app.key.ProductDeviceTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.oso.product.iface.ProductCodeControllerIFace;
@RestController
@RequestMapping(value="/productCodes")
public class ProductCodeController implements ProductCodeControllerIFace {
	
	@Autowired
	private FloorProductCodeMgrLiy productCodeMgr;
	
	@Autowired
	private FloorSetupLogMgr floorSetupLogMgr;
	
	@RequestMapping(value = "/productCodeList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] productCodeList(@RequestParam(value="pc_id")long pc_id, HttpServletResponse response)
			throws Exception {
		DBRow[] rows = productCodeMgr.getAllProductCode(pc_id);
		for(DBRow row : rows){
			int code_type = row.get("code_type",0);
			if(code_type == 2){
				row.add("sort", 1);
			}else if(code_type == 5){
				row.add("sort", 2);
			}else if(code_type == 4){
				row.add("sort", 3);
			}else{
				row.add("sort", 4);
			}
		}
		
		Arrays.sort(rows , new Comparator<DBRow>(){

			@Override
			public int compare(DBRow o1, DBRow o2) {
				int sort1 = o1.get("sort", 0);
				int sort2 = o2.get("sort", 0);
				if(sort1 > sort2){
					return 1;
				}
				if(sort1 < sort2){
					return -1;
				}
				return 0;
			}
			
		});

		
		return rows;
	}

	@RequestMapping(value = "/insertProductCode", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public DBRow insertProductCode(@RequestBody Map<String, String> map,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		//只获取AdminLoginBean
		HttpSession session = request.getSession();
		Map<String, Object> loginUser = (Map<String, Object>) session.getAttribute("adminSesion");
		
		DBRow row = new DBRow();
		row.add("pcode_id",map.get("pcode_id"));
		row.add("p_code",map.get("p_code"));
		row.add("pc_id",map.get("pc_id"));
		row.add("code_type",map.get("code_type"));
		row.add("retailer_id",map.get("retailer_id")==""?"0":map.get("retailer_id"));
		row.add("create_adid",loginUser.get("adid"));
		String date = DateUtil.DatetimetoStr(new Date());
		row.add("create_time",date);
		
		long insertId = productCodeMgr.insertProductCode(row);
		
		/*******************************************************************************************************/
		/** 
		 * 创建的时候, 在创建完成后执行此方法
		 * 更新的时候, 在更新前执行此方法
		 */
		DBRow logRow = new DBRow();
		//create/update/delete
		logRow.put("event", "create");
		//更新的表名
		logRow.put("table", "product_code");
		//主键字段名
		logRow.put("column", "pcode_id");
		//主键ID
		logRow.put("pri_key", insertId);
		//依赖表
		logRow.put("ref_table", "product");
		//依赖表主键字段名
		logRow.put("ref_column", "pc_id");
		//依赖表主键ID
		logRow.put("ref_pri_key", map.get("pc_id"));
		//账号ID
		logRow.put("create_by", loginUser.get("adid"));
		//设备类型//请求的来源
		logRow.put("device", ProductDeviceTypeKey.Web);
		//描述
		logRow.put("describe", "create product_code " + insertId);
		
		//具体更新的字段内容
		//将DBRow转换为数组
		DBRow[] content = DBRowUtils.dbRowConvertToArray(row);
		logRow.put("content", content);
		
		floorSetupLogMgr.addSetupLog(logRow);
		
		/*******************************************************************************************************/
		
		boolean flag = false;
		if (insertId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
		
	}
	/**
	 * 
	 * @param flag
	 * @return
	 */
	private DBRow getResultInfo(boolean flag){
		DBRow result = new DBRow();
		result.add("success",flag);
		return result;
	}
	
	@RequestMapping(value = "/deleteProductCode", produces = "application/json;charset=UTF-8", method = RequestMethod.DELETE)
	public @ResponseBody DBRow deleteProductCode(@RequestParam(value="pcode_id")long pcode_id, HttpServletResponse response)
			throws Exception {
		long delId = productCodeMgr.deleteProductCode(pcode_id);
		boolean flag = false;
		if (delId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	@RequestMapping(value = "/updateProductCode", produces = "application/json;charset=UTF-8", method = RequestMethod.PUT)
	public @ResponseBody DBRow updateProductCode(@RequestBody Map<String, String> map,HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		DBRow parm = new DBRow();
		parm.add("p_code",map.get("p_code"));
		parm.add("retailer_id",map.get("retailer_id")==""?0:map.get("retailer_id"));
		long pcode_id = Long.parseLong(map.get("pcode_id"));
		parm.add("pc_id",map.get("pc_id"));
		parm.add("code_type",map.get("code_type"));
		HttpSession session = request.getSession();
		Map<String, Object> loginUser = (Map<String, Object>) session.getAttribute("adminSesion");
		parm.add("create_adid",loginUser.get("adid"));
		String date = DateUtil.DatetimetoStr(new Date());
		parm.add("create_time",date);
		
		long upId = productCodeMgr.updateProductCode(pcode_id, parm);
		
		/*******************************************************************************************************/
		/** 
		 * 创建的时候, 在创建完成后执行此方法
		 * 更新的时候, 在更新前执行此方法
		 */
		DBRow logRow = new DBRow();
		//create/update/delete
		logRow.put("event", "update");
		//更新的表名
		logRow.put("table", "product_code");
		//主键字段名
		logRow.put("column", "pcode_id");
		//主键ID
		logRow.put("pri_key", pcode_id);
		//依赖表
		logRow.put("ref_table", "product");
		//依赖表主键字段名
		logRow.put("ref_column", "pc_id");
		//依赖表主键ID
		logRow.put("ref_pri_key", map.get("pc_id"));
		//账号ID
		logRow.put("create_by", loginUser.get("adid"));
		//设备类型//请求的来源
		logRow.put("device", ProductDeviceTypeKey.Web);
		//描述
		logRow.put("describe", "update product_code " + pcode_id);
		
		//具体更新的字段内容
		//将DBRow转换为数组
		DBRow[] content = DBRowUtils.dbRowConvertToArray(parm);
		logRow.put("content", content);
		
		floorSetupLogMgr.addSetupLog(logRow);
		
		/*******************************************************************************************************/
		
		boolean flag = false;
		if (upId>0) {
			flag=true;
		} 
		return getResultInfo(flag);
	}

	@RequestMapping(value = "/checkProductCode", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
	public DBRow checkProductCode(@RequestParam(value="p_code")String p_code,@RequestParam(value="retailer_id",defaultValue="0")long retailer_id,
			@RequestParam(value="pcode_id",required=false,defaultValue="0")long pcode_id,
			@RequestParam(value="pc_id")long pc_id,
			@RequestParam(value="code_type") String code_type, HttpServletResponse response)
			throws Exception {
		DBRow parm = new DBRow();
		parm.add("p_code",p_code);
		parm.add("retailer_id",retailer_id);
		DBRow result = new DBRow();
		boolean pcode_isExist = false;
		boolean retailer_isExist = false;
		if(pcode_id>0) {
			retailer_isExist = productCodeMgr.checkProductCode(pcode_id,pc_id,parm);
			pcode_isExist = false;
			result.add("retailer_id",retailer_isExist);
		} else {
			pcode_isExist = productCodeMgr.checkCodeField(" and p_code='"+p_code.trim()+"' and code_type='"+code_type+"'",pc_id);
			result.add("p_code",pcode_isExist);
			retailer_isExist = false;
			if(retailer_id>0) {
				pcode_isExist= false;
				retailer_isExist = productCodeMgr.checkCodeField(" and retailer_id="+retailer_id+" and code_type='"+code_type+"'",pc_id);
			}
			result.add("retailer_id",retailer_isExist);
		}
		if(pcode_isExist||retailer_isExist) {
			result.add("success",false);
		} else {
			result.add("success",true);
		}
		return result;
	}

	@RequestMapping(value = "/getRetailerList", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public @ResponseBody DBRow[] getAllRetailer() throws Exception {
		
		DBRow[] rows = productCodeMgr.getAllRetailer();
		return rows;
	}
	
	
}
