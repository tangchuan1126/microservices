package com.oso.product.iface;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.cwc.db.DBRow;

/**
 * 
 * @author liyi
 *
 */
public interface ProductCodeControllerIFace {
	public DBRow[] productCodeList(long pc_id,HttpServletResponse response) throws Exception;

	public DBRow insertProductCode(Map<String,String> map,HttpServletRequest request,HttpServletResponse response) throws Exception;
	
	public DBRow deleteProductCode(long pcode_id,HttpServletResponse response) throws Exception;
	
	public DBRow updateProductCode(Map<String,String> map,HttpServletRequest request,HttpServletResponse response) throws Exception;
	
	public DBRow checkProductCode(String p_code,long retailer_id,long pcode_id,long pc_id,String code_type,HttpServletResponse response) throws Exception;
	
	public DBRow[] getAllRetailer() throws Exception;
}
