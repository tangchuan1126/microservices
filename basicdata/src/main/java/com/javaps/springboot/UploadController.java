package com.javaps.springboot;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@EnableAutoConfiguration
@Controller
public class UploadController {
	
	@Value("${upload.dir}")
	private String uploadDir;
	
	@RequestMapping(value = "/upload", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	@ResponseBody
	public Map<String,Object> upload(@RequestParam(value="file",required=true) MultipartFile uploaded) throws Exception {
		Map<String,Object> m = new HashMap<String,Object>();
		m.put("originalFilename", uploaded.getOriginalFilename());
		m.put("contentType", uploaded.getContentType());
		m.put("name", uploaded.getName());
		m.put("size", uploaded.getSize());
		
		
		String saveTo = System.getProperty("java.io.tmpdir");
		if(uploadDir != null && new File(uploadDir).isDirectory()){
			saveTo = uploadDir;
		}
		uploaded.transferTo( new File(saveTo + uploaded.getOriginalFilename()));
		
		return m;
	}
}
