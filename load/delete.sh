#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ] 
then
	echo "USAGE:  delete.sh <JSESSIONID Value> <target URL>"
	exit 1
fi
curl -v \
-b "JSESSIONID=$1" \
--request DELETE \
$2
echo
