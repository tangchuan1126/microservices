package com.javaps.springboot;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Configuration
@Profile({"development","production"})
public class SessionAttrConfig  {

	private @Autowired RequestMappingHandlerAdapter adapter;

	@PostConstruct
	public void prioritizeCustomArgumentMethodHandlers () {
	  List<HandlerMethodArgumentResolver> argumentResolvers = 
	      new ArrayList<> ();
	  argumentResolvers.add(new SessionAttrResolver());
	  argumentResolvers.addAll(adapter.getArgumentResolvers());
	  
	  adapter.setArgumentResolvers (argumentResolvers);
	  
	}
	
}
