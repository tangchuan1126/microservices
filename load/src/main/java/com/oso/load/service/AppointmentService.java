package com.oso.load.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.oso.utils.RestUtil;

@Service

public class AppointmentService {
	
	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	@Autowired
	private FloorSyncLoadRelationMgr floorLoadMgr;
	
	@Autowired
	private FloorB2BOrderMgrZyj floorWMSOrdersMgr;
	
	private DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
	/**
	 * 修改或保存预约，非使用预约条
	 * 
	 * @param row
	 * @param invoices
	 * @param appointment_time /utc
	 * @param appointment_time1
	 * @param id
	 * @param superw
	 * @param uid
	 * @param loginUserName
	 * @param Sync10IP
	 * @param sessionid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String saveOrUpdateAppointment(DBRow row, List<Map<String,Object>> invoices, String appointment_time, String appointment_time1, String id, boolean superw, String uid, String loginUserName, String Sync10IP, String sessionid) throws Exception {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		
		if(id==null || id.length()==0 || "undefined".equals(id) ){// add	
			row.remove("id");
			row.put("user_created", loginUserName);
			row.put("date_created", new Date());
			row.put("status", "Open");
			row.put("appointment_time", format1.parse(appointment_time));
			
			row.remove("id");
			row.remove("key");
			// 新增appointment，返回aid
			String appointment = format2.format(format1.parse(appointment_time));
			String atype = row.getString("appointment_type", "");
			
			boolean b = false;
			
			if (superw) {
				if ("inbound".equals(atype)) {
					floorAppointmentMgr.sumOneWorkInNoLimit(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
				if ("outbound".equals(atype)) {
					floorAppointmentMgr.sumOneWorkOutNoLimit(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
			} else {
				if ("inbound".equals(atype)) {
					b = floorAppointmentMgr.sumOneWorkIn(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				} 
				
				if ("outbound".equals(atype)) {
					b = floorAppointmentMgr.sumOneWorkOut(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
				if (!b) throw new Exception("all the appointments have been taken!"+ appointment_time1);
			}
			String aid = ""+floorAppointmentMgr.addApp(row);
			floorAppointmentMgr.addAppLog("APPT", "", "", aid, loginUserName,"Create Appointment");
			// 保存关联单据
			if(invoices != null){
				for(Map<String,Object> invoice : invoices){
					String iid = invoice.get("id").toString();
					String type = invoice.get("type").toString();
					//新增关联单据
					floorAppointmentMgr.addAppInvoice(aid, type, iid,  uid);
					if ("load".equals(type)) {
						this.updateLoad(Long.parseLong(iid), appointment_time, row.get("carrier_id",""), Sync10IP, sessionid);
					}
					if ("order".equals(type)) {
						this.updateOrder(Long.parseLong(iid), appointment_time, row.get("carrier_id",""), Sync10IP, sessionid);
					}
					floorAppointmentMgr.addAppLog(type, iid, "", aid, loginUserName,"Modified Appointment");
				}
			}
			return aid;
		} else { // edit
			DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			row.put("date_updated", new Date());
			row.put("appointment_time", format1.parse(appointment_time));
			String nappointment = format2.format(format1.parse(appointment_time));
			
			long newtime = format1.parse(appointment_time).getTime();
			
			DBRow old = floorAppointmentMgr.getAppByAID(id);
			long oldtime = format3.parse(old.getString("appointment_time")).getTime();
			String atype = row.getString("appointment_type", "");
			String oappointment = format2.format(format3.parse(old.getString("appointment_time")));
			boolean b = false;
			if (newtime!=oldtime || !old.getString("appointment_type").equals(atype)) {
				//取消原先
				String oldtype = old.getString("appointment_type");
				if ("inbound".equals(oldtype)) {
					floorAppointmentMgr.subOneWorkIn(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
				if ("outbound".equals(oldtype)) {
					floorAppointmentMgr.subOneWorkOut(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
				//增加预约
				if (superw) {
					if ("inbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkInNoLimit(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
					if ("outbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkOutNoLimit(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
				} else {
					if ("inbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkIn(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
					if ("outbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkOut(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
				}
				if (!b) throw new Exception("all the appointments have been taken!"+ appointment_time1);
				if(newtime!=oldtime){
					floorAppointmentMgr.addAppLog("APPT", "", "", id, loginUserName,"Modified Appointment");
				}
			}
			row.remove("eta");
			row.remove("etd");
			//修改预约日期需要取消原预约，新增新预约。
			
			//if (old.get("appointment_time")=="")
			floorAppointmentMgr.updateApp(row);

			// 现有的关联单据
			DBRow[] oldInvoice = floorAppointmentMgr.getInvoiceByAID(id);
			// 保存关联单据，比较新旧数据，去掉已经删除的，增加新增的
			if(invoices != null){
				String idTypeStr = "1";
				if (invoices.size()>0) {
					List<String> ids = new ArrayList<String>();
					for(Map<String,Object> invoice : invoices){
						String iid = invoice.get("id").toString();
						String type = invoice.get("type").toString();
						idTypeStr += (",'"+iid+type+"'");
						ids.add(iid+":"+type);
						// 如果没有这个表单，添加
						if(!checkInvoice(oldInvoice, iid, type)){
							// 查询当前的aid
							String aidFrom = floorAppointmentMgr.getCurrentAid(type, iid);
							floorAppointmentMgr.addAppInvoice(id, type, iid, uid);
							//更新order appt or Load 的order
							
							// 查询添加后的aid
							String aidTo = floorAppointmentMgr.getCurrentAid(type, iid);
							// 写变化日志
							floorAppointmentMgr.addAppLog(type, iid, aidFrom, aidTo, loginUserName,"Modified Appointment");
						}
						if ("load".equals(type)) {
							this.updateLoad(Long.parseLong(iid), appointment_time, row.get("carrier_id",""), Sync10IP,sessionid);
						} else if ("order".equals(type)) {
							this.updateOrder(Long.parseLong(iid), appointment_time, row.get("carrier_id",""), Sync10IP,sessionid);
						}
					}
					for (DBRow item: oldInvoice) {
						String iid = item.get("invoice_id","");
						String type = item.get("invoice_type","");
						if (!ids.contains(iid+":"+type)) {
							if ("load".equals(type)) {
								this.updateLoad(Long.parseLong(iid), null, null, Sync10IP,sessionid);
							}
							if ("order".equals(type)) {
								this.updateOrder(Long.parseLong(iid), null, null, Sync10IP,sessionid);
							}
						}
					}
					
				} else {
					for (DBRow item: oldInvoice) {
						String iid = item.get("invoice_id","");
						String type = item.get("invoice_type","");
						if ("load".equals(type)) {
							this.updateLoad(Long.parseLong(iid), null, null, Sync10IP, sessionid);
						}
						if ("order".equals(type)) {
							this.updateOrder(Long.parseLong(iid), null, null, Sync10IP, sessionid);
						}
					}
				}
				floorAppointmentMgr.delInvoiceByAidAndNotInList(id, idTypeStr, loginUserName);
			}
		}
		return id;
	}
	
	/**
	 * 校验关系
	 * @param rows
	 * @param id
	 * @param type
	 * @return
	 */
	@Transactional(readOnly=true)
	private boolean checkInvoice(DBRow[] rows, String id, String type){
		if(rows == null){
			return false;
		} else {
			for(DBRow row: rows){
				String idA = row.getString("invoice_id");
				String typeA = row.getString("invoice_type");
				if(idA.equals(id) && typeA.equals(type)){
					return true;
				}
			}
			return false;
		}
	}
	
	public boolean useTicket(String storageId, String date, Integer hour, String type, String ticket) throws Exception {
		//floorAppointmentMgr.useTicket(ps_id, utc.substring(0,10), Integer.parseInt(utc.substring(11,13)) , atype, key);
		try {
			floorAppointmentMgr.clearOutTimeTicket(date);
		} finally {}
		return floorAppointmentMgr.useTicket(storageId, date, hour, type, ticket);
	}
	
	/**
	 * 保存预约信息
	 * 
	 * @param row
	 * @param invoices
	 * @param ps_id
	 * @param utc
	 * @param appointment_time
	 * @param key
	 * @param atype
	 * @param uid
	 * @param loginUserName
	 * @param Sync10IP
	 * @param sessionid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public boolean saveAppointmentUseTicket(DBRow row, List<Map<String,Object>> invoices, String ps_id, String utc, String appointment_time, String key, String atype, String uid, String loginUserName, String Sync10IP,  String sessionid) throws Exception {
		//boolean b = false;
		row.put("user_created", loginUserName);
		row.put("date_created", new Date());
		row.put("status", "Open");
		row.put("appointment_time", format1.parse(appointment_time));
		row.remove("eta");
		row.remove("etd");
		row.remove("id");
		row.remove("key");
		String aid = ""+floorAppointmentMgr.addApp(row);
		floorAppointmentMgr.addAppLog("APPT", "", "", aid, loginUserName,"Create Appointment");
		// 保存关联单据
		if(invoices != null){
			for(Map<String,Object> invoice : invoices){
				String id = invoice.get("id").toString();
				String type = invoice.get("type").toString();
				//新增关联单据
				floorAppointmentMgr.addAppInvoice(aid, type, id,  uid);
				if ("load".equals(type)) {
					updateLoad(Long.parseLong(id), appointment_time, row.get("carrier_id",""), Sync10IP, sessionid);
				}
				if ("order".equals(type)) {
					updateOrder(Long.parseLong(id), appointment_time, row.get("carrier_id",""), Sync10IP, sessionid);
				}
				floorAppointmentMgr.addAppLog(type, id, "", aid, loginUserName,"Modified Appointment");
			}
		}
		return true;
	}
	
	/**
	 * 删除预约
	 * 
	 * @param aid
	 * @param Sync10IP
	 * @param sessionid
	 * @throws Exception
	 */
	@Transactional
	public void delAppointment(String aid, String Sync10IP, String sessionid) throws Exception {
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DBRow old = floorAppointmentMgr.getAppByAID(aid);
		String oappointment = format2.format((Date)old.getValue("appointment_time"));
		String oldtype = old.getString("appointment_type");
		
		if ("inbound".equals(oldtype)) {
			floorAppointmentMgr.subOneWorkIn(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
		}
		if ("outbound".equals(oldtype)) {
			floorAppointmentMgr.subOneWorkOut(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
		}
		//获取关联
		DBRow[] oldInvoice = floorAppointmentMgr.getInvoiceByAID(aid);
		floorAppointmentMgr.delAppInvoice(aid);
		
		floorAppointmentMgr.delApp(aid);
		
		for (DBRow item: oldInvoice) {
			//清除关联
			String id = item.get("invoice_id","");
			String type = item.get("invoice_type","");
			if ("load".equals(type)) {
				updateLoad(Long.parseLong(id), null, null, Sync10IP, sessionid);
			}
			if ("order".equals(type)) {
				updateOrder(Long.parseLong(id), null, null, Sync10IP, sessionid);
			}
		}	
	}
	
	/**
	 * 修改Order的预约时间
	 * 
	 * @param oid
	 * @param appttime
	 * @param scac
	 * @param Sync10IP
	 * @return
	 * @throws Exception 
	 */
	private boolean updateOrder(long oid, String appttime, String scac, String Sync10IP, String sessionid) {
		try {
			DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
			DBRow para = new DBRow();
			if (scac!=null &&  !"".equals(scac)) {
				para.add("carriers", scac);
			} else {
				para.add("carriers", null);
			}
			if(appttime==null || "".equals(appttime)) {
				para.add("pickup_appointment", null);
			} else {
				para.add("pickup_appointment", format1.parse(appttime));
			}
			floorWMSOrdersMgr.modB2BOrder(oid, para);
			RestUtil.SyncCall(sessionid, Sync10IP, oid, para.get("carriers", ""), para.get("pickup_appointment", ""), null);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	/**
	 * 修改Order的预约时间
	 * 
	 * @param load
	 * @param appttime
	 * @param scac
	 * @param Sync10IP
	 * @return
	 * @throws Exception
	 */
	private boolean updateLoad(long load, String appttime,String scac, String Sync10IP, String sessionid) {
		try {
			DBRow row = floorLoadMgr.getWmsLoad(String.valueOf(load));
			if (scac==null) scac = row.get("carrier_id", "");
			DBRow[] orders = floorLoadMgr.getWmsOrdersByLoadId(String.valueOf(load));
			for (DBRow order: orders) {
				boolean f = updateOrder(order.get("order_id", 0L), appttime, scac, Sync10IP, sessionid);
				if (!f) return false;
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * 生成预约条
	 * 
	 * @param storageId
	 * @param utc
	 * @param type
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public String makeApptTicket(String storageId, String utc, String type, Map<String, Object> session) throws Exception {
		floorAppointmentMgr.clearOutTimeTicket(utc.substring(0,10));
		boolean superw = false;
		superw = DBRowUtils.existDeptAndPostInLoginBean(1000008, 10L, DBRowUtils.getAdminLoginBean(session));
		if (superw) {
			if ("inbound".equals(type)) {
				floorAppointmentMgr.sumOneWorkInNoLimit(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), null,true);
			}
			if ("outbound".equals(type)) {
				floorAppointmentMgr.sumOneWorkOutNoLimit(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), null,true);
			}
		}
		if ("inbound".equals(type)) {
			boolean b = floorAppointmentMgr.sumOneWorkIn(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
			if (!b) throw new Exception("All the appointments have been taken.");
		} else if ("outbound".equals(type)) {
			boolean b = floorAppointmentMgr.sumOneWorkOut(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
			if (!b) throw new Exception("All the appointments have been taken.");
		}
		return floorAppointmentMgr.makeTicket(storageId, utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), type, String.valueOf(session.get("adid")));
	}
	
	/**
	 * 取消预约条
	 * 
	 * @param storageId
	 * @param utc
	 * @param type
	 * @param key
	 * @return
	 * @throws Exception
	 */
	@Transactional
	public boolean cancelApptTicket(long storageId, String utc, String type, String key) throws Exception {
		boolean b = floorAppointmentMgr.cancelTicket(String.valueOf(storageId),utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), type, key);
		if (b) {
			boolean db = false;
			if ("inbound".equals(type)) {
				db = floorAppointmentMgr.subOneWorkIn(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), storageId, "", false);
				if (!db) throw new Exception("Cancel appointments Error!");
			} else if ("outbound".equals(type)) {
				db = floorAppointmentMgr.subOneWorkOut(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), storageId, "", false);
				if (!db) throw new Exception("Cancel appointments Error!");
			}
			return db;
		}
		return false;
	}
}