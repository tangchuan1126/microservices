package com.oso.load.beans;

import java.util.HashMap;
import java.util.Map;

public class B2bOrderStatus {
	private Map<String,String> statusMap = new HashMap<String,String>();
	
	public B2bOrderStatus() {
		// 1准备中2正常运输3审核中4完成5装箱中6差异运输
		statusMap.put("1", "准备中");
		statusMap.put("2", "正常运输");
		statusMap.put("3", "审核中");
		statusMap.put("4", "完成");
		statusMap.put("5", "装箱中");
		statusMap.put("6", "差异运输");
	}
	
	public String getOrderStatus(String key){
		return (String)statusMap.get(key);
	}
}
