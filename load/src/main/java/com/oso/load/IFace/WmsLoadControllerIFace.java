package com.oso.load.IFace;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.cwc.db.DBRow;


public interface WmsLoadControllerIFace {

	/**
	 * 分页查询Load列表
	 */
	@RequestMapping(value = "/getWmsLoadList", produces = "application/json; charset=UTF-8")
	Map getWmsLoadList(String load_no, String status, String start_time,
			String end_time, String company,  String customer, int page, int page_size) throws Exception;

	/**
	 * 分页查询Order
	 */
	@RequestMapping(value = "/getWmsOrderByComCusPoReq", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	Map getWmsOrderByComCusPoReq(String customer_id, String company_id,
			String po_no, String req_start, String req_end, String checkIds, int page,
			int page_size) throws Exception;

	/**
	 * 根据loadId查询单个load
	 */
	@RequestMapping(value = "/getWmsLoadById", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	Map getWmsLoadById(String load_id) throws Exception;

	/**
	 * 根据loadId查询单个load
	 */
	@RequestMapping(value = "/modMasterBol", produces = "application/json; charset=UTF-8",method = RequestMethod.POST)
	Map modMasterBol(String load_id,List<String> oids,String zipcode,String address,String cid) throws Exception;
	
	/**
	 * 根据loadId查询单个load
	 */
	@RequestMapping(value = "/getMasterBolByLoadId", produces = "application/json; charset=UTF-8",method = RequestMethod.POST)
	Map getMasterBolByLoadId(String load_id) throws Exception;
	
	/**
	 * 根据LoadNo查询Load-Order列表
	 */
	@RequestMapping(value = "/getOrderDetailByLoadNo", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	DBRow[] getOrderDetailByLoadNo(String load_no) throws Exception;

	/**
	 * 新增Load
	 */
	@RequestMapping(value = "/addLoad", produces = "application/json; charset=UTF-8")
	Map addLoad(String data, HttpSession session) throws Exception;

	/**
	 * 修改Load
	 */
	@RequestMapping(value = "/editLoad", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	Map editWmsLoad(String data, HttpSession session) throws Exception;

	/**
	 * 删除Load
	 */
	@RequestMapping(value = "/delLoad", produces = "application/json; charset=UTF-8")
	Map delWmsLoad(String data, HttpSession session) throws Exception;

	/**
	 * company列表
	 */
	@RequestMapping(value = "/companyList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	DBRow[] getCompanyList() throws Exception;

	/**
	 * customer列表
	 */
	@RequestMapping(value = "/customerList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	DBRow[] getCustomerList() throws Exception;
}