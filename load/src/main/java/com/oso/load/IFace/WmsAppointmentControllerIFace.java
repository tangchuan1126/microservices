package com.oso.load.IFace;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.cwc.db.DBRow;

public interface WmsAppointmentControllerIFace {

	/**
	 * 添加Appointment
	 */
	//@RequestMapping(value = "/addAppointment", produces = "application/json; charset=UTF-8")
	Map addAppointment(String data, HttpSession session) throws Exception;

	/**
	 * 添加Appointment
	 */
	@RequestMapping(value = "/getLoginStorageCatalog", produces = "application/json; charset=UTF-8")
	DBRow getLoginStorageCatalog(HttpSession session) throws Exception;

	/**
	 * 根据类型和单据id，查询单据详情
	 */
	@RequestMapping(value = "/getInvicesLikeTypeId", produces = "application/json; charset=UTF-8")
	DBRow[] getInvicesLikeTypeId(String no, String type) throws Exception;

	/**
	 * 根据类型和单据id，查询单据详情
	 */
	@RequestMapping(value = "/getInviceByTypeId", produces = "application/json; charset=UTF-8")
	DBRow[] getInviceByTypeId(String id, String type) throws Exception;
	
	/**
	 * 编辑Appointment
	 */
	@RequestMapping(value = "/editAppointment", produces = "application/json; charset=UTF-8")
	Map editAppointment(String aid) throws Exception;
	
	/**
	 * 删除Appointment
	 */
	//@RequestMapping(value = "/delAppointment", produces = "application/json; charset=UTF-8")
	Map delAppointment(String aid) throws Exception;

	/**
	 * 分页查询Appointment
	 */
	@RequestMapping(value = "/getAppointmentList", produces = "application/json; charset=UTF-8")
	Map getAppointmentList(int pageNo, int pageSize, String selectOrderType, String orderNum, String inputAppointmentTime, String inputCarrier, String searchFrom,String boundType) throws Exception;
	
	/**
	 * 分页查询load
	 */
	@RequestMapping(value = "/getAppLoadList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	Map getAppLoadList(int page, int rows, String loadNo, String startDate, String endDate, String checkIds) throws Exception;
	
	/**
	 * 分页查询load
	 */
	@RequestMapping(value = "/getAppOrderList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	Map getAppOrderList(int page, int rows, String orderNo, String startDate, String endDate, String checkIds) throws Exception;
	
	/**
	 * 分页查询load
	 */
	@RequestMapping(value = "/getAppPoList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	Map getAppPoList(int page, int rows, String loadNo, String startDate, String endDate, String checkIds) throws Exception;
	
	/**
	 * 根据开始结束时间获取指定仓库下预约的日统计数据
	 * 
	 * @param storage_id
	 * @param start
	 * @param end
	 * @return 
	 * @throws Exception
	 * @author wangcr
	 */
	//@RequestMapping(value = "/sumWorkByStartEnd", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Map<String,Object>> sumWorkByStartEnd(@RequestParam(value = "storage_id", required = true) long storage_id, @RequestParam(value = "start", required = true) String start, @RequestParam(value = "end", required = true) String end) throws Exception;

	/**
	 * 根据日期获取指定仓库下预约的整点统计数据
	 * @param storage_id
	 * @param date
	 * @return
	 * @throws Exception
	 */
	//@RequestMapping(value = "/sumWorkByDate", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> sumWorkByDate(@RequestParam(value = "storage_id", required = true) long storage_id, @RequestParam(value = "date", required = true) String date) throws Exception;
	
	/**
	 * 提供work限制数据
	 * 
	 * @param storage_id
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	//@RequestMapping(value = "/getBaseWorkByWeek", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> getBaseWorkByWeek(@RequestParam(value = "storage_id", required = true) long storage_id, @RequestParam(value = "start", required = true) String start, @RequestParam(value = "end", required = true) String end) throws Exception;
}