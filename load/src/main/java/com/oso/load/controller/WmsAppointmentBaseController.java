package com.oso.load.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

@RestController
@Transactional
public class WmsAppointmentBaseController 
{
	//DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
	//DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	//DateFormat format3 = new SimpleDateFormat("MM/dd/yyyy");

	@SuppressWarnings("serial")
	private static Map<String,String> WEEKS = new HashMap<String,String>(){{
	    put("0", "WEEK:A");
	    put("1", "WEEK:B");
	    put("2", "WEEK:C");
	    put("3", "WEEK:D");
	    put("4", "WEEK:E");
	    put("5", "WEEK:F");
	    put("6", "WEEK:G");
	    put("-1","DAY");
	}};
	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	/**
	 * 根据开始结束时间获取指定仓库下预约的日统计数据
	 * 
	 * @param storage_id
	 * @param start
	 * @param end
	 * @return 
	 * @throws Exception
	 * @author wangcr
	 */
	@RequestMapping(value = "/sumWorkByStartEnd", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Map<String, Object>> sumWorkByStartEnd(
			@RequestParam(value = "storage_id", required = true) long storage_id, 
			@RequestParam(value = "start", required = true) String start, 
			@RequestParam(value = "end", required = true) String end) throws Exception {
		List<Map<String, Object>> rt = new ArrayList<Map<String, Object>>();
		
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = format1.parse(start+" 00");
		Date date2 = new Date(format1.parse(end+" 00").getTime()+24*3600*1000);
		//DBRow[] rows = floorAppointmentMgr.sumWorkByStartEnd(start, end, storage_id );
		DBRow[] rows = floorAppointmentMgr.sumWorkByStartEndTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id, DateUtil.getSubHour( DateUtil.showUTCTime(date1, storage_id),format2.format(date1)) );
		for (DBRow row : rows) {
			Map<String, Object> mp = new HashMap<String, Object>();
			mp.put("title", "");
			mp.put("start", row.getString("date"));
			mp.put("lentxt", row.getString("work_in"));
			mp.put("type", "inbound");
			if (!"0".equals(row.getString("work_in")))
			rt.add(mp);
			
			Map<String, Object> mp1 = new HashMap<String, Object>();
			mp1.put("title", "");
			mp1.put("start", row.getString("date"));
			mp1.put("lentxt", row.getString("work_out"));
			mp1.put("type", "outbound");
			if (!"0".equals(row.getString("work_out"))) rt.add(mp1);
		}
		return rt;
	}
	
	/**
	 * 根据日期获取指定仓库下预约的整点统计数据
	 * @param storage_id
	 * @param date
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sumWorkByDate", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String, Object> sumWorkByDate(
			@RequestParam(value = "storage_id", required = true) long storage_id, 
			@RequestParam(value = "date", required = true) String date
		) throws Exception {
		
		Map<String, Object> rt = new HashMap<String, Object>();
		List<Map<String, Object>> rts = new ArrayList<Map<String, Object>>();
		
		//Map<String, Map<String,Object>> hours = new java.util.LinkedHashMap<String, Map<String,Object>>();
		
		//DateUtil.showUTCTime(date+" 00:00:00", storage_id)
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		//format1.setTimeZone(DateUtil.getTimeZone(storage_id));
		Date date1 = format1.parse(date+" 00");
		Date date2 = new Date(format1.parse(date+" 00").getTime()+24*3600*1000);
		
		DBRow[] rows = floorAppointmentMgr.sumWorkByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id);
		//int i=0;
		long hh = 0;// -3600000L;
		if (rows.length<24) {
			//未初始化
			//boolean rd = floorAppointmentMgr.initDay(date, storage_id);
			//rows = floorAppointmentMgr.sumWorkByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id);
			List<DBRow> rtl = new ArrayList<DBRow>();
			floorAppointmentMgr.clearOutTimeTicket(null);
			floorAppointmentMgr.vinitDay(date, storage_id,rtl);
			rows = rtl.toArray(new DBRow[0]);
			DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format2.setTimeZone(DateUtil.getTimeZone(storage_id));
			long nowlong = (new Date()).getTime()-hh;
			for (DBRow row : rows) {
				Map<String,Object> hour = new HashMap<String,Object>();
				String key ="";
				if (row.get("hour", 0)>9) {
					key = (date+" "+row.getString("hour")+":00:00").substring(0, 13);
				} else {
					key = (date+" 0"+row.getString("hour")+":00:00").substring(0, 13);
				}
				long ulong = format2.parse((key+":00:00")).getTime();
				
				hour.put("ID", key.substring(11));
				hour.put("CNTIN",  row.get("cntin",0));
				hour.put("SUMIN",  row.get("sumin",0));
				hour.put("CNTOUT", row.get("cntout",0));
				hour.put("SUMOUT", row.get("sumout",0));
				hour.put("TOTAL",  row.get("sumin",0)+row.get("sumout",0));
				hour.put("ALL",    row.get("cntin",0)+row.get("cntout",0));
				hour.put("TIME",   ulong>=nowlong);
				rts.add(hour);
			}
			rt.put("TOTAL", rts.size());
			rt.put("DATA", rts);
			return rt;
			
		}
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		format2.setTimeZone(DateUtil.getTimeZone(storage_id));
		long nowlong = (new Date()).getTime()-hh;//
		for (DBRow row : rows) {
			Map<String,Object> hour = new HashMap<String,Object>();
			String key ="";
			if (row.get("hour", 0)>9) {
				key = DateUtil.showLocalTime(row.getString("date")+" "+row.getString("hour")+":00:00", storage_id).substring(0, 13);
			} else {
				key = DateUtil.showLocalTime(row.getString("date")+" 0"+row.getString("hour")+":00:00", storage_id).substring(0, 13);
			}
			long ulong = format2.parse((key+":00:00")).getTime();
			
			hour.put("ID", key.substring(11));
			hour.put("CNTIN",  row.getValue("work_in"));
			hour.put("SUMIN",  row.getValue("limit_in"));
			hour.put("CNTOUT", row.getValue("work_out"));
			hour.put("SUMOUT", row.getValue("limit_out"));
			hour.put("TOTAL",  row.getValue("limit"));
			hour.put("ALL",    row.getValue("work"));
			hour.put("TIME",   ulong>=nowlong);
			rts.add(hour);
		}
		rt.put("TOTAL", 24);
		rt.put("DATA", rts);
		return rt;
	}
	
	/**
	 * 用户可用时段
	 * 
	 * @param storage_id
	 * @param date
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/sumWorkEnable", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String, Object> sumWorkEnable(
			@RequestParam(value = "storage_id", required = true) long storage_id, 
			@RequestParam(value = "date", required = true) String date
		) throws Exception {
		
		Map<String, Object> rt = new HashMap<String, Object>();
		List<Map<String, Object>> rts = new ArrayList<Map<String, Object>>();
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		Date date1 = format1.parse(date+" 00");
		Date date2 = new Date(format1.parse(date+" 00").getTime()+24*3600*1000);
		DBRow[] rows = floorAppointmentMgr.sumWorkByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id);
		long hh = 0;// -3600000L;
		if (rows.length<24) {
			//未初始化
			List<DBRow> rtl = new ArrayList<DBRow>();
			floorAppointmentMgr.vinitDay(date, storage_id,rtl);
			//rows = floorAppointmentMgr.sumWorkByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id);
			rows = rtl.toArray(new DBRow[0]);
			
			DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format2.setTimeZone(DateUtil.getTimeZone(storage_id));
			long nowlong = (new Date()).getTime()-hh;
			for (DBRow row : rows) {
				Map<String,Object> hour = new HashMap<String,Object>();
				String key ="";
				if (row.get("hour", 0)>9) {
					key = (date+" "+row.getString("hour")+":00:00").substring(0, 13);
				} else {
					key = (date+" 0"+row.getString("hour")+":00:00").substring(0, 13);
				}
				long ulong = format2.parse((key+":00:00")).getTime();
				if (ulong>=nowlong) {
					hour.put("ID", key.substring(11));
					hour.put("CNTIN", row.get("cntin", 0)<row.get("sumin",0)?1:0);
					hour.put("CNTOUT", row.get("cntout", 0)<row.get("sumout",0)?1:0);
					if (row.get("sumin",-1)>=0 && row.get("sumout",-1)>=0) {
						rts.add(hour);
					}
				}
			}
			rt.put("TOTAL", rts.size());
			rt.put("DATA", rts);
			return rt;
		} else {
			DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format2.setTimeZone(DateUtil.getTimeZone(storage_id));
			long nowlong = (new Date()).getTime()-hh;
			for (DBRow row : rows) {
				Map<String,Object> hour = new HashMap<String,Object>();
				String key ="";
				if (row.get("hour", 0)>9) {
					key = DateUtil.showLocalTime(row.getString("date")+" "+row.getString("hour")+":00:00", storage_id).substring(0, 13);
				} else {
					key = DateUtil.showLocalTime(row.getString("date")+" 0"+row.getString("hour")+":00:00", storage_id).substring(0, 13);
				}
				long ulong = format2.parse((key+":00:00")).getTime();
				if (ulong>=nowlong) {
					hour.put("ID", key.substring(11));
					hour.put("CNTIN", row.get("work_in",0)<row.get("limit_in",0)?1:0);
					hour.put("CNTOUT", row.get("work_out",0)<row.get("limit_out",0)?1:0);
					if (row.get("limit_in",-1)>=0 && row.get("limit_out",-1)>=0) {
						rts.add(hour);
					}
				}
			}
			rt.put("TOTAL",24);
			rt.put("DATA", rts);
			return rt;
		}
	}
	/**
	 * 提供work限制数据
	 * 
	 * @param storage_id
	 * @param start
	 * @param end
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getBaseWorkByWeek", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String, Object> getBaseWorkByWeek(
			@RequestParam(value = "storage_id", required = true) long storage_id, 
			@RequestParam(value = "start", required = true) String start, 
			@RequestParam(value = "end", required = true) String end) throws Exception {
		Map<String, Object> rt = new HashMap<String, Object>();
		Map<String, Object> Dates = new HashMap<String, Object>();
		java.text.SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		sdf.setTimeZone(DateUtil.getTimeZone(storage_id));
		
		Date dt = sdf.parse(start);
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*0))), "A");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*1))), "B");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*2))), "C");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*3))), "D");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*4))), "E");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*5))), "F");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*6))), "G");
		
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date1 = format1.parse(start+" 00");
		Date date2 = new Date(format1.parse(end+" 00").getTime()+24*3600*1000);
		
		int hour = DateUtil.getSubHour( DateUtil.showUTCTime(date1, storage_id), format2.format(date1));
		
		rt.put("DATES", Dates);
		DBRow[] rows = floorAppointmentMgr.sumBaseByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id, hour);
		if (rows.length<168) {
			rows = floorAppointmentMgr.initWeek2(start, end, storage_id);
		}
		//rows = floorAppointmentMgr.sumBaseByDayTotal(DateUtil.showUTCTime(date1, storage_id), DateUtil.showUTCTime(date2, storage_id), storage_id, hour);
		rt.put("DATA", rows);
		//rt.put("BASE", floorAppointmentMgr.getBaseDefault(storage_id));
		
		return rt;
	}

	/**
	 * 设置约车限制数据
	 * 
	 * @param storage_id
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/setBaseWork", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	@Transactional
	public Map<String, Object> setBaseWork(
			@RequestParam(value = "storage_id", required = true) long storage_id,
			@RequestParam(value = "weeks[]", required = true) List<String> weeks,
			@RequestParam(value = "data", required = true) String data
		) throws Exception {
		JSONArray array = new JSONArray(data);
		int cnt =0;
		List<DBRow> rows = new ArrayList<>();
		for (String week:weeks) {
			for (int i =0;i <array.length(); i++) {
				JSONObject obj =  array.getJSONObject(i);
				DBRow row = new DBRow();
				row.add("storage_id", storage_id);
				row.add("role_id", 1);
				row.add("date_type", WEEKS.get(""+Integer.valueOf(week)));
				row.add("`hour`", obj.getInt("hour"));
				row.add("limit_in", obj.getInt("value_in"));
				row.add("limit_out", obj.getInt("value_out"));
				rows.add(row);
			}
		}
		cnt = setBaseWork(rows);
		Map<String, Object> rt = new HashMap<String, Object>();
		rt.put("OK", cnt);
		return rt;
	}
	
	private int setBaseWork(List<DBRow> rows) throws Exception {
		int cnt =0;
		for (DBRow row:rows) {
			DBRow  r = floorAppointmentMgr.addOrUpdateDefaultValue(row);
			if (r!=null) cnt++;
		}
		return cnt;
	}
	
	private DBRow setBaseWork(long storage_id, String sdate, int hour, int week, Integer value_in, Integer value_out) throws Exception {
		DBRow row = new DBRow();
		row.put("storage_id", storage_id);
		//row.put("appointment_type", type);
		Date date = null;
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH");
		if (hour>9) {
			date = format1.parse(sdate+" "+ hour);
		} else {
			date = format1.parse(sdate+" 0"+ hour);
		}
		String utc =  DateUtil.showUTCTime(date, storage_id);
		row.put("work_date", utc.substring(0, 10));
		row.put("`hour`", Integer.parseInt(utc.substring(11, 13)));
		//row.put("`week`", week);
		if (value_in!=null)	row.put("`limit_in`",value_in);
		if (value_out!=null) row.put("`limit_out`",value_out);
		return floorAppointmentMgr.addOrUpdateWorkTotal(row);
	}
	
	/**
	 * 获取仓库的预约限制默认值
	 * 
	 * @param storage_id
	 * @param boundtype
	 * @return
	 */
	@RequestMapping(value = "/getDefaultValue", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String, Object> getDefaultValue(
			@RequestParam(value = "storage_id", required = true) long storage_id,
			//@RequestParam(value = "bound_type", required = false) String boundtype,
			@RequestParam(value = "week_id", required = true) Integer week
		) throws Exception {
		Map<String, Object> rt = new HashMap<String, Object>();
		List<Map<String, Object>> rts = new ArrayList<Map<String, Object>>();
		//for (int i=0;i<7;i++) {}
		DBRow[] rows = floorAppointmentMgr.getDefaultValue(storage_id, week);
		for (DBRow row:rows) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("ID", row.get("hour", 0));
			map.put("VALUE_IN", row.get("limit_in", 0));
			map.put("VALUE_OUT", row.get("limit_out", 0));
			rts.add(map);
		}
		rt.put("DATA", rts);
		//rt.put("BASE", floorAppointmentMgr.getBaseDefault(storage_id));
		return rt;
	}
	
	/**
	 * 设置仓库预约限制默认值
	 * 
	 * @param storage_id
	 * @param boundtype
	 * @param data
	 * @return
	 * @throws Exception
	 */
	//@RequestMapping(value = "/setDefaultValue", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	@Deprecated
	public Map<String, Object> setDefaultValue (
			@RequestParam(value = "storage_id", required = true)  long storage_id, 
			@RequestParam(value = "bound_type", required = true) String boundtype,
			@RequestParam(value = "data", required = true)  String data) throws Exception{
		JSONArray array = new JSONArray(data);
		int cnt =0;
		for (int i =0;i <array.length(); i++) {
			JSONObject obj =  array.getJSONObject(i);
			DBRow row = new DBRow();
			row.put("storage_id", storage_id);
			row.put("appointment_type", "inbound".equalsIgnoreCase(boundtype)?"inbound":"outbound");
			row.put("`hour`", obj.getInt("hour"));
			row.put("`limit_in`", obj.getInt("value_in"));
			row.put("`limit_out`", obj.getInt("value_out"));
			row.put("role_id", 1);
			DBRow  r = floorAppointmentMgr.addOrUpdateDefaultValue(row);
			if (r!=null) cnt++;
		}
		 Map<String, Object> rt = new HashMap<String, Object>();
		 rt.put("OK", cnt);
		 return rt;
	}
	
	/**
	 * 
	 * 
	 * @param storage_id
	 * @param boundtype
	 * @param start
	 * @param end
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	@RequestMapping(value = "/setDefaultValueByDate", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	@Transactional
	public Map<String, Object> setDefaultValueByDate (
			@RequestParam(value = "storage_id", required = true)  long storage_id,
			@RequestParam(value = "start", required = true) String start,
			@RequestParam(value = "end", required = true) String end,
			@RequestParam(value = "weeks[]", required = false) List<String> weeks,
			@RequestParam(value = "data", required = true)  String data
		) throws Exception{
		Map<String, Object> rt = new HashMap<String, Object>();
		JSONArray array = new JSONArray(data);
		int cnt =0;
		
		DBRow role = floorAppointmentMgr.addOrUpdateRole(start,end,"other");
		long role_id = role.get("bid", 0L);
		//Map<String, Map<String,Integer>> hours = new HashMap<String,Map<String,Integer>>();
		for (int i =0;i <array.length(); i++) {
			JSONObject obj =  array.getJSONObject(i);
			
			if (weeks==null || weeks.size()==0) {
				DBRow row = new DBRow();
				row.put("role_id", role_id);
				row.put("storage_id", storage_id);
				row.put("`hour`", obj.getInt("hour"));
				row.put("date_type", "DAY");
				if (obj.has("value_in") && !"".equals(obj.getString("value_in"))) {
					row.put("limit_in", obj.getInt("value_in"));
				}
				if (obj.has("value_out") && !"".equals(obj.getString("value_out"))) {
					row.put("limit_out", obj.getInt("value_out"));
				}
				DBRow  r = floorAppointmentMgr.addOrUpdateDefaultValue(row);
				if (r!=null) cnt++;
			} else {
				for (String day: weeks) {
					DBRow row = new DBRow();
					row.put("role_id", role_id);
					row.put("storage_id", storage_id);
					row.put("`hour`", obj.getInt("hour"));
					row.put("date_type", WEEKS.get(""+day));
					if (obj.has("value_in") && !"".equals(obj.getString("value_in"))) {
						row.put("limit_in", obj.getInt("value_in"));
					}
					if (obj.has("value_out") && !"".equals(obj.getString("value_out"))) {
						row.put("limit_out", obj.getInt("value_out"));
					}
					DBRow  r = floorAppointmentMgr.addOrUpdateDefaultValue(row);
					if (r!=null) cnt++;
				}
			}
		}
		List<String> days = getDays(start,end);
		DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
		format1.setTimeZone(DateUtil.getTimeZone(storage_id));
		
		for (String day:days) {
			for (int i =0;i <array.length(); i++) {
				JSONObject obj =  array.getJSONObject(i);
				if (weeks==null || weeks.size()==0) {
					obj.getInt("hour");
					Integer InO = null;
					Integer OutO = null;
					if (obj.has("value_in") && !"".equals(obj.getString("value_in"))) {
						InO =  obj.getInt("value_in");
					}
					if (obj.has("value_out") && !"".equals(obj.getString("value_out"))) {
						OutO = obj.getInt("value_out");
					}
					setBaseWork(storage_id, day, obj.getInt("hour"), -1, InO, OutO);
				} else {
					int wt = format1.parse(day).getDay();
					Integer InO = null;
					Integer OutO = null;
					if (obj.has("value_in") && !"".equals(obj.getString("value_in"))) {
						InO =  obj.getInt("value_in");
					}
					if (obj.has("value_out") && !"".equals(obj.getString("value_out"))) {
						OutO = obj.getInt("value_out");
					}
					if (weeks.contains(""+wt)) {
						setBaseWork(storage_id, day, obj.getInt("hour"), wt, InO, OutO);
					}
				}
			}
		}
		
		rt.put("OK", cnt);
		return rt;
	}
	
	@RequestMapping(value = "/getSCACByID", produces = "application/json; charset=UTF-8")
	public DBRow getSCAC(@RequestParam(value = "id", required = true) long id) {
		try {
			return floorAppointmentMgr.getSCACById(id);
		} catch (Exception e) {
			return new DBRow();
		}
	}
	
	private static List<String> getDays(String start , String end) throws Exception {
		List<String> rt = new ArrayList<String>();
		int li = DateUtil.getSubDay(DateUtil.DatetoStr(DateUtil.StringToDate(start)), DateUtil.DatetoStr(DateUtil.StringToDate(end)));
		Date date = DateUtil.formateDate(start);
		if (li>=0) {
			for (int i=0;i<=li;i++) {
				rt.add(DateUtil.DatetoStr( new Date(date.getTime()+ i*(86400000L)) ));
			}
		}
		return rt;
	}
	
	/*
	public static void main(String[] arg) throws Exception {
		List<String> rt = getDays("2015-01-01","2015-01-01");
		System.out.println(rt.get(0));
		System.out.println(rt.get(rt.size()-1));
		System.out.println(rt.size());
	}
	*/
}
