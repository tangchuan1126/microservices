package com.oso.load.controller;

import java.math.BigDecimal;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.floor.api.FloorWMSOrdersMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.app.util.StrUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.mongodb.util.JSON;
import com.oso.utils.CommonUtil;
import com.oso.utils.RestUtil;
import com.vvme.order.dao.B2bOrderItemDao;

@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
@RestController
@Transactional(readOnly=true)
public class WmsLoadController{
	
	@Value("${Sync10IP}")
	private String Sync10IP;
	
	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	@Autowired
	private FloorSyncLoadRelationMgr floorLoadMgr;
	
	@Autowired
	private FloorWMSOrdersMgr floorWMSOrdersMgr;
	
	@Autowired
	private B2bOrderItemDao b2bOrderItemDao;
	
	public void setB2bOrderItemDao(B2bOrderItemDao b2bOrderItemDao) {
		this.b2bOrderItemDao = b2bOrderItemDao;
	}

	public FloorSyncLoadRelationMgr getFloorLoadMgr() {
		return floorLoadMgr;
	}

	public void setFloorLoadMgr(FloorSyncLoadRelationMgr floorLoadMgr) {
		this.floorLoadMgr = floorLoadMgr;
	}
	
	/**
	 * 分页查询Load列表
	 */
	@RequestMapping(value = "/getWmsLoadList", produces = "application/json; charset=UTF-8")
	public Map getWmsLoadList(
			@RequestParam(value = "load_no", required = false) String load_no,
			@RequestParam(value = "status", required = false) String status,
			@RequestParam(value = "begin_time", required = false) String begin_time,
			@RequestParam(value = "end_time", required = false) String end_time,
			@RequestParam(value = "company", required = false) String company,
			@RequestParam(value = "customer", required = false) String customer,
			@RequestParam(value = "pageNo") int page,
			@RequestParam(value = "pageSize") int page_size) throws Exception {
		Map result = new HashMap();
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(safeInt(page));
		pc.setPageSize(10);
		DBRow[] rows = floorLoadMgr.getWmsLoadList(load_no, status, begin_time,
				end_time, company, customer, pc);

		List dataList = new ArrayList();
		Map load = null;
		List loadChildren = null;

		Map order = null;
		Map loadAndmasterOrderMap = null;
		List loadAndmasterOrderList = null;

		// load列表
		for (DBRow row : rows) {
			//row.add("req_ship_date", " ");
			//row.add("gate_check_in", " ");
			//row.add("feight_type", " ");
			
			String loadId = row.getString("load_id");
			loadAndmasterOrderMap = new HashMap();
			loadAndmasterOrderList = new ArrayList();
			/********************** load部分开始 *************************/
			load = new HashMap();
			loadChildren = new ArrayList();

			load.put("LOADID", row.getString("load_id"));
			load.put("LOADNO", row.getString("load_no"));
			load.put("aid", row.getString("aid"));

			// order 属性，值
			addToList(row, loadChildren, "Creator", "user_created");
			addToList(row, loadChildren, "Create Time", "created");
			addToList(row, loadChildren, "SCAC", "carrier_id");
			addToList(row, loadChildren, "Update Time", "updated");
//			addToList(row, loadChildren, "AppointmentId", "aid");
			
			//addToList(row, loadChildren, "Req.Ship.Date", "req_ship_date");
			//addToList(row, loadChildren, "Gate Check In", "gate_check_in");
			
			
			String appt=row.getString("appointment_time");
			if(appt.trim().length()!=0){
				long storageId=Long.parseLong(row.getString("storage_id"));
				Date dt=DateUtil.locationZoneTime(appt, storageId);
				if (dt!=null) appt = format1.format(dt);
				Map detail = new HashMap();
				detail.put("LABLE", "Appointment Time");
				detail.put("VALUE", appt);
				loadChildren.add(detail);
			}
			
			load.put("CHILDREN", loadChildren);

			loadAndmasterOrderList = getMasterOrderListByLoadId(row
					.getString("load_id"));

			loadAndmasterOrderMap.put("trid", loadId);
			loadAndmasterOrderMap.put("LOAD", load);
			loadAndmasterOrderMap.put("ORDERLIST", loadAndmasterOrderList);

			// load本身的修改日志，order上下日志
			List loadLogList = new ArrayList();
			
			// Load创建日志
			Map loadLogDetail = new HashMap();
			loadLogDetail.put("USER", row.getString("user_created"));
			loadLogDetail.put("OPTION", "created");
			loadLogDetail.put("VALUE", "");
			loadLogDetail.put("DATETIME", format1.parse(row.getString("created")).getTime());
			
			// 根据load_id查询load的操作日志
			List loadLogs = getLoadLogsById(loadId);

			loadLogList.addAll(loadLogs);
			loadLogList.add(loadLogDetail);

			Map LoadOptionMap = new HashMap();
			LoadOptionMap.put("LOGDETAILS", loadLogList);
			loadAndmasterOrderMap.put("OPTION", LoadOptionMap);

			dataList.add(loadAndmasterOrderMap);
		}

		result.put("DATA", dataList);
		result.put("PAGECTRL", pc);

		return result;
	}

	/**
	 * 根据load_id查询load的操作日志
	 */
	public List getLoadLogsById(String loadId)
			throws Exception {
		DBRow[] rows = floorLoadMgr.getLoadLogsById(loadId);

		List<Map> logList = new ArrayList<Map>();
		if (rows != null) {
			for(DBRow row : rows){
				// Load创建日志
				Map loadLogDetail = new HashMap();
				loadLogDetail.put("USER", row.getString("user_created"));
				loadLogDetail.put("OPTION", row.getString("lable"));
				loadLogDetail.put("VALUE", row.getString("text"));
				loadLogDetail.put("DATETIME", ((Date)row.getValue("date_created")).getTime());
				logList.add(loadLogDetail);
			}
		}

		return logList;
	}

	/**
	 * 根据ID查询load
	 */
	@RequestMapping(value = "/getWmsLoadById", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getWmsLoadById(@RequestParam(value = "load_id") String load_id)
			throws Exception {
		Map result = new HashMap();

		DBRow row = floorLoadMgr.getWmsLoad(load_id);

		Map load = null;

		if (row != null) {
			load = new HashMap();
			load.put("LoadNo", row.getString("load_no"));
			load.put("CarrierId", row.getString("carrier_id"));

			DBRow[] ordersByLoad = floorLoadMgr.getWmsOrdersByLoadId(load_id);
			load.put("OrderList", ordersByLoad);
		}

		return load;
	}

	/**
	 * 根据loadId查询masterOrder
	 */
	public List getMasterOrderListByLoadId(String load_id) throws Exception {
		DBRow[] ordersByLoad = floorLoadMgr.getMasterOrderByLoadId(load_id);
		// masterorder的属性、值对
		Map masterOrder = new HashMap();

		List masterOrderChildren = new ArrayList();
		List masterOrderList = new ArrayList();

		for (int i = 0; i < ordersByLoad.length; i++) {
			// 重新记录masterorder，初始化masterOrderChildren
			masterOrder = new HashMap();
			masterOrderChildren = new ArrayList();
			DBRow row = ordersByLoad[i];

			masterOrder.put("MASTERORDER", row.getString("master_order_id"));
			masterOrder.put("CONN", row.getString("customer_name")+" & "+row.getString("ship_to_address"));
			masterOrderChildren = getOrderListByMasterOrderId(row
					.getString("master_order_id"));

			// 添加上一个masterorder下的masterOrderList
			masterOrder.put("CHILDREN", masterOrderChildren);
			masterOrderList.add(masterOrder);
		}

		if (ordersByLoad.length > 2) {
			Map more = new HashMap();
			more.put("NAME", "more");
			more.put("PARA", "");
			more.put("TYPE", "href");
			more.put("VALUE", "More...");
			masterOrder.put("MORE", more);
		}

		return masterOrderList;
	}

	/**
	 * 根据MasterOrderId查询order
	 */
	public List getOrderListByMasterOrderId(String master_id) throws Exception {
		DBRow[] ordersByLoad = floorLoadMgr.getWmsOrderByMasterOrderId(master_id);
		Map<String, Object> order = null;

		SimpleDateFormat sdf=new SimpleDateFormat("MM/dd/yyyy HH:mm");
		SimpleDateFormat toDate=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		List masterOrderList = new ArrayList();
		for (int i = 0; i < ordersByLoad.length; i++) {
			order = new HashMap<>();
			DBRow row = ordersByLoad[i];

			// 组织单个order的属性、值对
			order.put("ORDERNO", row.getString("order_no"));

			List orderDetail = new ArrayList();

			Map orderDetailMap = new HashMap();
			orderDetailMap.put("LABLE", "Pallet Qty");
			orderDetailMap.put("VALUE", row.getString("pallets"));
			orderDetail.add(orderDetailMap);

			orderDetailMap = new HashMap();
			orderDetailMap.put("LABLE", "HUB");
			orderDetailMap.put("VALUE", row.getString("company_name"));
			orderDetail.add(orderDetailMap);

			orderDetailMap = new HashMap();
			orderDetailMap.put("LABLE", "MABD");
			
			int ps_id=Integer.parseInt(row.getString("send_psid"));
			String mabd=row.getString("mabd");
			if (mabd!=null && mabd.length()>=19) {
				try {
					//Map detail = new HashMap();
					//detail.put("LABLE", "MABD");
					orderDetailMap.put("VALUE", CommonUtil.dateToUTC(row, "mabd", ps_id).substring(0, 10));
					//orderDetailMap.add(detail);
				} catch (Exception e){
					orderDetailMap.put("VALUE", "");
				}
			} else {
				orderDetailMap.put("VALUE", "");
			}
			/*
			if(madb!=null && madb.trim().length()>0){
				Date date=toDate.parse(madb);
				madb=toDate.format(date);
				date=DateUtil.locationZoneTime(madb, ps_id);
				madb=sdf.format(date);
				if(madb.endsWith("00:00"))
					orderDetailMap.put("VALUE", madb.substring(0, madb.length()-5));
				else
					orderDetailMap.put("VALUE", madb);
			}else{
				orderDetailMap.put("VALUE", "");
			}*/
			orderDetail.add(orderDetailMap);

			order.put("CHILDREN", orderDetail);

			masterOrderList.add(order);
		}

		return masterOrderList;
	}

	/**
	 * 传入load和order的对应关系
	 * 
	 * 新建load，新建master order，新建关系表
	 */
	@RequestMapping(value = "/addLoad", produces = "application/json; charset=UTF-8")
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map addLoad(@RequestBody String data, HttpSession session)
			throws Exception {
		Map result = new HashMap();
		Map<String, Object> adminLogin = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		String loginUserName = (String) adminLogin.get("employe_name");

		Map<String,Object> loadMap = (Map<String,Object>) JSON.parse(data);
		String loadId=(String)loadMap.get("id");
		URLDecoder decoder=new URLDecoder();
		String loadNo = (String) loadMap.get("loadNo");
		String carrId=(String)loadMap.get("carrierId");
		
		if(StrUtil.isBlankNullUndefined(carrId)){
			result.put("result", "-1");
            result.put("error", "Cannot leave blank, must enter a valid SCAC.");
			return result;
		}
		
		if(loadId==null ||loadId.trim().length()==0){ //新增loadno
			DBRow check = floorLoadMgr.checkLoadNo(loadNo,carrId);
			if ( (check != null && check.getString("count") != null
					&& Integer.parseInt(check.getString("count")) > 0)) {
				result.put("result", "-1");
				result.put("error", "Load No. is repeated!");
				return result;
			}
	
			long newLoadId = floorLoadMgr.getOrInsterLoadNo("sync", loadNo, loginUserName,carrId);
			
			result.put("result", "0");
			result.put("loadId", newLoadId);
			return result;
		} else {
			//add by wangcr 2015/04/08 判断 load no 是否已经使用
			DBRow lrow = floorLoadMgr.existsLoadNo(loadNo, Long.parseLong(loadId));
			String sessionid = session.getId();
			if (lrow.get("cnt", 0)>0) {
				result.put("result", "-1");
	            result.put("error", "Load No. is repeated!");
				return result;
			}
			
			floorLoadMgr.updateLoad(Long.parseLong(loadId),loadNo,carrId,loginUserName);
			//SyncWMS
			DBRow[] orders = floorLoadMgr.findOrderList(Long.valueOf(loadId));
			for (DBRow ordern : orders) {
				 RestUtil.SyncCall(sessionid, Sync10IP, ordern.get("order_id", 0L), null, carrId, loadNo);
			}
			result.put("result", "0");
			return result;
		}
	}
	
	@RequestMapping(value = "/addLoadOrder", produces = "application/json; charset=UTF-8")
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map addLoadOrder(@RequestParam("id")long id,@RequestParam("orderList")List<String> orderList, HttpSession session)
			throws Exception {
		Map result = new HashMap();
		Map<String, Object> adminLogin = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		DBRow loadm = floorLoadMgr.getWmsLoad(String.valueOf( id));
		String loginUserName = (String) adminLogin.get("employe_name");
		floorLoadMgr.updateLoad(id, orderList, loginUserName);
		//更新order appt
		String aid=floorAppointmentMgr.getCurrentAid("load",id+"");
		if(!StringUtils.isEmpty(aid)){
			for(String oid:orderList){
				floorLoadMgr.SyncAppt(id,oid,false);
			}
		}
		//验证是否取消order的预约
		floorLoadMgr.checkOrders(orderList, loginUserName);
		String sessionid = session.getId();
		String atime = loadm.get("appointment_time", "");
		
		RestUtil.SyncCall(sessionid, Sync10IP, orderList, loadm.get("carrier_id", ""), null, loadm.get("load_no", ""));
		result.put("result", "0");
		return result;
	}
	
	@RequestMapping(value = "/delLoadOrder", produces = "application/json; charset=UTF-8")
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map delLoadOrder(@RequestParam("id")long id,@RequestParam("orderList")List<String> orderList, HttpSession session)
			throws Exception {
		Map result = new HashMap();
		delLMObyOrders(orderList, id);
		modWmsLoad(id);
		String sessionid = session.getId();
		RestUtil.SyncCall(sessionid, Sync10IP, orderList, "","", "");
		result.put("result", "0");
		return result;
	}

	public void delLMObyOrders(List orderIds, Long id) throws Exception {
		try {
			if (orderIds != null) {
				for (int i = 0; i < orderIds.size(); i++) {
					floorLoadMgr.delLoadOrderMaster(Integer.parseInt(orderIds.get(i).toString()), id);
				}
			}
		} catch (Exception ex) {
			throw new Exception("delLMObyOrders(row) orderIds:" + ex);
		}
	}
	
	public void modWmsLoad(long load_id) throws Exception{
		DBRow [] rows=floorLoadMgr.findOrderList(load_id);
		
		if(rows==null || rows.length==0){
			DBRow row=new DBRow();
			row.put("mabd", null);
			row.put("req_ship_date", null);
			row.put("freight_type", "");
			row.put("freight_term", "");
			floorLoadMgr.modWmsLoad(load_id,row);
			return;
		}
		
		Set<String> set=new HashSet<String>();
		for(DBRow row:rows){
			set.add(row.getString("order_id"));
		}
		
		DBRow row=floorLoadMgr.getB2bOrder(set);
		floorLoadMgr.modWmsLoad(load_id,row);
	}
	
	/**
	 * 修改Load
	 */
	@RequestMapping(value = "/editLoad", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map editWmsLoad(@RequestParam(value = "data") String data,
			HttpSession session) throws Exception {

		Map<String, Object> adminLogin = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);
		String loginUserName = (String) adminLogin.get("employe_name");

		Map load = (Map) JSON.parse(data);
		String loadNo = (String) load.get("loadNo");
		String carrId=(String)load.get("carrierId");

		List orderList = (List) load.get("orderList");

		List<String> orderIds = new ArrayList<String>();
		for (int i = 0; i < orderList.size(); i++) {
			Map order = (Map) orderList.get(i);
			orderIds.add((String) order.get("order_id"));
		}
		
		floorLoadMgr.syncLoad("", loadNo, orderIds, loginUserName,carrId);
		String sessionid = session.getId();
		RestUtil.SyncCall(sessionid, Sync10IP, orderList, carrId, "", null);
		Map result = new HashMap();
		result.put("result", "0");
		return result;
	}

	/**
	 * 删除Load
	 */
	@RequestMapping(value = "/delLoad", produces = "application/json; charset=UTF-8")
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map delWmsLoad(@RequestParam(value = "load_id") String loadId,
			HttpSession session) throws Exception {
		String sessionid = session.getId();
		try{
			Map result = new HashMap();
			// 先根据load_id判断有没有appointment关联了此load
			if(floorLoadMgr.checkLoadCanDel(loadId)){
				result.put("result", "-2");
				result.put("error", "Cannot delete, Pick Up Appointment has been scheduled!");
			} else {
				if(loadId != null && !loadId.equals("")){
					// 1. 根据load_id删除相关联的masterOrder
					DBRow[] orders = floorLoadMgr.findOrderList(Long.valueOf(loadId));
					floorLoadMgr.delMasterOrderByLoadId(loadId);
					
					// 2. 根据loadId删除关联关系
					floorLoadMgr.delLoadOrderMasterByLoadId(loadId);
					// 3. 根据loadId删除load
					floorLoadMgr.delLoad(loadId);
					for (DBRow ordern : orders) {
						 RestUtil.SyncCall(sessionid, Sync10IP, ordern.get("order_id", 0L), null, "", "");
					}
					result.put("result", "0");
				} else {
					result.put("result", "-1");
                    result.put("error", "Cannot leave blank, must enter a valid Load No.");
				}
			}
			
			return result;
		} catch (Exception ex) {
			throw new Exception("delWmsLoad(load_id):" + ex);
		}
	}

	/**
	 * 新增load，order，master关联记录
	 */
	public Map<String, Object> addLoadOrderMaster(long load_id, String load_no,
			String order_id, String master_id, HttpSession session)
			throws Exception {
		Map<String, Object> adminLogin = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);

		String loginUserName = (String) adminLogin.get("employe_name");

		floorLoadMgr.addLoadOrderMaster(load_id, load_no, order_id, master_id,
				loginUserName);

		Map<String, Object> result = new HashMap<String, Object>();
		result.put("result", "0");
		return result;
	}

	/**
	 * 根据LoadNo查询Load-Order列表
	 */
	@RequestMapping(value = "/getOrderDetailByLoadNo", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public DBRow[] getOrderDetailByLoadNo(
			@RequestParam(value = "load_no") String load_no) throws Exception {
		return floorLoadMgr.getMasterAndOrderByLoadNo(load_no);
	}

	/**
	 * 分页查询Order
	 */
	@RequestMapping(value = "/getWmsOrderByComCusPoReq", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getWmsOrderByComCusPoReq(
			@RequestParam(value = "0[customer_id]", required = false) String customer_id,
			@RequestParam(value = "0[company_id]", required = false) String company_id,
			@RequestParam(value = "0[order_no]", required = false) String order_id,
			@RequestParam(value = "0[req_start]", required = false) String req_start,
			@RequestParam(value = "0[req_end]", required = false) String req_end,
			@RequestParam(value = "0[checkIds]", required = false) String checkIds,
			@RequestParam(value = "page", defaultValue = "0", required = false) int page,
			@RequestParam(value = "rows", defaultValue = "10", required = false) int pageSize)
			throws Exception {

		Map result = new HashMap();

		PageCtrl pc = new PageCtrl();
		pc.setPageNo(safeInt(page));
		pc.setPageSize(safeInt(pageSize));
		
        String ordno = Pattern.matches("^B(\\d)+$", order_id)?order_id.replaceFirst("B", ""):order_id;

		DBRow[] rows = floorLoadMgr.getWmsOrderByComCusPoReq(customer_id,
				company_id, ordno, req_start, req_end, checkIds, pc);
		
		for(DBRow row:rows){
			long mabd=CommonUtil.format(row, "mabd", Long.parseLong(row.getString("send_psid")));
			long b2bOid=Long.parseLong(row.getString("order_id"));
			row.put("mabd", mabd);
			DBRow [] pos=b2bOrderItemDao.findByOid(b2bOid);
			Set<String> poSet=new HashSet<String>();
			for(DBRow data:pos){
				String po=data.getString("retail_po","");
				poSet.add(po);
			}
			row.put("po", poSet);
		}
		
		result.put("total", pc.getAllCount());
		result.put("rows", rows);

		return result;
	}

	public String listToString(List<String> stringList) {
		if (stringList == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		boolean flag = false;
		for (String string : stringList) {
			if (flag) {
				result.append(",");
			} else {
				flag = true;
			}
			result.append(string);
		}
		return result.toString();
	}

	/**
	 * company列表
	 */
	@RequestMapping(value = "/companyList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public DBRow[] getCompanyList() throws Exception {
		return floorLoadMgr.getCompanyList();
	}

	/**
	 * customer列表
	 */
	@RequestMapping(value = "/customerList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public DBRow[] getCustomerList() throws Exception {
		return floorLoadMgr.getCustomerList();
	}

	private int safeInt(Object v) {
		return (v == null) ? 0 : Integer.valueOf(v.toString());
	}

	
	private Long safeLong(Object v) {
		return (v == null) ? 0L : Long.valueOf(v.toString());
	}

	private Boolean safeBoolean(Object v) {
		return (v == null) ? false : (Boolean) v;
	}

	private String safeString(Object v) {
		return (v == null) ? "" : (String) v;
	}

	private List addToList(DBRow row, List list, String lable, String v) {
		String value = row.getString(v);
		if (value != null && !"".equals(value)) {
			Map detail = new HashMap();
			detail.put("LABLE", lable);
			detail.put("VALUE", value);
			list.add(detail);
		}
		return list;
	}
	
	/**
	 * 修改load下的masterbol
	 */
	@RequestMapping(value = "/modMasterBol", produces = "application/json; charset=UTF-8",method = RequestMethod.POST)
	@Transactional (propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT,readOnly=false,rollbackFor=Exception.class)
	public Map modMasterBol(@RequestParam("loadId")String load_id,
			@RequestParam("oids[]")List<String> oids,
			@RequestParam("zipCode")String zipcode,
			@RequestParam("address")String address,
			@RequestParam("city")String city,
			@RequestParam("state")String state,
			@RequestParam("country")String country,
			@RequestParam("cid")String cid,@RequestParam("hub")String hub, HttpSession session) throws Exception {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", "200");
		result.put("msg", "success");
		
		if(oids.size()==0){
			return result;
		}
		
		String oidsStr="";
		
		for(String oid:oids){
			oidsStr+=oid+",";
		}
		oidsStr=oidsStr.substring(0,oidsStr.length()-1);
		
		DBRow [] datas=floorWMSOrdersMgr.checkOrder(oidsStr);
		
		if(datas.length!=1){
			result.put("code", "500");
            result.put("msg", "Create masterbol order abnormal");
			
			return result;
		}else{
			DBRow r=datas[0];
			if(!r.getString("freight_carrier").equalsIgnoreCase("Consol")){
				result.put("code", "500");
                result.put("msg", "Create masterbol order must be Consol");
				
				return result;
			}
		}
		
		DBRow masterRow = new DBRow();
		masterRow.put("load_id", load_id);
		masterRow.put("ship_to_address", address);
		masterRow.put("ship_to_zipcode", zipcode);
		masterRow.put("ship_to_city", city);
		masterRow.put("ship_to_state", state);
		masterRow.put("ship_to_county", country);
		masterRow.put("customer_id", cid);
		masterRow.put("company_id", hub);
		masterRow.put("date_created", new Date());
		DBRow row=floorWMSOrdersMgr.getTotalPallets(oidsStr);
		BigDecimal pallets=(BigDecimal)row.getValue("total_pallets");
		masterRow.put("total_pallets", pallets.floatValue());
		
		DBRow mbl=floorLoadMgr.findMasterBol(cid,address,load_id);
		long masterId=0;
		if(mbl!=null){
			masterId=Long.parseLong(mbl.getString("master_order_id"));
			masterRow.remove("date_created");
			floorLoadMgr.updateMasterOrder(masterRow,masterId);
		}else{
			//新建masterbol
			masterId = floorLoadMgr.addMasterOrder(masterRow);
		}
		//更新order和masterbol的关系
		floorLoadMgr.modMasterId(oidsStr,masterId,Integer.parseInt(load_id));
		
		//删除老的masterbol
		floorLoadMgr.delMasterOrderByOids(load_id);
		String sessionid = session.getId();
		RestUtil.SyncCall(sessionid, Sync10IP, oids, "", "", "");
		return result;
	}

	@RequestMapping(value = "/getMasterBolByLoadId", produces = "application/json; charset=UTF-8",method = RequestMethod.POST)
	public Map getMasterBolByLoadId(String load_id) throws Exception {
		Map result = new HashMap();

		DBRow row = floorLoadMgr.getWmsLoad(load_id);

		Map load = null;

		if (row != null) {
			load = new HashMap();
			load.put("LoadNo", row.getString("load_no"));
			load.put("CarrierId", row.getString("carrier_id"));
			
			DBRow[] ordersByLoad = floorLoadMgr.getMasterBolByLoadId(load_id);
			for(DBRow data:ordersByLoad){
				long mabd=CommonUtil.format(data, "mabd", Long.parseLong(data.getString("send_psid")));
				long b2bOid=Long.parseLong(data.getString("order_id"));
				data.put("mabd", mabd);
				DBRow [] pos=b2bOrderItemDao.findByOid(b2bOid);
				Set<String> poSet=new HashSet<String>();
				for(DBRow r:pos){
					String po=r.getString("retail_po","");
					poSet.add(po);
				}
				data.put("po", poSet);
			}
			load.put("OrderList", ordersByLoad);
		}

		return load;
	}

}
