package com.oso.load.controller;

import java.io.IOException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.floor.api.zyj.FloorB2BOrderMgrZyj;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.javaps.springboot.SessionAttr;
import com.mongodb.util.JSON;
import com.oso.utils.RestUtil;

@RestController
@Transactional
public class WmsAppointmentWriteController
{
	@Value("${Sync10IP}")
	private String Sync10IP;

	@Autowired
	private FloorSyncLoadRelationMgr floorLoadMgr;
	
	@Autowired
	private FloorB2BOrderMgrZyj floorWMSOrdersMgr;

	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	@Autowired
	private HttpComponentsClientHttpRequestFactory clientHttpRequestFactory;
	
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/addAppointment", produces = "application/json; charset=UTF-8")
	public Map<String,Object> addOrUpdateAppointment(@RequestBody String data, HttpSession session) throws Exception
	{
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
		Map<String, Object> adminLogin = (Map<String, Object>) session.getAttribute(Config.adminSesion);
		String loginUserName = (String) adminLogin.get("employe_name");
		String uid=String.valueOf((Integer)adminLogin.get("adid"));
		//Object depts = adminLogin.get("department");
		
		String sessionid = session.getId();
		// String to json
		Map<String,Object> map = (Map<String,Object>)JSON.parse(data);
		
		
		String eta = "";//map.get("eta").toString();
		String etd = "";//map.get("etd").toString();
		String appointment_time = map.get("appointment_time").toString();
		
		DBRow row = new DBRow();
		row.putAll(map);
		row.remove("invoices");
		
		String scac=row.getString("carrier_id");
		List<Map<String,Object>> invoices = (List<Map<String,Object>>)map.get("invoices");
		row.put("driver_license", URLDecoder.decode(map.get("driver_license").toString(), "UTF-8"));
		row.put("driver_name", URLDecoder.decode(map.get("driver_name").toString(), "UTF-8"));
		row.put("storage_name", URLDecoder.decode(map.get("storage_name").toString(), "UTF-8"));
		row.put("storage_linkman", URLDecoder.decode(map.get("storage_linkman").toString(), "UTF-8"));
		row.put("carrier_name", URLDecoder.decode(map.get("carrier_name").toString(), "UTF-8"));
		row.put("carrier_linkman", URLDecoder.decode(map.get("carrier_linkman").toString(), "UTF-8"));
		row.put("licenseplate", URLDecoder.decode(map.get("licenseplate").toString(), "UTF-8"));
		
		// 快速创建时，仓库ID是空，查询session里登录人所属仓库填充进去
		String ps_id = null;
		if(map.get("storage_id")==null || map.get("storage_id").toString().equals("")){
			ps_id = String.valueOf(adminLogin.get("ps_id"));
			row.put("storage_id", ps_id);
			
			String storage_name = (String) adminLogin.get("storage_name");
			row.put("storage_name", storage_name);
			
			DBRow loginStorageCatalog = floorAppointmentMgr.getLoginStorageCatalogById(ps_id);
			row.put("storage_linkman", loginStorageCatalog.getString("contact"));
			row.put("storage_linkman_tel", loginStorageCatalog.getString("phone"));
		} else {
			ps_id = row.getString("storage_id");
		}
		//转换成UTC时间
		appointment_time = format1.format(format3.parse(DateUtil.showUTCTime(format1.parse(appointment_time), Long.parseLong(ps_id))));
		
		String aid = "";
		if( map.get("id")!=null){
			aid = map.get("id").toString();
		}
		//判断是否仓库主管，主管不受限制。
		boolean superw = false;
		superw = DBRowUtils.existDeptAndPostInLoginBean(1000008, 10L, DBRowUtils.getAdminLoginBean(adminLogin));
		
		if(map.get("id")==null || map.get("id").toString().equals("") || map.get("id").toString().equals("undefined")){// add
			map.remove("id");
			row.put("user_created", loginUserName);
			row.put("date_created", new Date());
			row.put("status", "Open");
			row.put("appointment_time", format1.parse(appointment_time));
			
			// 格式化时间类型
			if(!"".equals(eta))
				row.put("eta", format3.parse(DateUtil.showUTCTime(format1.parse(eta), Long.parseLong(ps_id))));
			else 
				row.remove("eta");
			if(!"".equals(etd))
				row.put("etd", format3.parse(DateUtil.showUTCTime(format1.parse(etd), Long.parseLong(ps_id))));
			else 
				row.remove("etd");
			row.remove("id");
			// 新增appointment，返回aid
			String appointment = format2.format(format1.parse(appointment_time));
			String atype = row.getString("appointment_type", "");
			
			boolean b = false;
			
			if (superw) {
				if ("inbound".equals(atype)) {
					floorAppointmentMgr.sumOneWorkInNoLimit(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
				if ("outbound".equals(atype)) {
					floorAppointmentMgr.sumOneWorkOutNoLimit(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
			} else {
				if ("inbound".equals(atype)) {
					b = floorAppointmentMgr.sumOneWorkIn(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				} 
				
				if ("outbound".equals(atype)) {
					b = floorAppointmentMgr.sumOneWorkOut(appointment.substring(0,10), Integer.parseInt(appointment.substring(11,13)), row.get("storage_id",0L), null,true);
				}
				if (!b) throw new Exception("all the appointments have been taken!"+ map.get("appointment_time").toString());
			}
			row.remove("key");
			aid = ""+floorAppointmentMgr.addApp(row);
			floorAppointmentMgr.addAppLog("APPT", "", "", aid, loginUserName,"Create Appointment");
			// 保存关联单据
			if(invoices != null){
				for(Map<String,Object> invoice : invoices){
					String id = invoice.get("id").toString();
					String type = invoice.get("type").toString();
					//新增关联单据
					floorAppointmentMgr.addAppInvoice(aid, type, id,  uid);
					if ("load".equals(type)) {
						this.updateLoad(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
					}
					if ("order".equals(type)) {
						this.updateOrder(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
					}
					if("rn".equalsIgnoreCase(type)){
						RestUtil.saveAppointmentById(sessionid,Sync10IP,id,appointment+":00",scac);
					}
					floorAppointmentMgr.addAppLog(type, id, "", aid, loginUserName,"Modified Appointment");
				}
			}
		} else { // edit
			row.put("date_updated", new Date());
			row.put("appointment_time", format1.parse(appointment_time));
			String nappointment = format2.format(format1.parse(appointment_time));
			
			long newtime = format1.parse(appointment_time).getTime();
			
			DBRow old = floorAppointmentMgr.getAppByAID(aid);
			long oldtime = format3.parse(old.getString("appointment_time")).getTime();
			String atype = row.getString("appointment_type", "");
			String oappointment = format2.format(format3.parse(old.getString("appointment_time")));
			boolean b = false;
			if (newtime!=oldtime || !old.getString("appointment_type").equals(atype)) {
				//取消原先
				String oldtype = old.getString("appointment_type");
				if ("inbound".equals(oldtype)) {
					floorAppointmentMgr.subOneWorkIn(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
				if ("outbound".equals(oldtype)) {
					floorAppointmentMgr.subOneWorkOut(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
				}
				//增加预约
				if (superw) {
					if ("inbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkInNoLimit(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
					if ("outbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkOutNoLimit(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
				} else {
					if ("inbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkIn(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
					if ("outbound".equals(atype)) {
						b = floorAppointmentMgr.sumOneWorkOut(nappointment.substring(0,10), Integer.parseInt(nappointment.substring(11,13)), row.get("storage_id",0L), null,true);
					}
				}
				if (!b) throw new Exception("all the appointments have been taken!"+ map.get("appointment_time").toString());
				if(newtime!=oldtime){
//					String otime=DateUtil.showLocalTime(oappointment+":00", Long.parseLong(ps_id));
//					otime=format1.format(format3.parse(otime));
					floorAppointmentMgr.addAppLog("APPT", "", "", aid, loginUserName,"Modified Appointment");
				}
			}
			
			// 格式化时间类型
			if(!"".equals(eta)){
				row.put("eta", format3.parse(DateUtil.showUTCTime(format1.parse(eta), Long.parseLong(ps_id))));
			}
			else {
				row.remove("eta");
			}
			if(!"".equals(etd)){
				row.put("etd", format3.parse(DateUtil.showUTCTime(format1.parse(etd), Long.parseLong(ps_id))));
			}
			else {
				row.remove("etd");
			}
			//修改预约日期需要取消原预约，新增新预约。
			
			//if (old.get("appointment_time")=="")
			floorAppointmentMgr.updateApp(row);

			// 现有的关联单据
			DBRow[] oldInvoice = floorAppointmentMgr.getInvoiceByAID(aid);
			// 保存关联单据，比较新旧数据，去掉已经删除的，增加新增的
			if(invoices != null){
				String idTypeStr = "1";
				if (invoices.size()>0) {
					List<String> ids = new ArrayList<String>();
					for(Map<String,Object> invoice : invoices){
						String id = invoice.get("id").toString();
						String type = invoice.get("type").toString();
						idTypeStr += (",'"+id+type+"'");
						ids.add(id+":"+type);
						// 如果没有这个表单，添加
						if(!checkInvoice(oldInvoice, id, type)){
							// 查询当前的aid
							String aidFrom = floorAppointmentMgr.getCurrentAid(type, id);
							floorAppointmentMgr.addAppInvoice(aid, type, id,  String.valueOf(adminLogin.get("adid")));
							//更新order appt or Load 的order
							
							// 查询添加后的aid
							String aidTo = floorAppointmentMgr.getCurrentAid(type, id);
							// 写变化日志
							floorAppointmentMgr.addAppLog(type, id, aidFrom, aidTo, loginUserName,"Modified Appointment");
						}
						if ("load".equals(type)) {
							this.updateLoad(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
						} else if ("order".equals(type)) {
							this.updateOrder(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
						} else if("rn".equalsIgnoreCase(type)){
							RestUtil.saveAppointmentById(sessionid,Sync10IP,id,nappointment+":00",scac);
						}
						
					}
					for (DBRow item: oldInvoice) {
						String id = item.get("invoice_id","");
						String type = item.get("invoice_type","");
						if (!ids.contains(id+":"+type)) {
							if ("load".equals(type)) {
								this.updateLoad(Long.parseLong(id), null, null, sessionid);
							}
							if ("order".equals(type)) {
								this.updateOrder(Long.parseLong(id), null, null, sessionid);
							}
							if("rn".equalsIgnoreCase(type)){
								RestUtil.delAppointmentById(sessionid,Sync10IP,id);
							}
						}
					}
					
				} else {
					for (DBRow item: oldInvoice) {
						String id = item.get("invoice_id","");
						String type = item.get("invoice_type","");
						if ("load".equals(type)) {
							this.updateLoad(Long.parseLong(id), null, null, sessionid);
						}
						if ("order".equals(type)) {
							this.updateOrder(Long.parseLong(id), null, null, sessionid);
						}
						if("rn".equalsIgnoreCase(type)){
							RestUtil.delAppointmentById(sessionid,Sync10IP,id);
						}
					}
				}
				floorAppointmentMgr.delInvoiceByAidAndNotInList(aid, idTypeStr, loginUserName);
			}
		}
		} catch(Exception e) {
			e.printStackTrace();
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("result", "1");
			result.put("message", e.getMessage());
			return result;
		}
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("result", "0");
		return result;
	}
	
	/**
	 * 使用预约条保存预约信息
	 * 
	 * @param data
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/saveAppointment", produces = "application/json; charset=UTF-8")
	public Map<String,Object> saveAppointment (@RequestBody String data, HttpSession session) throws Exception {		
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String sessionid = session.getId();
		try {
			Map<String, Object> adminLogin = (Map<String, Object>) session.getAttribute(Config.adminSesion);
			String loginUserName = (String) adminLogin.get("employe_name");
			String uid=String.valueOf((Integer)adminLogin.get("adid"));
			// String to json
			Map<String, Object> map = (Map<String, Object>)JSON.parse(data);
			
			String ps_id = String.valueOf(map.get("storage_id"));
			String key = String.valueOf(map.get("key")==null?"":map.get("key"));
			map.remove("key");
			
			DBRow row = new DBRow();
			row.putAll(map);
			row.remove("invoices");
			Object del_window = map.get("del_window");
			if (del_window==null) del_window = "";
			row.remove("del_window");
			
			String scac= row.get("carrier_id","");
			List<Map<String, Object>> invoices = (List<Map<String, Object>>) map.get("invoices");
			row.put("driver_license", URLDecoder.decode(map.get("driver_license").toString(), "UTF-8"));
			row.put("driver_name", URLDecoder.decode(map.get("driver_name").toString(), "UTF-8"));
			row.put("storage_name", URLDecoder.decode(map.get("storage_name").toString(), "UTF-8"));
			row.put("storage_linkman", URLDecoder.decode(map.get("storage_linkman").toString(), "UTF-8"));
			row.put("carrier_name", URLDecoder.decode(map.get("carrier_name").toString(), "UTF-8"));
			row.put("carrier_linkman", URLDecoder.decode(map.get("carrier_linkman").toString(), "UTF-8"));
			row.put("licenseplate", URLDecoder.decode(map.get("licenseplate").toString(), "UTF-8"));
			if (map.containsKey("carrier_email"))
				row.put("carrier_email", URLDecoder.decode(map.get("carrier_email").toString(), "UTF-8"));
			
			String atype = row.getString("appointment_type", "");
			String appointment_time = map.get("appointment_time").toString();
			String appointment = format2.format(format1.parse(appointment_time));
			
			//row.remove("KEY");
			if (key==null || "".equals(key)) throw new Exception("No Appointment Key");
			
			String appointment_time1 = format2.format(format3.parse(DateUtil.showUTCTime(format1.parse(appointment_time), Long.parseLong(ps_id))));
			appointment_time = format1.format(format3.parse(DateUtil.showUTCTime(format1.parse(appointment_time), Long.parseLong(ps_id))));
			try {
				//floorAppointmentMgr.clearOutTimeTicket(null);
			} catch (Exception e) {}
			boolean b = floorAppointmentMgr.useTicket(ps_id, appointment_time1.substring(0,10), Integer.parseInt(appointment_time1.substring(11,13)) , atype, key);
			if (b) {
				row.put("user_created", loginUserName);
				row.put("date_created", new Date());
				row.put("status", "Open");
				row.put("appointment_time", format1.parse(appointment_time));
				row.remove("eta");
				row.remove("etd");
				row.remove("id");
				String aid = ""+floorAppointmentMgr.addApp(row);
				
				floorAppointmentMgr.addAppLog("APPT", "", "", aid, loginUserName,"Create Appointment");
				// 保存关联单据
				if(invoices != null){
					for(Map<String, Object> invoice : invoices){
						String id = invoice.get("id").toString();
						String type = invoice.get("type").toString();
						//新增关联单据
						long aiid = floorAppointmentMgr.addAppInvoice(aid, type, id,  uid);
						invoice.put("aiid", aiid);
						if ("load".equals(type)) {
							this.updateLoad(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
						}
						if ("order".equals(type)) {
							this.updateOrder(Long.parseLong(id), appointment_time, row.get("carrier_id",""), sessionid);
						}
						if("rn".equalsIgnoreCase(type)){
							RestUtil.saveAppointmentById(sessionid,Sync10IP,id,appointment_time1+":00",scac);
						}
						floorAppointmentMgr.addAppLog(type, id, "", aid, loginUserName,"Modified Appointment");
					}
				}
				//send Email
				if (!"".equals(row.get("carrier_email", ""))) {
					this.sendMail(row, aid, appointment, session.getId());
				}
			} else {
				//无key
				throw new Exception("Appointment Key Error");
			}
		} catch (Exception e) {
			e.printStackTrace();
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("result", "1");
			result.put("message", e.getMessage());
			return result;
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("result", "0");
		return result;
	}
	
	/**
	 * 给carrier发邮件
	 * 
	 * @param row
	 */
	private void sendMail(DBRow row, String aid, String appointment, String JSESSIONID) {
		HttpClient hc = clientHttpRequestFactory.getHttpClient();
		//log.info(JSESSIONID);
		
		HttpPost hp = new HttpPost(String.format("http://%s/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=sendmail", Sync10IP));
		hp.addHeader("Accept", "application/json");
		hp.addHeader("Cache-Control","no-cache");
		hp.addHeader("Pragma","no-cache");
		if (JSESSIONID!=null && !"".equals(JSESSIONID)) {
			hp.addHeader("Cookie","JSESSIONID="+JSESSIONID+"; path=/Sync10");
		}
		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("email", row.get("carrier_email", "")));
        nvps.add(new BasicNameValuePair("html", getEmailHtml(row, aid, appointment)));
		try {
			hp.setEntity(new UrlEncodedFormEntity(nvps));
			hc.execute(hp);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String getEmailHtml(DBRow row, String aid, String appointment) {
		Map<String,Object> lables = new HashMap<String,Object>();
		lables.put("Del.Window", "Appointment");
		lables.put("carrier_id", "SCAC");
		lables.put("carrier_linkman", "Contacts");
		lables.put("carrier_linkman_tel", "Phone");
		lables.put("licenseplate", "License Plate");
		lables.put("driver_license", "Driver License");
		lables.put("driver_name", "Driver Name");
		
		lables.put("storage_name", "Hub");
		lables.put("appointment_type", "Type");
		lables.put("storage_linkman", "Contacts");
		lables.put("storage_linkman_tel", "Phone");
		
		
		StringBuffer sf = new StringBuffer("");
		sf.append("<!DOCTYPE html>")
		.append("<html>")
		.append("<head>")
		.append("<meta charset=\"utf-8\" />")
		.append("<title>Appointment Info</title>")
		.append("<style type=\"text/css\">")
		.append("body { font-family: \"Helvetica Neue\",Helvetica,Arial,sans-serif; font-size: 14px; line-height: 1.42857143; color: #333; background-color: #fff; margin: 0}")
		.append("* { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box;}")
		.append(".panel { margin-bottom: 20px; background-color: #fff; border: 1px solid transparent; border-radius: 4px; -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05); box-shadow: 0 1px 1px rgba(0,0,0,.05);}")
		.append(".panel-heading { color: #333; background-color: #f5f5f5; border-color: #ddd; padding: 10px 15px; border-bottom: 1px solid #ddd; border-top-left-radius: 3px; border-top-right-radius: 3px;}")
		.append(".panel-body { padding: 15px;}")
		.append("</style>")
		.append("<style type=\"text/css\">")
		.append("#preview { width: 640px; max-width: 40em; margin:1.5em auto; border: 1px solid #ddd;}")
		.append("#preview .preview-header { text-align: left; padding-top:0.75em; font-size: 16px; padding-left: 0.5em;}")
		.append("#preview .title { font-size: 12px; font-weight: 700; color: #959595; text-transform: uppercase; letter-spacing: 1px; margin-top:1em;}")
		.append(".linebox { background-color: #ddd; border: 0; height: 1px;}")
		.append("table.content { width:100%;}")
		.append(".content .name { width:25%; min-width:120px; text-align:right; padding-right:0.5em; height:2.5em; margin-bottom:1em;}")
		.append(".content .value { padding-left:0.5em; text-align:left;}")
		.append(".orderPanel { border-color: #ddd; width: 250px; margin: 0px 6px 10px 10px; float: left;}")
		.append(".orderPanel p span { min-width: 4em; margin-right: 0.5em; text-align: right; overflow: hidden; display: inline-block; *display: inline; zoom:1;}")
		.append(".orderPanel span.loadInfoStyle { text-align:left; margin-right:0; margin-left:0.5em;}")
		.append(".preview-foot td { text-align: right; padding: 1em 1em 1em 0;}")
		.append(".order-table { width: 90%; margin-bottom: 1.5em; background-color: #fff; border: 1px solid #ddd; border-spacing: 0; margin-left: 1em;}")
		.append(".order-table thead { background-color: #f5f5f5; border: 1px solid #ddd;}")
		.append(".order-table thead th { height: 2.5em; text-align: left; padding-left: 0.5em;}")
		.append(".order-table td { padding: 1em 1em 0.51em 0.5em;}")
		.append(".order-table .order-name { width: 20%; min-width: 5em; text-align: right;}")
		.append(".order-table .order-value { text-align: left;}")
		.append(".order-table .order-no { font-weight: normal; padding-left: 1em;}")
		.append("</style>")
		.append("</head>")
		.append("<body>")
		
		.append("<table id=\"preview\">")
		
		.append("<thead>")
		.append("	<th class=\"preview-header\">Hi ").append(strFilter(row.get("driver_name",""))).append(",<br/>Thanks for your appointment.</th>")
		.append("</thead>")
		.append("<tbody>")
		.append("	<tr><td><div class=\"title\">Driver Info</div><hr class=\"linebox\"/></td></tr>")
		.append("	<tr class=\"preview-block\">")
		.append("		<td>")
		.append("<table class=\"content\">")
		
		.append("<tr><td class=\"name\"><b>").append(lables.get("Del.Window")).append("</b></td><td class=\"value\">").append(strFilter(appointment)).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("carrier_id")).append("</b></td><td class=\"value\">").append(strFilter(row.get("carrier_id",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("carrier_linkman")).append("</b></td><td class=\"value\">").append(strFilter(row.get("carrier_linkman",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("carrier_linkman_tel")).append("</b></td><td class=\"value\">").append(strFilter(row.get("carrier_linkman_tel",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("licenseplate")).append("</b></td><td class=\"value\">").append(strFilter(row.get("licenseplate",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("driver_license")).append("</b></td><td class=\"value\">").append(strFilter(row.get("driver_license",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("driver_name")).append("</b></td><td class=\"value\">").append(strFilter(row.get("driver_name",""))).append("</td></tr>")
		
		.append("</table>")
		.append("		</td>")
		.append("	</tr>")
		.append("	<tr>")
		.append("		<td>")
		.append("			<div class=\"title\">Hub & Type</div>")
		.append("			<hr class=\"linebox\"/>")
		.append("		</td>")
		.append("	</tr>")
		.append("	<tr class=\"preview-block\">")
		.append("		<td>")
		.append("<table class=\"content\">")
		.append("<tr><td class=\"name\"><b>").append(lables.get("storage_name")).append("</b></td><td class=\"value\">").append(strFilter(row.get("storage_name",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("appointment_type")).append("</b></td><td class=\"value\">").append(strFilter(row.get("appointment_type",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("storage_linkman")).append("</b></td><td class=\"value\">").append(strFilter(row.get("storage_linkman",""))).append("</td></tr>")
		.append("<tr><td class=\"name\"><b>").append(lables.get("storage_linkman_tel")).append("</b></td><td class=\"value\">").append(strFilter(row.get("storage_linkman_tel",""))).append("</td></tr>")
		.append("</table>")
		.append("		</td>")
		.append("	</tr>")
		.append("	<tr>")
		.append("		<td>")
		.append("			<div class=\"title\">LR Info</div>")
		.append("			<hr class=\"linebox\"/>")
		.append("		</td>")
		.append("	</tr>")
		.append("	<tr class=\"preview-block\">")
		.append("		<td>");
		
		lables.put("City", "City:");
		lables.put("State", "State:");
		lables.put("plateNum", "Pallets:");
		
		DBRow[] invoices;
		try {
			invoices = floorAppointmentMgr.getInvoicesByaid(aid);
			for (DBRow invoice: invoices) {
				String rfno=invoice.getString("num","");
				String type = invoice.get("type").toString();
				DBRow[] citys=floorAppointmentMgr.getCityState(rfno);
				if(citys!=null){
					for(DBRow city:citys){
						invoice.putAll(city);
					}
				}
				sf.append("<table class=\"order-table\">")
				.append("	<thead><th colspan=\"2\"><span>").append(strFilter(type)).append("</span><span class=\"order-no\"> ").append(strFilter(invoice.get("num",""))).append("</span></th></thead>")
				.append("	<tbody>")
				.append("		<tr><td class=\"order-name\">").append(lables.get("City")).append("</td><td class=\"order-value\">").append(strFilter(invoice.get("city",""))).append("</td></tr>")
				.append("		<tr><td class=\"order-name\">").append(lables.get("State")).append("</td><td class=\"order-value\">").append(strFilter(invoice.get("state",""))).append("</td></tr>")
				.append("		<tr><td class=\"order-name\">").append(lables.get("plateNum")).append("</td><td class=\"order-value\">").append(strFilter(invoice.get("platenum",0f))).append("</td></tr>")
				.append("	</tbody>")
				.append("</table>");
			}
		} catch (Exception e) {
		}
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
		format1.setTimeZone(TimeZone.getTimeZone("PST"));
		sf.append("		</td>")
		.append("	</tr>")
		.append("	<tr><td><hr class=\"linebox\"/></td></tr>")
		.append("	<tr class=\"preview-foot\"><td>Logistics Team<br/>").append(format1.format(new Date())).append(" PST</td></tr>")
		.append("</tbody>")
		.append("</table>")
		.append("</body></html>");
		return sf.toString();
	}
	
	private String strFilter(Object obj) {
		if (obj==null) return "";
		return obj.toString().replaceAll("<", "&lt;").replaceAll(">", "&gt;");
	}
	
	/**
	 * 修改Order的预约时间
	 * 
	 * @param oid
	 * @param appttime 
	 * @return
	 * @throws Exception 
	 */
	private boolean updateOrder(long oid, String appttime, String scac, String sessionid) throws Exception {
		DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DBRow para = new DBRow();
		if (scac!=null &&  !"".equals(scac)) {
			para.add("carriers", scac);
		} else {
			para.add("carriers", null);
		}
		if(appttime==null || "".equals(appttime)) {
			para.add("pickup_appointment", null);
			appttime = "";
		} else {
			para.add("pickup_appointment", format1.parse(appttime));
		}
		floorWMSOrdersMgr.modB2BOrder(oid, para);
		RestUtil.SyncCall(sessionid, Sync10IP, oid, para.get("carriers", ""), appttime, null);
		return true;
	}
	/**
	 * 修改Order的预约时间
	 * @param load
	 * @param appttime
	 * @return
	 * @throws Exception
	 */
	private boolean updateLoad(long load, String appttime, String scac, String sessionid) throws Exception {
		DBRow row = floorLoadMgr.getWmsLoad(String.valueOf(load));
		//更新scac
		if (scac==null) {
			scac = row.get("carrier_id", "");
		} else {
			floorLoadMgr.updCarrier(load, scac);
		}
		DBRow[] orders = floorLoadMgr.getWmsOrdersByLoadId(String.valueOf(load));
		for (DBRow order: orders) {
			this.updateOrder(order.get("order_id", 0L), appttime, scac, sessionid);
		}
		return true;
	}
	
	private boolean checkInvoice(DBRow[] rows, String id, String type){
		if(rows == null){
			return false;
		} else {
			for(DBRow row: rows){
				String idA = row.getString("invoice_id");
				String typeA = row.getString("invoice_type");
				if(idA.equals(id) && typeA.equals(type)){
					return true;
				}
			}
			return false;
		}
	}
	
	/**
	 *  删除Appointment
	 *  
	 * @param aid
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/delAppointment", produces = "application/json; charset=UTF-8")
	public Map<String,Object> delAppointment(@RequestParam(value = "aid", required = true) String aid, HttpSession session) throws Exception {
		//DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
		DateFormat format2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		// 根据aid查询
		String sessionid = session.getId();
		try {
			DBRow old = floorAppointmentMgr.getAppByAID(aid);
			String oappointment = format2.format((Date)old.getValue("appointment_time"));
			String oldtype = old.getString("appointment_type");
			
			if ("inbound".equals(oldtype)) {
				floorAppointmentMgr.subOneWorkIn(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
			}
			if ("outbound".equals(oldtype)) {
				floorAppointmentMgr.subOneWorkOut(oappointment.substring(0,10), Integer.parseInt(oappointment.substring(11,13)), old.get("storage_id",0L), null,true);
			}
			//获取关联
			DBRow[] oldInvoice = floorAppointmentMgr.getInvoiceByAID(aid);
			floorAppointmentMgr.delAppInvoice(aid);
			
			for (DBRow item: oldInvoice) {
				//清除关联
				String id = item.get("invoice_id","");
				String type = item.get("invoice_type","");
				if ("load".equals(type)) {
					this.updateLoad(Long.parseLong(id), null, null, sessionid);
				}
				if ("order".equals(type)) {
					this.updateOrder(Long.parseLong(id), null, null, sessionid);
				}
				if("rn".equals(type)){
					RestUtil.delAppointmentById(sessionid,Sync10IP,id);
				}
			}
			floorAppointmentMgr.delApp(safeString(aid));
			
		} catch (Exception e) {
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("result", "1");
			result.put("message", e.getMessage());
			return result;
		}
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("result", "0");
		return result;
	}
	
	private String safeString(Object v) {
		return (v == null) ? "" : (String) v;
	}
	
	/**
	 * 生成预约请条
	 * 
	 * @param storageId
	 * @param date
	 * @param hour
	 * @param type
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/makeAppointment", produces = "application/json; charset=UTF-8")
	public Map<String,Object> makeAppointment(
		@RequestParam(value = "storageId", required = true) String storageId,
		@RequestParam(value = "date", required = true) String date,
		@RequestParam(value = "hour", required = true) String hour,
		@RequestParam(value = "type", required = true) String type,
		@SessionAttr(Config.adminSesion) Map<String,Object> session) throws Exception {
		
		int ihour = Integer.valueOf(hour);
		String utc = "";
		if (ihour>9) {
			utc = DateUtil.showUTCTime(date + " "+ihour+":00:00", Long.parseLong(storageId));
		} else {
			utc = DateUtil.showUTCTime(date + " 0"+ihour+":00:00", Long.parseLong(storageId));
		}
		String user = String.valueOf(session.get("adid"));
		
		DateFormat format3 = new SimpleDateFormat("yyyy-MM-dd HH:00:00");
		
		long t = DateUtil.getDate2LongTime(utc);
		long n = DateUtil.getDate2LongTime(format3.format(new Date()));
		if (t<n) {
			 throw new Exception("Time pass");
		}
		clearOutTimeTicket(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)));
		boolean superw = false;
		superw = DBRowUtils.existDeptAndPostInLoginBean(1000008, 10L, DBRowUtils.getAdminLoginBean(session));
		if (superw) {
			//主管突破限制
			if ("inbound".equals(type)) {
				floorAppointmentMgr.sumOneWorkInNoLimit(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), null,true);
			}
			if ("outbound".equals(type)) {
				floorAppointmentMgr.sumOneWorkOutNoLimit(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), null,true);
			}
		} else {
			if ("inbound".equals(type)) {
				boolean b = floorAppointmentMgr.sumOneWorkIn(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
				if (!b) throw new Exception("All the appointments have been taken!");
			} else if ("outbound".equals(type)) {
				boolean b = floorAppointmentMgr.sumOneWorkOut(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
				if (!b) throw new Exception("All the appointments have been taken!");
			}
		}
		
		String uid = floorAppointmentMgr.makeTicket(storageId, utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), type, user);
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("status", "200");
		result.put("key", uid);
		result.put("message", "");
		return result;
	}
	
	public void clearOutTimeTicket(String date, int hour) {
		try {
			floorAppointmentMgr.clearOutTimeTicket(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 取消预约请条
	 * 
	 * @param storageId
	 * @param date
	 * @param hour
	 * @param type
	 * @param key
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/cancelAppointment", produces = "application/json; charset=UTF-8")
	public Map<String,Object> cancelAppointment(
		@RequestParam(value = "storageId", required = true) String storageId,
		@RequestParam(value = "date", required = true) String date,
		@RequestParam(value = "hour", required = true) String hour,
		@RequestParam(value = "type", required = true) String type,
		@RequestParam(value = "key", required = true) String key,
		@SessionAttr(Config.adminSesion) Map<String,Object> session) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("status", "400");
		result.put("message", "");
		
		String utc = "";
		int ihour = Integer.valueOf(hour);
		if (ihour>9) {
			utc = DateUtil.showUTCTime(date + " "+ihour+":00:00", Long.parseLong(storageId));
		} else {
			utc = DateUtil.showUTCTime(date + " 0"+ihour+":00:00", Long.parseLong(storageId));
		}
		boolean b = floorAppointmentMgr.cancelTicket(storageId,utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), type, key);
		if (b) {
			boolean db = false;
			if ("inbound".equals(type)) {
				db = floorAppointmentMgr.subOneWorkIn(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
				if (!db) throw new Exception("Cancel appointments Error!");
			} else if ("outbound".equals(type)) {
				db = floorAppointmentMgr.subOneWorkOut(utc.substring(0,10), Integer.parseInt(utc.substring(11,13)), Long.parseLong(storageId), "", false);
				if (!db) throw new Exception("Cancel appointments Error!");
			}
			if (db) {
				Map<String,Object> result1 = new HashMap<String,Object>();
				result1.put("status", "200");
				result1.put("message", "");
				return result1;
			}
		}
		return result;
	}
}