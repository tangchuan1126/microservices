package com.oso.load.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorAppointmentMgr;
import com.cwc.app.floor.api.FloorSyncLoadRelationMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.oso.load.beans.B2bOrderStatus;
import com.oso.utils.CommonUtil;
import com.oso.utils.JSONUtils;
import com.oso.utils.RestUtil;

@RestController
@Transactional(readOnly=true)
@SuppressWarnings(value={"rawtypes","unchecked","unused"})
public class WmsAppointmentController{

	@Autowired
	private FloorSyncLoadRelationMgr floorLoadMgr;

	@Autowired
	private FloorAppointmentMgr floorAppointmentMgr;
	
	@Value("${ReceiptIp}")
	private String receiptIp;
	
	DateFormat format1 = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	DateFormat format2 = new SimpleDateFormat("MM/dd/yyyy");

	@RequestMapping(value = "/getLoginStorageCatalog", produces = "application/json; charset=UTF-8")
	public DBRow getLoginStorageCatalog(HttpSession session) throws Exception {
		Map<String, Object> adminLogin = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);
		String loginUserName = (String) adminLogin.get("employe_name");
		
		String id = String.valueOf(adminLogin.get("ps_id"));
		DBRow row = floorAppointmentMgr.getLoginStorageCatalogById(id);
		row.put("loginUserName", loginUserName);
		
		return row;
	} 

	@RequestMapping(value = "/getInvicesLikeTypeId", produces = "application/json; charset=UTF-8")
	public DBRow[] getInvicesLikeTypeId(@RequestParam(value = "no") String no, @RequestParam(value = "type") String type) throws Exception {

		DBRow[] row = null;
		if("load".equals(type)){
			// 根据ID查询load的详情
			row = floorAppointmentMgr.getAppLoadsLikeId(no);
		} else if("order".equals(type)){
			// 根据ID查询order的详情
			row = floorAppointmentMgr.getAppOrdersLikeId(no);
		} else {
			// 根据ID查询po的详情
			row = floorAppointmentMgr.getAppPosLikeNo(no);
		}
		return row;
	} 

	@RequestMapping(value = "/getInviceByTypeId", produces = "application/json; charset=UTF-8")
	public DBRow[] getInviceByTypeId(@RequestParam(value = "id") String id, @RequestParam(value = "type") String type) throws Exception {

		DBRow[] row = null;
		if("load".equals(type)){
			// 根据ID查询load的详情
			row = floorAppointmentMgr.getAppLoadById(id);
		} else if("order".equals(type)){
			// 根据ID查询order的详情
			row = floorAppointmentMgr.getAppOrderById(id);
		} else {
			// 根据ID查询po的详情
			row = floorAppointmentMgr.getAppPoByNo(id);
		}
		return row;
	}
	
	// 检查invoice是否已经存在，如果存在返回true
	private boolean checkInvoice(DBRow[] rows, String id, String type){
		if(rows == null){
			return false;
		} else {
			for(DBRow row: rows){
				String idA = row.getString("invoice_id");
				String typeA = row.getString("invoice_type");
				if(idA.equals(id) && typeA.equals(type)){
					return true;
				}
			}
			return false;
		}
	}

	@RequestMapping(value = "/editAppointment", produces = "application/json; charset=UTF-8")
	public Map editAppointment(@RequestParam(value = "aid", required = false) String aid,HttpSession session) throws Exception {
		// 根据aid查询
		DBRow app = floorAppointmentMgr.getAppByAID(aid);
		long appt=CommonUtil.format(app, "appointment_time", Long.parseLong(app.getString("storage_id")));
		app.put("appointment_time", appt);
		app.put("jet_lag", 0);
		DBRow[] invoices = floorAppointmentMgr.getInvoicesByaid(aid);

		if(app!=null){
			for(DBRow invoice:invoices){
				String type=invoice.getString("type");
				String rfno=invoice.getString("num");
				if(!"rn".equalsIgnoreCase(type)){
					DBRow[] citys=floorAppointmentMgr.getCityState(rfno);
					if(citys!=null){
						List list=new ArrayList();
						for(DBRow city:citys){
							list.add(DBRowUtils.dbRowAsMap(city));
						}
						invoice.put("address",list);
					}
				}else{
					String resp=RestUtil.getReceipt(session.getId(), receiptIp, rfno);
					
					if(!StringUtils.isEmpty(resp)){
						List<Map> jsonArr=JSONUtils.json2list(resp, Map.class);
						
						for(int i=0;i<jsonArr.size();i++){
							Map map=(Map) jsonArr.get(i);
							invoice.clear();
							
							invoice.put("id", map.get("id"));
							invoice.put("num", map.get("id"));
							invoice.put("type", "rn");
							invoice.put("customer", map.get("scac"));
							invoice.put("con", map.get("customerId"));
							invoice.put("shipToAddress", "");
							invoice.put("send_psid", map.get("psId"));
							invoice.put("mabd", "");
							invoice.put("referenceNo", map.get("referenceNo"));
							invoice.put("containerNo", map.get("containerNo"));
							invoice.put("plateNum", map.get("pallets"));
							invoice.put("weights", 0);
							invoice.put("address", "");
						}
					}
				}
			}
			app.put("invoices", invoices);
		}
		return app;
	}

	@RequestMapping(value = "/getAppLoadList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getAppLoadList(@RequestParam(value = "page", required = false) int page, 
			@RequestParam(value = "rows", required = false) int rows,
			@RequestParam(value = "0[no]", required = false) String loadNo,
			@RequestParam(value = "0[startDate]", required = false) String startDate,
			@RequestParam(value = "0[endDate]", required = false) String endDate,
			@RequestParam(value = "0[checkIds]", required = false) String checkIds
			,@RequestParam(value = "0[storageId]", required = true)String storageId) throws Exception {
		Map result = new HashMap();
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(page);
		pc.setPageSize(rows);
		
		DBRow[] loadRows = floorAppointmentMgr.getAppLoadList(loadNo, startDate, endDate, checkIds,storageId,pc);
		if(loadRows != null){
			result.put("total", pc.getAllCount());
			result.put("rows", loadRows);
		} else {
			result.put("total", "0");
		}
		return result;
	}
	/**
	 * 通过  Reference NO. 查询load/order
	 * 
	 * @param page
	 * @param rows
	 * @param type
	 * @param rfno
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/getAppLRList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getAppLRList(
			@RequestParam(value = "page", required = false) int page, 
			@RequestParam(value = "rows", required = false) int rows,
			@RequestParam(value = "0[checkLIds]", required = false) String checkLIds,
			@RequestParam(value = "0[checkOIds]", required = false) String checkOIds,
			@RequestParam(value = "0[checkRnIds]", required = false,defaultValue="0") String checkRNIds,
			@RequestParam(value = "0[otype]", required = true) String type,
			@RequestParam(value = "0[rfno]", required = true) String rfno,
			@RequestParam(value = "0[loadFlag]", required = false,defaultValue="0") int loadFlag,
			HttpSession session) throws Exception {
		Map<String,Object> rt = (Map<String,Object>) session.getAttribute("adminSesion");
		Map result = new HashMap();
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(page);
		pc.setPageSize(rows);
		DBRow[] Rows = null;
		if ("out".equalsIgnoreCase(type)) {
			switch(loadFlag){
				case 0: 
					String ordno = Pattern.matches("^B(\\d)+$", rfno)?rfno.replaceFirst("B", ""):rfno;
					Rows = floorAppointmentMgr.getAppLRList(ordno,checkLIds,checkOIds,"");
					if(Rows!=null){
						DBRow [] citys=floorAppointmentMgr.getCityState(rfno);
						if(citys!=null){
							List list=new ArrayList();
							for(DBRow city:citys){
								list.add(DBRowUtils.dbRowAsMap(city));
							}
							for(DBRow row:Rows){
								row.put("address",list);
							}
						}
					}
					break;
				case 1:
					Rows = floorAppointmentMgr.getAppLRList(rfno,checkLIds,checkOIds,"",loadFlag);
					if(Rows!=null){
						DBRow [] citys=floorAppointmentMgr.getCityState(rfno);
						if(citys!=null){
							List list=new ArrayList();
							for(DBRow city:citys){
								list.add(DBRowUtils.dbRowAsMap(city));
							}
							for(DBRow row:Rows){
								row.put("address",list);
							}
						}
					}
					break;
			}
		} else { //RN
			if(!StringUtils.isEmpty(rfno)){
				String sessionid=session.getId();
				String resp=RestUtil.getReceipt(sessionid, receiptIp, rfno,checkRNIds);
				
				List<Map> jsonArr=JSONUtils.json2list(resp, Map.class);
				Rows=new DBRow[jsonArr.size()];
				
				for(int i=0;i<jsonArr.size();i++){
					Map map=(Map) jsonArr.get(i);
					
					DBRow row=new DBRow();
					Rows[i]=row;
					
					row.put("id", map.get("id"));
					row.put("num", map.get("id"));
					row.put("bolNo", map.get("bolNo"));
					row.put("type", "rn");
					row.put("scac", map.get("scac"));
					row.put("customer_id", map.get("scac"));
					row.put("con", map.get("customerId"));
					row.put("shipToAddress", "");
					row.put("send_psid", map.get("psId"));
					row.put("mabd", "");
					row.put("referenceNo", map.get("referenceNo"));
					row.put("containerNo", map.get("containerNo"));
					row.put("pallets", map.get("pallets"));
					row.put("weights", 0);
					row.put("address", "");
				}
			}
		}
		
		if(Rows != null){
			result.put("total", pc.getAllCount());
			result.put("rows", Rows);
		} else {
			result.put("total", "0");
			result.put("rows", new ArrayList());
		}
		return result;
	}

	@RequestMapping(value = "/getAppOrderList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getAppOrderList(@RequestParam(value = "page", required = false) int page, 
			@RequestParam(value = "rows", required = false) int rows,
			@RequestParam(value = "0[otype]", required = false) String type,
			@RequestParam(value = "0[no]", required = false) String orderNo,
			@RequestParam(value = "0[startDate]", required = false) String startDate,
			@RequestParam(value = "0[endDate]", required = false) String endDate,
			@RequestParam(value = "0[checkIds]", required = false) String checkIds
			,@RequestParam(value = "0[storageId]", required = true)String storageId) throws Exception {
		Map result = new HashMap();
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(page);
		pc.setPageSize(rows);
		DBRow[] loadRows = null;
		if ("po".equals(type)) {
			loadRows = floorAppointmentMgr.getAppOrderListByPo(orderNo, startDate, endDate, checkIds,storageId, pc);
		} else {
			loadRows = floorAppointmentMgr.getAppOrderList(orderNo, startDate, endDate, checkIds,storageId, pc);
		}
		if(loadRows != null){
			result.put("total", pc.getAllCount());
			result.put("rows", loadRows);
		} else {
			result.put("total", "0");
		}
		return result;
	}

	@RequestMapping(value = "/getAppPoList", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map getAppPoList(@RequestParam(value = "page", required = false) int page, 
			@RequestParam(value = "rows", required = false) int rows,
			@RequestParam(value = "0[no]", required = false) String po,
			@RequestParam(value = "0[startDate]", required = false) String startDate,
			@RequestParam(value = "0[endDate]", required = false) String endDate,
			@RequestParam(value = "0[checkIds]", required = false) String checkIds,
			@RequestParam(value = "0[storageId]", required = true)String storageId) throws Exception {
		Map result = new HashMap();
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(page);
		pc.setPageSize(rows);

		DBRow[] loadRows = floorAppointmentMgr.getAppPoList(po, startDate, endDate, checkIds,storageId, pc);
		if(loadRows != null){
			result.put("total", pc.getAllCount());
			result.put("rows", loadRows);
		} else {
			result.put("total", "0");
		}
		return result;
	}
	
	/**
	 * appointment列表
	 * 
	 */
	@RequestMapping(value = "/getAppointmentList", produces = "application/json; charset=UTF-8")
	public Map getAppointmentList(
			@RequestParam(value = "pageNo") int pageNo,
			@RequestParam(value = "pageSize") int pageSize,
			@RequestParam(value = "selectOrderType", required = false) String selectType,
			@RequestParam(value = "orderNum", required = false) String number,
			@RequestParam(value = "inputAppointmentTime", required = false) String appTime,
			@RequestParam(value = "inputCarrier", required = false) String carrier,
			@RequestParam(value = "searchFrom", required = true) String company,
			@RequestParam(value="boundType",required=false)String boundType,HttpSession session) throws Exception {
		// 查询所有appointment
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		
		List resultList = new ArrayList();
		DBRow[] appList = floorAppointmentMgr.getAppointmentByPage(selectType, number, appTime, carrier, company, boundType, pc);
		for(DBRow app : appList){
			//app.add("req_ship_date", " ");
			//app.add("gate_check_in", " ");
			//app.add("feight_type", " ");
			
			Map appMap = new HashMap();
			String aid = app.getString("id");
			appMap.put("trid", aid);
			long storage_id = app.get("storage_id", 0L);
			
			appMap.put("jet_lag", app.getString("jet_lag"));
			appMap.put("Truck", getTruckDetail(app));
			appMap.put("Appiontment", getAppDetail(app));
			appMap.put("Invoice", getInvoiceDetails(aid,session.getId()));
			appMap.put("Log", getLogDetail(aid, storage_id));
			appMap.put("OPTION", getAppDetail(app));
			
			resultList.add(appMap);
		}
		Map data = new HashMap();
		data.put("PAGECTRL", pc);
		data.put("DATA", resultList);
		return data;
	}
	
	private Map getLogDetail(String aid, long ps_id) throws Exception {
		Map map = new HashMap();
		Map more = new HashMap();
		List list = new ArrayList();
		try{
			DBRow[] logList = floorAppointmentMgr.getAppLogs(aid);
			if(logList!=null && logList.length>0){
				
				for(DBRow row : logList){
					Map logDetail = new HashMap();
					
					String type = row.getString("invoice_type");
					
					logDetail.put("InvoiceType", type.replaceFirst(type.substring(0, 1), type.substring(0, 1).toUpperCase()));
					logDetail.put("InvoiceID", row.getString("invoice_no"));
					logDetail.put("OldAID", row.getString("aid_from"));
					logDetail.put("NewAID", row.getString("aid_to"));
					logDetail.put("USER", row.getString("user_created"));
					logDetail.put("DATETIME",  DateUtil.showLocationTime(format1.parse(row.getString("date_created")), ps_id, "MM/dd/yyyy HH:mm"));
					logDetail.put("REMARK", row.getString("remark"));
					list.add(logDetail);
				}
			}
			map.put("LOGDETAILS", list);
			
			return map;
		}catch(Exception ex){
			new Exception("getLogDetail"+ex);
			return map;
		}
	}
	
	private List getInvoiceDetails(String aid,String jsid) throws Exception {
		List resultList = new ArrayList();
		// 根据aid查询Invoice列表
		DBRow[] rows = floorAppointmentMgr.getInvoiceByAID(aid);
		if(rows != null && rows.length>0){
			B2bOrderStatus orderStatus = new B2bOrderStatus();
			for(DBRow row : rows){
				String type = row.getString("invoice_type");
				String invoiceId = row.getString("invoice_id");
				Map detail = new HashMap();
				detail.put("InvoiceTYPE", type.replaceFirst(type.substring(0, 1), type.substring(0, 1).toUpperCase()));
				detail.put("InvoiceID", row.getString("invoice_no"));
				detail.put("EntryID", row.getString(""));

				List list = new ArrayList();
				if("load".equals(type)){
					// 根据ID查询load的详情
					DBRow load = floorLoadMgr.getWmsLoad(invoiceId);
					if(load!=null){
						addToList(load, list, "Pallet Qty", "total_pallets");
						addToList(load, list, "Status", "status");
					}
				} else if("order".equals(type)){
					// 根据ID查询order的详情
					DBRow order = floorLoadMgr.getWmsOrdersById(invoiceId);

					if(order!=null){
						addToList(order, list, "Pallet Qty", "total_pallets");
//						addToList(order, list, "状态", "status");
						addToList2(list, "Status", order.getString("status"));
					}
				} else if("rn".equals(type)){
					String pallets=RestUtil.getReceiptPalletsById(jsid,receiptIp,row.getString("invoice_no"));
					
					if (pallets!=null ) {
						Map m=JSONUtils.json2map(pallets);
						Map _detail = new HashMap();
						_detail.put("LABLE", "Pallet Qty");
						_detail.put("VALUE", m.get("pallets"));
						list.add(_detail);
					}
				} else {
					// 根据ID查询po的详情
					DBRow order = floorLoadMgr.getWmsPoByNo(invoiceId);

					if(order!=null){
						addToList(order, list, "Pallet Qty", "total_pallets");
					}
				}
				detail.put("CHILDREN", list);
				
				resultList.add(detail);
			}
		}
		return resultList;
	}
	
	private Map getTruckDetail(DBRow row) throws Exception {
		Map map = new HashMap();
		map.put("CARRIER", row.getString("carrier_id"));
		
		List truckChildren = new ArrayList();
		
		String appStr=CommonUtil.dateToUTC(row, "appointment_time", Long.parseLong(row.getString("storage_id")));
		if (appStr != null && !"".equals(appStr)) {
			Map detail = new HashMap();
			detail.put("LABLE", "Appointment");
			detail.put("VALUE", appStr);
			truckChildren.add(detail);
		}
		
		addToList(row, truckChildren, "Contacts", "carrier_linkman");
		addToList(row, truckChildren, "Phone", "carrier_linkman_tel");
		addToList(row, truckChildren, "License plate", "licenseplate");
		addToList(row, truckChildren, "Driver License", "driver_license");
		addToList(row, truckChildren, "Driver Name", "driver_name");
		
		addToList(row, truckChildren, "Feight Type", "feight_type");
		
		map.put("CHILDREN", truckChildren);
		return map;
	}
	
	private Map getAppDetail(DBRow row) throws Exception {
		Map appointment = new HashMap();
		appointment.put("APPInfoID", row.getString("id"));
		appointment.put("STATE",  row.getString("status"));
		
		List appChildren = new ArrayList();
		
		
		String appStr=CommonUtil.dateToUTC(row, "appointment_time", Long.parseLong(row.getString("storage_id")));
		if (appStr != null && !"".equals(appStr)) {
			Map detail = new HashMap();
			detail.put("LABLE", "Appointment");
			detail.put("VALUE", appStr);
			appChildren.add(detail);
		}
		
		addToList(row, appChildren, "HUB", "storage_name");
		addToList(row, appChildren, "Contacts", "storage_linkman");
		addToList(row, appChildren, "Phone", "storage_linkman_tel");
		String mabd = row.getString("mabd", "");
		if (mabd!=null && mabd.length()>=19) {
			try {
				Map detail = new HashMap();
				detail.put("LABLE", "MABD");
				detail.put("VALUE", ""+ DateUtil.showLocationTime(new Date(DateUtil.getDate2LongTime(mabd)), row.get("storage_id",0L),"MM/dd/YYYY"));
				appChildren.add(detail);
			} catch (Exception e){}
		}
		
		//addToList(row, appChildren, "MABD", "mabd");
		
		addToList(row, appChildren, "Create By", "user_created");
		addToList(row, appChildren, "Create Date", "date_created");
		addToList(row, appChildren, "Update Date", "date_updated");
		
		String req_ship_date = row.getString("req_ship_date", "");
		if (req_ship_date!=null && req_ship_date.length()>=19) {
			try {
				Map detail = new HashMap();
				detail.put("LABLE", "Req.Ship.Date");
				detail.put("VALUE", ""+ DateUtil.showLocationTime(new Date(DateUtil.getDate2LongTime(req_ship_date)), row.get("storage_id",0L),"MM/dd/YYYY"));
				appChildren.add(detail);
			} catch (Exception e){}
		}
		//addToList(row, appChildren, "Req.Ship.Date", "req_ship_date");
		
		String gate_check_in = row.getString("gate_check_in", "");
		if (gate_check_in!=null && gate_check_in.length()>=19) {
			try {
				Map detail = new HashMap();
				detail.put("LABLE", "Gate Check In");
				detail.put("VALUE", ""+ DateUtil.showLocationTime(new Date(DateUtil.getDate2LongTime(gate_check_in)), row.get("storage_id",0L),"MM/dd/YYYY"));
				appChildren.add(detail);
			} catch (Exception e){}
		}
		
		//addToList(row, appChildren, "Gate Check In", "gate_check_in");
		
		
		appointment.put("CHILDREN", appChildren);
		
		return appointment;
	}

	private int safeInt(Object v) {
		return (v == null) ? 0 : Integer.valueOf(v.toString());
	}

	private Long safeLong(Object v) {
		return (v == null) ? 0L : Long.valueOf(v.toString());
	}

	private Boolean safeBoolean(Object v) {
		return (v == null) ? false : (Boolean) v;
	}

	private String safeString(Object v) {
		return (v == null) ? "" : (String) v;
	}

	private List addToList(DBRow row, List list, String lable, String v) {
		String value = row.getString(v);
		if (value != null && !"".equals(value)) {
			Map detail = new HashMap();
			detail.put("LABLE", lable);
			detail.put("VALUE", value);
			list.add(detail);
		}
		return list;
	}

	private List addToList2(List list, String lable, String value) {
		if (value != null && !"".equals(value)) {
			Map detail = new HashMap();
			detail.put("LABLE", lable);
			detail.put("VALUE", value);
			list.add(detail);
		}
		return list;
	}

	public FloorSyncLoadRelationMgr getFloorLoadMgr() {
		return floorLoadMgr;
	}

	public void setFloorLoadMgr(FloorSyncLoadRelationMgr floorLoadMgr) {
		this.floorLoadMgr = floorLoadMgr;
	}

	public FloorAppointmentMgr getFloorAppointmentMgr() {
		return floorAppointmentMgr;
	}

	public void setFloorAppointmentMgr(FloorAppointmentMgr floorAppointmentMgr) {
		this.floorAppointmentMgr = floorAppointmentMgr;
	}

	public List<Map<String, Object>> sumWorkByStartEnd(@RequestParam(value = "storage_id", required = true) long storage_id, @RequestParam(value = "start", required = true) String start, @RequestParam(value = "end", required = true) String end) throws Exception {
		List<Map<String, Object>> rt = new ArrayList();
		/*
		DBRow[] rows = floorAppointmentMgr.sumWorkByStartEndTotal(start, end, storage_id);
		for (DBRow row : rows) {
			Map<String, Object> mp = new HashMap();
			mp.put("title", "");
			mp.put("start", row.getString("date"));
			mp.put("lentxt", row.getString("work"));
			mp.put("type", row.getString("appointment_type"));
			rt.add(mp);
		}*/
		return rt;
	}

	public Map<String, Object> sumWorkByDate(@RequestParam(value = "storage_id", required = true) long storage_id, @RequestParam(value = "date", required = true) String date) throws Exception {
		Map<String, Object> rt = new HashMap();
		List<Map<String, Object>> rts = new ArrayList();
		
		Map<String, Map<String,Object>> hours = new  java.util.LinkedHashMap<String, Map<String,Object>>();
		DBRow[] rows = floorAppointmentMgr.sumWorkByDayTotal(date, date, storage_id);
		for (DBRow row : rows) {
			String key = row.getString("date")+" "+row.getString("hour");
			if ("".equals(key)) continue;
			if (hours.containsKey(key)) {
				if ("inbound".equals(row.getString("appointment_type"))) {
					hours.get(key).put("CNTIN", row.getValue("work"));
					hours.get(key).put("SUMIN", row.getValue("limit"));
				} else if ("outbound".equals(row.getString("appointment_type"))) {
					hours.get(key).put("CNTOUT", row.getValue("work"));
					hours.get(key).put("SUMOUT", row.getValue("limit"));
				}
			} else {
				Map<String,Object> hour = new HashMap();
				hour.put("ID", row.get("hour"));
				if ("inbound".equals(row.getString("appointment_type"))) {
					hour.put("CNTIN", row.getValue("work"));
					hour.put("SUMIN", row.getValue("limit"));
				} else if ("outbound".equals(row.getString("appointment_type"))) {
					hour.put("CNTOUT", row.getValue("work"));
					hour.put("SUMOUT", row.getValue("limit"));
				}
				hours.put(key, hour);
			}
		}
		for (String key:hours.keySet()) {
			rts.add(hours.get(key));
		}
		rt.put("DATA", rts);
		return rt;
	}

	public Map<String, Object> getBaseWorkByWeek(
			@RequestParam(value = "storage_id", required = true) long storage_id, 
			@RequestParam(value = "start", required = true) String start, 
			@RequestParam(value = "end", required = true) String end) throws Exception {
		Map<String, Object> rt = new HashMap();
		
		Map<String, Object> Dates = new HashMap();
		
		java.text.SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date dt = sdf.parse(start);
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*0))), "A");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*1))), "B");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*2))), "C");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*3))), "D");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*4))), "E");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*5))), "F");
		Dates.put(sdf.format(new Date(dt.getTime()+ (86400000L*6))), "G");
		rt.put("DATES", Dates);
		//rt.put("DATA", floorAppointmentMgr.sumBaseByDay(start, end, storage_id));
		rt.put("BASE", floorAppointmentMgr.getBaseDefault(storage_id));
		return rt;
	}
}
