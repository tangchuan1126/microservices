package com.oso.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.util.StringUtils;

public class RestUtil {

	public final static int SocketTIMEOUT=2000;
	public final static int ConnectTIMEOUT = -1;
	
	public RestUtil() {

	}

	@SuppressWarnings("unused")
	private static boolean SyncCall(String sessionid, String site, long oid) {
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet getRequest = new HttpGet("http://" + site + "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=synWmsOrder&type=3&oid="+ oid);
		getRequest.addHeader("accept", "application/json");
		getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
		HttpResponse response = null;
		try {
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			if ("1".equals(result)) return true;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return false;
	}
	
	public static boolean SyncCall(String sessionid, String site, long oid, String carriers, String pickup_appointment, String loadno) {
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String url = "http://" + site + "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=synWmsOrder&type=3&oid="+ oid;
		if (pickup_appointment!=null) {
			try {
				url += "&pickup_appointment="+ java.net.URLEncoder.encode(pickup_appointment, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if (carriers!=null) {
			try {
				url += "&carriers="+ java.net.URLEncoder.encode(carriers, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if (loadno!=null) {
			try {
				url += "&loadno="+ java.net.URLEncoder.encode(loadno, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		HttpResponse response = null;
		try {
			HttpGet getRequest = new HttpGet(url);
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			if ("1".equals(result)) return true;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return false;
	}

	public static boolean SyncCall(String sessionid, String site, List<String> oid, String carriers, String pickup_appointment, String loadno) {
		// boolean rt = true;
		String oids = "";
		String url = "http://" + site + "/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=synWmsOrder&type=3";
		if (pickup_appointment!=null) {
			try {
				url += "&pickup_appointment="+ java.net.URLEncoder.encode(pickup_appointment, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if (carriers!=null) {
			try {
				url += "&carriers="+ java.net.URLEncoder.encode(carriers, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		if (loadno!=null) {
			try {
				url += "&loadno="+ java.net.URLEncoder.encode(loadno, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		try {
			for (String id : oid) {
				oids += "&oid=" + Long.valueOf(id);
			}
			url += oids;
		} catch (Exception e) {
			return false;
		}
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpGet getRequest = new HttpGet(url);
		getRequest.addHeader("accept", "application/json");
		getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
		HttpResponse response = null;
		try {
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			if ("1".equals(result)) return true;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
				} catch (IOException e) {
				} // 会自动释放连接
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return false;
	}
	
	public static String getReceipt(String sessionid, String site,String no,String ids) {
		if(StringUtils.isEmpty(no))
			throw new NullPointerException("no");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpResponse response = null;
		try {
			no=URLEncoder.encode(no,"UTF-8");
			ids=URLEncoder.encode(ids,"UTF-8");
			String url = "http://" + site + "/Sync10/_receipt/search_receipt/"+ no+"/"+ids;
			
			HttpGet getRequest = new HttpGet(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SocketTIMEOUT).setConnectTimeout(ConnectTIMEOUT).build();//设置请求和传输超时时间
			getRequest.setConfig(requestConfig);
			
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			System.err.println("getReceipt@" +site +" time out.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
	public static String getReceiptById(String sessionid, String site,String no) {
		if(StringUtils.isEmpty(no))
			throw new NullPointerException("no");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String url = "http://" + site + "/Sync10/_receipt/receipt/search_receipt/"+ no;
		HttpResponse response = null;
		try {
			HttpGet getRequest = new HttpGet(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SocketTIMEOUT).setConnectTimeout(ConnectTIMEOUT).build();//设置请求和传输超时时间
			getRequest.setConfig(requestConfig);
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			System.err.println("getReceiptById@" +site +" time out.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
	public static String getReceiptPalletsById(String sessionid, String site,String no) {
		if(StringUtils.isEmpty(no))
			throw new NullPointerException("no");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String url = "http://" + site + "/Sync10/_receipt/receipt_bid/"+ no.trim();
		HttpResponse response = null;
		try {
			HttpGet getRequest = new HttpGet(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SocketTIMEOUT).setConnectTimeout(ConnectTIMEOUT).build();//设置请求和传输超时时间
			getRequest.setConfig(requestConfig);
			
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			System.err.println("getReceiptPalletsById@" +site +" time out.");
			//e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
	public static String saveAppointmentById(String sessionid, String site,String id,String data,String scac) {
		if(StringUtils.isEmpty(id))
			throw new NullPointerException("id");
		
		if(StringUtils.isEmpty(data))
			throw new NullPointerException("data");
		
		if(StringUtils.isEmpty(scac))
			throw new NullPointerException("scac");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		HttpResponse response = null;
		try {
			data=URLEncoder.encode(data,"UTF-8");
			scac=URLEncoder.encode(scac,"UTF-8");
			
			String url = "http://" + site + "/Sync10/_receipt/receipt_appointmentDate/"+ id+"/"+scac+"/"+data;
			HttpGet getRequest = new HttpGet(url);
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
	public static String delAppointmentById(String sessionid, String site,String id) {
		if(StringUtils.isEmpty(id))
			throw new NullPointerException("id");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		String url = "http://" + site + "/Sync10/_receipt/receipt_updateDate/"+ id;
		HttpResponse response = null;
		try {
			HttpGet getRequest = new HttpGet(url);
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
	public static String getReceipt(String sessionid, String site,String no) {
		if(StringUtils.isEmpty(no))
			throw new NullPointerException("no");
		
		CloseableHttpClient httpClient = HttpClients.createDefault();
		
		HttpResponse response = null;
		try {
			no=URLEncoder.encode(no,"UTF-8");
			String url = "http://" + site + "/Sync10/_receipt/search_receiptOne/"+ no;
			HttpGet getRequest = new HttpGet(url);
			RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(SocketTIMEOUT).setConnectTimeout(ConnectTIMEOUT).build();//设置请求和传输超时时间
			getRequest.setConfig(requestConfig);
			getRequest.addHeader("accept", "application/json");
			getRequest.addHeader("Cookie", "JSESSIONID=" + sessionid);
			response = httpClient.execute(getRequest);
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new Exception("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			HttpEntity httpEntity = response.getEntity();
			String result = EntityUtils.toString(httpEntity);
			return result;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (SocketTimeoutException e) {
			System.err.println("getReceipt@" +site +" time out.");
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null) {
				try {
					EntityUtils.consume(response.getEntity());
					
				} catch (IOException e) {}
			}
			if (httpClient!=null)
				try { httpClient.close(); } catch (IOException e) {}
		}
		return null;
	}
	
}