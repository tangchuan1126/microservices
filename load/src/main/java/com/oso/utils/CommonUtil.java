package com.oso.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.util.StringUtils;

import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class CommonUtil {
	public static SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm");
	
	public static String formatUSA(String date) throws ParseException{
		SimpleDateFormat s=new SimpleDateFormat("MM/dd/yyyy HH:mm");
		
		if(StringUtils.isEmpty(date))
			return "";
		
		Date d=sdf.parse(date);
		
		String usa=s.format(d);
		
		return usa;
	}
	
	public static String formatUSA(String date,String format) throws ParseException{
		SimpleDateFormat s=new SimpleDateFormat(format);
		
		if(StringUtils.isEmpty(date))
			return "";
		
		Date d=sdf.parse(date);
		
		String usa=s.format(d);
		
		return usa;
	}
	
	public static String dateToUTC(DBRow row,String fieldName,long ps_id) throws Exception{
		Date date=(Date)row.getValue(fieldName);
		return dateToUTC(date,ps_id);
	}
	
	public static String dateToUTC(Date date,long ps_id) throws Exception{
		if(date!=null){
			String dateStr=DateUtil.showLocationTime(date, ps_id);
			String dateTime=CommonUtil.formatUSA(dateStr);
			return dateTime;
		}
		return "";
	}
	
	public static long format(DBRow row,String fieldName,long ps_id) throws Exception{
		Date date=(Date)row.getValue(fieldName);
		if(date!=null){
			String dateStr=DateUtil.showLocationTime(date, ps_id);
			Date d=sdf.parse(dateStr);
			return d.getTime();
		}
		return 0l;
	}
}
