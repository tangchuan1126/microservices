package com.oso.app.action;

import java.util.Map;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * 基础接收操作
 * 
 * @author wang.cr
 *
 */
public class BaseController  {
	protected static Logger log = Logger.getLogger("microService");
	
	protected HttpSession session;
	
	public void setSession(HttpSession session) {
		this.session = session;
	}
	
	protected String APPPATH = "/appt/";
	
	/**
	 * 处理参数
	 * 
	 * @param url 
	 * @param startKey 开始字符
	 * @param uriVariables 参数列表
	 * @return
	 */
	protected String getUrlForParams(String url, String startKey, Map<String,Object> uriVariables) {
		String para ="";
		for (String var: uriVariables.keySet()) {
			para += String.format("&%s=%s", var, uriVariables.get(var));
		}
		return url + para.replaceFirst("&", startKey);
	}
	
	@SuppressWarnings("rawtypes")
	protected HttpEntity getHttpEntity(HttpSession session, MediaType mtype) {
		HttpHeaders headers = new HttpHeaders();
		if(mtype!=null)	headers.setContentType(mtype);
		headers.add("Cookie", "JSESSIONID="+ session.getId() +";");
		HttpEntity<String> entity = new HttpEntity<String>(headers);
		return entity;
	}
	
	protected HttpEntity<Object> getHttpEntity(Object body,HttpSession session, MediaType mtype) {
		HttpHeaders headers = new HttpHeaders();
		if(mtype!=null)	headers.setContentType(mtype);
		headers.add("Cookie", "JSESSIONID="+ session.getId() +";");
		HttpEntity<Object> entity = new HttpEntity<Object>(body,headers);
		return entity;
	}
}