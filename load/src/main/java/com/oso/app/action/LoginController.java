package com.oso.app.action;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.cwc.db.DBRow;

@RestController
public class LoginController extends BaseController  {
	
	@Value("${Sync10IP}")
	private String sync10_host;
	
	private final static String PATH = "/Sync10";
	
	@Autowired
	private HttpComponentsClientHttpRequestFactory clientHttpRequestFactory;
	
	@Autowired
	private RestTemplate restTemplate = new RestTemplate();
	
	private static final String getString(HttpServletRequest request, String paramName) {
		if (paramName == null) {
			return (null);
		}
		String t = request.getParameter(paramName);
		if (t != null && !t.equals("")) {

			return (t.trim());
		} else {
			return ("");
		}
	}
	
	/**
	 * 登陆
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/public/login",produces = "application/json; charset=UTF-8")
	public Map<String,Object> login(HttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String,Object> rt = new HashMap<String,Object>();
		rt.put("success", 0);
		String pwd = getString(request, "pwd");
		String account = getString(request, "account");
		HttpClient hc = clientHttpRequestFactory.getHttpClient();
		String JSESSIONID = "";
		HttpSession session = request.getSession(false);
		if (session!=null) {
			JSESSIONID = session.getId();
		}
		//log.info(JSESSIONID);
		
		HttpPost hp = new HttpPost(String.format("http://%s/Sync10/action/admin/AccountLoginMgrAction.action?account=%s&pwd=%s",sync10_host,account,pwd));
		hp.addHeader("Accept", "application/json");
		hp.addHeader("Cache-Control","no-cache");
		hp.addHeader("Pragma","no-cache");
		if (!"".equalsIgnoreCase(JSESSIONID)) {
			hp.addHeader("Cookie","JSESSIONID="+JSESSIONID+"; path="+ PATH);
		}
		HttpResponse res = hc.execute(hp);
		try {
			HttpEntity entity = res.getEntity();
			if (entity != null) {
				String rts = EntityUtils.toString(entity,"UTF-8");
				EntityUtils.consume(entity);
				if (rts.contains("\"success\":true")) {
					rt.put("success", 1);
					for (Header cookie : res.getHeaders("Set-Cookie")) {
						if (cookie.getValue().contains("JSESSIONID=")) {
							int start = cookie.getValue().indexOf("JSESSIONID=")+11;
							String sessionId = cookie.getValue().substring(start).split(";")[0];
							//log.info(sessionId);
							response.setHeader("JSESSIONID", sessionId);
							response.setHeader("Set-Cookie", "JSESSIONID="+sessionId+"; Path="+APPPATH);
							rt.put("JSESSIONID", sessionId);
							return rt;
						}
					}
					for (Header cookie : res.getHeaders("Cookie")) {
						if (cookie.getValue().contains("JSESSIONID=")) {
							int start = cookie.getValue().indexOf("JSESSIONID=")+11;
							String sessionId = cookie.getValue().substring(start).split(";")[0];
							//log.info(sessionId);
							response.setHeader("JSESSIONID", sessionId);
							response.setHeader("Set-Cookie", "JSESSIONID="+sessionId+"; Path="+APPPATH);
							rt.put("JSESSIONID", sessionId);
							return rt;
						}
					}
					rt.put("JSESSIONID", JSESSIONID);
					response.setHeader("Set-Cookie", "JSESSIONID="+JSESSIONID+"; Path="+APPPATH);
					return rt;
				}
			}
		} finally {
		}
		return rt;
		
		
	}
	
	/**
	 * 登出
	 * 
	 * @param response
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/logout",produces = "application/json; charset=UTF-8")
	public Map<String,Object> adminLogout(HttpServletResponse response, HttpSession session) throws Exception {
		Map<String,Object> rt = new HashMap<String,Object>();
		rt.put("success", 0);
		if (session==null) {
			rt.put("message", "no session");
			return rt;
		}
		try {
			HttpClient hc = clientHttpRequestFactory.getHttpClient();
			String JSESSIONID = "";
			if (session!=null) {
				JSESSIONID = session.getId();
			}
			
			HttpPost hp = new HttpPost(String.format("http://%s/Sync10/action/administrator/adminLogout.action",sync10_host));
			hp.addHeader("Accept", "application/json");
			hp.addHeader("Cache-Control","no-cache");
			hp.addHeader("Pragma","no-cache");
			if (!"".equalsIgnoreCase(JSESSIONID)) {
				hp.addHeader("Cookie","JSESSIONID="+JSESSIONID+"; path="+ PATH);
			}
			HttpResponse res = hc.execute(hp);
			try {
				HttpEntity entity = res.getEntity();
				if (entity != null) {
					//String rts = EntityUtils.toString(entity,"UTF-8");
					//log.info(rts);
					EntityUtils.consume(entity);
					rt.put("success", 1);
					rt.put("message", "");
				}
			} finally {}
		} catch (Exception e) {
			rt.put("success", 0);
			rt.put("message", "logout error");
		}
		return rt;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/session", produces = "application/json; charset=UTF-8", method= RequestMethod.GET)
	public Map<String,Object> getSession(HttpSession session) throws Exception {
		Map<String,Object> rt = (Map<String,Object>) session.getAttribute("adminSesion");
		Object type = rt.get("corporationType");
		if (type!=null && "2".equals(type.toString())) {
			try {
				rt.put("SCAC", this.getSCAC(Long.parseLong(rt.get("corporationId").toString()), session));
			} catch (NumberFormatException e) {
				rt.put("SCAC", "");
			}
		}
		return rt;
	}
	
	@RequestMapping(value = "/public/loginLicence", produces = "image/jpeg", method= RequestMethod.GET)
	public void loginLicence(HttpServletRequest request, HttpServletResponse response) {
		String JSESSIONID = "";
		HttpSession session = request.getSession(false);
		if (session!=null) {
			JSESSIONID = session.getId();
		} else {
			session = request.getSession(true);
			JSESSIONID = session.getId();
			Map<String,Object> sess = new HashMap<String,Object>();
			sess.put("id", JSESSIONID);
			session.setAttribute("adminSesion", sess);
		}
		//log.info(JSESSIONID);
		try {
			HttpClient hc = clientHttpRequestFactory.getHttpClient();
			HttpGet hp = new HttpGet(String.format("http://%s/Sync10/loginLicence?t=%d" ,sync10_host, (new Date()).getTime()));
			//hp.addHeader("Accept", "application/json");
			hp.addHeader("Cache-Control","no-cache");
			hp.addHeader("Pragma","no-cache");
			hp.addHeader("Expires", "0");
			if (JSESSIONID!=null && !"".equals(JSESSIONID)) {
				hp.addHeader("Cookie","JSESSIONID="+JSESSIONID+"; path="+ PATH);
			}
			HttpResponse res = hc.execute(hp);
			HttpEntity entity = res.getEntity();
			//log.info((String)res.getLastHeader("JSESSIONID").getValue());
			boolean wf = true;
			if (wf) {
				Header sid = res.getFirstHeader("JSESSIONID");
				if (sid!=null) {
					JSESSIONID = sid.getValue();
					response.setHeader("Set-Cookie", "JSESSIONID="+JSESSIONID+"; Path="+APPPATH);
					wf = false;
				}
			}
			if (wf) {
				for (Header cookie : res.getHeaders("Set-Cookie")) {
					if (cookie.getValue().contains("JSESSIONID=")) {
						int start = cookie.getValue().indexOf("JSESSIONID=")+11;
						String sessionId = cookie.getValue().substring(start).split(";")[0];
						response.setHeader("JSESSIONID", sessionId);
						JSESSIONID = sessionId;
						response.setHeader("Set-Cookie", "JSESSIONID="+sessionId+"; Path="+APPPATH);
						wf = false;
					}
				}
			}
			if (wf) {
				for (Header cookie : res.getHeaders("Cookie")) {
					if (cookie.getValue().contains("JSESSIONID=")) {
						int start = cookie.getValue().indexOf("JSESSIONID=")+11;
						String sessionId = cookie.getValue().substring(start).split(";")[0];
						response.setHeader("JSESSIONID", sessionId);
						JSESSIONID = sessionId;
						response.setHeader("Set-Cookie", "JSESSIONID="+sessionId+"; Path="+APPPATH);
						wf = false;
					}
				}
			}
			if (wf) {
				if (!"".equalsIgnoreCase(JSESSIONID)) {
					response.setHeader("JSESSIONID", JSESSIONID);
					response.setHeader("Set-Cookie", "JSESSIONID="+JSESSIONID+"; Path="+APPPATH);
				}
			}
			if (log.isLoggable(Level.FINE)) log.fine(JSESSIONID);
			IOUtils.copy(entity.getContent(), response.getOutputStream());
		} catch (Exception e) {
			log.warning(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/getStorage", produces = "application/json; charset=UTF-8")
	public Object getStoreage(
			@RequestParam(value = "key", defaultValue="", required= false) String key,
			@RequestParam(value = "type", defaultValue="", required= false) String type,
			@RequestParam(value = "rootType", defaultValue="选择仓库", required= false) String rootType,
			@RequestParam(value = "value", defaultValue="", required= false) String value,
			HttpSession session
		) throws Exception {
		
		Map<String,Object> uriVariables = new HashMap<String,Object>();
		if (key!=null)uriVariables.put("key", key);
		if (type!=null)uriVariables.put("type", type);
		if (value!=null)uriVariables.put("value", value);
		try {
			String url = String.format("http://%s/Sync10/action/administrator/order/b2BOrderJsonAction.action?action=getStorageType", sync10_host);

			String rt = restTemplate.postForObject(getUrlForParams(url,"&",uriVariables), getHttpEntity(session,null), String.class, uriVariables);
			return rt;
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 获取SCAC
	 * 
	 * @param id
	 * @param session
	 * @return
	 */
	private String getSCAC(long id, HttpSession session) {
		String url = String.format("http://%s/Sync10/_load/getSCACByID", sync10_host);
		Map<String,Object> uriVariables = new HashMap<String,Object>();
		try {
			uriVariables.put("id", id);
			DBRow row = restTemplate.postForObject(getUrlForParams(url,"?",uriVariables), getHttpEntity(session,null), DBRow.class, uriVariables);
			return row.get("scac", "");
		} catch (RestClientException e) {
			e.printStackTrace();
		}
		return "";
	}
}