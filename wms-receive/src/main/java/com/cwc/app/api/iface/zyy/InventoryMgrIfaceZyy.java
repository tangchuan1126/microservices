package com.cwc.app.api.iface.zyy;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

public interface InventoryMgrIfaceZyy {
	public List<Map<String, Object>> handleMovementAndInventoryByBatch(long line_id, String company_id, long ps_id, long adid) throws Exception;
	public void addInventoryLogByBatch(List<Map<String, Object>> data, HttpSession sess) throws Exception;
	public Long acquireLocationIdByLocationNameAndPsId(int location_type, long ps_id, String location_name, Boolean is_check_location) throws Exception;
}
