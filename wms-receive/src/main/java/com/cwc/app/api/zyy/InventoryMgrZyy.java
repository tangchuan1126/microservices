package com.cwc.app.api.zyy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import us.monoid.json.JSONObject;

import com.cwc.app.api.iface.zyy.InventoryMgrIfaceZyy;
import com.cwc.app.api.iface.zyy.RecordLogMgrIfaceZyy;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.floor.api.zzq.FloorReceiptsMgrZzq;
import com.cwc.app.key.InvenIncreaseTypeKey;
import com.cwc.app.key.InvenOperateTypeKey;
import com.cwc.app.key.LocationTypeKeys;
import com.cwc.db.DBRow;

@Service("inventoryMgrZyy")
public class InventoryMgrZyy implements InventoryMgrIfaceZyy {

	@Autowired
	private FloorReceiptsMgrZzq floorReceiptsMgrZzq;

	@Autowired
	private FloorLogMgrIFace floorLogMgr;
	
	@Override
	public List<Map<String, Object>> handleMovementAndInventoryByBatch(
			long line_id, String company_id, long ps_id, long adid)
			throws Exception {
		DBRow[] pallets = floorReceiptsMgrZzq.findContainerByLineIdForInventory(line_id);
		//批量插入库存
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		DBRow entryRow = floorReceiptsMgrZzq.queryEntryByLineId(line_id);
		String[] seal_number = new String[2];
		seal_number[0] = entryRow.getString("seal_delivery");
		seal_number[1] = entryRow.getString("seal_pick_up");
		for (DBRow con : pallets) {
			//得到location_id
			//long location_id = acquireLocationIdByLocationNameAndPsId(STAGING_TYPE, ps_id, con.get("location", ""), false);
			//update movements  现在关闭托盘时不会记录movement
			//updateMysqlMovements(con.get("con_id", 0L), adid, company_id, location_id, con.get("location", ""), STAGING_TYPE);
			
			Map<String, Object> map = buildSingleInventory(con.getString("con_id"), con.get("qty", 0), ps_id, con.getString("location"), LocationTypeKeys.STAGING_TYPE, seal_number);
			list.add(map);
		}
		return list;
	}

	@Override
	public void addInventoryLogByBatch(List<Map<String, Object>> data,
			HttpSession sess) throws Exception {
		JSONObject jsonObject = null;
		Map<String, Object> m = sess == null ? null : (Map<String, Object>) sess.getAttribute("adminSesion");
		String account = floorReceiptsMgrZzq.queryAccountByAdid(Integer.valueOf(m.get("adid").toString()));
		for(Map<String, Object> map: data){
			jsonObject = new JSONObject();
			DBRow con_props = (DBRow)map.get("con_props");
			DBRow product_props = (DBRow)map.get("product_props");
			DBRow slc_props = (DBRow)map.get("slc_props");
			DBRow rel_props = (DBRow)map.get("rel_props");
			
			
			jsonObject.put("ps_id", map.get("ps_id"));
			jsonObject.put("title_id", product_props.get("title_id"));
			jsonObject.put("customer_id", con_props.get("customer_id"));
			jsonObject.put("container_id", con_props.get("con_id"));
			jsonObject.put("product_line", product_props.get("product_line"));
			jsonObject.put("lot_number", product_props.get("lot_no"));
			jsonObject.put("pc_id", product_props.get("pc_id"));
			jsonObject.put("seal_number", map.get("seal_number"));
			jsonObject.put("slc_id", slc_props.get("slc_id"));
			jsonObject.put("slc_type", slc_props.get("slc_type"));
			jsonObject.put("operate_type", InvenOperateTypeKey.CREATE);
			jsonObject.put("is_increase", InvenIncreaseTypeKey.PLUS);
			jsonObject.put("quantity", rel_props.get("quantity"));
			jsonObject.put("account", account);
			jsonObject.put("adid", m.get("adid"));
			jsonObject.put("department",  m.get("department"));
			jsonObject.put("employe_name", m.get("employe_name"));
			
			floorLogMgr.addLog("InventoryLog", jsonObject, false);
		}
		
	}
	
	
	 
	private Map<String, Object> buildSingleInventory(String con_id, int qty, long ps_id, String location_name,  int location_type, String[] seal_number) throws NumberFormatException, Exception{
		DBRow con_props = floorReceiptsMgrZzq.queryContainerInfoById(Long.valueOf(con_id));
		if( con_props == null){
			throw new ReceiveForAndroidException("Pallet not found!");
		}
		if(!con_props.containsKey("type_id")){
			con_props.add("type_id", 0);
		}
		DBRow product_props = floorReceiptsMgrZzq.queryProductInfoByConatinerId(Long.valueOf(con_id));
		if( product_props == null){
			throw new ReceiveForAndroidException("Product not found!");
		}
		product_props.add("customer_id", con_props.get("customer_id", 0));
		product_props.add("union_flag", 0);
		if(!product_props.containsKey("product_line")){
			product_props.add("product_line", 0);
		}
		DBRow rel_props = new DBRow();
		rel_props.add("quantity", qty);
		DBRow locates_props = new DBRow();
		locates_props.add("title_id", product_props.getString("title_id"));
		locates_props.add("time_number", System.currentTimeMillis());
		locates_props.add("lot_number", product_props.getString("lot_no"));
		locates_props.add("pc_id", product_props.getString("pc_id"));
		DBRow slc_props = new DBRow();
		long location_id = acquireLocationIdByLocationNameAndPsId(location_type, ps_id, location_name, false);
		if(location_id != 0){
			slc_props.add("slc_id", location_id);
			slc_props.add("slc_type", location_type);
		}else{
			//默认 1是ghost
			slc_props.add("slc_id", 1);
			slc_props.add("slc_type", 1);
		}
		
		DBRow map = new DBRow();
		map.add("ps_id", 1000005);
		map.add("slc_props", slc_props);
		map.add("con_props", con_props);
		map.add("product_props", product_props);
		map.add("rel_props", rel_props);
		map.add("locates_props", locates_props);
		map.add("seal_number", seal_number);
		return map;
	}

	@Override
	public Long acquireLocationIdByLocationNameAndPsId(int location_type,
			long ps_id, String location_name, Boolean is_check_location)
			throws Exception {
		long location_id = 0L;
		DBRow[] locationRows = null;
		if(location_type == LocationTypeKeys.LOCATION_TYPE){	
			//location
			locationRows = floorReceiptsMgrZzq.queryLocationIdByLocationNameInLocation(ps_id, location_name);
		}else{	
			//staging
			locationRows = floorReceiptsMgrZzq.queryLocationIdByLocationNameInStaging(ps_id, location_name);
		}
		if(locationRows != null && locationRows.length > 0){
			location_id = locationRows[0].get("obj_id", 0L);
		}else if(is_check_location){
			throw new ReceiveForAndroidException("Location Not Exist!");
		}
		return location_id;
	}

	public void setFloorReceiptsMgrZzq(FloorReceiptsMgrZzq floorReceiptsMgrZzq) {
		this.floorReceiptsMgrZzq = floorReceiptsMgrZzq;
	}

	public void setFloorLogMgr(FloorLogMgrIFace floorLogMgr) {
		this.floorLogMgr = floorLogMgr;
	}
	
}
