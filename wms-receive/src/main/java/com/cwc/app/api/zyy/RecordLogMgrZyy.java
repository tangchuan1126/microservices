package com.cwc.app.api.zyy;

import com.cwc.app.api.iface.zyy.RecordLogMgrIfaceZyy;
import com.cwc.app.floor.api.sbb.FloorReceiptMgrSbb;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;


public class RecordLogMgrZyy implements RecordLogMgrIfaceZyy{

	private FloorReceiptMgrSbb floorReceiptMgrSbb;
	
	@Override
	public long recordCheckInLog(DBRow param) throws Exception {
		DBRow row = new DBRow();
		row.add("operator_id", param.get("adid", -1L));
		row.add("operator_time", DateUtil.NowStr());
		row.add("note", param.get("note", "-1"));
		row.add("dlo_id", param.get("entry_id", -1L));
		row.add("log_type", param.get("log_type", CheckInLogTypeKey.RECEIPT));
		row.add("data", param.get("data"));
		return this.recordLog(row, "check_in_log");
	}

	@Override
	public long recordLog(DBRow log, String table) throws Exception {
		return floorReceiptMgrSbb.insertRecordLog(log,table);
	}

	public void setFloorReceiptMgrSbb(FloorReceiptMgrSbb floorReceiptMgrSbb) {
		this.floorReceiptMgrSbb = floorReceiptMgrSbb;
	}
	
	
}
