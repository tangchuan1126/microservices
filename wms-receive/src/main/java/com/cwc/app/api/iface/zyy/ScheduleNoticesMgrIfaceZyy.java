package com.cwc.app.api.iface.zyy;

import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;

public interface ScheduleNoticesMgrIfaceZyy {

	public boolean noticeToOperator(long adid, String operator, DBRow schedule,String noticeURL,
			HttpSession session) throws IllegalArgumentException, Exception;

}
