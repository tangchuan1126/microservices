package com.cwc.app.api.iface.zyy;

import com.cwc.db.DBRow;

public interface RecordLogMgrIfaceZyy {
	public long recordCheckInLog(DBRow log) throws Exception;
	public long recordLog(DBRow log,String table)  throws Exception;
}
