package com.cwc.app.api.zyy;

import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;

import com.android.receive.util.CommonUtils;
import com.cwc.app.api.iface.zyy.ScheduleNoticesMgrIfaceZyy;
import com.cwc.db.DBRow;

public class ScheduleNoticesMgrZyy implements ScheduleNoticesMgrIfaceZyy {
	
	@Override
	public boolean noticeToOperator(long adid, String operator, DBRow schedule,String noticeURL,
			HttpSession session) throws IllegalArgumentException, Exception {
		String sessionID =session.getId();
		HttpClient client = new HttpClient();
		PostMethod post = new PostMethod(noticeURL);
		post.setRequestHeader("Cookie", "JSESSIONID="+sessionID);
		post.addParameter("schedule", CommonUtils.convertDBRowsToJsonString(schedule));
		post.addParameter("assignTo", operator);
		post.addParameter("adid", String.valueOf(adid));
		int statue = client.executeMethod(post);
		if(statue==200){
			return true;
		}else{
			throw new Exception();
		}
	}

}
