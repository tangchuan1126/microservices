package com.cwc.app.api.iface.zyy;

import java.util.Map;

import javax.servlet.http.HttpSession;

import com.cwc.db.DBRow;

public interface WritebackToWmsMgrInfaceZyy {

	public boolean writebackToWmsByRNAndCompany(String company_id,
			long receipt_no, DBRow[] lines) throws Exception;

	/**
	 * 验证 本次数据 跟WMS 数据的是否一致
	 * 
	 * @param company_id
	 * @param receipt_no
	 * @param lines
	 *            本次收货数据
	 * @return
	 * @throws Exception
	 */
	public DBRow verifyDataConsistency(String company_id, String receipt_no,
			DBRow[] lines) throws Exception;

	
	public void synchronizationReceiptedForCount(Map<String, String> parameter)
			throws Exception;

	/**
	 * 收货:收货完成后,同步mysql数据到selserver 并且关闭主单据 记录日志
	 * 
	 * @param
	 * @return null
	 * @author subin
	 * */
	public void synchronizationReceipted(Map<String, String> parameter)
			throws Exception;
	
	public DBRow finishCount(long line_id,long adid,HttpSession session) throws Exception;

}
