package com.cwc.app.api.zyy;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.android.receive.util.ReceiptUtils;
import com.cwc.app.api.iface.zyy.InventoryMgrIfaceZyy;
import com.cwc.app.api.iface.zyy.RecordLogMgrIfaceZyy;
import com.cwc.app.api.iface.zyy.WritebackToWmsMgrInfaceZyy;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.sbb.FloorReceiptMgrSbb;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.floor.api.wfh.FloorReceiptsMgrWfh;
import com.cwc.app.floor.api.zzq.FloorReceiptsMgrZzq;
import com.cwc.app.invent.BaseController;
import com.cwc.app.invent.CustomException;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.app.key.ContainerStatueKey;
import com.cwc.app.key.GoodsTypeKeys;
import com.cwc.app.key.LineStatueKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;

public class WritebackToWmsMgrZyy implements WritebackToWmsMgrInfaceZyy {
	
	public static final int VAILDATIONERROR = 90;

	private FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb;
	
	private FloorReceiptsMgrWfh floorReceiptsMgrWfh;
	
	private RecordLogMgrIfaceZyy recordLogMgrZyy;
	
	private FloorReceiptMgrSbb floorReceiptMgrSbb;
	
	private FloorReceiptsMgrZzq floorReceiptsMgrZzq;
	
	private InventoryMgrIfaceZyy inventoryMgrZyy;
	
	private BaseController invent;
	
	@Override
	public boolean writebackToWmsByRNAndCompany(String company_id,
			long receipt_no,DBRow[] lines) throws Exception {
		
		return false;
	}

	@Override
	public DBRow verifyDataConsistency(String company_id, String receipt_no,
			DBRow[] lines) throws Exception {
		try{
			DBRow result = new DBRow();
			DBRow para = new DBRow();
			para.add("company_id", company_id);
			para.add("receptNo", receipt_no);
			DBRow[] wmsLines = floorReceiptSqlServerMgrSbb.getReceiptLines(para);
			boolean returnFlag = false;
			String mes = "";
			for (DBRow wmsLine : wmsLines) {
				String wms_item = wmsLine.get("ItemID", "");
				String wms_lot_no = wmsLine.get("LotNo", "");
				int expected_qty = wmsLine.get("ExpectedQty", 0);
				//如果wms的item+lotNO在本地找不到则返回错误信息 若数量不对也返回
				for(DBRow line1:lines){
					String item = line1.get("item_id", "");
					String lot_no = line1.get("lot_no", "");
					int goods_qty = line1.get("goods_total", 0);
					int damage_qty = line1.get("damage_total", 0);
					if((item).equals(wms_item) && lot_no.equals(wms_lot_no) && expected_qty != (damage_qty+goods_qty)){
						mes += "Item: "+wms_item+" \nLot NO: "+wms_lot_no;
						returnFlag = true;
					}
				}
			}
			result.add("returnFlag", returnFlag);
			result.add("mes", mes);
			return result;
		}catch(Exception e){
			throw new Exception(""+e.getMessage());
		}
		
	}

	@Override
	public void synchronizationReceiptedForCount(Map<String, String> parameter)
			throws Exception {
		// 记录日志-开始关闭单据
		DBRow sRecParam = new DBRow();
		sRecParam.put("adid", Long.valueOf(parameter.get("adid")));
		sRecParam.put("entry_id", parameter.get("entry_id"));
		sRecParam.put("note", "start close receipt");
		sRecParam.put("data", "companyId:" + parameter.get("companyId") + ",receiptNo:" + parameter.get("receiptNo"));
		recordLogMgrZyy.recordCheckInLog(sRecParam);
				
		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(parameter.get("companyId"));
		long psId = storage.get("ps_id", 0L);
		String devannerDate = parameter.get("devanner_date");
		
		// 查询mysql数据
		DBRow param = new DBRow();
		param.put("company_id", parameter.get("companyId"));
		param.put("receipt_no", parameter.get("receiptNo"));
		
		
		
		DBRow[] plates = floorReceiptMgrSbb.getReceiptLinePlates(param);
		
		DBRow[] countPlates = floorReceiptMgrSbb.getReceiptLinePlatesCount(param);
		
		DBRow recepitContext = floorReceiptMgrSbb.getReceiptContext(param);
		
		//加上删除的逻辑
		DBRow deleteParam = new DBRow();
		deleteParam.put("companyId", parameter.get("companyId"));
		deleteParam.put("receiptNo", parameter.get("receiptNo"));
		DBRow PlatesCnt = floorReceiptSqlServerMgrSbb.getReceiptLinePlatesCnt(deleteParam);
		System.out.println("=============Pallets count:"+PlatesCnt.get("cnt", 0)+"===============");
		if(PlatesCnt.get("cnt", 0) > 0){
			System.out.println("=============begin deleteReceiptLinePlates ===============");
			floorReceiptSqlServerMgrSbb.deleteReceiptLinePlates(deleteParam);
			System.out.println("=============deleteReceiptLinePlates finished ===============");
		}
		
		for (DBRow oneResult : plates) {
			
			//如果有两个line 一个需要回写 一个不需要则在这里做判断
			if(oneResult.get("is_check_sn", 0)==1){
				continue;
			}
			
			// 插入ReceiptLinePlates
			DBRow receiptLinePlates = new DBRow();
			receiptLinePlates.put("CompanyID", oneResult.get("company_id"));
			receiptLinePlates.put("ReceiptNo", oneResult.get("receipt_no"));
			receiptLinePlates.put("[LineNo]", oneResult.get("line_no"));
			receiptLinePlates.put("SupplierID", oneResult.get("supplier_id"));
			receiptLinePlates.put("LotNo", oneResult.get("lot_no"));
			receiptLinePlates.put("PlateNo", oneResult.get("plate_no"));
			receiptLinePlates.put("ReceivedQty", oneResult.get("cp_quantity", 0f));
			floorReceiptSqlServerMgrSbb.insertReceiptLinePlates(receiptLinePlates);

			// 默认W12
			String stPsId = "W12";

			if (!oneResult.get("location", "").equals("")) {

				stPsId = oneResult.get("location", "");
			}
			// 插入Movements
			String[] movements = { oneResult.get("assign_user_id", ""), oneResult.get("company_id", ""), oneResult.get("plate_no", ""),"Open", stPsId };

			floorReceiptSqlServerMgrSbb.insertMovements(movements);
		}
		
		//更新单据明细托盘数量
		for(DBRow oneResult : countPlates){
			
			floorReceiptSqlServerMgrSbb.updateReceiptLines(oneResult);
		}
		
		/** 关闭主单据 **/
		DBRow recRow = new DBRow();
		recRow.put("companyId", parameter.get("companyId"));
		recRow.put("receiptNo", parameter.get("receiptNo"));

		// 验证是否存在值
		DBRow receipt = floorReceiptSqlServerMgrSbb.getReceipts(recRow);

		if (receipt.get("ContainerNo", "").equals("") || receipt.get("ContainerNo", "").equals("NA")) {

			recRow.put("ContainerNo", recepitContext.get("equipment_number", ""));
		}

		if (receipt.get("Seals", "").equals("") || receipt.get("ContainerNo", "").equals("NA")) {

			recRow.put("Seals", recepitContext.get("seal_delivery", ""));
		}

		recRow.put("status", "Closed");
		
		//edit by zhengziqi at July.2nd 2015
		recRow.put("DevannedDate", DateUtil.showLocalTime(devannerDate, psId).split(" ")[0]);
		recRow.put("DateUpdated", DateUtil.showLocalTime(devannerDate, psId));
		recRow.put("UserUpdated", ReceiptUtils.subStringReceipt(parameter.get("employe_name"),15));
		
		if(!receipt.get("status", "").equals("Closed")){
			
			DBRow[] paramRn = new DBRow[3];
			
			DBRow row1 = new DBRow();
			row1.put("type","string");
			row1.put("value",ReceiptUtils.subStringReceipt(parameter.get("employe_name"),15));
			
			DBRow row2 = new DBRow();
			row1.put("type","string");
			row1.put("value",parameter.get("companyId"));
			
			DBRow row3 = new DBRow();
			row1.put("type","int");
			row1.put("value",Integer.valueOf(parameter.get("receiptNo")));
			
			paramRn[0] = row1;
			paramRn[1] = row2;
			paramRn[2] = row3;
			
			//调用sqlserver存储过程
			floorReceiptSqlServerMgrSbb.closeWmsRn(paramRn);
		}
		
		//关闭单据
		floorReceiptSqlServerMgrSbb.updateReceipts(recRow);
		
		// 记录日志-结束关闭单据
		DBRow eRecParam = new DBRow();
		eRecParam.put("adid", Long.valueOf(parameter.get("adid")));
		eRecParam.put("entry_id", parameter.get("entry_id"));
		eRecParam.put("note", "end close receipt");
		eRecParam.put("data", "companyId:" + parameter.get("companyId") + ",receiptNo:" + parameter.get("receiptNo"));
		recordLogMgrZyy.recordCheckInLog(sRecParam);
	}

	@Override
	@Transactional(value = "sqlserverTxManager", rollbackFor = {Exception.class, RuntimeException.class },propagation = Propagation.REQUIRES_NEW)
	public void synchronizationReceipted(Map<String, String> parameter)
			throws Exception {
		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(parameter.get("companyId"));
		long psId = storage.get("ps_id", 0L);
		String devannerDate = parameter.get("devanner_date");

		
//		记录日志-开始关闭单据
		DBRow sRecParam = new DBRow();
		sRecParam.put("adid", Long.valueOf(parameter.get("adid")));
		sRecParam.put("entry_id", parameter.get("entry_id"));
		sRecParam.put("note", "start close receipt");
		sRecParam.put("data", "companyId:" + parameter.get("companyId") + ",receiptNo:" + parameter.get("receiptNo"));
		recordLogMgrZyy.recordCheckInLog(sRecParam);
		
//		查询mysql数据
		DBRow param = new DBRow();
		param.put("company_id", parameter.get("companyId"));
		param.put("receipt_no", parameter.get("receiptNo"));
//		TODO 是否查询的时候，要加上 ps_id ,不同的仓库  companyId 是否 一样
		DBRow[] plates = floorReceiptMgrSbb.getReceiptLinePlates(param);
		DBRow[] plateSn = floorReceiptMgrSbb.getReceiptLinePlatesSn(param);
		DBRow recepitContext = floorReceiptMgrSbb.getReceiptContext(param);
		DBRow[] countPlates = floorReceiptMgrSbb.getReceiptLinePlatesCount(param);

//		删除按照数量收货记录的数据
		DBRow delParam = new DBRow();
		delParam.put("companyId", parameter.get("companyId"));
		delParam.put("receiptNo", parameter.get("receiptNo"));
		
//		floorReceiptSqlServerMgrSbb.deleteReceiptLinePlates(delParam);
		DBRow PlatesCnt = floorReceiptSqlServerMgrSbb.getReceiptLinePlatesCnt(delParam);
		
		if(PlatesCnt.get("cnt", 0) > 0){
			
			floorReceiptSqlServerMgrSbb.deleteReceiptLinePlates(delParam);
		}
		for (DBRow oneResult : plates) {
			// 插入ReceiptLinePlates
			DBRow receiptLinePlates = new DBRow();
			receiptLinePlates.put("CompanyID", oneResult.get("company_id"));
			receiptLinePlates.put("ReceiptNo", oneResult.get("receipt_no"));
			receiptLinePlates.put("[LineNo]", oneResult.get("line_no"));
			receiptLinePlates.put("SupplierID", oneResult.get("supplier_id"));
			receiptLinePlates.put("LotNo", oneResult.get("lot_no"));
			receiptLinePlates.put("PlateNo", oneResult.get("plate_no"));
			receiptLinePlates.put("ReceivedQty", oneResult.get("cp_quantity", 0F));
			floorReceiptSqlServerMgrSbb.insertReceiptLinePlates(receiptLinePlates);

			// 关闭Movements
			DBRow moveWhere = new DBRow();
			moveWhere.put("CompanyID", oneResult.get("company_id"));
			moveWhere.put("PlateNo", oneResult.get("plate_no"));

			DBRow moveData = new DBRow();
			moveData.put("Status", "Closed");
			
			moveData.put("DateUpdated", DateUtil.showLocalTime(devannerDate, psId));
			moveData.put("UserUpdated", ReceiptUtils.subStringReceipt(parameter.get("employe_name"),15));

			floorReceiptSqlServerMgrSbb.updateMovements(moveWhere, moveData);

			// 默认W12
			String stPsId = "W12";

			if (!oneResult.get("location", "").equals("")) {

				stPsId = oneResult.get("location", "");
			}
			
			// 插入Movements
			String[] movements = { ReceiptUtils.subStringReceipt(oneResult.get("assign_user_id", ""),15),
					oneResult.get("company_id", ""), 
					oneResult.get("plate_no", ""),"Open", stPsId };
					System.out.println("=========="+oneResult.get("plate_no", "")+"============");
			floorReceiptSqlServerMgrSbb.insertMovements(movements);

			// 是否需要扫sn
			if (("1").equals(oneResult.get("is_check_sn", "")) || plateSn.length > 0) {
				
				//判断托盘下是否扫了SN
				if(floorReceiptMgrSbb.getSerialProductByPlateNo(oneResult.get("con_id","")).length > 0){
					
					// 插入ReceiptLineSerialNos
					DBRow receiptLineSerialNos = new DBRow();
					receiptLineSerialNos.put("CompanyID", oneResult.get("company_id"));
					receiptLineSerialNos.put("ReceiptNo", oneResult.get("receipt_no"));
					receiptLineSerialNos.put("[LineNo]", oneResult.get("line_no"));
					// X 表示托盘,SN就表示托盘号
					receiptLineSerialNos.put("SerialNoType", "X");
					receiptLineSerialNos.put("SerialNo", oneResult.get("plate_no"));
					receiptLineSerialNos.put("Qty", oneResult.get("cp_quantity", 0F));
					receiptLineSerialNos.put("UserID",ReceiptUtils.subStringReceipt(oneResult.get("qty_user",""),15));
					
					// 按照托盘收货-完成任务时间
					receiptLineSerialNos.put("TimeStamp", DateUtil.showLocalTime(oneResult.get("qty_date", ""), psId));
					floorReceiptSqlServerMgrSbb.insertReceiptLineSerialNos(receiptLineSerialNos);
					
					// 插入Pallets
					DBRow pallets = new DBRow();
					pallets.put("CompanyID", oneResult.get("company_id"));
					pallets.put("palletNo", oneResult.get("plate_no"));
					pallets.put("Status", "Closed");
					pallets.put("LotNo", oneResult.get("lot_no"));
					pallets.put("AllowedQty", oneResult.get("cp_quantity", 0F));
					pallets.put("PalletPrinted", "1");
					pallets.put("PalletLabelPrinted", "0");
					pallets.put("DateCreated", DateUtil.showLocalTime(oneResult.get("create_date", ""), psId));// 创建托盘时间
					pallets.put("UserCreated", ReceiptUtils.subStringReceipt(oneResult.get("account",""),15));// 创建托盘用户
					pallets.put("UserUpdated", ReceiptUtils.subStringReceipt(oneResult.get("account",""),15));
					pallets.put("DateUpdated", DateUtil.showLocalTime(oneResult.get("create_date", ""), psId));

					// 查询CustomerItems[SerialNoLength]
					DBRow paramCus = new DBRow();
					paramCus.put("CompanyID", oneResult.get("company_id"));
					paramCus.put("CustomerID", oneResult.get("customer_id"));
					paramCus.put("ItemID", oneResult.get("item_id"));

					DBRow customer = floorReceiptSqlServerMgrSbb.getCustomerItems(paramCus);

					if (customer != null) {
						pallets.put("DigitLimit", customer.get("SerialNoLength", ""));
					} else {
						pallets.put("DigitLimit", "");
					}

					floorReceiptSqlServerMgrSbb.insertPallets(pallets);
				}
			}
		}

		for (DBRow oneResult : plateSn) {

			// 是否需要扫sn
			if (("1").equals(oneResult.get("is_check_sn", "")) || plateSn.length > 0) {

				// 插入PalletLines
				DBRow palletLines = new DBRow();
				palletLines.put("CompanyID", oneResult.get("company_id"));
				palletLines.put("SerialNoType", "X");
				palletLines.put("PalletNo", oneResult.get("plate_no"));
				palletLines.put("SerialNo", oneResult.get("serial_number"));
				palletLines.put("Quantity", "1");
				palletLines.put("UserID", ReceiptUtils.subStringReceipt(oneResult.get("account", ""),15));// 账号
				palletLines.put("TimeStamp", DateUtil.showLocalTime(oneResult.get("update_date", ""), psId));// 时间
				palletLines.put("ReferenceNo", "");

				floorReceiptSqlServerMgrSbb.insertPalletLines(palletLines);
			}
		}
		
//		更新单据明细托盘数量
		for(DBRow oneResult : countPlates){
			
			floorReceiptSqlServerMgrSbb.updateReceiptLines(oneResult);
		}
		
		/** 关闭主单据 **/
		DBRow recRow = new DBRow();
		recRow.put("companyId", parameter.get("companyId"));
		recRow.put("receiptNo", parameter.get("receiptNo"));

//		验证是否存在值
		DBRow receipt = floorReceiptSqlServerMgrSbb.getReceipts(recRow);

		if (receipt.get("ContainerNo", "").equals("") || receipt.get("ContainerNo", "").equals("NA")) {

			recRow.put("ContainerNo", recepitContext.get("equipment_number", ""));
		}

		/*if (receipt.get("CarrierID", "").equals("") || receipt.get("ContainerNo", "").equals("NA")) {
			String carrierId = recepitContext.get("company_name", "");
			if(carrierId.length() > 15){
				carrierId = carrierId.substring(0, 15);
			}
			recRow.put("CarrierID", carrierId);
		}*/

		if (receipt.get("Seals", "").equals("") || receipt.get("ContainerNo", "").equals("NA")) {

			recRow.put("Seals", recepitContext.get("seal_delivery", ""));
		}

		recRow.put("status", "Closed");
		
		//edit by zhengziqi at July.2nd 2015
		recRow.put("DevannedDate", DateUtil.showLocalTime(devannerDate, psId).split(" ")[0]);
		recRow.put("DateUpdated", DateUtil.showLocalTime(devannerDate, psId));
		recRow.put("UserUpdated", ReceiptUtils.subStringReceipt(parameter.get("employe_name"),15));
		
		//如果是第二次关闭单据,把托盘的位置信息 回写到mysql
		if(receipt.get("status", "").equals("Closed")){
			
			DBRow movRow = new DBRow();
			movRow.put("companyId", parameter.get("companyId"));
			movRow.put("receiptNo", parameter.get("receiptNo"));
			//查询托盘的位置
			DBRow[] movements = floorReceiptSqlServerMgrSbb.getMovements(movRow);
			
			//回写到mysql
			for(DBRow oneResult : movements){
				
				DBRow conRow = new DBRow();
				conRow.put("container", oneResult.get("PlateNo",0));
				conRow.put("location",oneResult.get("LocationID",0));
				
				floorReceiptMgrSbb.updateContainer(conRow);
			}
		} else {
			
			DBRow[] paramRn = new DBRow[3];
			
			DBRow row1 = new DBRow();
			row1.put("type","string");
			row1.put("value",ReceiptUtils.subStringReceipt(parameter.get("employe_name"),15));
			
			DBRow row2 = new DBRow();
			row1.put("type","string");
			row1.put("value",parameter.get("companyId"));
			
			DBRow row3 = new DBRow();
			row1.put("type","int");
			row1.put("value",Integer.valueOf(parameter.get("receiptNo")));
			
			paramRn[0] = row1;
			paramRn[1] = row2;
			paramRn[2] = row3;
			
			//调用sqlserver存储过程
			floorReceiptSqlServerMgrSbb.closeWmsRn(paramRn);
		}
		
		//关闭单据
		floorReceiptSqlServerMgrSbb.updateReceipts(recRow);

		// 记录日志-结束关闭单据
		DBRow eRecParam = new DBRow();
		eRecParam.put("adid", Long.valueOf(parameter.get("adid")));
		eRecParam.put("entry_id", parameter.get("entry_id"));
		eRecParam.put("note", "end close receipt");
		eRecParam.put("data", "companyId:" + parameter.get("companyId") + ",receiptNo:" + parameter.get("receiptNo"));
		recordLogMgrZyy.recordCheckInLog(eRecParam);
	}

	@Override
	public DBRow finishCount(long line_id, long adid, HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
//			DBRow receipt = floorReceiptsMgrWfh.findReceiptByLineId(line_id);
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(line.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			
			// 参数验证
			if (line_id == 0 || adid == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			// count的数量是否够了
			DBRow sumQTY = floorReceiptsMgrWfh.findSUMReceivedQTY(line_id, GoodsTypeKeys.TYPE_NORMAL,false);
			// 如果在盛放好的货物的托盘上还有坏的货物没有count，则不可以finish count
			if (sumQTY.get("damage_qty", 0) > 0) {
				throw new ReceiveForAndroidException("Please count contains damage pallet again!");
			}
			//验证pc_id
			if(line.get("pc_id", 0L) < 1){
				throw new ReceiveForAndroidException("Product not found!");
			}
			
			DBRow[] pallet = floorReceiptsMgrWfh.findPalletByLineId(line_id, 0, GoodsTypeKeys.TYPE_NORMAL);
			
			for (DBRow p : pallet) {
				// count完所有坏的货物 则好托盘不会出现坏的货物并且数量不为0
				if (p.get("normal_qty", 0) == 0) {
					throw new ReceiveForAndroidException("Please count all pallet!");
				}
				// 是否所有的pallet都已经打印过了标签
				if (p.get("print_user", 0) == 0) {
					throw new ReceiveForAndroidException("Please print all the labels!");
				}
			}
			// 是否是finish count最后一个line
			DBRow[] lines = floorReceiptsMgrWfh.findReceiptLineByReceiptId(line.get("receipt_id", 0L));
			boolean countAll = true;
			boolean is_check_sn = true;
			for (DBRow ln : lines) {
				//这条还未提交到数据库所以肯定是0  这样可以过滤出来
				if (ln.get("counted_user", 0L) == 0 && ln.get("receipt_line_id", 0L)!=line_id) { 
					countAll = false;
				}
				//如果有一条line需要扫sn则不会关闭rn以及回写
				if(ln.get("is_check_sn", 0) == 1){
					is_check_sn = false;
				}
			}
			//关闭rn验证是否与wms的预期数量相符
			if(countAll){
				//验证当前item扫描的数量是否与wms一致不一致则不会关闭
				if (is_check_sn) {
					boolean returnFlag = false;
					DBRow verifyDBRow = verifyDataConsistency(line.get("company_id", ""),  line.get("receipt_no", ""), lines);
					returnFlag = verifyDBRow.get("returnFlag", false);
					if(returnFlag){
						ret = BCSKeyReceive.FAIL;
						err = VAILDATIONERROR;
						result.add("ret", ret);
						result.add("err", err);
						result.add("data",verifyDBRow.get("mes","") + "\nExpected QTY and actual QTY not equal, so can't close RN !");
						return result;
					}
				}
			}
			
			// 将line的status改为2 并且写入counted的时间与操作人
			DBRow upRow = new DBRow();
			upRow.add("status", LineStatueKey.STATUS_TO_CC);
			upRow.add("unloading_finish_time", DateUtil.NowStr());
			upRow.add("counted_user", adid);
			floorReceiptsMgrWfh.updateReceiptLine(line_id, upRow);
			
			// ***已经count了所有的托盘
			if (countAll) {
				//这里只判断rn下的line是否有需要回写的line
				// 如果需要扫描sn则不回写wms
				if (is_check_sn) {
					// 获取entry_id
					DBRow rn = floorReceiptsMgrZzq.queryEntryIdByRNId(line.get("receipt_id", ""));
					String entry_id = rn.getString("dlo_id");
					// 获取adminLoginBean
					@SuppressWarnings("unchecked")
					Map<String, Object> adminBean = (Map<String, Object>) session
					.getAttribute(Config.adminSesion);
					// 关闭rn
					String closeTime = DateUtil.NowStr();
					upRow = new DBRow();
					upRow.add("devanner_date", closeTime);
					upRow.add("status", "Closed");
					floorReceiptsMgrZzq.updateReceiptStatus(line.get("receipt_id", ""), upRow);
					//回写wms
					Map<String, String> parameter = new HashMap<String, String>();
					parameter.put("companyId", line.get("company_id", ""));
					parameter.put("receiptNo", line.get("receipt_no", ""));
					parameter.put("adid", adid+"");
					parameter.put("entry_id", entry_id);
					parameter.put("devanner_date", closeTime);
					parameter.put("employe_name", adminBean.get("employe_name").toString());
					
					synchronizationReceiptedForCount(parameter);
					//System.out.println("===============synchronizationReceiptedForCount finished==================");
//					this.synchronizationReceiptedForCount(parameter);
				}
			}
			// 这条line是否需要扫面sn不需要则关闭line
			if (line.get("is_check_sn", 0) != 1) {
			//	System.out.println("===============is_check_sn=1 ==================");
				upRow = new DBRow();
				upRow.add("status", LineStatueKey.STATUS_CLOSE);
				floorReceiptsMgrWfh.updateReceiptLine(line_id, upRow);
				//System.out.println("===============updateReceiptLine ==================");
				//关闭所有的托盘
				floorReceiptsMgrWfh.updateContainerStatusByLineID(line_id, ContainerStatueKey.CON_CLOSE);
				//System.out.println("===============updateContainerStatusByLineID ==================");
				//add inventory
				List<Map<String, Object>> data = inventoryMgrZyy.handleMovementAndInventoryByBatch(line_id, line.get("company_id", ""), ps_id, adid);
				//System.out.println("===============handleMovementAndInventoryByBatch ==================");
				//System.out.println("===============data is "+data+" ==================");
				DBRow r = null;
				try{
					r = invent.copyTree(ps_id, data);	
					
				}catch(Exception ex){
					System.out.println("===============copyTree failed ==================");
					ex.printStackTrace();
				}
				
				if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
					throw new ReceiveForAndroidException("Inventory exception!");
				}
				
				
				//Remove Inventory Log temporarily by zhengziqi at Aug.7th 2015
				inventoryMgrZyy.addInventoryLogByBatch(data, session);
				System.out.println("===============addInventoryLogByBatch ==================");
				
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (JSONException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		System.out.println("===============finish count==================");
		return result;
	}
	
	
	
	public void setFloorReceiptSqlServerMgrSbb(
			FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb) {
		this.floorReceiptSqlServerMgrSbb = floorReceiptSqlServerMgrSbb;
	}

	public void setFloorReceiptsMgrWfh(FloorReceiptsMgrWfh floorReceiptsMgrWfh) {
		this.floorReceiptsMgrWfh = floorReceiptsMgrWfh;
	}

	public void setRecordLogMgrZyy(RecordLogMgrIfaceZyy recordLogMgrZyy) {
		this.recordLogMgrZyy = recordLogMgrZyy;
	}

	public void setFloorReceiptMgrSbb(FloorReceiptMgrSbb floorReceiptMgrSbb) {
		this.floorReceiptMgrSbb = floorReceiptMgrSbb;
	}

	public void setFloorReceiptsMgrZzq(FloorReceiptsMgrZzq floorReceiptsMgrZzq) {
		this.floorReceiptsMgrZzq = floorReceiptsMgrZzq;
	}

	public void setInventoryMgrZyy(InventoryMgrIfaceZyy inventoryMgrZyy) {
		this.inventoryMgrZyy = inventoryMgrZyy;
	}

	public void setInvent(BaseController invent) {
		this.invent = invent;
	}
	
}
