package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class GoodsTypeKeys {
	DBRow row;
	
	public static int TYPE_NORMAL = 0;
	public static int TYPE_DAMAGE = 2;
	public static int TYPE_ALL = 3;
	
	public GoodsTypeKeys(){
		row = new DBRow();
		row.add(String.valueOf(GoodsTypeKeys.TYPE_NORMAL), "TYPE_NORMAL");
		row.add(String.valueOf(GoodsTypeKeys.TYPE_DAMAGE), "TYPE_DAMAGE");
		row.add(String.valueOf(GoodsTypeKeys.TYPE_ALL), "TYPE_ALL");
	}
	
	public ArrayList getGoodsTypeKeys() {
		return (row.getFieldNames());
	}

	public String getGoodsTypeKeysValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getGoodsTypeKeysValue(String id) {
		return (row.getString(id));
	}

}
