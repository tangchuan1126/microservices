package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.cwc.app.key.CreateContainerProcessKey.java] 
 * @ClassName:    [CreateContainerProcessKey]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年6月15日 上午10:50:13]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年6月15日 上午10:50:13]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class CreateContainerProcessKey {
	
	DBRow row;
	
	public static final int PROCESS_COUNT = 0;	//在count时生成托盘
	public static final int PROCESS_CC = 1;		//在CC时生成托盘
	public static final int PROCESS_SCAN = 2;	//在scan时生成托盘
	
	public CreateContainerProcessKey(){
		row = new DBRow();
		row = new DBRow();
		row.add(String.valueOf(CreateContainerProcessKey.PROCESS_COUNT), "PROCESS_COUNT");
		row.add(String.valueOf(CreateContainerProcessKey.PROCESS_CC), "PROCESS_CC");
		row.add(String.valueOf(CreateContainerProcessKey.PROCESS_SCAN), "PROCESS_SCAN");
	}
	
	public ArrayList getCreateContainerProcessKeys() {
		return (row.getFieldNames());
	}

	public String getCreateContainerProcessKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getCheckInLogTypeKeyValue(String id) {
		return (row.getString(id));
	}
}
