package com.cwc.app.key;

/**
 * 
 * @ProjectName: [wms-receive]
 * @Package: [com.cwc.app.key.ScheduleScreenKey.java]
 * @ClassName: [ScheduleScreenKey]
 * @Description: [一句话描述该类的功能]
 * @Author: [赵永亚]
 * @CreateDate: [2015年7月16日 下午3:16:54]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年7月16日 下午3:16:54]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class ScheduleScreenKey {

	public final static String SCANNING_SCHEDULE = "scanningschedule";
	public final static String CC_SCHEDULE = "ccschedule";
	public final static String SCAN_SCHEDULE = "scanschedule";
}
