package com.cwc.app.key;
/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.cwc.app.key.BCSKey.java] 
 * @ClassName:    [BCSKey]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年7月9日 下午3:06:41]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年7月9日 下午3:06:41]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class BCSKeyReceive extends BCSKey {
	
	/**
	 *  收货信息
	 */
	public static int VAILDATIONERROR = 90;
	
	/**
	 *  SN 重复
	 */
	public static int SN_REPEAT = 91;
	/**
	 * 不在任务中
	 */
	public static int OUT_SCHEDULE = 92;
	/**
	 * 没有找到 Location 
	 */
	public static int NOT_FIND_LOACTION = 93;
}
