package com.cwc.app.key;
/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.cwc.app.key.ContainerPutType.java] 
 * @ClassName:    [ContainerPutType]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年7月27日 下午4:49:03]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年7月27日 下午4:49:03]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class ContainerPutType {
	public static final int CON_PUT_EMPTY = 1;		//空的托盘
	public static final int CON_PUT_LACK = 2;		//没放满的托盘
	public static final int CON_PUT_FULL = 3;		//放满的托盘
	public static final int CON_PUT_BEYOND = 4;		//放多的托盘
}
