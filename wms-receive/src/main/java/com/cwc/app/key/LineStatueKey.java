package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 
 * @ProjectName: [wms-receive]
 * @Package: [com.cwc.app.key.LiseStatueKey.java]
 * @ClassName: [LiseStatueKey]
 * @Description: [line 的状态信息]
 * @Author: [赵永亚]
 * @CreateDate: [2015年6月12日 上午9:37:32]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年6月12日 上午9:37:32]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class LineStatueKey {
	DBRow row;
	
	public static int UNPROCESS = 0;
	public static int STATUS_TO_CC = 2;
	public static int STATUS_SCAN = 3;
	public static int STATUS_CLOSE = 4;
	
	public LineStatueKey(){
		row = new DBRow();
		row.add(String.valueOf(LineStatueKey.UNPROCESS), "UNPROCESS");
		row.add(String.valueOf(LineStatueKey.STATUS_TO_CC), "STATUS_TO_CC");
		row.add(String.valueOf(LineStatueKey.STATUS_SCAN), "STATUS_SCAN");
		row.add(String.valueOf(LineStatueKey.STATUS_CLOSE), "STATUS_CLOSE");
	}
	
	public ArrayList getLineStatueKeys() {
		return (row.getFieldNames());
	}

	public String getLineStatueKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getLineStatueKeyValue(String id) {
		return (row.getString(id));
	}

}
