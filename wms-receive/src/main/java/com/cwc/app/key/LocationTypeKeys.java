package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class LocationTypeKeys {
	DBRow row;
	
	public static final int STAGING_TYPE = 2;
	public static final int LOCATION_TYPE = 1;
	
	public LocationTypeKeys(){
		row = new DBRow();
		row.add(String.valueOf(LocationTypeKeys.STAGING_TYPE), "TYPE_NORMAL");
		row.add(String.valueOf(LocationTypeKeys.LOCATION_TYPE), "LOCATION_TYPE");
	}
	
	public ArrayList getLocationTypeKeys() {
		return (row.getFieldNames());
	}

	public String getLocationTypeKeysValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getLocationTypeKeysValue(String id) {
		return (row.getString(id));
	}

}
