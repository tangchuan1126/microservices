package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

/**
 * 
 * @ProjectName: [wms-receive]
 * @Package: [com.cwc.app.key.PalletStatueKey.java]
 * @ClassName: [LiseStatueKey]
 * @Description: [container 的状态信息] 
 * @Author: [赵永亚]
 * @CreateDate: [2015年6月12日 上午9:37:32]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年6月12日 上午9:37:32]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class PalletStatueKey {
	DBRow row;
	
	public static int OPEN = 1;
	public static int CLOSED = 2;
	
	public PalletStatueKey(){
		row = new DBRow();
		row.add(String.valueOf(PalletStatueKey.OPEN), "OPEN");
		row.add(String.valueOf(PalletStatueKey.CLOSED), "CLOSED");
	}
	
	public ArrayList getLineStatueKeys() {
		return (row.getFieldNames());
	}

	public String getLineStatueKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getLineStatueKeyValue(String id) {
		return (row.getString(id));
	}

}
