package com.cwc.app.key;

import java.util.ArrayList;

import com.cwc.db.DBRow;

public class ContainerStatueKey {
	DBRow row;
	
	public static final int CON_OPEN = 1;			//托盘是打开状态
	public static final int CON_CLOSE = 2;			//托盘是关闭状态
	
	
	public ContainerStatueKey(){
		row = new DBRow();
		row.add(String.valueOf(ContainerStatueKey.CON_OPEN), "CON_OPEN");
		row.add(String.valueOf(ContainerStatueKey.CON_CLOSE), "CON_CLOSE");
	}
	
	public ArrayList getContainerStatueKeys() {
		return (row.getFieldNames());
	}

	public String getContainerStatueKeyValue(int id) {
		return (row.getString(String.valueOf(id)));
	}

	public String getContainerStatueKeyValue(String id) {
		return (row.getString(id));
	}

}
