package com.cwc.app.floor.api.zyy;

import java.sql.SQLException;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import com.android.cctask.controller.CCTaskForAndroidController;
import com.cwc.app.key.ContainerPutType;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.CreateContainerProcessKey;
import com.cwc.app.key.LineStatueKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;

/**
 * 
 * @ProjectName: [wms-receive]
 * @Package: [com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy.java]
 * @ClassName: [FloorReceiptsMgrZyy]
 * @Description: [Receipt 的相关操作]
 * @Author: [赵永亚]
 * @CreateDate: [2015年5月12日 下午2:56:08]
 * @UpdateUser: [赵永亚]
 * @UpdateDate: [2015年5月12日 下午2:56:08]
 * @UpdateRemark: [说明本次修改内容]
 * @Version: [v1.0]
 * 
 */
public class FloorReceiptsMgrZyy {

	public void modifySchedule(long task_id, DBRow upRow) throws Exception {
		try {
			String where = "WHERE schedule_id= " + task_id;
			dbUtilAutoTran.update(where, "schedule", upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》modifySchedule"
					+ e.getMessage());
		}
	}
	
	public void modifyScheduleSub(long task_id, DBRow upRow) throws Exception {
		try {
			String where = "WHERE schedule_id= " + task_id;
			dbUtilAutoTran.update(where, "schedule_sub", upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》modifySchedule"
					+ e.getMessage());
		}
	}

	public DBRow getScheduleByTaskId(long task_id) throws Exception {
		String sql = "SELECT * FROM `schedule` WHERE schedule_id=?";
		DBRow param = new DBRow();
		param.add("task_id", task_id);
		return dbUtilAutoTran.selectPreSingle(sql, param);
	}
	public DBRow[] getScheduleOperatorsByTaskId(long task_id) throws Exception {
		String sql = "SELECT * FROM `schedule_sub` WHERE schedule_id=?";
		DBRow param = new DBRow();
		param.add("task_id", task_id);
		return dbUtilAutoTran.selectPreMutliple(sql, param);
	}

	public void delScheduleOperatorsByTaskId(long task_id) throws Exception {
		try{
			
			String table = "schedule_sub";
			String wherecond = " WHERE schedule_id =?";
			DBRow para = new DBRow();
			para.add("schedule_id", task_id);
			dbUtilAutoTran.deletePre(wherecond, para, table);
		}catch(Exception e){
			throw new Exception("FloorReceiptsMgrZyy->delScheduleOperatorsByTaskId"
					+ e.getMessage());
			
		}
	}
	/**
	 * 根据task_id 获取 receipt_lines 的相关信息 主要获取item_id
	 * 
	 * @param task_id
	 *            schedule表主键"schedule_id"
	 * @param adid
	 *            对应子表中的 schedule_sub "schedule_execute_id"
	 * @param associate_type
	 * @param associate_process
	 * @return
	 */
	public DBRow getReceiptLinesByTaskId(long task_id, long adid,
			int associate_type, int associate_process) throws Exception {
		return getReceiptLinesByTaskId(task_id, adid, 0, associate_type,
				associate_process);
	}

	/**
	 * 
	 * @param task_id
	 * @param adid
	 * @param is_task_finish
	 *            子任务是否结束 0 为结束，1表示已结束
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow getReceiptLinesByTaskId(long task_id, long adid,
			int is_task_finish, int associate_type, int associate_process)
			throws Exception {
		try {
			// StringBuffer sql = new
			// StringBuffer("SELECT rl.*  ,sl.schedule_id FROM receipt_lines rl JOIN (SELECT r.receipt_id,s.* FROM receipts r  JOIN (");
			// sql.append("SELECT schedule_id ,associate_id FROM schedule WHERE associate_id = ? AND schedule_id = ? AND schedule_state < ? )s")
			// .append(" on  r.receipt_id = s.associate_id) sl ON rl.receipt_id= sl.receipt_id");

			// StringBuffer sql = new
			// StringBuffer("SELECT rl.*  ,sl.schedule_id FROM receipt_lines rl JOIN (SELECT r.receipt_id,s1.* FROM receipts r  JOIN (");
			// sql.append(" SELECT s.* FROM (SELECT  schedule_id, associate_id FROM schedule WHERE associate_type=? AND associate_process=?) s")
			// .append(" JOIN (SELECT schedule_id FROM schedule_sub WHERE schedule_execute_id =? AND schedule_id=? AND is_task_finish=?) ss ON s.schedule_id = ss.schedule_id)s1")
			// .append(" on  r.receipt_id = s1.associate_id) sl ON rl.receipt_id= sl.receipt_id");
			// SELECT rl.* , s1.schedule_id FROM receipt_lines rl JOIN (SELECT
			// s.* FROM (SELECT schedule_id, associate_id FROM schedule WHERE
			// associate_type=25 AND associate_process=62) s JOIN (SELECT
			// schedule_id FROM schedule_sub WHERE schedule_execute_id =1000066
			// AND schedule_id=1112976 AND is_task_finish=0) ss ON s.schedule_id
			// = ss.schedule_id)s1 ON rl.receipt_line_id = s1.associate_id ;
			StringBuffer sql = new StringBuffer(
					"SELECT rl.* , s1.schedule_id FROM receipt_lines rl JOIN (SELECT s.* FROM (SELECT schedule_id, associate_id FROM schedule");
			sql.append(
					" WHERE associate_type=? AND associate_process=? and schedule_id = ?) s JOIN (SELECT schedule_id FROM schedule_sub WHERE schedule_execute_id =? ")
					.append(" AND schedule_id=? AND is_task_finish=?) ss ON s.schedule_id = ss.schedule_id)s1 ON rl.receipt_line_id = s1.associate_id ");

			DBRow param = new DBRow();
			param.put("associate_type", associate_type);
			param.put("associate_process", associate_process);
			param.put("schedule_id_s", task_id);
			param.put("schedule_execute_id", adid);
			param.put("schedule_id", task_id);
			param.put("is_task_finish", is_task_finish);
			return dbUtilAutoTran.selectPreSingle(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy->getReceiptLinesByTaskId"
					+ e.getMessage());
		}
	}

	/**
	 * 查询任务信息
	 * 
	 * @param receipt_line_id
	 * @param adid
	 * @param is_task_finish
	 * @param associate_type
	 * @param associate_process
	 * @return
	 * @throws Exception
	 */
	public DBRow getScheduleByReceiptAndAdid(long receipt_line_id, long adid,
			int is_task_finish, int associate_type, int associate_process)
			throws Exception {

		try {
			StringBuffer sql = new StringBuffer();

			sql.append(
					"SELECT s.* FROM (SELECT  * FROM schedule WHERE associate_type=? AND associate_process=? AND associate_id=?) s JOIN ")
					.append(" (SELECT schedule_id FROM schedule_sub WHERE schedule_execute_id =?");
			DBRow param = new DBRow();
			param.put("associate_type", associate_type);
			param.put("associate_process", associate_process);
			param.put("associate_id", receipt_line_id);
			param.put("schedule_execute_id", adid);
			if (is_task_finish != -1) {
				sql.append(" and is_task_finish =?");
				param.put("is_task_finish", is_task_finish);
			}
			sql.append(") ss ON s.schedule_id = ss.schedule_id");
			return dbUtilAutoTran.selectPreSingle(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getScheduleByReceiptAndAdid"
							+ e.getMessage());
		}
	}
	
	public DBRow getScheduleByReceipt(long receipt_line_id,  int associate_type, int associate_process)
			throws Exception {

		try {
			StringBuffer sql = new StringBuffer();

			sql.append(
					"SELECT  * FROM schedule WHERE associate_type=? AND associate_process=? AND associate_id=? ");
			DBRow param = new DBRow();
			param.put("associate_type", associate_type);
			param.put("associate_process", associate_process);
			param.put("associate_id", receipt_line_id);
			return dbUtilAutoTran.selectPreSingle(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getScheduleByReceipt" + e.getMessage()+"receipt_line_id:"+receipt_line_id);
		}
	}

	/**
	 * 根据 line_id 获取相关配置信息，比如 plate_no,托盘配置数量等
	 * 
	 * @param receipt_line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExpectedQtyAndPallet(long receipt_line_id)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT rc.* ,(cc.length*cc.width*cc.height) as expected_qty  FROM ( SELECT rrc.*,con.con_id FROM (SELECT * FROM receipt_rel_container WHERE receipt_line_id =?) ")
					.append("  rrc JOIN(SELECT * FROM container WHERE is_delete =0)con ON con.con_id = rrc.plate_no ")
					.append(" )rc JOIN (SELECT * FROM  container_config WHERE receipt_line_id =?)cc ON rc.container_config_id = cc.container_config_id;");
			DBRow param = new DBRow();
			param.put("receipt_line_id1", receipt_line_id);
			param.put("receipt_line_id2", receipt_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getExpectedQtyAndPallet"
					+ e.getMessage());
		}
	}

	/**
	 * 根据line Id 获取到托盘配置的数量，1表示 是clp的托盘数量，3 表示 tlp 的托盘数量
	 * 
	 * @param line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getPalletQtyByLineId(long line_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT rc.container_type ,SUM((cc.length*cc.width*cc.height)) as expected_qty FROM ( ")
					.append(" SELECT rrc.*,con.con_id,con.container_type FROM ( ")
					.append(" SELECT * FROM receipt_rel_container WHERE receipt_line_id =?) rrc JOIN(SELECT * FROM container WHERE is_delete =0)con ON con.con_id = rrc.plate_no  ")
					.append(")rc JOIN (SELECT * FROM  container_config WHERE receipt_line_id =? AND is_delete =0)cc ON rc.container_config_id = cc.container_config_id GROUP BY container_type;");
			DBRow param = new DBRow();
			param.put("receipt_line_id1", line_id);
			param.put("receipt_line_id2", line_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getPalletQtyByLineId"
					+ e.getMessage());
		}
	}

	public int getBreakPalletQtyByLineId(long line_id) throws Exception {
		try {
			String sql = "select sum(quantity) as qty from cc_from where receipt_line_id="
					+ line_id;
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			return row.get("qty", 0);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getBreakPalletQtyByLineId"
							+ e.getMessage());
		}
	}
	
	public int getCCPalletQtyByLineId(long line_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT IFNULL(SUM(cc.length * cc.width * cc.height ) ,0 )AS qty  FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND con.process = 1  AND rrc.receipt_line_id= ").append(line_id)
				.append(" JOIN container_config cc ON cc.container_config_id = rrc.container_config_id");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			
			return row.get("qty", 0);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getCCPalletQtyByLineId"
							+ e.getMessage());
		}
	}

	public int getCountQTYByLineId(long line_id) throws Exception{
		try {
			int count = 0;
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT SUM( rrc.normal_qty) as qty FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND con.process = 0  AND rrc.receipt_line_id= ").append(line_id)
				.append(" JOIN container_config cc ON cc.container_config_id = rrc.container_config_id ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			count+= row.get("qty", 0);
			// 加上以break 的 
			
			sql.setLength(0);
			sql.append("SELECT SUM(quantity) as qty FROM cc_from WHERE receipt_line_id =").append(line_id);
			row = dbUtilAutoTran.selectSingle(sql.toString());
			if(null!=row){
				count+= row.get("qty", 0);
			}
			return count;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getCountQTYByLineId"
							+ e.getMessage());
		}
	}
	
	public int getCountQTYByLineIdAndConfigId(long line_id ,long config_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT SUM( rrc.normal_qty) as qty FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND con.process = 0  AND rrc.receipt_line_id= ").append(line_id)
				.append(" JOIN container_config cc ON cc.container_config_id = rrc.container_config_id  AND cc.container_config_id  = ").append(config_id);
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			
			return row.get("qty", 0);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getCountQTYByLineIdAndConfigId"
							+ e.getMessage());
		}
	}
	public int getInboundQTYByLineId(long line_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT SUM( rrc.normal_qty) as qty FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND con.process = 0  AND rrc.receipt_line_id= ").append(line_id)
				.append(" JOIN container_config cc ON cc.container_config_id = rrc.container_config_id AND cc.keep_inbound = 1 ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			
			return row.get("qty", 0);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getCountQTYByLineIdAndConfigId"
							+ e.getMessage());
		}
	}

	public DBRow[] getBreakContainersByLineId(long line_id) throws Exception {
		try {
			String sql = "select * from cc_from where receipt_line_id="
					+ line_id;
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getBreakContainersByLineId"
							+ e.getMessage());
		}
	}
	/**
	 * 获取CC时 CLP的数量，在 break 时验证
	 * 
	 * @param receipt_id
	 * @param receipt_line_id
	 * @param adid
	 * @param associate_type
	 * @param associate_process
	 * @param container_typ
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getCCExpectedQtyConfig(long receipt_line_id, long adid,
			int associate_type, int associate_process, int container_typ)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT r.* , rrcq.qty AS qty FROM (SELECT s.*, ss.schedule_execute_id FROM (SELECT * FROM schedule WHERE associate_type=? AND associate_process=? AND associate_id =?) s JOIN ")
					.append(" (SELECT * FROM schedule_sub WHERE schedule_execute_id=?) ss ON ss.schedule_id=s.schedule_id)sc JOIN (SELECT * FROM receipt_lines WHERE  receipt_line_id=?)r ON sc.receipt_line_id=r.receipt_line_id JOIN ")
					.append(" (SELECT rrc.receipt_line_id, SUM(cc.length*cc.width*cc.height) AS qty FROM receipt_rel_container rrc JOIN (SELECT * FROM container WHERE container_type=1)con ON rrc.plate_no = con.con_id JOIN")
					.append(" container_config cc ON rrc.container_config_id = cc.container_config_id)rrcq ON r.receipt_line_id = rrcq.receipt_line_id");
			DBRow param = new DBRow();
			param.put("associate_type", associate_type);
			param.put("associate_process", associate_process);
			param.put("associate_id", receipt_line_id);
			param.put("schedule_execute_id", adid);
			param.put("receipt_line_id", receipt_line_id);

			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getCCExpectedQtyConfig"
					+ e.getMessage());
		}
	}

	/**
	 * 获取托盘的实际数量
	 * 
	 * @param receipt_line_id
	 * @param plate_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExpectedQtyAndPallet(long receipt_line_id, long plate_no)
			throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT rc.* ,(cc.length*cc.width*cc.height) as expected_qty  FROM ( SELECT rrc.*,con.con_id,con.is_delete FROM (SELECT * FROM receipt_rel_container WHERE receipt_line_id =? and plate_no=?) ")
					.append("  rrc JOIN( SELECT * FROM container )con ON con.con_id = rrc.plate_no ")
					.append(" )rc JOIN (SELECT * FROM  container_config WHERE receipt_line_id =?)cc ON rc.container_config_id = cc.container_config_id;");
			DBRow param = new DBRow();
			param.put("receipt_line_id", receipt_line_id);
			param.put("plate_no", plate_no);
			param.put("receipt_line_id2", receipt_line_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getExpectedQtyAndPallet"
					+ e.getMessage());
		}
	}

	/**
	 * 获取托盘的实际数量
	 * 
	 * @param receipt_line_id
	 * @param plate_no
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getExpectedQtyAndPallet(long receipt_line_id, long plate_no,
			boolean is_delete) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT rc.* ,(cc.length*cc.width*cc.height) as expected_qty  FROM ( SELECT rrc.*,con.con_id,con.is_delete FROM (SELECT * FROM receipt_rel_container WHERE receipt_line_id =? and plate_no=?) ")
					.append("  rrc JOIN(SELECT * FROM container WHERE is_delete =?)con ON con.con_id = rrc.plate_no ")
					.append(" )rc JOIN (SELECT * FROM  container_config WHERE receipt_line_id =?)cc ON rc.container_config_id = cc.container_config_id;");
			DBRow param = new DBRow();
			param.put("receipt_line_id", receipt_line_id);
			param.put("plate_no", plate_no);
			param.put("is_delete", is_delete);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getExpectedQtyAndPallet"
					+ e.getMessage());
		}
	}

	/**
	 * 添加到cc_from 表
	 * 
	 * @param row
	 * @return
	 * @throws Exception
	 */
	public long addCCFrom(DBRow row) throws Exception {
		try {
			return dbUtilAutoTran.insertReturnId("cc_from", row);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》addCCFrom"
					+ e.getMessage());
		}
	}

	/**
	 * 逻辑删除托盘
	 * 
	 * @param con_id
	 * @throws Exception
	 */
	public void delContainerById(long con_id) throws Exception {
		try {
			delContainerById(con_id, false);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》delContainerById"
					+ e.getMessage());
		}
	}

	/**
	 * 删除托盘，flag 为true 是 物理删除，否则逻辑删除
	 * 
	 * @param con_id
	 * @param flag
	 * @throws Exception
	 */
	public void delContainerById(long con_id, boolean flag) throws Exception {
		try {
			String table = "container";
			String wherecond = " WHERE con_id =?";
			DBRow para = new DBRow();
			para.add("con_id", con_id);
			if (flag) {
				wherecond += " and container_type = 1";
				dbUtilAutoTran.deletePre(wherecond, para, table);
				// TODO 物理删除是同时删除关系配置表中的数据
				wherecond = " WHERE plate_no =?";
				dbUtilAutoTran.deletePre(wherecond, para,
						"receipt_rel_container");

			} else {
				DBRow data = new DBRow();
				wherecond = " WHERE con_id =" + con_id;
				data.add("is_delete", 1);
				dbUtilAutoTran.update(wherecond, table, data);
			}

		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》delContainerById"
					+ e.getMessage());
		}
	}

	/**
	 * 根据 con_id 修改托盘的删除状态
	 * 
	 * @param con_id
	 * @param is_delete
	 * @throws Exception
	 */
	public void modifyContainerIsDelete(long con_id, int is_delete)
			throws Exception {
		try {
			String where = " WHERE con_id = " + con_id;
			String table = "container";
			DBRow data = new DBRow();
			data.put("is_delete", is_delete);
			dbUtilAutoTran.update(where, table, data);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》modifyContainerIsDelete"
					+ e.getMessage());
		}
	}

	/**
	 * 根据 receipt_rel_container_id 修改关联表中的数量
	 * 
	 * @param con_id
	 * @param qty
	 * @throws Exception
	 */
	@Transactional
	public void modifyReceiptRelContainerNormalQty(
			long receipt_rel_container_id, long qty) throws Exception {
		try {
			String where = " WHERE receipt_rel_container_id = "
					+ receipt_rel_container_id;
			String table = "receipt_rel_container";
			DBRow data = new DBRow();
			data.put("normal_qty", qty);
			dbUtilAutoTran.update(where, table, data);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》modifyReceiptRelContainerNormalQty"
							+ e.getMessage());
		}
	}

	/**
	 * 根据托盘配置ID 删除 配置信息 flag 为true 是 物理删除，flag 为 false 时 逻辑删除
	 * 
	 * @param container_config_id
	 * @param flag
	 * @throws Exception
	 */
	public void delContainerConfigById(long container_config_id, boolean flag)
			throws Exception {
		try {

			String table = "container_config";

			String wherecond = " WHERE container_config_id =?";
			DBRow para = new DBRow();
			para.add("container_config_id", container_config_id);
			if (flag) {
				dbUtilAutoTran.deletePre(wherecond, para, table);
				// 同时删除配关系配置表中的数据，同时删除托盘信息
			} else {
				DBRow data = new DBRow();
				data.add("is_delete", 1);
				dbUtilAutoTran.updatePre(wherecond, para, table, data);
			}

		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》delContainerConfigById"
					+ e.getMessage());
		}
	}

	/**
	 * 物理删除 托盘和配置的关联表
	 * 
	 * @param container_config_id
	 * @throws Exception
	 */
	public void deleteReceiptRelContainerByCCId(long container_config_id)
			throws Exception {
		try {
			String table = "receipt_rel_container";
			String wherecond = " WHERE container_config_id =?";
			DBRow para = new DBRow();
			para.add("container_config_id", container_config_id);
			dbUtilAutoTran.deletePre(wherecond, para, table);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》deleteReceiptRelContainerByCCId"
					+ e.getMessage());
		}
	}

	/**
	 * 根据托盘ID ，查询关系表中的数据信息
	 * 
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getReceiptRelContainerByConId(long con_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT * FROM receipt_rel_container rrc1 WHERE EXISTS (SELECT container_config_id FROM receipt_rel_container rrc2 WHERE rrc2.plate_no =? AND  rrc1.container_config_id = rrc2.container_config_id)");
			DBRow param = new DBRow();
			param.put("plate_no", con_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy-》getReceiptRelContainerByConId"
							+ e.getMessage());
		}
	}

	/**
	 * 根据line_id 和托盘类型，获取 托盘配置信息
	 * 
	 * @param line_id
	 * @param container_type
	 *            1CLP，2BLP，3TLP, 4ILP
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getBraekPalletByLine(long line_id) throws Exception {
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(
					"SELECT aa.*, cc.cc_from_id ,  cc.quantity as break_qty from (SELECT DISTINCT con.status, ci.customer_name AS customer_id, ")
					.append(" con.detail_id AS con_detail_id , con.con_id, con.location ,con.is_delete, con.container AS container_no, ")
					.append(" con.container_type, con.item_id , con.lot_number AS lot_no, cc.config_type, cc.length , cc.width, cc.height,  ")
					.append(" cc.pallet_type, cc.note, a.adid, cc.create_date, cc.goods_type , a.employe_name,  ")
					.append(" (cc.length*cc.height*cc.width) AS config_qty , cp.cp_quantity, rrc.*, p_user.employe_name AS print_user_name  ")
					.append(" FROM receipt_rel_container rrc JOIN container con ON rrc.plate_no = con.con_id   JOIN  ")
					.append(" container_config cc ON cc.container_config_id = rrc.container_config_id LEFT JOIN container_product cp ON  ")
					.append(" cp.cp_lp_id = con.con_id LEFT JOIN admin a ON cc.create_user = a.adid LEFT JOIN admin p_user ON ")
					.append(" rrc.print_user = p_user.adid LEFT JOIN customer_id ci ON ci.customer_key = con.customer_id WHERE ")
					.append(" rrc.receipt_line_id=?)aa JOIN (SELECT * FROM cc_from WHERE receipt_line_id = ?)cc ON aa.con_id = cc.container_id ");
			// String sql =
			// "SELECT cc.* ,cc.container_id as container_no, cc.container_id as wms_container_id  FROM cc_from cc  WHERE receipt_line_id = ?;";
			DBRow param = new DBRow();
			param.add("line_id", line_id);
			param.add("line_id1", line_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getBraekPalletByLine"
					+ e.getMessage());
		}
	}

	/**
	 * 根据 cc_from_id 获取删除的托盘
	 * 
	 * @param cc_from_id
	 * @return
	 * @throws Exception
	 */
	public DBRow getBraekPalletById(long cc_from_id) throws Exception {
		try {
			String sql = "SELECT * FROM cc_from  WHERE cc_from_id = ?;";
			DBRow param = new DBRow();
			param.add("line_id", cc_from_id);
			return dbUtilAutoTran.selectPreSingle(sql, param);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》getBraekPalletById"
					+ e.getMessage());
		}
	}

	/**
	 * 根据ID 删除 cc_from 表中的记录
	 * 
	 * @param cc_from_id
	 * @throws Exception
	 */
	public void delBreakPalletById(long cc_from_id) throws Exception {
		try {
			String sql = "  WHERE cc_from_id =   " + cc_from_id;
			int n = dbUtilAutoTran.delete(sql, "cc_from");
			System.out.println(n);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》delBreakPalletById"
					+ e.getMessage());
		}
	}

	private DBUtilAutoTran dbUtilAutoTran;

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}


	public DBRow[] getBoxTypesInfo(long pc_id, int container_type_id,
			int title_id, int ship_to_id, long active) throws Exception {
		try {
			DBRow para = new DBRow();
			StringBuffer sql = new StringBuffer();
			sql.append("select lpt.*,ct.type_name,newItst.ship_to_ids,newItst.title_ids");
			sql.append(" from license_plate_type lpt");
			sql.append(" join container_type ct on lpt.basic_type_id=ct.type_id");
			sql.append(" left join (select ltst.lp_type_id,GROUP_CONCAT(distinct convert(ltst.ship_to_id,char) separator',') as ship_to_ids,GROUP_CONCAT(distinct convert(ltst.title_id,char) separator',') as title_ids from lp_title_ship_to ltst group by ltst.lp_type_id) newItst on lpt.lpt_id=newItst.lp_type_id");
			sql.append(" where lpt.pc_id = ?");
			para.add("pc_id", pc_id);
			if (container_type_id > 0) {
				sql.append(" and lpt.basic_type_id = ?");
				para.add("basic_type_id", container_type_id);
			}
			if (ship_to_id > 0) {
				sql.append(" and FIND_IN_SET(?,newItst.ship_to_ids)");
				para.add("ship_to_id", ship_to_id);
			}
			if (title_id > 0) {
				sql.append(" and FIND_IN_SET(?,newItst.title_ids)");
				para.add("title_id", title_id);
			}

			if (active > 0) {
				sql.append(" AND lpt.active = ?");
				para.add("active", active);
			}

			sql.append(" order by newItst.ship_to_ids asc,newItst.title_ids asc,lpt.lpt_id desc");
			return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} catch (Exception e) {
			throw new Exception(
					"getBoxTypesInfo(long pc_id, int container_type_id,int title_id, int ship_to_id, long active):"
							+ e);
		}
	}

	/**
	 *  获取没有拆的托盘信息
	 * @param receipt_line_id
	 * @param cc_id
	 * @param goods_type
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findNoBreakPalletByLineId(long receipt_line_id, long cc_id,
			int goods_type) throws Exception {
		try {
			String sql = "SELECT DISTINCT con.status,con.process,ci.customer_name AS customer_id,con.detail_id as con_detail_id"
					+ " ,con.con_id, con.location,IFNULL(con.location_type,1) as location_type , con.container as container_no ,con.item_id"
					+ " ,con.lot_number AS lot_no,con.container_type ,cc.config_type,cc.length"
					+ " ,cc.width,cc.height,cc.pallet_type,cc.note,a.adid,cc.create_date,cc.goods_type,cc.keep_inbound"
					+ " ,a.employe_name,(cc.length*cc.height*cc.width) AS config_qty "
					+ " , cp.cp_quantity, rrc.*,p_user.employe_name as print_user_name"
					+ " FROM receipt_rel_container rrc JOIN "
					+ " (SELECT c.* FROM container c WHERE NOT EXISTS (SELECT * FROM cc_from cc WHERE c.con_id = cc.container_id AND receipt_line_id="
					+ receipt_line_id
					+ "  )) "
//					+ " con ON rrc.plate_no = con.con_id AND con.is_delete=0  and con.container_type=3 " 
					+ " con ON rrc.plate_no = con.con_id AND con.is_delete=0  AND con.process =  "+CreateContainerProcessKey.PROCESS_COUNT 
					+ " JOIN container_config cc ON cc.container_config_id = rrc.container_config_id "
					+ " LEFT JOIN container_product cp ON cp.cp_lp_id = con.con_id "
					+ " LEFT JOIN admin a ON cc.create_user = a.adid"
					+ " LEFT JOIN admin p_user ON rrc.print_user = p_user.adid"
					+ " LEFT JOIN customer_id ci ON ci.customer_key = con.customer_id"
					+ " WHERE rrc.receipt_line_id=" + receipt_line_id;
			if (goods_type != 3) {
				sql += " AND cc.goods_type=" + goods_type;
			}
			if (cc_id > 0) {
				sql += " AND rrc.container_config_id=" + cc_id;
			}
			sql += " ORDER BY con.`status` ASC ,con.con_id desc";
			return dbUtilAutoTran.selectMutliple(sql);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrWfh.findNoBreakPalletByLineId error:"
					+ e);
		}
	}

	public DBRow[] getNotCloseLineByReceipt(long receipt_id) throws Exception {
		try {
			return getNotCloseLineByReceiptAnd(receipt_id,
					LineStatueKey.STATUS_CLOSE);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getNotCloseLineByReceipt error:" + e);
		}

	}

	public DBRow[] getNotCloseLineByReceiptAnd(long receipt_id, int status)
			throws Exception {
		try {
			String sql = "SELECT * FROM receipt_lines where status!=? and receipt_id=?";
			DBRow param = new DBRow();
			param.add("status", status);
			param.add("receipt_id", receipt_id);
			return dbUtilAutoTran.selectPreMutliple(sql, param);
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getNotCloseLineByReceiptAnd error:"
							+ e);
		}
	}
	
	public DBRow[] getBoxTypesInfo(long pc_id, int container_type_id,
			int title_id, int ship_to_id, int customer_id, long active)
			throws Exception {
		try {
			DBRow para = new DBRow();
			StringBuffer sql = new StringBuffer();
			sql.append("select lpt.*,ct.type_name,newItst.ship_to_ids,newItst.title_ids,newItst.customer_ids ");
			sql.append(" from license_plate_type lpt");
			sql.append(" join container_type ct on lpt.basic_type_id=ct.type_id");
			sql.append(" left join (select ltst.lp_type_id,GROUP_CONCAT(DISTINCT convert(ltst.ship_to_id,char) separator',') as ship_to_ids,GROUP_CONCAT(DISTINCT convert(ltst.title_id,char) separator',') as title_ids,GROUP_CONCAT(DISTINCT convert(ltst.customer_id,char) separator',') as customer_ids from lp_title_ship_to ltst group by ltst.lp_type_id) newItst on lpt.lpt_id=newItst.lp_type_id");
			sql.append(" where lpt.pc_id = ?");
			para.add("pc_id", pc_id);
			if (container_type_id > 0) {
				sql.append(" and lpt.basic_type_id = ?");
				para.add("basic_type_id", container_type_id);
			}
			if (ship_to_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.ship_to_ids) or FIND_IN_SET(0,newItst.ship_to_ids))");
				para.add("ship_to_id", ship_to_id);
			}
			if (title_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.title_ids) or FIND_IN_SET(0,newItst.title_ids))");
				para.add("title_id", title_id);
			}
			if (customer_id > 0) {
				sql.append(" and (FIND_IN_SET(?,newItst.customer_ids) or FIND_IN_SET(0,newItst.customer_ids))");
				para.add("customer_id", customer_id);
			}
			if (active > 0L) {
				sql.append(" AND lpt.active = ?");
				para.add("active", active);
			}
//			sql.append(" union(");
//			sql.append(" SELECT lpt.*, ct.type_name,NULL as\tship_to_ids,NULL  as\ttitle_ids,NULL as\tcustomer_ids FROM license_plate_type lpt");
//			sql.append(" JOIN container_type ct ON lpt.basic_type_id = ct.type_id");
//			sql.append(" LEFT JOIN lp_title_ship_to  ltst  on lpt.lpt_id = ltst.lp_type_id ");
//			sql.append(" WHERE\tlpt.pc_id =" + pc_id
//					+ " AND ltst.customer_id is null");
//			if (container_type_id > 0) {
//				sql.append(" and lpt.basic_type_id =" + container_type_id);
//			}
//			if (active > 0L) {
//				sql.append(" AND lpt.active =" + active);)
//			}
			sql.append(" ORDER BY ship_to_ids DESC,title_ids DESC,lpt_id DESC");
			return this.dbUtilAutoTran.selectPreMutliple(sql.toString(), para);
		} catch (Exception e) {
			throw new Exception(
					"getBoxTypesInfo(long pc_id, int container_type_id,int title_id, int ship_to_id, long active):"
							+ e);
		}
		
	}
	
	public DBRow[] getNoCloseSchedulesByReceiptId(long receipt_id,int associate_type ,int associate_process) throws Exception{
		try{
			String sql ="SELECT  s.* FROM `schedule` s  JOIN receipt_lines rl ON rl.receipt_line_id = s.associate_id "
					+ "AND s.associate_type="+associate_type+" AND s.associate_process = "+associate_process+" AND "
					+ "rl.receipt_id ="+receipt_id+" AND s.schedule_state=0;";
			
			return dbUtilAutoTran.selectMutliple(sql);
		}catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy ->getNoCloseSchedulesByReceiptId()");
		}
	}
	
	public boolean checkIsInSchedule(long line_id ,int associate_type ,int associate_process,long adid) throws Exception{
		boolean flag = false;
		try{
		StringBuffer buf = new StringBuffer("");
		DBRow para = new DBRow();
		buf.append("SELECT s.schedule_id from `schedule` s  JOIN schedule_sub ss where s.schedule_id = ss.schedule_id")
			.append("  AND ss.schedule_execute_id =  ?")
			.append("  AND s.associate_type =  ?")
			.append("  AND s.associate_process =  ?")
			.append("  AND s.associate_id =  ?");
		
		para.add("schedule_execute_id", adid);
		para.add("associate_type", associate_type);
		para.add("associate_process", associate_process);
		para.add("associate_id", line_id);
		DBRow[] row = dbUtilAutoTran.selectPreMutliple(buf.toString(), para);
		if(null!=row && row.length>0){
			//已经加入扫描任务
			flag = true;
		}
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->checkIsInSchedule()");
		}
		return flag;
	}
	
	public void joinExistedSchedule(long adid , long schedule_id) throws Exception{
		
		try {
			DBRow row = new DBRow();
			row.add("schedule_id", schedule_id);
			row.add("schedule_state", 0);
			row.add("is_task_finish", 0);
			row.add("schedule_execute_id", adid);
			dbUtilAutoTran.insertReturnId("schedule_sub", row);
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->joinExistedSchedule()");
		}
	}
	
	
	
	public DBRow[] getScheduleByPsId (long ps_id,int associate_type ,int associate_process,PageCtrl pc) throws Exception{
		try{StringBuffer sql = new StringBuffer();
			sql.append("SELECT r.receipt_no as schedule_id  ,s.create_time AS startTime ,GROUP_CONCAT(a.employe_name) as employe_name ,a.adid ,rl.item_id,rl.receipt_line_id , s.associate_id,r.receipt_type as task_type ,rl.expected_qty as qty ")
				.append(" ,rl.company_id")
				.append(" from `schedule` s JOIN schedule_sub ss ON s.schedule_id = ss.schedule_id  ")
				.append(" 	AND s.associate_type = ? AND s.associate_process = ? AND s.schedule_state <10 ")
				.append(" JOIN admin a ON a.adid = ss.schedule_execute_id LEFT JOIN receipt_lines rl ON rl.receipt_line_id = s.associate_id ");
			
			if(associate_process == CCTaskForAndroidController.CCTASK_SCANER ){
				sql.append(" AND rl.scan_start_time IS  NULL " );
			}
				
			sql.append(" JOIN receipts r ON rl.receipt_id = r.receipt_id AND r.ps_id = ?   GROUP BY s.schedule_id ");
			DBRow para = new DBRow();
			para.put("associate_type", associate_type);
			para.put("associate_process", associate_process);
			para.put("ps_id", ps_id);
			return dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->getScheduleByPsId()");
		}
	}
	public DBRow[] getScanningScheduleByPsId (long ps_id ,PageCtrl pc) throws Exception{
		try{StringBuffer sql = new StringBuffer();
		sql.append("SELECT  r.receipt_no as schedule_id  ,rl.scan_start_time AS startTime  ,GROUP_CONCAT(a.employe_name) as employe_name ,a.adid ,rl.item_id,rl.receipt_line_id,r.receipt_type as task_type , s.associate_id ,rl.expected_qty as qty")
			.append(" ,rl.company_id")
			.append(" from `schedule` s JOIN schedule_sub ss ON s.schedule_id = ss.schedule_id  ")
			.append(" 	AND s.associate_type = ").append(CCTaskForAndroidController.CCTASK).append(" AND s.associate_process = ").append(CCTaskForAndroidController.CCTASK_SCANER).append(" AND s.schedule_state <10 ")
			.append(" JOIN admin a ON a.adid = ss.schedule_execute_id LEFT JOIN receipt_lines rl ON rl.receipt_line_id = s.associate_id AND  rl.scan_start_time IS NOT NULL ")
			.append(" JOIN receipts r ON rl.receipt_id = r.receipt_id AND r.ps_id = ?  GROUP BY s.schedule_id ");
		DBRow para = new DBRow();
		para.put("ps_id", ps_id);
		return dbUtilAutoTran.selectPreMutliple(sql.toString(), para,pc);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->getScheduleByPsId()");
		}
	}
	
	
	public void updatePalletConfig(long line_id , long config_id ,int keepInbound) throws Exception{
		
		try {
			String where = "WHERE container_config_id= " + config_id +" AND receipt_line_id= "+line_id ;
			DBRow upRow = new DBRow();
			upRow.add("keep_inbound", keepInbound);
			dbUtilAutoTran.update(where, "container_config", upRow);
		} catch (Exception e) {
			throw new Exception("FloorReceiptsMgrZyy-》updatePalletConfig"
					+ e.getMessage());
		}
		
	}
	public DBRow[] getSerialProductCountByLineIds(String lines) throws Exception{
		try{
			String sql = "SELECT count(sp.serial_product_id) as spCount  ,receipt_line_id from receipt_rel_container  rrc JOIN serial_product sp ON rrc.plate_no = sp.at_lp_id AND rrc.receipt_line_id IN("+lines+") GROUP BY receipt_line_id";
			return dbUtilAutoTran.selectMutliple(sql);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->getSerialProductCountByLineIds(String lines)");
		}
		
	}
	/**
	 * 把 CLP 不满的托盘转 成TLP 
	 * @param receipt_id
	 * @throws Exception 
	 */
	public void updateConClpToTlp(long receipt_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" UPDATE container con JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND con.is_full != ").append(ContainerPutType.CON_PUT_FULL)
				.append(" and con.container_type = ").append(ContainerTypeKey.CLP)
			   .append(" JOIN receipt_lines  rl ON rrc.receipt_line_id = rl.receipt_line_id AND receipt_id = ").append(receipt_id)
			   .append(" SET con.container_type =  ").append(ContainerTypeKey.TLP);
			dbUtilAutoTran.executeSQL(sql.toString());
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->updateConClpToTlp(receipt_id)");
		}
	}
	
	public int getContainersByConfigAndProcessAndLineId(long config_id ,int process,long line_id) throws Exception{
		int count = 0;
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT count(con.con_id) as qty FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND")
				.append(" rrc.container_config_id = ").append(config_id)
				.append(" AND receipt_line_id = ").append(line_id)
				.append(" AND con.process =").append(process);
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			count= row.get("qty", 0);
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->getContainersByConfigAndProcessAndLineId ");
		}
		
		return count;
	}
	
	
	public DBRow[] getPartialConIds(long line_id ,long config_id ,int process,int is_partial ) throws Exception{
		try{
			// 把 container 转成 con_id  为了少改代码 
		StringBuffer sql= new StringBuffer();
		sql.append("SELECT con.container as con_id FROM container con  JOIN receipt_rel_container rrc on con.con_id = rrc.plate_no")
			.append(" AND rrc.container_config_id = ").append(config_id)
			.append(" and rrc.receipt_line_id =").append(line_id).append(" AND con.process = ").append(process)
			.append(" JOIN container_config cc ON cc.is_partial = ").append(is_partial)
			.append(" AND cc.container_config_id = rrc.container_config_id");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			e.printStackTrace();
			throw new Exception("FloorReceiptsMgrZyy ->getPartialConIds ");
		}
		
	}
	
	public DBRow[] getInboundConfig(long line_id) throws Exception{
		
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" select ct.type_name as pallet_name,rls.receipt_no ,cc.*,ifnull(st.ship_to_name,'') as ship_to_name , ifnull(lpt.lp_name,'') as lp_name , rrc.print_user as print_user_name,rrc.print_date , count(rrc.receipt_rel_container_id) as plate_number,")
				.append(" case when cc.goods_type = 0 then IFNULL(SUM(rrc.normal_qty),0)+SUM(IFNULL(cf.quantity,0))  ")
				.append(" else IFNULL(SUM(rrc.damage_qty),0)+SUM(IFNULL(cf.quantity,0)) end as goods_qty ,c.location ,c.location_id ,IFNULL(c.location_type,1)")
				.append(" ,c.process from container_config cc ")
				.append(" join receipt_rel_container rrc on cc.container_config_id = rrc.container_config_id and rrc.receipt_line_id = cc.receipt_line_id ")
				.append(" join container c on c.con_id = rrc.plate_no and c.process = ").append(CreateContainerProcessKey.PROCESS_COUNT)
				.append(" join receipt_lines rls on rrc.receipt_line_id =rls.receipt_line_id ")
				.append(" join container_type ct  on ct.type_id=cc.pallet_type ")
				.append(" LEFT JOIN ship_to st ON st.ship_to_id = cc.ship_to_id")
				.append(" LEFT JOIN license_plate_type lpt on lpt.lpt_id = cc.license_plate_type_id")
				.append(" LEFT JOIN cc_from cf ON cf.container_id = rrc.plate_no ")
				.append(" where cc.receipt_line_id = ")
				.append(line_id);
			sql.append(" group by cc.container_config_id  ,c.process");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getInboundConfig() error:" + e);
		}
	}
	
	
	/**
	 *  获取 CC 和 Scan 创建的托盘数量
	 * @param receipt_id
	 * @return
	 * @throws Exception
	 */
	public int getContainerCreateInProcessCCOrScan(long receipt_id) throws Exception{
		int num = 0;
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT count(con.con_id) as qty FROM receipt_lines rl")
				.append(" JOIN receipt_rel_container rrc ON rl.receipt_line_id  = rrc.receipt_line_id AND rl.receipt_id = ")
				.append(receipt_id)
				.append(" JOIN container con on con.con_id= rrc.plate_no AND (con.process=1 OR con.process = 2)");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			
			if(row!=null){
				num = row.get("qty",0);
			}
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getContainerCreateInProcessCCOrScan() error:" + e);
		}
		
		return num;
		
	}
	
	public boolean compareInboundContainerFinallyContainer(long receipt_id) throws Exception{
		
		String inboundIds="";
		String finallyIds = "";
		
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT GROUP_CONCAT(con.con_id)as ids FROM receipt_lines rl")
				.append(" JOIN receipt_rel_container rrc ON rl.receipt_line_id  = rrc.receipt_line_id AND rl.receipt_id = ")
				.append(receipt_id)
				.append(" JOIN container con on con.con_id= rrc.plate_no AND  con.process=0");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			if(null!=row)
				inboundIds = row.get("ids", "");
			
			sql.setLength(0);
			sql.append(" SELECT GROUP_CONCAT(con.con_id)as ids FROM receipt_lines rl")
			.append(" JOIN receipt_rel_container rrc ON rl.receipt_line_id  = rrc.receipt_line_id AND rl.receipt_id = ")
			.append(receipt_id)
			.append(" JOIN container con on con.con_id= rrc.plate_no AND  con.is_delete = 0");
			DBRow row1 = dbUtilAutoTran.selectSingle(sql.toString());
			if(null!=row1)
				finallyIds = row1.get("ids", "");
			
			if(inboundIds.equals(finallyIds)){
				
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.compareInboundContainerFinallyContainer() error:" + e);
		}
	}
	
	public DBRow[] getCCConfig(long line_id) throws Exception{
	
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" select ct.type_name as pallet_name,rls.receipt_no ,cc.*,ifnull(st.ship_to_name,'') as ship_to_name , ifnull(lpt.lp_name,'') as lp_name , rrc.print_user as print_user_name,rrc.print_date , count(rrc.receipt_rel_container_id) as plate_number,")
				.append(" case when cc.goods_type = 0 then SUM(rrc.normal_qty)  ")
				.append(" else SUM(rrc.damage_qty) end as goods_qty ")
				.append(" ,c.process from container_config cc ")
				.append(" join receipt_rel_container rrc on cc.container_config_id = rrc.container_config_id and rrc.receipt_line_id = cc.receipt_line_id ")
				.append(" join container c on c.con_id = rrc.plate_no and c.process = ").append(CreateContainerProcessKey.PROCESS_CC)
				.append(" join receipt_lines rls on rrc.receipt_line_id =rls.receipt_line_id ")
				.append(" join container_type ct  on ct.type_id=cc.pallet_type ")
				.append(" LEFT JOIN ship_to st ON st.ship_to_id = cc.ship_to_id")
				.append(" LEFT JOIN license_plate_type lpt on lpt.lpt_id = cc.license_plate_type_id")
				.append(" where cc.receipt_line_id = ")
				.append(line_id);
			sql.append(" group by cc.container_config_id  ,c.process");
			return dbUtilAutoTran.selectMutliple(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getInboundConfig() error:" + e);
		}
		
	}
	
	public DBRow getLineByConid(long con_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT  rl.*  FROM receipt_rel_container rrc JOIN  receipt_lines rl ON rl.receipt_line_id =  rrc.receipt_line_id and rrc.plate_no = ")
				.append(con_id);
			return dbUtilAutoTran.selectSingle(sql.toString());
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getLineByConid() error:" + e);
		}
	}
	
	public String getShipToNamesByShipToIds(String shipToIds) throws Exception{
		try {
			if("0".equals(shipToIds) || "".equals(shipToIds)){
				return "";
			}
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT  GROUP_CONCAT(ship_to_name) as ship_to_names FROM ship_to WHERE ship_to_id IN (")
			.append(shipToIds).append(") ");
			DBRow row = dbUtilAutoTran.selectSingle(sql.toString());
			return null!=row ? row.get("ship_to_names", ""):"";
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getShipToNamesByShipToIds() error:" + e);
		}
	}
	
	public DBRow[] getLocaitonByPsId(String ps_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT slc.slc_id as id ,slc.slc_position as location_name , sla.area_name as area  FROM storage_location_catalog slc ")
				.append(" JOIN storage_location_area sla ON slc.slc_area = sla.area_id AND slc.slc_psid = ")
			.append(ps_id);
			DBRow[] row = dbUtilAutoTran.selectMutliple(sql.toString());
			return row;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getLocaitonByPsId() error:" + e);
		}
	}
	
	public DBRow[] getZoneByPsId(String ps_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
//			sql.append(" SELECT GROUP_CONCAT(tt.title_name) as titles , sla.area_name as area  FROM storage_location_area sla   ")
//				.append(" JOIN check_in_zone_with_title ct ON sla.area_id = ct.area_id   AND sla.area_psid =   ")
//				.append(ps_id).append(" JOIN title tt ON tt.title_id = ct.title_id     GROUP BY sla.area_id ORDER BY sla.area_id DESC ");
			sql.append("SELECT   a.area_name as area,  IFNULL(GROUP_CONCAT(tt.title_name),'NA') as titles FROM( ")
				.append(" SELECT  area_name,area_id FROM storage_location_area where area_psid=").append(ps_id)
				.append(" ) a LEFT JOIN check_in_zone_with_title ct ON a.area_id = ct.area_id")
				.append(" LEFT JOIN title tt ON tt.title_id = ct.title_id GROUP BY a.area_name ORDER BY a.area_id DESC ");
			DBRow[] row = dbUtilAutoTran.selectMutliple(sql.toString());
			return row;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getLocaitonByPsId() error:" + e);
		}
	}
	
	public DBRow[] getStorageByPsId(String ps_id) throws Exception{
		try {
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT id , location_name  FROM storage_load_unload_location where psc_id  = ").append(ps_id);
			DBRow[] row = dbUtilAutoTran.selectMutliple(sql.toString());
			return row;
		} catch (Exception e) {
			throw new Exception(
					"FloorReceiptsMgrZyy.getStorageByPsId() error:" + e);
		}
	}
	
	public long getPalletFloorLoadId () throws Exception{
		long id = 0;
		try{
			String sql = "select type_id from container_type where type_name ='PALLETFLOORLOAD'";
			DBRow row = dbUtilAutoTran.selectSingle(sql);
			
			if(null!= row ){
				id = row.get("type_id",0l);
			}
			
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getPalletFloorLoadId() error:" + e);
		}
		
		return id;
	}
	
	/**
	 * 获取Close RN 后的所有托盘信息
	 * @param line_id
	 * @return
	 * @throws Exception
	 */
	public DBRow[] getFinallyContainersByLineId(long line_id ) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT con.* FROM container con ")
				.append(" JOIN receipt_rel_container rrc ON con.con_id = rrc.plate_no AND rrc.receipt_line_id =")
				.append( line_id)
				.append(" order by con.location");
			
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getFinallyContainersByLineId() error:" + e);
		}
	}
	
	
	public DBRow[] getBreakPartialByLineId(long line_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT ct.type_name as pallet_name ,cc.*,rrc.plate_no as con_id, IFNULL(rrc.normal_qty,0)+IFNULL(rrc.damage_qty,0) as goods_qty,rrc.print_date,rrc.print_user,a.employe_name as print_user_name FROM cc_from cf")
				.append(" JOIN receipt_rel_container rrc ON cf.container_id = rrc.plate_no AND (rrc.normal_qty >0 or rrc.damage_qty >0) AND rrc.receipt_line_id =")
				.append( line_id)
				.append(" JOIN container_config cc ON rrc.container_config_id= cc.container_config_id")
				.append(" JOIN container_type ct ON ct.type_id = cc.pallet_type")
				.append(" LEFT JOIN admin a on a.adid = rrc.print_user");
				
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getBreakPartialByLineId() error:" + e);
		}
	}
	
	
	public DBRow[] getMovementByAdidAndTypeAndProcess(long adid,int type ,int process) throws Exception{
		
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT r.customer_id AS customer,r.receipt_no, r.receipt_id,sc.schedule_id,")
			.append(" r.supplier_id as title, assignUser.employe_name AS create_user, sc.create_time")
			.append(" FROM `schedule` sc JOIN schedule_sub ss ON ss.schedule_id = sc.schedule_id ")
			.append(" JOIN receipts r ON r.receipt_id = sc.associate_id ")
			.append(" LEFT JOIN admin assignUser ON assignUser.adid = sc.assign_user_id")
			.append(" WHERE sc.schedule_state != 10 AND sc.associate_type = ").append(type)
			.append(" AND sc.associate_process = ").append(process)
			.append(" AND ss.schedule_execute_id = ").append(adid);
			
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getMovementByAdidAndTypeAndProcess() error:" + e);
		}
	}
	
	
	public DBRow[] getCompanyByPsId(long ps_id) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append("SELECT DISTINCT company_id FROM wms_company_dic WHERE ps_id = ")
				.append(ps_id);
			
			DBRow[] rows = dbUtilAutoTran.selectMutliple(sql.toString());
			return rows;
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getCompanyByPsId() error:" + e);
		}
	}
	/**
	 * 为了打印 inbound 和new config 
	 * @param line_id
	 * @param process
	 * @return
	 * @throws Exception
	 */
	public DBRow [] getContainerByLineIdAndProcess(long line_id,int process) throws Exception{
		try{
			StringBuffer sql = new StringBuffer();
			sql.append(" SELECT con.con_id FROM container con JOIN receipt_rel_container rrc ON con.con_id  = rrc.plate_no  AND con.is_delete=0")
				.append(" AND rrc.receipt_line_id = ").append(line_id)
				.append(" AND con.process = ")
				.append(process)
				.append(" JOIN container_config cc ON cc.container_config_id = rrc.container_config_id ORDER BY cc.license_plate_type_id DESC");
				
			return  dbUtilAutoTran.selectMutliple(sql.toString());
		}catch(Exception e){
			throw new Exception(
					"FloorReceiptsMgrZyy.getContainerByLineIdAndProcess() error:" + e);
		}
	}
}
