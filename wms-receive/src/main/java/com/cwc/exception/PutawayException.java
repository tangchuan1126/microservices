/**
 * 
 */
package com.cwc.exception;

/**
 * @author 	Zhengziqi
 * 2015年4月11日
 *
 */
public class PutawayException extends RuntimeException{
	
	private static final long serialVersionUID = 7291855411593904320L;

	public PutawayException(String message){
	        super(message);
    }

}
