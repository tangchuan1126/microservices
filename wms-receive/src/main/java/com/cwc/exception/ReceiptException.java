package com.cwc.exception;
/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.cwc.exception.ReceiptException.java] 
 * @ClassName:    [ReceiptException]  
 * @Description:  [收货异常处理]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年7月21日 下午5:09:45]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年7月21日 下午5:09:45]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public class ReceiptException  extends RuntimeException {
	
	private int errCode ;
	private Object data;
	
	public ReceiptException(String msg ,int errCode,Object data){
		super(msg);
		this.errCode = errCode;
		this.data =  data;
	}
	
	public int getErrCode(){
		return errCode;
	}
	
	public Object getData(){
		return data;
	}

}
