package com.cwc.exception;


public class ReceiveToCreateProductException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	
	private Object data;
	
	public ReceiveToCreateProductException(String message){
		super(message);
	}

}
