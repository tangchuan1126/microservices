/**
 * 
 */
package com.cwc.exception;

/**
 * @author 	Zhengziqi
 * 2015年4月11日
 *
 */
public class ParamNeedConfirmException extends RuntimeException{
	
	private static final long serialVersionUID = 1850754284102172547L;

	public ParamNeedConfirmException(String message){
	        super(message);
    }

}
