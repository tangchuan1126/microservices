package com.cwc.exception;
/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.cwc.exception.SNRepeatException.java] 
 * @ClassName:    [SNRepeatException]  
 * @Description:  [验证SN是否重复异常]
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年7月9日 下午3:10:20]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年7月9日 下午3:10:20]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
public  class OutScheduleException  extends RuntimeException{
	/**
	 * 
	 */
	private Object data;
	private static final long serialVersionUID = 1L;

	public OutScheduleException(String message,Object data) {
		super(message);
		this.data = data;
	}

	public Object getData() {
		return data;
	}	
	
	
}
