package com.javaps.springboot;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Context;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran_Optm;
import com.cwc.init.StorageJetLagInit;
import com.radiadesign.catalina.session.RedisSessionHandlerValve;
import com.radiadesign.catalina.session.RedisSessionManager;

@Configuration
@ComponentScan({"com.javaps.springboot","com.receive.sync.controller","com.android.receive","com.android.cctask","com.android.bigscreen"})
@EnableAutoConfiguration
@ImportResource("classpath:applicationContext.xml")
public class MicroServiceApp implements Filter {
	
	@Value("${redis.host}")
	private String redis_host;
	
	@Value("${config.path}")
	private String configPath;
	
	@Autowired
	private StorageJetLagInit storageJetLagInit;
	@Autowired
	DBUtilAutoTran_Optm dbUtilAutoTran;
	public static void main(String[] args) {
		ConfigBean.putBeans("admin", "admin");
		DBRow.setKeyUseUpper(false); 
		DBRow.setKeyIgnoreCase(true);
		SpringApplication.run(MicroServiceApp.class, args);
	}

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		RedisSessionHandlerValve rshv = new RedisSessionHandlerValve();

		factory.addContextValves(rshv);
		factory.addContextCustomizers(new TomcatContextCustomizer() {

			@Override
			public void customize(Context context) {
				
				RedisSessionManager rsm = new RedisSessionManager();
				rsm.setHost(redis_host);
				rsm.setPort(6379);
				rsm.setDatabase(0);
				rsm.setMaxInactiveInterval(1800);
				rsm.setSerializationStrategyClass("com.radiadesign.catalina.session.JsonSerializer");
				
				context.setManager(rsm);
				context.setSessionCookiePath("/Sync10");
				context.setSessionCookiePathUsesTrailingSlash(true);
				context.setSessionTimeout(30);
				
				putBeans(configPath);
			}
		});

		return factory;
	}

	@Bean
	public FilterRegistrationBean authFilter() {
		
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(this);
		
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/*");
		
		filterRegBean.setUrlPatterns(urlPatterns);
		
		return filterRegBean;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,FilterChain chain) throws IOException, ServletException {
		
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		res.setCharacterEncoding("UTF-8");

		HttpSession sess = req.getSession(false);
		String url = req.getRequestURI();
		@SuppressWarnings("unchecked")
		Map<String, Object> m = sess == null ? null : (Map<String, Object>) sess.getAttribute("adminSesion");
		
		if(!noAuth(req)){
			if (m == null || !Boolean.TRUE.equals(m.get("login"))) {
				
				m = new HashMap<String, Object>();
				m.put("error", "未认证的访问");
				res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);	
				res.setContentType("application/json; charset=UTF-8");
				new ObjectMapper().writeValue(res.getWriter(), m);
				return;
			}
		}
		
		chain.doFilter(request, response);
	}
	
	private boolean noAuth(HttpServletRequest req){
		String url = req.getRequestURI();
		if(url.contains("/android/scheduleScreen")){
			return true;
		}
		return false;
	}
	
	// Filter implementation
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		try {
			storageJetLagInit.init(dbUtilAutoTran);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
	@Override
	public void destroy() {
	
	}
	
	/**
	 * 充填ConfigBean的数据库表字段
	 * @param urlMppingPath
	 */
	public static void putBeans(String urlMppingPath)
	{

		InputStream is=MicroServiceApp.class.getResourceAsStream(urlMppingPath);

		SAXReader reader = new SAXReader();
		Document document = null;
		try 
		{
			document = reader.read(is);
		}
		catch (DocumentException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Element root = document.getRootElement();
		
		String tagName = null;
		String tagValue = null;
		
		ArrayList elementAl = new ArrayList();
		
		for(Iterator i = root.elementIterator();i.hasNext(); )
		{
			Element element = (Element) i.next();
			
			tagName = element.attributeValue("name");
			tagValue = element.attributeValue("value");
			
			if (ConfigBean.getStringValue(tagName)==null)
			{
				ConfigBean.putBeans(tagName,tagValue);						
			}
	    }
	}
}