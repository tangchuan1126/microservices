package com.android.bigscreen;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.android.cctask.controller.CCTaskForAndroidController;
import com.android.receive.util.CommonUtils;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.key.BCSKey;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.app.key.ScheduleScreenKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.fr.third.org.apache.poi.util.StringUtil;

/**
 * @author 2015年3月4日
 *
 */
@Controller
@RestController
public class ScheduleForBigScreenController {

	@Autowired
	private FloorReceiptsMgrZyy floorReceiptsMgrZyy;

	/**
	 *
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/scheduleScreen", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String scheduleScreen(
			@RequestParam(value = "data", required = false, defaultValue = "0") String data,
			@RequestParam(value = "Method", required = false, defaultValue = "0") String method,
			@RequestParam(value = "pageNo", required = false, defaultValue = "0") int pageNo,
			@RequestParam(value = "pageSize", required = false, defaultValue = "0") int pageSize,
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			
			HttpSession session) throws Exception {
		
		// TODO finishCount
		
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if("multipleScreen".equals(method)){
				JSONObject jo = new JSONObject(data);
				try {
					JSONArray ja = jo.getJSONArray("data");
					int len = ja.length();
					for (int i = 0; i < len; i++) {
						JSONObject screen = ja.getJSONObject(i);
						String _method = screen.getString("method");
						PageCtrl pc = new PageCtrl();
		 				pageSize = screen.getInt("pageSize");
		 				pageNo = screen.getInt("pageNo");
		 				pc.setPageSize(pageSize);
		 				pc.setPageNo(pageNo);
		 				DBRow row = doMethod(_method, ps_id, pc);
		 				result.add(_method, row);
					}
				} catch (JSONException e) {
					ret = BCSKey.FAIL ;
					err = BCSKey.SYSTEMERROR;
				}
			}else{
				PageCtrl pc = new PageCtrl();
				pc.setPageSize(pageSize);
				pc.setPageNo(pageNo);
				result = doMethod(method,ps_id ,pc);
			}
			
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	/**
	 *
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/bigscreen/ccSchedule", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String ccSchedule(
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session) throws Exception {

		// TODO finishCount

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;

		try {
			DBRow[] rows = floorReceiptsMgrZyy.getScheduleByPsId(ps_id, CCTaskForAndroidController.CCTASK,  CCTaskForAndroidController.CCTASK_SUPERVISOR, null);
			for(DBRow r:rows){
				String startTime = r.get("startTime", "");
				Date now = new Date();
				Date locationDate = DateUtil.locationZoneTime(startTime, ps_id);
				r.add("total_time",( now.getTime()-locationDate.getTime())/1000);
			}
			result.add("data", rows);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	@Transactional
	@RequestMapping(value = "/android/bigscreen/scanSchedule", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String scanSchedule(
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session) throws Exception {

		// TODO finishCount

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;

		try {
			DBRow[] rows = floorReceiptsMgrZyy.getScheduleByPsId(ps_id, CCTaskForAndroidController.CCTASK,  CCTaskForAndroidController.CCTASK_SCANER, null);
			for(DBRow r:rows){
				String startTime = r.get("startTime", "");
				Date now = new Date();
				Date locationDate = DateUtil.locationZoneTime(startTime, ps_id);
				r.add("total_time",( now.getTime()-locationDate.getTime())/1000);
			}
			result.add("data", rows);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional
	@RequestMapping(value = "/android/bigscreen/scanningSchedule", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String scanningSchedule(
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session) throws Exception {
		
		// TODO finishCount
		
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try {
			DBRow[] rows = floorReceiptsMgrZyy.getScanningScheduleByPsId(ps_id, null);
			for(DBRow r:rows){
				String startTime = r.get("startTime", "");
				Date now = new Date();
				Date locationDate = DateUtil.locationZoneTime(startTime, ps_id);
				r.add("total_time",( now.getTime()-locationDate.getTime())/1000);
			}
			result.add("data", rows);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	
	private DBRow doMethod(String method, long ps_id, PageCtrl pc) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKey.SUCCESS ;
		int err = BCSKey.FAIL ;
		try{
			if(ScheduleScreenKey.CC_SCHEDULE.equals(method)){
				// CC 
				DBRow[] rows = floorReceiptsMgrZyy.getScheduleByPsId(ps_id, CCTaskForAndroidController.CCTASK,  CCTaskForAndroidController.CCTASK_SUPERVISOR, pc);
				for(DBRow r:rows){
					String startTime = r.get("startTime", "");
					Date now = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date locationDate = sdf.parse(startTime);
					r.add("total_time",( now.getTime()-locationDate.getTime())/(1000*60));
					
				}
				result.add("total", pc.getPageCount());
				result.add("data", rows);
				result.add("total_num", pc.getAllCount());
			}
			if(ScheduleScreenKey.SCAN_SCHEDULE.equals(method)){
				//scan
				DBRow[] rows = floorReceiptsMgrZyy.getScheduleByPsId(ps_id, CCTaskForAndroidController.CCTASK,  CCTaskForAndroidController.CCTASK_SCANER, pc);
				for(DBRow r:rows){
					String startTime = r.get("startTime", "");
					Date now = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date locationDate = sdf.parse(startTime);
					r.add("total_time",( now.getTime()-locationDate.getTime())/(1000*60));
				}
				result.add("total", pc.getPageCount());
				result.add("data", rows);
				result.add("total_num", pc.getAllCount());
			}
			if(ScheduleScreenKey.SCANNING_SCHEDULE.equals(method)){
				long time = System.currentTimeMillis();
				DBRow[] rows = floorReceiptsMgrZyy.getScanningScheduleByPsId(ps_id, pc);
				StringBuffer lineIds = new StringBuffer();
				for(DBRow r:rows){
					String startTime = r.get("startTime", "");
					Date now = new Date();
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date locationDate = sdf.parse(startTime);
					r.add("total_time",( now.getTime()-locationDate.getTime())/(1000*60));
					lineIds.append(r.get("receipt_line_id", "")).append(",");
					r.add("progress", 0);
				}
				if(lineIds.length()>0){
					DBRow[] spCounts = floorReceiptsMgrZyy.getSerialProductCountByLineIds(lineIds.substring(0, lineIds.length()-1));
					// 继续遍历
					
					for(DBRow r:rows){
						for(DBRow sp :spCounts){
							if(r.get("receipt_line_id", "").equals(sp.get("receipt_line_id",""))){
								int progress = (int)((100.0 * sp.get("spCount", 0))/ r.get("qty",1));
								r.add("progress", progress);
								break;
							}
						}
					}
				}
				result.add("total", pc.getPageCount());
				result.add("data", rows);
				result.add("total_num", pc.getAllCount());
			}
		}catch(Exception e){
			ret = BCSKey.FAIL ;
			err = BCSKey.SYSTEMERROR;
		}
		
		result.add("ret", ret);
		result.add("err", err);
		
		return  result;
	}
	
}
