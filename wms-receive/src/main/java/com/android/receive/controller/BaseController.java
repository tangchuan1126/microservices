package com.android.receive.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

import com.cwc.app.api.iface.zyy.ScheduleNoticesMgrIfaceZyy;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.floor.api.wfh.FloorReceiptsMgrWfh;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.floor.api.zzq.FloorReceiptsMgrZzq;

/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.android.receive.controller.BaseController.java] 
 * @ClassName:    [BaseController]  
 * @Description:  [控制层基类]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年7月21日 下午4:21:39]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年7月21日 下午4:21:39]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */
@Controller
public class BaseController {
	
	@Value("${notice.url}")
	protected String noticeURL;
	
	@Autowired
	protected FloorReceiptsMgrWfh floorReceiptsMgrWfh;
	
	@Autowired
	protected FloorReceiptsMgrZyy floorReceiptsMgrZyy;
	
	@Autowired
	protected FloorReceiptsMgrZzq floorReceiptsMgrZzq;
	
	@Autowired
	protected FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb;
	
	@Autowired
	protected ScheduleNoticesMgrIfaceZyy scheduleNoticesMgrZyy;
	
	protected Logger log = Logger.getLogger(this.getClass());

	public FloorReceiptSqlServerMgrSbb getFloorReceiptSqlServerMgrSbb() {
		return floorReceiptSqlServerMgrSbb;
	}

	public Logger getLog() {
		return log;
	}

	public void setFloorReceiptsMgrWfh(FloorReceiptsMgrWfh floorReceiptsMgrWfh) {
		this.floorReceiptsMgrWfh = floorReceiptsMgrWfh;
	}

	public void setFloorReceiptsMgrZyy(FloorReceiptsMgrZyy floorReceiptsMgrZyy) {
		this.floorReceiptsMgrZyy = floorReceiptsMgrZyy;
	}

	public void setFloorReceiptsMgrZzq(FloorReceiptsMgrZzq floorReceiptsMgrZzq) {
		this.floorReceiptsMgrZzq = floorReceiptsMgrZzq;
	}
	
	

	
}
