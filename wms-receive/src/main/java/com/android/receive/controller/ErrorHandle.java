package com.android.receive.controller;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.android.receive.util.CommonUtils;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.db.DBRow;

@RestController
public class ErrorHandle implements ErrorController{
	
	 private static final String PATH = "/error";
	 
	 @RequestMapping(value = PATH)
	 public String error() throws Exception {
		 DBRow error = new DBRow();
		 int ret = BCSKeyReceive.FAIL;
		 int err = BCSKeyReceive.SYSTEMERROR;
		 error.add("ret", ret);
		 error.add("err", err);
		 error.add("data", "System error!");
        return CommonUtils.convertDBRowsToJsonString(error);
	 }
	 @Override
	 public String getErrorPath() {
		return PATH;
	 }

}
