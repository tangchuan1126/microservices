package com.android.receive.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;
import javax.sound.sampled.Line;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.android.receive.util.CommonUtils;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.app.key.LineStatueKey;
import com.cwc.app.key.ProcessKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.ReceiveToCreateProductException;

/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.android.receive.controller.StorageLocationController.java] 
 * @ClassName:    [StorageLocationController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年8月5日 上午10:35:34]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年8月5日 上午10:35:34]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */

@Controller
@RestController
public class MovementController extends BaseController {
	
	
	public static  int ASSOCIATE_TYPE = 61;
	
	
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquireFinallyPallets", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String getZoneAndLoction(
			@RequestParam(value = "adid", required = false, defaultValue = "") long adid,
			@RequestParam(value = "ps_id", required = false, defaultValue = "") String ps_id,
			@RequestParam(value = "receipt_id", required = false, defaultValue = "0") long receipt_id
			) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if("".equals(ps_id) || 0 == receipt_id){
				throw new ReceiveForAndroidException("param error");
			}
			// TODO  根据receipt_id 获取所有的RN
			DBRow receipts = floorReceiptsMgrWfh.findReceiptById(receipt_id);
			// TODO 根据receipt_id 获取所有的Lines
			DBRow[] lines = floorReceiptsMgrWfh.findReceiptLineByReceiptIdAndQuantity(receipt_id);
			
			if(null== lines || lines.length==0 ){
				throw new  ReceiveForAndroidException("No datas");
			}
			
			Map<String , Boolean> locationExists = new HashMap<String, Boolean>();
			for(DBRow line :lines){
				
//				if(LineStatueKey.STATUS_SCAN==line.get("status",0)){
//					//如果没有扫描完，否则不能做movement
//					throw new ReceiveForAndroidException("Not allowed movemnet ,if not finished scan");
//				}
				
				//TODO  根据line_id 获取最终的托盘信息
				DBRow[]pallets = floorReceiptsMgrZyy.getFinallyContainersByLineId(line.get("receipt_line_id",0l));
				String company_id = line.get("company_id","");
				if(null!=pallets){
					for(DBRow row :pallets){
						String location = row.get("location","");
						String key = location+"_"+company_id;
						boolean flag =false;
						if(locationExists.containsKey(key)){
							flag = locationExists.get(key);
						}else{
							flag = floorReceiptSqlServerMgrSbb.checkLocationIsExist(row.get("location",""), line.get("company_id",""));
							locationExists.put(key, flag);
						}
						row.add("location_exists", flag?1:0);
					}
				}
				//TODO 
				line.add("pallets", pallets);
			}
			receipts.add("lines", lines);
			result.add("data", receipts);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/assignMovementTaskToSupervisor", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String assignMovementTaskToSupervisor(
			@RequestParam(value = "adid", required = false, defaultValue = "") long adid,
			@RequestParam(value = "operator", required = false, defaultValue = "") String operator,
			@RequestParam(value = "ps_id", required = false, defaultValue = "") String ps_id,
			@RequestParam(value = "receipt_id", required = false, defaultValue = "0") long receipt_id,
			HttpSession session
			)throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if("".equals(ps_id) || "".equals(operator)||0l==receipt_id){
				throw new ReceiveForAndroidException("param error");
			}
			DBRow receipt = floorReceiptsMgrWfh.findReceiptById(receipt_id);
			if(null==receipt){
				throw  new ReceiveForAndroidException("No RN");
			}
			String schedule_detail="Create Movement Task RN:"+receipt.get("receipt_no", "")+".";
			String schedule_overview="Movement Task:RN"+receipt.get("receipt_no", "")+".";
			
			DBRow schedule = createSchedule(adid, receipt_id, ASSOCIATE_TYPE, ProcessKey.MovementTask, schedule_detail, schedule_overview);
			//TODO 分配子任务
			createScheduleSub(schedule.get("schedule_id", 0l),operator);
			scheduleNoticesMgrZyy.noticeToOperator(adid, operator, schedule, noticeURL, session);
			
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/closeMovenmentSchedule", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String closeMovenmentSchedule(
			@RequestParam(value = "adid", required = false, defaultValue = "") long adid,
			@RequestParam(value = "ps_id", required = false, defaultValue = "") String ps_id,
			@RequestParam(value = "receipt_id", required = false, defaultValue = "0") long receipt_id,
			HttpSession session
			)throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if("".equals(ps_id)  ||0l==receipt_id){
				throw new ReceiveForAndroidException("param error");
			}
			DBRow schedule = floorReceiptsMgrZyy.getScheduleByReceipt(receipt_id, ASSOCIATE_TYPE, ProcessKey.MovementTask);
			if(schedule==null){
				throw new ReceiveForAndroidException("No schedule");
			}
			
			long schedule_id =schedule.get("schedule_id", 0l); 
			DBRow[] scheduleSub =floorReceiptsMgrWfh.findSheduleSubByScheduleID(schedule_id);
			//关闭子任务
			for (DBRow task : scheduleSub) {
				DBRow subRow = new DBRow();
				subRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
				subRow.add("is_task_finish", 1);
				subRow.add("schedule_finish_adid", adid);
				floorReceiptsMgrWfh.closeScheduleSub(task.get("schedule_sub_id", 0L), subRow);
			}
			DBRow  upRow = new DBRow();
			upRow.add("end_time", DateUtil.NowStr());
			upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
			floorReceiptsMgrWfh.closeSchedule(schedule_id, upRow);			
			
			
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/findMovementTaskList", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findMovementTaskList(
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session
			)throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if(0l==adid){
				throw new ReceiveForAndroidException("param error");
			}
			DBRow[] tasks = floorReceiptsMgrZyy.getMovementByAdidAndTypeAndProcess(adid,ASSOCIATE_TYPE, ProcessKey.MovementTask);
			
//			if(null!=tasks && tasks.length>0){
//				//TODO 查询RN
//				
//				for(DBRow row :tasks){
//					long receipt_id  =row.get("associate_id", 0l);
//					DBRow rn = floorReceiptsMgrWfh.findReceiptById(receipt_id);
//					row.add("receipt", rn);
//				}
//				
//			}
			result.add("data", tasks);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	
	
	/**
	 * 创建主任务
	 * @param adid
	 * @param receipt_id
	 * @param associate_type
	 * @param associate_process
	 * @param schedule_detail
	 * @param schedule_overview
	 * @return
	 * @throws Exception
	 */
	private DBRow  createSchedule(long adid,long receipt_id,int  associate_type,int associate_process,String schedule_detail,String schedule_overview) throws Exception{
		DBRow scheduleRow = new DBRow();
		scheduleRow.add("assign_user_id", adid);
		scheduleRow.add("schedule_detail", schedule_detail);// "Create MovementTask: "+receipt_id
		scheduleRow.add("schedule_state", 0F);
		scheduleRow.add("schedule_is_note", 1);
		scheduleRow.add("is_need_replay", 1);
		scheduleRow.add("is_update", 0);
		scheduleRow.add("is_all_day", 0);
		scheduleRow.add("is_schedule", 1);
		scheduleRow.add("schedule_overview", schedule_overview);// "MovementTask: " + receipt_id +" Need Assign");
		scheduleRow.add("associate_type", associate_type); // MovementTask  
		scheduleRow.add("associate_process", associate_process); 
		scheduleRow.add("associate_id", receipt_id);
		scheduleRow.add("sms_short_notify", 1);
		scheduleRow.add("sms_email_notify", 1);
		scheduleRow.add("is_task", 0);
		scheduleRow.add("associate_main_id", receipt_id);
		scheduleRow.add("create_time", DateUtil.NowStr());
		long scheduleID = floorReceiptsMgrWfh.addSchedule(scheduleRow);
		scheduleRow.add("schedule_id", scheduleID);
		return scheduleRow;
	}
	
	private void createScheduleSub(long scheduleID,String operator) throws Exception{
		//TODO 添加子任务  schedule_sub
		String[] supurvisor = operator.split(",");
		DBRow sbRow ;
		for (String s : supurvisor) {
			sbRow = new DBRow();
			sbRow.add("schedule_id", scheduleID);
			sbRow.add("schedule_execute_id", s);
			sbRow.add("schedule_state", 0);
			sbRow.add("is_task_finish", 0);
			floorReceiptsMgrWfh.addScheduleSub(sbRow);
		}
	}
}
