package com.android.receive.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.android.receive.util.CommonUtils;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.db.DBRow;
import com.cwc.exception.ReceiveToCreateProductException;

/**  
 * 
 * @ProjectName:  [wms-receive]
 * @Package:      [com.android.receive.controller.StorageLocationController.java] 
 * @ClassName:    [StorageLocationController]  
 * @Description:  [一句话描述该类的功能]  
 * @Author:       [赵永亚]  
 * @CreateDate:   [2015年8月5日 上午10:35:34]  
 * @UpdateUser:   [赵永亚]  
 * @UpdateDate:   [2015年8月5日 上午10:35:34]  
 * @UpdateRemark: [说明本次修改内容] 
 * @Version:      [v1.0]
 *   
 */

@Controller
@RestController
public class StorageLocationController {
	
	@Autowired
	private FloorReceiptsMgrZyy floorReceiptsMgrZyy;
	
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/getZoneAndLoction", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String getZoneAndLoction(
			@RequestParam(value = "ps_id", required = false, defaultValue = "") String ps_id)
	throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow rceiptNoRow = null;
		try {
			if("".equals(ps_id)){
				throw new ReceiveForAndroidException("param error");
			}
			Map<String ,DBRow> zoneMap = new HashMap<String, DBRow>();
			Map<String ,List<DBRow>> locationMap = new HashMap<String, List<DBRow>>();
			
			DBRow[] zoneTitles = floorReceiptsMgrZyy.getZoneByPsId(ps_id);
			for(DBRow row :zoneTitles){
				zoneMap.put(row.get("area", ""), row);
			}
			DBRow[] locations = floorReceiptsMgrZyy.getLocaitonByPsId(ps_id);
			for(DBRow row :locations){
				String key = row.get("area", "");
				List<DBRow> list = locationMap.get(key);
				if(null == list){
					list = new ArrayList<DBRow>();
				}
				row.remove("area");
				list.add(row);
				locationMap.put(key, list);
			}
			
			List<DBRow> rtnList = new ArrayList<DBRow>();
			Set<String> keys = locationMap.keySet();
			for(String key :keys){
				DBRow row = zoneMap.get(key);
				if(null!=row){
					row.add("locations", locationMap.get(key));
					rtnList.add(row);
				}
			}
			result.add("data", rtnList);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/getStorageByPsId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String getStorageByPsId(
			@RequestParam(value = "ps_id", required = false, defaultValue = "") String ps_id)
					throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if("".equals(ps_id)){
				throw new ReceiveForAndroidException("param error");
			}
			DBRow [] row   = floorReceiptsMgrZyy.getStorageByPsId(ps_id);
			result.add("data", row);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}
}
