package com.android.receive.controller;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.DeleteMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.multipart.FilePart;
import org.apache.commons.httpclient.methods.multipart.MultipartRequestEntity;
import org.apache.commons.httpclient.methods.multipart.Part;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.android.cctask.controller.CCTaskForAndroidController;
import com.android.receive.util.CommonUtils;
import com.cwc.app.api.iface.zyy.WritebackToWmsMgrInfaceZyy;
import com.cwc.app.exception.printTask.AndroidPrintServerUnLineException;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.floor.api.sbb.FloorReceiptMgrSbb;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.floor.api.wfh.FloorReceiptsMgrWfh;
import com.cwc.app.floor.api.zj.FloorTitleMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.floor.api.zr.FloorCheckInMgrZr;
import com.cwc.app.floor.api.zr.FloorPrintTaskMgrZr;
import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.floor.api.zyj.FloorProprietaryMgrZyj;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.floor.api.zzq.FloorReceiptsMgrZzq;
import com.cwc.app.invent.BaseController;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.app.key.CheckInChildDocumentsStatusTypeKey;
import com.cwc.app.key.CheckInLogTypeKey;
import com.cwc.app.key.ContainerStatueKey;
import com.cwc.app.key.ContainerTypeKey;
import com.cwc.app.key.CreateContainerProcessKey;
import com.cwc.app.key.GoodsTypeKeys;
import com.cwc.app.key.InvenIncreaseTypeKey;
import com.cwc.app.key.InvenOperateTypeKey;
import com.cwc.app.key.LengthUOMKey;
import com.cwc.app.key.LineStatueKey;
import com.cwc.app.key.LogServModelKey;
import com.cwc.app.key.PriceUOMKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.WeightUOMKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.OutScheduleException;
import com.cwc.exception.ParamNeedConfirmException;
import com.cwc.exception.ReceiveToCreateProductException;
import com.cwc.exception.SNRepeatException;

/**
 * @author Zhengziqi 2015年3月4日
 *
 */
@Controller
@RestController
public class ReceiptForAndroidController {

	public static int RefreshTimes = 10; // 刷新的时间间隔
	public static final int LETTER = 1; // 1表示letter
	public static final int LABEL = 0; // 0表示label
	public static final int VAILDATIONERROR = 90;
	public static final int STAGING_TYPE = 2;
	public static final int LOCATION_TYPE = 1;

	public static final int CONTAINER_CONFIG_TYPE = 1; // 通过托盘配置id查询托盘id
	public static final int RECEIPT_TYPE = 2; // 通过receipt_id查询托盘id
	public static final int RECEIPT_LINE_TYPE = 3; // 通过line_id查询托盘id
	public static final int RECEIPT_LINE_INBOUND = 4; // inbound
	public static final int RECEIPT_LINE_NEWCONFIG = 5; // new confing

	public static final int GOODS_TYPE_NORMAL = 0; // 放好的货物的托盘
	public static final int GOODS_TYPE_DAMAGE = 2; // 放坏的货物的托盘
	public static final int GOODS_TYPE_ALL = 3; // 查询全部的托盘

	public static final int SN_STATUE_NORMAL = 1; // 好的sn
	public static final int SN_STATUE_DAMAGE = 2; // 坏的sn
	public static final int SN_STATUE_ALL = 3; // 全部的sn

	public static final int CON_PUT_EMPTY = 1; // 空的托盘
	public static final int CON_PUT_LACK = 2; // 没放满的托盘
	public static final int CON_PUT_FULL = 3; // 放满的托盘
	public static final int CON_PUT_BEYOND = 4; // 放多的托盘

	@Value("${db.mysql.host}")
	private String db_mysql_host;

	@Value("${print.url}")
	private String print_url;

	@Value("${file.url}")
	private String file_url;

	@Value("${inventory.url}")
	private String inventory_url;

	@Autowired
	private BaseController invent;

	@Autowired
	private FloorReceiptsMgrZzq floorReceiptsMgrZzq;

	@Autowired
	private FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb;

	@Autowired
	private FloorReceiptsMgrWfh floorReceiptsMgrWfh;

	@Autowired
	private FloorReceiptsMgrZyy floorReceiptsMgrZyy;

	@Autowired
	private FloorReceiptMgrSbb floorReceiptMgr;

	@Autowired
	private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

	@Autowired
	private FloorPrintTaskMgrZr floorPrintTaskMgrZr;

	@Autowired
	private FloorCheckInMgrZwb floorCheckInMgrZwb;

	@Autowired
	private FloorCheckInMgrZr floorCheckInMgrZr;

	@Autowired
	private FloorAccountMgrSbb floorAccountMgr;

	@Autowired
	private FloorLogMgrIFace floorLogMgr;

	@Autowired
	private FloorProprietaryMgrZyj floorProprietaryMgrZyj;

	@Autowired
	private WritebackToWmsMgrInfaceZyy writebackToWmsMgrZyy;

	@Autowired
	private FloorContainerMgrZYZ floorContainerMgrZYZ;

	/**
	 * 获取RN所属Line信息 2015年3月4日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquireRNLineInfo", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireRNLineInfo(
			@RequestParam(value = "company_id", required = false, defaultValue = "") String company_id,
			@RequestParam(value = "receipt_no", required = false, defaultValue = "") String receipt_no,
			@RequestParam(value = "detail_id", required = false, defaultValue = "") String detail_id)

	throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow rceiptNoRow = null;
		try {
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(company_id);
			long ps_id = storage.get("ps_id", 0L);
			if ("".equals(company_id) || "".equals(receipt_no)
					|| "".equals(detail_id)) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 参数
			DBRow param = new DBRow();
			param.put("company_id", company_id);
			param.put("receipt_no", receipt_no);
			param.put("detail_id", detail_id);
			// add by zhaoyy add ps_id
			param.put("ps_id", ps_id);
			Map<String, Object> syncResult = synchronizationExpectedReceiptAndLines(param);
			if (!syncResult.containsKey("success")
					|| (Boolean) syncResult.get("success") != true) {
				throw new ReceiveForAndroidException(
						"Receipt NO. not found in WMS!");
			}

			// 参数
			DBRow paramReceipt = new DBRow();
			paramReceipt.add("detail_id", detail_id);
			paramReceipt.add("company_id", company_id);
			paramReceipt.add("receipt_no", receipt_no);
			JSONObject datas = new JSONObject();

			JSONObject receiptObject = new JSONObject();
			rceiptNoRow = floorReceiptsMgrZzq
					.queryReceiptInfoByRNAndDetailId(paramReceipt);

			receiptObject.put("receipt_id", rceiptNoRow.get("receipt_id"));
			receiptObject.put("ctnr", rceiptNoRow.get("container_no"));
			receiptObject.put("company_id", rceiptNoRow.get("company_id"));
			receiptObject.put("customer_id", rceiptNoRow.get("customer_id"));
			receiptObject.put("title", rceiptNoRow.get("supplier_id"));
			receiptObject.put("note", rceiptNoRow.get("detail_note"));
			receiptObject.put("receipt_no", rceiptNoRow.get("receipt_no"));
			receiptObject.put("status", rceiptNoRow.get("status"));
			receiptObject.put("receipt_ticket_printed",
					rceiptNoRow.get("receipt_ticket_printed"));

			JSONArray receiptLineArray = new JSONArray();
			DBRow paramRNRow = new DBRow();
			paramRNRow.add("receipt_id",
					Integer.valueOf(rceiptNoRow.getString("receipt_id")));
			DBRow[] receiptLineRows = floorReceiptsMgrZzq
					.queryReceiptLinesInfoByRNId(paramRNRow);
			// 更新line表pc_id字段
			List<DBRow> noPcIdLine = updateLinePCId(receiptLineRows);
			if (null != noPcIdLine) {
				//
				result.add("products", createProducts(noPcIdLine, rceiptNoRow));
				System.out
						.println("========throw  ReceiveToCreateProductException========");
				throw new ReceiveToCreateProductException(
						"Product not found! create product");
			}

			buildJSONArray(receiptLineArray, receiptLineRows, ps_id);
			receiptObject.put("lines", receiptLineArray);
			datas.put("receipt", receiptObject);
			result.add("datas", datas);

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (ReceiveToCreateProductException e) {
			// TODO 提示创建商品信息
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	private JSONArray createProducts(List<DBRow> noPcIdLine, DBRow rceiptNoRow)
			throws Exception {
		System.out.println("=============createProducts==============");
		JSONArray jsonArray = new JSONArray();
		int size = noPcIdLine.size();
		for (int i = 0; i < size; i++) {
			JSONObject json = new JSONObject();
			DBRow paramCus = new DBRow();
			paramCus.put("CompanyID", noPcIdLine.get(i).get("company_id"));
			paramCus.put("CustomerID", rceiptNoRow.get("customer_id"));
			paramCus.put("ItemID", noPcIdLine.get(i).get("item_id"));
			DBRow customer = floorReceiptSqlServerMgrSbb
					.getCustomerItems(paramCus);

			String item_id = noPcIdLine.get(i).get("item_id", "");
			String lot_no = noPcIdLine.get(i).get("lot_no", "");

			String _lot_no = ("None".equals(lot_no) || "".equals(lot_no)) ? ""
					: "/" + lot_no;
			String p_name = item_id + _lot_no;
			json.put("productName", p_name);
			json.put("mainCode", item_id);

			if (null != customer) {
				json.put("length", customer.get("length"));
				json.put("width", customer.get("width"));
				json.put("height", customer.get("height"));
				json.put("price", customer.get("unitprice"));
				json.put("upcCode", customer.get("upc"));
				boolean flag = (boolean) customer.get("serialnoscanlotnocheck");
				json.put("is_check_sn", flag ? 1 : 0);
				json.put("snLength", customer.get("serialnolength"));
				json.put("weight", customer.get("poundperpackage"));
				json.put("lengthUom", LengthUOMKey.INCH);
				json.put("weightUom", WeightUOMKey.LBS);
				json.put("priceUom", PriceUOMKey.USD);
			}

			DBRow[] tcArray = new DBRow[1];
			DBRow tc = new DBRow();
			// TODO 验证ID
			DBRow t = floorProprietaryMgrZyj
					.findProprietaryByTitleName(rceiptNoRow.get("supplier_id",
							""));
			DBRow c = floorProprietaryMgrZyj.getCustomerByName(rceiptNoRow.get(
					"customer_id", ""));
			if (t == null) {
				String supplier = rceiptNoRow.get("supplier_id", "");
				throw new ReceiveToCreateProductException(
						"Can not find any Proprietary for " + supplier);
			}

			if (c == null) {
				String customerId = rceiptNoRow.get("customer_id", "");
				throw new ReceiveToCreateProductException("No Customer for "
						+ customerId);
			}

			tc.add("title", t.get("title_id"));
			tc.add("customer", c.get("customer_key"));
			tcArray[0] = tc;
			DBRow[] file = new DBRow[0];
			json.put("tcArray", tcArray);
			json.put("file", file);
			jsonArray.put(json);
		}
		return jsonArray;
	}

	// 更新rn下所有line的pc_id
	private List<DBRow> updateLinePCId(DBRow[] receiptLineRows)
			throws Exception {
		List<DBRow> errRow = null;
		for (int i = 0, j = receiptLineRows.length; i < j; i++) {
			DBRow oneResult = receiptLineRows[i];
			String pc_id = oneResult.getString("pc_id");
			if ("".equals(pc_id)) {
				String productId = "";
				if ("".equals(oneResult.get("lot_no", ""))
						|| "None".equals(oneResult.get("lot_no", ""))) {
					productId = oneResult.get("item_id", "");
				} else {
					productId = oneResult.get("item_id", "") + "/"
							+ oneResult.get("lot_no", "");
				}
				DBRow[] product = floorReceiptMgr.getProductByPname(productId);
				// 暂时不验证product_code信息
				// edit by zhengziqi at June.25th 2015
				// //去product_code表查p_code
				// if(product == null || product.length < 1){
				// productId = oneResult.get("item_id", "");
				// product =
				// floorReceiptsMgrZzq.queryProductByMainCode(productId);
				// }

				DBRow row;
				if (product != null && product.length == 1) {
					row = new DBRow();
					row.put("pc_id", product[0].get("pc_id", -1L));
					// add by zhaoyy 2015年6月24日 14:33:34 更新 is_check_sn 和
					// sn_size
					int sn_size = product[0].get("sn_size", 0);
					int is_check_sn = sn_size == 0 ? 0 : 1;
					row.put("sn_size", sn_size);
					row.put("is_check_sn", is_check_sn);
					floorReceiptsMgrZzq.updateReceiptLine(
							oneResult.get("receipt_line_id", "0"), row);
					oneResult.add("pc_id", row.get("pc_id", 0));
				} else {
					// TODO VIZIO 则提示创建商品
					DBRow receipts = floorReceiptsMgrWfh
							.findReceiptById(oneResult.get("receipt_id", 0l));
					// edit by zhengziqi at July.1st 2015
					// if("VIZIO".equals(receipts.get("customer_id", ""))){
					if (null == errRow) {
						errRow = new ArrayList<DBRow>();
					}
					errRow.add(oneResult);
					// if(true){
					//
					// }else{
					// throw new
					// ReceiveForAndroidException("Product not found!");
					// }
				}
			}
		}
		return errRow;
	}

	/**
	 * 获取RN所属Line信息 2015年3月4日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *             acquireRNLineInfoByReceiptID
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquireRNLineInfoByReceiptID", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireRNLineInfoByReceiptID(
			@RequestParam(value = "receipt_no", required = false, defaultValue = "") String receipt_no,
			@RequestParam(value = "company_id", required = false, defaultValue = "") String company_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session)

	throws Exception {

		Map<String, Object> adminBean = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);
		long admin_ps_id = Long.valueOf(adminBean.get("ps_id").toString());
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if (receipt_no.equals("")) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			DBRow datas = new DBRow();
			DBRow[] rceiptNoRows = null;
			DBRow rceiptNoRow = null;
			if ("".equals(company_id)) {
				rceiptNoRows = floorReceiptsMgrWfh
						.queryReceiptInfoByRN(receipt_no);
			} else {
				rceiptNoRows = new DBRow[0]; // 为了不报错
				// 参数
				DBRow paramReceipt = new DBRow();
				paramReceipt.add("company_id", company_id);
				paramReceipt.add("receipt_no", receipt_no);
				rceiptNoRow = floorReceiptsMgrZzq
						.queryReceiptInfoByRN(paramReceipt);
			}
			if (rceiptNoRows.length == 0 && "".equals(company_id)) {
				result.add("ret", BCSKeyReceive.FAIL);
				result.add("err", VAILDATIONERROR);
				result.add("data", "Receipt number does not exist!");
				return CommonUtils.convertDBRowsToJsonString(result);
			}
			if (company_id.equals("") && rceiptNoRows.length > 1) {
				// TODO 加上ps_id 验证
				List<DBRow> list = new ArrayList<DBRow>();
				for (int i = 0, j = rceiptNoRows.length; i < j; i++) {
					long ps_id = rceiptNoRows[i].get("ps_id", 0l);
					if (ps_id == admin_ps_id) {
						list.add(rceiptNoRows[i]);
					}
				}
				int _list_size = list.size();

				if (null == list || _list_size == 0) {
					throw new ReceiveForAndroidException("Permission denied");
				} else if (_list_size == 1) {
					rceiptNoRow = list.get(0);
				} else {
					result.add("ret", BCSKeyReceive.SUCCESS);
					result.add("err", BCSKeyReceive.FAIL);
					datas.add("receipt", list.toArray(new DBRow[0]));
					datas.add("is_receipt_multiply", 1);
					result.add("datas", datas);
					return CommonUtils.convertDBRowsToJsonString(result);
				}

			} else if (rceiptNoRows.length == 1) {
				rceiptNoRow = rceiptNoRows[0];
			}
			if (null == rceiptNoRow) {
				throw new ReceiveForAndroidException(
						"Receipt number does not exist!");

			}
			if (rceiptNoRow.get("ps_id", 0l) != admin_ps_id) {
				throw new ReceiveForAndroidException("Permission denied");
			}
			JSONObject receiptObject = new JSONObject();
			receiptObject.put("receipt_id", rceiptNoRow.get("receipt_id"));
			receiptObject.put("ctnr", rceiptNoRow.get("container_no"));
			receiptObject.put("company_id", rceiptNoRow.get("company_id"));
			receiptObject.put("customer_id", rceiptNoRow.get("customer_id"));
			receiptObject.put("title", rceiptNoRow.get("supplier_id"));
			receiptObject.put("note", rceiptNoRow.get("note"));
			receiptObject.put("receipt_no", rceiptNoRow.get("receipt_no"));
			receiptObject.put("status", rceiptNoRow.get("status"));

			JSONArray receiptLineArray = new JSONArray();
			DBRow paramRNRow = new DBRow();
			paramRNRow.add("receipt_id",
					Integer.valueOf(rceiptNoRow.getString("receipt_id")));
			DBRow[] receiptLineRows = floorReceiptsMgrZzq
					.queryReceiptLinesInfoByRNId(paramRNRow);
			// DBRow receipt = floorReceiptsMgrWfh.findReceiptByNo(receipt_no);
			// update by zhaoyy 2015年7月14日 16:20:42 防止报错 修改成 an ID 去查询 同一个
			// receipt_no 可能搜索出多条信息，所以改成了ID
			DBRow receipt = floorReceiptsMgrWfh.findReceiptById(rceiptNoRow
					.get("receipt_id", 0l));
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			buildJSONArray(receiptLineArray, receiptLineRows, ps_id);

			DBRow[] dp = floorAccountMgr.getAccountDepartmentAndPost(adid);

			boolean isSupervisor = false;
			for (DBRow r : dp) {
				int postid = r.get("postid", 0);
				if (postid == 10) {
					isSupervisor = true;
					break;
				}
			}
			for (int i = 0, j = receiptLineArray.length(); i < j; i++) {
				JSONObject json = receiptLineArray.getJSONObject(i);
				json.put("is_supervisor", isSupervisor);
			}
			receiptObject.put("lines", receiptLineArray);
			datas.put("receipt", receiptObject);
			datas.put("is_receipt_multiply", 0);

			result.add("datas", datas);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		// 添加printServer
		// result.add("printServer", getPrintServerByLoginUser(ps_id, area_id,
		// LABEL));
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 根据参数创建pallet(TLP类型的container)，并关联到相应RN line上 2015年3月4日
	 * 
	 * @param parameter
	 * @throws Exception
	 *             update 创建托盘的是 加上 location_type 参数
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/createContainer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createContainer(
			@RequestParam(value = "config_type", required = false, defaultValue = "0") int config_type, // 1:Input
																										// Qty
																										// Per
																										// Pallet;2:Total
																										// [Remain]
																										// Qty
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "length", required = false, defaultValue = "0") int length,
			@RequestParam(value = "width", required = false, defaultValue = "0") int width,
			@RequestParam(value = "height", required = false, defaultValue = "0") int height,
			@RequestParam(value = "remainder_length", required = false, defaultValue = "0") int remainder_length,
			@RequestParam(value = "remainder_width", required = false, defaultValue = "0") int remainder_width,
			@RequestParam(value = "remainder_height", required = false, defaultValue = "0") int remainder_height,
			@RequestParam(value = "pallet_type", required = false, defaultValue = "0") long pallet_type, // container_type主键
			@RequestParam(value = "location", required = false, defaultValue = "") String location,
			@RequestParam(value = "location_type", required = false, defaultValue = "2") int location_type,
			@RequestParam(value = "location_id", required = false, defaultValue = "0") long location_id,
			@RequestParam(value = "note", required = false, defaultValue = "") String note,
			@RequestParam(value = "pallet_qty", required = false, defaultValue = "0") int pallet_qty,
			@RequestParam(value = "is_scan_create", required = false, defaultValue = "0") int is_scan_create,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "is_check_location", required = false, defaultValue = "1") long is_check_location,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type, // pallet的作用
																										// 0(正常的货物存放)
																										// 1(多出的货物存放)
																										// 2（损坏的货物存放）
			@RequestParam(value = "isTask", required = false, defaultValue = "0") int isTask, // 是否是CC
																								// Task
																								// 操作
																								// 1(是)
			@RequestParam(value = "is_return_plts", required = false, defaultValue = "0") int is_return_plts // 在count页面添加
	) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			if (config_type == 0 || receipt_line_id == 0 || "".equals(item_id)
					|| "".equals(lot_no) || adid == 0 || ps_id == 0
					|| isTask == 2) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			boolean flag = true;
			// 在ccTask config pallet时不能超过received qty ||
			// receive的时候如果选择创建一个pallet 数量就是line的预计收货数量expected_qty
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			DBRow sum_count = floorReceiptsMgrWfh.findSUMConfigByLineId(
					receipt_line_id, GOODS_TYPE_NORMAL); // 查找以前配置好的pallet数量
			DBRow receivedRow = floorReceiptsMgrWfh.findSUMReceivedQTY(
					receipt_line_id, GOODS_TYPE_ALL, false); // 查找以前count的
																// noromalqty的总数
			int total = line.get("expected_qty", 0); // receive时，config_type 为2时
														// 数量就是预计收货数量
														// expected_qty
			int normalQtySUM = receivedRow.get("normal_qty", 0);
			int config_total = sum_count.get("number", 0);
			int received_qty_now = length * width * height * pallet_qty;
			if (pallet_qty > 200) {
				throw new ReceiveForAndroidException("Create too many");
			}
			// 验证
			if (isTask == 1) { // 如果是在CCTask的时候重新配置托盘，则数量(L*W*H*palet_qty)不能超过received_qty
								// ?暂时不考虑CCTask创建
				if (received_qty_now + config_total > normalQtySUM) {
					flag = false;
					throw new ReceiveForAndroidException(
							"Quantity is greater than the received QTY configuration");
				}
				// if ("".equals(pallet_type)) {
				// flag = false;
				// throw new
				// ReceiveForAndroidException("Please select a pallet type");
				// }
			}
			Boolean is_check_location_staging = (location_type == 1) ? true
					: false;
			if (location_id == 0) {
				location_id = acquireLocationIdByLocationNameAndPsId(
						location_type, ps_id, location,
						is_check_location_staging);
				if (is_check_location == 1L
						&& !checkLocationIsExist(location,
								receipt.get("company_id", ""))) {
					throw new ParamNeedConfirmException(
							(location_type == 1) ? "Location " : "Staging "
									+ location
									+ " not found, confirm to commit?");
				}
			}

			//

			// 添加pallet 即某种类型的用几个 添加一次 如果为第三种方式 有余数了则按照余数创建一个托盘
			// int remainderQty = 0;
			if (flag) {
				StringBuffer log = new StringBuffer();
				if (goods_type == GOODS_TYPE_NORMAL)
					log.append("Create Pallet Config:|");
				else if (goods_type == GOODS_TYPE_DAMAGE) // 损坏的货物
					log.append("Create Pallet(Damage) Config:|");
				// 如果config_type 为1 代表输入几个就创建几个pallet 如果为2 则创建一个 L（总数） *1*1
				// 1:Input Qty Per Pallet;2:Total [Remain] Qty
				if (config_type == 2) {
					length = total - config_total;
					width = 1;
					height = 1;
					pallet_qty = 1;
				}
				log.append(length + "x" + width + "x" + height + "|");
				// if ("".equals(pallet_type)) {
				// log.append("per pallet: "
				// + (config_type == 1 ? pallet_qty : 1) + "|");
				// } else {
				log.append(pallet_type + ": "
						+ (config_type == 1 ? pallet_qty : 1) + "|");
				// }
				// 如果使用的是auto的配置则会将 configTotal % pallet_qry 重新创建一种配置
				if (remainder_length != 0) {
					log.append(remainder_length + "x1x1|");
					// if ("".equals(pallet_type)) {
					// log.append("per pallet: 1");
					// } else {
					log.append(pallet_type + ": 1");
					// }
				}

				// DateFormat df = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
				// df.setTimeZone("UTC");
				// // 添加日志
				DBRow taskLog = new DBRow();
				taskLog.add("operator_type", "Creat Pallet Config");
				taskLog.add("operator_id", adid);
				taskLog.add("operator_time", DateUtil.NowStr());
				taskLog.add("receipt_line_id", receipt_line_id);
				taskLog.add("receipt_line_status", line.get("status", 0));
				taskLog.add("data", log.substring(0, log.length() - 1));
				floorReceiptsMgrWfh.addTaskLog(taskLog);

				long cc_id = 0L;
				DBRow data = null;
				boolean createFlag = true;
				// edited by zhengziqi at 11th April, 2015
				// update plate_number in case of none configuration pallet
				int existConfigPalletNO = 0;
				int is_partial = 0;
				if (width * length * height == 0) {
					is_partial = 2; // 无配置类型托盘规格
				}
				// validate container_config exist
				// 只考虑普通tlp配置，无规格类型托盘配置
				DBRow[] configRow = floorReceiptsMgrZzq
						.queryContainerConfigByOwnParamsForTLP(receipt_line_id,
								null, length, width, height, is_partial);
				if (configRow != null && configRow.length == 1) {
					cc_id = configRow[0].get("container_config_id", 0L);
					// update by zhaoyy get("qty", 0)-》get("plate_number", 0);
					// 2015年6月1日 12:04:06
					existConfigPalletNO = configRow[0].get("plate_number", 0);
				} else {
					data = new DBRow();
					data.add("config_type", config_type);
					data.add("receipt_line_id", receipt_line_id);
					data.add("item_id", item_id);
					data.add("lot_no", lot_no);
					data.add("length", length);
					data.add("width", width);
					data.add("height", height);
					if (is_partial > 0) {
						data.add("is_partial", is_partial);
					}
					long _pallet_type = floorReceiptsMgrZyy
							.getPalletFloorLoadId();
					data.add("pallet_type", _pallet_type);
					data.add("note", note);
					data.add("create_user", adid);
					data.add("create_date", DateUtil.NowStr()); // 不知是否是utc时间？DateUtil.showUTCTime(DateUtil.NowStr(),
																// ps_id) 报错暂时不用
					data.add("goods_type", goods_type); // 托盘的作用
					// 添加 container_config
					cc_id = floorReceiptsMgrWfh.addContainerConfig(data);
				}

				// 如果是auto的方式需要多创建一种托盘配置，
				long remainder_cc_id = 0l;
				if (remainder_length != 0) { // 使用了auto 并且出现余数 则添加一种配置这种配置的规格就是
					data.add("length", remainder_length);
					data.add("width", 1);
					data.add("height", 1);
					data.add("is_partial", 1);
					remainder_cc_id = floorReceiptsMgrWfh
							.addContainerConfig(data);
				}
				// 添加container 和 receiptRelContainer
				// 如果是在scan的时候创建会在contianer_product表新建一条数量为0的数据
				if (detail_id == 0) {
					DBRow[] cons = floorReceiptsMgrWfh.findContainerByLineId(
							receipt_line_id, 3);
					for (DBRow con : cons) {
						if (con.get("detail_id", 0L) != 0) {
							detail_id = con.get("detail_id", 0L);
							break;
						}
					}
				}
				int process = (is_scan_create > 0) ? CreateContainerProcessKey.PROCESS_SCAN
						: CreateContainerProcessKey.PROCESS_COUNT;
				String con_ids = addContainerAndReceiptRelContainer(
						receipt_line_id, item_id, lot_no, location,
						location_type, location_id, pallet_qty, adid,
						line.get("company_id", ""), cc_id, ps_id, detail_id,
						is_scan_create, length * width * height,
						ContainerTypeKey.TLP, process, pallet_type);
				// 如果是auto的方式需要多创建一个托盘
				if (remainder_cc_id != 0) {
					con_ids += addContainerAndReceiptRelContainer(
							receipt_line_id, item_id, lot_no, location,
							location_type, location_id, 1, adid,
							line.get("company_id", ""), remainder_cc_id, ps_id,
							detail_id, is_scan_create, length * width * height,
							ContainerTypeKey.TLP, process, pallet_type);
				}
				// 更新receipt_line
				// DBRow receiptLine = new DBRow();
				// receiptLine.add("pallets", pallet_qty);
				// floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id,
				// receiptLine);
				con_ids = con_ids.substring(0, con_ids.length() - 1);
				DBRow[] containers = null;
				if (!con_ids.equals("")) {
					containers = floorReceiptsMgrWfh
							.findPalletByConIDS(con_ids);
				}
				if (is_return_plts == 1) {
					for (DBRow con : containers) {
						con.add("create_date",
								DateUtil.showLocalparseDateTo24Hours(
										con.get("create_date", ""), ps_id));
						con.add("wms_container_id", con.get("container_no", 0l));
						con.add("container_no",
								"(94)"
										+ CommonUtils.formatNumber(
												"#000000000000",
												con.get("container_no", 0l)));
					}
					result.add("containers", containers == null ? new DBRow[0]
							: containers);
				} else {
					// 返回container_config row
					DBRow row = floorReceiptsMgrWfh
							.findContainerConfigByConConfigId(cc_id); // 创建的配置
					DBRow remainderRow = floorReceiptsMgrWfh
							.findContainerConfigByConConfigId(remainder_cc_id); // auto方式创建的第二种配置
					if (remainderRow != null
							&& remainderRow.get("container_config_id", 0L) != 0) {
						remainderRow.add("plate_number", 1);
						remainderRow.add("receipt_no",
								receipt.get("receipt_no"));
					}
					row.add("plate_number", pallet_qty + existConfigPalletNO);
					row.add("receipt_no", receipt.get("receipt_no"));
					List<DBRow> returnRow = new ArrayList<DBRow>();
					returnRow.add(row);
					if (remainderRow != null)
						returnRow.add(remainderRow);
					result.add("container_config",
							returnRow.toArray(new DBRow[0]));
				}
				// update movements
				// added by zhengziqi at May.5th 2015
				// List<String> containerList =
				// Arrays.asList(con_ids.split(","));
				for (DBRow row : containers) {
					updateMysqlMovements(row.get("con_id", 0l), adid,
							receipt.get("company_id", ""), location_id,
							location, location_type);
				}
				result.add("success", "true");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (ParamNeedConfirmException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = VAILDATIONERROR;
			result.add("is_check_location", 0);
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * create clp palltes to target line 2015年5月12日
	 * 
	 * @param receipt_line_id
	 * @param item_id
	 * @param lot_no
	 * @param length
	 * @param width
	 * @param height
	 * @param pallet_type
	 * @param location
	 * @param location_type
	 *            1是location 2是 staging
	 * @param note
	 * @param pallet_qty
	 * @param license_plate_type_id
	 * @param adid
	 * @param is_return_plts
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/createContainerForCC", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createContainerForCC(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "length", required = false, defaultValue = "0") int length,
			@RequestParam(value = "width", required = false, defaultValue = "0") int width,
			@RequestParam(value = "height", required = false, defaultValue = "0") int height,
			@RequestParam(value = "location", required = false, defaultValue = "") String location,
			@RequestParam(value = "location_type", required = false, defaultValue = "2") int location_type,
			@RequestParam(value = "location_id", required = false, defaultValue = "2") long location_id,
			@RequestParam(value = "note", required = false, defaultValue = "") String note,
			@RequestParam(value = "pallet_qty", required = false, defaultValue = "0") int pallet_qty,
			@RequestParam(value = "license_plate_type_id", required = false, defaultValue = "0") int license_plate_type_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "ship_to", required = false, defaultValue = "0") long ship_to,
			@RequestParam(value = "is_check_location", required = false, defaultValue = "1") long is_check_location,
			@RequestParam(value = "is_return_plts", required = false, defaultValue = "0") int is_return_plts, // 在count页面添加
			@RequestParam(value = "remainder_length", required = false, defaultValue = "0") int remainder_length)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			int status = line.get("status", 0);
			String company_id = line.get("company_id", "");
			// validate parameter
			if (receipt_line_id == 0 || "".equals(item_id) || "".equals(lot_no)
					|| adid == 0 || ps_id == 0 || license_plate_type_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			if (pallet_qty > 200) {
				throw new ReceiveForAndroidException("Create too many");
			}
			// 验证位置信息
			// long location_id =
			// acquireLocationIdByLocationNameAndPsId(location_type, ps_id,
			// location, true);
			// TODO 验证 WMS location
			if ("".equals(line.get("cc_start_time", ""))) {
				DBRow upRow = new DBRow();
				upRow.add("cc_start_time", DateUtil.NowStr());
				upRow.add("cc_user", adid);
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
			}

			// 在ccTask config pallet时不能超过received qty
			if (checkContainerQtyWhenCreateCLP(receipt_line_id, length, width,
					height, pallet_qty, remainder_length)) {
				throw new ReceiveForAndroidException(
						"Quantity is greater than the received QTY configuration");
			}
			// 获取pallet_type
			DBRow palletTypeRow = floorReceiptsMgrZzq
					.queryPalletTypeByLicensePlateTypeId(license_plate_type_id);
			long container_type_id = 0L;
			String pallet_type_name = null;
			if (palletTypeRow != null && palletTypeRow.size() > 0) {
				pallet_type_name = palletTypeRow.get("type_name", "");
				container_type_id = palletTypeRow.get("type_id", 0L);
			}
			// add receipt_line_log
			addReceiptLineLog(receipt_line_id, length, width, height,
					pallet_qty, pallet_type_name, adid, status);
			long cc_id = 0L;
			int existPalletNumber = 0;
			// validate container_config exist
			// DBRow[] configRow =
			// floorReceiptsMgrZzq.queryContainerConfigByOwnParams(receipt_line_id,
			// null, license_plate_type_id, ship_to);
			DBRow[] configRow = floorReceiptsMgrZzq
					.queryContainerConfigByOwnParams(receipt_line_id, null, 0,
							ship_to);
			if (pallet_qty > 0) {

				for (DBRow config : configRow) {
					if (config.get("license_plate_type_id", 0) == license_plate_type_id) {
						cc_id = config.get("container_config_id", 0L);
						// 托盘的数量要重新计算，
						// existPalletNumber = configRow[0].get("plate_number",
						// 0);
					}
				}

				if (cc_id == 0l) {
					// 添加 container_config
					DBRow data = null;
					// edited by zhengziqi at 11th April, 2015
					// update plate_number in case of none configuration pallet
					data = new DBRow();
					data.add("receipt_line_id", receipt_line_id);
					data.add("item_id", item_id);
					data.add("lot_no", lot_no);
					data.add("length", length);
					data.add("width", width);
					data.add("height", height);
					data.add("pallet_type", container_type_id);
					data.add("note", note);
					data.add("create_user", adid);
					data.add("create_date", DateUtil.NowStr());
					data.add("goods_type", GOODS_TYPE_NORMAL); // 托盘的作用
					data.add("license_plate_type_id", license_plate_type_id);
					data.add("ship_to_id", ship_to);
					cc_id = floorReceiptsMgrWfh.addContainerConfig(data);
				}
			}
			// if(configRow != null && configRow.length == 1){
			// cc_id = configRow[0].get("container_config_id", 0L);
			// //update by zhaoyy get("qty", 0)-》get("plate_number", 0);
			// 2015年6月1日 12:04:06
			// existPalletNumber = configRow[0].get("plate_number", 0);
			// }else{
			// // 添加 container_config
			// DBRow data = null;
			// //edited by zhengziqi at 11th April, 2015
			// //update plate_number in case of none configuration pallet
			// data = new DBRow();
			// data.add("receipt_line_id", receipt_line_id);
			// data.add("item_id", item_id);
			// data.add("lot_no", lot_no);
			// data.add("length", length);
			// data.add("width", width);
			// data.add("height", height);
			// data.add("pallet_type", container_type_id);
			// data.add("note", note);
			// data.add("create_user", adid);
			// data.add("create_date", DateUtil.NowStr());
			// data.add("goods_type", GOODS_TYPE_NORMAL); // 托盘的作用
			// data.add("license_plate_type_id", license_plate_type_id);
			// data.add("ship_to_id", ship_to);
			// cc_id = floorReceiptsMgrWfh.addContainerConfig(data);
			// }
			// 添加container receipt_rel_container
			String con_ids = addContainerAndReceiptRelContainer(
					receipt_line_id, item_id, lot_no, location, location_type,
					location_id, pallet_qty, adid, company_id, cc_id, ps_id, 0,
					0, length * width * height, ContainerTypeKey.CLP,
					CreateContainerProcessKey.PROCESS_CC,
					// 应基础数据要求，container表的type_id字段在clp类型时，存储的是license_plate_type_id
					license_plate_type_id);

			long remainder_cc_id = 0l;
			if (remainder_length > 0) {
				for (DBRow config : configRow) {
					if (config.get("license_plate_type_id", 0) == 0
							&& config.get("length", 0) == remainder_length
							&& config.get("width", 0) == 1
							&& config.get("height", 0) == 1) {
						remainder_cc_id = config.get("container_config_id", 0L);
						// existPalletNumber = configRow[0].get("plate_number",
						// 0);
					}
				}

				if (remainder_cc_id == 0l) {
					DBRow data = new DBRow();
					data.add("length", remainder_length);
					data.add("width", 1);
					data.add("height", 1);
					data.add("is_partial", 1);
					data.add("receipt_line_id", receipt_line_id);
					data.add("item_id", item_id);
					data.add("lot_no", lot_no);

					// data.add("pallet_type", container_type_id);
					long _pallet_type = floorReceiptsMgrZyy
							.getPalletFloorLoadId();
					data.add("pallet_type", _pallet_type);
					data.add("note", note);
					data.add("create_user", adid);
					data.add("create_date", DateUtil.NowStr());
					data.add("goods_type", GOODS_TYPE_NORMAL); // 托盘的作用
					data.add("ship_to_id", 0L);
					remainder_cc_id = floorReceiptsMgrWfh
							.addContainerConfig(data);
				}
				if (remainder_cc_id != 0l) {
					con_ids += addContainerAndReceiptRelContainer(
							receipt_line_id, item_id, lot_no, location,
							location_type, location_id, 1, adid,
							line.get("company_id", ""), remainder_cc_id, ps_id,
							detail_id, 0, length * width * height,
							ContainerTypeKey.TLP,
							CreateContainerProcessKey.PROCESS_CC,
							container_type_id);
				}
			}
			if (con_ids.length() >= 0) {
				con_ids = con_ids.substring(0, con_ids.length() - 1);
			}

			// 构建返回的数据 //获取生成的托盘
			if (is_return_plts == 1) {
				DBRow[] containers = null;
				if (!con_ids.equals("")) {
					containers = floorReceiptsMgrWfh
							.findPalletByConIDS(con_ids);
				}
				for (DBRow con : containers) {
					con.add("create_date",
							DateUtil.showLocalparseDateTo24Hours(
									con.get("create_date", ""), ps_id));
					con.add("wms_container_id", con.get("container_no", 0l));
					con.add("container_no",
							"(94)"
									+ CommonUtils.formatNumber("#000000000000",
											con.get("container_no", 0l)));
				}
				result.add("containers", containers == null ? new DBRow[0]
						: containers);
			} else {
				// 返回container_config row
				List<DBRow> returnRow = new ArrayList<DBRow>();
				DBRow[] containerConfigRows = floorReceiptsMgrZzq
						.queryContainerConfigByOwnParams(receipt_line_id, "",
								0L, 0L);
				for (DBRow row : containerConfigRows) {
					String ship_to_ids = row.get("ship_to_ids", "");
					String[] ids = ship_to_ids.split(",");
					row.add("license_plate_type_id",
							row.get("license_plate_type_id", 0));
					if (Arrays.asList(ids).contains("0")) {
						row.add("ship_to_names", "");
					} else {
						row.add("ship_to_names", floorReceiptsMgrZyy
								.getShipToNamesByShipToIds(ship_to_ids));
					}
					//
					if (row.containsKey("process")
							&& row.get("process", 0) == CreateContainerProcessKey.PROCESS_CC) {
						// 只有CC 是 才区分
						if (row.get("is_partial", 0) == 1) {
							// 如果是 is_partial 的。
							DBRow[] palletIds = floorReceiptsMgrZyy
									.getPartialConIds(
											row.get("receipt_line_id", 0l),
											row.get("container_config_id", 0l),
											row.get("process", 0),
											row.get("is_partial", 0));
							for (DBRow conId : palletIds) {
								DBRow _row = (DBRow) row.clone();
								_row.add("con_id", conId.get("con_id", ""));
								_row.put("plate_number", 1);
								_row.add("keep_inbound", 0);
								returnRow.add(_row);
							}

						} else {
							// 不是 is_partial
							returnRow.add(row);
						}
					}
				}
				result.add("container_config", returnRow.toArray(new DBRow[0]));
			}
			// update movements
			// added by zhengziqi at May.5th 2015
			List<String> containerList = Arrays.asList(con_ids.split(","));
			for (String con_id : containerList) {
				updateMysqlMovements(Long.parseLong(con_id), adid,
						receipt.get("company_id", ""), location_id, location,
						location_type);
			}
			result.add("success", "true");
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	public void addReceiptLineLog(long receipt_line_id, int length, int width,
			int height, int pallet_qty, String pallet_type, long adid,
			int status) throws Exception {
		StringBuffer log = new StringBuffer();
		log.append("Create CLP Pallet Config:|");
		// 如果config_type 为1 代表输入几个就创建几个pallet 如果为2 则创建一个 L（总数） *1*1
		// 1:Input Qty Per Pallet;2:Total [Remain] Qty
		log.append(length + "x" + width + "x" + height + "|");
		log.append(pallet_type + ": " + pallet_qty + "|");
		// 添加日志
		DBRow taskLog = new DBRow();
		taskLog.add("operator_type", "Creat CLP Config");
		taskLog.add("operator_id", adid);
		taskLog.add("operator_time", DateUtil.NowStr());
		taskLog.add("receipt_line_id", receipt_line_id);

		taskLog.add("receipt_line_status", status);
		taskLog.add("data", log.substring(0, log.length() - 1));
		floorReceiptsMgrWfh.addTaskLog(taskLog);
	}

	public Boolean checkContainerQtyWhenCreateCLP(long receipt_line_id,
			int length, int width, int height, int pallet_qty,
			int remainder_length) throws Exception {

		// int normalCLPSUM =0;
		// DBRow[] qtys =
		// floorReceiptsMgrZyy.getPalletQtyByLineId(receipt_line_id);
		// if(null!=qtys && qtys.length>0){
		// for(DBRow r:qtys){
		// int container_type = r.get("container_type", 0);
		// if(container_type==1){
		// normalCLPSUM = r.get("expected_qty", 0);
		// }
		// }
		// }
		int countQTQ = floorReceiptsMgrZyy.getCountQTYByLineId(receipt_line_id);
		int keepInboundQTY = floorReceiptsMgrZyy
				.getInboundQTYByLineId(receipt_line_id);
		int ccQTY = floorReceiptsMgrZyy.getCCPalletQtyByLineId(receipt_line_id);
		// DBRow receivedByCount =
		// floorReceiptsMgrWfh.findSUMReceivedQTY(receipt_line_id,
		// GOODS_TYPE_NORMAL, false,CreateContainerProcessKey.PROCESS_COUNT); //
		// 查找以前count的 noromalqty的总数
		// int normalQtySUM = receivedByCount.get("normal_qty", 0);
		int received_qty_now = length * width * height * pallet_qty;
		// 如果是在CCTask的时候重新配置托盘，则数量(L*W*H*palet_qty)不能超过received_qty
		// ?暂时不考虑CCTask创建
		return received_qty_now + ccQTY + remainder_length + keepInboundQTY > countQTQ;
	}

	public Boolean checkLocationIsExist(String location, String company_id)
			throws Exception {
		return floorReceiptSqlServerMgrSbb.checkLocationIsExist(location,
				company_id);
	}

	/**
	 * scan的时候创建pallet
	 * 
	 * @param parameter
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/scanCreateContainer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String scanCreateContainer(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "length", required = false, defaultValue = "0") int length,
			@RequestParam(value = "width", required = false, defaultValue = "0") int width,
			@RequestParam(value = "height", required = false, defaultValue = "0") int height,
			@RequestParam(value = "pallet_type", required = false, defaultValue = "") String pallet_type,
			@RequestParam(value = "location", required = false, defaultValue = "") String location,
			@RequestParam(value = "note", required = false, defaultValue = "") String note,
			@RequestParam(value = "pallet_qty", required = false, defaultValue = "0") int pallet_qty,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type // pallet的作用
																										// 0(正常的货物存放)
																										// 1(多出的货物存放)
																										// 2（损坏的货物存放）
	) throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			if (receipt_line_id == 0 || "".equals(item_id) || "".equals(lot_no)
					|| length == 0 || width == 0 || height == 0 || adid == 0
					|| ps_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 不能创建过多
			if (pallet_qty > 200) {
				throw new ReceiveForAndroidException("Create too many");
			}

			// 添加 config damge以前有配置的话不添加新的配置会更新到现有的damge配置下
			long cc_id = 0L;
			DBRow damageConfig = floorReceiptsMgrWfh
					.findDamageContainerConfigByLineId(receipt_line_id,
							GOODS_TYPE_DAMAGE);
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			if (goods_type == GOODS_TYPE_DAMAGE && damageConfig != null) {
				cc_id = damageConfig.get("container_config_id", 0L);
			} else { // 否则需要添加新的配置不论damage还是正常
				// 添加日志
				StringBuffer log = new StringBuffer();
				if (goods_type == GOODS_TYPE_NORMAL)
					log.append("Create Pallet Config:|");
				else if (goods_type == GOODS_TYPE_DAMAGE) // 损坏的货物
					log.append("Create Pallet(Damage) Config:|");

				// log.append(data.get("length", 0) + "X" + data.get("width",
				// 0)+ "X" + data.get("height", 0) + "|");
				log.append(pallet_type + ": " + pallet_qty);
				DBRow taskLog = new DBRow();
				taskLog.add("operator_type", "Creat Pallet Config");
				taskLog.add("operator_id", adid);
				taskLog.add("operator_time", DateUtil.NowStr());
				taskLog.add("receipt_line_id", receipt_line_id);
				taskLog.add("receipt_line_status", line.get("status", 0));
				taskLog.add("data", log.substring(0, log.length() - 1));
				floorReceiptsMgrWfh.addTaskLog(taskLog);

				DBRow data = new DBRow();
				data.add("receipt_line_id", receipt_line_id);
				data.add("item_id", item_id);
				data.add("lot_no", lot_no);
				if (goods_type == 2) {
					data.add("length", 0);
					data.add("width", 0);
					data.add("height", 0);
				} else {
					data.add("length", length);
					data.add("width", width);
					data.add("height", height);
				}
				data.add("pallet_type", pallet_type);
				data.add("create_user", adid);
				data.add("create_date", DateUtil.NowStr()); // 不知是否是utc时间？DateUtil.showUTCTime(DateUtil.NowStr(),
															// ps_id) 报错暂时不用
				data.add("goods_type", goods_type); // 托盘的作用

				// 添加 container_config
				cc_id = floorReceiptsMgrWfh.addContainerConfig(data);
			}
			// 添加contianer
			String con_ids = "";
			for (int i = 0; i < pallet_qty; i++) {
				int palletSequence = floorReceiptSqlServerMgrSbb
						.getPalletNo(line.getString("company_id"));
				DBRow container = new DBRow();
				container.add("container_type", ContainerTypeKey.TLP);
				container.add("item_id", item_id);
				container.add("lot_number", lot_no);
				container.add("create_user", adid);
				container.add("create_date", DateUtil.NowStr());
				container.add("container", palletSequence);
				container.add("location", location);
				container.add("detail_id", detail_id);
				long con_id = floorReceiptsMgrWfh.addContainer(container);
				con_ids += con_id + ",";
				// 添加 receipt_rel_container
				DBRow receiptRelContainer = new DBRow();
				if (goods_type == 2) {
					receiptRelContainer.add("damage_qty", width * length
							* height);
				} else {
					receiptRelContainer.add("normal_qty", width * length
							* height);
				}
				receiptRelContainer.add("receipt_line_id", receipt_line_id);
				receiptRelContainer.add("plate_no", con_id);
				receiptRelContainer.add("container_config_id", cc_id);
				receiptRelContainer.add("detail_id", detail_id);
				floorReceiptsMgrWfh.addReceiptRelContainer(receiptRelContainer);
				// 添加container_product
				DBRow upRow = new DBRow();
				upRow.add("cp_quantity",
						goods_type == GOODS_TYPE_DAMAGE ? length : width
								* height * length);
				upRow.add("cp_pc_id", 0); // 默认值
				upRow.add("cp_lp_id", con_id);
				this.floorReceiptsMgrWfh.bindQTYToPallet(upRow);
			}
			DBRow[] containers = null;
			if (!con_ids.equals("")) {
				con_ids = con_ids.substring(0, con_ids.length() - 1);
				containers = floorReceiptsMgrWfh.findPalletByConIDS(con_ids);
				for (DBRow con : containers) {
					con.add("create_date",
							DateUtil.showLocalparseDateTo24Hours(
									con.get("create_date", ""), ps_id));
					con.add("wms_container_id", con.get("container_no", 0l));
					con.add("container_no",
							"(94)"
									+ CommonUtils.formatNumber("#000000000000",
											con.get("container_no", 0l)));
				}
			}
			result.add("containers", containers == null ? new DBRow[0]
					: containers);

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	// 添加container receiptRelContianer
	private String addContainerAndReceiptRelContainer(long receipt_line_id,
			String item_id, String lot_no, String location, int location_type,
			long location_id, int pallet_qty, long adid, String company_id,
			long cc_id, long ps_id, long detail_id, int is_scan_create,
			int count, int container_type, int process, long container_type_id)
			throws Exception {
		// 从wms获取pallet sequence，将其保存至container表的container字段

		// 添加container receipt_rel_container
		DBRow line = floorReceiptsMgrWfh
				.findReceiptLineByLineId(receipt_line_id);
		DBRow receipt = floorReceiptsMgrWfh
				.findReceiptByLineId(receipt_line_id);
		DBRow customer = floorReceiptsMgrWfh.findCustomerByCustomer(receipt
				.get("customer_id", ""));
		if (customer == null) {
			customer = new DBRow();
		}

		String con_ids = "";
		for (int i = 0; i < pallet_qty; i++) {
			int palletSequence = floorReceiptSqlServerMgrSbb.getPalletNo(company_id);
			DBRow container = new DBRow();
			container.add("container_type", container_type);
			container.add("item_id", item_id);
			container.add("lot_number", lot_no);
			container.add("create_user", adid);
			container.add("create_date", DateUtil.NowStr());
			container.add("container", palletSequence);
			container.add("location", location);
			container.add("location_id", location_id);
			container.add("location_type", location_type);

			container.add("customer_id", customer.get("customer_key", 0));
			container.add("title_id", receipt.get("title_id", ""));
			container.add("detail_id", detail_id);
			container.add("is_has_sn", (line.get("is_check_sn", 0) == 1) ? 1
					: 2);
			container.add("at_ps_id", ps_id);
			container.add("process", process);
			if (container_type_id > 0) {
				container.add("type_id", container_type_id);
			}
			long con_id = floorReceiptsMgrWfh.addContainer(container);
			con_ids += con_id + ",";
			// 添加 receipt_rel_container
			DBRow receiptRelContainer = new DBRow();
			receiptRelContainer.add("receipt_line_id", receipt_line_id);
			receiptRelContainer.add("plate_no", con_id);
			receiptRelContainer.add("container_config_id", cc_id);
			receiptRelContainer.add("detail_id", detail_id);
			floorReceiptsMgrWfh.addReceiptRelContainer(receiptRelContainer);
			// scan的时候创建会在container_product加一条为0的数据
			if (is_scan_create == 1) {
				DBRow containerProduct = new DBRow();
				containerProduct.add("cp_quantity", count);
				containerProduct.add("cp_pc_id", line.get("pc_id", 0)); // 默认值
				containerProduct.add("cp_lp_id", con_id);
				this.floorReceiptsMgrWfh.bindQTYToPallet(containerProduct);
				// 更改receipe_rel_container
				DBRow upRow = new DBRow();
				upRow.add("normal_qty", count);
				upRow.add("damage_qty", 0);
				floorReceiptsMgrWfh.updateReceiptRelContainerByConID(con_id,
						upRow);
			}
		}
		return con_ids;
	}

	private DBRow getContainer(long receipt_line_id, String item_id,
			String lot_no, String location, int location_type,
			long location_id, int pallet_qty, long adid, String company_id,
			long cc_id, long ps_id, long detail_id, int is_scan_create,
			int count, int container_type, int process, long container_type_id,
			DBRow customer) {
		// DBRow container = new DBRow();
		// container.add("container_type", container_type);
		// container.add("item_id", item_id);
		// container.add("lot_number", lot_no);
		// container.add("create_user", adid);
		// container.add("create_date", DateUtil.NowStr());
		// container.add("container", palletSequence);
		// container.add("location", location);
		// container.add("location_type", location_type);
		//
		// container.add("customer_id", customer.get("customer_key", 0));
		// container.add("title_id", receipt.get("title_id", ""));
		// container.add("detail_id", detail_id);
		// container.add("is_has_sn", (line.get("is_check_sn", 0) == 1)? 1: 2);
		// container.add("at_ps_id", ps_id);
		// container.add("process", process);
		// if(container_type_id > 0){
		// container.add("type_id", container_type_id);
		// }
		return null;
	}

	/**
	 * 创建 damage container
	 * 
	 * @param parameter
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/createDamageContainer", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createDamageContainer(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "pallet_type", required = false, defaultValue = "0") long pallet_type,
			@RequestParam(value = "location", required = false, defaultValue = "") String location,
			@RequestParam(value = "location_type", required = false, defaultValue = "2") int location_type,
			@RequestParam(value = "location_id", required = false, defaultValue = "0") long location_id,
			@RequestParam(value = "pallet_qty", required = false, defaultValue = "0") int pallet_qty,
			@RequestParam(value = "is_scan_create", required = false, defaultValue = "0") int is_scan_create,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			// @RequestParam(value = "type_id", required = false, defaultValue =
			// "0") long type_id,
			@RequestParam(value = "is_check_location", required = false, defaultValue = "1") long is_check_location,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type // pallet的作用
																										// 0(正常的货物存放)
																										// 1(多出的货物存放)
																										// 2（损坏的货物存放）
	) throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			// 验证参数
			if (receipt_line_id == 0 || item_id.equals("") || lot_no.equals("")
					|| adid == 0 || goods_type != 2) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			boolean flag = true;
			if (pallet_qty > 200) {
				throw new ReceiveForAndroidException("Create too many");
			}
			// 验证位置信息
			if (location_id == 0) {
				location_id = acquireLocationIdByLocationNameAndPsId(
						location_type, ps_id, location, false);
				if (is_check_location == 1L
						&& !checkLocationIsExist(location,
								receipt.get("company_id", ""))) {
					throw new ParamNeedConfirmException("Location " + location
							+ " not found, confirm to commit?");
				}
			}

			if (flag) {
				DBRow line = floorReceiptsMgrWfh
						.findReceiptLineByLineId(receipt_line_id);
				DBRow damageConfig = floorReceiptsMgrWfh
						.findDamageContainerConfigByLineId(receipt_line_id, 2);
				long cc_id = 0L;
				if (damageConfig == null) {
					// 添加日志
					StringBuffer log = new StringBuffer();
					if (goods_type == GOODS_TYPE_NORMAL)
						log.append("Create Pallet Config:|");
					else if (goods_type == GOODS_TYPE_DAMAGE) // 损坏的货物
						log.append("Create Pallet(Damage) Config:|");

					// log.append(data.get("length", 0) + "X" +
					// data.get("width", 0)+ "X" + data.get("height", 0) + "|");
					log.append("pallet_type" + ": " + pallet_type);
					DBRow taskLog = new DBRow();
					taskLog.add("operator_type", "Creat Pallet Config");
					taskLog.add("operator_id", adid);
					taskLog.add("operator_time", DateUtil.NowStr());
					taskLog.add("receipt_line_id", receipt_line_id);
					taskLog.add("receipt_line_status", line.get("status", 0));
					taskLog.add("data", log.substring(0, log.length() - 1));
					floorReceiptsMgrWfh.addTaskLog(taskLog);

					DBRow data = new DBRow();
					data.add("receipt_line_id", receipt_line_id);
					data.add("item_id", item_id);
					data.add("lot_no", lot_no);
					data.add("length", 0);
					data.add("width", 0);
					data.add("height", 0);
					data.add("pallet_type", pallet_type);
					data.add("create_user", adid);
					data.add("create_date", DateUtil.NowStr()); // 不知是否是utc时间？DateUtil.showUTCTime(DateUtil.NowStr(),
																// ps_id) 报错暂时不用
					data.add("goods_type", goods_type); // 托盘的作用

					// 添加 container_config
					cc_id = floorReceiptsMgrWfh.addContainerConfig(data);
				} else {
					cc_id = damageConfig.get("container_config_id", 0L);
				}

				// 添加container
				String con_ids = "";
				if (detail_id == 0) {
					DBRow[] containers = floorReceiptsMgrWfh
							.findContainerByLineId(receipt_line_id, 0);
					for (DBRow con : containers) {
						if (con.get("detail_id", 0L) != 0) {
							detail_id = con.get("detail_id", 0L);
							break;
						}
					}
				}
				int process = (is_scan_create > 0) ? CreateContainerProcessKey.PROCESS_SCAN
						: CreateContainerProcessKey.PROCESS_COUNT;

				for (int i = 0; i < pallet_qty; i++) {
					int palletSequence = floorReceiptSqlServerMgrSbb
							.getPalletNo(line.getString("company_id"));
					DBRow container = new DBRow();
					DBRow customer = floorReceiptsMgrWfh
							.findCustomerByCustomer(receipt.get("customer_id",
									""));
					if (customer == null) {
						customer = new DBRow();
					}
					container.add("container_type", ContainerTypeKey.TLP);
					container.add("item_id", item_id);
					container.add("lot_number", lot_no);
					container.add("create_user", adid);
					container.add("create_date", DateUtil.NowStr());
					container.add("container", palletSequence);
					container.add("type_id", pallet_type);
					container.add("location", location);
					container.add("location_id", location_id);
					container.add("location_type", location_type);
					container.add("detail_id", detail_id);
					container.add("customer_id",
							customer.get("customer_key", 0));// receipt.get("customer_id",
																// ""));
					container.add("title_id", receipt.get("title_id", 0));
					container.add("process", process);
					container.add("is_has_sn",
							(line.get("is_check_sn", 0) == 1) ? 1 : 2);
					container.add("at_ps_id", ps_id);

					long con_id = floorReceiptsMgrWfh.addContainer(container);
					con_ids += con_id + ",";
					// 添加 receipt_rel_container
					DBRow receiptRelContainer = new DBRow();
					receiptRelContainer.add("receipt_line_id", receipt_line_id);
					receiptRelContainer.add("plate_no", con_id);
					receiptRelContainer.add("container_config_id", cc_id);
					receiptRelContainer.add("detail_id", detail_id);
					floorReceiptsMgrWfh
							.addReceiptRelContainer(receiptRelContainer);
					// 添加container_product
					if (is_scan_create == 1) {
						DBRow containerProduct = new DBRow();
						containerProduct.add("cp_quantity", 0);
						containerProduct.add("cp_pc_id", line.get("pc_id", 0)); // 默认值
						containerProduct.add("cp_lp_id", con_id);
						this.floorReceiptsMgrWfh
								.bindQTYToPallet(containerProduct);
					}
					// update movements
					// added by zhengziqi at May.5th 2015
					updateMysqlMovements(con_id, adid,
							receipt.get("company_id", ""), location_id,
							location, location_type);
				}
				// 返回container_config row
				List<DBRow> damage = new ArrayList<DBRow>();
				DBRow[] row = floorReceiptsMgrWfh.findPalletByLineId(
						receipt_line_id, 0L, GOODS_TYPE_DAMAGE);
				con_ids = con_ids.substring(0, con_ids.length() - 1);
				for (DBRow dbRow : row) {
					if (con_ids.contains(dbRow.get("con_id", ""))) {
						// DBRow[] photos =
						// floorReceiptsMgrWfh.findPalletPhotoByDetailId(line.get("detail_id",
						// 0L), dbRow.get("con_id", 0L));
						dbRow.add(
								"create_time",
								DateUtil.showLocalparseDateTo24Hours(
										dbRow.get("create_time", ""), ps_id));
						dbRow.add("photos", null);
						dbRow.add("wms_container_id",
								dbRow.get("container_no", 0l));
						dbRow.add(
								"container_no",
								"(94)"
										+ CommonUtils.formatNumber(
												"#000000000000",
												dbRow.get("container_no", 0l)));
						damage.add(dbRow);
					}
				}

				result.add("damage_container", damage.toArray(new DBRow[0]));
				result.add("success", "true");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (ParamNeedConfirmException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = VAILDATIONERROR;
			result.add("is_check_location", 0);
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 根据line id以及configID查询出line下这个palletConfig的配置信息所属的pallets信息 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquirePalletsInfoByLine", produces = "application/json; charset=UTF-8")
	public String acquirePalletsInfoByLine(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "container_config_id", required = false, defaultValue = "0") long container_config_id,
			@RequestParam(value = "", required = false, defaultValue = "0") int is_start_scan,
			@RequestParam(value = "", required = false, defaultValue = "-1") int process,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid)
			throws Exception {
		System.out.println("============receipt_line_id:" + receipt_line_id
				+ "=================");
		System.out.println("============container_config_id:"
				+ container_config_id + "=================");
		System.out.println("============is_start_scan:" + is_start_scan
				+ "=================");
		System.out.println("============process:" + process
				+ "=================");
		System.out.println("============adid:" + adid + "=================");

		long time = System.currentTimeMillis();
		DBRow receipt = floorReceiptsMgrWfh
				.findReceiptByLineId(receipt_line_id);
		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get(
				"company_id", ""));
		long ps_id = storage.get("ps_id", 0L);
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 验证参数
			if (receipt_line_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}

			// 第一次进入scan pallet 写入时间 记录时间
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			if (is_start_scan == 1) {

				DBRow upRow = new DBRow();
				upRow.add("scan_start_time", DateUtil.NowStr());
				upRow.add("scan_start_adid", adid);
				upRow.add("status", LineStatueKey.STATUS_SCAN);
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
				// 将所有的托盘都置为open
				floorReceiptsMgrWfh.updateContainerStatusByLineID(
						receipt_line_id, ContainerStatueKey.CON_OPEN);
				// 将RN的状态改为Open
				upRow = new DBRow();
				upRow.add("status", "Open");
				floorReceiptsMgrZzq.updateReceiptStatus(
						line.get("receipt_id", ""), upRow);
				// TODO add schedule start_time and
				DBRow schedules = floorReceiptsMgrZyy.getScheduleByReceipt(
						receipt_line_id, CCTaskForAndroidController.CCTASK,
						CCTaskForAndroidController.CCTASK_SCANER);
				if (null != schedules) {
					upRow = new DBRow();
					upRow.add("start_time", DateUtil.NowStr());
					floorReceiptsMgrWfh.closeSchedule(
							schedules.get("schedule_id", 0l), upRow);
				}
			}
			DBRow[] pallets = this.floorReceiptsMgrWfh.findPalletByLineId(
					receipt_line_id, container_config_id, GOODS_TYPE_ALL,
					process);
			List<DBRow> palletArray = new ArrayList<DBRow>();
			List<DBRow> clppalletArray = new ArrayList<DBRow>();
			List<DBRow> tlppalletArray = new ArrayList<DBRow>();

			List<DBRow> damagedArray = new ArrayList<DBRow>();

			DBRow[] rnSNRows = floorReceiptsMgrWfh.findScanedSNBySns(line.get(
					"receipt_id", 0L));

			// 查询每个pallet对应的sn 以及照片
			for (DBRow row : pallets) {
				row.add("create_date",
						DateUtil.showLocalparseDateTo24Hours(
								row.get("create_date", ""), ps_id));
				DBRow[] sn = null;
				// edit by zhengziqi at July.7th 2015
				// if(row.get("status", 1) == 2){
				List<DBRow> rows = new ArrayList<DBRow>();
				for (DBRow r : rnSNRows) {
					// TODO 优化筛选
					if (r.get("at_lp_id", 0L) == (row.get("con_id", 0L))) {
						rows.add(r);
					}
				}
				sn = new DBRow[rows.size()];
				rows.toArray(sn);

				// }else{
				// sn = new DBRow[0];
				// }

				int is_repeat = 0;
				for (DBRow ss : sn) {
					if (ss.get("is_repeat", 0) == 1) {
						is_repeat = 1;
						break;
					}
				}
				row.add("is_repeat", is_repeat);
				row.add("sn", sn);
				row.add("sn_count", sn.length);
				row.add("wms_container_id", row.get("container_no", 0));
				row.add("container_no",
						"(94)"
								+ CommonUtils.formatNumber("#000000000000",
										row.get("container_no", 0)));
				DBRow[] photos = floorReceiptsMgrWfh.findPalletPhotoByDetailId(
						row.get("con_detail_id", 0L), row.get("con_id", 0L));
				for (DBRow ph : photos) {
					ph.add("uri", "_fileserv/file/" + ph.get("file_id", ""));
				}
				row.add("photos", photos);
				if (row.get("goods_type", 0) == GOODS_TYPE_DAMAGE) {
					damagedArray.add(row);
				} else {
					if (row.get("container_type", 0) == 1) {
						clppalletArray.add(row);
					}
					if (row.get("container_type", 0) == 3) {
						tlppalletArray.add(row);
					}
					palletArray.add(row);
				}
			}
			// 为了适应 以前的版本 ，tlp 的托盘 参数名为 从 tlppallets 改成 pallets 2015年5月21日
			// 10:47:27
			result.add("pallets", palletArray.toArray(new DBRow[0]));
			// result.add("clppallets", clppalletArray.toArray(new DBRow[0]));
			result.add("damaged", damagedArray.toArray(new DBRow[0]));
			result.add("success", "true");
			// add by zhaoyy 2015年6月19日 10:23:22
			DBRow[] containerTypes = floorContainerMgrZYZ.getAllContainerType(
					null, null);
			result.add("conTypes", containerTypes);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (OutScheduleException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.OUT_SCHEDULE;
			result.add("data", e.getMessage());
			result.add("line_id", e.getData());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		String s = CommonUtils.convertDBRowsToJsonString(result);
		return s;
	}

	/**
	 * 根据line id以及configID查询出line下这个palletConfig的配置信息所属的pallets信息 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 * 
	 * 
	 *             add by ,从搜索 进入scan 时调用的接口， 因
	 *             /android/acquirePalletsInfoByLine 调用 过多，无法 分离 所以添加了 一个
	 *             相同处理的接口 方法 只是 添加了 // 判断扫描人是否 再扫描中 的验证
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquireScanPalletsInfoByLine", produces = "application/json; charset=UTF-8")
	public String acquireScanPalletsInfoByLine(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "container_config_id", required = false, defaultValue = "0") long container_config_id,
			@RequestParam(value = "is_start_scan", required = false, defaultValue = "0") int is_start_scan,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid)
			throws Exception {
		long time = System.currentTimeMillis();
		DBRow receipt = floorReceiptsMgrWfh
				.findReceiptByLineId(receipt_line_id);
		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get(
				"company_id", ""));
		long ps_id = storage.get("ps_id", 0L);
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 验证参数
			if (receipt_line_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 判断扫描人是否 再扫描中

			if (!floorReceiptsMgrZyy.checkIsInSchedule(receipt_line_id,
					CCTaskForAndroidController.CCTASK,
					CCTaskForAndroidController.CCTASK_SCANER, adid)) {
				throw new OutScheduleException(
						"The task is not assigned to you . do this task ?",
						receipt_line_id);
			}

			// 第一次进入scan pallet 写入时间 记录时间
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			if (is_start_scan == 1) {

				DBRow upRow = new DBRow();
				upRow.add("scan_start_time", DateUtil.NowStr());
				upRow.add("scan_start_adid", adid);
				upRow.add("status", LineStatueKey.STATUS_SCAN);
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
				// 将所有的托盘都置为open
				floorReceiptsMgrWfh.updateContainerStatusByLineID(
						receipt_line_id, ContainerStatueKey.CON_OPEN);
				// 将RN的状态改为Open
				upRow = new DBRow();
				upRow.add("status", "Open");
				floorReceiptsMgrZzq.updateReceiptStatus(
						line.get("receipt_id", ""), upRow);
				// TODO add schedule start_time and
				DBRow schedules = floorReceiptsMgrZyy.getScheduleByReceipt(
						receipt_line_id, CCTaskForAndroidController.CCTASK,
						CCTaskForAndroidController.CCTASK_SCANER);
				if (null != schedules) {
					upRow = new DBRow();
					upRow.add("start_time", DateUtil.NowStr());
					floorReceiptsMgrWfh.closeSchedule(
							schedules.get("schedule_id", 0l), upRow);
				}
			}
			DBRow[] pallets = this.floorReceiptsMgrWfh.findPalletByLineId(
					receipt_line_id, container_config_id, GOODS_TYPE_ALL, -1);
			List<DBRow> palletArray = new ArrayList<DBRow>();
			List<DBRow> clppalletArray = new ArrayList<DBRow>();
			List<DBRow> tlppalletArray = new ArrayList<DBRow>();

			List<DBRow> damagedArray = new ArrayList<DBRow>();

			DBRow[] rnSNRows = floorReceiptsMgrWfh.findScanedSNBySns(line.get(
					"receipt_id", 0L));

			// 查询每个pallet对应的sn 以及照片
			for (DBRow row : pallets) {
				row.add("create_date",
						DateUtil.showLocalparseDateTo24Hours(
								row.get("create_date", ""), ps_id));
				DBRow[] sn = null;
				// edit by zhengziqi at July.7th 2015
				// if(row.get("status", 1) == 2){
				List<DBRow> rows = new ArrayList<DBRow>();
				for (DBRow r : rnSNRows) {
					// TODO 优化筛选
					if (r.get("at_lp_id", 0L) == (row.get("con_id", 0L))) {
						rows.add(r);
					}
				}
				sn = new DBRow[rows.size()];
				rows.toArray(sn);

				// }else{
				// sn = new DBRow[0];
				// }

				int is_repeat = 0;
				for (DBRow ss : sn) {
					if (ss.get("is_repeat", 0) == 1) {
						is_repeat = 1;
						break;
					}
				}
				row.add("is_repeat", is_repeat);
				row.add("sn", sn);
				row.add("sn_count", sn.length);
				row.add("wms_container_id", row.get("container_no", 0));
				row.add("container_no",
						"(94)"
								+ CommonUtils.formatNumber("#000000000000",
										row.get("container_no", 0)));
				DBRow[] photos = floorReceiptsMgrWfh.findPalletPhotoByDetailId(
						row.get("con_detail_id", 0L), row.get("con_id", 0L));
				for (DBRow ph : photos) {
					ph.add("uri", "_fileserv/file/" + ph.get("file_id", ""));
				}
				row.add("photos", photos);
				if (row.get("goods_type", 0) == GOODS_TYPE_DAMAGE) {
					damagedArray.add(row);
				} else {
					if (row.get("container_type", 0) == 1) {
						clppalletArray.add(row);
					}
					if (row.get("container_type", 0) == 3) {
						tlppalletArray.add(row);
					}
					palletArray.add(row);
				}
			}
			// 为了适应 以前的版本 ，tlp 的托盘 参数名为 从 tlppallets 改成 pallets 2015年5月21日
			// 10:47:27
			result.add("pallets", palletArray.toArray(new DBRow[0]));
			// result.add("clppallets", clppalletArray.toArray(new DBRow[0]));
			result.add("damaged", damagedArray.toArray(new DBRow[0]));
			result.add("success", "true");
			// add by zhaoyy 2015年6月19日 10:23:22
			DBRow[] containerTypes = floorContainerMgrZYZ.getAllContainerType(
					null, null);
			result.add("conTypes", containerTypes);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (OutScheduleException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.OUT_SCHEDULE;
			result.add("data", e.getMessage());
			result.add("line_id", e.getData());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		String s = CommonUtils.convertDBRowsToJsonString(result);
		return s;
	}

	/**
	 * 根据line id以及configID查询出line下这个 为CC break 单独写的接口
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquirePalletsInfoByLineForBreak", produces = "application/json; charset=UTF-8")
	public String acquirePalletsInfoByLineForBreak(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "container_config_id", required = false, defaultValue = "0") long container_config_id,
			@RequestParam(value = "is_start_scan", required = false, defaultValue = "0") int is_start_scan,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			if ("".equals(line.get("scan_start_adid", ""))) {
				result.add("is_start_scan", 0);
			} else {
				result.add("is_start_scan", 1);
			}
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			// 验证参数
			if (receipt_line_id == 0) {
				throw new ReceiveForAndroidException("Parameter error ");
			}

			DBRow[] pallets = this.floorReceiptsMgrZyy
					.findNoBreakPalletByLineId(receipt_line_id,
							container_config_id, GOODS_TYPE_NORMAL);
			List<DBRow> palletArray = new ArrayList<DBRow>();

			// 查询每个pallet对应的sn 以及照片
			for (DBRow row : pallets) {
				if (row.get("process", 0) == CreateContainerProcessKey.PROCESS_COUNT) {

					row.add("create_date",
							DateUtil.showLocalparseDateTo24Hours(
									row.get("create_date", ""), ps_id));
					DBRow[] sn = this.floorReceiptsMgrWfh.findSNByPalletId(row
							.get("con_id", 0L));
					// 看这个托盘下面是否有与其他托盘相同的sn is_repeat
					int is_repeat = 0;
					for (DBRow ss : sn) {
						if (ss.get("is_repeat", 0) == 1) {
							is_repeat = 1;
							break;
						}
					}
					row.add("is_repeat", is_repeat);
					row.add("sn", sn);
					row.add("sn_count", sn.length);
					row.add("wms_container_id", row.get("container_no", 0));
					row.add("container_no",
							"(94)"
									+ CommonUtils.formatNumber("#000000000000",
											row.get("container_no", 0)));
					DBRow[] photos = floorReceiptsMgrWfh
							.findPalletPhotoByDetailId(
									row.get("con_detail_id", 0L),
									row.get("con_id", 0L));
					for (DBRow ph : photos) {
						ph.add("uri", "_fileserv/file/" + ph.get("file_id", ""));
					}
					row.add("photos", photos);
					// if(row.get("container_type",0) ==3){
					// 托盘是在count 时创建的
					palletArray.add(row);
					// }
				}
			}
			// add by zhaoyy 2015年5月22日 11:53:42 ，添加已经break 的托盘信息
			DBRow[] breakPallets = floorReceiptsMgrZzq
					.queryBreakedPalletInfoByLine(receipt_line_id);
			result.add("breakPallets", breakPallets);
			result.add("pallets", palletArray.toArray(new DBRow[0]));
			result.add("success", "true");
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 从wms请求line数据，如果与mysql库存在差异(line增减,line的item_id,lot_no,qty属性变更),
	 * 会将新增变更同步到mysql数据库(line删除操作,line更新操作,需要用户确认),并将差异信息返回android端 2015年3月4日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/reload", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String reload(
			@RequestParam(value = "company_id", required = false, defaultValue = "") String company_id,
			@RequestParam(value = "receipt_no", required = false, defaultValue = "") String receipt_no)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		// 验证参数
		if ("".equals(company_id) || "".equals(receipt_no)) {
			result.add("ret", BCSKeyReceive.FAIL);
			result.add("err", BCSKeyReceive.SYSTEMERROR);
			result.add("data", "Parameter error");
			return CommonUtils.convertDBRowsToJsonString(result);
		}
		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(company_id);
		long ps_id = storage.get("ps_id", 0L);

		JSONObject datas = new JSONObject();
		// 参数
		DBRow paramReceipt = new DBRow();
		paramReceipt.add("company_id", company_id);
		paramReceipt.add("receipt_no", receipt_no);

		// 调用wms接口，获取wms的RN line信息
		DBRow[] receiptLinesFromWMS = floorReceiptSqlServerMgrSbb
				.getOldExpectedReceiptLines(paramReceipt);
		List<String> lineNosWMS = new ArrayList<String>();
		for (int i = 0; i < receiptLinesFromWMS.length; i++) {
			lineNosWMS.add(receiptLinesFromWMS[i].getString("line_no"));
		}
		// 获取RN所属所有line info
		DBRow[] linesInfo = floorReceiptsMgrZzq
				.queryReceiptLinesInfoByRN(paramReceipt);
		List<DBRow> lineNosToDel = new ArrayList<DBRow>();
		// 返回失效line info(WMS不存在的line)
		String lineId;
		for (DBRow row : linesInfo) {
			lineId = row.getString("line_no");
			if (!lineNosWMS.contains(lineId)) {
				lineNosToDel.add(row);
			}
		}
		// String lineNosToDelStr = "".equals(lineNosToDel.toString()) ? ""
		// : lineNosToDel.toString().substring(0,
		// lineNosToDel.length() - 1);
		JSONArray receiptLinesArray = new JSONArray();
		buildJSONArray(receiptLinesArray, lineNosToDel, ps_id);
		datas.put("lineNosToDel", receiptLinesArray);

		// 批量更新wms的rn lines信息(item_id,lot_no,qty)至mysql数据库rn
		// lines表的(wms_item,wms_lot_no,wms_qty)字段
		floorReceiptsMgrZzq.updateRNLineByBatch(receiptLinesFromWMS);
		DBRow param = new DBRow();
		param.add("company_id", company_id);
		param.add("receipt_no", receipt_no);
		// param.add("lineNos", CommonUtils.listToString(lineNosWMS));
		DBRow[] receiptLinesUpdated = floorReceiptsMgrZzq
				.queryLinesDifferentWithWMS(param);

		JSONArray receiptLinesUpdatedArray = new JSONArray();
		buildJSONArray(receiptLinesUpdatedArray, receiptLinesUpdated, ps_id);

		datas.put("receiptLinesUpdated", receiptLinesUpdatedArray);
		// 批量增加WMS存在，但mysql不存在的line
		// 原line ids
		List<String> lineNos = Arrays.asList(floorReceiptsMgrZzq
				.queryReceiptLineNos(param).split(","));
		List<DBRow> linesToInsert = new ArrayList<DBRow>();
		List<String> lineNosToInsert = new ArrayList<String>();
		for (String line : lineNosWMS) {
			if (!lineNos.contains(line)) {
				lineNosToInsert.add(line);
			}
		}
		for (DBRow row : receiptLinesFromWMS) {
			if (lineNosToInsert.contains(row.getString("line_no"))) {
				linesToInsert.add(row);
			}
		}
		long RNId = Long.valueOf(floorReceiptsMgrZzq.queryReceiptInfoByRN(
				paramReceipt).getString("receipt_id"));
		floorReceiptsMgrZzq.insertReceiptLinesByList(linesToInsert, RNId);
		JSONArray receiptLinesInsertedArray = new JSONArray();
		buildJSONArray(receiptLinesInsertedArray, linesToInsert, ps_id);
		datas.put("receiptLinesInserted", receiptLinesInsertedArray);

		result.add("datas", datas);
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	public void buildJSONArray(JSONArray receiptLinesUpdatedArray,
			List<DBRow> receiptLinesList, long ps_id) throws Exception {
		DBRow[] receiptLines = receiptLinesList
				.toArray(new DBRow[receiptLinesList.size()]);
		buildJSONArray(receiptLinesUpdatedArray, receiptLines, ps_id);
	}

	public void buildJSONArray(JSONArray receiptLinesUpdatedArray,
			DBRow[] receiptLines, long ps_id) throws Exception {
		JSONObject receiptLinesObject;
		JSONObject itemSetupObject;
		for (DBRow row : receiptLines) {
			DBRow location = floorReceiptsMgrWfh.findLocationByLineId(row.get(
					"receipt_line_id", 0L));
			DBRow[] goods_sn_size = floorReceiptsMgrWfh.findSNByLineId(
					row.get("receipt_line_id", 0L), 0, 0);
			DBRow[] goodsDa_sn_size = floorReceiptsMgrWfh.findSNByLineId(
					row.get("receipt_line_id", 0L), 0, 1);
			DBRow[] damage_sn_size = floorReceiptsMgrWfh.findSNByLineId(
					row.get("receipt_line_id", 0L), 2, 3);
			// 查询goods_type=0的托盘上存放的损坏货物数量
			DBRow sumQTY = floorReceiptsMgrWfh.findSUMReceivedQTY(
					row.get("receipt_line_id", 0L), 0, false);
			receiptLinesObject = new JSONObject();
			receiptLinesObject.put("sn_normal_qty", goods_sn_size.length);
			receiptLinesObject.put("sn_damage_qty", damage_sn_size.length
					+ goodsDa_sn_size.length);
			receiptLinesObject.put("location", location.get("location", ""));

			receiptLinesObject.put("pc_id", row.get("pc_id", 0));
			receiptLinesObject.put("normal_qty", row.get("goods_total", 0));
			receiptLinesObject.put("damage_qty", row.get("damage_total", 0)
					+ sumQTY.get("damage_qty", 0));
			// stock_outqty暂时没有使用
			// receiptLine_sObject.put("stock_out_qty", row.get("expected_qty",
			// 0)
			// - (row.get("goods_qty", 0) + row.get("damage_total", 0)));
			receiptLinesObject.put("unloading_finish_time",
					row.get("unloading_finish_time", ""));

			receiptLinesObject.put("receipt_line_id",
					row.get("receipt_line_id"));
			receiptLinesObject.put("item_id", row.get("item_id"));
			receiptLinesObject.put("lot_no", row.get("lot_no"));
			receiptLinesObject.put("expected_qty", row.get("expected_qty"));
			receiptLinesObject.put("note", row.get("note"));
			receiptLinesObject.put("sn_size", row.get("sn_size"));
			receiptLinesObject.put("receive_qty", row.get("received_qty", 0));
			receiptLinesObject.put("config_pallet", row.get("pallets_no", 0));
			// DBRow[] qtys =
			// floorReceiptsMgrZyy.getPalletQtyByLineId(row.get("receipt_line_id",
			// 0L));
			// if(null!=qtys && qtys.length>0){
			// for(DBRow r:qtys){
			// int container_type = r.get("container_type", 0);
			// if(container_type==1){
			// //clp 的数量
			// receiptLinesObject.put("clp_expected_qty", r.get("expected_qty",
			// 0));
			// }else if(container_type==3){
			// //tlp 的数量
			// receiptLinesObject.put("tlp_expected_qty", r.get("expected_qty",
			// 0));
			// }
			// }
			// }
			int ccQTY = floorReceiptsMgrZyy.getCCPalletQtyByLineId(row.get(
					"receipt_line_id", 0L));
			receiptLinesObject.put("clp_expected_qty", ccQTY);

			int countQTY = floorReceiptsMgrZyy.getCountQTYByLineId(row.get(
					"receipt_line_id", 0L));
			receiptLinesObject.put("tlp_expected_qty", countQTY);

			// 因为存在逻辑删除的原因所以这个参数可能不准确 会在这里在查询一次
			DBRow receivePalletCount = floorReceiptsMgrWfh
					.findReceivePalletCount(row.get("receipt_line_id", 0L));
			if (receivePalletCount == null)
				receivePalletCount = new DBRow();
			receiptLinesObject.put("count_pallet",
					receivePalletCount.get("receive_pallet", 0));

			receiptLinesObject.put("line_status", row.get("status"));
			receiptLinesObject.put(
					"unloading_finish_time",
					DateUtil.showLocalparseDateTo24Hours(
							row.get("unloading_finish_time", ""), ps_id));
			receiptLinesObject.put("counted_user_name",
					row.get("counted_user_name", ""));
			receiptLinesObject.put("counted_user", row.get("counted_user", 0L));
			receiptLinesObject.put(
					"scan_end_time",
					DateUtil.showLocalparseDateTo24Hours(
							row.get("scan_end_time", ""), ps_id));
			receiptLinesObject.put("scan_end_adid",
					row.get("scan_end_adid", 0L));
			receiptLinesObject.put("scan_end_user",
					row.get("scan_end_user", ""));

			receiptLinesObject.put(
					"scan_start_time",
					DateUtil.showLocalparseDateTo24Hours(
							row.get("scan_start_time", ""), ps_id));
			receiptLinesObject.put("scan_start_adid",
					row.get("scan_start_adid", 0L));
			receiptLinesObject.put("scan_start_user",
					row.get("scan_start_user", ""));

			receiptLinesObject.put(
					"cc_start_time",
					DateUtil.showLocalparseDateTo24Hours(
							row.get("cc_start_time", ""), ps_id));
			String ccUser = row.get("cc_user", "");
			receiptLinesObject.put("cc_user", ccUser);
			if (!"".equals(ccUser)) {
				DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long
						.parseLong(ccUser));
				receiptLinesObject.put("cc_start_user",
						admin.get("employe_name", ""));
			} else {
				receiptLinesObject.put("cc_start_user", "");
			}

			DBRow[] hadBreaked = floorReceiptsMgrZyy.getBraekPalletByLine(row
					.get("receipt_line_id", 0L));
			if (null != hadBreaked && hadBreaked.length > 0) {
				receiptLinesObject.put("is_start_break", 1);
			} else {
				receiptLinesObject.put("is_start_break", 0);
			}
			// 由于存在无配置的托盘（partial=2）,此处config_qty没有意义,使用line的expected_qty
			// DBRow configQTY = floorReceiptsMgrWfh
			// .findContainerConfigQTYByLineId(
			// row.get("receipt_line_id", 0L), 0);
			// receiptLinesObject.put("config_qty",
			// configQTY.get("config_qty", ""));
			// 添加这个line下是否有相同的sn
			DBRow sameSN = floorReceiptsMgrWfh.findSameSNByLine(row.get(
					"receipt_line_id", 0L));
			if (sameSN != null && sameSN.get("is_repeat", 0) == 1) {
				receiptLinesObject.put("is_repeat", 1);
			} else {
				receiptLinesObject.put("is_repeat", 0);
			}
			itemSetupObject = new JSONObject();
			itemSetupObject.put("is_need_sn", row.get("is_check_sn"));
			itemSetupObject.put("sn_size", row.get("sn_size"));
			itemSetupObject.put("is_check_lot_no", row.get("is_check_lot_no"));
			receiptLinesObject.put("itemsetup", itemSetupObject);
			receiptLinesUpdatedArray.put(receiptLinesObject);
		}
	}

	public String defaultZero(Object o) {
		if (o != null && !"".equals(o.toString())) {
			return o.toString();
		}
		return "0";
	}

	/**
	 * 在android端确认了line删除变更之后(手动接触pallet,SN,line的关联关系),
	 * 将该line的expected_qty至为0,本line将不再被检索到;更新line信息 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/confirmLineChange", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String confirmLineChange(
			@RequestParam(value = "company_id", required = false, defaultValue = "") String company_id,
			@RequestParam(value = "receipt_no", required = false, defaultValue = "") String receipt_no,
			@RequestParam(value = "lineNosToDel", required = false, defaultValue = "") String lineNosToDel)
			throws Exception {
		DBRow result = new DBRow();
		Boolean issuccess = false;
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;

		// 验证参数
		if ("".equals(company_id) || "".equals(receipt_no)
				|| "".equals(lineNosToDel)) {
			result.add("ret", BCSKeyReceive.FAIL);
			result.add("err", BCSKeyReceive.SYSTEMERROR);
			result.add("data", "Parameter error");
			return CommonUtils.convertDBRowsToJsonString(result);
		}

		// 参数
		DBRow paramReceipt = new DBRow();
		paramReceipt.add("company_id", company_id);
		paramReceipt.add("receipt_no", receipt_no);
		paramReceipt.add("lineNosToDel", lineNosToDel);

		// 删除
		if (isCheckedLineDel(paramReceipt) && lineNosToDel != null
				&& !"".equals(lineNosToDel)) {
			floorReceiptsMgrZzq.delReceiptLinesByList(company_id, receipt_no,
					lineNosToDel);
			issuccess = true;
		}
		// 更新Item_id,Lot_no发生变化的line
		DBRow param = new DBRow();
		param.add("company_id", company_id);
		param.add("receipt_no", receipt_no);
		if (isCheckedLineUpdate(param)) {
			floorReceiptsMgrZzq.updateReceiptLinesByWMSItemLotInfo(company_id,
					receipt_no);
			issuccess = true;
		}
		// 更新只有qty发生变化的line
		floorReceiptsMgrZzq.updateReceiptLinesByWMSInfo(company_id, receipt_no);
		// 清空指定rn的line的wms_*数据
		floorReceiptsMgrZzq.updateReceiptLinesWMSInfoByRN(company_id,
				receipt_no);
		issuccess = true;

		result.add("issuccess", issuccess);
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 *
	 * 验证Item_id,Lot_no发生变化的line更新操作前是否已经将sn与pallet关系清除 2015年3月6日
	 * 
	 * @param paramReceipt
	 * @return
	 * @throws Exception
	 *
	 */
	private Boolean isCheckedLineUpdate(DBRow paramReceipt) throws Exception {
		Boolean result = true;
		DBRow[] lineHasQTY = floorReceiptsMgrZzq
				.countSNRelWithPalletInLineUpdate(paramReceipt);
		for (DBRow row : lineHasQTY) {
			if (Integer.valueOf(row.getString("counts")) > 0) {
				result = false;
				break;
			}
		}
		return result;
	}

	/**
	 *
	 * 验证line删除操作前是否已经将sn与pallet关系清除 2015年3月5日
	 * 
	 * @param paramReceipt
	 * @return
	 * @throws Exception
	 *
	 */
	private Boolean isCheckedLineDel(DBRow paramReceipt) throws Exception {
		Boolean result = true;
		DBRow[] lineHasQTY = floorReceiptsMgrZzq
				.countSNRelWithPalletInLineList(paramReceipt);
		if (lineHasQTY != null && lineHasQTY.length > 0) {
			result = false;
		}
		return result;
	}

	/**
	 * 关闭这条line
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/closeLine", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String closeLine(
			@RequestParam(value = "line_id", required = false, defaultValue = "0") long line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "type", required = false, defaultValue = "0") long type)
			throws Exception {
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow result = new DBRow();
		try {
			if (line_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 托盘都close了才会关闭line
			Boolean isAllPalletsClosed = floorReceiptsMgrZzq
					.checkIfAllPalletsClosed(line_id);
			if (isAllPalletsClosed) {
				throw new ReceiveForAndroidException("Please close all pallet");
			}
			// 在好的托盘上面没有坏的sn才可以closeLine
			DBRow[] sns = floorReceiptsMgrWfh.findSNByLineId(line_id, 0, 1);
			if (sns.length > 0) {
				throw new ReceiveForAndroidException(
						"Please move damage goods to damage pallets!");
			}

			// 如果sn.size != count.size 不可以关闭line
			// DBRow snCount = floorReceiptsMgrWfh.findSNCountByLineId(line_id);
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(line_id);
			DBRow[] noCloseLine = floorReceiptsMgrZyy
					.getNotCloseLineByReceipt(line.get("receipt_id", 0l));
			// type = 0 表示 是从 任务进来关闭 line 的 1表示是从单据 进来关闭 line的

			if (type == 0) {
				if (null != noCloseLine
						&& ((noCloseLine.length == 1 && line.get(
								"receipt_line_id", 0l) == noCloseLine[0].get(
								"receipt_line_id", 0l)) || noCloseLine.length == 0)) {
					result.add("is_last_line", true);
				} else {
					result.add("is_last_line", false);
					closeLine(line, adid, true);
				}
			} else {
				// 单据处理
				if (null != noCloseLine
						&& ((noCloseLine.length == 1 && line.get(
								"receipt_line_id", 0l) == noCloseLine[0].get(
								"receipt_line_id", 0l)) || noCloseLine.length == 0)) {
					closeLine(line, adid, false);
				} else {
					result.add("is_last_line", false);
					closeLine(line, adid, true);
				}
			}

			// if(type==0 && null!=noCloseLine && (noCloseLine.length==1 ||
			// noCloseLine.length==0) ){
			// result.add("is_last_line", true);
			// // throw new
			// ReceiveForAndroidException("Is last line  to close RN");
			// }else{
			// result.add("is_last_line", false);
			// closeLine(line,adid,false);
			// }
			// if(snCount.get("sn_count", 0) != (line.get("goods_total",
			// 0)+line.get("damage_total", 0))){
			// result.add("ret", BCSKey.FAIL);
			// result.add("err", VAILDATIONERROR);
			// result.add("data", "The scaned SNs is not as counted!");
			// return CommonUtils.convertDBRowsToJsonString(result);
			// }

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 
	 * @param line
	 * @param adid
	 * @param is_last_line
	 *            是否是最有一个line ，如果是 责关闭schedule ，否是不关闭
	 * @throws ReceiveForAndroidException
	 * @throws Exception
	 */
	private void closeLine(DBRow line, long adid, boolean is_last_line)
			throws ReceiveForAndroidException, Exception {
		long line_id = line.get("receipt_line_id", 0l);
		DBRow upRow = new DBRow();
		DBRow[] goodsSN = floorReceiptsMgrWfh.findSNByLineId(line_id,
				GOODS_TYPE_NORMAL, SN_STATUE_ALL); // 查询全部就可以
													// 因为能走到这里好的托盘上面肯定没有坏的sn
		DBRow[] damageSN = floorReceiptsMgrWfh.findSNByLineId(line_id,
				GOODS_TYPE_DAMAGE, SN_STATUE_ALL);
		// 添加关闭时间 关闭人 将sn的数量更新到line上面
		if (line.get("scan_end_adid", 0) == 0) {
			upRow.add("scan_end_adid", adid);
			upRow.add("scan_end_time", DateUtil.NowStr());
		}
		upRow.add("status", LineStatueKey.STATUS_CLOSE);
		upRow.add("goods_total", goodsSN.length);
		upRow.add("damage_total", damageSN.length);

		floorReceiptsMgrWfh.updateReceiptLine(line.get("receipt_line_id", 0L),
				upRow);
		// 提示是否是最后一个line

		// 将container_product 数量 更新到receipt_rel_contianer表
		DBRow[] cps = floorReceiptsMgrWfh.findContainerProductByLineId(line_id,
				3);
		for (DBRow cp : cps) {
			upRow = new DBRow();
			if (cp.get("goods_type", 0) == GOODS_TYPE_DAMAGE) {
				upRow.add("normal_qty", 0);
				upRow.add("damage_qty", cp.get("cp_quantity", 0F));
			} else {
				upRow.add("normal_qty", cp.get("cp_quantity", 0F));
				upRow.add("damage_qty", 0);
			}
			floorReceiptsMgrWfh.updateReceiptRelContainerByConID(
					cp.get("cp_lp_id", 0L), upRow);
		}
		if (is_last_line) {
			// TODO 关闭任务
			closeScanScheduleByline(line_id, adid);
		}
	}

	private void closeScanScheduleByline(long line_id, long adid)
			throws Exception {
		DBRow upRow;
		DBRow schedules = floorReceiptsMgrZyy.getScheduleByReceipt(line_id,
				CCTaskForAndroidController.CCTASK,
				CCTaskForAndroidController.CCTASK_SCANER);
		if (null != schedules) {
			long schedule_id = schedules.get("schedule_id", 0l);
			DBRow[] scheduleSub = floorReceiptsMgrWfh
					.findSheduleSubByScheduleID(schedule_id);
			// 关闭子任务
			for (DBRow task : scheduleSub) {
				DBRow subRow = new DBRow();
				subRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
				subRow.add("is_task_finish", 1);
				subRow.add("schedule_finish_adid", adid);
				floorReceiptsMgrWfh.closeScheduleSub(
						task.get("schedule_sub_id", 0L), subRow);
			}
			upRow = new DBRow();
			upRow.add("end_time", DateUtil.NowStr());
			upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
			floorReceiptsMgrWfh.closeSchedule(schedule_id, upRow);
		}
	}

	/**
	 * 关闭这条line
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/closeLineAndRn", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String closeLineAndRn(
			@RequestParam(value = "line_id", required = false, defaultValue = "0") long line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session) throws Exception {
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow result = new DBRow();
		try {
			if (line_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 托盘都close了才会关闭line
			Boolean isAllPalletsClosed = floorReceiptsMgrZzq
					.checkIfAllPalletsClosed(line_id);
			if (isAllPalletsClosed) {
				throw new ReceiveForAndroidException("Please close all pallet");
			}
			// 在好的托盘上面没有坏的sn才可以closeLine
			DBRow[] sns = floorReceiptsMgrWfh.findSNByLineId(line_id, 0, 1);
			if (sns.length > 0) {
				throw new ReceiveForAndroidException(
						"Please move damage goods to damage pallets!");
			}

			// 如果sn.size != count.size 不可以关闭line
			// DBRow snCount = floorReceiptsMgrWfh.findSNCountByLineId(line_id);
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(line_id);
			// if(snCount.get("sn_count", 0) != (line.get("goods_total",
			// 0)+line.get("damage_total", 0))){
			// result.add("ret", BCSKey.FAIL);
			// result.add("err", VAILDATIONERROR);
			// result.add("data", "The scaned SNs is not as counted!");
			// return CommonUtils.convertDBRowsToJsonString(result);
			// }
			// 验证当前item扫描的数量是否与wms一致不一致则不会关闭
			checkSnQty(String.valueOf(line.get("receipt_id")));

			closeLine(line, adid, true);
			closeRn("Closed", String.valueOf(adid),
					String.valueOf(line.get("receipt_id")), session);
			if (!floorReceiptsMgrZyy
					.compareInboundContainerFinallyContainer(line.get(
							"receipt_id", 0l))) {
				result.add("is_print_rn", 1);
			} else {
				result.add("is_print_rn", 0);
			}
		} catch (SNRepeatException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.SN_REPEAT;
			result.add("data", e.getData());
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 将扫描到的sn信息绑定至指定pallet 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/bindSNInfoToPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String bindSNInfoToPallet(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") String con_id,
			@RequestParam(value = "sns", required = false, defaultValue = "") String sns_s,
			@RequestParam(value = "receipt_id", required = false, defaultValue = "0") String receipt_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "is_close", required = false, defaultValue = "0") int is_close,
			HttpSession session, HttpServletRequest request) throws Exception {
		// 参数
		DBRow paramReceipt = new DBRow();
		paramReceipt.add("con_id", con_id);
		paramReceipt.add("sns", sns_s);
		paramReceipt.add("receipt_id", receipt_id);
		paramReceipt.add("lot_no", lot_no);
		paramReceipt.add("item_id", item_id);
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		int normal_qty = 0;
		int damage_qty = 0;
		try {
			// 验证参数
			if ("".equals(con_id) || "".equals(receipt_id) || "".equals(lot_no)
					|| "".equals(item_id)) {
				throw new ReceiveForAndroidException("Parameter error!");
			}

			DBRow receipt = floorReceiptsMgrWfh.findReceiptById(Long
					.parseLong(receipt_id));
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);

			boolean flag = true;
			JSONArray sns = new JSONArray(sns_s);
			// 验证这次提交的sn在同一个托盘中不可以重复 即本次提交中有没有重复的sn
			StringBuffer sb = new StringBuffer();
			for (int i = 0, j = sns.length(); i < j; i++) {
				JSONObject sn = sns.getJSONObject(i);
				if (sb.toString().contains(sn.getString("sn"))) {
					throw new ReceiveForAndroidException("SN has repeat!");
				}
				sb.append(sn.getString("sn") + ",");
			}
			// 获取line信息
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByPalletId(Long
					.parseLong(con_id));
			receipt_id = line.get("receipt_id", "");
			// 验证 item 与lot是否是这条line
			if (!paramReceipt.get("lot_no", "").equals(line.get("lot_no", ""))
					|| !paramReceipt.get("item_id", "").equals(
							line.get("item_id", ""))) {
				throw new ReceiveForAndroidException(
						"Do not belong to this itemID&lotNo!");
			}
			// 验证pc_id
			if (line.get("pc_id", 0L) < 1) {
				throw new ReceiveForAndroidException("Product not found!");
			}
			// 验证sn的长度时候相符
			// DBRow[] sn_back =
			// floorReceiptsMgrWfh.findSNByPalletId(Long.parseLong(con_id));
			for (int i = 0; i < sns.length(); i++) {
				JSONObject sn = sns.getJSONObject(i);
				if (line.get("sn_size", 0) != 0
						&& line.get("sn_size", 0) != sn.getString("sn")
								.length()) {
					throw new ReceiveForAndroidException(
							"The length of SN is wrong!");
				}
			}

			// if(locationRows == null || locationRows.length == 0){
			// ret = BCSKey.FAIL;
			// err = VAILDATIONERROR;
			// result.add("ret", ret);
			// result.add("err", err);
			// result.add("data", "Location info error!");
			// return CommonUtils.convertDBRowsToJsonString(result);
			// }

			if (flag) {
				DBRow upRow = null;
				// 每次扫描前删除pallet下已扫描过的sn
				// update by zhaoyy 2015年7月8日 12:03:04 del sn by con_id
				floorReceiptsMgrWfh.deleteSNByConID(Long.parseLong(con_id));
				// for (DBRow ss : sn_back) {
				// floorReceiptsMgrWfh.deleteSNBySPId(ss.get(
				// "serial_product_id", 0L));
				// }
				// 添加照片
				String res = uploadPhotos(request);
				// 更新file表
				if (!res.equals(""))
					updateFile(Long.parseLong(con_id), res);
				// //标识sn是否在其他的托盘上面有重复的

				DBRow[] rnSNRows = floorReceiptsMgrWfh
						.findScanedSNBySns(paramReceipt.get("receipt_id", 0L));
				DBRow container = floorReceiptsMgrWfh.findContainerByConId(Long
						.parseLong(con_id));
				for (int i = 0; i < sns.length(); i++) {
					JSONObject sn = sns.getJSONObject(i);
					upRow = new DBRow();
					// sameSN = null;
					// DBRow sameSN = this.floorReceiptsMgrWfh.findSameSN(
					// paramReceipt.get("receipt_id", 0L),
					// sn.getString("sn"), paramReceipt.get("lot_no", ""),
					// paramReceipt.get("item_id", ""));
					String checkSn = sn.getString("sn").toUpperCase();
					for (int m = 0, n = rnSNRows.length; m < n; m++) {
						String oldSn = rnSNRows[m].get("serial_number", "");
						if (checkSn.equals(oldSn)) {
							DBRow r = floorReceiptsMgrWfh
									.findContainerByConId(Long
											.parseLong(con_id));
							DBRow _data = new DBRow();
							_data.put("container", r.get("container") + ","
									+ rnSNRows[m].get("container"));
							_data.put("serial_number", checkSn);
							throw new SNRepeatException(
									CommonUtils
											.convertDBRowsToJsonString(_data),
									_data);
						}
					}
					// 如果有相同的则标识出来
					// if (sameSN != null
					// && sameSN.get("serial_product_id", 0L) != 0) {
					// upRow.add("is_repeat", 1);
					// // 与重复的那个托盘也需要标识
					// DBRow upSp = new DBRow();
					// upSp.add("is_repeat", 1);
					// floorReceiptsMgrWfh.updateSerialProduct(
					// sameSN.get("serial_product_id", 0L), upSp);
					// }
					// 添加sn到 serial_product
					upRow.add("serial_number", checkSn);
					upRow.add("is_damage", sn.getString("is_damage"));
					upRow.add("update_date", DateUtil.NowStr());
					upRow.add("update_user", adid);
					upRow.add("pc_id", line.get("pc_id", 0));
					upRow.add("supplier_id", line.get("supplier", ""));
					upRow.add("at_lp_id", paramReceipt.get("con_id", 0L));
					upRow.add("at_slc_id", container.get("location_id", 0L));
					upRow.add("at_slc_type", container.get("location_type", 0));

					long serial_product_id = this.floorReceiptsMgrWfh
							.bindSNToPallet(upRow);
					upRow.add("serial_product_id", serial_product_id);
					//
					if ("0".equals(sn.getString("is_damage"))) {
						normal_qty++;
					} else {
						damage_qty++;
					}
				}

				// 更新contianer_product
				// TODO
				// 应该用pc_id和con_id作为条件查询
				DBRow cp = floorReceiptsMgrWfh
						.findContainerProductByPalletId(Long.parseLong(con_id));
				upRow = new DBRow();
				upRow.add("cp_quantity", sns.length());
				// 如果已经有了 则update，否则则反之
				if (cp != null && cp.get("cp_id", 0l) != 0) {
					floorReceiptsMgrWfh.updateContainerProduct(
							cp.get("cp_id", 0l), upRow);
				} else {
					// 没有就添加
					upRow.add("cp_pc_id", line.get("pc_id", ""));
					upRow.add("cp_lp_id", con_id);
					this.floorReceiptsMgrWfh.bindQTYToPallet(upRow);
				}
				// 更新contianer
				DBRow cc = floorReceiptsMgrWfh.findContainerConfigByConID(Long
						.parseLong(con_id));
				int is_full = CON_PUT_EMPTY;
				if (sns.length() != 0) {
					if (cc.get("length", 0) * cc.get("width", 0)
							* cc.get("height", 0) > sns.length()) {
						is_full = CON_PUT_LACK;
					} else if (cc.get("length", 0) * cc.get("width", 0)
							* cc.get("height", 0) == sns.length()
							|| cc.get("length", 0) * cc.get("width", 0)
									* cc.get("height", 0) == 0) {
						// 正好放满
						is_full = CON_PUT_FULL;
					} else if (cc.get("length", 0) * cc.get("width", 0)
							* cc.get("height", 0) < sns.length()) {
						// 放多了
						is_full = CON_PUT_BEYOND;
					}
				}
				upRow = new DBRow();
				// 如果是close则关闭托盘
				if (is_close == 1) {
					upRow.add("status", ContainerStatueKey.CON_CLOSE);
				}
				upRow.add("is_full", is_full);
				upRow.add("is_has_sn", 1);
				floorReceiptsMgrWfh.updateContainer(Long.parseLong(con_id),
						upRow);
				// TODO 更新 receipt_rel_container 这个表中的数据
				upRow = new DBRow();
				upRow.add("normal_qty", normal_qty);
				upRow.add("damage_qty", damage_qty);
				floorReceiptsMgrWfh.updateReceiptRelContainerByConID(
						Long.parseLong(con_id), upRow);

				// 记录托盘location
				if (is_close == 1) {
					// add inventory
					List<Map<String, Object>> data = buildParamsForInventory(
							con_id, sns.length(), ps_id,
							container.get("location", ""),
							container.get("location_type", STAGING_TYPE));
					String s = new JSONArray(data).toString();
					DBRow r = invent.copyTree(ps_id, data);
					if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
						throw new ReceiveForAndroidException(
								"Inventory exception!");
					}
					// Remove Inventory Log temporarily by zhengziqi at Aug.7th
					// 2015
					addInventoryLogByBatch(data, session);
				}
				result.add("success", "true");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (SNRepeatException e) {
			// wyf 要求 是 SUCCESS
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.SN_REPEAT;
			result.add("data", e.getData());
		} catch (JSONException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	public void deleteInventoryLogByBatch(long ps_id, long con_id,
			String account, Map<String, Object> m) throws Exception {
		JSONObject jsonObject = new JSONObject();
		DBRow conRow = floorReceiptsMgrZzq.queryContainerInfoById(con_id);

		jsonObject.put("ps_id", ps_id);
		jsonObject.put("title_id", conRow.get("title_id"));
		jsonObject.put("customer_id", conRow.get("customer_id"));
		jsonObject.put("container_id", conRow.get("con_id"));
		jsonObject.put("lot_number", conRow.get("lot_no"));
		jsonObject.put("pc_id", conRow.get("pc_id"));
		jsonObject.put("slc_id", conRow.get("location_id"));
		jsonObject.put("receipt_no", conRow.get("receipt_no", 0L));
		jsonObject.put("slc_type", conRow.get("location_type"));
		jsonObject.put("quantity", conRow.get("quantity"));

		jsonObject.put("product_line", 0);
		jsonObject.put("seal_number", new String[2]);

		jsonObject.put("operate_type", InvenOperateTypeKey.DELETE);
		jsonObject.put("is_increase", InvenIncreaseTypeKey.MINUS);
		jsonObject.put("account", account);
		jsonObject.put("adid", m.get("adid"));
		jsonObject.put("department", m.get("department"));
		jsonObject.put("employe_name", m.get("employe_name"));
		floorLogMgr
				.addLog(LogServModelKey.STORE_RECEIVE_LOG, jsonObject, false);
	}

	public void addInventoryLogByBatch(List<Map<String, Object>> data,
			HttpSession sess) throws Exception {
		JSONObject jsonObject = null;
		Map<String, Object> m = sess == null ? null
				: (Map<String, Object>) sess.getAttribute("adminSesion");
		String account = floorReceiptsMgrZzq.queryAccountByAdid(Integer
				.valueOf(m.get("adid").toString()));
		for (Map<String, Object> map : data) {
			jsonObject = new JSONObject();
			DBRow con_props = (DBRow) map.get("con_props");
			DBRow product_props = (DBRow) map.get("product_props");
			DBRow slc_props = (DBRow) map.get("slc_props");
			DBRow rel_props = (DBRow) map.get("rel_props");

			jsonObject.put("ps_id", map.get("ps_id"));
			jsonObject.put("title_id", product_props.get("title_id"));
			jsonObject.put("customer_id", con_props.get("customer_id"));
			jsonObject.put("container_id", con_props.get("con_id"));
			jsonObject.put("product_line", product_props.get("product_line"));
			jsonObject.put("lot_number", product_props.get("lot_no"));
			jsonObject.put("pc_id", product_props.get("pc_id"));
			jsonObject.put("seal_number", map.get("seal_number"));
			jsonObject.put("slc_id", slc_props.get("slc_id"));
			jsonObject.put("receipt_no", con_props.get("receipt_no", 0L));
			jsonObject.put("slc_type", slc_props.get("slc_type"));
			jsonObject.put("operate_type", InvenOperateTypeKey.CREATE);
			jsonObject.put("is_increase", InvenIncreaseTypeKey.PLUS);
			jsonObject.put("quantity", rel_props.get("quantity"));
			jsonObject.put("account", account);
			jsonObject.put("adid", m.get("adid"));
			jsonObject.put("department", m.get("department"));
			jsonObject.put("employe_name", m.get("employe_name"));

			floorLogMgr.addLog(LogServModelKey.STORE_RECEIVE_LOG, jsonObject,
					false);
		}

	}

	public void addPutAwayLogByBatch(long ps_id, long to_location_id,
			int to_location_type, Long[] con_ids, HttpSession sess)
			throws Exception {
		JSONObject jsonObject = null;
		Map<String, Object> m = sess == null ? null
				: (Map<String, Object>) sess.getAttribute("adminSesion");
		String account = floorReceiptsMgrZzq.queryAccountByAdid(Integer
				.valueOf(m.get("adid").toString()));
		for (long con_id : con_ids) {
			jsonObject = new JSONObject();
			DBRow conRow = floorReceiptsMgrZzq.queryContainerInfoById(con_id);
			jsonObject.put("container_id", conRow.get("con_id"));
			jsonObject.put("title_id", conRow.get("title_id"));
			jsonObject.put("customer_id", conRow.get("customer_id"));
			if (conRow.get("location_id", 0L) != 0L) {
				jsonObject.put("from_slc_id", conRow.get("location_id"));
				jsonObject.put("from_slc_type", conRow.get("location_type"));
			} else {
				// 默认 1是ghost
				jsonObject.put("from_slc_id", 1L);
				jsonObject.put("from_slc_type", 1);
			}

			jsonObject.put("ps_id", ps_id);
			jsonObject.put("to_slc_id", to_location_id);
			jsonObject.put("to_slc_type", to_location_type);

			jsonObject.put("account", account);
			jsonObject.put("adid", m.get("adid"));
			jsonObject.put("department", m.get("department"));
			jsonObject.put("employe_name", m.get("employe_name"));

			floorLogMgr.addLog("PutAwayLog", jsonObject, false);
		}

	}

	/**
	 * 不扫描SN，只记录qty到pallet上 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/bindQTYToPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String bindQTYToPallet(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") long con_id,
			@RequestParam(value = "customer_con_id", required = false, defaultValue = "") String customer_con_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "normal_qty", required = false, defaultValue = "0") int normal_qty,
			@RequestParam(value = "damage_qty", required = false, defaultValue = "0") int damage_qty,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type, // 是否是损坏货物的count
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "location", required = false, defaultValue = "0") String location,
			@RequestParam(value = "location_type", required = false, defaultValue = "1") String location_type, // 1:location;2:staging
			HttpServletRequest request) throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 验证参数
			if (con_id == 0 || "".equals(lot_no) || "".equals(item_id)) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			boolean flag = true;
			// Integer.MAX_VALUE/200=10737418

			if (normal_qty + damage_qty > 10737418) {
				throw new ReceiveForAndroidException("Qty too large!");
			}
			if (normal_qty + damage_qty == 0) {
				throw new ReceiveForAndroidException("Please input Qty!");
			}
			DBRow line = this.floorReceiptsMgrWfh
					.findReceiptLineByPalletId(con_id);
			// 是否是这个lot_no item_id
			if (!line.get("lot_no", "").equals(lot_no)
					|| !line.get("item_id", "").equals(item_id)) {
				throw new ReceiveForAndroidException(
						"Do not belong to this itemID&lotNo!");
			}

			// 添加
			if (flag) {
				// 查找之前count的
				// TODO
				// 应该用pc_id和con_id作为条件查询
				DBRow cp = floorReceiptsMgrWfh
						.findContainerProductByPalletId(con_id);
				DBRow upRow = new DBRow();
				upRow.add("cp_quantity",
						goods_type == GOODS_TYPE_DAMAGE ? damage_qty
								: normal_qty); // 如果是放坏的货物的托盘count则记录damge_qty到container_product表
												// ，否则则反之
				// 如果已经有了 则update
				if (cp != null && cp.get("cp_id", 0l) != 0) {
					floorReceiptsMgrWfh.updateContainerProduct(
							cp.get("cp_id", 0l), upRow);
				} else {
					// 没有就添加
					upRow.add("cp_pc_id", line.get("pc_id", 0)); // 默认值
					upRow.add("cp_lp_id", con_id);
					this.floorReceiptsMgrWfh.bindQTYToPallet(upRow);
				}

				// 添加照片
				String res = uploadPhotos(request);
				// 更新file表
				if (!res.equals(""))
					updateFile(con_id, res);
				// 更新line上的总数 total
				DBRow rrc = floorReceiptsMgrWfh
						.findReceiptRelContainerByConID(con_id);
				upRow = new DBRow();
				upRow.add(
						"goods_total",
						(line.get("goods_total", 0) - rrc.get("normal_qty", 0F) + normal_qty)); // 如果以前没有则-0
																								// 若果有则-对应的数量不会出问题

				String pallets = null;
				if (goods_type == GOODS_TYPE_DAMAGE) { // 只有盛放损坏货物的托盘发生变化时才更新line上的damage_total
					// Added by zhengziqi at April.6th 2015
					// clear damaged qty in normal pallet when goods_type = 2
					DBRow damageInfo = floorReceiptsMgrZzq
							.countDamageQtyInNormalPallet(con_id);
					int goodsQtyInNormalPallet = 0;
					if (damageInfo != null && damageInfo.size() > 0) {
						goodsQtyInNormalPallet = damageInfo.get("goods", 0);
						pallets = damageInfo.getString("pallets");
						// 清空具有损坏货物的正常托盘的count值，包括normal_qty和damage_qty
						floorReceiptsMgrZzq
								.updateReceiptRelContainerDamageQty(pallets);
						// 移除contianerProduct
						floorReceiptsMgrWfh
								.deleteContainerProductByConIDs(pallets);
						// update line total qty.
						upRow.add(
								"goods_total",
								(line.get("goods_total", 0) - goodsQtyInNormalPallet));
					}
					upRow.add("damage_total", (line.get("damage_total", 0)
							- rrc.get("damage_qty", 0F) + damage_qty));
				}
				floorReceiptsMgrWfh.updateReceiptLine(
						line.get("receipt_line_id", 0L), upRow);
				// 更新contianer 记录客人的托盘ID
				int is_full = CON_PUT_EMPTY;
				DBRow cc = floorReceiptsMgrWfh
						.findContainerConfigByConID(con_id);
				// 没有放满
				if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) > normal_qty + damage_qty) {
					is_full = CON_PUT_LACK;
				} else if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) == normal_qty + damage_qty
						|| cc.get("length", 0) * cc.get("width", 0)
								* cc.get("height", 0) == 0) {
					// 正好放满
					is_full = CON_PUT_FULL;
				} else if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) < normal_qty + damage_qty) {
					// 放多了
					is_full = CON_PUT_BEYOND;
				}
				upRow = new DBRow();
				upRow.add("is_full", is_full);
				if (!"".equals(customer_con_id)) {
					upRow.add("customer_con_id", customer_con_id);
				}
				// 更新托盘位置信息
				upRow.add("location", location);
				upRow.add("location_type", location_type);
				floorReceiptsMgrWfh.updateContainer(con_id, upRow);
				// 更新receipt rel contianer 需要放在up line total之后 因为需要减去之前count的数量
				upRow = new DBRow();
				upRow.add("normal_qty", normal_qty);
				upRow.add("damage_qty", damage_qty);
				upRow.add("detail_id", detail_id);
				floorReceiptsMgrWfh
						.updateReceiptContainerByConID(con_id, upRow);
				DBRow object = new DBRow();
				if (goods_type == GOODS_TYPE_DAMAGE) { // 正常货物的count才会做这些操作
					// 如果已经有sn则全部移除
					floorReceiptsMgrWfh.deleteSNByLineId(line.get(
							"receipt_line_id", 0L));
				}
				// 损坏托盘类型，要返回托盘列表
				if (goods_type == GOODS_TYPE_DAMAGE) {
					List<DBRow> array = new ArrayList<DBRow>();
					DBRow[] palletsRows = floorReceiptsMgrWfh
							.findContainerByConIds(pallets);
					for (DBRow container : palletsRows) {
						DBRow[] photos = floorReceiptsMgrWfh
								.findPalletPhotoByDetailId(
										container.get("con_detail_id", 0L),
										container.get("con_id", 0L));
						for (DBRow ph : photos) {
							ph.add("uri",
									"_fileserv/file/" + ph.get("file_id", ""));
						}
						container.add("wms_container_id",
								container.get("container_no", 0));
						container.add(
								"container_no",
								"(94)"
										+ CommonUtils.formatNumber(
												"#000000000000", container.get(
														"container_no", 0l)));
						container.add("photos", photos);
						array.add(container);
					}
					object.add("palletsArray", array.toArray(new DBRow[0]));
				}
				// 添加返回数据
				DBRow container = floorReceiptsMgrWfh
						.findContainerByConId(con_id);
				DBRow[] photos = floorReceiptsMgrWfh.findPalletPhotoByDetailId(
						container.get("con_detail_id", 0L),
						container.get("con_id", 0L));
				for (DBRow ph : photos) {
					ph.add("uri", "_fileserv/file/" + ph.get("file_id", ""));
				}
				container.add("wms_container_id",
						container.get("container_no", 0));
				container.add(
						"container_no",
						"(94)"
								+ CommonUtils.formatNumber("#000000000000",
										container.get("container_no", 0l)));
				container.add("photos", photos);
				object.put("updatePallet", container);

				result.add("data", object);
				result.add("success", "true");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	// 更新file表
	private void updateFile(long con_id, String res) throws JSONException,
			Exception {
		DBRow upRow;
		String fileIds = "";
		if (res != null && !"".equals(res)) {
			JSONArray obj = new JSONArray(res);
			for (int i = 0; i < obj.length(); i++) {
				JSONObject photo = obj.getJSONObject(i);
				fileIds += photo.getString("file_id") + ",";
			}
		}
		if (!fileIds.equals("")) {
			DBRow container = floorReceiptsMgrWfh.findContainerByConId(con_id);
			DBRow entry = floorReceiptsMgrWfh.findDetailByDetailId(container
					.get("con_detail_id", 0L));
			fileIds = fileIds.substring(0, fileIds.length() - 1);
			upRow = new DBRow();
			upRow.add("file_with_class", 8);
			upRow.add("file_with_type", 52);
			if (entry != null)
				upRow.add("file_with_id", entry.get("dlo_id", 0L));
			upRow.add("expire_in_secs", 0);
			upRow.add("file_with_detail_id", container.get("con_detail_id", 0L));
			upRow.add("option_file_param", con_id);
			floorReceiptsMgrWfh.updateFileByFileIds(fileIds, upRow);
		}
	}

	/**
	 * 上传图片
	 * 
	 * @param con_id
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/uploadPhoto", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String uploadPhoto(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") long con_id,
			HttpServletRequest request) throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 添加照片
			String res = uploadPhotos(request); // 上传照片
			// 更新file表
			String fileIds = "";
			if (res != null && !"".equals(res)) {
				JSONArray obj = new JSONArray(res);
				for (int i = 0; i < obj.length(); i++) {
					JSONObject photo = obj.getJSONObject(i);
					fileIds += photo.getString("file_id") + ",";
				}
			}
			if (!fileIds.equals("")) {
				DBRow container = floorReceiptsMgrWfh
						.findContainerByConId(con_id);
				DBRow entry = floorReceiptsMgrWfh
						.findDetailByDetailId(container.get("detail_id", 0L));
				fileIds = fileIds.substring(0, fileIds.length() - 1);
				DBRow upRow = new DBRow();
				if (entry != null)
					upRow.add("file_with_id", entry.get("dlo_id", 0L));
				upRow.add("expire_in_secs", 0);
				upRow.add("file_with_class", 8);
				upRow.add("file_with_type", 52);
				upRow.add("file_with_detail_id", container.get("detail_id", 0L));
				upRow.add("option_file_param", con_id);
				floorReceiptsMgrWfh.updateFileByFileIds(fileIds, upRow);

				DBRow[] photos = floorReceiptsMgrWfh.findPalletPhotoByDetailId(
						container.get("detail_id", 0L), con_id);
				for (DBRow ph : photos) {
					ph.add("uri", "_fileserv/file/" + ph.get("file_id", ""));
				}
				result.add("photo", photos);
			}
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 一次count line下所有的pallet
	 * 
	 * @param con_id
	 * @param lot_no
	 * @param item_id
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/countLineAllQTY", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String countLineAllQTY(
			@RequestParam(value = "line_id", required = false, defaultValue = "0") long line_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "normal_total", required = false, defaultValue = "0") int normal_total,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 验证参数
			if (line_id == 0 || "".equals(lot_no) || "".equals(item_id)) {
				throw new ReceiveForAndroidException("Parameter error!");
			}

			boolean flag = true;
			// 验证
			DBRow line = this.floorReceiptsMgrWfh
					.findReceiptLineByLineId(line_id);

			/*
			 * int expected_qty = line.get("expected_qty", 0); if (normal_total
			 * != expected_qty) { flag = false; result.add("data",
			 * "Quantity discrepancy"); ret = BCSKey.FAIL; err =
			 * VAILDATIONERROR; }
			 */
			// 是否是这个lot_no item_id
			if (!line.get("lot_no", "").equals(lot_no)
					|| !line.get("item_id", "").equals(item_id)) {
				throw new ReceiveForAndroidException(
						"Do not belong to this itemID&lotNo!");
			}

			// 添加
			if (flag) {
				DBRow upRow = new DBRow();
				// 不处理之前count的过的pallet 查找之前count过的palelt
				DBRow[] containerProduct = floorReceiptsMgrWfh
						.findContainerProductByLineId(line_id, 0);
				// 查找正常的托盘 所有的pallet
				DBRow[] container = floorReceiptsMgrWfh.findPalletByLineId(
						line_id, 0L, 0, -1);
				int line_total = line.get("goods_total", 0);
				for (DBRow con : container) {
					flag = true;
					for (DBRow cp : containerProduct) {
						// 如果这个托盘已经count过则跳过不重复count
						if (con.get("con_id", 0L) == cp.get("cp_lp_id", 0L)) {
							flag = false;
							break;
						}
					}
					if (flag) {
						// 只有有明确托盘上面放多少个才会count这个托盘
						if (con.get("config_qty", 0) > 0) {
							// 添加container product
							upRow = new DBRow();
							upRow.add("cp_quantity", con.get("config_qty", 0));
							upRow.add("cp_pc_id", line.get("pc_id", 0));
							upRow.add("cp_lp_id", con.get("con_id", 0L));
							this.floorReceiptsMgrWfh.bindQTYToPallet(upRow);
							// 更新line container 关系表上面的 normal damage
							upRow = new DBRow();
							upRow.add("normal_qty", con.get("config_qty", 0));
							upRow.add("damage_qty", 0);
							upRow.add("detail_id", detail_id);
							floorReceiptsMgrWfh.updateReceiptContainerByConID(
									con.get("con_id", 0L), upRow);
							// 更新line上的总数 total
							line_total += con.get("config_qty", 0);
							upRow = new DBRow();
							upRow.add("goods_total", line_total);
							floorReceiptsMgrWfh.updateReceiptLine(
									line.get("receipt_line_id", 0L), upRow);
							// 更新container
							int is_full = CON_PUT_FULL;
							// 正好放满
							upRow = new DBRow();
							upRow.add("is_full", is_full);
							floorReceiptsMgrWfh.updateContainer(
									con.get("con_id", 0L), upRow);
						}
					}
				}
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 验证RN下所有line数据是否正常，关闭rn 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/closeRN", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String closeRN(
			@RequestParam(value = "status", required = false, defaultValue = "") String status,
			@RequestParam(value = "adid", required = false, defaultValue = "") String adid,
			@RequestParam(value = "receipt_id", required = false, defaultValue = "") String receipt_id,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;

		// 获取entry_id,company_id,receipt_no
		try {
			// //验证当前item扫描的数量是否与wms一致不一致则不会关闭
			checkSnQty(receipt_id);
			closeRn(status, adid, receipt_id, session);
			if (!floorReceiptsMgrZyy
					.compareInboundContainerFinallyContainer(Long
							.parseLong(receipt_id))) {
				result.add("is_print_rn", 1);
			} else {
				result.add("is_print_rn", 0);
			}
		} catch (SNRepeatException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.SN_REPEAT;
			result.add("data", e.getData());
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 验证当前item扫描的数量是否与wms一致不一致则不会关闭
	 * 
	 * @param receipt_id
	 * @throws ReceiveForAndroidException
	 * @throws Exception
	 */
	private void checkSnQty(String receipt_id)
			throws ReceiveForAndroidException, Exception {
		DBRow receipt = floorReceiptsMgrWfh.findReceiptById(Long
				.parseLong(receipt_id));
		DBRow[] lines = floorReceiptsMgrWfh
				.findReceiptLineByReceiptIdAndQuantity(Long
						.parseLong(receipt_id));
		DBRow para = new DBRow();
		para.add("company_id", receipt.get("company_id", ""));
		para.add("receptNo", receipt.get("receipt_no", ""));
		DBRow[] wmsLines = floorReceiptSqlServerMgrSbb.getReceiptLines(para);
		String mes = "";
		for (DBRow wmsLine : wmsLines) {
			boolean returnFlag = true;
			String wms_item = wmsLine.get("ItemID", "");
			String wms_lot_no = wmsLine.get("LotNo", "");
			int expected_qty = wmsLine.get("ExpectedQty", 0);
			// 如果wms的item+lotNO在本地找不到则返回错误信息 若数量不对也返回
			for (DBRow line1 : lines) {
				String item = line1.get("item_id", "");
				String lot_no = line1.get("lot_no", "");
				int qty = line1.get("quantity", 0);
				if ((item).equals(wms_item) && lot_no.equals(wms_lot_no)
						&& expected_qty == (qty)) {
					mes += "Item: " + wms_item + " \nLot NO: " + wms_lot_no;
					returnFlag = false;
				}
			}
			if (returnFlag) {
				throw new ReceiveForAndroidException(
						"\nExpected QTY and actual QTY not equal, so can't close RN !\n"
								+ mes);
			}
		}

	}

	private synchronized void closeRn(String status, String adid,
			String receipt_id, HttpSession session)
			throws ReceiveForAndroidException, Exception {

		// TODO 检测配置是否改变，如果改变了，修改提示是否重新打印

		// 获取entry_id,company_id,receipt_no
		DBRow rn = floorReceiptsMgrZzq.queryEntryIdByRNId(receipt_id);
		String entry_id = rn.getString("dlo_id");
		String company_id = rn.getString("company_id");
		String receipt_no = rn.getString("receipt_no");

		DBRow receipt = floorReceiptsMgrWfh.findReceiptById(Long
				.parseLong(receipt_id));
		// 重复关闭RN的验证
		if ("closed".equals(receipt.get("status", "").toLowerCase())) {
			throw new ReceiveForAndroidException("RN had bean closed ");
		}

		DBRow[] schedules = floorReceiptsMgrZyy.getNoCloseSchedulesByReceiptId(
				Long.parseLong(receipt_id), CCTaskForAndroidController.CCTASK,
				CCTaskForAndroidController.CCTASK_SCANER);

		if (null != schedules && schedules.length > 0) {
			for (DBRow r : schedules) {
				closeScanScheduleByline(r.get("associate_id", 0l),
						Long.parseLong(adid));
			}
		}

		DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get(
				"company_id", ""));
		long ps_id = storage.get("ps_id", 0L);

		// 获取adminLoginBean
		@SuppressWarnings("unchecked")
		Map<String, Object> adminBean = (Map<String, Object>) session
				.getAttribute(Config.adminSesion);

		// 验证参数
		if ("".equals(status) || "".equals(receipt_id)) {
			throw new ReceiveForAndroidException("Parameter error");
		}

		// 参数
		DBRow paramReceipt = new DBRow();
		paramReceipt.add("receipt_id", receipt_id);
		// 验证RN下同item_id,lot_no的SN是否重复
		isCheckedSNByRN(paramReceipt);
		// 验证rn的所有pallet 是否是close
		// &&
		// 验证rn的所有需要SN验证的line,是否扫描了足够sn isCheckedRNSN(paramReceipt) &&

		if (isCheckedRNSN(paramReceipt) && isCloseAllPallet(paramReceipt)) {
			// 关闭rn
			String closeTime = DateUtil.NowStr();
			DBRow upRow = new DBRow();
			upRow.add("devanner_date", closeTime);
			upRow.add("status", "Closed");
			floorReceiptsMgrZzq.updateReceiptStatus(receipt_id, upRow);

			// 更新YMS detail的状态 并记录日志
			closeTask(Long.parseLong(receipt_id), Long.parseLong(adid));

			// 关闭单据,同步sqlserver
			Map<String, String> parameter = new HashMap<String, String>();
			parameter.put("adid", adid);
			parameter.put("entry_id", entry_id);
			parameter.put("companyId", company_id);
			parameter.put("receiptNo", receipt_no);
			parameter.put("psId", ps_id + "");
			parameter.put("devanner_date", closeTime);
			parameter.put("employe_name", adminBean.get("employe_name") + "");

			writebackToWmsMgrZyy.synchronizationReceipted(parameter);

			// TODO 吧不满的CLP的托盘，转成TLP
			floorReceiptsMgrZyy.updateConClpToTlp(Long.parseLong(receipt_id));

		} else {
			throw new ReceiveForAndroidException(
					"SN Repeat Or Pallet has no closed ");
		}
		// result.add("ret", ret);
		// result.add("err", err);
	}

	/**
	 * close YMS task
	 * 
	 * @param receipt_id
	 * @param adid
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	private void closeTask(long receipt_id, long adid) throws Exception {
		// 更新detail
		DBRow upRow;
		StringBuffer log = new StringBuffer();
		DBRow[] lines = floorReceiptsMgrWfh
				.findReceiptLineByReceiptId(receipt_id);
		int sn_goods_total = 0;
		int sn_damage_total = 0;
		int goods_total = 0;
		int damage_total = 0;
		String detail_ids = "";
		for (DBRow line : lines) {
			long line_id = line.get("receipt_line_id", 0L);
			DBRow detail_id = floorReceiptsMgrWfh.findDetailByLineId(line_id);
			// 得到所有的detai_id
			detail_ids += detail_id.get("detail_id", "") + ",";
			DBRow[] sn_goods_qty = floorReceiptsMgrWfh.findSNByLineId(line_id,
					0, 3); // 这里查询全部就可以 因为在closeLine的时候不允许好的托盘上面有坏的sn
			DBRow[] sn_damage_qty = floorReceiptsMgrWfh.findSNByLineId(line_id,
					2, 3);
			// 计算得到sn的总数 与count的总数 所有的line
			sn_goods_total += sn_goods_qty.length;
			sn_damage_total += sn_damage_qty.length;
			goods_total += line.get("goods_total", 0);
			damage_total += line.get("damage_total", 0);
			// 添加日志记录明细
			log.append("Item: ").append(line.get("item_id", "")).append(",");
			log.append("Lot No.: ").append(line.get("lot_no", "")).append(",");
			log.append("expected G.:").append(line.get("goods_total", 0));
			if (line.get("damage_total", 0) > 0) {
				log.append(" D.: ").append(line.get("damage_total", 0));
			}
			log.append(",");
			log.append("SN G.:").append(sn_goods_qty.length);
			if (sn_damage_qty.length > 0) {
				log.append(" D.: ").append(sn_damage_qty.length);
			}
			log.append(",");
		}
		detail_ids = detail_ids.substring(0, detail_ids.length() - 1);
		String[] detailIDs = detail_ids.split(",");
		for (String detail_id : detailIDs) {
			if ("0".equals(detail_id))
				continue;
			int number_status = 0;
			// 更新detail
			upRow = new DBRow();
			if (sn_goods_total + sn_damage_total != goods_total + damage_total) {
				number_status = CheckInChildDocumentsStatusTypeKey.EXCEPTION;
				upRow.add("number_status",
						CheckInChildDocumentsStatusTypeKey.EXCEPTION);
			} else {
				number_status = CheckInChildDocumentsStatusTypeKey.CLOSE;
				upRow.add("number_status",
						CheckInChildDocumentsStatusTypeKey.CLOSE);
			}
			DBRow detail = floorReceiptsMgrWfh.findDetailByDetailId(Long
					.parseLong(detail_id));
			if (detail.get("finish_time", "").equals("")) {
				upRow.add("finish_time", DateUtil.NowStr());
			}
			// 将资源与task释放并写回到detail表
			DBRow detailRes = floorReceiptsMgrWfh
					.findDetailResourcesByDetailId(Long.parseLong(detail_id));
			if (detailRes != null) {
				upRow.add("rl_id", detailRes.get("resources_id", 0));
				upRow.add("occupancy_type", detailRes.get("resources_type", 0));
				// 删除关系表的记录
				floorReceiptsMgrWfh.deleteDetailResources(detailRes.get(
						"srr_id", 0L));
			}
			floorReceiptsMgrWfh.updateDetail(Long.parseLong(detail_id), upRow);
			// 添加CheckIN 日志
			DBRow row = new DBRow();
			row.add("operator_id", adid);
			row.add("note", "Close Task");
			row.add("dlo_id", detail.get("dlo_id", 0L));
			row.add("log_type", CheckInLogTypeKey.AndroidCloseTask);
			row.add("operator_time", DateUtil.NowStr());
			row.add("data", log.toString());
			this.floorCheckInMgrZwb.addCheckInLog(row);
			// 关闭这个task所有未关闭的schedule
			DBRow[] schedule = floorReceiptsMgrWfh.findScheduleByDetail(0, 0,
					Long.parseLong(detail_id));
			for (DBRow sc : schedule) {
				long schedule_id = sc.get("schedule_id", 0L);
				DBRow[] scheduleSub = floorReceiptsMgrWfh
						.findSheduleSubByScheduleID(schedule_id);
				for (DBRow task : scheduleSub) {
					upRow = new DBRow();
					upRow.add("schedule_state",
							ScheduleFinishKey.ScheduleFinish);
					upRow.add("is_task_finish", 1);
					upRow.add("schedule_finish_adid", adid);
					floorReceiptsMgrWfh.closeScheduleSub(
							task.get("schedule_sub_id", 0L), upRow);
				}
				String closeTaskDate = DateUtil.NowStr();
				upRow = new DBRow();
				upRow.add("end_time", closeTaskDate);
				upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
				floorReceiptsMgrWfh.closeSchedule(schedule_id, upRow);
			}
			// 关闭LR
			DBRow detailRow = floorReceiptsMgrWfh.findDetailByDetailId(Long
					.parseLong(detail_id));
			long LR_ID = detailRow.get("LR_ID", 0l);
			int count = floorCheckInMgrZr.countRLIdNotFinishTask(LR_ID);
			if (count == 0) {
				// 关闭order_system
				DBRow updateRow = new DBRow();
				updateRow.add("number_status", number_status);
				if (number_status == CheckInChildDocumentsStatusTypeKey.CLOSE
						|| number_status == CheckInChildDocumentsStatusTypeKey.EXCEPTION) { // 只有在Close的时候才会写finish_time的数据
					updateRow.add("finish_time", DateUtil.NowStr());
				}
				floorCheckInMgrZr.updateOrderSystem(LR_ID, updateRow);
			}
		}
	}

	/**
	 *
	 * 获取打印server列表 2015年3月16日
	 * 
	 * @param ps_id
	 * @param area_id
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/aquirePrintServers", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquirePrintServers(

			@RequestParam(value = "adid", required = false, defaultValue = "") String adid,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		long ps_id = this.getLoginPsID(session);
		String area_id = floorReceiptsMgrZzq.queryAreaIdByAdid(adid);
		area_id = ("".equals(area_id)) ? "0" : area_id;

		// 验证参数
		if ("".equals(ps_id) || "".equals(area_id)) {
			result.add("ret", BCSKeyReceive.FAIL);
			result.add("err", BCSKeyReceive.SYSTEMERROR);
			result.add("data", "Parameter error");
			return CommonUtils.convertDBRowsToJsonString(result);
		}
		result.add(
				"printServer",
				getPrintServerByLoginUser(Long.valueOf(ps_id),
						Long.valueOf(area_id), LABEL));

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	private boolean checkConfigChange() {
		// TODO
		return false;
	}

	/**
	 * 指定打印server打印标签 2015年3月16日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/printContainers", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String printContainers(
			@RequestParam(value = "params") String params,
			@RequestParam(value = "ps_id") String ps_id,
			@RequestParam(value = "printer") String printer,
			@RequestParam(value = "param_type", required = false, defaultValue = "0") int param_type,
			@RequestParam(value = "Version") String Version,
			@RequestParam(value = "Password") String Password,
			@RequestParam(value = "Mac") String Mac,
			@RequestParam(value = "Machine") String Machine,
			@RequestParam(value = "LoginAccount") String LoginAccount,
			@RequestParam(value = "adid") String adid, HttpSession session)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		String messageFromSync = null;
		try {
			String pallet_ids = null;
			// 将container_config_id数组转换为pallet_ids
			if (param_type == CONTAINER_CONFIG_TYPE) {
				pallet_ids = floorReceiptsMgrZzq
						.queryPalletIdsByContainerConfigId(params);
				// 将receipt id转换为pallet_ids
			} else if (param_type == RECEIPT_TYPE) {
				pallet_ids = floorReceiptsMgrZzq
						.queryPalletsByReceiptNoAndCompanyId(Long
								.valueOf(params));
				// params即为pallet_ids
			} else if (param_type == RECEIPT_LINE_TYPE) {
				pallet_ids = floorReceiptsMgrZzq
						.queryPalletsByReceiptLineId(Long.valueOf(params));
				// params即为pallet_ids
			} else if (param_type == RECEIPT_LINE_INBOUND) {
				// inbound 的config
				// pallet_ids = floorReceiptsMgrZzq
				// .queryPalletsByReceiptLineId(Long.valueOf(params));
				DBRow[] inboundRows = floorReceiptsMgrZyy
						.getContainerByLineIdAndProcess(Long.valueOf(params),
								CreateContainerProcessKey.PROCESS_COUNT);
				DBRow[] breakRow = floorReceiptsMgrZyy
						.getBreakPartialByLineId(Long.valueOf(params));

				String breakContainers = "";
				if (null != breakRow && breakRow.length > 0) {
					for (DBRow _r : breakRow) {
						breakContainers += _r.get("con_id", "") + ",";
					}
				}
				pallet_ids = "";
				if (null != inboundRows && inboundRows.length > 0) {
					for (DBRow _r : inboundRows) {
						String conId = _r.get("con_id", "");
						if (breakContainers.indexOf(conId) == -1) {
							pallet_ids += conId + ",";
						}
					}
				}
				if (pallet_ids.length() > 1) {
					pallet_ids = pallet_ids.substring(0,
							pallet_ids.length() - 1);
				}
			} else if (param_type == RECEIPT_LINE_NEWCONFIG) {
				// // 新的config
				// pallet_ids = floorReceiptsMgrZzq
				// .queryPalletsByReceiptLineId(Long.valueOf(params));
				DBRow[] inboundRows = floorReceiptsMgrZyy
						.getContainerByLineIdAndProcess(Long.valueOf(params),
								CreateContainerProcessKey.PROCESS_CC);
				DBRow[] breakRows = floorReceiptsMgrZyy
						.getBreakPartialByLineId(Long.valueOf(params));
				pallet_ids = "";
				if (null != inboundRows && inboundRows.length > 0) {
					for (DBRow _r : inboundRows) {
						String conId = _r.get("con_id", "");
						pallet_ids += conId + ",";
					}
				}
				if (null != breakRows && breakRows.length > 0) {
					for (DBRow _r : breakRows) {
						pallet_ids += _r.get("con_id", "") + ",";
					}
				}
				if (pallet_ids.length() > 1) {
					pallet_ids = pallet_ids.substring(0,
							pallet_ids.length() - 1);
				}
			} else {
				pallet_ids = params;
			}
			if (pallet_ids == null || "".equals(pallet_ids)) {
				throw new ReceiveForAndroidException("Pallets not found!");
			}
			// 创建GET方法的实例
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
			String dateStr = sdf.format(new Date());
			// dateStr = DateUtil.showUTCTime(dateStr, Long.parseLong(ps_id));
			StringBuffer sb = new StringBuffer("");
			sb.append(print_url)
					.append("?Version=")
					.append(Version)
					.append("&Password=")
					.append(Password)
					.append("&Mac=")
					.append(Mac)
					// edit by zhengziqi at June.16th 2015
					// .append("&Machine=").append(Machine).append("&LoginAccount=")
					.append("&Machine=").append("").append("&LoginAccount=")
					.append(LoginAccount.trim()).append("&Method=")
					.append("TLPLabelPrint").append("&print_server_id=")
					.append(printer).append("&palletIds=").append(pallet_ids)
					.append("&date=").append(dateStr);
			messageFromSync = httpclientPrint(sb.toString(), session.getId());

			if (messageFromSync != null) {
				JSONObject o = new JSONObject(messageFromSync);
				if (o.getString("ret") != null
						&& "1".equals(o.getString("ret"))) {
					floorReceiptsMgrZzq.updatePrintInfo(pallet_ids, adid,
							DateUtil.NowStr(), 1);
				}
			} else {
				throw new ReceiveForAndroidException("HttpClient fail!");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
			result.add("ret", ret);
			result.add("err", err);
			return CommonUtils.convertDBRowsToJsonString(result);
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		JSONObject object = new JSONObject(messageFromSync);
		object.put("print_date",
				DateUtil.showLocationTime(new Date(), Long.valueOf(ps_id)));
		return object.toString();
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/printRNCount", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String printRNCount(
			@RequestParam(value = "company_id") String company_id,
			@RequestParam(value = "receipt_no") String receipt_no,
			@RequestParam(value = "printer") String printer,
			@RequestParam(value = "Version") String Version,
			@RequestParam(value = "Password") String Password,
			@RequestParam(value = "Mac") String Mac,
			@RequestParam(value = "Machine") String Machine,
			@RequestParam(value = "LoginAccount") String LoginAccount,
			@RequestParam(value = "adid") String adid, HttpSession session)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		String messageFromSync = null;
		try {
			if (StringUtils.isEmpty(company_id)
					|| StringUtils.isEmpty(receipt_no)) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			// 将container_config_id转换为pallet_ids
			SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yy");
			String dateStr = sdf.format(new Date());
			StringBuffer sb = new StringBuffer();
			sb.append(print_url)
					.append("?Version=")
					.append(Version)
					.append("&Password=")
					.append(Password)
					.append("&Mac=")
					.append(Mac)
					// edit by zhengziqi at June.16th 2015
					// .append("&Machine=").append(Machine).append("&LoginAccount=")
					.append("&Machine=").append("").append("&LoginAccount=")
					.append(LoginAccount).append("&Method=")
					.append("RNLabelPrint").append("&print_server_id=")
					.append(printer).append("&company_id=").append(company_id)
					.append("&receipt_no=").append(receipt_no).append("&date=")
					.append(dateStr);
			messageFromSync = httpclientPrint(sb.toString(), session.getId());
			if (messageFromSync != null) {
				JSONObject o = new JSONObject(messageFromSync);
				if (o.getString("ret") != null
						&& "1".equals(o.getString("ret"))) {
					DBRow upRow = new DBRow();
					upRow.add("receipt_ticket_printed", 1);
					upRow.add("print_date", DateUtil.NowStr());
					upRow.add("print_user", adid);
					upRow.add("print_number", 1);
					floorReceiptsMgrZzq.updateRNPrintInfo(
							Long.valueOf(receipt_no), company_id, upRow);
				}
			} else {
				throw new ReceiveForAndroidException("HttpClient fail!");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return messageFromSync;
	}

	public String httpclientPrint(String str, String session_id) {
		// 构造HttpClient的实例
		HttpClient client = new HttpClient();
		// 创建GET方法的实例

		GetMethod getMethod = new GetMethod(str);
		client.getHostConfiguration().setHost("http://" + db_mysql_host);
		getMethod.setRequestHeader("Cookie", "JSESSIONID=" + session_id);

		// 使用系统提供的默认的恢复策略
		getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());
		try {
			// 执行getMethod
			int statusCode = client.executeMethod(getMethod);
			if (statusCode != 200) {
				System.err.println("Method failed: "
						+ getMethod.getStatusLine());
			}
			// 读取内容
			InputStream is = getMethod.getResponseBodyAsStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] data = new byte[4096];
			int count = -1;
			while ((count = is.read(data, 0, 4096)) != -1) {
				outStream.write(data, 0, count);
				data = null;
			}
			String jsonResult = new String(outStream.toByteArray(), "UTF-8");

			// 处理内容
			return jsonResult;
		} catch (HttpException e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			e.printStackTrace();
		} catch (IOException e) {
			// 发生网络异常
			e.printStackTrace();
		} finally {
			// 释放连接
			getMethod.releaseConnection();
		}
		return null;
	}

	/**
	 * 验证RN下所有line的expected_qty是否和实际相符 2015年3月6日
	 * 
	 * @param paramReceipt
	 * @return
	 * @throws Exception
	 *
	 */
	private Boolean isCloseAllPallet(DBRow paramReceipt) throws Exception {
		Boolean result = true;
		DBRow[] pallet = floorReceiptsMgrWfh.findContainerByRNId(
				paramReceipt.get("receipt_id", 0), 0, 1);
		if (pallet.length > 0) {
			result = false;
		}
		return result;
	}

	/**
	 *
	 * 验证rn的所有需要SN验证的line,是否扫描了足够sn 2015年3月6日
	 * 
	 * @param paramReceipt
	 * @return
	 * @throws Exception
	 *
	 */
	private Boolean isCheckedRNSN(DBRow paramReceipt) throws Exception {

		DBRow[] lines = floorReceiptsMgrWfh
				.findReceiptLineByReceiptId(paramReceipt.get("receipt_id", 0L));

		for (DBRow line : lines) {
			DBRow sn = floorReceiptsMgrWfh.findSNCountByLineId(line.get(
					"receipt_line_id", 0L));
			if (line.get("goods_total", 0) + line.get("damage_total", 0) != sn
					.get("sn_count", 0)) {
				return false;
			}
		}
		return true;
	}

	/**
	 *
	 * 验证RN下同item_id,lot_no的SN是否重复 2015年3月6日
	 * 
	 * @param paramReceipt
	 * @return
	 * @throws Exception
	 *
	 */
	private void isCheckedSNByRN(DBRow paramReceipt) throws Exception {
		// DBRow[] lines = floorReceiptsMgrWfh
		// .findReceiptLineByReceiptId(paramReceipt.get("receipt_id", 0l));
		try {
			DBRow data = null;
			DBRow[] rnSNRows = floorReceiptsMgrWfh
					.findScanedSNBySns(paramReceipt.get("receipt_id", 0L));
			Map<String, DBRow> map = new HashMap<String, DBRow>();
			for (DBRow r : rnSNRows) {
				String key = r.get("serial_number", "");
				if (map.containsKey(key)) {
					data = new DBRow();
					DBRow _r = map.get(key);
					data.put("container",
							r.get("container") + "," + _r.get("container"));
					data.put("serial_number", r.get("serial_number"));
					break;
				} else {
					map.put(key, r);
				}
			}

			if (data != null) {
				throw new SNRepeatException(
						CommonUtils.convertDBRowsToJsonString(data), data);
			}
		} catch (SNRepeatException e) {
			throw new SNRepeatException(e.getMessage(), e.getData());
		}

		// List<String> list = new ArrayList<String>();
		// for (DBRow line : lines) {
		// DBRow sameSN = floorReceiptsMgrWfh.findSameSNByLine(line.get(
		// "receipt_line_id", 0L));
		// if (sameSN != null) {
		// return false;
		// }
		// }
	}

	/**
	 * 标记一个sn 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/markSN", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String markSN(
			@RequestParam(value = "sp_id", required = false, defaultValue = "0") long sp_id,
			@RequestParam(value = "is_damage", required = false, defaultValue = "0") long is_damage)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 验证参数
			if (sp_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			DBRow upRow = new DBRow();
			upRow.add("is_damage", is_damage);
			floorReceiptsMgrWfh.updateSerialProduct(sp_id, upRow);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 删除一个sn 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/deleteSN", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String deleteSN(
			@RequestParam(value = "sp_id", required = false, defaultValue = "0") long sp_id,
			@RequestParam(value = "isTask", required = false, defaultValue = "0") long isTask)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {

			// 验证参数
			if (sp_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}

			if (sp_id > 0) {
				// 删除这个sn
				DBRow sn = floorReceiptsMgrWfh.findSNBySpID(sp_id);
				this.floorReceiptsMgrWfh.deleteSNBySPId(sp_id);
				DBRow con = floorReceiptsMgrWfh.findContainerByConId(sn.get(
						"at_lp_id", 0L));
				DBRow upRow = null;
				// 更新contianer_product
				DBRow cp = floorReceiptsMgrWfh
						.findContainerProductByPalletId(con.get("con_id", 0L));
				upRow = new DBRow();
				upRow.add("cp_quantity", cp.get("cp_quantity", 0F) - 1);
				floorReceiptsMgrWfh.updateContainerProduct(cp.get("cp_id", 0L),
						upRow);
				result.add("success", "true");
				// 更新contianer
				long con_id = sn.get("at_lp_id", 0L);
				DBRow cc = floorReceiptsMgrWfh
						.findContainerConfigByConID(con_id);
				DBRow[] sns = floorReceiptsMgrWfh.findSNByPalletId(con_id);
				int is_full = CON_PUT_EMPTY;
				if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) > sns.length) {
					is_full = CON_PUT_LACK;
				} else if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) == sns.length
						|| cc.get("length", 0) * cc.get("width", 0)
								* cc.get("height", 0) == 0) {
					// 正好放满
					is_full = CON_PUT_FULL;
				} else if (cc.get("length", 0) * cc.get("width", 0)
						* cc.get("height", 0) < sns.length) {
					// 放多了
					is_full = CON_PUT_BEYOND;
				}
				upRow = new DBRow();
				upRow.add("is_full", is_full);
				if (sns.length == 0) {
					upRow.add("is_has_sn", 2);
				}
				floorReceiptsMgrWfh.updateContainer(con_id, upRow);
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 删除一个托盘下的所有sn
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/deletePalletSN", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public String deletePalletSN(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") long con_id)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {

			// 验证参数
			if (con_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			this.floorReceiptsMgrWfh.deleteSNByConID(con_id);
			DBRow con = floorReceiptsMgrWfh.findContainerByConId(con_id);
			DBRow upRow = null;
			// 更新contianer_product
			DBRow cp = floorReceiptsMgrWfh.findContainerProductByPalletId(con
					.get("con_id", 0L));
			if (null != cp) {
				upRow = new DBRow();
				upRow.add("cp_quantity", 0);
				floorReceiptsMgrWfh.updateContainerProduct(cp.get("cp_id", 0L),
						upRow);
			}
			// 更新contianer
			int is_full = CON_PUT_EMPTY;
			upRow = new DBRow();
			upRow.add("is_full", is_full);
			DBRow[] sns = floorReceiptsMgrWfh.findSNByPalletId(con_id);
			if (sns.length == 0) {
				upRow.add("is_has_sn", 2);
			}
			floorReceiptsMgrWfh.updateContainer(con_id, upRow);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 删除某个config 以及所有的pallet下属关系 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/deletePalletConfig", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deletePalletConfig(
			@RequestParam(value = "container_config_id", required = false, defaultValue = "0") long container_config_id,
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "isTask", required = false, defaultValue = "0") long isTask,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "process", required = false, defaultValue = "1") int process, // 0.count
																								// 1.cc
																								// 2.scan
			// @RequestParam(value = "adid", required = false, defaultValue =
			// "0") int adid,
			HttpSession session, HttpServletRequest request) throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			if (adid == 0 || receipt_line_id == 0 || container_config_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			StringBuffer log = new StringBuffer();
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			DBRow palletConfig = floorReceiptsMgrWfh
					.findContainerConfigByConConfigId(container_config_id);
			// TODO 删除托盘时 添加验证 ，update by zhaoyy 2015年7月23日 15:37:31
			if (palletConfig.get("keep_inbound", 0) == 1) {
				throw new ReceiveForAndroidException(
						"Pallet config keep inbound ,not allowed delete");
			}
			DBRow[] containers = floorReceiptsMgrWfh
					.findContainerByConConfigId(container_config_id);
			// 记录日志
			log.append("Delete pallet config: ");
			log.append(palletConfig.get("length", 0) + "X"
					+ palletConfig.get("width", 0) + "X"
					+ palletConfig.get("height", 0) + "|");
			log.append("Delete pallet: ");
			for (DBRow container : containers) {
				// 不允许删除CC的图片托盘
				DBRow[] sn = floorReceiptsMgrWfh.findSNByPalletId(container
						.get("con_id", 0L));
				log.append(container.get("con_id", "")).append("|");
				for (DBRow s : sn) {
					log.append("	SN: " + s.get("serial_number", ""));
				}
			}

			// ***一 删除这种配置下pallet的sn 循环删除便于以后记录日志
			DBRow[] sns = floorReceiptsMgrWfh
					.findSNByConConfigId(container_config_id);
			for (DBRow sn : sns) {
				// 在scan这一步不能删除count时创建托盘的数据 wfh 2015-5-19
				if (process == 3
						&& sn.get("process", 0) == CreateContainerProcessKey.PROCESS_COUNT) {
					continue;
				}
				floorReceiptsMgrWfh.deleteSNBySPId(sn.get("serial_product_id",
						0l));
			}
			// ***二 删除这种配置下pallet的count qty 循环删除对应的qty 以更新line的received_qty
			// 便于以后记录日志
			DBRow[] count_qty = floorReceiptsMgrWfh
					.findCountQTYByConConfigId(container_config_id);
			for (DBRow count : count_qty) {
				// 在scan这一步不能删除count时创建托盘的数据 wfh 2015-5-19
				if (process == 3
						&& count.get("process", 0) == CreateContainerProcessKey.PROCESS_COUNT) {
					continue;
				}
				floorReceiptsMgrWfh.deleteContainerProductByCpId(count.get(
						"cp_id", 0L));
			}
			// 更新line上的总数 total 在删除托盘之前更新
			// 只查询未删除的托盘
			DBRow[] rrc = floorReceiptsMgrWfh.findReceiptRelContainer(
					container_config_id, 0);
			// 只有放好的货物的托盘才会出现删除配置的操作,并且在count好的货物的时候出现坏的不会将数量更新到line上
			int goods_total = 0;
			for (DBRow r : rrc) {
				goods_total += r.get("normal_qty", 0F);
			}
			DBRow upRow = new DBRow();
			upRow.add("goods_total", line.get("goods_total", 0) - goods_total); // 如果以前没有则-0
																				// 若果有则-对应的数量不会出问题
			floorReceiptsMgrWfh.updateReceiptLine(
					line.get("receipt_line_id", 0L), upRow);
			// ***三 删除这种配置下所有的pallet 循环为了添加添加日志
			String photoIDS = "";
			boolean havaCountPallet = false;
			String deletePalletIds = "";

			Map<String, Object> m = session == null ? null
					: (Map<String, Object>) session.getAttribute("adminSesion");
			String account = floorReceiptsMgrZzq.queryAccountByAdid(Integer
					.valueOf(m.get("adid").toString()));

			for (DBRow container : containers) {
				// update movement status
				// added by zhengziqi at May.5th 2015
				floorReceiptsMgrZzq.updateMovementStatus(
						container.get("con_id", 0L), adid, DateUtil.NowStr());
				if (container.get("status", ContainerStatueKey.CON_OPEN) == ContainerStatueKey.CON_CLOSE) {
					// delete inventory
					// 在scan操作时，如果删除了已关闭的tlp托盘，删除库存数据
					DBRow ps = floorReceiptsMgrWfh.findPSIDByCompanyId(line
							.get("company_id", ""));
					long ps_id = ps.get("ps_id", 0L);
					DBRow r = invent.deleteContainer(ps_id,
							container.get("con_id", 0L));
					if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
						throw new ReceiveForAndroidException(
								"Inventory exception!");
					}
					deleteInventoryLogByBatch(ps_id,
							container.get("con_id", 0L), account, m);
					// DBRow deletePalletRow = new DBRow();
					// deletePalletRow.add("ps_id", ps_id);
					// deletePalletRow.add("con_id", conID);
					// StringBuffer sb = new
					// StringBuffer(inventory_url).append("/BaseController/deleteContainer");
					// messageFromSync =
					// httpclientForInventroyPost(sb.toString(), new
					// JSONObject(deletePalletRow).toString(),
					// request.getSession(false).getId());
				}

				// 在scan这一步不能删除count时创建托盘的数据 wfh 2015-5-19 但是得更新movement
				// 在符合条件删除库存
				// 将托盘置为逻辑删除
				DBRow falseDelete = new DBRow();
				falseDelete.add("is_delete", 1);
				if (process == 3
						&& container.get("process", 0) == CreateContainerProcessKey.PROCESS_COUNT) {
					havaCountPallet = true;
					floorReceiptsMgrWfh.updateContainer(
							container.get("con_id", 0L), falseDelete);
					continue;
				}
				floorReceiptsMgrWfh.deleteContainerByConId(container.get(
						"con_id", 0L));
				deletePalletIds += container.get("con_id", 0L) + ",";

				DBRow[] photos = floorReceiptsMgrWfh.findPalletPhotoByDetailId(
						container.get("detail_id", 0L),
						container.get("con_id", 0L));
				for (DBRow p : photos) {
					photoIDS += p.get("file_id", 0L) + ",";
				}
			}

			// 移除删除的pallet下面的照片
			if (photoIDS.length() > 0) {
				photoIDS = photoIDS.substring(0, photoIDS.length() - 1);
				this.deletePhotos(photoIDS, request);
			}
			// ***四 删除这种配置
			DBRow falseDelete = new DBRow();
			falseDelete.add("is_delete", 1);
			// 配置下有count的时候创建的托盘不能删除配置
			if (process == 3 && havaCountPallet) {
				// 如果有count的时候创建的托盘则逻辑删除这种配置
				floorReceiptsMgrWfh.updateContainerConfig(container_config_id,
						falseDelete);
			} else {
				floorReceiptsMgrWfh
						.deleteContainerConfigByConConfigId(container_config_id);

			}

			// ***五 删除line与这种配置以及pallet的关系
			if (deletePalletIds.length() > 0) {
				deletePalletIds = deletePalletIds.substring(0,
						deletePalletIds.length() - 1);
				floorReceiptsMgrWfh
						.deleteReceiptContainerByConIds(deletePalletIds);
				// floorReceiptsMgrWfh.deleteReceRelConByConConfigId(container_config_id);
			}

			// 添加log
			DBRow taskLog = new DBRow();
			taskLog.add("operator_type", "Delete pallet config");
			taskLog.add("operator_id", adid);
			taskLog.add("operator_time", DateUtil.NowStr());
			taskLog.add("receipt_line_id", receipt_line_id);
			taskLog.add("receipt_line_status", line.get("status", 0));
			taskLog.add("data", log.substring(0, log.length() - 1));
			floorReceiptsMgrWfh.addTaskLog(taskLog);
			result.add("success", "true");
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 删除qty 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/deleteQTY", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deleteQTY(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") long con_id)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 参数验证
			if (con_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			// 查找pallet的qty cp_id
			DBRow cp = floorReceiptsMgrWfh
					.findContainerProductByPalletId(con_id);
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByPalletId(con_id);
			DBRow cc = floorReceiptsMgrWfh.findContainerConfigByConID(con_id);

			// 如果已经有sn则全部移除
			DBRow[] sns = floorReceiptsMgrWfh.findSNByPalletId(con_id);
			for (DBRow sn : sns) {
				floorReceiptsMgrWfh.deleteSNBySPId(sn.get("serial_product_id",
						0L));
			}
			// 删除这个pallet的qty数据
			DBRow upRow = new DBRow();
			if (cp != null && cp.get("cp_id", 0l) > 0) {
				floorReceiptsMgrWfh.deleteContainerProductByCpId(cp.get(
						"cp_id", 0l));
				result.add("success", "true");
			}
			// 更新 receipt line
			DBRow rrc = floorReceiptsMgrWfh
					.findReceiptRelContainerByConID(con_id);
			upRow = new DBRow();
			upRow.add("goods_total",
					line.get("goods_total", 0) - rrc.get("goods_qty", 0F)); // 如果以前没有则-0
																			// 若果有则-对应的数量不会出问题
			if (cc.get("goods_type", 0) == GOODS_TYPE_DAMAGE) { // 只有盛放损坏货物的托盘发生变化时才更新line上的damage_total
				upRow.add("damage_total",
						line.get("damage_total", 0) - rrc.get("damage_qty", 0F));
			}
			floorReceiptsMgrWfh.updateReceiptLine(
					line.get("receipt_line_id", 0L), upRow);
			// 更新receipt container
			upRow = new DBRow();
			upRow.add("normal_qty", 0);
			upRow.add("damage_qty", 0);
			floorReceiptsMgrWfh.updateReceiptContainerByConID(con_id, upRow);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 获得item下正常货物pallet的的palletConfig 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/findItemPalletConfig", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findItemPalletConfig(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type,
			@RequestParam(value = "is_clp", required = false, defaultValue = "0") int is_clp,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);

			// 参数验证
			if (receipt_line_id == 0 || ps_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			// 货物类型,0:Normal;1:Overage;2:Damaged;3:ALL
			DBRow[] config = floorReceiptsMgrWfh.findPalletConfigByLineId(
					receipt_line_id, goods_type, is_clp); // 查找正常的托盘配置
			List<DBRow> palletArray = new ArrayList<DBRow>();
			// List<DBRow> damagedArray = new ArrayList<DBRow>();
			// 查找每个config下面的托盘是否有打印过的
			for (DBRow row : config) {
				DBRow[] conRelCon = floorReceiptsMgrWfh
						.findReceiptRelContainer(
								row.get("container_config_id", 0L), 0);
				String printUser = "";
				String printUserName = "";
				for (DBRow con : conRelCon) {
					if (!con.get("print_user", "").equals("")) {
						if (printUser.contains(con.get("print_user", "")))
							continue;
						printUser += con.get("print_user", "") + ",";
						printUserName += con.get("print_user_name", "") + ",";
					}
				}
				printUser = printUser.length() > 0 ? printUser.substring(0,
						printUser.length() - 1) : printUser;
				printUserName = printUserName.length() > 0 ? printUserName
						.substring(0, printUserName.length() - 1)
						: printUserName;
				row.add("print_user", printUser);
				row.add("print_user_name", printUserName);

				if (row.get("goods_type", 0) == GOODS_TYPE_DAMAGE) {
					// damagedArray.add(row);
				} else {
					if (row.get("is_partial", 0) == 1) {
						// 如果是 is_partial 的。
						DBRow[] palletIds = floorReceiptsMgrZyy
								.getPartialConIds(
										row.get("receipt_line_id", 0l),
										row.get("container_config_id", 0l),
										row.get("process", 0),
										row.get("is_partial", 0));
						for (DBRow conId : palletIds) {
							DBRow _row = (DBRow) row.clone();
							_row.add("con_id", conId.get("con_id", ""));
							_row.put("plate_number", 1);
							_row.put("keep_inbound", 0);
							palletArray.add(_row);
						}

					} else {
						// 不是 is_partial
						palletArray.add(row);
					}
				}
			}
			// 添加坏的托盘
			DBRow[] damageRow = floorReceiptsMgrWfh.findPalletByLineId(
					receipt_line_id, 0, GOODS_TYPE_DAMAGE);
			for (DBRow damage : damageRow) {
				damage.add(
						"create_date",
						DateUtil.showLocalparseDateTo24Hours(
								damage.get("create_date", ""), ps_id));
				damage.add("wms_container_id", damage.get("container_no", 0));
			}

			result.add("pallets", palletArray.toArray(new DBRow[0]));
			if (is_clp == 0) {
				result.add("damaged", damageRow);
			}
			// add by zhaoyy 2015年6月19日 10:23:22
			DBRow[] containerTypes = floorContainerMgrZYZ.getAllContainerType(
					null, null);
			result.add("conTypes", containerTypes);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 删除选定的pallet 2015年3月5日
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/deletePallet", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deletePallets(
			@RequestParam(value = "con_ids", required = false, defaultValue = "") String con_ids,
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "is_scan_delete", required = false, defaultValue = "0") int is_scan_delete,
			HttpSession session, HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		String messageFromSync = null;
		try {
			// 参数验证
			if (receipt_line_id == 0 || "".equals(con_ids) || adid == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			String photoIDS = "";
			boolean dataFlag = false;
			boolean haveCountPallet = false;
			// 查找这种配置下托盘一共有多少个
			DBRow[] containerCount = floorReceiptsMgrWfh
					.findContainerRelReceipt(con_ids);
			StringBuffer log = new StringBuffer();
			log.append("Delete pallet|");
			String[] con_id = con_ids.split(",");

			DBRow upFalseDeletePallet = new DBRow();
			upFalseDeletePallet.add("is_delete", 1);

			con_ids = ""; // 将ID清空 留作后面物理删除使用
			String con_logic_delete = "";// 逻辑删除的托盘，需要删除sn
			Map<String, Object> m = session == null ? null
					: (Map<String, Object>) session.getAttribute("adminSesion");
			String account = floorReceiptsMgrZzq.queryAccountByAdid(Integer
					.valueOf(m.get("adid").toString()));

			for (String conID : con_id) {
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByPalletId(Long
						.parseLong(conID));
				DBRow container = floorReceiptsMgrWfh.findContainerByConId(Long
						.parseLong(conID));
				if (container.get("keep_inbound", 0) == 1) {
					throw new ReceiveForAndroidException(
							"Pallet config keep inbound ,not allowed delete");
				}
				// 如果是在scan的时候删除不进行下面的操作
				if (is_scan_delete != 1) {
					DBRow rrc = floorReceiptsMgrWfh
							.findReceiptRelContainerByConID(Long.valueOf(conID));

					// 是否加上提示
					DBRow containerConfig = floorReceiptsMgrWfh
							.findContainerConfigByConConfigId(containerCount[0]
									.get("container_config_id", 0L));
					if (containerConfig.get("goods_type", 0) == GOODS_TYPE_NORMAL) { // 必须是正常配置的托盘并且有损坏的货物数量才会提示
						if (rrc.get("damage_qty", 0) > 0) {
							dataFlag = true;
						}
					}
					// 更新 receipt_line 上的total
					DBRow upRow = new DBRow();
					upRow.add(
							"goods_total",
							line.get("goods_total", 0)
									- rrc.get("normal_qty", 0F)); // 如果以前没有则-0
					// 若果有则-对应的数量不会出问题
					DBRow cc = floorReceiptsMgrWfh.findContainerByConId(Long
							.parseLong(conID));
					if (cc.get("goods_type", 0) == GOODS_TYPE_DAMAGE) { // 只有盛放损坏货物的托盘发生变化时才更新line上的damage_total
						upRow.add("damage_total", line.get("damage_total", 0)
								- rrc.get("damage_qty", 0F));
					}
					floorReceiptsMgrWfh.updateReceiptLine(
							line.get("receipt_line_id", 0L), upRow);
				} else if (is_scan_delete == 1
						&& container.get("status", ContainerStatueKey.CON_OPEN) == ContainerStatueKey.CON_CLOSE) {
					// delete inventory
					// 在scan操作时，如果删除了已关闭的tlp托盘，删除库存数据
					DBRow lineRow = floorReceiptsMgrWfh
							.findReceiptLineByLineId(receipt_line_id);
					DBRow ps = floorReceiptsMgrWfh.findPSIDByCompanyId(lineRow
							.get("company_id", ""));
					long ps_id = ps.get("ps_id", 0L);
					DBRow r = invent.deleteContainer(ps_id,
							container.get("con_id", 0L));
					if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
						throw new ReceiveForAndroidException(
								"Inventory exception!");
					}
					deleteInventoryLogByBatch(ps_id,
							container.get("con_id", 0L), account, m);
				}

				// update movement status
				// added by zhengziqi at May.5th 2015 逻辑删 物理删都会更新movenment
				floorReceiptsMgrZzq.updateMultipleMovementStatus(conID, adid,
						DateUtil.NowStr());

				log.append("pallet: "
						+ "(94)"
						+ CommonUtils.formatNumber("#000000000000",
								container.get("container", 0L)));

				int process = container.get("process", 0);
				// 在sacn的时候逻辑删除count创建的托盘
				if (process == CreateContainerProcessKey.PROCESS_COUNT
						&& is_scan_delete == 1) {
					haveCountPallet = true;
					floorReceiptsMgrWfh.updateContainer(Long.valueOf(conID),
							upFalseDeletePallet);
					con_logic_delete += conID;
				} else if (process == CreateContainerProcessKey.PROCESS_CC
						&& is_scan_delete == 1) {
					haveCountPallet = true;
					floorReceiptsMgrWfh.updateContainer(Long.valueOf(conID),
							upFalseDeletePallet);
					con_logic_delete += conID;
					// TODO CC 创建的TLP 托盘 也是可以删除的 物理删除 需要处理，在Break的数量的问题是否会
					// if(container.get("container_type", 0)==
					// ContainerTypeKey.TLP){
					// floorReceiptsMgrZyy.delContainerById(Long.parseLong(conID));
					// }else{
					// //添加删除Scan 时空的 CLP的托盘逻辑 2015年6月5日 17:09:13
					// DBRow[] qtys =
					// floorReceiptsMgrZyy.getPalletQtyByLineId(receipt_line_id);
					// if(null!=qtys && qtys.length>0){
					// for(DBRow r:qtys){
					// int container_type = r.get("container_type", 0);
					// if(container_type==1){
					// //clp 的数量
					// line.add("clp_expected_qty", r.get("expected_qty", 0));
					// }
					// }
					// }
					// DBRow[] conSn = floorReceiptsMgrWfh.findSNByConIds(
					// conID);
					// DBRow[] noBreakPallet =
					// floorReceiptsMgrZyy.findNoBreakPalletByLineId(receipt_line_id,
					// 0, ReceiptForAndroidController.GOODS_TYPE_NORMAL);
					// if(null!=conSn && conSn.length>0){
					// throw new
					// ReceiveForAndroidException("Has sn not allow delete");
					// }else{
					// //删除CLP 托盘 逻辑删除托盘 line.get("clp_expected_qty", 0) >
					// hasbreakQty &&
					// if( null==noBreakPallet || noBreakPallet.length==0 ){
					// floorReceiptsMgrZyy.delContainerById(Long.parseLong(conID));
					// ret = BCSKeyReceive.SUCCESS;
					// err = BCSKeyReceive.FAIL;
					// result.add("ret", ret);
					// result.add("err", err);
					// result.add("data", "Delete success");
					// return CommonUtils.convertDBRowsToJsonString(result);
					// }else{
					// throw new
					// ReceiveForAndroidException("Not allowed delete CLP pallet");
					// }
					// }
					//
					// }
					// 出去 count 时 创建的托盘 ，其他的托盘 全是 物理删除，不管CC

				} else {
					// 否则物理删除托盘 再将物理删除的托盘id记录下来
					con_ids += conID + ",";
					// 拿到这些托盘下面的照片
					DBRow[] photo = floorReceiptsMgrWfh
							.findPalletPhotoByDetailId(
									container.get("con_detail_id", 0L),
									Long.parseLong(conID));
					for (DBRow p : photo) {
						photoIDS += p.get("file_id", 0L) + ",";
					}
				}
			}
			// 逻辑删除托盘逻辑
			// add by zhengziqi at July.6th 2015
			if (con_logic_delete.length() > 0) {
				// 删除sn
				DBRow[] sns = floorReceiptsMgrWfh
						.findSNByConIds(con_logic_delete);
				for (DBRow sn : sns) {
					floorReceiptsMgrWfh.deleteSNBySPId(sn.get(
							"serial_product_id", 0l));
				}
			}
			// 物理删除托盘逻辑
			if (con_ids.length() > 0) {
				con_ids = con_ids.substring(0, con_ids.length() - 1);
				// 删除sn
				DBRow[] sns = floorReceiptsMgrWfh.findSNByConIds(con_ids);
				for (DBRow sn : sns) {
					floorReceiptsMgrWfh.deleteSNBySPId(sn.get(
							"serial_product_id", 0l));
				}
				// 删除contianer_product
				DBRow[] qtys = floorReceiptsMgrWfh
						.findCountQTYByConIds(con_ids);
				for (DBRow qty : qtys) {
					floorReceiptsMgrWfh.deleteContainerProductByCpId(qty.get(
							"cp_id", 0l));
				}
				// 删除container
				floorReceiptsMgrWfh.deleteContainerByConIds(con_ids);
				// 删除line与container与配置的关系
				floorReceiptsMgrWfh.deleteReceiptContainerByConIds(con_ids);
				// 移除删除的pallet下面的照片
				if (photoIDS.length() > 0) {
					photoIDS = photoIDS.substring(0, photoIDS.length() - 1);
					this.deletePhotos(photoIDS, request);
				}
			}
			// 如果有count时的托盘 并且这种配置的托盘全部删除了则将这种配置更新为逻辑删除 放到物理删除托盘之后
			// 如果逻辑删除的托盘等于这种配置下所有的托盘则删除这种配置
			DBRow[] containers = floorReceiptsMgrWfh
					.findContainerByConConfigId(containerCount[0].get(
							"container_config_id", 0L));
			int logicDelete = 0;
			for (DBRow con : containers) {
				if (con.get("is_delete", 0) == 1) {
					logicDelete++;
				}
			}
			if (haveCountPallet && containers.length == logicDelete) {
				floorReceiptsMgrWfh.updateContainerConfig(
						containerCount[0].get("container_config_id", 0L),
						upFalseDeletePallet);
			}
			// 如果这种配置的托盘全部删除了 则删除这种配置
			else if (containers.length == logicDelete) {
				floorReceiptsMgrWfh
						.deleteContainerConfigByConConfigId(containerCount[0]
								.get("container_config_id", 0L));
			}
			// 添加log
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			DBRow taskLog = new DBRow();
			taskLog.add("operator_type", "Delete pallet");
			taskLog.add("operator_id", adid);
			taskLog.add("operator_time", DateUtil.NowStr());
			taskLog.add("receipt_line_id", receipt_line_id);
			taskLog.add("receipt_line_status", line.get("status", 0));
			taskLog.add("data", log.substring(0, log.length() - 1));
			floorReceiptsMgrWfh.addTaskLog(taskLog);
			if (dataFlag)
				result.add("data",
						"Delete the tray contains damaged goods may need to remove the damage pallet");
			result.add("success", "true");

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		if (messageFromSync != null) {
			JSONObject object = new JSONObject(messageFromSync);
			return object.toString();
		}
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 获取receipt下的所有line和pallet
	 * 
	 * @param line_id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/findAllPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findAllPallet(
			@RequestParam(value = "receipt_id", required = false, defaultValue = "0") long receipt_id)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 参数验证
			if (receipt_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			DBRow receipt = floorReceiptsMgrWfh.findReceiptById(receipt_id);
			DBRow ps = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get(
					"company_id", ""));
			long ps_id = ps.get("ps_id", 0L);
			DBRow[] lines = floorReceiptsMgrWfh
					.findReceiptLineByReceiptId(receipt_id);
			for (DBRow line : lines) {
				DBRow[] pallets = floorReceiptsMgrWfh.findContainerByLineId(
						line.get("receipt_line_id", 0L), GOODS_TYPE_ALL);
				for (DBRow pallet : pallets) {
					pallet.add(
							"print_date",
							DateUtil.showLocalparseDateToNoYear24Hours(
									pallet.get("print_date", ""), ps_id));
					pallet.add(
							"container_no",
							"(94)"
									+ CommonUtils.formatNumber("#000000000000",
											pallet.get("container_no", 0)));
				}
				line.add("pallets", pallets);
			}
			result.add("data", lines);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 * 完成这条line的count
	 * 
	 * @param line_id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/finishCount", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String finishCount(
			@RequestParam(value = "line_id", required = false, defaultValue = "0") long line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			HttpSession session, HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;

		try {
			result = writebackToWmsMgrZyy.finishCount(line_id, adid, session);
			return CommonUtils.convertDBRowsToJsonString(result);
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
			result.add("ret", ret);
			result.add("err", err);
			throw new Exception(e.getMessage());
		}
	}

	public List<Map<String, Object>> handleMovementAndInventoryByBatch(
			long line_id, String company_id, long ps_id, long adid)
			throws Exception {
		DBRow[] pallets = floorReceiptsMgrZzq
				.findContainerByLineIdForInventory(line_id);
		// 批量插入库存
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		DBRow entryRow = floorReceiptsMgrZzq.queryEntryByLineId(line_id);
		String[] seal_number = new String[2];
		seal_number[0] = entryRow.getString("seal_delivery");
		seal_number[1] = entryRow.getString("seal_pick_up");
		for (DBRow con : pallets) {
			// 得到location_id
			// long location_id =
			// acquireLocationIdByLocationNameAndPsId(STAGING_TYPE, ps_id,
			// con.get("location", ""), false);
			// update movements 现在关闭托盘时不会记录movement
			// updateMysqlMovements(con.get("con_id", 0L), adid, company_id,
			// location_id, con.get("location", ""), STAGING_TYPE);

			Map<String, Object> map = buildSingleInventory(
					con.getString("con_id"), con.get("qty", 0), ps_id,
					con.getString("location"), STAGING_TYPE, seal_number);
			list.add(map);
		}
		return list;
	}

	/**
	 *
	 * log Movement 2015年4月11日
	 * 
	 * @param pallet_id
	 * @param location_id
	 * @param location_name
	 * @param company_id
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/logMovement", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String logMovement(
			@RequestParam(value = "plate_nos", required = false, defaultValue = "0") String plate_nos,
			@RequestParam(value = "location_type", required = false, defaultValue = "1") int location_type,
			@RequestParam(value = "location_name", required = false, defaultValue = "") String location_name,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			HttpSession session, HttpServletRequest request) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// 获取adminLoginBean
			@SuppressWarnings("unchecked")
			Map<String, Object> adminBean = (Map<String, Object>) session
					.getAttribute(Config.adminSesion);

			// 获取location_id
			long location_id = acquireLocationIdByLocationNameAndPsId(
					location_type, ps_id, location_name, true);

			// 更新
			DBRow[] rows = floorReceiptsMgrZzq.queryConIdByPlateNos(plate_nos);
			if (rows == null || rows.length < 1) {
				throw new ReceiveForAndroidException("Pallet not found!");
			}
			// container
			floorReceiptsMgrZzq.updateLocationInfoByPlateNos(plate_nos,
					location_name, location_type, location_id);
			// sertial_product
			floorReceiptsMgrZzq.updateLocationInfoForSerialProductByPlateNos(
					plate_nos, location_type, location_id);

			List<Long> conArray = new ArrayList<Long>();
			for (int i = 0; i < rows.length; i++) {
				DBRow row = rows[i];
				long pallet_id = row.get("con_id", 0L);
				long container_id = row.get("container", 0L);
				String company_id = row.get("company_id", "");
				// update movements
				updateMysqlMovements(pallet_id, adid, company_id, location_id,
						location_name, location_type);

				// 回写wms
				DBRow moveWhere = new DBRow();
				moveWhere.put("CompanyID", company_id);
				moveWhere.put("PlateNo", container_id);
				DBRow moveData = new DBRow();
				moveData.put("Status", "Closed");
				moveData.put("DateUpdated",
						DateUtil.showLocationTime(new Date(), ps_id));// 更新时间
				moveData.put("UserUpdated", adminBean.get("employe_name") + "");// 更新用户名
				floorReceiptSqlServerMgrSbb
						.updateMovements(moveWhere, moveData);
				String[] movements = {
						// 用户名
						adminBean.get("employe_name") + "",
						// company_id
						company_id,
						// 托盘NO
						container_id + "",
						// 状态
						"Open", location_name + "" };
				floorReceiptSqlServerMgrSbb.insertMovements(movements);

				// put away已关闭的托盘，才会引发后续库存操作
				if (row.get("status", 1) == 2) {
					conArray.add(row.get("con_id", 0L));
				}
			}
			// update inventory
			Long[] con_ids = conArray.toArray(new Long[conArray.size()]);
			DBRow r = invent
					.putAway(ps_id, location_id, location_type, con_ids);
			if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
				throw new ReceiveForAndroidException("Inventory exception!");
			}
			addPutAwayLogByBatch(ps_id, location_id, location_type, con_ids,
					session);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (JSONException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	/**
	 *
	 * update Mysql Movements 2015年4月16日
	 * 
	 * @param pallet_id
	 * @param adid
	 * @param company_id
	 * @param location_id
	 * @param location_name
	 * @throws Exception
	 *
	 */
	public void updateMysqlMovements(long pallet_id, long adid,
			String company_id, long location_id, String location_name,
			int location_type) throws Exception {

		DBRow mRow = floorReceiptsMgrZzq.queryOpenMovement(pallet_id);
		if (mRow != null) {
			floorReceiptsMgrZzq.updateMovementStatus(pallet_id, adid,
					DateUtil.NowStr());
		}
		DBRow movementRow = new DBRow();
		movementRow.add("company_id", company_id);
		movementRow.add("plate_no", pallet_id);
		movementRow.add("slc_id", location_id);
		movementRow.add("slc_name", location_name);
		movementRow.add("slc_type", location_type);
		movementRow.add("movement_printed", 0);
		movementRow.add("plate_printed", 0);
		movementRow.add("date_created", DateUtil.NowStr());
		movementRow.add("user_created", adid);
		floorReceiptsMgrZzq.addMovement(movementRow);
	}

	/**
	 * `re Location_Id By Location_Name And Ps_Id 2015年4月15日
	 * 
	 * @param location_type
	 * @param ps_id
	 * @param location_name
	 * @return
	 * @throws Exception
	 *
	 */
	public Long acquireLocationIdByLocationNameAndPsId(int location_type,
			long ps_id, String location_name, Boolean is_check_location)
			throws Exception {
		long location_id = 0L;
		DBRow[] locationRows = null;
		if (location_type == LOCATION_TYPE) {
			// location
			locationRows = floorReceiptsMgrZzq
					.queryLocationIdByLocationNameInLocation(ps_id,
							location_name);
		} else {
			// staging
			locationRows = floorReceiptsMgrZzq
					.queryLocationIdByLocationNameInStaging(ps_id,
							location_name);
		}
		if (locationRows != null && locationRows.length > 0) {
			location_id = locationRows[0].get("obj_id", 0L);
		} else if (is_check_location) {
			String msg = (location_type == 1) ? "Location " : "Staging ";
			throw new ReceiveForAndroidException(msg + "Not Exist!");
		}
		return location_id;
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/acquireMovementByPalletId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireMovementByPalletId(
			@RequestParam(value = "plate_no", required = false, defaultValue = "") String plate_no,
			@RequestParam(value = "company_id", required = false, defaultValue = "W12") String company_id,
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow movementRow = null;
		try {
			plate_no = replaceByRegularExpress(plate_no);
			// update by zhaoyy 添加 company_id
			DBRow row = floorReceiptsMgrZzq.queryConIdByPlateNoAndompanyId(
					Long.valueOf(plate_no), company_id);
			if (row != null) {
				long pallet_id = row.get("con_id", 0L);
				movementRow = floorReceiptsMgrZzq.queryOpenMovement(pallet_id);
			} else {
				throw new ReceiveForAndroidException("Pallet not found!");
			}
			// 查询wms数据
			// 现在不考虑wms的托盘的movement edit by zhengziqi at Aug.13th 2015
			// if(movementRow == null){
			// movementRow =
			// floorReceiptsMgrZzq.qureyWmsMovementInfo(Long.valueOf(plate_no));
			// if(movementRow != null){
			// movementRow.add("movement_printed",
			// (Boolean)movementRow.get("movement_printed")?1:0);
			// movementRow.add("plate_printed",
			// (Boolean)movementRow.get("plate_printed")?1:0);
			// }
			// }
			if (movementRow == null) {
				throw new ReceiveForAndroidException(
						"Movement record not found!");
			}
			movementRow.add("plate_no", plate_no);
			movementRow.add(
					"date_created",
					DateUtil.showLocalTime(
							movementRow.getString("date_created"), ps_id));
			movementRow.add("con_id", row.get("con_id", 0L));
			result.add("data", movementRow);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		// 验证
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	String replaceByRegularExpress(String str) {
		Pattern pattern = Pattern.compile("940+");
		Matcher matcher = pattern.matcher(str);
		// 替换第一个符合正则的数据
		String result = matcher.replaceFirst("");
		return result;
	}

	public Map<String, Object> synchronizationExpectedReceiptAndLines(
			DBRow parameter) throws Exception {

		Map<String, Object> result = new HashMap<String, Object>();

		// 参数
		DBRow paramReceipt = new DBRow();
		paramReceipt.put("companyId", parameter.get("company_id"));
		paramReceipt.put("receiptNo", parameter.get("receipt_no"));
		paramReceipt.put("detail_id", parameter.get("detail_id"));
		// paramReceipt.add("detail_id", parameter.get("detail_id", 0L));
		DBRow sqlRow = new DBRow();
		sqlRow.add("companyId", parameter.get("company_id"));
		sqlRow.add("receipt_no", parameter.get("receipt_no"));

		// 查询SqlServer预计收货主单据
		DBRow oldReceiptRow = floorReceiptSqlServerMgrSbb
				.getOldExpectedReceipt(sqlRow);

		// 添加 ps_id

		oldReceiptRow.add("ps_id", parameter.get("ps_id"));

		// 查询SqlServer预计收货明细
		DBRow[] oldReceiptLinesRow = floorReceiptSqlServerMgrSbb
				.getOldExpectedReceiptLines(sqlRow);

		// 验证
		// Map<String,Object> validate =
		// this.validateReceipt(oldReceiptLinesRow);

		// if((boolean) validate.get("success")){

		// oldReceiptLinesRow = (DBRow[]) validate.get("oldReceiptLinesRow");
		// 同步到MySql预计收货表
		if (oldReceiptLinesRow != null && oldReceiptLinesRow.length > 0) {
			oldReceiptRow.add("detail_id", parameter.get("detail_id", 0L));
			this.updateExpectedReceiptAndLine(paramReceipt, oldReceiptRow,
					oldReceiptLinesRow);
			result.put("success", true);
		}

		/*
		 * }else{
		 * 
		 * result.put("success", false); result.put("error",
		 * "No products Record"); }
		 */

		return result;
	}

	/** 同步到MySql预计收货表-[customerItems] */
	private void updateExpectedReceiptAndLine(DBRow paramReceipt,
			DBRow oldReceiptRow, DBRow[] oldReceiptLinesRow) throws Exception {

		// 存在此预收货主单据
		if (oldReceiptRow != null) {

			DBRow param = new DBRow();
			param.put("company_id", paramReceipt.get("companyId", "-1"));
			param.put("receipt_no", paramReceipt.get("receiptNo", "-1"));

			// 查询MySql预计收货主单据
			DBRow receiptRow = floorReceiptMgr.getExpectedReceipt(param);

			// 插入MySql预计收货主单据和明细
			if (receiptRow == null) {
				// 更新handl_time
				DBRow upRow = new DBRow();
				upRow.add("handle_time", DateUtil.NowStr());
				floorCheckInMgrZwb.updateDetailByIsExist(
						paramReceipt.get("detail_id", 0L), upRow);

				long receiptId = floorReceiptMgr
						.insertExpectedReceipt(oldReceiptRow);

				if (oldReceiptLinesRow != null) {

					for (DBRow oneResult : oldReceiptLinesRow) {
						oneResult
								.add("unloading_start_time", DateUtil.NowStr());
						oneResult.put("receipt_id", receiptId);

						oneResult.put("wms_item",
								oneResult.get("item_id", "-1"));
						oneResult.put("wms_lot_no",
								oneResult.get("lot_no", "-1"));
						oneResult.put("wms_qty",
								oneResult.get("expected_qty", -1L));
						// oneResult.put("detail_id",
						// paramReceipt.get("detail_id", 0L));
						floorReceiptMgr.insertExpectedReceiptLines(oneResult);
					}
				}
			}
		}
	}

	/**
	 * 验证 itme_id/lot_no 与 product_id 是否相同
	 * 
	 * @param 收货明细
	 * @return 如果相同,返回加入pc_id 的收货明细和success=true,如果不同,返回success=false
	 * @author subin
	 * */
	public Map<String, Object> validateReceipt(DBRow[] oldReceiptLinesRow)
			throws Exception {

		Map<String, Object> returnVal = new HashMap<String, Object>();

		boolean result = true;

		for (DBRow oneResult : oldReceiptLinesRow) {

			String productId = "";

			productId = oneResult.get("item_id", "") + "/"
					+ oneResult.get("lot_no", "");
			DBRow[] product = floorReceiptMgr.getProductByPname(productId);
			// 去product_code表查p_code
			if (product == null || product.length < 1) {
				productId = oneResult.get("item_id", "");
				product = floorReceiptsMgrZzq.queryProductByMainCode(productId);
			}

			if (product == null || product.length != 1) {

				result = false;

				returnVal.put("company_id", oneResult.get("company_id", ""));
				returnVal.put("receipt_no", oneResult.get("receipt_no", ""));
				returnVal.put("line_no", oneResult.get("line_no", ""));
				returnVal.put("item_id", oneResult.get("item_id", ""));
				returnVal.put("lot_no", oneResult.get("lot_no", ""));

				break;

			} else {

				oneResult.put("pc_id", product[0].get("pc_id", -1L));
			}
		}
		returnVal.put("success", result);
		returnVal.put("oldReceiptLinesRow", oldReceiptLinesRow);

		return returnVal;
	}

	public DBRow getPrintServerByLoginUser(long ps_id, long area_id, int type)
			throws Exception {
		DBRow returnRow = new DBRow();
		try {
			DBRow[] rows = floorGoogleMapsMgrCc
					.getPrinterServerByAreaIdandPtype(area_id);
			DBRow defaultPrintServer = null;
			if (rows != null && rows.length > 0) {
				defaultPrintServer = rows[0];
			}
			DBRow[] allPrintServer = floorPrintTaskMgrZr
					.queryAllPrintServerPrinterAndZoneBy(ps_id, type);
			if (defaultPrintServer != null) {
				boolean flag = isDefaultServerInAllPrintServer(
						defaultPrintServer.get("printer_server_id", 0l),
						allPrintServer);
				returnRow.add("default",
						flag ? defaultPrintServer.get("printer_server_id", 0l)
								: 0);
			} else {
				returnRow.add("default", 0);
			}
			fixPrintServerData(allPrintServer);
			returnRow.add("allPrintServer", allPrintServer);
			return returnRow;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	/**
	 * 清除printServer 不需要返回的数据
	 * 
	 * @param rows
	 * @throws Exception
	 */
	public List<DBRow> fixPrintServerData(DBRow[] rows) throws Exception {
		if (rows != null && rows.length > 0) {
			List<DBRow> list = new ArrayList<DBRow>();
			for (DBRow row : rows) {
				row.remove("adid");
				row.remove("last_refresh_time");
				row.remove("title");
				row.remove("employe_name");
				row.remove("ps_id");

				Boolean isonline = checkAddPrintTask(CommonUtils.getLong(row
						.getString("PRINTER_SERVER_ID")));
				row.add("isonline", isonline);
				if (isonline) {
					list.add(row);
				}
			}
			for (DBRow row : rows) {
				if (!(Boolean) row.get("isonline")) {
					list.add(row);
				}
			}
			return list;
		}
		return null;
	}

	/**
	 * 检查当前的printServer 1.是否数据库存在，当前是否可以浏览器当前是否push到
	 * 2.因为无法判断浏览器是否是关闭这种情况,printServer 里面有时间字段，这个字段会每隔RefreshTimes
	 * 秒更新，如果android打印的时候，当前时间在这个之间那么 就是可以使用的
	 * 
	 * @param row
	 * @throws Exception
	 */
	private Boolean checkAddPrintTask(long print_server_id)
			throws AndroidPrintServerUnLineException, Exception {
		boolean flag = false;
		long nowTime = new Date().getTime();
		DBRow data = floorPrintTaskMgrZr.getPrintServerById(print_server_id);

		if (data != null) {
			flag = nowTime - data.get("last_refresh_time", 0l) <= 5 * RefreshTimes * 1000;
		}
		return flag;
	}

	private boolean isDefaultServerInAllPrintServer(long defaultServer,
			DBRow[] allPrintServer) {
		boolean flag = false;
		for (DBRow row : allPrintServer) {
			if (defaultServer == row.get("printer_server_id", 0l)) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	// 上传图片
	private String uploadPhotos(HttpServletRequest request) throws Exception {
		ZipInputStream zis = null;
		File zip = null;
		ZipFile zf = null;
		try {
			List<File> uploadFiles = new ArrayList<File>();
			// 获取该文件的上传域
			if (ServletFileUpload.isMultipartContent(request)) {
				javax.servlet.http.Part part = request.getPart("file"); // 拿到文件
																		// part
				zip = new File(part.getName()); // zip file 在这个方法结束删除
				this.inputStream2File(part.getInputStream(), zip); // 将流的内容写到file中

				zf = new ZipFile(zip);
				zis = new ZipInputStream(part.getInputStream());
				ZipEntry ze;
				while ((ze = zis.getNextEntry()) != null) {
					if (!ze.isDirectory()) {
						File zipFile = new File(ze.getName()); // photo
																// file在上传完成之后再delete
						this.inputStream2File(zf.getInputStream(ze), zipFile);
						uploadFiles.add(zipFile);
					}
				}
				// 上传到文件服务器
				return this.uploadPhotoToFileServer(uploadFiles, request);
			}
			return "";
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			// 关闭资源
			if (zis != null)
				zis.close();
			if (zf != null)
				zf.close();
			if (zip != null)
				zip.delete();
		}
	}

	// 将图片通过httpClien 上传到文件服务
	private String uploadPhotoToFileServer(List<File> uploadFiles,
			HttpServletRequest request) throws Exception {
		PostMethod post = null;
		try {
			if (uploadFiles.size() > 0) {
				String sessionID = request.getSession(false).getId();
				HttpClient client = new HttpClient();
				post = new PostMethod(file_url + "/upload");
				post.setRequestHeader("Cookie", "JSESSIONID=" + sessionID);
				post.getParams().setContentCharset("UTF-8");
				List<Part> body = new ArrayList<Part>();
				for (int i = 0; i < uploadFiles.size(); i++) {
					Part p = new FilePart(uploadFiles.get(i).getName(),
							uploadFiles.get(i), null, "UTF-8");
					body.add(p);
				}
				post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
						new DefaultHttpMethodRetryHandler());
				RequestEntity requestEntity = new MultipartRequestEntity(
						body.toArray(new Part[0]), post.getParams());
				post.setRequestEntity(requestEntity);

				int statue = client.executeMethod(post);
				String bodyString = post.getResponseBodyAsString();
				if (statue != 200) {
					throw new Exception();
				}
				return bodyString;
			} else {
				return "";
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (post != null)
				post.releaseConnection();
			for (int i = 0; i < uploadFiles.size(); i++) { // 删除解压过后生成的jpg文件
				uploadFiles.get(i).delete();
			}
		}
	}

	// 将图片删除
	private String deletePhotos(String fileIds, HttpServletRequest request)
			throws Exception {
		DeleteMethod delete = null;
		try {
			if (fileIds != null && !"".equals(fileIds)) {
				String sessionID = request.getSession(false).getId();
				HttpClient client = new HttpClient();
				delete = new DeleteMethod(file_url + "/file/" + fileIds);
				delete.setRequestHeader("Cookie", "JSESSIONID=" + sessionID);
				delete.getParams().setContentCharset("UTF-8");
				int statue = client.executeMethod(delete);
				String bodyString = delete.getResponseBodyAsString();
				if (statue != 200) {
					throw new Exception();
				}
				return bodyString;
			} else {
				return "";
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (delete != null)
				delete.releaseConnection();
		}

	}

	public String inputStream2String(InputStream in) throws IOException {
		StringBuffer out = new StringBuffer();
		byte[] b = new byte[4096];
		for (int n; (n = in.read(b)) != -1;) {
			out.append(new String(b, 0, n));
		}
		in.close(); // 关闭输入流
		return out.toString();
	}

	// 将inputStream中的内容写到file文件中去
	private void inputStream2File(InputStream is, File file) throws Exception {
		OutputStream os = null;
		try {
			os = new FileOutputStream(file);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = is.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (os != null) {
				os.close();
			}
			is.close();
		}
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class }, propagation = Propagation.REQUIRES_NEW)
	public void recordLog(DBRow param) throws Exception {

		DBRow row = new DBRow();
		row.add("operator_id", param.get("adid", -1L));
		row.add("operator_time", DateUtil.NowStr());
		row.add("note", param.get("note", "-1"));
		row.add("dlo_id", param.get("entry_id", -1L));
		row.add("log_type", param.get("log_type", CheckInLogTypeKey.RECEIPT));
		row.add("data", param.get("data"));

		floorReceiptMgr.insertRecordLog(row);
	}

	@SuppressWarnings("unchecked")
	private long getLoginPsID(HttpSession session) {
		Map<String, Object> m = session == null ? null
				: (Map<String, Object>) session.getAttribute("adminSesion");
		return Long.parseLong(m.get("ps_id").toString());
	}

	/**
	 *
	 * 2015年4月14日
	 * 
	 * @param con_id
	 * @param sn_no
	 * @throws NumberFormatException
	 * @throws Exception
	 *
	 */
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/addInventroy", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String addInventroy(
			@RequestParam(value = "con_id", required = false, defaultValue = "0") String con_id,
			@RequestParam(value = "qty", required = false, defaultValue = "0") int qty,
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "location_name", required = false, defaultValue = "0") String location_name,
			@RequestParam(value = "location_type", required = false, defaultValue = "1") int location_type,
			HttpSession session, HttpServletRequest request)
			throws NumberFormatException, Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			// add inventory
			List<Map<String, Object>> data = buildParamsForInventory(con_id,
					qty, ps_id, location_name, location_type);
			String s = new JSONArray(data).toString();
			System.out.println(s);
			DBRow r = invent.copyTree(ps_id, data);
			// StringBuffer sb = new
			// StringBuffer(inventory_url).append("/BaseController/copyTree");
			// String messageFromSync =
			// httpclientForInventroyPost(sb.toString(), new
			// JSONObject(data).toString(), request.getSession(false).getId());
			if (r == null || r.get("ret", 0) != BCSKeyReceive.SUCCESS) {
				throw new ReceiveForAndroidException("Inventory exception!");
			}
			// Remove Inventory Log temporarily by zhengziqi at Aug.7th 2015
			addInventoryLogByBatch(data, session);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	public List<Map<String, Object>> buildParamsForInventory(String con_ids,
			int qty, long ps_id, String location_name, int location_type)
			throws NumberFormatException, Exception {
		List<String> containerIdList = Arrays.asList(con_ids.split(","));
		List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
		DBRow entryRow = floorReceiptsMgrZzq.queryEntryByConIds(con_ids);
		String[] seal_number = new String[2];
		seal_number[0] = entryRow.getString("seal_delivery");
		seal_number[1] = entryRow.getString("seal_pick_up");
		for (String con_id : containerIdList) {
			Map<String, Object> map = buildSingleInventory(con_id, qty, ps_id,
					location_name, location_type, seal_number);
			list.add(map);
		}
		return list;
	}

	public Map<String, Object> buildSingleInventory(String con_id, int qty,
			long ps_id, String location_name, int location_type,
			String[] seal_number) throws NumberFormatException, Exception {
		DBRow con_props = floorReceiptsMgrZzq.queryContainerInfoById(Long
				.valueOf(con_id));
		if (con_props == null) {
			throw new ReceiveForAndroidException("Pallets need close!");
		}
		if (!con_props.containsKey("type_id")) {
			con_props.add("type_id", 0);
		}
		int damage = con_props.get("is_damage", 1);
		con_props.add("damage", (damage == 0) ? 1 : damage);
		DBRow product_props = floorReceiptsMgrZzq
				.queryProductInfoByConatinerId(Long.valueOf(con_id));
		if (product_props == null) {
			throw new ReceiveForAndroidException("Product not found!");
		}
		product_props.add("union_flag", 0);
		// customer_id, title_id -> product_props
		// title_id -> locates_props
		if (!product_props.containsKey("product_line")) {
			product_props.add("product_line", 0);
		}
		DBRow rel_props = new DBRow();
		rel_props.add("quantity", (int) con_props.get("quantity", 0f));
		DBRow locates_props = new DBRow();
		locates_props.add("title_id", product_props.getString("title_id"));
		locates_props.add("time_number", System.currentTimeMillis());
		locates_props.add("lot_number", product_props.getString("lot_no"));
		locates_props.add("pc_id", product_props.getString("pc_id"));
		DBRow slc_props = new DBRow();
		long location_id = acquireLocationIdByLocationNameAndPsId(
				location_type, ps_id, location_name, false);
		if (location_id != 0) {
			slc_props.add("slc_id", location_id);
			slc_props.add("slc_type", location_type);
		} else {
			// 默认 1是ghost
			slc_props.add("slc_id", 1);
			slc_props.add("slc_type", 1);
		}

		// 去掉不需要的字段
		String[] unUsedConProps = { "is_damage", "location_id",
				"location_type", "lot_no", "pc_id", "quantity", "receipt_no",
				"title_id" };
		String[] unUsedProductProps = { "lot_no" };
		removeUnusedProps(con_props, unUsedConProps);
		removeUnusedProps(product_props, unUsedProductProps);

		DBRow map = new DBRow();
		map.add("slc_props", slc_props);
		map.add("con_props", con_props);
		map.add("product_props", product_props);
		map.add("rel_props", rel_props);
		map.add("locates_props", locates_props);
		// map.add("seal_number", seal_number);
		return map;
	}

	public void removeUnusedProps(DBRow row, String[] unUsedProps) {
		for (int i = 0; i < unUsedProps.length; i++) {
			if (row.containsKey(unUsedProps[i])) {
				row.remove(unUsedProps[i]);
			}
		}

	}

	public String httpclientForInventroyPost(String path, String params,
			String sessionID) {
		HttpClient client = new HttpClient();
		// 创建POST方法的实例
		PostMethod post = new PostMethod(path);
		post.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		post.setRequestHeader("Cookie", "JSESSIONID=" + sessionID);
		post.setRequestBody(params);
		// 使用系统提供的默认的恢复策略
		post.getParams().setParameter(HttpMethodParams.RETRY_HANDLER,
				new DefaultHttpMethodRetryHandler());
		try {
			// 执行getMethod
			int statusCode = client.executeMethod(post);
			if (statusCode != 200) {
				System.err.println("Method failed: " + post.getStatusLine());
			}
			// 读取内容
			InputStream is = post.getResponseBodyAsStream();
			ByteArrayOutputStream outStream = new ByteArrayOutputStream();
			byte[] data = new byte[4096];
			int count = -1;
			while ((count = is.read(data, 0, 4096)) != -1) {
				outStream.write(data, 0, count);
				data = null;
			}
			String jsonResult = new String(outStream.toByteArray(), "UTF-8");

			// 处理内容
			return jsonResult;
		} catch (HttpException e) {
			// 发生致命的异常，可能是协议不对或者返回的内容有问题
			System.out.println("Please check your provided http address!");
			e.printStackTrace();
		} catch (IOException e) {
			// 发生网络异常
			e.printStackTrace();
		} finally {
			// 释放连接
			post.releaseConnection();
		}
		return null;
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/createContainerConcernCLPConfig", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createContainerConcernCLPConfig(
			@RequestParam(value = "config_type", required = false, defaultValue = "0") int config_type, // 1:Input
																										// Qty
																										// Per
																										// Pallet;2:Total
																										// [Remain]
																										// Qty
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "detail_id", required = false, defaultValue = "0") long detail_id,
			@RequestParam(value = "item_id", required = false, defaultValue = "") String item_id,
			@RequestParam(value = "lot_no", required = false, defaultValue = "") String lot_no,
			@RequestParam(value = "length", required = false, defaultValue = "0") int length,
			@RequestParam(value = "width", required = false, defaultValue = "0") int width,
			@RequestParam(value = "height", required = false, defaultValue = "0") int height,
			@RequestParam(value = "remainder_length", required = false, defaultValue = "0") int remainder_length,
			@RequestParam(value = "remainder_width", required = false, defaultValue = "0") int remainder_width,
			@RequestParam(value = "remainder_height", required = false, defaultValue = "0") int remainder_height,
			@RequestParam(value = "pallet_type", required = false, defaultValue = "0") long pallet_type, // container_type主键
			@RequestParam(value = "location", required = false, defaultValue = "") String location,
			@RequestParam(value = "location_type", required = false, defaultValue = "2") int location_type,
			@RequestParam(value = "location_id", required = false, defaultValue = "0") long location_id,
			@RequestParam(value = "note", required = false, defaultValue = "") String note,
			@RequestParam(value = "pallet_qty", required = false, defaultValue = "0") int pallet_qty,
			@RequestParam(value = "is_scan_create", required = false, defaultValue = "0") int is_scan_create,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "goods_type", required = false, defaultValue = "0") int goods_type, // 0:normal;1:overage;2:damage
			@RequestParam(value = "isTask", required = false, defaultValue = "0") int isTask, // 0:else;1:CC
																								// Task
			@RequestParam(value = "is_check_location", required = false, defaultValue = "1") long is_check_location,
			@RequestParam(value = "is_match_clp_config", required = false, defaultValue = "0") long is_match_clp_config,
			@RequestParam(value = "license_plate_type_id", required = false, defaultValue = "0") int license_plate_type_id,
			@RequestParam(value = "is_return_plts", required = false, defaultValue = "0") int is_return_plts // 在count页面添加
	) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh
					.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt
					.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			if (config_type == 0 || receipt_line_id == 0 || "".equals(item_id)
					|| "".equals(lot_no) || adid == 0 || ps_id == 0
					|| isTask == 2) {
				throw new ReceiveForAndroidException("Parameter error");
			}
			boolean flag = true;
			// 在ccTask config pallet时不能超过received qty ||
			// receive的时候如果选择创建一个pallet 数量就是line的预计收货数量expected_qty
			DBRow line = floorReceiptsMgrWfh
					.findReceiptLineByLineId(receipt_line_id);
			DBRow sum_count = floorReceiptsMgrWfh.findSUMConfigByLineId(
					receipt_line_id, GOODS_TYPE_NORMAL); // 查找以前配置好的pallet数量
			DBRow receivedRow = floorReceiptsMgrWfh.findSUMReceivedQTY(
					receipt_line_id, GOODS_TYPE_ALL, false); // 查找以前count的
																// noromalqty的总数
			int total = line.get("expected_qty", 0); // receive时，config_type
														// 为2时数量就是预计收货数量expected_qty
			int normalQtySUM = receivedRow.get("normal_qty", 0);
			int config_total = sum_count.get("number", 0);
			int received_qty_now = length * width * height * pallet_qty;
			if (pallet_qty > 200) {
				throw new ReceiveForAndroidException("Create too many");
			}
			// 验证
			if (isTask == 1) { // 如果是在CCTask的时候重新配置托盘，则数量(L*W*H*palet_qty)不能超过received_qty
				if (received_qty_now + config_total > normalQtySUM) {
					flag = false;
					throw new ReceiveForAndroidException(
							"Quantity is greater than the received QTY configuration");
				}
				// if ("".equals(pallet_type)) {
				// flag = false;
				// throw new
				// ReceiveForAndroidException("Please select a pallet type");
				// }
			}
			Boolean is_check_location_staging = (location_type == 1) ? true
					: false;
			// WMS 没有location记录，询问是否仍然创建
			if (location_id == 0) {
				location_id = acquireLocationIdByLocationNameAndPsId(
						location_type, ps_id, location,
						is_check_location_staging);
				if (is_check_location == 1L
						&& !checkLocationIsExist(location,
								receipt.get("company_id", ""))) {
					result.add("is_check_location", 0);
					throw new ParamNeedConfirmException("Location " + location
							+ " not found, confirm to commit?");
				}
			}

			// 有匹配的Clp配置，询问是否创建clp托盘
			DBRow[] clpConfigRows = floorReceiptsMgrZzq
					.queryAvailableCLPConfig(receipt_line_id, length, width,
							height);
			if (is_match_clp_config == 1L && clpConfigRows != null
					&& clpConfigRows.length >= 1) {
				result.add("clpConfigRows", clpConfigRows);
				result.add("is_match_clp_config", 0);
				throw new ParamNeedConfirmException(
						"Match product configuration, confirm to create CLP?");
			}
			// 添加pallet 即某种类型的用几个 添加一次 如果为第三种方式 有余数了则按照余数创建一个托盘
			// int remainderQty = 0;
			if (flag) {
				StringBuffer log = new StringBuffer();
				if (goods_type == GOODS_TYPE_NORMAL)
					log.append("Create Pallet Config:|");
				else if (goods_type == GOODS_TYPE_DAMAGE) // 损坏的货物
					log.append("Create Pallet(Damage) Config:|");
				// 如果config_type 为1 代表输入几个就创建几个pallet 如果为2 则创建一个 L（总数） *1*1
				// 1:Input Qty Per Pallet;2:Total [Remain] Qty
				if (config_type == 2) {
					length = total - config_total;
					width = 1;
					height = 1;
					pallet_qty = 1;
				}
				log.append(length + "x" + width + "x" + height + "|");
				// if ("".equals(pallet_type)) {
				// log.append("per pallet: "
				// + (config_type == 1 ? pallet_qty : 1) + "|");
				// } else {
				log.append(pallet_type + ": "
						+ (config_type == 1 ? pallet_qty : 1) + "|");
				// }
				// 如果使用的是auto的配置则会将 configTotal % pallet_qty 重新创建一种配置
				if (remainder_length != 0) {
					log.append(remainder_length + "x1x1|");
					// if ("".equals(pallet_type)) {
					// log.append("per pallet: 1");
					// } else {
					log.append(pallet_type + ": 1");
					// }
				}

				// DateFormat df = new SimpleDateFormat("yyy-MM-dd hh:mm:ss");
				// df.setTimeZone("UTC");
				// // 添加日志
				DBRow taskLog = new DBRow();
				taskLog.add("operator_type", "Creat Pallet Config");
				taskLog.add("operator_id", adid);
				taskLog.add("operator_time", DateUtil.NowStr());
				taskLog.add("receipt_line_id", receipt_line_id);
				taskLog.add("receipt_line_status", line.get("status", 0));
				taskLog.add("data", log.substring(0, log.length() - 1));
				floorReceiptsMgrWfh.addTaskLog(taskLog);

				long cc_id = 0L;
				DBRow data = null;
				boolean createFlag = true;
				// edited by zhengziqi at 11th April, 2015
				// update plate_number in case of none configuration pallet
				int existConfigPalletNO = 0;
				int is_partial = 0;
				if (width * length * height == 0) {
					is_partial = 2; // 无配置类型托盘规格
				}
				// validate container_config exist
				// 先判断托盘类型是否是clp，如果是
				if (is_match_clp_config == 0L && license_plate_type_id > 0) {
					DBRow[] configRow = floorReceiptsMgrZzq
							.queryContainerConfigByOwnParams(receipt_line_id,
									pallet_type + "", license_plate_type_id, 0L);
					if (configRow != null && configRow.length == 1) {
						cc_id = configRow[0].get("container_config_id", 0L);
						// update by zhaoyy get("qty", 0)-》get("plate_number",
						// 0); 2015年6月1日 12:04:06
						existConfigPalletNO = configRow[0].get("plate_number",
								0);
					} else {
						// 添加 container_config
						DBRow containerConfigRow = null;
						// edited by zhengziqi at 11th April, 2015
						// update plate_number in case of none configuration
						// pallet
						containerConfigRow = new DBRow();
						containerConfigRow.add("receipt_line_id",
								receipt_line_id);
						containerConfigRow.add("item_id", item_id);
						containerConfigRow.add("lot_no", lot_no);
						containerConfigRow.add("length", length);
						containerConfigRow.add("width", width);
						containerConfigRow.add("height", height);
						containerConfigRow.add("pallet_type", pallet_type);
						containerConfigRow.add("note", note);
						containerConfigRow.add("create_user", adid);
						containerConfigRow
								.add("create_date", DateUtil.NowStr());
						containerConfigRow.add("goods_type", GOODS_TYPE_NORMAL); // 托盘的作用
						containerConfigRow.add("license_plate_type_id",
								license_plate_type_id);
						containerConfigRow.add("ship_to_id", 0L);
						cc_id = floorReceiptsMgrWfh
								.addContainerConfig(containerConfigRow);
					}
				} else {
					// 否则托盘类型是tlp，如下
					if (pallet_qty > 0) {
						DBRow[] configRow = floorReceiptsMgrZzq
								.queryContainerConfigByOwnParamsForTLP(
										receipt_line_id, null, length, width,
										height, is_partial);
						if (configRow != null && configRow.length == 1) {
							cc_id = configRow[0].get("container_config_id", 0L);
							// update by zhaoyy get("qty",
							// 0)-》get("plate_number", 0); 2015年6月1日 12:04:06
							existConfigPalletNO = configRow[0].get(
									"plate_number", 0);
						} else {
							data = new DBRow();
							data.add("config_type", config_type);
							data.add("receipt_line_id", receipt_line_id);
							data.add("item_id", item_id);
							data.add("lot_no", lot_no);
							data.add("length", length);
							data.add("width", width);
							data.add("height", height);
							if (is_partial > 0) {
								data.add("is_partial", is_partial);
							}
							data.add("pallet_type", pallet_type);
							data.add("note", note);
							data.add("create_user", adid);
							data.add("create_date", DateUtil.NowStr()); // 不知是否是utc时间？DateUtil.showUTCTime(DateUtil.NowStr(),
							// ps_id) 报错暂时不用
							data.add("goods_type", goods_type); // 托盘的作用
							// 添加 container_config
							cc_id = floorReceiptsMgrWfh
									.addContainerConfig(data);
						}
					}
				}

				// 如果是auto的方式需要多创建一种托盘配置，
				long remainder_cc_id = 0l;
				if (remainder_length != 0) { // 使用了auto 并且出现余数 则添加一种配置这种配置的规格就是
					data = new DBRow();
					data.add("length", remainder_length);
					data.add("width", 1);
					data.add("height", 1);
					data.add("is_partial", 1);
					data.add("receipt_line_id", receipt_line_id);
					data.add("item_id", item_id);
					data.add("lot_no", lot_no);
					long _pallet_type = floorReceiptsMgrZyy
							.getPalletFloorLoadId();
					data.add("pallet_type", _pallet_type);
					// data.add("pallet_type", pallet_type);
					data.add("note", note);
					data.add("create_user", adid);
					data.add("create_date", DateUtil.NowStr());
					data.add("goods_type", GOODS_TYPE_NORMAL); // 托盘的作用
					data.add("ship_to_id", 0L);
					remainder_cc_id = floorReceiptsMgrWfh
							.addContainerConfig(data);
				}
				// 添加container 和 receiptRelContainer
				// 如果是在scan的时候创建会在contianer_product表新建一条数量为0的数据
				if (detail_id == 0) {
					DBRow[] cons = floorReceiptsMgrWfh.findContainerByLineId(
							receipt_line_id, 3);
					for (DBRow con : cons) {
						if (con.get("detail_id", 0L) != 0) {
							detail_id = con.get("detail_id", 0L);
							break;
						}
					}
				}
				int process = (is_scan_create > 0) ? CreateContainerProcessKey.PROCESS_SCAN
						: CreateContainerProcessKey.PROCESS_COUNT;
				int cntainerType = ContainerTypeKey.TLP;
				if (license_plate_type_id > 0) {
					cntainerType = ContainerTypeKey.CLP;
				}

				String con_ids = "";
				con_ids += addContainerAndReceiptRelContainer(receipt_line_id,
						item_id, lot_no, location, location_type, location_id,
						pallet_qty, adid, line.get("company_id", ""), cc_id,
						ps_id, detail_id, is_scan_create, length * width
								* height, cntainerType, process,
						(license_plate_type_id > 0) ? license_plate_type_id
								: pallet_type);
				// 如果是auto的方式需要多创建一个托盘
				if (remainder_cc_id != 0) {
					con_ids += addContainerAndReceiptRelContainer(
							receipt_line_id, item_id, lot_no, location,
							location_type, location_id, 1, adid, line.get(
									"company_id", ""), remainder_cc_id, ps_id,
							detail_id, is_scan_create, length * width * height,
							ContainerTypeKey.TLP, process,
							(license_plate_type_id > 0) ? license_plate_type_id
									: pallet_type);
				}
				// 更新receipt_line
				con_ids = con_ids.substring(0, con_ids.length() - 1);
				DBRow[] containers = null;
				if (!con_ids.equals("")) {
					containers = floorReceiptsMgrWfh
							.findPalletByConIDS(con_ids);
				}
				if (is_return_plts == 1) {
					for (DBRow con : containers) {
						con.add("create_date",
								DateUtil.showLocalparseDateTo24Hours(
										con.get("create_date", ""), ps_id));
						con.add("wms_container_id", con.get("container_no", 0l));
						con.add("container_no",
								"(94)"
										+ CommonUtils.formatNumber(
												"#000000000000",
												con.get("container_no", 0l)));
					}
					result.add("containers", containers == null ? new DBRow[0]
							: containers);
				} else {
					// 返回container_config row
					List<DBRow> returnRow = new ArrayList<DBRow>();
					DBRow[] containerConfigRows = floorReceiptsMgrZzq
							.queryContainerConfigByOwnParams(receipt_line_id,
									"", 0L, 0L);
					for (DBRow row : containerConfigRows) {
						String ship_to_ids = row.get("ship_to_ids", "");
						String[] ids = ship_to_ids.split(",");
						row.add("license_plate_type_id",
								row.get("license_plate_type_id", 0));
						if (Arrays.asList(ids).contains("0")) {
							row.add("ship_to_names", "");
						} else {
							row.add("ship_to_names", floorReceiptsMgrZyy
									.getShipToNamesByShipToIds(ship_to_ids));
						}
						//
						if (row.containsKey("process")
								&& row.get("process", 0) == CreateContainerProcessKey.PROCESS_COUNT) {
							// 只有CC 是 才区分
							if (row.get("is_partial", 0) == 1) {
								// 如果是 is_partial 的。
								DBRow[] palletIds = floorReceiptsMgrZyy
										.getPartialConIds(row.get(
												"receipt_line_id", 0l),
												row.get("container_config_id",
														0l), row.get("process",
														0), row.get(
														"is_partial", 0));
								for (DBRow conId : palletIds) {
									DBRow _row = (DBRow) row.clone();
									_row.add("con_id", conId.get("con_id", ""));
									_row.put("plate_number", 1);
									_row.add("keep_inbound", 0);
									returnRow.add(_row);
								}

							} else {
								// 不是 is_partial
								returnRow.add(row);
							}
						}
					}

					result.add("container_config",
							returnRow.toArray(new DBRow[0]));
				}
				// update movements
				// added by zhengziqi at May.5th 2015
				// List<String> containerList =
				// Arrays.asList(con_ids.split(","));
				for (DBRow row : containers) {
					updateMysqlMovements(row.get("con_id", 0l), adid,
							receipt.get("company_id", ""), location_id,
							location, location_type);
				}
				result.add("success", "true");
			}
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (ParamNeedConfirmException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/keepInbound", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String keepInboundPalletConfig(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "config_id", required = false, defaultValue = "0") long config_id,
			@RequestParam(value = "keep_inbound", required = false, defaultValue = "0") int keep_inbound,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {

			if (receipt_line_id == 0 || config_id == 0) {
				throw new ReceiveForAndroidException("Parameter error");
			}

			if (keep_inbound == 1) {
				// TODO 验证数据信息 查询所有CC 是的数量
				DBRow receivedByCount = floorReceiptsMgrWfh.findSUMReceivedQTY(
						receipt_line_id, GOODS_TYPE_NORMAL, false,
						CreateContainerProcessKey.PROCESS_COUNT); // 查找以前count的
																	// noromalqty的总数
				int countQTY = receivedByCount.get("normal_qty", 0);
				int ccQTY = floorReceiptsMgrZyy
						.getCCPalletQtyByLineId(receipt_line_id);
				int configQTY = floorReceiptsMgrZyy
						.getCountQTYByLineIdAndConfigId(receipt_line_id,
								config_id);
				// 查询 keep_inboud 的 配置的托盘数量
				// TODO 获取已经锁定的数量
				int inboundQTY = floorReceiptsMgrZyy
						.getInboundQTYByLineId(receipt_line_id);
				if (countQTY - ccQTY - inboundQTY < configQTY) {
					throw new ReceiveForAndroidException(
							"CC QTY overflow, not allowed keep inbound");
				}

			}

			floorReceiptsMgrZyy.updatePalletConfig(receipt_line_id, config_id,
					keep_inbound);

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/getCompanyByPsId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String getCompanyByPsId(
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid)
			throws Exception {

		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {

			if (ps_id == 0l) {
				throw new ReceiveForAndroidException("Parameter error");
			}

			DBRow[] rows = floorReceiptsMgrZyy.getCompanyByPsId(ps_id);
			result.add("data", rows);

		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	public void setFloorReceiptSqlServerMgrSbb(
			FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb) {
		this.floorReceiptSqlServerMgrSbb = floorReceiptSqlServerMgrSbb;
	}

	public void setFloorReceiptsMgrWfh(FloorReceiptsMgrWfh floorReceiptsMgrWfh) {
		this.floorReceiptsMgrWfh = floorReceiptsMgrWfh;
	}

	public void setFloorReceiptMgr(FloorReceiptMgrSbb floorReceiptMgr) {
		this.floorReceiptMgr = floorReceiptMgr;
	}

	public void setFloorLogMgr(FloorLogMgrIFace floorLogMgr) {
		this.floorLogMgr = floorLogMgr;
	}

	public void setFloorReceiptsMgrZzq(FloorReceiptsMgrZzq floorReceiptsMgrZzq) {
		this.floorReceiptsMgrZzq = floorReceiptsMgrZzq;
	}
}
