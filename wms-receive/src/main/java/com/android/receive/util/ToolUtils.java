package com.android.receive.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
@RestController
public class ToolUtils {
	@Autowired
	private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
	
    public static final String  defaultDateFormat ="yyyy-MM-dd";
    public static final  String defaultDateTimeFormat = "yyyy-MM-dd HH:mm:ss";
    public static final String  defaultDateTimeFormatDHL = "yyyy-MM-ddTHH:mm:ss";
    public static final String  defaultDate24Formate = "MM/dd/yy HH:mm";
    private static final SimpleDateFormat defaultFormage = new SimpleDateFormat(defaultDateTimeFormat);

	public  Date createDateTime(String time)
			throws Exception
		{
			 

			return formateDate(time);
		}
	public  Date formateDate(String d) throws Exception{
        Pattern dateFormats = Pattern.compile( "(\\d\\d\\d\\d-\\d\\d-\\d\\d|\\d\\d/\\d\\d/\\d\\d\\d\\d)( \\d\\d:\\d\\d:\\d\\d)?" );

        Matcher m = dateFormats.matcher(d);
        if(!m.find()) {
        	throw new Exception("date Error"+d);
         }
        Date date = null;
        if(m.groupCount() == 2){
            //with time part
            if(m.group(2)!=null)
                date = new SimpleDateFormat( m.group(1).indexOf("-") > 0 ? "yyyy-MM-dd HH:mm:ss"  : "MM/dd/yyyy HH:mm:ss").parse(d);
            else 
                date = new SimpleDateFormat( m.group(1).indexOf("-") > 0 ? "yyyy-MM-dd"  : "MM/dd/yyyy").parse(d);
        }
        if(date == null){
        	throw new Exception("date Error"+d);
        }
        return date ;
    }
	 
	  public long timeToLong(String t) {
		    try {
		      return defaultFormage.parse(t).getTime();
		    }
		    catch (ParseException e) {
		      e.printStackTrace();
		    }
		    return new Date().getTime();
	  }
	  
}
