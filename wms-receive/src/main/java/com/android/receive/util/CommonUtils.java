/**
 * 
 */
package com.android.receive.util;

import java.util.ArrayList;
import java.util.List;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.db.DBRow;

/**
 * @author Zhengziqi 2015年3月4日
 *
 */
public class CommonUtils {

	/**
	 * 把DBRow[] 或者是 DBRow 转换成JSON.toString();
	 * 
	 * @param data
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public static String convertDBRowsToJsonString(DBRow... data)
			throws Exception {
		String value = "";
		if (data != null && data.length == 1) {
			DBRow dbrowData = (DBRow) data[0];
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData
					.getFieldNames();
			for (String fileName : filedNames) {
				Object inner = dbrowData.getValue(fileName);

				if (inner instanceof DBRow[]) {
					json.put(fileName.toLowerCase(), getJSON(inner));
				} else if (inner instanceof DBRow) {
					json.put(fileName.toLowerCase(), getJSON(inner));
				} else {

					json.put(fileName.toLowerCase(), inner);
				}

			}
			value = json.toString();
		}
		if (data != null && data.length > 1) {
			DBRow[] dbrows = data;
			JSONArray array = new JSONArray();
			for (DBRow temp : dbrows) {
				array.put(getJSON(temp));
			}
			value = array.toString();
		}

		return value;
	}

	@SuppressWarnings("unchecked")
	public static Object getJSON(Object data) throws Exception {
		Object obj = new Object();
		if (data instanceof DBRow[]) {
			DBRow[] datas = (DBRow[]) data;
			JSONArray jas = new JSONArray();
			for (DBRow temp : datas) {
				jas.put(getJSON(temp));
			}
			obj = jas;
		} else if (data instanceof DBRow) {
			DBRow dbrowData = (DBRow) data;
			JSONObject json = new JSONObject();
			ArrayList<String> filedNames = (ArrayList<String>) dbrowData
					.getFieldNames();
			for (String fileName : filedNames) {
				json.put(fileName.toLowerCase(),
						getJSON(dbrowData.getValue(fileName)));
			}
			obj = json;
		} else {
			obj = data;
		}
		return obj;
	}
	
	public static String listToString(List<String> list){
		StringBuffer sb = new StringBuffer();
		for(String s: list){
			sb.append(s).append(",");
		}
		if(sb.length()>0){
			return sb.substring(0, sb.lastIndexOf(","));
		}else{
			return "";
		}
	}
	
	/**
	 * 格式化字符串
	 * @param formatstr
	 * @param number
	 * @return
	 */
	public static String formatNumber(String formatstr,double number)
	{
		java.text.DecimalFormat df = new java.text.DecimalFormat(formatstr);
		return(df.format(number));
	}
	
	/**
     * 把string转换成long
     * @param strName
     * @return
     * @throws Exception
     * @throws NumberFormatException
     */
    public static final long getLong(String strName)
    	throws Exception,NumberFormatException
    {
    	if ( strName==null )
    	{
    		throw new Exception("getLong(String strName):Input value is NULL!");
    	}
    	else
    	{
    		try
			{
				return(Long.parseLong(strName.trim()));
			} 
    		catch (NumberFormatException e) 
			{
    			return(0);
			}
    	}
    }
}
