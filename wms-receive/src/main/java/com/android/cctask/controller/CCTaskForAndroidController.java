package com.android.cctask.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

import com.android.receive.controller.ReceiptForAndroidController;
import com.android.receive.util.CommonUtils;
import com.cwc.app.api.iface.zyy.InventoryMgrIfaceZyy;
import com.cwc.app.api.iface.zyy.ScheduleNoticesMgrIfaceZyy;
import com.cwc.app.api.iface.zyy.WritebackToWmsMgrInfaceZyy;
import com.cwc.app.exception.receive.ReceiveForAndroidException;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.app.floor.api.sbb.FloorReceiptMgrSbb;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.floor.api.wfh.FloorReceiptsMgrWfh;
import com.cwc.app.floor.api.zj.FloorShipToMgrZJ;
import com.cwc.app.floor.api.zl.FloorContainerMgrZYZ;
import com.cwc.app.floor.api.zr.FloorBoxTypeZr;
import com.cwc.app.floor.api.zyy.FloorReceiptsMgrZyy;
import com.cwc.app.floor.api.zzq.FloorReceiptsMgrZzq;
import com.cwc.app.invent.BaseController;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.app.key.ContainerStatueKey;
import com.cwc.app.key.CreateContainerProcessKey;
import com.cwc.app.key.GoodsTypeKeys;
import com.cwc.app.key.LineStatueKey;
import com.cwc.app.key.ScheduleFinishKey;
import com.cwc.app.key.YesOrNotKey;
import com.cwc.app.util.Config;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.exception.OutScheduleException;

/**
 * @author 	
 * 2015年3月4日
 *
 */
@Controller
@RestController
public class CCTaskForAndroidController {
	
	public static final int LETTER = 1 ;					//1表示letter
	public static final int LABEL = 0 ; 					//0表示label  
	public static final int VAILDATIONERROR = 90;
	public static final int CCTASK = 26 ;  					//CCTask 模块
	public static final int CCTASK_SUPERVISOR	=  62 ;		//CCTask_supervisor 
	public static final int CCTASK_SCANER 	=  63 ;			//CCTask_scan 
	
	@Value("${notice.url}")
	private String noticeURL;
	
	@Autowired
	private FloorReceiptsMgrWfh floorReceiptsMgrWfh;
	@Autowired
	private FloorReceiptsMgrZzq floorReceiptsMgrZzq;
	
	@Autowired
	private FloorReceiptsMgrZyy floorReceiptsMgrZyy;
	
	@Autowired
	private FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgrSbb;
	
	
	@Autowired
	private FloorBoxTypeZr floorBoxTypeZr;

	@Autowired
	private FloorShipToMgrZJ floorShipToMgrZJ;

	
	@Autowired
	private FloorReceiptMgrSbb floorReceiptMgr;
	
	@Autowired
	private FloorLogMgrIFace floorLogMgr;
	
	@Autowired
	private BaseController invent;
	
	@Autowired
	private FloorContainerMgrZYZ floorContainerMgrZYZ;
	
	@Autowired
	private WritebackToWmsMgrInfaceZyy writebackToWmsMgrZyy;
	
	@Autowired
	private ScheduleNoticesMgrIfaceZyy scheduleNoticesMgrZyy;
	
	/**
	 *
	 * acquire Container_Configs by Product
	 * 2015年5月14日
	 * @param receipt_line_id
	 * @param adid
	 * @param ship_to
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/acquireProductContainerConfigs", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireProductContainerConfigs(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "ship_to", required = false, defaultValue = "0") int ship_to,
			HttpSession session) throws Exception {
		
		//TODO finishCount 
		
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try {
			DBRow line = floorReceiptsMgrWfh.findReceiptLineAndTitleByLineId(receipt_line_id);
			DBRow[] configRows = floorBoxTypeZr.getBoxTypesInfo(line.get("pc_id", 0L), 0, line.get("title_id", 0), 0, line.get("customer_key", 0), YesOrNotKey.YES);
			for (int i = 0; i < configRows.length; i++) 
			{
				DBRow row = floorReceiptsMgrZzq.queryLpTitleAndShipToGroupShipTo(configRows[i].get("lpt_id", 0L));
				configRows[i].add("ship_to_ids", null == row ? "0" : row.getString("ship_to_ids"));
				configRows[i].add("ship_to_names", floorReceiptsMgrZyy.getShipToNamesByShipToIds(configRows[i].get("ship_to_ids","")));
			}

			if( configRows == null || configRows.length == 0){
				throw new ReceiveForAndroidException("No product config exists!");
			}
			DBRow data = new DBRow();
			data.add("configs", configRows);
			result.add("data", data);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());	
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	@Transactional
	@RequestMapping(value = "/android/acquireProductShipTo", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireProductShipTo(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow receipt = floorReceiptsMgrWfh.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			
			DBRow line = floorReceiptsMgrWfh.findReceiptLineAndTitleByLineId(receipt_line_id);
			if(line.get("pc_id", 0L) < 1){
				throw new ReceiveForAndroidException(" Product not found!");
			}
			DBRow[] shipToRows = floorReceiptsMgrZzq.queryShipToByProductId(line.get("pc_id", 0L), line.get("title_id", 0), line.get("customer_key", 0), ps_id);
			
			result.add("data", shipToRows);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());	
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 *
	 * acquire clp config info by line_id    
	 * 2015年5月14日
	 * @param receipt_line_id
	 * @param adid
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/acquirePalletConfigsByLine", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquirePalletConfigsByLine(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "need_pc_info", required = false, defaultValue = "0") long need_pc_info,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			//开始任务
			DBRow schedule = floorReceiptsMgrZyy.getScheduleByReceipt(receipt_line_id, CCTASK, CCTASK_SUPERVISOR);
			if(schedule==null){
//				throw new ReceiveForAndroidException(" Schedule not found!");
			}else if(schedule.get("start_time", "").equals("")){
				DBRow upRow = new DBRow();
				upRow.add("start_time", DateUtil.NowStr());
				floorReceiptsMgrZyy.modifySchedule(schedule.get("schedule_id", 0l), upRow);
			}
			DBRow receipt = floorReceiptsMgrWfh.findReceiptByLineId(receipt_line_id);
			DBRow storage = floorReceiptsMgrWfh.findPSIDByCompanyId(receipt.get("company_id", ""));
			long ps_id = storage.get("ps_id", 0L);
			//获取tlp和clp信息  
			//tlp 改成 pallets ，clp  改成 pallets_cc
			DBRow[] containerConfigRows = floorReceiptsMgrZzq.queryContainerConfigByOwnParams(receipt_line_id, "", 0L, 0L);
			if( containerConfigRows == null || containerConfigRows.length == 0){
				throw new ReceiveForAndroidException("No container config exists!");
			}
			JSONObject data = new JSONObject();
			JSONArray tlp = new JSONArray();
			JSONArray clp = new JSONArray();
			JSONArray pltD = new JSONArray();
			for(DBRow row: containerConfigRows){
				String  ship_to_ids  = row.get("ship_to_ids", "");
				String[] ids = ship_to_ids.split(",");
				row.add("license_plate_type_id", row.get("license_plate_type_id",0));
				if(Arrays.asList(ids).contains("0")){
					row.add("ship_to_names", "");
				}else{
					row.add("ship_to_names", floorReceiptsMgrZyy.getShipToNamesByShipToIds(ship_to_ids));
				}
				//
				if(row.containsKey("process") && row.get("process", 0)==CreateContainerProcessKey.PROCESS_CC){
					// 只有CC 是 才区分
					if(row.get("is_partial", 0)==1){
						//如果是 is_partial 的。
						DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
						for(DBRow conId :palletIds){
							DBRow _row = (DBRow)row.clone();
							_row.add("con_id", conId.get("con_id",""));
							_row.put("plate_number", 1);
							_row.add("keep_inbound", 0);
							clp.put(_row);
						}
						
					}else{
						// 不是 is_partial
						clp.put(row);
					}
				}else{
					if(row.get("goods_type", 0)==GoodsTypeKeys.TYPE_DAMAGE){
						pltD.put(row);
					}
//					else{
//						if(row.get("is_partial", 0)==1){
//							//如果是 is_partial 的。
//							DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
//							for(DBRow conId :palletIds){
//								DBRow _row = (DBRow)row.clone();
//								_row.add("con_id", conId.get("con_id",""));
//								_row.put("plate_number", 1);
//								tlp.put(_row);
//							}
//							
//						}else{
//							// 不是 is_partial
//							tlp.put(row);
//						}
//					}
					
				}
//				if(row.containsKey("license_plate_type_id") && row.get("license_plate_type_id", 0L) > 0){
//					clp.put(row);
//				}else{
//					tlp.put(row);
//				}
			}
			
			DBRow[] inboundConfig = floorReceiptsMgrZyy.getInboundConfig(receipt_line_id);
			if(null!=inboundConfig && inboundConfig.length>0){
				for(DBRow row :inboundConfig){
					if(row.get("goods_type", 0)==GoodsTypeKeys.TYPE_DAMAGE){
						pltD.put(row);
					}else{
						if(row.get("is_partial", 0)==1){
							//如果是 is_partial 的。
							DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
							for(DBRow conId :palletIds){
								DBRow _row = (DBRow)row.clone();
								_row.add("con_id", conId.get("con_id",""));
								_row.put("plate_number", 1);
								tlp.put(_row);
							}
							
						}else{
							// 不是 is_partial
							tlp.put(row);
						}
					}
					
				}
			}
			data.put("pallets", tlp);
			data.put("pallets_cc", clp);
			data.put("pallets_d", pltD);
			
			if(need_pc_info == 1){
				DBRow line = floorReceiptsMgrWfh.findReceiptLineAndTitleByLineId(receipt_line_id);
				DBRow[] shipToRows = null;
				DBRow[] configRows = null;
				if(line.get("pc_id", 0L) >0){
					shipToRows = floorShipToMgrZJ.getAllShipTo();
					configRows = floorBoxTypeZr.getBoxTypesInfo(line.get("pc_id", 0L), 0, line.get("title_id", 0), 0, line.get("customer_key", 0), YesOrNotKey.YES);
					for (int i = 0; i < configRows.length; i++) {
						DBRow row = floorReceiptsMgrZzq.queryLpTitleAndShipToGroupShipTo(configRows[i].get("lpt_id", 0L));
						configRows[i].add("ship_to_ids", null == row ? "0" : row.getString("ship_to_ids"));
						//TODO 
						configRows[i].add("ship_to_names", floorReceiptsMgrZyy.getShipToNamesByShipToIds(configRows[i].get("ship_to_ids","")));
					}
				}
				data.put("configRows", configRows);
				data.put("shipToRows", shipToRows);
			}
			
			result.add("data", data);
		} catch (ReceiveForAndroidException e) {
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());	
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 *
	 * assign cc task to supervisor; assign scan task to labor 2015年5月11日
	 * 
	 * @param receipt_id 
	 * @param operator 指定给操作人
	 * @param adid 用户ID
	 * @param ps_id Product ID
	 * @param task_type 任务类型 61 task to supervisor ,62 assign scan task to labor
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/createCCAndScanTask", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createCCAndScanTask(
			@RequestParam(value = "receipt_line_id", required = false, defaultValue = "0") long receipt_line_id,
			@RequestParam(value = "operator", required = false, defaultValue = "") String operator,
			@RequestParam(value = "adid", required = false, defaultValue = "0") long adid,
			@RequestParam(value = "ps_id", required = false, defaultValue = "0") long ps_id,
			@RequestParam(value = "task_type", required = false, defaultValue = "62") int task_type,
			HttpSession session) throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		DBRow finishResult = writebackToWmsMgrZyy.finishCount(receipt_line_id, adid, session);
		if(finishResult.get("ret", 0)!= BCSKeyReceive.SUCCESS || finishResult.get("err", 0)!=BCSKeyReceive.FAIL){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("ret", ret);
			result.add("err", err);
			result.add("data", "finishCount error");	
			return CommonUtils.convertDBRowsToJsonString(result);
		}
		try {
			if (ps_id == 0 || adid == 0 || receipt_line_id == 0 || "".equals(operator)) {
				result.add("ret", BCSKeyReceive.FAIL);
				result.add("err", BCSKeyReceive.SYSTEMERROR);
				result.add("data", "Parameter error");
				return CommonUtils.convertDBRowsToJsonString(result);
			}
			DBRow line  = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
			if (null == line ) {
				result.add("data", "Not receipt line data");
				result.add("ret", BCSKeyReceive.FAIL);
				result.add("err", VAILDATIONERROR);
				return CommonUtils.convertDBRowsToJsonString(result);
			}
			switch (task_type) {
			case CCTASK_SUPERVISOR:
				result = assignCcTaskToSupervisor(result, receipt_line_id, operator, adid, ps_id,line.get("receipt_id",0l),session);
				break;
			case CCTASK_SCANER:
				result = assignScanTaskToLabor(result, receipt_line_id, operator, adid, ps_id, line.get("receipt_id",0l),session);
				break;
			default:
				result.add("data", "Task type error!!!");
				ret = BCSKeyReceive.FAIL;
				err = VAILDATIONERROR;
				result.add("ret", ret);
				result.add("err", err);
				break;
			}
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 * 创建扫描任务
	 * @param result DBRow 返回值  
	 * @param receipt_line_id
	 * @param operator
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	private DBRow assignScanTaskToLabor(DBRow result, long receipt_line_id,
			String operator, long adid, long ps_id,long receipt_id,HttpSession session) throws Exception {
		//TODO counted 完成 后 给 Labor 扫描
		try{
			DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK,
					CCTASK_SCANER, receipt_line_id);
			DBRow scheduleRow  = null;
			StringBuffer log = new StringBuffer(); // 记录日志
			
			if(null!=schedule && schedule.length>0){
				//更新对应的人即可 
				boolean isClose = false;
//				//有对应的任务，不能在分配任务啦。
//				result.add("data", "Please don't repeat the assign task");
//				result.add("ret", BCSKeyReceive.FAIL);
//				result.add("err", VAILDATIONERROR);
				scheduleRow = schedule[0];
				long scheduleID = scheduleRow.get("schedule_id", 0l);
//				DBRow[] operatorRow = floorReceiptsMgrZyy.getScheduleOperatorsByTaskId(scheduleID);
				floorReceiptsMgrZyy.delScheduleOperatorsByTaskId(scheduleID);
				DBRow updateRow =null;
				if(scheduleRow.get("schedule_state", 0)==10){
					updateRow = new DBRow();
					updateRow.add("schedule_state", 0);
					//更改
					floorReceiptsMgrZyy.modifySchedule(scheduleID, updateRow);
					isClose  =true;
				}
				//删除已经分配的人 
				String[] scans = operator.split(",");
				 
				if(scans.length>0)
					log.append("Repeat assign To:|");
				DBRow sbRow ;
				for (String s : scans) {
					sbRow = new DBRow();
					sbRow.add("schedule_id", scheduleID);
					sbRow.add("schedule_execute_id", s);
					sbRow.add("schedule_state", 0);
					sbRow.add("is_task_finish", 0);
					floorReceiptsMgrWfh.addScheduleSub(sbRow);
					DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long.parseLong(s));
					log.append(admin.get("employe_name", "")).append(",");
				}
				result.add("ret", BCSKeyReceive.SUCCESS);
				result.add("err", BCSKeyReceive.FAIL);
				result.add("data",scheduleRow);
				//更新改任务的人
				return result;
			}else{
				
				//创建scan 任务
				String  schedule_detail = "Create ScanTask: "+receipt_line_id;
				String  schedule_overview = "ScanTask: "+receipt_line_id+" can begin";
				scheduleRow = createSchedule(adid, receipt_line_id, CCTASK, CCTASK_SCANER, schedule_detail, schedule_overview,receipt_id);
				long scheduleID = scheduleRow.get("schedule_id",0l);
				
				//添加schedule_sub
				String[] scan = operator.split(",");
				if(scan.length>0)
					log.append("Assign To:|");
				DBRow sbRow ;
				for (String s : scan) {
					sbRow = new DBRow();
					sbRow.add("schedule_id", scheduleID);
					sbRow.add("schedule_execute_id", s);
					sbRow.add("schedule_state", 0);
					sbRow.add("is_task_finish", 0);
					floorReceiptsMgrWfh.addScheduleSub(sbRow);
					DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long.parseLong(s));
					log.append(admin.get("employe_name", "")).append(",");
				}
				//TODO 修改line 的状态  
				DBRow upRow = new DBRow();
				upRow.add("status", LineStatueKey.STATUS_SCAN);
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
				//写入日志
//				DBRow lines = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
//				for(DBRow line:lines){
//					long receipt_line_id =line.get("receipt_line_id", 0L); 
					
//				}
				
				result.add("ret", BCSKeyReceive.SUCCESS);
				result.add("err", BCSKeyReceive.FAIL);
				result.add("data",scheduleRow);
//				result.add("success",scheduleNoticesMgrZyy.noticeToOperator(adid,operator,scheduleRow,noticeURL,session));
			}
			DBRow taskLog = new DBRow();
			taskLog.add("operator_type", "Create Task  ");
			taskLog.add("operator_id", adid);
			taskLog.add("operator_time", DateUtil.NowStr());
			taskLog.add("receipt_line_id", receipt_line_id);
			taskLog.add("receipt_line_status", 2);
			taskLog.add("data", log.toString());
			floorReceiptsMgrWfh.addTaskLog(taskLog);
			result.add("success",noticeToOperator(adid,operator,scheduleRow,noticeURL,session));
			
			return result;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	} 
	
	/**
	 * 创建扫描任务,删除已有扫描任务
	 * @param result DBRow 返回值  
	 * @param receipt_line_id
	 * @param operator
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 */
	private DBRow updateScanTaskToLabor(DBRow result, long receipt_line_id,
			String operator, long adid, long ps_id,long receipt_id,HttpSession session) throws Exception {
		//TODO counted 完成 后 给 Labor 扫描
		try{
			DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK,
					CCTASK_SCANER, receipt_line_id);
			if(null!=schedule && schedule.length == 1){
				DBRow scheduleRow = schedule[0];
				DBRow upRow = new DBRow();
				upRow.add("start_time", DateUtil.NowStr());
				upRow.add("assign_user_id", operator);
				floorReceiptsMgrZyy.modifySchedule(scheduleRow.get("schedule_id", 0l), upRow);
				
				upRow = new DBRow();
				upRow.add("schedule_execute_id", operator);
				floorReceiptsMgrZyy.modifyScheduleSub(scheduleRow.get("schedule_id", 0l), upRow);
			}else{
				throw new ReceiveForAndroidException("Schedule error!");
			}
			StringBuffer log = new StringBuffer(); // 记录日志
			
			//TODO 修改line 的状态  
			DBRow upRow = new DBRow();
			upRow.add("status", LineStatueKey.STATUS_SCAN);
			floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
			//写入日志
//			DBRow lines = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
//			for(DBRow line:lines){
//				long receipt_line_id =line.get("receipt_line_id", 0L); 
				
//			}
			DBRow taskLog = new DBRow();
			taskLog.add("operator_type", "Create Task  ");
			taskLog.add("operator_id", adid);
			taskLog.add("operator_time", DateUtil.NowStr());
			taskLog.add("receipt_line_id", receipt_line_id);
			taskLog.add("receipt_line_status", 2);
			taskLog.add("data", log.toString());
			floorReceiptsMgrWfh.addTaskLog(taskLog);
			
			result.add("ret", BCSKeyReceive.SUCCESS);
			result.add("err", BCSKeyReceive.FAIL);
			result.add("data",schedule[0]);
//			result.add("success",scheduleNoticesMgrZyy.noticeToOperator(adid,operator,scheduleRow,noticeURL,session));
			result.add("success",noticeToOperator(adid,operator,schedule[0],noticeURL,session));
			return result;
		}catch(Exception e){
			throw new RuntimeException(e);
		}
	} 

	/**
	 * 分配CC Task 给 Supervisor
	 * @param result
	 * @param receipt_line_id
	 * @param operator
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @param receipt_id
	 * @return
	 * @throws Exception
	 */
	private DBRow assignCcTaskToSupervisor(DBRow result, long receipt_line_id,
		String operator, long adid, long ps_id, long receipt_id,HttpSession session) throws Exception {
		//查询是否 有任务
		DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK, CCTASK_SUPERVISOR, receipt_line_id);
		
		//有对应的任务，不能在分配任务
		if(null!=schedule && schedule.length>0){
			throw new ReceiveForAndroidException("Please don't repeat the assign task");
		}
		StringBuffer log = new StringBuffer(); // 记录日志
		//TODO 数量验证
		DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
		//TODO 设置任务
		String  schedule_detail = "Create CCTask: "+line.get("receipt_no", "");
		String  schedule_overview = "CCTask: " + line.get("receipt_no", "") + " Need Assign";
		
		DBRow scheduleRow = createSchedule(adid, receipt_line_id, CCTASK, CCTASK_SUPERVISOR, schedule_detail, schedule_overview,receipt_id);
		long scheduleID = scheduleRow.get("schedule_id",0l);
		
		//TODO 添加子任务  schedule_sub
		String[] supurvisor = operator.split(",");
		log.append("Create CCTask 【 RECEIPTS").append(receipt_line_id).append("】");
		
		log.append("Assign to:");
		DBRow sbRow ;
		for (String s : supurvisor) {
			sbRow = new DBRow();
			sbRow.add("schedule_id", scheduleID);
			sbRow.add("schedule_execute_id", s);
			sbRow.add("schedule_state", 0);
			sbRow.add("is_task_finish", 0);
			floorReceiptsMgrWfh.addScheduleSub(sbRow);
			DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long.parseLong(s));
			log.append(admin.get("employe_name", "")).append(",");
		}
		//TODO 修改line 的状态  
		DBRow upRow = new DBRow();
		upRow.add("status", LineStatueKey.STATUS_TO_CC);
//			upRow.add("unloading_finish_time", DateUtil.NowStr());
		floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
		//TODO 添加日志
//			DBRow[] lines = floorReceiptsMgrWfh.findReceiptLineByReceiptId(receipt_id);
//			for(DBRow line:lines){
//				long receipt_line_id =line.get("receipt_line_id", 0L); 
			
//			}
		
		DBRow taskLog = new DBRow();
		taskLog.add("operator_type", "Create CcTask To Supervisor");
		taskLog.add("operator_id", adid);
		taskLog.add("operator_time", DateUtil.NowStr());
		taskLog.add("receipt_line_id", receipt_line_id);
		taskLog.add("receipt_line_status", 2);
		taskLog.add("data", log.toString());
		floorReceiptsMgrWfh.addTaskLog(taskLog);
		
		result.add("ret", BCSKeyReceive.SUCCESS);
		result.add("err", BCSKeyReceive.FAIL);
		result.add("data", scheduleRow);
		result.add("success",scheduleNoticesMgrZyy.noticeToOperator(adid,operator,scheduleRow,noticeURL,session));
		return result;
	}

	
	/**
	 * 创建主任务
	 * @param adid
	 * @param receipt_id
	 * @param associate_type
	 * @param associate_process
	 * @param schedule_detail
	 * @param schedule_overview
	 * @return
	 * @throws Exception
	 */
	private DBRow  createSchedule(long adid,long receipt_line_id,int  associate_type,int associate_process,String schedule_detail,String schedule_overview,long receipt_id ) throws Exception{
		DBRow scheduleRow = new DBRow();
		scheduleRow.add("assign_user_id", adid);
		scheduleRow.add("schedule_detail", schedule_detail);// "Create CCTask: "+receipt_id
		scheduleRow.add("schedule_state", 0F);
		scheduleRow.add("schedule_is_note", 1);
		scheduleRow.add("is_need_replay", 1);
		scheduleRow.add("is_update", 0);
		scheduleRow.add("is_all_day", 0);
		scheduleRow.add("is_schedule", 1);
		scheduleRow.add("schedule_overview", schedule_overview);// "CCTask: " + receipt_id +" Need Assign");
		scheduleRow.add("associate_type", associate_type); // CCTASK  
		scheduleRow.add("associate_process", associate_process); // CCTASK_SUPERVISOR
		scheduleRow.add("associate_id", receipt_line_id);
		scheduleRow.add("sms_short_notify", 1);
		scheduleRow.add("sms_email_notify", 1);
		scheduleRow.add("is_task", 0);
		scheduleRow.add("associate_main_id", receipt_id);
		scheduleRow.add("create_time", DateUtil.NowStr());
		long scheduleID = floorReceiptsMgrWfh.addSchedule(scheduleRow);
		scheduleRow.add("schedule_id", scheduleID);
		return scheduleRow;
	}
	
//	/**
//	 * 通知任务给 操作员
//	 * @param adid
//	 * @param operator
//	 * @param schedule
//	 * @param session
//	 * @return
//	 * @throws Exception
//	 */
//	private boolean noticeToOperator(long adid,String operator,DBRow schedule,HttpSession session) throws Exception{
//		//发送通知访问sync10
//		String sessionID =session.getId();
//		HttpClient client = new HttpClient();
//		PostMethod post = new PostMethod(noticeURL);
////		client.getHostConfiguration().setHost("http://192.168.1.15", 8888);
//		post.setRequestHeader("Cookie", "JSESSIONID="+sessionID);
//		post.addParameter("schedule", CommonUtils.convertDBRowsToJsonString(schedule));
//		post.addParameter("assignTo", operator);
//		post.addParameter("adid", String.valueOf(adid));
//		int statue = client.executeMethod(post);
//		if(statue==200){
//			return true;
//		}else{
//			throw new Exception();
//		}
//	}
	
	/**
	 *
	 * acquire receipt_line info by task id
	 * 2015年5月11日
	 * @param line_id
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/acquireReceiptLinesByLineId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireReceiptLinesByLineId(
			@RequestParam(value = "line_id",required = false,defaultValue="0") long line_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		if (adid == 0 || line_id == 0) {
			result.add("ret", BCSKeyReceive.FAIL);
			result.add("err", BCSKeyReceive.SYSTEMERROR);
			result.add("data", "Parameter error");
			return CommonUtils.convertDBRowsToJsonString(result);
		}
		//TODO 根据task_id 和adid 获取信息
		try{
//			if(!floorReceiptsMgrZyy.checkIsInSchedule(line_id, CCTaskForAndroidController.CCTASK, CCTaskForAndroidController.CCTASK_SCANER, adid)){
//				throw new OutScheduleException("The task is not assigned to you . do this task ?",line_id);
//			}
			int associate_type = CCTASK;// schedule.get("associate_type",0);
			int associate_process = CCTASK_SCANER ;//schedule.get("associate_process",0);
			DBRow schedule = floorReceiptsMgrZyy.getScheduleByReceipt(line_id, associate_type, associate_process);
//					floorReceiptsMgrZyy.getScheduleByReceiptAndAdid(line_id, adid, -1, associate_type, associate_process);
			
			if(null==schedule){
				ret = BCSKeyReceive.FAIL;
				err = VAILDATIONERROR;
				result.add("data", "Not schedule ");
			}else{
//				DBRow line = floorReceiptsMgrZyy.getReceiptLinesByTaskId(schedule.get("schedule_id", 0), adid,associate_type,associate_process);
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(schedule.get("associate_id", 0l));
				
				// 获取 line中的 clp 和tpl 的数量
				DBRow[] qtys = floorReceiptsMgrZyy.getPalletQtyByLineId(line.get("receipt_line_id", 0L));
				
				int clp_expected_qty = floorReceiptsMgrZyy.getCCPalletQtyByLineId(line.get("receipt_line_id", 0L));
				int count_qty = floorReceiptsMgrZyy.getCCPalletQtyByLineId(line.get("receipt_line_id", 0L));
				line.add("clp_expected_qty", clp_expected_qty);
				line.add("tlp_expected_qty",  count_qty);
//				if(null!=qtys && qtys.length>0){
//					for(DBRow r:qtys){
//						int container_type  = r.get("container_type", 0);
//						if(container_type==1){
//							//clp 的数量
//							line.add("clp_expected_qty",  r.get("expected_qty", 0));
//						}else if(container_type==3){
//							//tlp 的数量
//							line.add("tlp_expected_qty",  r.get("expected_qty", 0));
//						}
//					}
//				}
				DBRow paramRNRow = new DBRow();
				paramRNRow.add("receipt_id",
						Integer.valueOf(line.getString("receipt_id")));
				DBRow[] receiptLineRows = floorReceiptsMgrZzq
						.queryReceiptLinesInfoByRNId(paramRNRow);
				if(null!=receiptLineRows && receiptLineRows.length==1){
					DBRow row = receiptLineRows[0];
					line.put("config_pallet", row.get("pallets_no", 0));
				}
				
//				line.add("count_pallet", line.get("pallets"));
				line.remove("pallets");
				if(line.get("damage_total",0)==0){
					line.add("damage_total", 0);
				}
				line.add("normal_qty",line.get("goods_total",0));
				line.add("damage_qty", line.get("damage_total",0));
				DBRow[] noBreakPallet = floorReceiptsMgrZyy.findNoBreakPalletByLineId(line.get("receipt_line_id", 0L), 0, ReceiptForAndroidController.GOODS_TYPE_NORMAL);
				// 根据 line获取 托盘的数量
				//已经break 的数量
				int hasbreakQty = floorReceiptsMgrZyy.getBreakPalletQtyByLineId(line.get("receipt_line_id", 0L));
				
				//添加go_to_scan 判断是否 可以进入第扫描
//				if(line.get("clp_expected_qty", 0)==hasbreakQty || null==noBreakPallet || noBreakPallet.length==0){
//					line.add("go_to_scan", true);
//				}else{
//					line.add("go_to_scan", false);
//				}
				// 只确定 数量是否相等，如果不相等 不能跳转。
				if(line.get("clp_expected_qty", 0)==hasbreakQty){
					line.add("go_to_scan", true);
				}else{
					line.add("go_to_scan", false);
				}
				line.add("already_breark_qty", hasbreakQty);
				line.add("scaned_sn", floorReceiptsMgrWfh.findSNCountByLineId(line.get("receipt_line_id", 0L)).get("sn_count", 0));
				DBRow[] goods_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 0,0);
				DBRow[] goodsDa_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 0,1);
				DBRow[] damage_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 2,3);
				line.put("sn_normal_qty", goods_sn_size.length);
				line.put("sn_damage_qty", damage_sn_size.length + goodsDa_sn_size.length);
				
				DBRow[] containerConfigRows = floorReceiptsMgrZzq.queryContainerConfigByOwnParams(line_id, "", 0L, 0L);
				//去掉 配置，适应 前期的代码 2015年6月30日 18:09:01 update by zhaoyy
//				if( containerConfigRows == null || containerConfigRows.length == 0){
//					throw new ReceiveForAndroidException("No container config exists!");
//				}
				JSONObject data = new JSONObject();
				JSONArray tlp = new JSONArray();
				JSONArray clp = new JSONArray();
				JSONArray pltD = new JSONArray();
				for(DBRow row: containerConfigRows){
					String  ship_to_ids  = row.get("ship_to_ids", "");
					String[] ids = ship_to_ids.split(",");
					row.add("license_plate_type_id", row.get("license_plate_type_id",0));
					if(Arrays.asList(ids).contains("0")){
						row.add("ship_to_names", "");
					}else{
						row.add("ship_to_names", floorReceiptsMgrZyy.getShipToNamesByShipToIds(ship_to_ids));
					}
//					if(row.containsKey("license_plate_type_id") && row.get("license_plate_type_id", 0L) > 0){
//						clp.put(row);
//					}else{
//						tlp.put(row);
//					}
//					
//					if(row.containsKey("process") && row.get("process", 0)==CreateContainerProcessKey.PROCESS_CC){
//						// 只有CC 是 才区分
//						clp.put(row);
//					}else{
//						tlp.put(row);
//					}
					if(row.containsKey("process") && row.get("process", 0)==CreateContainerProcessKey.PROCESS_CC){
						// 只有CC 是 才区分
						if(row.get("is_partial", 0)==1){
							//如果是 is_partial 的。
							DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
							for(DBRow conId :palletIds){
								DBRow _row = (DBRow)row.clone();
								_row.add("con_id", conId.get("con_id",""));
								_row.add("keep_inbound", 0);
								_row.put("plate_number", 1);
								clp.put(_row);
							}
							
						}else{
							// 不是 is_partial
							clp.put(row);
						}
					}else{

					}
				}
				
				DBRow[] inboundConfig = floorReceiptsMgrZyy.getInboundConfig(line_id);
				if(null!=inboundConfig && inboundConfig.length>0){
					for(DBRow row :inboundConfig){
						if(row.get("goods_type", 0)==GoodsTypeKeys.TYPE_DAMAGE){
							pltD.put(row);
						}else{
							if(row.get("is_partial", 0)==1){
								//如果是 is_partial 的。
								DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
								for(DBRow conId :palletIds){
									DBRow _row = (DBRow)row.clone();
									_row.add("con_id", conId.get("con_id",""));
									_row.put("plate_number", 1);
									tlp.put(_row);
								}
								
							}else{
								// 不是 is_partial
								tlp.put(row);
							}
						}
						
					}
				}
				// 添加break partial  的托盘信息
				DBRow[] breakPartial = floorReceiptsMgrZyy.getBreakPartialByLineId(line_id);
				for(DBRow row :breakPartial){
					row.add("license_plate_type_id",0);
					row.add("is_partial",1);
					row.add("plate_number",1);
				}
				// 删除 没有用到的TLP
				// pallets 显示的是 count 时 创建的托盘的 配置
				result.put("break_partial", breakPartial);
				result.put("pallets", tlp);
				result.put("pallets_cc", clp);
				result.put("pallets_d", pltD);
				DBRow location = floorReceiptsMgrWfh.findLocationByLineId(line.get("receipt_line_id", 0L));
				line.add("location",  location.get("location",""));
				result.add("data", line);
			}
			
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (OutScheduleException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.OUT_SCHEDULE;
			result.add("data", e.getMessage());
			result.add("line_id", e.getData());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception(e.getMessage());
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 *
	 * acquire receipt_line info by task id
	 * 2015年5月11日
	 * @param line_id
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/acquireScanReceiptLinesByLineId", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String acquireScanReceiptLinesByLineId(
			@RequestParam(value = "line_id",required = false,defaultValue="0") long line_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		if (adid == 0 || line_id == 0) {
			result.add("ret", BCSKeyReceive.FAIL);
			result.add("err", BCSKeyReceive.SYSTEMERROR);
			result.add("data", "Parameter error");
			return CommonUtils.convertDBRowsToJsonString(result);
		}
		//TODO 根据task_id 和adid 获取信息
		try{
			if(!floorReceiptsMgrZyy.checkIsInSchedule(line_id, CCTaskForAndroidController.CCTASK, CCTaskForAndroidController.CCTASK_SCANER, adid)){
				throw new OutScheduleException("The task is not assigned to you . do this task ?",line_id);
			}
			int associate_type = CCTASK;// schedule.get("associate_type",0);
			int associate_process = CCTASK_SCANER ;//schedule.get("associate_process",0);
			DBRow schedule = floorReceiptsMgrZyy.getScheduleByReceipt(line_id, associate_type, associate_process);
//					floorReceiptsMgrZyy.getScheduleByReceiptAndAdid(line_id, adid, -1, associate_type, associate_process);
			
			if(null==schedule){
				ret = BCSKeyReceive.FAIL;
				err = VAILDATIONERROR;
				result.add("data", "Not schedule ");
			}else{
//				DBRow line = floorReceiptsMgrZyy.getReceiptLinesByTaskId(schedule.get("schedule_id", 0), adid,associate_type,associate_process);
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(schedule.get("associate_id", 0l));
				
				// 获取 line中的 clp 和tpl 的数量
//				DBRow[] qtys = floorReceiptsMgrZyy.getPalletQtyByLineId(line.get("receipt_line_id", 0L));
				
				int clp_expected_qty = floorReceiptsMgrZyy.getCCPalletQtyByLineId(line.get("receipt_line_id", 0L));
				int expected_qty = floorReceiptsMgrZyy.getCountQTYByLineId(line.get("receipt_line_id", 0L));
				line.add("clp_expected_qty",  clp_expected_qty);
				line.add("tlp_expected_qty",  expected_qty);
//				if(null!=qtys && qtys.length>0){
//					for(DBRow r:qtys){
//						int container_type  = r.get("container_type", 0);
//						if(container_type==1){
//							//clp 的数量
//							
//						}else if(container_type==3){
//							//tlp 的数量
//							
//						}
//					}
//				}
				DBRow paramRNRow = new DBRow();
				paramRNRow.add("receipt_id",
						Integer.valueOf(line.getString("receipt_id")));
				DBRow[] receiptLineRows = floorReceiptsMgrZzq
						.queryReceiptLinesInfoByRNId(paramRNRow);
				if(null!=receiptLineRows && receiptLineRows.length==1){
					DBRow row = receiptLineRows[0];
					line.put("config_pallet", row.get("pallets_no", 0));
				}
				
//				line.add("count_pallet", line.get("pallets"));
				line.remove("pallets");
				if(line.get("damage_total",0)==0){
					line.add("damage_total", 0);
				}
				line.add("normal_qty",line.get("goods_total",0));
				line.add("damage_qty", line.get("damage_total",0));
				DBRow[] noBreakPallet = floorReceiptsMgrZyy.findNoBreakPalletByLineId(line.get("receipt_line_id", 0L), 0, ReceiptForAndroidController.GOODS_TYPE_NORMAL);
				// 根据 line获取 托盘的数量
				//已经break 的数量
				int hasbreakQty = floorReceiptsMgrZyy.getBreakPalletQtyByLineId(line.get("receipt_line_id", 0L));
				
				//添加go_to_scan 判断是否 可以进入第扫描
				if(line.get("clp_expected_qty", 0)==hasbreakQty || null==noBreakPallet || noBreakPallet.length==0){
					line.add("go_to_scan", true);
				}else{
					line.add("go_to_scan", false);
				}
				line.add("already_breark_qty", hasbreakQty);
				line.add("scaned_sn", floorReceiptsMgrWfh.findSNCountByLineId(line.get("receipt_line_id", 0L)).get("sn_count", 0));
				DBRow[] goods_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 0,0);
				DBRow[] goodsDa_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 0,1);
				DBRow[] damage_sn_size = floorReceiptsMgrWfh.findSNByLineId(line.get("receipt_line_id", 0L), 2,3);
				line.put("sn_normal_qty", goods_sn_size.length);
				line.put("sn_damage_qty", damage_sn_size.length + goodsDa_sn_size.length);
				
//				DBRow[] containerConfigRows = floorReceiptsMgrZzq.queryContainerConfigByOwnParams(line_id, "", 0L, 0L);
//				//去掉 配置，适应 前期的代码 2015年6月30日 18:09:01 update by zhaoyy
////				if( containerConfigRows == null || containerConfigRows.length == 0){
////					throw new ReceiveForAndroidException("No container config exists!");
////				}
				JSONObject data = new JSONObject();
				JSONArray tlp = new JSONArray();
				JSONArray clp = new JSONArray();
//				for(DBRow row: containerConfigRows){
//					String  ship_to_ids  = row.get("ship_to_ids", "");
//					String[] ids = ship_to_ids.split(",");
//					row.add("license_plate_type_id", row.get("license_plate_type_id",0));
//					if(Arrays.asList(ids).contains("0")){
//						row.add("ship_to_names", "");
//					}else{
//						row.add("ship_to_names", floorReceiptsMgrZyy.getShipToNamesByShipToIds(ship_to_ids));
//					}
//					if(row.containsKey("process") && row.get("process", 0)==CreateContainerProcessKey.PROCESS_CC){
//						// 只有CC 是 才区分
//						clp.put(row);
//					}
//				}
				DBRow[] ccConfig = floorReceiptsMgrZyy.getCCConfig(line_id);
				if(null!=ccConfig && ccConfig.length>0){
					for(DBRow row :ccConfig){
						if(row.get("goods_type", 0)==GoodsTypeKeys.TYPE_DAMAGE){
						}else{
							if(row.get("is_partial", 0)==1){
								//如果是 is_partial 的。
								DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
								for(DBRow conId :palletIds){
									DBRow _row = (DBRow)row.clone();
									_row.add("con_id", conId.get("con_id",""));
									_row.put("plate_number", 1);
									clp.put(_row);
								}
								
							}else{
								// 不是 is_partial
								clp.put(row);
							}
						}
						
					}
				}
				DBRow[] inboundConfig = floorReceiptsMgrZyy.getInboundConfig(line_id);
				if(null!=inboundConfig && inboundConfig.length>0){
					for(DBRow row :inboundConfig){
						if(row.get("goods_type", 0)==GoodsTypeKeys.TYPE_DAMAGE){
						}else{
							if(row.get("is_partial", 0)==1){
								//如果是 is_partial 的。
								DBRow[] palletIds = floorReceiptsMgrZyy.getPartialConIds(row.get("receipt_line_id", 0l), row.get("container_config_id", 0l), row.get("process", 0), row.get("is_partial", 0));
								for(DBRow conId :palletIds){
									DBRow _row = (DBRow)row.clone();
									_row.add("con_id", conId.get("con_id",""));
									_row.put("plate_number", 1);
									tlp.put(_row);
								}
								
							}else{
								// 不是 is_partial
								tlp.put(row);
							}
						}
						
					}
				}
				// 添加break partial  的托盘信息
				DBRow[] breakPartial = floorReceiptsMgrZyy.getBreakPartialByLineId(line_id);
				for(DBRow row :breakPartial){
					row.add("license_plate_type_id",0);
					row.add("is_partial",1);
					row.add("plate_number",1);
				}
				result.put("break_partial", breakPartial);
				// 删除 没有用到的TLP
				result.put("pallets", tlp);
				result.put("pallets_cc", clp);
				DBRow location = floorReceiptsMgrWfh.findLocationByLineId(line.get("receipt_line_id", 0L));
				line.add("location",  location.get("location",""));
				result.add("data", line);
			}
			
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (OutScheduleException e) {
			ret = BCSKeyReceive.SUCCESS;
			err = BCSKeyReceive.OUT_SCHEDULE;
			result.add("data", e.getMessage());
			result.add("line_id", e.getData());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception(e.getMessage());
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 *
	 * create clp palltes to target line
	 * 2015年5月11日
	 * @param license_plate_type_id
	 * @param pallet_type
	 * @param qty
	 * @param location
	 * @param location_type
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/createCLPContainerToLine", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createCLPContainerToLine(
			@RequestParam(value = "license_plate_type_id",required = false,defaultValue="0") long license_plate_type_id,
			@RequestParam(value = "pallet_type",required = false,defaultValue="0") String pallet_type,
			@RequestParam(value = "qty",required = false,defaultValue="0") long qty,
			@RequestParam(value = "location",required = false,defaultValue="0") String location,
			@RequestParam(value = "location_type",required = false,defaultValue="0") String location_type,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 *
	 * create scan task to scaner
	 * 2015年5月11日
	 * @param receipt_id
	 * @param operator
	 * @param adid
	 * @param ps_id
	 * @param session
	 * @return
	 * @throws Exception
	 *	
	 *	操作，supervisor 把 CC 的配置信息填后，把任务配给 scaner
	 */
	@Transactional
	@RequestMapping(value = "/android/createScanTaskToScaner", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String createScanTaskToScaner(
			@RequestParam(value = "receipt_line_id",required = false,defaultValue="0") long receipt_line_id,
			@RequestParam(value = "operator",required = false,defaultValue="") String operator,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		//TODO supervisor assign 
		try{
			//参数验证
			if(receipt_line_id==0 || ps_id == 0 || adid==0 || "".equals(operator)){
//				result.add("ret", BCSKey.FAIL);
//				result.add("err", BCSKey.SYSTEMERROR);
//				result.add("data", "Parameter error");
//				return CommonUtils.convertDBRowsToJsonString(result);
				throw new ReceiveForAndroidException("Parameter error");
			}
			
			DBRow schedules = floorReceiptsMgrZyy.getScheduleByReceipt(receipt_line_id,CCTASK, CCTASK_SUPERVISOR);
			
			if(null!= schedules){
				//TODO supervisor 分配完扫描任务后，supervisor 要关闭 自己的任务？
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
				result = assignScanTaskToLabor(result, receipt_line_id, operator, adid, ps_id ,line.get("receipt_id",0l), session);
				if(result.get("ret", 0)==BCSKeyReceive.SUCCESS){
					//TODO 关闭任务
					long schedule_id =schedules.get("schedule_id", 0l); 
					DBRow[] scheduleSub =floorReceiptsMgrWfh.findSheduleSubByScheduleID(schedule_id);
					//关闭子任务
					for (DBRow task : scheduleSub) {
	    				DBRow subRow = new DBRow();
	    				subRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
	    				subRow.add("is_task_finish", 1);
	    				subRow.add("schedule_finish_adid", adid);
	    				floorReceiptsMgrWfh.closeScheduleSub(task.get("schedule_sub_id", 0L), subRow);
	    			}
					DBRow  upRow = new DBRow();
	    			upRow.add("end_time", DateUtil.NowStr());
	    			upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
	    			floorReceiptsMgrWfh.closeSchedule(schedule_id, upRow);
				}
				return CommonUtils.convertDBRowsToJsonString(result);
			}else{
//				ret = BCSKey.FAIL;
//				err = VAILDATIONERROR;
//				result.add("data", "Not schedule ");
				//TODO supervisor 分配完扫描任务后，supervisor 要关闭 自己的任务？
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
				DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK, CCTASK_SCANER, receipt_line_id);
				if(null!=schedule && schedule.length == 1){
					result = updateScanTaskToLabor(result, receipt_line_id, operator, adid, ps_id ,line.get("receipt_id",0l), session);
				}else{
					result = assignScanTaskToLabor(result, receipt_line_id, operator, adid, ps_id ,line.get("receipt_id",0l), session);
				}
			}
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》createScanTaskToScaner");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	} 
	
	@Transactional
	@RequestMapping(value = "/android/deleteClpPalletConfig", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deleteClpPalletConfig(
			@RequestParam(value="container_config_id",required=false,defaultValue="0")long container_config_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid
		) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		//TODO 检测托盘是否在使用中 和添加删除日志
		try{
			boolean flag = false;
			if (adid == 0 || container_config_id == 0 ) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			
			//查询配置
			DBRow[] containers = floorReceiptsMgrWfh
					.findContainerByConConfigId(container_config_id);
			//逐条删除 托盘
			for(DBRow row :containers){
				if(row.get("process", 0)==CreateContainerProcessKey.PROCESS_CC){
					floorReceiptsMgrZyy.delContainerById(row.get("con_id", 0l), true);
				}else{
					flag = true;
				}
			}
			
			if(!flag){
				//TODO 删除配置
				floorReceiptsMgrZyy.delContainerConfigById(container_config_id, true);
				//删除关联表
				floorReceiptsMgrZyy.deleteReceiptRelContainerByCCId(container_config_id);
			}
			
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》deleteClpPalletConfig");
		}
		
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}

	@Transactional
	@RequestMapping(value = "/android/deleteClpPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deleteClpPallet(
			@RequestParam(value="receipt_line_id",required=false,defaultValue="0")long receipt_line_id,
			@RequestParam(value="con_id",required=false,defaultValue="0")long con_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try{
			//TODO 检测托盘是否在使用中 和添加删除日志
			if (adid == 0 || receipt_line_id == 0 || con_id == 0) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			DBRow[] rrc = floorReceiptsMgrZyy.getReceiptRelContainerByConId(con_id);
			if(null!=rrc && rrc.length>0){
				floorReceiptsMgrZyy.delContainerById(con_id, true);
				if(rrc.length==1){
					//删除配置信息
					long container_config_id = rrc[0].get("container_config_id", 0l);
					floorReceiptsMgrZyy.delContainerConfigById(container_config_id, true);
				}
			}else{
				//没[]有数据
				ret = BCSKeyReceive.FAIL;
				err = BCSKeyReceive.FAIL;
				result.add("data", "Not data ");
				throw new ReceiveForAndroidException("Not pallet");
			}
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》deleteClpPallet");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	@Transactional(rollbackFor = { Exception.class, RuntimeException.class })
	@RequestMapping(value = "/android/scanDeleteClpPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String scanDeleteClpPallet(
			@RequestParam(value="con_id",required=false,defaultValue="0")long con_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try{
			//TODO 查询该托盘上是否已经 有了SN ，如果有SN 责 不允许删除
			//如果该line下的TLP的托盘没有拆完，责不允许删除
			//坏的货物+已经做CLP的托盘数量不等于Count的数量，不让删除。
			DBRow[] conSn = floorReceiptsMgrWfh.findSNByConConfigId(con_id);
			if(null!=conSn && conSn.length>0){
				throw new ReceiveForAndroidException("Has sn not allow delete");
			}else{
				//删除CLP 托盘 逻辑删除托盘 
				floorReceiptsMgrZyy.delContainerById(con_id);
			}
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new Exception("CCTaskForAndroidController-》deleteClpPallet");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 * 删除 break 的托盘
	 * @param cc_from_id  cc_from 表中的  id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/deleteBreakPallet", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deleteBreakPallet(
			@RequestParam(value="cc_from_id",required=false,defaultValue="0")long cc_from_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try{
			deleteBreakPalletByCcfromId(cc_from_id,adid);
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》deleteClpPallet");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	private void deleteBreakPalletByCcfromId (long cc_from_id,long adid) throws Exception  {
		if (adid == 0 || cc_from_id == 0 ) {
			throw new ReceiveForAndroidException("Parameter error!");
		}
		
		//TODO 根据cc_from_id 获取 cc_from 的方法
		DBRow from = floorReceiptsMgrZyy.getBraekPalletById(cc_from_id);
		if(null!=from){
			// 根据 container_id  container 
			long con_id = from.get("container_id", 0L);
			DBRow line = floorReceiptsMgrZyy.getLineByConid(con_id);
			if(null!= line && !"".equals(line.get("scan_start_adid", ""))){
				throw new ReceiveForAndroidException("Has started scanning, can't be deleted");
			}
			long line_id = from.get("receipt_line_id", 0L);
			int quantity = from.get("quantity",0);
			DBRow[] container  = floorReceiptsMgrZyy.getExpectedQtyAndPallet(line_id ,con_id);
			if(null!= container && container.length>0){
				for(DBRow row :container){
					if(row.get("is_delete", 0l)==1){
						//把托盘 修改成 未删除状态
						floorReceiptsMgrZyy.modifyContainerIsDelete(con_id, 0);
					}
					//TODO 修改   receipt_rel_container 表中的商品数量
					int qty  = row.get("normal_qty", 0)+quantity;
					long receipt_rel_container_id = row.get("receipt_rel_container_id", 0L);
					floorReceiptsMgrZyy.modifyReceiptRelContainerNormalQty(receipt_rel_container_id, qty);
					//删除cc_from 中的数据
					floorReceiptsMgrZyy.delBreakPalletById(cc_from_id);
					
					DBRow cpRow = floorReceiptsMgrWfh.findContainerProductByPalletId(row.get("con_id", 0l));
					if(null!=cpRow){
						cpRow.add("cp_quantity", cpRow.get("cp_quantity", 0f)+quantity);
						floorReceiptsMgrWfh.updateContainerProduct(cpRow.get("cp_id",0), cpRow);
					}
				}
				
			}
		}else{
			throw  new ReceiveForAndroidException( "No delete break pallet :"+cc_from_id);
		}
	}
	/**
	 * 删除 break 的托盘
	 * @param cc_from_id  cc_from 表中的  id
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/deleteBreakPalletByLine", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String deleteBreakPalletByLine(
			@RequestParam(value="line_id",required=false,defaultValue="0")long line_id,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid) throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		
		try{
			if (adid == 0 || line_id == 0 ) {
				throw new ReceiveForAndroidException("Parameter error!");
			}
			
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(line_id);
			if(!"".equals(line.get("scan_start_adid", ""))){
				throw new ReceiveForAndroidException("Has started scanning, can't be deleted");
			}
			
			//TODO 根据cc_from_id 获取 cc_from 的方法
			DBRow[] froms = floorReceiptsMgrZyy.getBraekPalletByLine(line_id);
			//edit by zhengziqi at July.2nd 2015
//			if(null==froms || froms.length==0){ 
//				throw new ReceiveForAndroidException("Breaked pallets not found!");
//			}
			for(DBRow from :froms){
				// 根据 container_id  container 
				long con_id = from.get("plate_no", 0L);
				long cc_from_id = from.get("cc_from_id",0l);
				int quantity = from.get("break_qty",0);
				DBRow[] container  = floorReceiptsMgrZyy.getExpectedQtyAndPallet(line_id ,con_id);
				if(null!= container && container.length>0){
					for(DBRow row :container){
						if(row.get("is_delete", 0l)==1){
							//把托盘 修改成 未删除状态
							floorReceiptsMgrZyy.modifyContainerIsDelete(con_id, 0);
						}
						//TODO 修改   receipt_rel_container 表中的商品数量
						int qty  = row.get("normal_qty", 0)+quantity;
						long receipt_rel_container_id = row.get("receipt_rel_container_id", 0L);
						floorReceiptsMgrZyy.modifyReceiptRelContainerNormalQty(receipt_rel_container_id, qty);
						//删除cc_from 中的数据
						floorReceiptsMgrZyy.delBreakPalletById(cc_from_id);
						
						DBRow cpRow = floorReceiptsMgrWfh.findContainerProductByPalletId(row.get("con_id", 0l));
						if(null!=cpRow){
							cpRow.add("cp_quantity", cpRow.get("cp_quantity", 0f)+quantity);
							floorReceiptsMgrWfh.updateContainerProduct(cpRow.get("cp_id",0), cpRow);
						}
					}
					
				}
			}
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》deleteBreakPalletByLine");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	
	
	@Transactional
	@RequestMapping(value = "/android/breakPallets", produces = "application/json; charset=UTF-8")
	public String scanBreakPallets(
			@RequestParam(value="receipt_line_id",required=false,defaultValue="0")long receipt_line_id ,
			@RequestParam(value="pallet_qty",required=false,defaultValue="") String pallet_qty ,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid) throws Exception{
		
		//pallet_qty:{[{pallet_no:1000,qty:100},{pallet_no:1001,qty:100}]}
		//想要获取总数 需要参数 receipt_line_id 
		//目前的现在 的写法，无法在此处判断 receipt_line_id 是否应该被 adid操作,查询方法需要修改
		//要想获取一个托盘上的数量 需要知道托盘配置信息 托=
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try{
			//参数验证
			if(receipt_line_id==0 ||  adid==0 || "".equals(pallet_qty ) ){
				throw new ReceiveForAndroidException("Parameter error!");
			}
			//
			JSONArray palletQty = new JSONArray(pallet_qty);
			DBRow[]  breakContainers =floorReceiptsMgrZyy.getBreakContainersByLineId(receipt_line_id);
			Map<String,DBRow> map = new HashMap<String, DBRow>();
			for(DBRow row:breakContainers){
				map.put(row.get("container_id","" ), row);
			}
			
			for(int i = 0,j=palletQty.length();i<j;i++){
				JSONObject json = palletQty.getJSONObject(i);
				String key = json.getString("pallet_no");
				if(map.containsKey(key)){
					deleteBreakPalletByCcfromId(map.get(key).get("cc_from_id", 0l), adid);
				}
			}
			
			// 获取已经拆的托盘数量
			int hasbreakQty = floorReceiptsMgrZyy.getBreakPalletQtyByLineId(receipt_line_id);
			
			if(palletQty.length()==0){
//				ret = BCSKey.FAIL;
//				err = BCSKey.FAIL;
//				result.add("data","Not Pallets break");
				throw new ReceiveForAndroidException("Not Pallets break");
			}else{
				DBRow[] lines = floorReceiptsMgrZyy.getPalletQtyByLineId(receipt_line_id);
				
				
				
				
				if(null==lines || lines.length==0){
					//没有数据 ，
					throw new ReceiveForAndroidException("Not line ");
				}
				int expected_qty = floorReceiptsMgrZyy.getCountQTYByLineId(receipt_line_id);
//				for(DBRow r: lines){
//					if(r.get("container_type", 0l)==1){
//						expected_qty = r.get("expected_qty", 0);
//						break;
//					}
//				}
				expected_qty  -= hasbreakQty;
				int breakQty = strArrayToTotalNum(palletQty);
				if(expected_qty < breakQty){
					//数量不匹配
					throw new ReceiveForAndroidException("Qty not match expected_qty");
				}else{
					StringBuffer logBuf = new StringBuffer();
					
					logBuf.append("BREAK Pallets:");
					
					//TODO 逐条验证，验证通过后 放在cc_from表中 。根据receipt_line_id 获取到对应托盘的数量和托盘ID
//					DBRow[] receiptLineConfig = floorReceiptsMgrZyy.getExpectedQtyAndPallet(receipt_line_id);
					DBRow[] receiptLineConfig = floorReceiptsMgrWfh.findPalletByLineId(
							receipt_line_id, 0, 3);
					// 处理数据
					boolean exceed = false;
					for(int i = 0,j=palletQty.length();i<j && !exceed;i++){
						
						JSONObject json = palletQty.getJSONObject(i);
						long p_no = json.getLong("pallet_no");
						long plate_qty =  json.getLong("qty");
						for(DBRow row :receiptLineConfig){
							if(row.get("keep_inbound", 0)==1 && row.get("con_id",0l)== p_no){
								throw  new ReceiveForAndroidException("Pallet config keep inbound ,not allowed break");
							}
							long _p_no = row.get("plate_no", 0l);
							long _expected_qty = row.get("normal_qty",0l);
							if(_p_no==p_no){
								if(_expected_qty>=plate_qty){
									//添加的cc_from
									DBRow ccFrom = new DBRow();
									ccFrom.add("receipt_line_id",row.get("receipt_line_id",0 ));
									ccFrom.add("container_id",row.get("plate_no",0 ));
									ccFrom.add("quantity",plate_qty);
									ccFrom.add("adid",adid);
									ccFrom.add("time",DateUtil.NowStr());
									long ccFromId = floorReceiptsMgrZyy.addCCFrom(ccFrom);
									logBuf.append(ccFromId).append(",");
									//TODO 修改拆开托盘的数量
									if(_expected_qty==plate_qty){
										floorReceiptsMgrZyy.delContainerById(_p_no);
									}
									// 修改 数据量 ，并吧 该托盘编辑为
									floorReceiptsMgrZyy.modifyReceiptRelContainerNormalQty(row.get("receipt_rel_container_id",0l ), _expected_qty-plate_qty);
									//TODO 查询container_product
									DBRow cpRow = floorReceiptsMgrWfh.findContainerProductByPalletId(row.get("con_id", 0l));
									if(null!=cpRow){
										float cp_quantity = cpRow.get("cp_quantity", 0f);
										cpRow.add("cp_quantity",cp_quantity-plate_qty);
										floorReceiptsMgrWfh.updateContainerProduct(cpRow.get("cp_id",0), cpRow);
									}
									
									//TODO 如果 托盘拆下的数量，小于托盘的实际数量,则不能逻辑删除托盘，怎么修改
									break;
								}else{
									//TODO 回滚 相关数据
									ret = BCSKeyReceive.FAIL;
									err = BCSKeyReceive.FAIL;
									result.add("data",p_no +" Qty Exceed expected_qty");
									exceed = true;
									break;
								}
							}
						}
					}
				}
			}
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			result.add("data", "System errors ");
			throw new Exception("CCTaskForAndroidController-》scanBreakPallets");
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 * 创建CCTask任务assign给superVisor
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/assignSupervisorTask", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String assignSupervisorTask(
			@RequestParam(value = "receipt_id",required = false,defaultValue="0") long receipt_id,
			@RequestParam(value = "receipt_line_id",required = false,defaultValue="0") long receipt_line_id,
			@RequestParam(value = "note",required = false,defaultValue="") String note,
			@RequestParam(value = "assignSupurvisor",required = false,defaultValue="") String assignSupurvisor,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			//参数验证
			if(receipt_line_id==0 || ps_id == 0 || adid==0 || receipt_id ==0 || "".equals(assignSupurvisor)){
				throw new ReceiveForAndroidException("Parameter error");
			}
			
			/*line的status字段
	        0:WMS原始数据;1:已配置托盘并打印;2:cc task;3:scan task;4:关闭
			 */
			boolean flag = true;
			StringBuffer log = new StringBuffer();  //记录日志
			//创建task 如果 expected_qty != normal_qty+ damage_qty + stock_qty  则不可以创建人物
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
			DBRow sumQTY = floorReceiptsMgrWfh.findSUMReceivedQTY(receipt_line_id,3,true);
			int receivedQty = line.get("received_qty", 0);
			float damageQty = sumQTY.get("damage_qty", 0F);
			float normalQty = sumQTY.get("normal_qty", 0F);
			if(receivedQty > damageQty+normalQty){
				flag = false;
//				result.add("data", "Please count all of the pallet");
//				ret = BCSKey.FAIL;
//				err = VAILDATIONERROR;
				throw new ReceiveForAndroidException("Please count all of the pallet");
			}
			//如果任务已经assign则不再进行操作
			DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK,CCTASK_SUPERVISOR,receipt_line_id);
			if(schedule!=null && schedule.length>0){
				flag = false;
				throw new ReceiveForAndroidException("Please don't repeat the assign task");
			}
			
			if(flag){
				//如果已经有这条schedule 则只更新就可以
				
				//将line的status改为2  并且写入counted的时间与操作人 
				DBRow scheduleRow = null;
				DBRow upRow = new DBRow();
				upRow.add("status", 2);
				upRow.add("unloading_finish_time", DateUtil.NowStr());
				upRow.add("counted_user", adid);
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
				//添加supervisor schedule
				upRow = new DBRow();
				upRow.add("assign_user_id",adid);
				upRow.add("schedule_detail","Create CCTask: "+receipt_line_id);
				upRow.add("schedule_state",0F);
				upRow.add("schedule_is_note",1);
				upRow.add("is_need_replay",1);
				upRow.add("is_update",0);
				upRow.add("is_all_day",0);
				upRow.add("is_schedule",1);
				upRow.add("schedule_overview","CCTask: "+receipt_line_id+" Need Assign");
				upRow.add("associate_type",CCTASK);  //以后添加新的 type  ccTask
				upRow.add("associate_process",CCTASK_SUPERVISOR);  //以后添加新的 process  supervisor  scaner
				upRow.add("associate_id",receipt_line_id);  
				upRow.add("sms_short_notify",1);  
				upRow.add("sms_email_notify",1);  
				upRow.add("is_task",0);  
				upRow.add("associate_main_id",receipt_id); 
				upRow.add("create_time", DateUtil.NowStr());	//DateUtil.showUTCTime(DateUtil.NowStr(), ps_id)
				long scheduleID = floorReceiptsMgrWfh.addSchedule(upRow);
				upRow.add("schedule_id", scheduleID);
				scheduleRow = (DBRow) upRow.clone();
				//添加schedule_sub
				String[] supurvisor = assignSupurvisor.split(",");
				log.append("Create CCTask【"+receipt_line_id+"】|");
				
				log.append("Assign to:");
				for (String s : supurvisor) {
					upRow = new DBRow();
					upRow.add("schedule_id", scheduleID);
					upRow.add("schedule_execute_id", s);
					upRow.add("schedule_state", 0);
					upRow.add("is_task_finish", 0);
					floorReceiptsMgrWfh.addScheduleSub(upRow);
					DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long.parseLong(s));
					log.append(admin.get("employe_name", "")).append(",");
				}
				
				//写入日志
				DBRow taskLog = new DBRow();
				taskLog.add("operator_type", "Create Task");
				taskLog.add("operator_id", adid);
				taskLog.add("operator_time", DateUtil.NowStr());
				taskLog.add("receipt_line_id", receipt_line_id);
				taskLog.add("receipt_line_status", 2);
				taskLog.add("data", log.substring(0, log.length()-1));
				floorReceiptsMgrWfh.addTaskLog(taskLog);
				//是否是assign最后一个line
				DBRow[] lines = floorReceiptsMgrWfh.findReceiptLineByReceiptId(receipt_id);
				boolean allAssignFlag = true;
				for (DBRow ln : lines) {
					if(ln.get("counted_user", 0L) == 0){
						allAssignFlag = false;
					}
				}
				if(allAssignFlag){
					
					DBRow receipt = floorReceiptsMgrWfh.findReceiptByLineId(receipt_line_id);
					String entry_id = floorReceiptsMgrZzq.queryEntryIdByLineId(receipt_line_id+"");
					Map<String, String> parameter = new HashMap<String,String>();
					parameter.put("adid", String.valueOf(adid));
					parameter.put("entry_id", entry_id);
					parameter.put("companyId", receipt.get("company_id", ""));
					parameter.put("receiptNo", receipt.get("receipt_no", ""));
					writebackToWmsMgrZyy.synchronizationReceipted(parameter);
				}
			}
		}catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	/**
	 * 创建ScanTask任务assign给scan
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/assignScanTask", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String assignScanTask(
			@RequestParam(value = "receipt_id",required = false,defaultValue="0") long receipt_id,
			@RequestParam(value = "receipt_line_id",required = false,defaultValue="0") long receipt_line_id,
			@RequestParam(value = "assignScan",required = false,defaultValue="") String assignScan,
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required = false,defaultValue="0") long ps_id,
			HttpSession session
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			//参数验证
			if(receipt_line_id==0 || ps_id == 0 || adid==0 || receipt_id ==0 || "".equals(assignScan)){
				throw new ReceiveForAndroidException("Parameter error");
			}
			
			/*line的status字段
	        0:WMS原始数据;1:已配置托盘并打印;2:cc task;3:scan task;4:关闭
			 */
			boolean flag = true;
			StringBuffer assignLog = new StringBuffer(); 
			
			//如果任务已经assign则不再进行操作
			DBRow[] schedule = floorReceiptsMgrWfh.findSchedule(CCTASK,CCTASK_SCANER,receipt_line_id);
			if(schedule!=null && schedule.length>0){
				throw new ReceiveForAndroidException("Please don't repeat the assign task");
			}
			
			if(flag){
				DBRow scheduleRow = null;
				//finish supervisor 的schedule
				finishSchedule(receipt_line_id, adid,CCTASK,CCTASK_SUPERVISOR,ps_id);
				//将line的status  add cc info  by zhaoyy 2015年6月19日 16:51:19
				DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
				DBRow upRow = new DBRow();
				upRow.add("status", LineStatueKey.STATUS_SCAN);
				if("".equals(line.get("cc_start_time", ""))){
					upRow.add("cc_start_time",DateUtil.NowStr());
					upRow.add("cc_user", adid);
				}
				upRow.add("cc_finish_time",DateUtil.NowStr());
				floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
				//添加scan schedule
				upRow = new DBRow();
				upRow.add("assign_user_id",adid);
				upRow.add("schedule_detail","Create ScanTask: "+receipt_line_id);
				upRow.add("schedule_state",0F);
				upRow.add("schedule_is_note",1);
				upRow.add("is_need_replay",1);
				upRow.add("is_update",0);
				upRow.add("is_all_day",0);
				upRow.add("is_schedule",1);
				upRow.add("schedule_overview","ScanTask: "+receipt_line_id+" can begin");
				upRow.add("associate_type",CCTASK); 
				upRow.add("associate_process",CCTASK_SCANER);  
				upRow.add("associate_id",receipt_line_id);  
				upRow.add("sms_short_notify",1);  
				upRow.add("sms_email_notify",1);  
				upRow.add("is_task",0);  
				upRow.add("associate_main_id",receipt_id); 
				upRow.add("create_time", DateUtil.NowStr());	//DateUtil.showUTCTime(DateUtil.NowStr(), ps_id)
				long scheduleID = floorReceiptsMgrWfh.addSchedule(upRow);
				upRow.add("schedule_id", scheduleID);
				scheduleRow = (DBRow) upRow.clone();
				//添加schedule_sub
				String[] scan = assignScan.split(",");
				if(scan.length>0)
					assignLog.append("Assign To:|");
				for (String s : scan) {
					upRow = new DBRow();
					upRow.add("schedule_id", scheduleID);
					upRow.add("schedule_execute_id", s);
					upRow.add("schedule_state", 0);
					upRow.add("is_task_finish", 0);
					floorReceiptsMgrWfh.addScheduleSub(upRow);
					DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(Long.parseLong(s));
					assignLog.append(admin.get("employe_name", "")).append(",");
				}
				
				//写入日志
				DBRow taskLog = new DBRow();
				taskLog.add("operator_type", "Create Task");
				taskLog.add("operator_id", adid);
				taskLog.add("operator_time", DateUtil.NowStr());
				taskLog.add("receipt_line_id", receipt_line_id);
				taskLog.add("receipt_line_status", 3);
				taskLog.add("data", assignLog.length()>0?assignLog.substring(0, assignLog.length()-1):"");
				floorReceiptsMgrWfh.addTaskLog(taskLog);
				
			}
		} catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new Exception(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	/**
	 * close scan的task
	 * 
	 * @param parameter
	 * @return
	 * @throws Exception
	 *
	 */
	@Transactional
	@RequestMapping(value = "/android/finishScanTask", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String finishScanTask(
			@RequestParam(value = "receipt_line_id",required =false,defaultValue="0") long receipt_line_id,
			@RequestParam(value = "adid",required =false,defaultValue="0") long adid,
			@RequestParam(value = "ps_id",required =false,defaultValue="0") long ps_id
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			
			//参数验证
			if(receipt_line_id==0 || ps_id == 0 || adid==0 ){
//				result.add("ret", BCSKey.FAIL);
//				result.add("err", BCSKey.SYSTEMERROR);
//				result.add("data", "Parameter error");
//				return CommonUtils.convertDBRowsToJsonString(result);
				throw new ReceiveForAndroidException("Parameter error");
			}
			
			/*line的status字段
	        0:WMS原始数据;1:已配置托盘并打印;2:cc task;3:scan task;4:关闭
			 */

			//将line的status
			DBRow upRow = new DBRow();
			upRow.add("status", 4);
			floorReceiptsMgrWfh.updateReceiptLine(receipt_line_id, upRow);
			//关闭schedule
			finishSchedule(receipt_line_id, adid, CCTASK, CCTASK_SCANER,ps_id);
			result.add("success", "true");
		} catch(ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	//关闭schedule任务
	private String finishSchedule(long receipt_line_id, long adid,int associate_type,int associate_process,long ps_id)
			throws Exception {
		StringBuffer log = new StringBuffer();
		DBRow upRow;
		//关闭schedule的任务
		DBRow[] schedule = floorReceiptsMgrWfh.findSheduleByLineID(receipt_line_id,associate_type,associate_process);
		log.append("Finish CCTask【"+receipt_line_id+"】|");
		DBRow admin = floorReceiptsMgrWfh.findAdminByAdid(adid);
		log.append("finish by: ").append(admin.get("employe_name", ""));
		DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
		//写入日志
		DBRow taskLog = new DBRow();
		taskLog.add("operator_type", "Finish Task");
		taskLog.add("operator_id", adid);
		taskLog.add("operator_time", DateUtil.NowStr());
		taskLog.add("receipt_line_id", receipt_line_id);
		taskLog.add("receipt_line_status", line.get("status", 0));
		taskLog.add("data", log);
		floorReceiptsMgrWfh.addTaskLog(taskLog);
		
		long schedule_id = 0;
		for (DBRow task : schedule) {
			schedule_id = task.get("schedule_id", 0L);
			upRow = new DBRow();
			upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
			upRow.add("is_task_finish", 1);
			upRow.add("schedule_finish_adid", adid);
			floorReceiptsMgrWfh.closeScheduleSub(task.get("schedule_sub_id", 0L), upRow);
		}
		String closeTaskDate = DateUtil.NowStr();
		upRow = new DBRow();
		upRow.add("end_time", closeTaskDate);
		upRow.add("schedule_state", ScheduleFinishKey.ScheduleFinish);
		floorReceiptsMgrWfh.closeSchedule(schedule_id, upRow);
		
		return log.toString();
	}
	
	/**
	 * 查找这个supervisor的ccTask
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/findSupervisorTaskList", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findSupervisorTaskList(
			@RequestParam(value = "adid",required = false,defaultValue="0") long adid
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow[] tasks = floorReceiptsMgrWfh.findTaskAndroid(CCTASK, CCTASK_SUPERVISOR, adid);
			for(DBRow r :tasks){
				r.add("normal_qty", r.get("goods_total",0));
				if(r.get("damage_total",0)==0){
					r.add("damage_total", 0);
				}
				r.add("damage_qty", r.get("damage_total",0));
			}
			result.add("data", tasks);
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new Exception(e);
		}

		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	/**
	 * 查找这个scaner的scanTask
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/findScanTaskList", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findScanTaskList(
			@RequestParam(value = "adid",required =false,defaultValue="0") long adid
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow[] tasks = floorReceiptsMgrWfh.findTaskAndroid(CCTASK, CCTASK_SCANER, adid);
			for(DBRow row :tasks){
				DBRow[] qtys = floorReceiptsMgrZyy.getPalletQtyByLineId(row.get("receipt_line_id", 0L));
//				
//				if(null!=qtys && qtys.length>0){
//					for(DBRow r:qtys){
//						int container_type  = r.get("container_type", 0);
//						if(container_type==1){
//							//clp 的数量
//							
//						}else if(container_type==3){
//							//tlp 的数量
//							
//						}
//					}
//				}
				
				int countQTY = floorReceiptsMgrZyy.getCountQTYByLineId(row.get("receipt_line_id", 0L));
				int ccQTY = floorReceiptsMgrZyy.getCCPalletQtyByLineId(row.get("receipt_line_id", 0L));
				row.add("clp_expected_qty",  ccQTY);
				row.add("tlp_expected_qty",  countQTY);

				int hasbreakQty =0;
				DBRow[] noBreakPallet =null;
				if(row.get("clp_expected_qty",0)==0){
					noBreakPallet = new DBRow[0];
				}else{
					hasbreakQty = floorReceiptsMgrZyy.getBreakPalletQtyByLineId(row.get("receipt_line_id", 0L));
					noBreakPallet = floorReceiptsMgrZyy.findNoBreakPalletByLineId(row.get("receipt_line_id", 0L), 0, ReceiptForAndroidController.GOODS_TYPE_NORMAL);
				}
				
				
				
				//添加go_to_scan 判断是否 可以进入第扫描
				if(row.get("clp_expected_qty", 0)==hasbreakQty || null==noBreakPallet || noBreakPallet.length==0){
					row.add("go_to_scan", true);
				}else{
					row.add("go_to_scan", false);
				}
				DBRow location = floorReceiptsMgrWfh.findLocationByLineId(row.get("receipt_line_id", 0L));
				row.add("location",  location.get("location",""));
				row.add("scaned_sn", floorReceiptsMgrWfh.findSNCountByLineId(row.get("receipt_line_id", 0L)).get("sn_count", 0));
				
				DBRow[] goods_sn_size = floorReceiptsMgrWfh.findSNByLineId(row.get("receipt_line_id", 0L), 0,0);
				
				
				DBRow[] goodsDa_sn_size = floorReceiptsMgrWfh.findSNByLineId(row.get("receipt_line_id", 0L), 0,1);
				
				DBRow[] damage_sn_size = floorReceiptsMgrWfh.findSNByLineId(row.get("receipt_line_id", 0L), 2,3);
				
				row.put("sn_normal_qty", goods_sn_size.length);
				row.put("sn_damage_qty", damage_sn_size.length + goodsDa_sn_size.length);
				if(row.get("damage_total",0)==0){
					row.add("damage_total", 0);
				}
				row.add("normal_qty",row.get("goods_total",0));
				row.add("damage_qty", row.get("damage_total",0));
				row.add("already_breark_qty", hasbreakQty);
			}
			result.add("data", tasks);
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new Exception(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	/**
	 * 查找这个scanTask的detail
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/findScanTaskDetail", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String findScanTaskDetail(
			@RequestParam(value = "receipt_line_id",required = false,defaultValue="0") long receipt_line_id
			)
			throws Exception {
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try {
			DBRow line = floorReceiptsMgrWfh.findReceiptLineByLineId(receipt_line_id);
			line.add("scaned_sn", floorReceiptsMgrWfh.findSNCountByLineId(receipt_line_id).get("sn_count", 0));
			result.add("data", line);
		} catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new Exception(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	/**
	 * 
	 * @param receipt_line_id
	 * @param type 类型 ，1 表示 CC 0、表示 scan 
	 * @param adid
	 * @param session
	 * @return
	 * @throws Exception
	 */
	@Transactional
	@RequestMapping(value = "/android/joinSchedule", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String joinSchedule(
			@RequestParam(value = "receipt_line_id",required = false,defaultValue="0") long receipt_line_id,
			@RequestParam(value = "type") int type, 
			@RequestParam(value = "adid") long adid, HttpSession session ) throws Exception {
		
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		//为了方便处理 采用 CCTASK_SCANER-type 来确定 是CC 还 是 scan
		try{
			DBRow line = floorReceiptsMgrWfh.findReceiptByLineId(receipt_line_id);
			if(null== line ){
				throw new ReceiveForAndroidException("No find line: "+receipt_line_id);
			}
			if(line.get("is_check_sn", 0)==1 && line.get("status", 0)==LineStatueKey.STATUS_CLOSE){
				//需要扫 SN 但 改line 以及关闭 怎么处理
			}
			if(line.get("is_check_sn", 0)==0 && line.get("status", 0)==LineStatueKey.STATUS_CLOSE){
				//TODO 不需要扫描SN时
				DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK, CCTASK_SCANER-type, receipt_line_id);
				//如果查询不到
				if(type==0){
					assignScanTask(line.get("receipt_id", 0l), receipt_line_id, ""+adid, adid, line.get("ps_id", 0l), session);
				}else{
					// 创建CC任务时，要不要加上 权限验证
					createCCAndScanTask(receipt_line_id, ""+adid, adid, line.get("ps_id", 0l), CCTASK_SUPERVISOR, session);
//					assignCcTaskToSupervisor(result, receipt_line_id, operator, adid, ps_id, receipt_id, session);
				}
			}
			
			DBRow schedule[] = floorReceiptsMgrWfh.findSchedule(CCTASK, CCTASK_SCANER-type, receipt_line_id);
			if(null!= schedule && schedule.length>0){
				if(schedule.length==1){
					floorReceiptsMgrZyy.joinExistedSchedule(adid, schedule[0].get("schedule_id",0));
					result.add("data", "true");
				}else{
					throw new ReceiveForAndroidException("More schedule ");
				}
			}
			
		}catch( ReceiveForAndroidException e){
			ret = BCSKeyReceive.FAIL;
			err = VAILDATIONERROR;
			result.add("data", e.getMessage());
		}catch (Exception e) {
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	
	
	private int strArrayToTotalNum(JSONArray data) throws JSONException{
		int tmp = 0; 
		for(int i= 0,j = data.length(); i<j;i++){
			JSONObject json  = data.getJSONObject(i);
			tmp+=json.getInt("qty");
		}
		return tmp;
	}
	private int strToInt(String str){
		if(null==str || "".equals(str) ||!isTurnIntOrLong(str)){
			return 0;
		}
		return Integer.valueOf(str);
	}
	private  boolean isTurnIntOrLong(String param){
		Pattern p = Pattern.compile("^[-]?\\d+$");// 不限制号段
		Matcher m = p.matcher(param);
		return m.matches();
	}
	
	

	@Transactional
	@RequestMapping(value = "/android/getAllContainerType", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public String getAllContainerType() throws Exception{
		DBRow result = new DBRow();
		int ret = BCSKeyReceive.SUCCESS;
		int err = BCSKeyReceive.FAIL;
		try{
			DBRow[] containerTypes = floorContainerMgrZYZ.getAllContainerType(null, null);
			result.add("data",containerTypes) ;
			
		}catch(Exception e){
			ret = BCSKeyReceive.FAIL;
			err = BCSKeyReceive.SYSTEMERROR;
			throw new RuntimeException(e);
		}
		result.add("ret", ret);
		result.add("err", err);
		return CommonUtils.convertDBRowsToJsonString(result);
	}
	
	
	
	private boolean noticeToOperator(long adid, String operator, DBRow schedule,String noticeURL,
			HttpSession session) throws IllegalArgumentException, Exception {
		int statue = 0;
		try{
			String sessionID =session.getId();
			HttpClient client = new HttpClient();
			PostMethod post = new PostMethod(noticeURL);
			post.setRequestHeader("Cookie", "JSESSIONID="+sessionID);
			post.addParameter("schedule", CommonUtils.convertDBRowsToJsonString(schedule));
			post.addParameter("assignTo", operator);
			post.addParameter("adid", String.valueOf(adid));
			statue = client.executeMethod(post);
		}catch(Exception e){
		}
//		String sessionID =session.getId();
//		HttpClient client = new HttpClient();
//		PostMethod post = new PostMethod(noticeURL);
//		post.setRequestHeader("Cookie", "JSESSIONID="+sessionID);
//		post.addParameter("schedule", CommonUtils.convertDBRowsToJsonString(schedule));
//		post.addParameter("assignTo", operator);
//		post.addParameter("adid", String.valueOf(adid));
//		int statue = client.executeMethod(post);
		if(statue==200){
			return true;
		}else{
			throw new Exception();
		}
	}
}
