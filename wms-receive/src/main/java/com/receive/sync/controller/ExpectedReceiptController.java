package com.receive.sync.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.sbb.FloorReceiptMgrSbb;
import com.cwc.app.floor.api.sbb.FloorReceiptSqlServerMgrSbb;
import com.cwc.app.key.BCSKeyReceive;
import com.cwc.db.DBRow;

@RestController
public class ExpectedReceiptController {
	
	@Autowired
	private FloorReceiptMgrSbb floorReceiptMgr;
	
	@Autowired
	private FloorReceiptSqlServerMgrSbb floorReceiptSqlServerMgr;
	
	@RequestMapping(value = "/product/data", produces = "application/json;charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> productData (
			
			@RequestParam(value = "companyId", required = true, defaultValue = "") String companyId,
			@RequestParam(value = "customerId", required = true, defaultValue = "") String customerId,
			@RequestParam(value = "itemId", required = true, defaultValue = "") String itemId
			
			) throws Exception {
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		//参数
		DBRow customer = new DBRow();
		customer.put("companyId", companyId);
		customer.put("customerId", customerId);
		customer.put("itemId", itemId);
		
		if(!companyId.equals("") && !customerId.equals("") && !itemId.equals("")){
			
			DBRow customerItem = floorReceiptSqlServerMgr.getCustomerItemsForProduct(customer);
			
			if(customerItem != null){
				
				result.put("product",customerItem);
				result.put("ret",BCSKeyReceive.SUCCESS);
				result.put("err",0);
				
			} else {
				
				result.put("data"," product not exists");
				result.put("ret",BCSKeyReceive.FAIL);
				result.put("err",90);
			}
			
		} else {
			
			result.put("data"," parameter error ");
			result.put("ret",BCSKeyReceive.FAIL);
			result.put("err",90);
		}
		
		return result;
	}
}