wms-receive [收货]	
============================	

- **统一的编码规范**

		/**
		 * 收货:同步预计收货主单据和明细
		 * @param companyId,receiptNo
		 * @return 同步后的主单据和明细
		 * @author subin
		 * */
		@Transactional
		@RequestMapping(value = "/receive/syncExpectedReceiptAndLines", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
		public Map<String,Object> synchronizationExpectedReceiptAndLines(
			
				@RequestBody Map<String, String> parameter
			) throws Exception {
			
			//参数
			DBRow paramReceipt = new DBRow();
			paramReceipt.add("companyId", parameter.get("companyId"));
			paramReceipt.add("receiptNo", parameter.get("receiptNo"));
			
			//查询SqlServer预计收货主单据
			DBRow oldReceiptRow = floorInboundMgr.getOldExpectedReceipt(paramReceipt);
			
			//查询SqlServer预计收货明细
			DBRow[] oldReceiptLinesRow = floorInboundMgr.getOldExpectedReceiptLines(paramReceipt);
			
			//同步到MySql预计收货表
			this.updateExpectedReceiptAndLine(paramReceipt,oldReceiptRow,oldReceiptLinesRow);
			
			Map<String,Object> result = new HashMap<String,Object>();
			result.put("receipt", oldReceiptRow);
			result.put("receiptLines", oldReceiptLinesRow);
			
			return result;
		}

- **统一URL**

		@RequestMapping(value = "/receive/getExpectedReceiptAndLines", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)

- **使用自定义配置文件和自定义包，尽量避免代码冲突**

		//建议:不要使用名字区别，可以使用模块功能区别，如果要标识开发者，可以在注释里表示.
		applicationContext_user.xml
		com.micro.inbound.actual


- **使用Junit做测试，提高正确性和开发效率**

		@Test
		public void test() throws Exception {
			
			Map<String, String> parameter = new HashMap<String, String>();
			parameter.put("companyId", "W15");
			parameter.put("receiptNo", "2388");
			
			
			Map<String,Object> map = resource.getExpectedReceiptAndLines(parameter);
			assertTrue(map.get("receipt")!=null);
		}

