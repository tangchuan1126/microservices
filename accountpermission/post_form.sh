#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ] || [ ! -f "$2" ] || [ -z "$3" ]
then
	echo "USAGE:  post_form.sh <JSESSIONID Value> <form file path> <target URL>"
	exit 1
fi
curl -v \
-b "JSESSIONID=$1" \
--request POST \
--header "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" \
--data @$2 \
$3
echo