#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ] || [ ! -f "$2" ] || [ -z "$3" ]
then
	echo "USAGE:  upload_file.sh <JSESSIONID Value> <file path> <url>"
	exit 1
fi
curl -v \
-b "JSESSIONID=$1" \
-F "file=@$2" \
"$3"
echo

