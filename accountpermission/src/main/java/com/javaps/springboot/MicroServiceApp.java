package com.javaps.springboot;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Context;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
//import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.radiadesign.catalina.session.RedisSessionHandlerValve;
import com.radiadesign.catalina.session.RedisSessionManager;

@Configuration
@ComponentScan({"com.javaps.springboot","com.oso.auth"})
@EnableAutoConfiguration
@ImportResource("classpath:applicationContext.xml")
//@EnableMongoRepositories(basePackages="com.gis.model")
public class MicroServiceApp implements Filter {
	@Value("${redis.host}")
	private String redis_host;
	
	@Value("${session.maxInactiveInterval.web}")
	private int session_maxInactiveInterval_web;
	
	@Value("${session.maxInactiveInterval.mobile}")
	private int session_maxInactiveInterval_mobile;
	
	@Value("${session.timeout}")
	private int session_timeout;
	
	@Value("${config.path}")
	private String configPath;
	
	@Value("${spring.data.mongodb.host}")
	private String mongoHost;
	
	@Value("${spring.data.mongodb.username}")
	private String mongoUsername;
	
	@Value("${spring.data.mongodb.password_plain}")
	private String mongoPassword;
	
	
	private RedisSessionManager rsm = new RedisSessionManager();
	

	 public @Bean MongoDbFactory mongoDbFactory() throws Exception {
	    UserCredentials userCredentials = new UserCredentials(mongoUsername, mongoPassword);
	    return new SimpleMongoDbFactory(new MongoClient(new ServerAddress(mongoHost)), "gps", userCredentials);
	  }
	
	  public @Bean MongoTemplate mongoTemplate() throws Exception {
	    return new MongoTemplate(mongoDbFactory());
	  }
	
	class CustomHttpServletRequest extends HttpServletRequestWrapper {
		private HttpSession session;
		
		CustomHttpServletRequest(HttpServletRequest req, HttpSession sess){
			super(req);
			this.session = sess;
		}
		
		@Override
		public HttpSession getSession(boolean create) {
			return session;
		}
		@Override
		public HttpSession getSession() {
			return session;
		}
		
	}

	public static void main(String[] args) {
		ConfigBean.putBeans("admin", "admin");
		DBRow.setKeyUseUpper(false); 
		DBRow.setKeyIgnoreCase(true);
		SpringApplication.run(MicroServiceApp.class, args);
	}

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
		RedisSessionHandlerValve rshv = new RedisSessionHandlerValve();

		factory.addContextValves(rshv);
		factory.addContextCustomizers(new TomcatContextCustomizer() {

			@Override
			public void customize(Context context) {
				
				rsm.setHost(redis_host);
				rsm.setPort(6379);
				rsm.setDatabase(0);
				rsm.setMaxInactiveInterval(session_maxInactiveInterval_web);
				rsm.setSerializationStrategyClass("com.radiadesign.catalina.session.JsonSerializer");
				context.setManager(rsm);
				context.setSessionCookiePath("/Sync10");
				context.setSessionCookiePathUsesTrailingSlash(true);
				context.setSessionTimeout(session_timeout);
				
				putBeans(configPath);
			}

		});

		return factory;
	}
	
	@Bean
	public FilterRegistrationBean authFilter() {
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(this);
		List<String> urlPatterns = new ArrayList<String>();
		urlPatterns.add("/*");
		filterRegBean.setUrlPatterns(urlPatterns);
		return filterRegBean;
	}

	// Filter implementation
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		res.setCharacterEncoding("UTF-8");
		
		HttpSession sess = req.getSession(false);
		
		if(sess == null) sess = getSessionByHeader(req);
			
		Map<String, Object> m = sess == null ? null
				: (Map<String, Object>) sess.getAttribute("adminSesion");
		

		if (!req.getRequestURI().startsWith("/public/") && 
			( m == null || !Boolean.TRUE.equals(m.get("login")))) {
			m = new HashMap<String, Object>();
			m.put("error", "未认证的访问");
			res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);	
			res.setContentType("application/json; charset=UTF-8");
			new ObjectMapper().writeValue(res.getWriter(), m);
			return;
		}

		chain.doFilter(new CustomHttpServletRequest(req,sess), response);
	}
	
	private HttpSession getSessionByHeader(HttpServletRequest req) throws IOException {
		String sessionId = req.getHeader("JSESSIONID");
		System.out.println(sessionId);
		if(sessionId==null) return null;
		HttpSession sess = rsm.loadSessionFromRedis(sessionId);
		//注意：以下这个设置虽然也有效，但其实不应该在这里做，
		//而是应该在移动设备登录接口内做，就是移动设备登录成功后，返回令牌之前，做此设置
		//因为本微服务并不是认证服务接口，所以仅在此示意
		if(sess!=null) {
			System.out.println(sess.getAttributeNames());
			sess.setMaxInactiveInterval(this.session_maxInactiveInterval_mobile);
		}
		
		return sess;
	}

	@Override
	public void destroy() {
	}
	
	/**
	 * 充填ConfigBean的数据库表字段
	 * @param urlMppingPath
	 */
	public static void putBeans(String urlMppingPath)
	{

		InputStream is=MicroServiceApp.class.getResourceAsStream(urlMppingPath);

		SAXReader reader = new SAXReader();
		Document document = null;
		try 
		{
			document = reader.read(is);
		}
		catch (DocumentException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Element root = document.getRootElement();
		
		String tagName = null;
		String tagValue = null;
		
		ArrayList elementAl = new ArrayList();
		
		for(Iterator i = root.elementIterator();i.hasNext(); )
		{
			Element element = (Element) i.next();
			
			tagName = element.attributeValue("name");
			tagValue = element.attributeValue("value");
			
			if (ConfigBean.getStringValue(tagName)==null)
			{
				ConfigBean.putBeans(tagName,tagValue);						
			}
	    }
	}
}

