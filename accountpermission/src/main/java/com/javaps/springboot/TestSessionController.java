package com.javaps.springboot;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorAdminMgr;
import com.cwc.app.util.Config;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;

@RestController
@Transactional
public class TestSessionController {
	@Autowired
	private FloorAdminMgr floorAdminMgr;
	@Value("${config.path}")
	private String configPath;

	@RequestMapping(value = "/", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> readSessionAttrs(
			@SessionAttr(Config.adminSesion) Map<String,Object> alb,
			@SessionAttr("loginlicenceSesion") String lls
			) throws Exception {
		System.out.println(lls);
		return alb ;
	}
	
	//发送文件示例
	@RequestMapping(value = "/public/file_access_demo", produces = "text/xml; charset=UTF-8", method = RequestMethod.GET)
	public void xmlFileAccess(HttpServletResponse res) throws Exception {
		InputStream is = this.getClass().getResourceAsStream(configPath);
		
		IOUtils.copy(is, res.getOutputStream());
		
		is.close();
	}

	@RequestMapping(value = "/", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> writeSessionAttrs(HttpServletRequest req,
			@RequestBody Map<String, Object> data) throws Exception {
		HttpSession sess = req.getSession(false);
		for (String k : data.keySet()) {
			sess.setAttribute(k, data.get(k));
		}

		return data;
	}
	@RequestMapping(value="/origin_uri", produces = "text/plain; charset=UTF-8", method = RequestMethod.GET)
	public String originUri(HttpServletRequest req) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append(req.getHeader("X-Origin-Uri")).append("\n").append(req.getRequestURI());
		return sb.toString();
	}
	
	@RequestMapping(value = "/admin/{adid}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> getAdmin(@PathVariable("adid") long adid) throws Exception {
		return floorAdminMgr.getDetailAdmin(adid);
	}
	
	@RequestMapping(value = "/tx", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow[] testTx() throws Exception {
		System.out.println(ConfigBean.getStringValue("admin"));
		DBRow admin1 = new DBRow();
		admin1.add("account", "test_admin_1");
		admin1.add("email", "test_admin_1@vvme.com");
		long result= floorAdminMgr.addAdmin(admin1);
		
		//if(result >=0 ) throw new Exception("TEST Rollback");
		DBRow admin2 = new DBRow();
		admin2.add("account", "test_admin_2");
		admin2.add("email", "test_admin_2@vvme.com");
		if(true) throw new Exception("TEST Rollback");
		floorAdminMgr.addAdmin(admin2);
		PageCtrl pc = new PageCtrl();
		pc.setPageSize(1000);
		pc.setPageNo(1);
		return floorAdminMgr.getAllAdmin(pc);
	}
}
