package com.oso.auth.controller;


import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.PageCtrl;
import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Node;
import com.oso.auth.beans.Resource;
import com.oso.auth.service.MenuService;
import com.oso.auth.service.ResourceService;
import com.oso.auth.util.NodeUtil;

/**
 * 
 * @author lujintao
 *
 */
@RestController
public class ResourceController  {
	@Autowired
	private ResourceService resourceService;
	@Autowired
	private MenuService menuService;
	
	/**
	 * 添加菜单
	 * @param resource
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> add(@RequestBody  Resource resource) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> error_map = this.validateInput(resource,false);
		if(error_map.size() > 0){
			map.put("errors", error_map);
			map.put("success", false);
		}else{
			int newId = this.resourceService.add(resource);
			boolean flag = (newId > 0) ? true : false;
			if(!flag){
				error_map.put("server", "Error ocurs in server");
				map.put("errors", error_map);
			}else{
				map.put("success", flag);	
				map.put("id", newId);
			}
		}
		return map;
	}
	
	/**
	 * 修改菜单
	 * @param resource
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> update(@RequestBody Resource resource,@PathVariable("id")int id) throws Exception{
		resource.setId(id);
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> error_map = this.validateInput(resource,true);
		boolean flag = false;
		if(error_map.size() == 0){
		    flag = this.resourceService.update(resource);
			if(!flag){
				error_map.put("server", "Error occured in server");
			}
		}
		map.put("errors", error_map);
		map.put("success", flag);
		return map;
	}

	/**
	 * 修改菜单
	 * @param resource
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource/status", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> changeStatus(@RequestBody Resource resource) throws Exception{
		
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag = this.resourceService.changeStatus(resource);
		map.put("success", flag);
		return map;
		
	}
	
	/**
	 * 删除菜单
	 * @param resource
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public Map<String,Object> deleteresource(@PathVariable("id") long id) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		boolean flag = true;
		//判断是否允许被删除
		if(this.resourceService.canBeDelete(id)){
			 flag = this.resourceService.delete(id);
			 if(!flag){
				 result.put("error","Error occurs in server");
			 }
			
		}else{
			flag = false;
			result.put("error", "This resource can't be deleted");
		}
		
		result.put("success", flag);
		return result;
	}

	/**
	 * 获得单个菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Resource getResource(@PathVariable("id") int id) throws Exception{
		return this.resourceService.get(id);
	}
	
	/**
	 * 判断在parentId 指定的菜单下是否存在特定的菜单；如果请求参数中id不为空，判断是否存在名为title,但id不等于给定id的菜单；否则，判断是否存在 名为title的菜单
	 * @param parentId  父菜单ID
	 * @param title     菜单名称
	 * @return 存在返回false;不存在返回true;
	 */
	@RequestMapping(value = "/resource/exist/menu/{menuId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> exists(@PathVariable("menuId")int menuId,HttpServletRequest request){
		Map<String,Object> map = new HashMap<String,Object>();
		int id = 0;
		String id_str = request.getParameter("id");
		String description = request.getParameter("description");
		String url = request.getParameter("url");
		String method = request.getParameter("method");
		if(id_str != null && id_str.trim().length() > 0){
			id = Integer.parseInt(id_str);
		}
		if(description != null && description.trim().length() > 0){
			map.put("success", !this.resourceService.exists(menuId, id,description));
		}else{
			if(url == null || url.trim().equals("") || method == null || method.trim().equals("")){
				map.put("success", true);
			}else{
				map.put("success", !this.resourceService.exists(menuId, id,url,method));
			}
		}
		return map;
	}
	
	/**
	 * 获得指定菜单下,并满足特定条件的资源
	 * @param resource
	 * @throws Exception
	 */
	@RequestMapping(value = "/resource/menu/{menuId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public List<Resource> getResources(@PathVariable("menuId") int menuId, HttpServletRequest request) throws Exception{
		Menu menu = new Menu();
		menu.setId(menuId);
		PageCtrl page = new PageCtrl();
		String pageSize = request.getParameter("pageSize");
		String pageNo = request.getParameter("pageNo");
		if(pageSize != null && pageSize.trim().length() > 0){
			page.setPageSize(Integer.parseInt(pageSize));
			page.setPageNo(Integer.parseInt(pageNo));
		}
		Map<String,Object> conditions = new HashMap<String,Object>();
		String auth = request.getParameter("auth");
		String keyword = request.getParameter("keyword");
		conditions.put("auth", auth);
		conditions.put("keyword",keyword);
		return this.resourceService.getResources(menu, conditions, page);
	}

	

	/**
	 * 对输入数据进行校验
	 * @param resource
	 * @return
	 */
	private Map<String,Object> validateInput(Resource resource,boolean update){
		Map<String,Object> error_map = new HashMap<String,Object>();
		if(resource != null){
			if(update){
				if(resource.getId() <= 0){
					error_map.put("id", "Id must be rendered");
				}				
			}
			if(resource.getDescription() == null || resource.getDescription().trim().length() == 0 ){
				error_map.put("description", "Enter description");
			}else{
				if(this.resourceService.exists(resource.getPage(),resource.getId(),resource.getDescription())){
					error_map.put("description", "Resource already exists");
				}
			}
			if(resource.getPage() == 0){
				error_map.put("menu", "Menu must be rendered");
			}
			if(resource.getMethod() == null || resource.getMethod().trim().length() == 0){
				error_map.put("method", "Please select method");
			}
			if(resource.getUrl() == null || resource.getUrl().trim().length() == 0){
				error_map.put("url", "Enter uri");
			}
			if(resource.getMethod() != null && resource.getMethod().trim().length() > 0 && resource.getUrl() != null && resource.getUrl().trim().length() > 0){
				if(this.resourceService.exists(resource.getPage(), resource.getId() , resource.getUrl(), resource.getMethod())){
					error_map.put("method", "Resource with same uri and method already exists");
				}
			}
			
		}
		return error_map;
	}


}
