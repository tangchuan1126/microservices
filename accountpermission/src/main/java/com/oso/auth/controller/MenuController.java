package com.oso.auth.controller;

import java.net.URLConnection;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Node;
import com.oso.auth.service.MenuService;
import com.oso.auth.service.impl.MenuServiceImpl;
import com.oso.auth.util.NodeUtil;

/**
 * 
 * @author lujintao
 *
 */
@RestController
public class MenuController  {
	@Autowired
	private MenuService menuService;
	
	
	/**
	 * 添加菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addMenu(@RequestBody  Menu menu) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> error_map = this.validateInput(menu,false);
		if(error_map.size() > 0){
			map.put("errors", error_map);
			map.put("success", false);
			
		}else{
			int newId = this.menuService.add(menu);
			boolean flag = (newId > 0) ? true : false;
			if(!flag){
				error_map.put("server", "Error ocurs in server");
				map.put("errors", error_map);
			}else{
				map.put("success", flag);
				map.put("id", newId);
				Node node = NodeUtil.toNode(menu);
				map.put("icon", node.getIcon());
				map.put("type", node.getType());
				map.put("level", node.getLevel());
				map.put("others", node.getOthers());				
			}
		}
		return map;
	}
	
	/**
	 * 修改菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> updateMenu(@RequestBody Menu menu,@PathVariable("id")int id) throws Exception{
		menu.setId(id);
		Map<String,Object> map = new HashMap<String,Object>();
		Map<String,Object> error_map = this.validateInput(menu,true);
		boolean flag = false;
		if(error_map.size() > 0){
			map.put("errors", error_map);
			map.put("success", false);
		}else{
		
		    flag = this.menuService.update(menu);
		    if(!flag){
		    	error_map.put("server", "Error occured in server");
		    }
		}
		map.put("errors", error_map);
		map.put("success", flag);
		return map;
	}

	/**
	 * 修改菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu/status", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> changeStatus(@RequestBody Menu menu) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag = this.menuService.changeStatus(menu);
		map.put("success", flag);
		return map;
	}

	/**
	 * 改变顺序 ，将id对应的菜单移动到anotherId对应的菜单后面
	 * @param id 
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu/order/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.PUT)
	public Map<String,Object> changeOrder(@PathVariable("id") int id,@RequestParam(value="oldParentId", defaultValue="-1") int oldParentId,
			@RequestParam("parentId") int parentId,@RequestParam("position") int position,@RequestParam("oldPosition") int oldPosition) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		boolean flag = this.menuService.changeOrder(id,oldParentId,parentId,oldPosition,position);
		map.put("success", flag);
		return map;
	}
	
	/**
	 * 删除菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.DELETE)
	public Map<String,Object> deleteMenu(@PathVariable("id") long id) throws Exception{
		Map<String,Object> result = new HashMap<String,Object>();
		boolean flag = true;
		//判断是否允许被删除
		if(this.menuService.canBeDelete(id)){
			 flag = this.menuService.delete(id);
			 if(!flag){
				 result.put("error","Error occurs in server");
			 }
			
		}else{
			flag = false;
			result.put("error", "This menu can't be deleted");
		}
		
		result.put("success", flag);
		return result;
	}

	/**
	 * 获得单个菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu/{id}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Menu getMenu(@PathVariable("id") long id) throws Exception{
		return this.menuService.get(id);
	}
	
	/**
	 * 获得所有菜单
	 * @param menu
	 * @throws Exception
	 */
	@RequestMapping(value = "/menu", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Node getAllMenu() throws Exception{
		return this.menuService.createMenuTree();
	}
	
	/**
	 * 判断在parentId 指定的菜单下是否存在特定的菜单；如果请求参数中id不为空，判断是否存在名为title,但id不等于给定id的菜单；否则，判断是否存在 名为title的菜单
	 * @param parentId  父菜单ID
	 * @param title     菜单名称
	 * @return 存在返回false;不存在返回true;
	 */
	@RequestMapping(value = "/menu/exist/{parentId}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> exists(@PathVariable("parentId")int parentId,@RequestParam("title")String title,HttpServletRequest request){
		Map<String,Object> map = new HashMap<String,Object>();
		int id = 0;
		int controlType = 0;
		String id_str = request.getParameter("id");
		String controlType_str = request.getParameter("controlType");
		if(id_str != null && id_str.trim().length() > 0){
			id = Integer.parseInt(id_str);
		}
		if(controlType_str != null && controlType_str.trim().length() > 0){
			controlType = Integer.parseInt(controlType_str);
		}
		map.put("success", !this.menuService.exists(parentId, id,title,controlType));
		return map;
	}

	/**
	 * 对输入数据进行校验
	 * @param menu
	 * @return
	 */
	private Map<String,Object> validateInput(Menu menu,boolean update){
		Map<String,Object> error_map = new HashMap<String,Object>();
		if(menu != null){
			if(update){
				if(menu.getId() <= 0){
					error_map.put("id", "Id must be rendered");
				}				
			}
			if(menu.getText() == null || menu.getText().trim().length() == 0 ){
				error_map.put("title", "Enter title");
			}else{
				if(this.menuService.exists(menu.getParentId(),menu.getId(), menu.getText(),menu.getControlType())){
					error_map.put("title", "Menu already exists");
				}
			}
			
			
			if(menu.getParentId() > 0){
				if(menu.getLink() == null || menu.getLink().trim().length() == 0){
					error_map.put("link", "Enter uri");
				}
			}
			
		}
		return error_map;
	}


}
