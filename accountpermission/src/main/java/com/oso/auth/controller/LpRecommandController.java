package com.oso.auth.controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.key.WeightUOMKey;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.oso.auth.iface.LpRecommandControllerIFace;

@RestController
public class LpRecommandController implements LpRecommandControllerIFace {
	
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 功能：获取所需clp及数量
	 * @param title
	 * @param shipToid
	 * @param pc_id
	 * @param orderQty
	 * @param pallets
	 * @return
	 */	
	@Override
	public DBRow getPoundPerPackage(@RequestParam(value="title", defaultValue="0") long title, @RequestParam(value="shipToid", defaultValue="0") long shipToid, @RequestParam(value="pc_id", defaultValue="-1") long pc_id, @RequestParam(value="orderQty", defaultValue="-1") int orderQty, @RequestParam(value="pallets", defaultValue="-1") float pallets){
		DBRow recommand = new DBRow();
		
		//托盘数不能为小数 
		if(pallets!=-1 && pallets%1==0 && orderQty!=-1 && pc_id!=-1){
			
			StringBuffer sqlBuffer = new StringBuffer();
			
			sqlBuffer.append("SELECT p.pc_id AS pc_id, ")
					 .append("       p.weight AS p_weight, ")
					 .append("       p.weight AS p_weight, ")
					 .append("       lp.lpt_id AS clp_type_id, ")
					 .append("       lp.lp_name AS clp_type_name, ")
					 .append("       lp.total_weight AS clp_type_weight, ")
					 .append("       lp.weight_uom AS weight_uom, ")
					 .append("       lp.inner_total_pc AS pc_qty, ")
					 .append("       lp.ibt_height AS clp_packaging_weight ")
					 .append("	FROM lp_title_ship_to ltst ")
					 .append("	LEFT JOIN license_plate_type lp ON lp.pc_id = ltst.lp_pc_id ")
					 .append("	LEFT JOIN product p ON p.pc_id = ltst.lp_pc_id ")
					 .append(" WHERE lp.active = 1 ")
					 .append("	 AND lp.inner_total_pc * "+ pallets +" = "+ orderQty)
					 .append("	 AND ltst.lp_pc_id = "+ pc_id)
					 .append("	 AND ltst.title_id = ?")
					 .append("	 AND ltst.ship_to_id = ?")
					 .append(" ORDER BY ltst.lp_title_ship_id")
					 .append(" LIMIT 1");
			
			DBRow params = new DBRow();
			params.add("title_id", title);
			params.add("ship_to_id", shipToid);
			
			try {
				DBRow[] recommands = dbUtilAutoTran.selectPreMutliple(sqlBuffer.toString(), params);
				if(recommands!=null && recommands.length>=1){
					recommand = calculateAndRecommandData(recommands, orderQty, pallets);
				}
				
				//若title或shipto不为空，且没有找到合适的容器，重新查询title和shipto都为空
				if((recommand==null || recommand.isEmpty()) && (title!=0 || shipToid!=0)){
					params.clear();
					params.add("title_id", 0);
					params.add("ship_to_id", 0);
					
					recommands = dbUtilAutoTran.selectPreMutliple(sqlBuffer.toString(), params);
					if(recommands!=null && recommands.length>=1){
						recommand = calculateAndRecommandData(recommands, orderQty, pallets);
					}
				}
			} catch (SQLException e) {
				recommand = new DBRow();
			}			
		}
		
		return recommand;
	}
	
	/**
	 * 功能：计算并生成推荐结果
	 * @param recommands
	 * @return DBRow
	 */
	private DBRow calculateAndRecommandData(DBRow[] recommands, int orderQty, float pallets){
		DBRow recommand = new DBRow();
		
		if(recommands!=null && recommands.length>=1){
			DBRow first = recommands[0];
			
			WeightUOMKey weightUOMKey = new WeightUOMKey();
			
			recommand.add("pc_id", first.get("pc_id", 0L));
			recommand.add("p_weight", first.get("p_weight", 0F));
			recommand.add("total_weight", first.get("clp_type_weight", 0F) * pallets);
			
			DBRow clpType = new DBRow();
			clpType.add("clp_type_id", first.get("clp_type_id", 0L));
			clpType.add("clp_type_name", first.getString("clp_type_name"));
			clpType.add("clp_type_weight", first.get("clp_type_weight", 0F));
			clpType.add("weight_uom", weightUOMKey.getWeightUOMKey(first.get("weight_uom", 0)));
			clpType.add("clp_qty", pallets);
			clpType.add("pc_qty", first.get("pc_qty", 0));
			clpType.add("clp_packaging_weight", first.get("clp_packaging_weight", 0F));
			
			DBRow[] clpTypes = new DBRow[]{clpType};
			
			recommand.add("clp_types", clpTypes);
		}
		
		return recommand;
	}
}
