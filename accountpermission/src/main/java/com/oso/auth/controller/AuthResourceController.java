package com.oso.auth.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.util.Config;
import com.cwc.db.DBRow;
import com.oso.auth.PermitMgr;
import com.oso.auth.iface.AuthResourceControllerIFace;

@RestController
public class AuthResourceController implements AuthResourceControllerIFace {
	
	@Autowired
	private PermitMgr permitMgr;
	
	@Autowired
	private HttpSession session;
	
	@Override
	public DBRow getUIPermsInfo(@RequestParam(value="model", defaultValue="") String model, @RequestParam(value="adid", defaultValue="-1") long adid) {
		DBRow permsInfo = null;
		
		String account = "";
		
		if(adid == -1){
			Map<String,Object>  adminSession = (Map<String,Object>) session.getAttribute(Config.adminSesion);
			
			if(adminSession != null){
				adid = (Integer) adminSession.get("adid");
				account = String.valueOf(adminSession.get("account"));
			}
		}
		
		try {
			permsInfo = permitMgr.getUIPermsInfo(model, adid);
			
			//设置系统管理员万能账号			
			if("admin".equals(account) && permsInfo!=null){
				Map<String,Object> ctrlPerm = (Map<String, Object>) permsInfo.get("ctrlPerm");
				permsInfo.remove("userPerm");
				permsInfo.add("userPerm", ctrlPerm);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			permsInfo = new DBRow();
		}
		
		return permsInfo;
	}

}
