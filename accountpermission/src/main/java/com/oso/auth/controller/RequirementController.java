package com.oso.auth.controller;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.oso.auth.iface.RequirementControllerIFace;

@RestController
public class RequirementController implements RequirementControllerIFace {
	
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	@Override
	public DBRow getAllRequirements(@RequestParam(value="titleId", defaultValue="-1") long titleId, @RequestParam(value="shipToId", defaultValue="-1") long shipToId, @RequestParam(value="riId", defaultValue="-1") long riId, @RequestParam(value="pageNo", defaultValue="1") int pageNo, @RequestParam(value="pageSize", defaultValue="10") int pageSize, @RequestParam(value="pageSize", defaultValue="") String searchConditions) {
		DBRow[] requirements;
		
		PageCtrl pc = new PageCtrl();
		pc.setPageNo(pageNo);
		pc.setPageSize(pageSize);
		
		Map<String,Object> parameter = new HashMap<String,Object>();
		parameter.put("PageCtrl", pc);
		parameter.put("searchConditions", searchConditions);
		
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT ri.ri_id, ")
				 .append("       ri.requirement_name, ")
				 .append("       ifnull(tscd.requirement_value_int, tscd.requirement_value_str) AS requirement_value, ")
				 .append("       ri.display_type, ")
				 .append("       ri.description ")
				 .append("  FROM requirement_item ri ")
				 .append("  LEFT JOIN title_ship_config_detail tscd ON tscd.ri_id = ri.ri_id");
		
		try {
			requirements = this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString(), pc);
		} catch (SQLException e) {
			e.printStackTrace();
			requirements = null;
		}
		
		if(requirements != null){
			for(DBRow x : requirements){
				String displayType = DisplayType.getDisplayType(x.get("display_type", 0));
				x.remove("display_type");
				x.add("display_type", displayType);
				
				//String required = Required.getRequired(x.get("requirement_value", 0));
				/*String requirement_value = x.getString("requirement_value");
				x.remove("requirement_value");*/
				
				//x.add("requirement_value", getDisplayValue(displayType, requirement_value));
			}
		}
		
		DBRow rs = new DBRow();
		rs.add("requirements", requirements);
		rs.add("pageCtrl", pc);
		
		return rs;
	}
	
	@Override
	public DBRow delRequirement(@PathVariable(value="riId") long riId){
		DBRow result = new DBRow();
		
		StringBuffer wherePart = new StringBuffer(" WHERE 1=1 ");
		wherePart.append(" AND ri_id = ? ");
		
		DBRow condparams = new DBRow();
		condparams.add("ri_id", riId);
		
		int execRs = 0;
		try {
			execRs = this.dbUtilAutoTran.deletePre(wherePart.toString(), condparams, "requirement_item");
		} catch (Exception e) {
			e.printStackTrace();
			execRs = 0;
		}
		
		result.add("ret", execRs==0 ? 0 : 1 );
		if(execRs == 0){
			result.add("error", "fail to delete ");
		} else {
			result.add("success", "true");
		}
		
		return result;
	}
	
	private DBRow getDisplayValue(String displayType, String requirement_value){
		
		DBRow requires = new DBRow();
		if(requirement_value ==null || requirement_value.indexOf(",")==-1){
			return requires;
		}
		
		String[] values = requirement_value.split(",");
		
		for(String value : values){
			if(DisplayType.DROP_LIST.getDisplayType().equals(displayType)
				|| DisplayType.CHECK_BOX.getDisplayType().equals(displayType)){
				requires.add(Required.getRequired(Integer.valueOf(value)), value);
			}
		}
		
		return requires;
	}
	
	enum DisplayType{
		
		TEXT_FIELD("TextField", 1), TEXT_AREA("TextArea", 2), RADIO("Radio", 3), CHECK_BOX("CheckBox", 4), DROP_LIST("DropList", 5);
		
		private int value;
		private String displayType;
		
	    private DisplayType(String displayType, int value) {
	        this.value = value;
	        this.displayType = displayType;
	    }
	    
	    public static String getDisplayType(int value) {  
	        for (DisplayType c : DisplayType.values()) {  
	            if (c.value == value) {  
	                return c.displayType;  
	            }  
	        }  
	        return "";  
	    }
	    
	    public static int getValue(String displayType) {  
	        for (DisplayType c : DisplayType.values()) {  
	            if (c.displayType.endsWith(displayType)) {  
	                return c.value;  
	            }  
	        }  
	        return -1;  
	    }
	    
	    public int getValue() {
	        return value;
	    }
	    
	    
	    public String getDisplayType(){
	    	return displayType;
	    }
	}
	
	enum Required{
		Y("Y", 1), N("N", 2);
		
		private int value;
		private String required;
		
	    private Required(String required, int value) {
	        this.value = value;
	        this.required = required;
	    }
	    
	    public static String getRequired(int value) {  
	        for (Required c : Required.values()) {  
	            if (c.value == value) {  
	                return c.required;  
	            }  
	        }  
	        return "";  
	    }
	    
	    public static int getValue(String required) {  
	        for (Required c : Required.values()) {  
	            if (c.required.equals(required)) {  
	                return c.value;  
	            }  
	        }  
	        return -1;  
	    }
	    
	    public int getValue() {
	        return value;
	    }
	    
	    public String getRequired(){
	    	return required;
	    }
	}
}
