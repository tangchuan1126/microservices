package com.oso.auth.iface;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.DBRow;

@RestController
@RequestMapping("/auth")
public interface AuthResourceControllerIFace {
	
	@RequestMapping(value = "/perms", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow getUIPermsInfo(@RequestParam(value="model") String model, @RequestParam(value="adid", defaultValue="-1") long adid);	
}
