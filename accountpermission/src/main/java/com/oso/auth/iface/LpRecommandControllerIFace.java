package com.oso.auth.iface;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.db.DBRow;

@RestController
@RequestMapping("/data")
public interface LpRecommandControllerIFace {
	
	/**
	 * @param title
	 * @param shipto
	 * @param pc_id
	 * @param quantities
	 * @param pallets
	 * @return
	 */
	@RequestMapping(value = "/lpRecommand", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public DBRow getPoundPerPackage(@RequestParam(value="title", defaultValue="0") long title, @RequestParam(value="shipToid", defaultValue="0") long shipToid, @RequestParam(value="pc_id", defaultValue="-1") long pc_id, @RequestParam(value="orderQty", defaultValue="-1") int orderQty, @RequestParam(value="pallets", defaultValue="-1") float pallets);	
}
