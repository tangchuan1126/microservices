package com.oso.auth;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.oso.auth.util.StringUtil;

@Service("permitMgr")
public class PermitMgr {
	
	@Autowired
	private DBUtilAutoTran dbUtilAutoTran;
	
	/**
	 * 功能：获取角色授权的Uri
	 * @param adgid 
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getRoleAuthResource(long adgid) throws Exception {
		HashMap<String, DBRow> roleAuthResourceMap = new HashMap<String, DBRow>();
		
		/*DBRow dept = new DBRow();
		dept.add("adgid", deptid);
		
		DBRow[] roleAuthUri = floorAdminMgr.findAdminAthAction(new DBRow[]{dept}, 0L, 0L, null);*/
		
		DBRow[] roleAuthResource = findRoleAuthResource(adgid);
		
		if(roleAuthResource != null){
			for(DBRow authResource : roleAuthResource){
				if(!StringUtil.isBlank(authResource.getString("action_uri"))){
					roleAuthResourceMap.put(authResource.getString("action_uri"), authResource);
				}
			}
		}
		
		return roleAuthResourceMap;
	}
	
	/**
	 * 功能：获取指定用户拥有角色的授权Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 * @since   Sync10-ui 1.0      
	 * @date    2015年3月16日
	 * @author  lixf
	 */
	public HashMap<String, DBRow> getUserRolesAuthResource(long adid) throws Exception {
		HashMap<String, DBRow> userRolesAuthResourceMap = new HashMap<String, DBRow>();
		
		/*DBRow[] userRoles = floorAdminMgr.findAdminRoleByAdid(adid);
		if(userRoles==null || userRoles.length<1){
			return userRolesAuthUriMap;
		}
		
		DBRow[] userRolesAuthResource = floorAdminMgr.findAdminAthAction(userRoles, 0L, 0L, null);*/
		
		DBRow[] userRolesAuthResource = findUserRolesAuthResource(adid);
		
		if(userRolesAuthResource != null){
			for(DBRow authResource : userRolesAuthResource){
				if(!StringUtil.isBlank(authResource.getString("action_uri"))){
					userRolesAuthResourceMap.put(authResource.getString("action_uri"), authResource);
				}
			}
		}
		
		return userRolesAuthResourceMap;
	}
	
	/**
	 * 功能：获取用户授权的Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, DBRow> getUserAuthResource(long adid) throws Exception {
		HashMap<String, DBRow> userAuthResourceMap = new HashMap<String, DBRow>();
		
		/*DBRow[] userRoles = floorAdminMgr.findAdminRoleByAdid(adid);
		if(userRoles==null || userRoles.length<1){
			return userAuthUriMap;
		}
		
		DBRow[] userAuthResource = floorAdminMgr.findAdminAthAction(userRoles, adid, 0L, null);*/
		
		DBRow[] userAuthResource = findUserAuthResource(adid);
		
		if(userAuthResource != null){
			for(DBRow authResource : userAuthResource){
				if(!StringUtil.isBlank(authResource.getString("action_uri"))){
					userAuthResourceMap.put(authResource.getString("action_uri"), authResource);
				}
			}
		}
		
		return userAuthResourceMap;
	}
	
	/**
	 * 功能：根据adid获取用户扩展的Uri
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public HashMap<String, DBRow> getUserExtendAuthResource(long adid) throws Exception {
		HashMap<String,DBRow> userExtendAuthResourceMap = new HashMap<String, DBRow>();
		
		//DBRow[] userExtendAuthResource = floorAdminMgr.findAdminAthAction(new DBRow[0], adid, 0L, null);
		
		DBRow[] userExtendAuthResource = findUserAuthResource(adid);
		
		if(userExtendAuthResource != null){
			for(DBRow authResource : userExtendAuthResource){
				if(!StringUtil.isBlank(authResource.getString("action_uri"))){
					userExtendAuthResourceMap.put(authResource.getString("action_uri"), authResource);
				}
			}
		}
		
		return userExtendAuthResourceMap;
	}
	
	/**
	 * 功能：获取角色的授权资源
	 * @param adgid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findRoleAuthResource(long adgid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append(" WHERE aga.adgid = "+ adgid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户拥有角色的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findUserRolesAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append("	LEFT JOIN admin_department ad ON ad.department_id = aga.adgid ")
				 .append(" WHERE ad.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findUserAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM admin_group_auth aga ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = aga.ataid ")
				 .append("	LEFT JOIN admin_department ad ON ad.department_id = aga.adgid ")
				 .append(" WHERE ad.adid = "+ adid).append(" ")
				 .append("UNION ")
				 .append("SELECT aa.* ")
				 .append("	FROM turboshop_admin_group_auth_extend agae ")
				 .append("	LEFT JOIN authentication_action aa ON aa.ataid = agae.ataid ")
				 .append(" WHERE agae.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取用户扩展的授权资源
	 * @param adid
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findUserExtendsAuthResource(long adid) throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
		 .append("	FROM turboshop_admin_group_auth_extend agae ")
		 .append("	LEFT JOIN authentication_action aa ON aa.ataid = agae.ataid ")
		 .append(" WHERE agae.adid = "+ adid);
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取所有需要控制权限的资源
	 * @return
	 * @throws Exception
	 */
	public DBRow[] findAllCtrlPermResource() throws Exception {
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT aa.* ")
				 .append("	FROM authentication_action aa ");
		
		return this.dbUtilAutoTran.selectMutliple(sqlBuffer.toString());
	}
	
	/**
	 * 功能：获取界面权限数据
	 * @param model
	 * @param adid
	 * @return
	 */
	public DBRow getUIPermsInfo(String model, long adid) throws Exception{
		DBRow perm = new DBRow();
		
		DBRow[] ctrlPerms = findAllCtrlPermResource();
		
		List<DBRow> ctrlPermsList = new LinkedList<DBRow>();
		
		for(DBRow ctrlPerm : ctrlPerms){
			String uri = ctrlPerm.getString("action_uri");
			String method = ctrlPerm.getString("request_method");
			
			if(!StringUtil.isBlank(uri)){
				DBRow tmpPerm = new DBRow();
				
				tmpPerm.add("uri", uri);
				tmpPerm.add("method", method);
				
				ctrlPermsList.add(tmpPerm);
			}
		}
		
		Map<String,Object> ctrlPerm = new HashMap<String, Object>();
		ctrlPerm.put("size", ctrlPermsList.size());
		ctrlPerm.put("perms", ctrlPermsList);
		
		DBRow[] userPerms = findUserAuthResource(adid);
		
		List<DBRow> userPermsList = new LinkedList<DBRow>();
		
		if(userPerms != null){
			for(DBRow userPerm : userPerms){
				String uri = userPerm.getString("action_uri");
				String method = userPerm.getString("request_method");
				
				if(!StringUtil.isBlank(uri)){
					DBRow tmpPerm = new DBRow();
					
					tmpPerm.add("uri", uri);
					tmpPerm.add("method", method);
					
					userPermsList.add(tmpPerm);
				}
			}
		}
		
		Map<String,Object> userPerm = new HashMap<String, Object>();
		userPerm.put("size", userPermsList.size());
		userPerm.put("perms", userPermsList);	
		
		perm.add("ctrlPerm", ctrlPerm);
		perm.add("userPerm", userPerm);
		
		return perm;
	}
}
