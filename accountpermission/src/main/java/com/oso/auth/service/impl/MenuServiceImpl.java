package com.oso.auth.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.util.Assert;

import com.cwc.app.floor.api.sbb.FloorAccountMgrSbb;
import com.cwc.app.floor.api.zr.FloorAccountMgrZr;
import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Node;
import com.oso.auth.beans.Resource;
import com.oso.auth.service.MenuService;
import com.oso.auth.service.ResourceService;
import com.oso.auth.util.NodeUtil;

/**
 * 菜单服务类,提供针对菜单的各种操作实现
 * 
 * @author lujintao
 *
 */
public class MenuServiceImpl implements MenuService {

	private static final String MENU_TABLE = "turboshop_control_tree";
	private DBUtilAutoTran dbUtilAutoTran;

	// 是否已完成初始化
	private boolean flag = false;
	private FloorAccountMgrSbb floorAccountMgr;
	private ResourceService resourceService;

	/**
	 * 根据id获取指定菜单
	 * 
	 * @param id
	 * @return
	 */
	public Menu get(long id) {
		Menu menu = null;
		try {
			DBRow dbRow = this.dbUtilAutoTran.selectSingle(new StringBuilder(
					"select * from turboshop_control_tree where id=")
					.append(id).toString());
			if (dbRow != null) {
				menu = this.convertToMenu(dbRow);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return menu;
	}

	/**
	 * 添加菜单，返回新添加菜单的ID
	 * 
	 * @param menu
	 * @return 如果添加成功，返回新添加菜单的ID；否则，返回0
	 */
	public int add(Menu menu) {
		int newId = 0;
		if (menu != null) {
			try {
				newId = (int) this.dbUtilAutoTran.getSequance(MENU_TABLE) + 1;
				menu.setId(newId);
				DBRow dbRow = this.dbUtilAutoTran
						.selectSingle(new StringBuilder(
								"select MAX(sort) sort from turboshop_control_tree where parentId=")
								.append(menu.getParentId()).toString());
				if (dbRow != null) {
					if (dbRow.get("sort") != null) {
						menu.setSort((Integer) dbRow.get("sort") + 1);
					}
				}

				this.dbUtilAutoTran.insertReturnId(MENU_TABLE,
						this.convertFromMenu(menu));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				newId = 0;
			}
		}
		return newId;
	}

	/**
	 * 修改菜单
	 * 
	 * @param menu
	 * @return 修改成功，返回true;否则，返回false
	 */
	public boolean update(Menu menu) {
		boolean flag = false;
		if (menu != null) {
			Menu oldMenu = this.get(menu.getId());
			if (oldMenu != null) {
				if (menu.getParentId() > 0) {
					menu.setControlType(oldMenu.getControlType());
				}

				try {
					this.dbUtilAutoTran.update(new StringBuilder("where id=")
							.append(menu.getId()).toString(), MENU_TABLE, this
							.convertFromMenu(menu));
					// 如果一级菜单的菜单类型发生了改变,子菜单的菜单类型也跟着改变
					if (menu.getParentId() == 0
							&& (menu.getControlType() != oldMenu
									.getControlType())) {
						this.dbUtilAutoTran.update(new StringBuilder(
								"where parentId=").append(menu.getId())
								.toString(),
								"control_type=" + menu.getControlType(),
								MENU_TABLE);
					}

					flag = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return flag;
	}

	/**
	 * 改变菜单的状态。如果之前是启用，就变禁用；禁用，就变启用
	 * 
	 * @param menu
	 *            菜单
	 * @return
	 */
	public boolean changeStatus(Menu menu) {
		boolean flag = false;
		Assert.notNull(menu);
		try {
			this.dbUtilAutoTran.update("where id=" + menu.getId(), "enabled="
					+ ((menu.isEnabled()) ? 0 : 1), MENU_TABLE);
			this.dbUtilAutoTran.update("where parentId=" + menu.getId(),
					"enabled=" + ((menu.isEnabled()) ? 0 : 1), MENU_TABLE);
			flag = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 改变菜单位置 ，将id对应的菜单移动到parentId对应菜单的指定位置下
	 * @param id
	 * @param oldParentId 菜单移动前父菜单ID
	 * @param parentId 新的父ID
	 * @param oldPosition  移动前的位置
	 * @param position 新的位置 从0开始
	 * @return
	 */
	public boolean changeOrder(int id,int oldParentId,int parentId,int oldPosition,int position) {
		boolean flag = false;
		try {
			
			// 菜单的新序号,默认为1
			int sort = 1;
			//如果只移动一个位置
			if(oldParentId == parentId && Math.abs(position - oldPosition) == 1){
				String sql = "select id,sort from "
						+ MENU_TABLE + " where parentid=?  order by sort asc limit " + (Math.min(oldPosition, position)) + ",2";
				DBRow para = new DBRow();
				para.put("parentId", parentId);
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(sql, para);
				if(rows != null && rows.length == 2){
					//与当前菜单交换顺序的菜单ID
					int anotherId = 0;
					//与当前菜单交换顺序的菜单的新序号
					int another_sort = 0;
					
					if(oldPosition < position){
						sort = rows[1].get("sort",0);
						another_sort = rows[0].get("sort",1);
						anotherId = rows[1].get("id",1);
					}else{
						sort = rows[0].get("sort",0);
						another_sort = rows[1].get("sort",1);
						anotherId = rows[0].get("id",1);						
					}
					this.dbUtilAutoTran.update("where id=" + anotherId, "sort=" + another_sort, MENU_TABLE);
				}
				
			}else{
				// 新的父菜单下所有子菜单的个数
				int totalCount = 1;
				// 最小序号
				int min_sort = 0;
				// 最大序号
				int max_sort = 0;
				String sql = "select min(sort) min_sort,max(sort) max_sort,count(id) count from "
						+ MENU_TABLE + " where parentId=?";
				DBRow para = new DBRow();
				para.put("parentId", parentId);
				DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
				if(row != null){
					min_sort =  row.get("min_sort",0);
					max_sort =  row.get("max_sort",0);
					totalCount =  row.get("count",0);
				}
				if(totalCount == 0){
					sort = 1;
				}else{
					// 如果是移动到末尾
					if (position + 1 >= totalCount) {
						sort = max_sort + 1;
					} else {
						// 如果是移动到首位
						if (position == 0) {
							sort = min_sort ;
						} else {
							
							
							int start = 0;
							if(oldParentId != parentId || oldPosition > position){
								start = position;
							}else{
								start = position + 1;
							}
							
							
							sql = "select sort   from " + MENU_TABLE + " where parentId=? order by sort asc limit " + start + ",1";
							DBRow param = new DBRow();
							param.put("parentId", parentId);
							DBRow dbRow = this.dbUtilAutoTran.selectPreSingle(sql,param);
							sort = (int)dbRow.get("sort");
						}
						//将位于该菜单后的菜单的序号加1
						this.dbUtilAutoTran.update("where parentId=" + parentId + " and sort>=" + sort,"sort=sort+1" , MENU_TABLE);
					}				
				}				
			}
			
			
			


			//定义更新菜单的SQL
			StringBuilder update_sql = new StringBuilder();
			if (oldParentId != parentId) {
				update_sql.append("parentId=").append(parentId).append(",");
				DBRow new_parent_menu = this.dbUtilAutoTran.selectSingle("select enabled from " + MENU_TABLE + " where id=" + parentId);
				if(new_parent_menu != null && !(boolean)new_parent_menu.get("enabled")){
					update_sql.append("enabled=0,");
				}
				
			}
			update_sql.append("sort=").append(sort);
			//更新待调整顺序菜单的序号
			this.dbUtilAutoTran.update("where id=" + id,update_sql.toString(), MENU_TABLE);
			flag = true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 删除菜单
	 * 
	 * @param id
	 *            菜单ID
	 * @return 删除成功，返回true;否则，返回false
	 */
	public boolean delete(long id) {
		boolean flag = false;

		if (id > 0) {
			String sqlWhere = "where id=" + id;
			try {
				this.dbUtilAutoTran.delete(sqlWhere, MENU_TABLE);
				flag = true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return flag;
	}

	/**
	 * 判断指定ID的菜单是否允许被删除
	 * 
	 * @param id
	 * @return
	 */
	public boolean canBeDelete(long id) {
		//菜单已分配权限或有子菜单不允许删除
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT DISTINCT 1 FROM turboshop_control_tree_map tctm WHERE tctm.ctid = "+ id).append(" ")
				 .append("UNION ")
				 .append("SELECT DISTINCT 1 FROM turboshop_control_tree_map_extend tctme WHERE tctme.ctid = "+ id).append(" ")
				 .append("UNION ")
				 .append("SELECT DISTINCT 1 FROM turboshop_control_tree tct WHERE tct.parentid = "+ id +" ");
		
		DBRow rs = null;
		try {
			rs = this.dbUtilAutoTran.selectSingle(sqlBuffer.toString());
		} catch (SQLException e) {
			rs = null;
		}
		
		return rs==null;
	}


	/**
	 * 判断在parentId 指定的菜单下是否存在名为title的,类型为指定类型且id不为指定id的菜单
	 * @param parentId 父菜单ID
	 * @param id  菜单ID
	 * @param title  菜单名称
	 * @param controlType 菜单类型
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int parentId,int id,String title,int controlType){

		boolean flag = false;
		try {
			String sql = "select count(id) count from " + MENU_TABLE + " where parentId = ?  and id<>? and title=?";
			DBRow para = new DBRow();
			para.add("parentId", parentId);
			para.add("id", id);
			para.add("title", title);
			if(controlType > 0){
				sql += " and control_type=?";
				para.add("control_type", controlType);
			}
			DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
			flag = ((int) row.get("count", 0) > 0) ? true : false;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return flag;
	}

	/**
	 * 根据菜单创建菜单树
	 * 
	 * @return
	 */
	public Node createMenuTree() {
		Node rootNode = new Node("-1", "Root", "img/root.png");
		rootNode.setType(NodeUtil.ROOT);
		rootNode.addState("opened", true);
		rootNode.addAttribute("enabled", true);
		try {
			List<Menu> menuList = new ArrayList<Menu>();
			DBRow[] rows = this.dbUtilAutoTran.select(MENU_TABLE);
			if (rows != null && rows.length > 0) {
				for (int i = 0; i < rows.length; i++) {
					menuList.add(this.convertToMenu(rows[i]));
				}
				List<Resource> resourceList = this.resourceService.getAll();
				Set<Menu> menu_set = this.deal(menuList, resourceList);
				if (menu_set != null && !menu_set.isEmpty()) {
					for (Menu menu : menu_set) {
						rootNode.addChild(NodeUtil.toNode(menu));
						
					}
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return rootNode;
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}

	/**
	 * 实现菜单向DBRow的转换
	 * 
	 * @param menu
	 * @return 返回菜单对于的DBRow对象
	 */
	private DBRow convertFromMenu(Menu menu) {
		if (menu != null) {
			DBRow menuRow = new DBRow();
			menuRow.add("id", menu.getId());
			menuRow.add("parentId", menu.getParentId());
			menuRow.add("sort", menu.getSort());
			menuRow.add("title", menu.getText());
			menuRow.add("link", menu.getLink());
			menuRow.add("ico", menu.getIcon());
			menuRow.add("control_type", menu.getControlType());
			menuRow.add("enabled", menu.isEnabled() ? 1 : 0);
			return menuRow;
		} else {
			return null;
		}

	}

	/**
	 * 实现从row向菜单的转换
	 * 
	 * @param row
	 * @return 返回row对于的菜单对象
	 */
	private Menu convertToMenu(DBRow row) {
		if (row != null) {
			Menu menu = new Menu();
			menu.setId(row.get("id", 0));
			menu.setParentId(row.get("parentId", 0));
			menu.setSort(row.get("sort", 0));
			menu.setText(row.get("title", ""));
			menu.setLink(row.get("link", ""));
			menu.setIcon(row.get("icon", ""));
			menu.setControlType((Integer) row.get("control_type",1));
			menu.setEnabled((boolean) row.get("enabled"));
			return menu;
		} else {
			return null;
		}

	}

	/**
	 * 对菜单集合进行处理，建立菜单之间的父子关系，并返还一级菜单集合
	 * 
	 * @param menuList
	 * @return
	 */
	private Set<Menu> deal(List<Menu> menuList, List<Resource> resourceList) {
		Set<Menu> result = new TreeSet<Menu>();
		if (menuList != null) {
			Map<Object, Menu> menu_map = new HashMap<Object, Menu>();
			for (Menu menu : menuList) {
				menu_map.put(menu.getId(), menu);
			}
			for (Menu menu : menuList) {
				if (menu.getParentId() == 0) {
					result.add(menu);
				} else {
					Menu parent_menu = menu_map.get(menu.getParentId());
					if (parent_menu != null) {
						parent_menu.getChildren().add(menu);
					}
				}
			}
			for (Resource resource : resourceList) {
				Menu menu = menu_map.get(resource.getPage());
				if (menu != null) {
					menu.getResourceList().add(resource);
				}
			}
		}
		return result;
	}

	

	public ResourceService getResourceService() {
		return resourceService;
	}

	public void setResourceService(ResourceService resourceService) {
		this.resourceService = resourceService;
	}
}
