package com.oso.auth.service.impl;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.util.Assert;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.db.PageCtrl;
import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Resource;
import com.oso.auth.service.ResourceService;

/**
 * 
 * @author lujintao
 *
 */
public class ResourceServiceImpl implements ResourceService{
	private static final String TABLE = "authentication_action";
	private static final String MENU_TABLE = "turboshop_control_tree";
	private DBUtilAutoTran dbUtilAutoTran;
	/**
	 * 添加资源，返回新添加资源的ID
	 * @param resource
	 * @return   如果添加成功，返回新添加资源的ID；否则，返回0
	 */
	public int add(Resource resource){
		int  newId = 0;
		if(resource != null){
			try {
				newId = (int)this.dbUtilAutoTran.insertReturnId(TABLE, this.convertFrom(resource));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return newId;
	}
	
	/**
	 * 修改资源
	 * @param menu
	 * @return  修改成功，返回true;否则，返回false
	 */
	public boolean update(Resource resource){
		boolean flag = false;
		if(resource != null){
			String sqlWhere = "where ataid=" + resource.getId();
			try {
				this.dbUtilAutoTran.update(sqlWhere, TABLE, this.convertFrom(resource));
				flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * 改变资源的状态。如果之前需要权限访问，则修改成不需权限访问；反之亦然
	 * @param resource   资源
	 * @return
	 */
	public boolean changeStatus(Resource resource){
		boolean flag = false;
		Assert.notNull(resource);
		try {
			String where = "";
			//更新菜单下所有的资源
			if(resource.getPage() > 0){
				DBRow dbRow = this.dbUtilAutoTran.selectSingle(new StringBuilder("select id,parentId from turboshop_control_tree where id=").append(resource.getPage()).toString());
				if(dbRow != null && dbRow.get("parentId", 0) == 0){
					where = "where page in( select id from  turboshop_control_tree where parentId=" + resource.getPage() + ")";
				}else{
					where = "where page=" + resource.getPage();
				}
			}else{
				where = "where ataid=" + resource.getId();
			}
			
			this.dbUtilAutoTran.update(where, "auth=" + (( resource.isAuth() ) ? 0 : 1), TABLE);
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	/**
	 * 删除资源
	 * @param id  资源ID
	 * @return  删除成功，返回true;否则，返回false
	 */
	public boolean delete(long id){
		boolean flag = false;
		
		if(id > 0){
			String sqlWhere = "where ataid=" + id;
			try {
				this.dbUtilAutoTran.delete(sqlWhere, TABLE);
				flag = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	
	/**
	 * 判断资源是否允许被删除
	 * @param id
	 * @return
	 */
	public boolean canBeDelete(long id){
		StringBuffer sqlBuffer = new StringBuffer();
		
		sqlBuffer.append("SELECT DISTINCT 1 FROM admin_group_auth aga WHERE aga.ataid = "+ id).append(" ")
				 .append("UNION ")
				 .append("SELECT DISTINCT 1 FROM turboshop_admin_group_auth_extend agae WHERE agae.ataid = "+ id);
		
		DBRow rs = null;
		try {
			rs = this.dbUtilAutoTran.selectSingle(sqlBuffer.toString());
		} catch (SQLException e) {
			rs = null;
		}
		
		return rs==null;
	}
	
	/**
	 * 判断在menuId 指定的菜单下是否存在名为description的,且id不为指定id的资源
	 * @param menuId 菜单ID
	 * @param id  资源ID
	 * @param description  资源描述
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int menuId,int id,String description){
		
		boolean flag = false;
			try {
				 String sql = "select count(ataid) count from " + TABLE + " where page = ?  and ataid<>? and description=?";
					 
			     DBRow para = new DBRow();
		         para.add("page", menuId);
		         para.add("ataid", id);
		         para.add("description", description);				
				 DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
				 flag = ((int)row.get("count", 0) > 0) ? true : false;
				 
			} catch (Exception e) {
				e.printStackTrace();
			}			
		return flag;
	}

	/**
	 * 判断在menuId 指定的菜单下是否存在url和method为指定值的,且id不为指定id的资源
	 * @param menuId 菜单ID
	 * @param resourceId  资源ID
	 * @param description  资源描述
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int menuId,int resourceId,String url,String method){
		url = (url == null) ? "" : url;
		method = (method == null) ? "" : method;
		
		boolean flag = false;
		try {
			 String sql = "select count(ataid) count from " + TABLE + " where page = ?  and ataid<>? and action_uri=? and request_method=?";
				 
		     DBRow para = new DBRow();
	         para.add("page", menuId);
	         para.add("ataid", resourceId);
	         para.add("action_uri", url);
	         para.add("request_method", method);
			 DBRow row = this.dbUtilAutoTran.selectPreSingle(sql, para);
			 flag = ((int)row.get("count", 0) > 0) ? true : false;
			 
		} catch (Exception e) {
			e.printStackTrace();
		}			
	return flag;		
	}
	
	/**
	 * 根据ID获取对应资源
	 * @param id
	 * @return
	 */
	public Resource get(int id){
		Resource resource = null;
		try {
			DBRow row = this.dbUtilAutoTran.selectSingle(new StringBuilder("select * from authentication_action where ataid=").append(id).toString());
			resource = this.convertToResource(row);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resource;
	}
	
	/**
	 * 获得所有的资源
	 * @return
	 */
	public List<Resource>  getAll(){
		List<Resource> list = new ArrayList<Resource>();
		try {
			DBRow[] rows = this.dbUtilAutoTran.selectMutliple("select * from " + TABLE + " order by ataid asc");
			if(rows != null && rows.length > 0){
				for(int i = 0; i < rows.length; i++){
					list.add(this.convertToResource(rows[i]));
				}
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * 获取指定菜单下的特定页的资源
	 * @param menu
	 * @return
	 */
	public List<Resource> getResources(Menu menu,PageCtrl page){
		List<Resource> list = new ArrayList<Resource>();
		Assert.notNull(menu, "menu is null");
		if(menu != null){
			try {
				DBRow[] rows = this.dbUtilAutoTran.selectMutliple("where page=" + menu.getId(),page);
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						list.add(this.convertToResource(rows[i]));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
		return list;
	}
	
	/**
	 * 获取指定菜单下并满足指定条件的特定页的资源
	 * @param menu   指定菜单
	 * @param conditions  查询条件
	 * @param page   分页信息
	 * @return
	 */
	public List<Resource> getResources(Menu menu,Map<String,Object> conditions,PageCtrl page){
		List<Resource> list = new ArrayList<Resource>();
		Assert.notNull(menu, "menu is null");
		if(menu != null){
			try {
				DBRow data_row = new DBRow();
				data_row.add("page", menu.getId());
				StringBuilder builder = new StringBuilder("where page=? ");
				
				if(conditions != null && conditions.size() > 0){
					String keyword = null;
					if(conditions.containsKey("keyword") && (keyword = (String)conditions.get("keyword")) != null){
						String[] items = keyword.split(" ");
						int i = 0;
						for(String item : items){
							builder.append(" and description like ? ");
							data_row.add("description" + i, item);							
							i++;
						}
					}
					Object auth = null;
					if(conditions.containsKey("auth") && (auth = conditions.get("auth")) != null){
						builder.append(" and auth=? ");
						data_row.add("auth",(boolean)auth? 1 : 0);
					}
				}
				
				DBRow[] rows = this.dbUtilAutoTran.selectPreMutliple(builder.toString(),data_row,page);
				if(rows != null && rows.length > 0){
					for(int i = 0; i < rows.length; i++){
						list.add(this.convertToResource(rows[i]));
					}
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		
		return list;
	}
	
	/**
	 * 实现菜单向DBRow的转换
	 * @param menu
	 * @return  返回菜单对于的DBRow对象 
	 */
	private DBRow convertFrom(Resource resource){
		if(resource != null){
			DBRow row = new DBRow();
			row.add("ataid", resource.getId());
			row.add("page", resource.getPage());
			row.add("action", resource.getAction());
			row.add("description", resource.getDescription());
			row.add("action_uri", resource.getUrl());
			row.add("request_method", resource.getMethod());
			row.add("auth", resource.isAuth() ? 1 : 0);
			return row;
		}else{
			return null;
		}

	}
	
	/**
	 * 实现row 向 Resource 对象的转换
	 * @param menu
	 * @return  返回菜单对于的DBRow对象 
	 */
	private Resource convertToResource(DBRow row){
		if(row != null){
			Resource resource = new Resource();
			resource.setId(row.get("ataid", 0));
			resource.setPage(row.get("page", 0));
			resource.setAction(row.get("action",""));
			resource.setDescription(row.getString("description"));
			resource.setUrl(row.getString("action_uri"));
			resource.setMethod(row.getString("request_method"));
			resource.setAuth((Boolean)row.get("auth"));
			return resource;
		}else{
			return null;
		}
	}

	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran) {
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
}
