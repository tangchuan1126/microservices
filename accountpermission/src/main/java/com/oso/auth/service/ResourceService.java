package com.oso.auth.service;

import java.util.List;
import java.util.Map;

import com.cwc.db.PageCtrl;
import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Resource;;

public interface ResourceService {
	/**
	 * 添加资源，返回新添加资源的ID
	 * @param resource
	 * @return   如果添加成功，返回新添加资源的ID；否则，返回0
	 */
	public int add(Resource resource);
	
	/**
	 * 修改资源
	 * @param menu
	 * @return  修改成功，返回true;否则，返回false
	 */
	public boolean update(Resource resource);

	/**
	 * 改变资源的状态。如果之前需要权限访问，则修改成不需权限访问；反之亦然
	 * @param resource   资源
	 * @return
	 */
	public boolean changeStatus(Resource resource);
	
	/**
	 * 删除资源
	 * @param id  资源ID
	 * @return  删除成功，返回true;否则，返回false
	 */
	public boolean delete(long id);
	
	/**
	 * 判断资源是否允许被删除
	 * @param id
	 * @return
	 */
	public boolean canBeDelete(long id);
	
	
	/**
	 * 判断在menuId 指定的菜单下是否存在名为description的,且id不为指定id的资源
	 * @param menuId 菜单ID
	 * @param resourceId  资源ID
	 * @param description  资源描述
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int menuId,int resourceId,String description);

	/**
	 * 判断在menuId 指定的菜单下是否存在url和method为指定值的,且id不为指定id的资源
	 * @param menuId 菜单ID
	 * @param resourceId  资源ID
	 * @param description  资源描述
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int menuId,int resourceId,String url,String method);
	
	/**
	 * 根据ID获取对应资源
	 * @param id
	 * @return
	 */
	public Resource get(int id);
	
	/**
	 * 获得所有的资源
	 * @return
	 */
	public List<Resource>  getAll();
	
	/**
	 * 获取指定菜单下的特定页的资源
	 * @param menu
	 * @return
	 */
	public List<Resource> getResources(Menu menu,PageCtrl page);
	
	/**
	 * 获取指定菜单下并满足指定条件的特定页的资源
	 * @param menu   指定菜单
	 * @param conditions  查询条件
	 * @param page   分页信息
	 * @return
	 */
	public List<Resource> getResources(Menu menu,Map<String,Object> conditions,PageCtrl page);
	
}
