package com.oso.auth.service;

import java.util.Set;

import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Node;

/**
 * 菜单服务接口
 * @author lujintao
 *
 */
public interface MenuService {
	
	/**
	 * 根据id获取指定菜单
	 * @param id
	 * @return
	 */
	public Menu get(long id);
	
	/**
	 * 添加菜单，返回新添加菜单的ID
	 * @param menu
	 * @return   如果添加成功，返回新添加菜单的ID；否则，返回0
	 */
	public int add(Menu menu);
	
	/**
	 * 修改菜单
	 * @param menu
	 * @return  修改成功，返回true;否则，返回false
	 */
	public boolean update(Menu menu);
	
	/**
	 * 改变菜单的状态。如果之前是启用，就变禁用；禁用，就变启用
	 * @param menu   菜单
	 * @return
	 */
	public boolean changeStatus(Menu menu);
	
	/**
	 * 改变菜单位置 ，将id对应的菜单移动到parentId对应菜单的指定位置下
	 * @param id
	 * @param oldParentId 菜单移动前父菜单ID
	 * @param parentId 新的父ID
	 * @param oldPosition  移动前的位置
	 * @param position 新的位置 从0开始
	 * @return
	 */
	public boolean changeOrder(int id,int oldParentId,int parentId,int oldPosition,int position);
	
	
	/**
	 * 删除菜单
	 * @param id  菜单ID
	 * @return  删除成功，返回true;否则，返回false
	 */
	public boolean delete(long id);

	/**
	 * 判断指定ID的菜单是否允许被删除
	 * @param id
	 * @return
	 */
	public boolean canBeDelete(long id);
	
	
	/**
	 * 判断在parentId 指定的菜单下是否存在名为title的,类型为指定类型且id不为指定id的菜单
	 * @param parentId 父菜单ID
	 * @param id  菜单ID
	 * @param title  菜单名称
	 * @param controlType 菜单类型
	 * @return  存在，返回true;否则，返回false
	 */
	public boolean exists(int parentId,int id,String title,int controlType);
	
	/**
	 * 根据菜单创建菜单树
	 * @return
	 */
	public Node createMenuTree();
	
	
}
