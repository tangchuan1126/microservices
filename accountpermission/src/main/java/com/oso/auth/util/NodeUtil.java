package com.oso.auth.util;

import org.springframework.util.Assert;

import com.oso.auth.beans.Menu;
import com.oso.auth.beans.Node;
import com.oso.auth.beans.Resource;

public class NodeUtil {
	public static final String FIRST_WEB_MENU = "first_web_menu";
	public static final String SECOND_WEB_MENU = "second_web_menu";
	public static final String FIRST_APP_MENU = "first_app_menu";
	public static final String SECOND_APP_MENU = "second_app_menu";
	public static final String WEB_RESOURCE = "web_resource";
	public static final String APP_RESOURCE = "app_resource";
	public static final String ROOT = "root";
	
	public static Node toNode(Menu menu) {
		Assert.notNull(menu);
		Node node = new Node();
		node.setId(menu.getId() + "");
		node.setIcon(menu.getIcon());
		node.setParentId(menu.getParentId() + "");
		node.setLevel(menu.getParentId() == 0 ? 1 : 2);
		node.addAttribute("enabled", menu.isEnabled());
		node.addAttribute("controlType", menu.getControlType());
		node.addState("disabled", !menu.isEnabled());
		node.addState("opened", false );
		String type = "";
		
		if(menu.getParentId() == 0){
			type = (menu.getControlType() == Menu.WEB_FLAG) ? NodeUtil.FIRST_WEB_MENU : NodeUtil.FIRST_APP_MENU ;
		}else{
			type = (menu.getControlType() == Menu.WEB_FLAG) ? NodeUtil.SECOND_WEB_MENU : NodeUtil.SECOND_APP_MENU ;
		}
		node.setType(type);
		String text = menu.getText();

		//菜单下需要权限访问的资源总数
		int  total_auth_resource = 0;
		
		//菜单下资源总数
		int total_resource = 0;
		if (menu.getChildren() != null && !menu.getChildren().isEmpty() && node.getLevel() == 1) {
			//可用子菜单的总数
			int avaliable_count = 0;

			for (Menu item : menu.getChildren()) {
				if(item.isEnabled()){
					avaliable_count++;
				}
				total_resource += item.getResourceList().size();
				for(Resource resource : item.getResourceList()){
					if(resource.isAuth()){
						total_auth_resource++;
					}
				}
				node.getChildren().add(toNode(item));
			}
			
			text = menu.getText() + "  (<font class=\"enabled\">" + avaliable_count + "</font>/<font class=\"total\">" + menu.getChildren().size() + "</font>" +  ")";
		} else if (menu.getResourceList() != null && !menu.getResourceList().isEmpty()) {
			total_resource = menu.getResourceList().size();
			for (Resource item : menu.getResourceList()) {
				node.getChildren().add(toNode(item,(menu.getControlType() == Menu.WEB_FLAG) ? true : false));
				if(item.isAuth()){
					total_auth_resource++;
				}
			}
			text = menu.getText() + "  (<font class=\"enabled\">" + total_auth_resource + "</font>/<font class=\"total\">" + menu.getResourceList().size() + "</font>" +  ")";
			
		}else{
			text = menu.getText() + "  (<font class=\"enabled\">0</font>/<font class=\"total\">0</font>" +  ")";
		}
		node.setText(text);
		node.addState("selected", total_resource == total_auth_resource);
		node.addState("undetermined", (total_resource > total_auth_resource && total_auth_resource > 0));
		return node;
	}

	/**
	 * 提取资源信息，将资源转化成对应的Node对象
	 * @param resource 
	 * @param isWeb	 资源所属菜单是否是WEB菜单
	 * @return
	 */
	public static Node toNode(Resource resource,boolean isWeb) {
		Assert.notNull(resource);
		Node node = new Node();
		node.setId("role_" + resource.getId());
		node.setText(resource.getDescription());
		node.setIcon(resource.getIcon());
		node.setLevel(3);
		node.setType( (isWeb) ? NodeUtil.WEB_RESOURCE : NodeUtil.APP_RESOURCE);
		node.addAttribute("auth", resource.isAuth());
		node.addState("selected", resource.isAuth());
		return node;
	}
}
