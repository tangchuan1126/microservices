package com.oso.auth.beans;

import java.io.Serializable;
import java.util.Set;

/**
 * 表示用户可操作的资源
 * @author lujintao
 *
 */
public class Resource{
	
	private int id;
	//所属菜单ID
	private int page;
	private String action;
	private String url;
	private String description;
	private String method;
	private boolean auth = true;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMethod() {
		return method;
	}
	public void setMethod(String method) {
		this.method = method;
	}
	public boolean isAuth() {
		return auth;
	}
	public void setAuth(boolean auth) {
		this.auth = auth;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	public String getIcon(){
		return (this.auth) ? "img/auth.png" : "img/public.png" ;
	}
	
	public Set getChildren(){
		return null;
	}
	

	
}
