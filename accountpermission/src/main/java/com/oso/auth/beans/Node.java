package com.oso.auth.beans;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.springframework.util.Assert;

/**
 * 定义了树节点的公共方法
 * @author lujintao
 *
 */
public class Node{
	private String id;
	private String text;
	private String icon;
	private String type;
	private String parentId;
	//节点在树中所处的级别   根节点对应的level为0
	private int level = 0;
	//其它属性
	private Map<String,Object> others = new HashMap<String,Object>();
	private Map<String,Object> state = new HashMap<String,Object>();
	
	private Set<Node> children = new LinkedHashSet<Node>();
	
	public Node(){
		
	}
	
	public Node(String id,String text,String icon){
		this.id = id;
		this.text = text;
		this.icon = icon;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Set<Node> getChildren() {
		return children;
	}
	public void setChildren(Set<Node> children) {
		this.children = children;
	}
	
	public void addChild(Node node){
		if(this.children == null){
			this.children = new HashSet<Node>();
		}
		this.children.add(node);
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public Map<String, Object> getOthers() {
		return others;
	}

	public void addAttribute(String key,Object value){
		this.others.put(key, value);
	}

	public Map<String, Object> getState() {
		return state;
	}
	
	public void addState(String key,Object value){
		this.state.put(key, value);
	}	
}
