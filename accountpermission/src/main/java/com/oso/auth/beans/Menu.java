package com.oso.auth.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.util.Assert;

/**
 * 菜单
 * 此类具有与equals不一致的自然排序
 * @author lujintao
 *
 */
public class Menu implements Comparable<Menu> ,Serializable{
	//表示菜单是WEB菜单
	public static final int WEB_FLAG = 1;
	//表示菜单是APP菜单
	public static final int APP_FLAG = 1;	
	private int id;
	private int parentId;
	private int sort = 1;
	//菜单类型  1表示WEB菜单，2表示APP菜单
	private int controlType = 1;
	private String text;
	private String link;
	private String icon;
	//是否可用
	private boolean enabled = true;
	//子菜单列表
	private Set<Menu> children = new TreeSet<Menu>();
	//菜单下的资源列表
	private List<Resource> resourceList = new ArrayList<Resource>();
	

	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public int getSort() {
		return sort;
	}
	public void setSort(int sort) {
		this.sort = sort;
	}
	public int getControlType() {
		return controlType;
	}
	public void setControlType(int controlType) {
		this.controlType = controlType;
	}

	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}


	public String getIcon() {
		if(this.icon == null || this.icon.trim().length() == 0){
			if(this.controlType == 1){
				if(this.isEnabled()){
					icon = "img/web.png";
				}else{
					icon = "img/web_disabled.png";
				}
			}else{
				if(this.isEnabled()){
					icon = "img/app.png";
				}else{
					icon = "img/app_disabled.png";
				}			
			}			
		}
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	
	public Set<Menu> getChildren() {
		return children;
	}
	public void setChildren(Set<Menu> children) {
		this.children = children;
	}

	public List<Resource> getResourceList() {
		return resourceList;
	}
	public void setResourceList(List<Resource> resourceList) {
		this.resourceList = resourceList;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	@Override
	public int compareTo(Menu another) {
		// TODO Auto-generated method stub
		Assert.notNull(another);
		
		return (this.sort == another.getSort()) ? this.getId() - another.getId() : this.sort - another.getSort();
	}
}
