package com.msa;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.log.FilterProductStoreLogBean;
import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.app.floor.api.FloorLogFilterMgr;
import com.cwc.app.floor.api.FloorLogMgrIFace;
import com.cwc.db.PageCtrl;

@RestController
public class ProductStoreLogController {
	@Autowired
	private FloorLogFilterMgr logFilterMgr;
	
	@Autowired
	private FloorLogMgrIFace logMgr;
	
	@RequestMapping(value = "/productStoreLog", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> searchLog(
			@RequestParam(value="ps_id",defaultValue="0",required=false) long ps_id,
			@RequestParam(value="pc_id",defaultValue="0",required=false) long pc_id,
			@RequestParam(value="pageSize",defaultValue="0",required=false) int pageSize,
			@RequestParam(value="pageNo",defaultValue="0",required=false) int pageNo) throws Exception {
		FilterProductStoreLogBean psLog = new FilterProductStoreLogBean();
		psLog.setPs_id(ps_id);
		psLog.setPc_id(pc_id);
		int onGroup = 0;
		int in_or_out = 1;
		PageCtrl pc = new PageCtrl();
		pc.setPageSize(pageSize==0 ? 20 : pageSize);
		pc.setPageNo(pageNo==0 ? 1 : pageNo);
		String start_date = "2013-07-01";
		String end_date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		
		Map<String,Object> result= new HashMap<String,Object>();
		result.put("data",logFilterMgr.getSearchProductStoreSysLogs(start_date,end_date,psLog, onGroup, in_or_out, pc));
		result.put("pageCtrl", pc);
		
		return result;
	}
	
	//请求体直接转换为JavaBean演示
	@RequestMapping(value = "/productStoreLog", produces = "application/json; charset=UTF-8", method = RequestMethod.POST)
	public Map<String,Object> addLog(
			@RequestBody ProductStoreLogBean psLog
			) throws Exception {
//		System.out.println(new JSONObject(psLog).toString(4));
		Map<String,Object> result = new HashMap<String,Object>();
//		result.put("id", logMgr.availableProductStoreLog(psLog, false));
		result.put("success", true);
		return result;
	}
}
