package com.msa;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.FloorProductStoreMgr;

@RestController
public class ProductStoreController {
	@Autowired
	private FloorProductStoreMgr productStoreMgr;
	
	@RequestMapping(value = "/productStore/{ps_id}", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map physicalCount(@PathVariable("ps_id") long ps_id) throws Exception {
		return productStoreMgr.physicalCount(ps_id, 0, 0);
	}
	
}
