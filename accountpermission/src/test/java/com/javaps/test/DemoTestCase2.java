package com.javaps.test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Map;

import javax.annotation.Resource;

import org.junit.Test;

import us.monoid.json.JSONObject;

import com.cwc.app.beans.log.ProductStoreLogBean;
import com.cwc.db.DBRow;
import com.msa.ProductStoreLogController;

public class DemoTestCase2 extends BaseTestCase {
	
	@Resource
	ProductStoreLogController pslc;
	
	@Test
	public void test_1() throws Exception {
		ProductStoreLogBean psLog = new ProductStoreLogBean();
		psLog.setAccount("hanlong");
		psLog.setPs_id(99999);
		psLog.setPc_id(99999);
		psLog.setLot_number("test");
		psLog.setOperation(1);
		psLog.setQuantity(1);
		psLog.setAdid(99999);
		psLog.setTitle_id(99999);
		psLog.setProduct_line_id(99999);
		psLog.setCatalogs(new long[]{99999});
		
		
		Map<String,Object> result = pslc.addLog(psLog);
		assertTrue((Boolean)result.get("success") && result.get("id")!=null);
		
		String id = (String)result.get("id");
		
		result = pslc.searchLog(99999, 99999, 100000, 1);
		assertTrue(result.get("data") != null);
		DBRow[] logs = (DBRow[])result.get("data");
		
		for(DBRow log:logs){
			System.out.println(new JSONObject(log).toString(4));
			if(log.get("ID","").equals(id)) return;
		}
		
		fail("没有找到之前加入的日志！");
	}
}
