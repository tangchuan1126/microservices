package com.javaps.test;

import static org.junit.Assert.assertTrue;

import java.util.Map;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import us.monoid.json.JSONObject;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.javaps.springboot.TestSessionController;
import com.msa.ProductStoreController;

public class DemoTestCase1 extends BaseTestCase {
	
	@Resource
	TestSessionController tsc;
	
	@Resource
	ProductStoreController psc;
	
	@Before
	public void setUp() throws Exception {
		ConfigBean.putBeans("admin", "admin");
		
		//将以下两行注释掉，测试一样可以成功，说明兼容以前的行为。
		DBRow.setKeyUseUpper(false); 
		DBRow.setKeyIgnoreCase(true);
	}
	
	@Test
	public void test_getAdmin() throws Exception {
		long adid = 100198;
		Map<String,Object> m = tsc.getAdmin(adid);
		System.out.println(new JSONObject(m).toString(4));
		
		assertTrue(m!=null && m.containsKey("ADID") &&  m.get("ADID").toString().equals(String.valueOf(adid)));
		assertTrue(m!=null && m.containsKey("adid") &&  m.get("adid").toString().equals(String.valueOf(adid)));
		
	}
	
	@Ignore
	@Test
	public void test_productStore() throws Exception {
		long ps_id = 100000;
		
		Map resp = psc.physicalCount(ps_id);
		
		System.out.println(new JSONObject(resp).toString(4));
		assertTrue(resp.size()>0);
	}

}
