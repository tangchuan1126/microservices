#!/bin/sh
if [ -z "$1" ] || [ -z "$2" ] || [ ! -f "$1" ] 
then
	echo "USAGE:  $0  <file path> <url>"
	exit 1
fi
curl -v -XPOST --data-binary  @"$1"  "$2"  --header "Content-Type: `file -b --mime-type $1`"
