#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ] || [ ! -f "$2" ] || [ -z "$3" ]
then
	echo "USAGE:  post_json.sh <JSESSIONID Value> <JSON file path> <target URL>"
	exit 1
fi
curl -v \
-b "JSESSIONID=$1" \
--request PUT \
--header "Content-Type: application/json" \
--data @$2 \
$3
echo