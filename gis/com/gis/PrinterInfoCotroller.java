package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.javaps.springboot.SessionAttr;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PrinterInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @Autowired
  private StoragesCotroller storagesCotroller;

  @RequestMapping(value={"/printerInfoCotroller/qureyprintServers"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Object> qureyprintServers(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="pageNo", defaultValue="0", required=false) int pageNo, @RequestParam(value="pageSize", defaultValue="0", required=false) int pageSize)
    throws Exception
  {
    PageCtrl pc = new PageCtrl();
    pc.setPageSize(pageSize == 0 ? 4 : pageSize);
    pc.setPageNo(pageNo == 0 ? 1 : pageNo);
    Map result = new HashMap();
    result.put("data", this.floorGoogleMapsMgrCc.getPrintServerByPsId(ps_id, pc));
    result.put("pageCtrl", pc);
    return result;
  }

  @RequestMapping(value={"/printerInfoCotroller/deletePrint"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow deletePrint(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="p_id", required=true) long p_id, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception {
    DBRow row = new DBRow();
    String isManager = alb.get("administrator").toString();
    if (!isManager.equals("true")) {
      row.add("flag", "authError");
      throw new Exception("authError");
    }
    try {
      this.floorGoogleMapsMgrCc.deletePrinterById(ps_id, p_id);
      row.add("flag", "true");

      return row;
    }
    catch (Exception e)
    {
      e = 
        e;

      row.add("flag", "false");
      e.printStackTrace();

      return row; } finally {  } return row;
  }

  @RequestMapping(value={"/printerInfoCotroller/savePrinter"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow savePrinter(@RequestBody Map<String, Object> map, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception {
    DBRow ret = new DBRow();
    String isManager = alb.get("administrator").toString();
    if (!isManager.equals("true")) {
      ret.add("flag", "authError");
      throw new Exception("authError");
    }
    String p_id = !map.get("p_id").toString().equals("") ? map.get("p_id").toString() : "0";
    long psId = Long.parseLong(map.get("ps_id").toString());
    long p_type = Long.parseLong(map.get("p_type").toString());
    String type = map.get("type").toString();
    String servers_name = map.get("servers_name").toString();
    double x = Double.parseDouble(map.get("x").toString());
    double y = Double.parseDouble(map.get("y").toString());
    String size = map.get("size").toString();
    String name = map.get("name").toString();
    String pageType = map.get("pageType").toString();
    String area_id = map.get("area_id").toString();
    DBRow row = new DBRow();
    String latlng = this.storagesCotroller.convertCoordinateToLatlng(psId, x, y).getString("latlng", "");

    if (latlng.isEmpty()) {
      ret.add("flag", Boolean.valueOf(false));
      throw new NullPointerException("no latlngs data!");
    }
    row.add("latlng", latlng);

    if (p_type == 0L) {
      String port = map.get("port").toString();
      String ip = map.get("ip").toString();
      row.add("ip", ip);
      row.add("port", port);
      row.add("servers", "");
      row.add("servers_name", "");
    } else if (p_type == 1L) {
      String servers = map.get("servers").toString();
      row.add("servers", servers);
      row.add("servers_name", servers_name);
      row.add("ip", "");
      row.add("port", "");
    }
    row.add("p_id", p_id);
    row.add("ps_id", psId);
    row.add("name", name);
    row.add("type", type);
    row.add("size", size);
    row.add("x", x);
    row.add("y", y);
    row.add("p_type", p_type);
    row.add("physical_area", area_id);
    try {
      long insertId = 0L;
      if ((pageType.equals("0")) || (p_id.equals("0"))) {
        insertId = this.floorGoogleMapsMgrCc.addPrinter(row);
        ret.add("id", insertId);
      } else {
        this.floorGoogleMapsMgrCc.updatePrinter(row);
      }
      ret.add("flag", "true");
      ret.add("latlng", latlng);

      return ret;
    }
    catch (Exception e)
    {
      e = 
        e;

      e.printStackTrace();
      ret.add("flag", "false");

      return ret; } finally {  } return ret;
  }
}