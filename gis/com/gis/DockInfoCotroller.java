package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.key.EquipmentTypeKey;
import com.cwc.app.util.DBRowUtils;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DockInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/dockInfoCotroller/queryDoorYard"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Map<String, Object>> getDocksYardData(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    Map map = new HashMap();
    DBRow[] docks = this.floorGoogleMapsMgrCc.getDocksData(ps_id);
    DBRow[] yards = this.floorGoogleMapsMgrCc.getParkingData(ps_id);
    if (docks.length > 0) {
      for (int i = 0; i < docks.length; i++) {
        DBRow row = docks[i];
        String key = row.getString("ps_id") + "_docks_" + row.getString("obj_id");
        map.put(key, DBRowUtils.dbRowAsMap(row));
      }
    }
    if (yards.length > 0) {
      for (int i = 0; i < yards.length; i++) {
        DBRow row = yards[i];
        String key = row.getString("ps_id") + "_parking_" + row.getString("obj_id");
        map.put(key, DBRowUtils.dbRowAsMap(row));
      }
    }
    return map;
  }
  public Map<String, List<Map<String, String>>> getDocksData(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map rt = new HashMap();
    List list = new ArrayList();
    DBRow[] docks = this.floorGoogleMapsMgrCc.getDocksData(ps_id);
    if (docks.length > 0) {
      for (int i = 0; i < docks.length; i++) {
        Map map = new HashMap();
        DBRow r = docks[i];
        map.put("latlng", r.getString("latlng"));
        map.put("obj_name", r.getString("obj_name"));
        map.put("obj_id", r.getString("obj_id"));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }
  public Map<String, List<Map<String, String>>> getParkingData(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map rt = new HashMap();
    List list = new ArrayList();
    DBRow[] yards = this.floorGoogleMapsMgrCc.getParkingData(ps_id);
    if (yards.length > 0) {
      for (int i = 0; i < yards.length; i++) {
        Map map = new HashMap();
        DBRow r = yards[i];
        map.put("latlng", r.getString("latlng"));
        map.put("obj_name", r.getString("obj_name"));
        map.put("obj_id", r.getString("obj_id"));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }

  @RequestMapping(value={"/dockInfoCotroller/queryDoorYardAndroid"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, List<Map<String, Object>>> getDocksYardDataAndroid(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception
  {
    Map rt = new HashMap();
    DBRow[] docks = this.floorGoogleMapsMgrCc.getDocksData(ps_id);
    DBRow[] yards = this.floorGoogleMapsMgrCc.getParkingData(ps_id);
    List dockArray = new ArrayList();
    List yardArray = new ArrayList();
    if (docks.length > 0) {
      for (int i = 0; i < docks.length; i++) {
        Map map = new HashMap();
        DBRow r = docks[i];
        map.put("latlng", r.getString("latlng", ""));
        map.put("obj_name", r.getString("obj_name", ""));
        map.put("obj_id", r.getString("obj_name", ""));
        dockArray.add(map);
      }
    }
    if (yards.length > 0) {
      for (int i = 0; i < yards.length; i++) {
        Map map = new HashMap();
        DBRow r = yards[i];
        map.put("latlng", r.getString("latlng", ""));
        map.put("obj_name", r.getString("obj_name", ""));
        map.put("obj_id", r.getString("obj_name", ""));
        yardArray.add(map);
      }
    }
    rt.put("dock", dockArray);
    rt.put("yard", yardArray);
    return rt;
  }

  @RequestMapping(value={"/dockInfoCotroller/getDocksParkingOccupancy"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow getDocksParkingOccupancy(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    DBRow result = new DBRow();
    try {
      DBRow[] datas = this.floorGoogleMapsMgrCc.getParkingDocksOccupancy(ps_id);
      if (datas.length > 0) {
        result.add("flag", "true");
        result.add("objs", datas);
      }
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("dockInfoCotroller.getDocksParkingOccupancy error:" + e);
    }
    return result;
  }

  @RequestMapping(value={"/dockInfoCotroller/queryDoorByPsId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getDocksByPsId(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    try
    {
      return this.floorGoogleMapsMgrCc.getDocksData(ps_id);
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("dockInfoCotroller.getDocksByPsId error:" + e);
    }
  }

  @RequestMapping(value={"/dockInfoCotroller/getDocksParkingOccupancyDetail"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<DBRow> getDocksParkingOccupancyDetail(@RequestParam(value="id", required=true) long id, @RequestParam(value="type", required=true) int type, @RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    int _type = 0;
    if (type == 3)
      _type = 1;
    else if (type == 4) {
      _type = 2;
    }
    DBRow[] rows = this.floorGoogleMapsMgrCc.getParkingDocksOccupancyDetail(id, _type);
    List list = new ArrayList();
    if (rows.length > 0) {
      Map map = new HashMap();
      for (int i = 0; i < rows.length; i++) {
        DBRow row = new DBRow();
        DBRow task = new DBRow();
        List taskList = new ArrayList();
        DBRow dbrow = rows[i];
        String equipId = dbrow.getString("relation_id");
        int number_status = dbrow.get("number_status", 0);
        String item_five = "";
        switch (number_status) {
        case 1:
          item_five = "Unprocess";
          break;
        case 2:
          item_five = "Processing";
          break;
        case 3:
          item_five = "Closed";
          break;
        case 4:
          item_five = "Exception";
          break;
        case 5:
          item_five = "Partially";
        }

        int number_type = dbrow.get("number_type", 0);
        String item_six = "";
        switch (number_type) {
        case 10:
          item_six = "Load";
          break;
        case 11:
          item_six = "Order";
          break;
        case 12:
          item_six = "CTNR";
          break;
        case 13:
          item_six = "BOL";
          break;
        case 14:
          item_six = "Delivery_Others";
          break;
        case 17:
          item_six = "PONO";
          break;
        case 18:
          item_six = "Pickup_Others";
        case 15:
        case 16:
        }task.add("number", dbrow.getString("number"));
        task.add("number_status", item_five);
        task.add("number_type", item_six);
        taskList.add(task);
        if (map.containsKey(equipId)) {
          ((List)map.get(equipId)).add(task);
          ((DBRow)list.get(list.size() - 1)).add("tasks", map.get(equipId));
        } else {
          int equipment_status = dbrow.get("equipment_status", 0);
          String item_one = "";
          switch (equipment_status) {
          case 1:
            item_one = "Unprocess";
            break;
          case 2:
            item_one = "Processing";
            break;
          case 3:
            item_one = "InYard";
            break;
          case 4:
            item_one = "Leaving";
            break;
          case 5:
            item_one = "Left";
          }

          String item_three = "";
          int equipment_type = dbrow.get("equipment_type", 0);
          if (equipment_type == EquipmentTypeKey.Tractor)
            item_three = "tractor";
          else if (equipment_type == EquipmentTypeKey.Container) {
            item_three = "container";
          }
          int occupy_status = dbrow.get("occupy_status", 0);
          String o_status = "";
          if (occupy_status == 1)
            o_status = "Reserved";
          else if (occupy_status == 2) {
            o_status = "Occupied";
          }
          int rel_type = dbrow.get("rel_type", 0);
          String item_four = "";
          switch (rel_type) {
          case 1:
            item_four = "Inbound";
            break;
          case 2:
            item_four = "Outbound";
            break;
          case 3:
            item_four = "In / Out";
            break;
          case 4:
            item_four = "Unknown";
            break;
          case 6:
            item_four = "Patrol Create";
            break;
          case 5:
            item_four = "CTNR";
            break;
          case 7:
            item_four = "Visitor / Parking";
          }

          String v = dbrow.getString("check_in_time");
          String v_ = DateUtil.showLocalparseDateTo24Hours(v, ps_id);
          row.add("check_in_time", v_);
          row.add("relation_id", dbrow.getString("relation_id"));
          row.add("relation_type", "Equipment");
          row.add("module_id", dbrow.getString("module_id"));
          row.add("resources_type", dbrow.getString("resources_type"));
          row.add("resources_id", dbrow.getString("resources_id"));
          row.add("occupy_status", o_status);
          row.add("equipment_status", item_one);
          row.add("equipment_number", dbrow.getString("equipment_number"));
          row.add("equipment_type", item_three);
          row.add("rel_type", item_four);
          row.add("tasks", taskList);
          map.put(equipId, taskList);
          list.add(row);
        }
      }
    }
    return list;
  }
  @RequestMapping(value={"/dockInfoCotroller/getDocksParkingOccupancyDetailAndroid"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<DBRow> getDocksParkingOccupancyDetailAndroid(@RequestParam(value="id", required=true) long id, @RequestParam(value="type", required=true) int type, @RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    int _type = 0;
    if (type == 3)
      _type = 1;
    else if (type == 4) {
      _type = 2;
    }
    DBRow[] rows = this.floorGoogleMapsMgrCc.getParkingDocksOccupancyDetail(id, _type);
    List list = new ArrayList();
    if (rows.length > 0) {
      Map map = new HashMap();
      for (int i = 0; i < rows.length; i++) {
        DBRow row = new DBRow();
        DBRow task = new DBRow();
        List taskList = new ArrayList();
        DBRow dbrow = rows[i];
        String equipId = dbrow.getString("relation_id");
        int number_status = dbrow.get("number_status", 0);

        int number_type = dbrow.get("number_type", 0);

        task.add("number", dbrow.getString("number"));

        task.add("number_status", number_status);

        task.add("number_type", number_type);
        taskList.add(task);
        if (map.containsKey(equipId)) {
          ((List)map.get(equipId)).add(task);
          ((DBRow)list.get(list.size() - 1)).add("tasks", map.get(equipId));
        } else {
          int equipment_status = dbrow.get("equipment_status", 0);
          String item_one = "";
          switch (equipment_status) {
          case 1:
            item_one = "Unprocess";
            break;
          case 2:
            item_one = "Processing";
            break;
          case 3:
            item_one = "InYard";
            break;
          case 4:
            item_one = "Leaving";
            break;
          case 5:
            item_one = "Left";
          }

          String item_three = "";
          int equipment_type = dbrow.get("equipment_type", 0);
          if (equipment_type == EquipmentTypeKey.Tractor)
            item_three = "tractor";
          else if (equipment_type == EquipmentTypeKey.Container) {
            item_three = "container";
          }
          int occupy_status = dbrow.get("occupy_status", 0);
          String o_status = "";
          if (occupy_status == 1)
            o_status = "Reserved";
          else if (occupy_status == 2) {
            o_status = "Occupied";
          }
          int rel_type = dbrow.get("rel_type", 0);
          String item_four = "";
          switch (rel_type) {
          case 1:
            item_four = "Inbound";
            break;
          case 2:
            item_four = "Outbound";
            break;
          case 3:
            item_four = "In / Out";
            break;
          case 4:
            item_four = "Unknown";
            break;
          case 6:
            item_four = "Patrol Create";
            break;
          case 5:
            item_four = "CTNR";
            break;
          case 7:
            item_four = "Visitor / Parking";
          }

          String v = dbrow.getString("check_in_time");
          String v_ = DateUtil.showLocalparseDateTo24Hours(v, ps_id);
          row.add("check_in_time", v_);
          row.add("relation_id", dbrow.getString("relation_id"));
          row.add("relation_type", "Equipment");
          row.add("module_id", dbrow.getString("module_id"));
          row.add("resources_type", dbrow.getString("resources_type"));
          row.add("resources_id", dbrow.getString("resources_id"));
          row.add("occupy_status", o_status);
          row.add("equipment_status", item_one);
          row.add("equipment_number", dbrow.getString("equipment_number"));
          row.add("equipment_type", item_three);
          row.add("rel_type", item_four);
          row.add("tasks", taskList);
          map.put(equipId, taskList);
          list.add(row);
        }
      }
    }
    return list;
  }
  @RequestMapping(value={"/dockInfoCotroller/setStorageDoorStatus"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Map<String, String> setStorageDoorStatus(@RequestParam(value="sd_id", required=true) long sd_id, @RequestParam(value="status", required=true) int status) throws Exception {
    Map map = new HashMap();
    try {
      int id = this.floorGoogleMapsMgrCc.setStorageDoorStatus(sd_id, status);
      map.put("flag", "true");
      map.put("id", id + "");
      return map;
    } catch (Exception e) {
      map.put("flag", "false");
      e.printStackTrace();
    }return map;
  }

  @RequestMapping(value={"/dockInfoCotroller/getDocksCounts"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow getDocksCounts(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception
  {
    DBRow map = new DBRow();
    try {
      DBRow[] rows = this.floorGoogleMapsMgrCc.getAreaDoorCountsByPsId(ps_id);
      map.add("flag", "true");
      map.add("datas", rows);

      return map;
    }
    catch (Exception e)
    {
      e = 
        e;

      map.add("flag", "false");
      e.printStackTrace();

      return map; } finally {  } return map;
  }
}