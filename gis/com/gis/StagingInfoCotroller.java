package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StagingInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/stagingInfoCotroller/queryStagingLayerDatas"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] queryStagingLayerDatas(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    return this.floorGoogleMapsMgrCc.getStagingInfoByPsId(ps_id);
  }

  public Map<String, List<Map<String, String>>> getStagingDatas(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map rt = new HashMap();
    List list = new ArrayList();
    DBRow[] stagings = this.floorGoogleMapsMgrCc.getStagingInfoByPsId(ps_id);
    if (stagings.length > 0) {
      for (int i = 0; i < stagings.length; i++) {
        Map map = new HashMap();
        DBRow r = stagings[i];
        map.put("latlng", r.getString("latlng"));
        map.put("obj_name", r.getString("obj_name"));
        map.put("obj_id", r.getString("obj_id"));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }

  @RequestMapping(value={"/stagingInfoCotroller/queryContainerCounts"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryContainerCounts(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="ic_id", required=true) long ic_id) throws Exception {
    DBRow data = new DBRow();
    try {
      DBRow[] rows = this.floorGoogleMapsMgrCc.queryContainerCounts(ic_id, ps_id);
      if (rows.length > 0) {
        data.add("rows", rows);
        data.add("flag", "true");
      } else {
        data.add("flag", "nodata");
      }
      return data;
    } catch (Exception e) {
      data.add("flag", "false");
      e.printStackTrace();
    }return data;
  }
}