package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.app.util.Environment;
import com.cwc.db.DBRow;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jxl.Cell;
import jxl.CellType;
import jxl.NumberCell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ExcelCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @Autowired
  private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;

  @RequestMapping(value={"/excelCotroller/queryAreaPerson"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public boolean exportExcel(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmss");
    String dateString = sdf.format(new Date());
    String path = System.getProperty("java.io.tmpdir") + "export" + File.separator;

    File file = new File(path);

    if ((!file.exists()) && (!file.isDirectory())) {
      file.mkdir();
    } else {
      file.delete();
      file.mkdir();
    }
    String fileName = ps_id + "_" + dateString + ".xls";
    String realPath = path + fileName;
    boolean flag = createExcelNew(ps_id, realPath);
    return flag;
  }

  public boolean createExcelNew(long ps_id, String path)
    throws Exception
  {
    boolean rt = false;
    try {
      WritableWorkbook workbook = null;
      String outputFile = path;

      OutputStream os = new FileOutputStream(outputFile);
      workbook = Workbook.createWorkbook(os);
      WritableSheet webcam = workbook.createSheet("Webcam", 0);
      WritableSheet parking = workbook.createSheet("Parking", 0);
      WritableSheet staging = workbook.createSheet("Staging", 0);
      WritableSheet docks = workbook.createSheet("Docks", 0);
      WritableSheet location = workbook.createSheet("Location", 0);
      WritableSheet area = workbook.createSheet("Zone", 0);
      WritableSheet base = workbook.createSheet("Base", 0);
      DBRow[] baseDate = this.floorGoogleMapsMgrCc.getExcelBaseData(ps_id);
      DBRow[] areaData = this.floorGoogleMapsMgrCc.getExcelAreaData(ps_id);
      DBRow[] docksData = this.floorGoogleMapsMgrCc.getExcelDocksData(ps_id);
      DBRow[] parkingData = this.floorGoogleMapsMgrCc.getExcelParkingData(ps_id);
      DBRow[] locationData = this.floorGoogleMapsMgrCc.getExcelLocationData(ps_id);
      DBRow[] stagingData = this.floorGoogleMapsMgrCc.getExcelStagingData(ps_id);
      DBRow[] webcamData = this.floorGoogleMapsMgrCc.getExcelWebcamData(ps_id);
      String[] str = new String[9];
      String[] webcamStr = new String[11];
      int num = 1;
      Pattern pattern = Pattern.compile("^[0-9]+(.[0-9]*)?$");
      for (int m = 0; m < baseDate.length; m++) {
        str[0] = baseDate[m].getString("type", "");
        str[1] = baseDate[m].getString("param", "");
        str[2] = baseDate[m].getString("lng", "");
        str[3] = baseDate[m].getString("lat", "");
        for (int n = 0; n < str.length; n++) {
          if ((n == 0) || (n == 1)) {
            Matcher match = pattern.matcher(str[n]);
            if (!match.matches()) {
              Label label = new Label(n, num, str[n]);
              base.addCell(label);
            } else {
              Number number = new Number(n, num, Double.parseDouble(str[n]));

              base.addCell(number);
            }
          } else if ((n == 2) || (n == 3)) {
            if ((str[n] != null) && (!str[n].equals(""))) {
              Number number = new Number(n, num, Double.parseDouble(str[n]));

              base.addCell(number);
            } else {
              Label label = new Label(n, num, str[n]);
              base.addCell(label);
            }
          }
        }
        num++;
      }
      num = 1;
      String[] head = { "Type", "Zone Name", "Titles", "Docks", "X", "Y", "X-Length", "Y-Length", "Angle" };
      for (int i = 0; i < head.length; i++) {
        area.addCell(new Label(i, num, head[i]));
      }
      if (areaData.length > 0) {
        num++;
        for (int i = 0; i < areaData.length; i++) {
          str[0] = "Zone";
          str[1] = areaData[i].getString("area_name");
          str[2] = areaData[i].getString("title_name");
          str[3] = areaData[i].getString("doorId");
          str[4] = areaData[i].getString("x");
          str[5] = areaData[i].getString("y");
          str[6] = areaData[i].getString("height");
          str[7] = areaData[i].getString("width");
          str[8] = areaData[i].getString("angle");
          for (int j = 0; j < str.length; j++) {
            if ((j == 0) || (j == 1) || (j == 2) || (j == 3)) {
              Label label = new Label(j, num, str[j].toString());
              area.addCell(label);
            } else if ((j == 4) || (j == 5) || (j == 6) || (j == 7) || (j == 8))
            {
              if ((str[j] != null) && (!str[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(str[j]));

                area.addCell(number);
              }

            }

          }

          num++;
        }
      }

      num = 1;
      String[] docks_head = { "Type", "Zone", "Dock Name", "Storage Type", "X", "Y", "X-Length", "Y-Length", "Angle" };
      for (int i = 0; i < head.length; i++) {
        docks.addCell(new Label(i, num, docks_head[i]));
      }
      if (docksData.length > 0)
      {
        num++;
        for (int i = 0; i < docksData.length; i++) {
          str[0] = "Docks";
          str[1] = docksData[i].getString("area_name");
          str[2] = docksData[i].getString("doorId");
          str[3] = "";
          str[4] = docksData[i].getString("x");
          str[5] = docksData[i].getString("y");
          str[6] = docksData[i].getString("height");
          str[7] = docksData[i].getString("width");
          str[8] = docksData[i].getString("angle");
          for (int j = 0; j < str.length; j++) {
            if ((j == 0) || (j == 1)) {
              Label label = new Label(j, num, str[j].toString());
              docks.addCell(label);
            } else if ((j == 2) || (j == 4) || (j == 5) || (j == 6) || (j == 7) || (j == 8))
            {
              if ((str[j] != null) && (!str[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(str[j]));

                docks.addCell(number);
              }

            }

          }

          num++;
        }
      }

      num = 1;
      String[] location_head = { "Type", "Zone", "Location Name", "Storage Type", "X", "Y", "X-Length", "Y-Length", "Angle" };
      for (int i = 0; i < head.length; i++) {
        location.addCell(new Label(i, num, location_head[i]));
      }
      if (locationData.length > 0) {
        num++;
        for (int i = 0; i < locationData.length; i++) {
          str[0] = "Location";
          str[1] = locationData[i].getString("area_name");
          str[2] = locationData[i].getString("slc_position");
          str[3] = locationData[i].getString("is_three_dimensional");
          str[4] = locationData[i].getString("x");
          str[5] = locationData[i].getString("y");
          str[6] = locationData[i].getString("height");
          str[7] = locationData[i].getString("width");
          str[8] = locationData[i].getString("angle");
          for (int j = 0; j < str.length; j++) {
            if ((j == 0) || (j == 1) || (j == 2) || (j == 3)) {
              if ((str[3].equals("0")) || (str[3] == "0"))
                str[3] = "2D";
              else if ((str[3].equals("1")) || (str[3] == "1")) {
                str[3] = "3D";
              }
              Label label = new Label(j, num, str[j].toString());
              location.addCell(label);
            } else if ((j == 4) || (j == 5) || (j == 6) || (j == 7) || (j == 8))
            {
              if ((str[j] != null) && (!str[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(str[j]));

                location.addCell(number);
              }

            }

          }

          num++;
        }

      }

      num = 1;
      String[] parking_head = { "Type", "Zone", "Parking Name", "Storage Type", "X", "Y", "X-Length", "Y-Length", "Angle" };
      for (int i = 0; i < head.length; i++) {
        parking.addCell(new Label(i, num, parking_head[i]));
      }
      if (parkingData.length > 0) {
        num++;
        for (int i = 0; i < parkingData.length; i++) {
          str[0] = "Parking";
          str[1] = parkingData[i].getString("area_name");
          str[2] = parkingData[i].getString("yc_no");
          str[3] = "";
          str[4] = parkingData[i].getString("x");
          str[5] = parkingData[i].getString("y");
          str[6] = parkingData[i].getString("height");
          str[7] = parkingData[i].getString("width");
          str[8] = parkingData[i].getString("angle");
          for (int j = 0; j < str.length; j++) {
            if ((j == 0) || (j == 1) || (j == 2)) {
              Label label = new Label(j, num, str[j].toString());
              parking.addCell(label);
            } else if ((j == 4) || (j == 5) || (j == 6) || (j == 7) || (j == 8))
            {
              if ((str[j] != null) && (!str[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(str[j]));

                parking.addCell(number);
              }

            }

          }

          num++;
        }
      }

      num = 1;
      String[] staging_head = { "Type", "Docks", "Staging Name", "Storage Type", "X", "Y", "X-Length", "Y-Length", "Angle" };
      for (int i = 0; i < head.length; i++) {
        staging.addCell(new Label(i, num, staging_head[i]));
      }

      if (stagingData.length > 0) {
        num++;
        for (int i = 0; i < stagingData.length; i++) {
          str[0] = "Staging";
          str[1] = stagingData[i].getString("doorId");
          str[2] = stagingData[i].getString("location_name");
          str[3] = "";
          str[4] = stagingData[i].getString("x");
          str[5] = stagingData[i].getString("y");
          str[6] = stagingData[i].getString("height");
          str[7] = stagingData[i].getString("width");
          str[8] = stagingData[i].getString("angle");
          for (int j = 0; j < str.length; j++) {
            if ((j == 0) || (j == 1) || (j == 2)) {
              Label label = new Label(j, num, str[j].toString());
              staging.addCell(label);
            } else if ((j == 4) || (j == 5) || (j == 6) || (j == 7) || (j == 8))
            {
              if ((str[j] != null) && (!str[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(str[j]));

                staging.addCell(number);
              }

            }

          }

          num++;
        }
      }
      num = 1;
      String[] webcam_head = { "Type", "IP", "Port", "User", "Password", "X", "Y", "Inner_Radius", "Outer_Radius", "S_Degree", "E_Degree" };
      for (int i = 0; i < head.length; i++) {
        webcam.addCell(new Label(i, num, webcam_head[i]));
      }
      if (webcamData.length > 0) {
        num++;
        for (int i = 0; i < webcamData.length; i++) {
          webcamStr[0] = "Webcam";
          webcamStr[1] = webcamData[i].getString("ip");
          webcamStr[2] = webcamData[i].getString("port");
          webcamStr[3] = webcamData[i].getString("user");
          webcamStr[4] = webcamData[i].getString("password");
          webcamStr[5] = webcamData[i].getString("x");
          webcamStr[6] = webcamData[i].getString("y");
          webcamStr[7] = webcamData[i].getString("inner_radius");
          webcamStr[8] = webcamData[i].getString("outer_radius");
          webcamStr[9] = webcamData[i].getString("s_degree");
          webcamStr[10] = webcamData[i].getString("e_degree");

          for (int j = 0; j < webcamStr.length; j++)
          {
            if ((j == 0) || (j == 1) || (j == 3) || (j == 4)) {
              Label label = new Label(j, num, webcamStr[j].toString());
              webcam.addCell(label);
            }
            else if ((j == 2) || (j == 5) || (j == 6) || (j == 7) || (j == 8) || (j == 9) || (j == 10))
            {
              if ((webcamStr[j] != null) && (!webcamStr[j].equals(""))) {
                Number number = new Number(j, num, Double.parseDouble(webcamStr[j]));

                webcam.addCell(number);
              }

            }

          }

          num++;
        }

      }

      workbook.write();

      workbook.close();
      os.flush();
      os.close();
      rt = true;

      return rt;
    }
    catch (FileNotFoundException ex)
    {
      ex = 
        ex;

      ex.printStackTrace();
      rt = false;

      return rt;
    }
    catch (IOException ex)
    {
      ex = 
        ex;

      ex.printStackTrace();
      rt = false;

      return rt;
    }
    catch (WriteException ex)
    {
      ex = 
        ex;

      ex.printStackTrace();
      rt = false;

      return rt; } finally {  } return rt;
  }

  public List<DBRow> parseExcel(String filename, String newFileName, long storageId, String storageTitle)
    throws Exception
  {
    List rows = new ArrayList();
    int currLine = 0;
    String currSheet = "";
    String path = Environment.getHome() + "upl_imags_tmp/" + filename;
    InputStream is = new FileInputStream(path);
    try {
      Workbook rwb = Workbook.getWorkbook(is);
      Sheet rs = null;

      Sheet[] sheets = rwb.getSheets();

      String[] sheetNames = rwb.getSheetNames();
      String[] arr$ = sheetNames; int len$ = arr$.length; int i$ = 0; if (i$ < len$) { String s = arr$[i$];
        if ("base".equals(s.toLowerCase())) {
          rs = rwb.getSheet(s);
        }
        else {
          DBRow errInfo = new DBRow();
          errInfo.add("err", "Not found base data");
          rows.clear();
          rows.add(errInfo);
          return rows;
        }
      }
      double maxX = 0.0D;
      double maxY = 0.0D;
      int pCount = 0;
      DBRow whRow = new DBRow();
      char[] whChar = new char[26];
      for (int i = 0; i < rs.getRows(); i++) {
        currLine = i;
        currSheet = "Base";
        String type = rs.getCell(0, i).getContents().trim().toLowerCase();
        DBRow row = new DBRow();
        if ("warehouse".equals(type)) {
          char pName = rs.getCell(1, i).getContents().trim().toUpperCase().toCharArray()[0];
          row.add("folder_name", "base");
          row.add("area_name", "warehouse");
          row.add("placemark_name", pName + "");
          row.add("ps_id", storageId);
          if (((pName >= 'A') && (pName <= 'Z')) || ((pName >= 'a') && (pName <= 'z'))) {
            whRow.add(pName + "", ((NumberCell)rs.getCell(2, i)).getValue() + "," + ((NumberCell)rs.getCell(3, i)).getValue());
            whChar[pCount] = pName;
            row.add("x", ((NumberCell)rs.getCell(2, i)).getValue());
            row.add("y", ((NumberCell)rs.getCell(3, i)).getValue());
          }
          rows.add(row);
          pCount++;
        } else if ("maxx".equals(type)) {
          row.add("folder_name", "base");
          row.add("area_name", "maxX");
          maxX = ((NumberCell)rs.getCell(1, i)).getValue();
          row.add("placemark_name", maxX);
          row.add("ps_id", storageId);
          rows.add(row);
        } else if ("maxy".equals(type)) {
          row.add("folder_name", "base");
          row.add("area_name", "maxY");
          maxY = ((NumberCell)rs.getCell(1, i)).getValue();
          row.add("placemark_name", maxY);
          row.add("ps_id", storageId);
          rows.add(row);
        }

      }

      if ((pCount >= 4) && (maxX > 0.0D) && (maxY > 0.0D))
      {
        whChar = Arrays.copyOfRange(whChar, 0, pCount);
        Arrays.sort(whChar);
      } else {
        DBRow errInfo = new DBRow();
        errInfo.add("err", "Warehouse base data error");
        rows.clear();
        rows.add(errInfo);
        return rows;
      }

      String pointA = whRow.get(whChar[0] + "", "");
      String pointB = whRow.get(whChar[1] + "", "");
      String pointD = whRow.get(whChar[(pCount - 1)] + "", "");
      double aLng = Double.parseDouble(pointA.split(",")[0]);
      double aLat = Double.parseDouble(pointA.split(",")[1]);
      double bLng = Double.parseDouble(pointB.split(",")[0]);
      double bLat = Double.parseDouble(pointB.split(",")[1]);
      double dLng = Double.parseDouble(pointD.split(",")[0]);
      double dLat = Double.parseDouble(pointD.split(",")[1]);

      double ratio = Math.hypot(aLat - bLat, aLng - bLng) / maxX;
      double radX = Math.atan((aLat - bLat) / (aLng - bLng));
      double radY = Math.atan((aLat - dLat) / (aLng - dLng));
      if (radY <= 0.0D) {
        radY += 3.141592653589793D;
      }
      radY -= 1.570796326794897D;
      double radN = radX - radY;
      double maxY_map = Math.hypot(aLat - dLat, aLng - dLng) / ratio;
      for (Sheet s : sheets) {
        rs = s;
        currSheet = rs.getName().toLowerCase();
        for (int i = 0; i < rs.getRows(); i++) {
          currLine = i;
          String sheetName = rs.getName().toLowerCase();
          if (!"base".equals(sheetName))
          {
            if ("webcam".equals(sheetName)) {
              String type = rs.getCell(0, i).getContents().trim().toLowerCase();
              if ("webcam".equals(type)) {
                DBRow row = new DBRow();
                String camIp = rs.getCell(1, i).getContents().trim();
                String camPort = rs.getCell(2, i).getContents().trim();
                String camUser = rs.getCell(3, i).getContents().trim();
                String camPass = rs.getCell(4, i).getContents().trim();
                String x = rs.getCell(5, i).getContents().trim();
                String y = rs.getCell(6, i).getContents().trim();
                String inner_radius = rs.getCell(7, i).getContents().trim();
                String outer_radius = rs.getCell(8, i).getContents().trim();
                String s_degree = rs.getCell(9, i).getContents().trim();
                String e_degree = rs.getCell(10, i).getContents().trim();
                String latlng = this.floorGoogleMapsMgrCc.convertCoordinateToLatlng(storageId, Long.parseLong(x), Long.parseLong(y));
                row.add("latlng", latlng);
                row.add("ip", camIp);
                row.add("ps_id", storageId);
                row.add("port", camPort);
                row.add("user", camUser);
                row.add("password", camPass);
                row.add("x", x);
                row.add("y", y);
                row.add("area_name", inner_radius);
                row.add("placemark_name", outer_radius);
                row.add("width", s_degree);
                row.add("height", e_degree);
                row.add("folder_name", "webcam");
                row.add("source", newFileName);
                rows.add(row);
              }
            } else {
              DBRow row = new DBRow();
              row.add("ps_id", storageId);
              String type = rs.getCell(0, i).getContents().trim().toLowerCase();
              String areaName = rs.getCell(1, i).getContents().trim();
              String areaNext = rs.getCell(2, i).getContents().trim();
              if ("zone".equals(type)) {
                type = "area";
              }

              if (("area".equals(type)) || ("docks".equals(type)) || ("staging".equals(type)) || ("location".equals(type)) || ("parking".equals(type)))
              {
                String reg = "\\d+";
                Pattern pat = Pattern.compile(reg);
                String dimensional = rs.getCell(3, i).getContents().trim().toUpperCase();
                int is3D = "3D".equals(dimensional) ? 1 : 0;

                if ((rs.getCell(4, i).getType() != CellType.EMPTY) && (rs.getCell(5, i).getType() != CellType.EMPTY) && (rs.getCell(6, i).getType() != CellType.EMPTY) && (rs.getCell(7, i).getType() != CellType.EMPTY))
                {
                  double X = Math.rint(((NumberCell)rs.getCell(4, i)).getValue() * 100.0D) / 100.0D;
                  double Y = Math.rint(((NumberCell)rs.getCell(5, i)).getValue() * 100.0D) / 100.0D;
                  double L = Math.rint(((NumberCell)rs.getCell(6, i)).getValue() * 100.0D) / 100.0D;
                  double W = Math.rint(((NumberCell)rs.getCell(7, i)).getValue() * 100.0D) / 100.0D;
                  row.add("x", X);
                  row.add("y", Y);
                  row.add("width", W);
                  row.add("height", L);
                  double angle = 0.0D;
                  if ((rs.getColumns() >= 9) && (rs.getCell(8, i).getType() != CellType.EMPTY)) {
                    angle = Math.rint(((NumberCell)rs.getCell(8, i)).getValue() * 100.0D) / 100.0D;
                  }

                  double[] XS = { X, X + L, X + L, X };
                  double[] YS = { Y, Y, Y + W, Y + W };

                  if (angle != 0.0D) {
                    for (int j = 1; j < 4; j++) {
                      XS[j] -= XS[0];
                      YS[j] -= YS[0];
                      double r = Math.toRadians(angle) + Math.atan(YS[j] / XS[j]);
                      double l = Math.hypot(XS[j], YS[j]);
                      XS[j] = (l * Math.cos(r) + XS[0]);
                      YS[j] = (l * Math.sin(r) + YS[0]);
                      if (Double.isNaN(XS[j])) {
                        XS[j] = XS[0];
                      }
                      if (Double.isNaN(YS[j])) {
                        YS[j] = YS[0];
                      }
                    }
                  }
                  row.add("angle", angle);
                  String cds = "";

                  for (int j = 0; j < 4; j++) {
                    YS[j] /= maxY / maxY_map;

                    XS[j] += YS[j] * Math.sin(radN) * Math.cos(radN);
                    YS[j] *= Math.cos(radN);

                    int sign = XS[j] > 0.0D ? 1 : -1;
                    double rad = radX + Math.atan(YS[j] / XS[j]);
                    double len = Math.hypot(XS[j], YS[j]);
                    double lat = sign * len * ratio * Math.sin(rad) + aLat;
                    double lng = sign * len * ratio * Math.cos(rad) + aLng;
                    if (Double.isNaN(lat)) {
                      lat = aLat;
                    }
                    if (Double.isNaN(lng)) {
                      lng = aLng;
                    }
                    cds = cds + lng + "," + lat + ",0.0 ";
                  }
                  cds = cds + cds.split(" ")[0];

                  row.add("latLng", cds);
                  String pName = null;
                  row.add("folder_name", type);
                  row.add("area_name", areaName.toUpperCase());
                  row.add("placemark_name", areaNext.toUpperCase());
                  row.add("source", newFileName);
                  if ("area".equals(type))
                    row.add("is_three_dimensional", dimensional);
                  else {
                    row.add("is_three_dimensional", is3D);
                  }

                  rows.add(row);
                }
              }
            }
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      DBRow errInfo = new DBRow();
      errInfo.add("err", "Can't parse data at sheet[" + currSheet + "] line " + (currLine + 1));
      rows.clear();
      rows.add(errInfo);
      is.close();
      return rows;
    }
    is.close();
    return rows;
  }
}