package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RoadInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/roadInfoCotroller/queryRoutePath"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow[] getRoutePath(@RequestBody Map<String, Object> map)
    throws Exception
  {
    long psId = Long.parseLong(map.get("ps_id").toString());
    int fromType = Integer.parseInt(map.get("from_type").toString());
    String fromPosition = map.get("from_position").toString();
    String fromLatlng = map.get("from_latlng").toString();
    int toType = Integer.parseInt(map.get("to_type").toString());
    String toPosition = map.get("to_position").toString();
    String toLatlng = map.get("to_latlng").toString();
    DBRow[] result = null;
    String fRoadStr = null;
    String tRoadStr = null;
    long fRid = 0L;
    long tRid = 0L;

    if (fromType == 1) {
      DBRow fLoc = this.floorGoogleMapsMgrCc.getLocationByPosition(psId, fromPosition);
      fRoadStr = fLoc.getString("entery_point") + "," + fLoc.getString("road_point");
      fRid = fLoc.get("entery_road", 0L);
    } else {
      String nearRoad = this.floorGoogleMapsMgrCc.getNearestRoad(psId, fromLatlng).getString("road");
      fRoadStr = nearRoad.split(",")[1];
      fRid = Integer.parseInt(nearRoad.split(",")[0]);
    }

    if (toType == 1) {
      DBRow tLoc = this.floorGoogleMapsMgrCc.getLocationByPosition(psId, toPosition);
      tRoadStr = tLoc.getString("road_point") + "," + tLoc.getString("entery_point");
      tRid = tLoc.get("entery_road", 0L);
    } else {
      String nearRoad = this.floorGoogleMapsMgrCc.getNearestRoad(psId, toLatlng).getString("road");
      tRoadStr = nearRoad.split(",")[1];
      tRid = Integer.parseInt(nearRoad.split(",")[0]);
    }

    if (fRid == tRid) {
      DBRow r = new DBRow();
      r.add("path", fRoadStr + "," + tRoadStr);
      result = new DBRow[] { r };
    } else {
      DBRow fRoad = this.floorGoogleMapsMgrCc.getStorageRoadByRid(fRid);
      DBRow tRoad = this.floorGoogleMapsMgrCc.getStorageRoadByRid(tRid);
      long fSource = fRoad.get("source", 0L);
      long fTarget = fRoad.get("target", 0L);
      long tSource = tRoad.get("source", 0L);
      long tTarget = tRoad.get("target", 0L);

      String pathStr = this.floorGoogleMapsMgrCc.getDijkstraByPointId(psId, fSource + "", tSource + "," + tTarget).getString("path");
      pathStr = pathStr + " " + this.floorGoogleMapsMgrCc.getDijkstraByPointId(psId, new StringBuilder().append(fTarget).append("").toString(), new StringBuilder().append(tSource).append(",").append(tTarget).toString()).getString("path");

      String[] paths = pathStr.split(" ");
      String[] data = new String[4];
      int count = 0;
      for (String s : paths) {
        if (!"0".equals(s)) {
          DBRow[] points = this.floorGoogleMapsMgrCc.getRoutePathPoints(psId, s);

          String[] ps = s.split(",");
          String pStr = fRoadStr;
          for (String p : ps) {
            for (DBRow point : points) {
              String p_id = point.getString("point_id");
              if (p.equals(p_id)) {
                String ss = null;
                Object obj = point.getValue("latlng");
                if ((obj instanceof String))
                  ss = (String)obj;
                else {
                  ss = byteArrayToString((byte[])obj);
                }
                pStr = pStr + "," + ss;
              }
            }
          }
          pStr = pStr + "," + tRoadStr;
          data[(count++)] = pStr;
        }
      }

      result = new DBRow[count];
      for (int i = 0; i < count; i++) {
        DBRow r = new DBRow();
        r.add("path", data[i]);
        result[i] = r;
      }
    }
    return result;
  }

  public String byteArrayToString(byte[] a) {
    StringBuffer sb = new StringBuffer();
    for (int i : a) {
      sb.append((char)i);
    }
    return sb.toString();
  }

  @RequestMapping(value={"/roadInfoCotroller/deleteRoad"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public boolean deleteRoad(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="r_id", required=true) long r_id) throws Exception {
    boolean rt = false;
    try {
      int id = this.floorGoogleMapsMgrCc.deleteRoadByRid(ps_id, r_id);
      if (id != 0) {
        rt = true;
      }

      return rt;
    }
    catch (Exception e)
    {
      e = 
        e;

      e.printStackTrace();

      return rt; } finally {  } return rt;
  }
  @Transactional
  @RequestMapping(value={"/roadInfoCotroller/savaRoadAndPoint"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public boolean savaRoadAndPoint(@RequestBody Map<String, Object> map) throws Exception {
    boolean rt = false;
    try {
      int psId = Integer.parseInt(map.get("psid").toString());
      Map data = (Map)map.get("data");
      List roadJson = (List)data.get("road");
      List pointJson = (List)data.get("point");
      Map pointIdMap = new HashMap();
      if (pointJson.size() > 0) {
        for (int i = 0; i < pointJson.size(); i++) {
          Map p = (Map)pointJson.get(i);
          String status = p.get("status").toString();
          if (status.equals("1")) {
            String tempId = p.get("id").toString();
            String geomStr = p.get("geom_str").toString();
            DBRow row = new DBRow();
            row.add("ps_id", psId);
            row.add("geom", geomStr);
            long id = this.floorGoogleMapsMgrCc.addStorageRoadPoint(row);
            pointIdMap.put(tempId, Long.valueOf(id));
          } else if (!status.equals("2"))
          {
            if (status.equals("3")) {
              long id = Integer.parseInt(p.get("id").toString());
              this.floorGoogleMapsMgrCc.deleteStorageRoadPointById(id);
            }
          }
        }
      }
      if (roadJson.size() > 0)
        for (int i = 0; i < roadJson.size(); i++) {
          Map r = (Map)roadJson.get(i);
          String status = r.get("status").toString();
          if (status.equals("1")) {
            String geomStr = r.get("geom_str").toString();
            String sourceStr = r.get("source").toString();
            String targetStr = r.get("target").toString();

            long source = 0L;
            long target = 0L;

            if (pointIdMap.containsKey(sourceStr))
              source = ((Long)pointIdMap.get(sourceStr)).longValue();
            else {
              source = Long.parseLong(r.get("source").toString());
            }
            if (pointIdMap.containsKey(targetStr))
              target = ((Long)pointIdMap.get(targetStr)).longValue();
            else {
              target = Long.parseLong(r.get("target").toString());
            }

            DBRow row = new DBRow();
            row.add("ps_id", psId);
            row.add("source", source);
            row.add("target", target);
            row.add("geom", geomStr);
            this.floorGoogleMapsMgrCc.addStorageRoad(row);
          } else if (!status.equals("2"))
          {
            if (status.equals("3")) {
              long id = Long.parseLong(r.get("id").toString());
              this.floorGoogleMapsMgrCc.deleteStorageRoadById(id);
            }
          }
        }
      rt = true;

      return rt;
    }
    catch (Exception e)
    {
      e = 
        e;

      rt = false;
      e.printStackTrace();

      return rt; } finally {  } return rt;
  }

  @RequestMapping(value={"/roadInfoCotroller/queryRoadData"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryRoadData(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="main", required=false) long m, @RequestParam(value="entery", required=false) long e, @RequestParam(value="point", required=false) long p)
    throws Exception
  {
    DBRow road = new DBRow();
    if (m != 0L) {
      DBRow[] mainRoad = this.floorGoogleMapsMgrCc.getStorageRoadByPsid(ps_id);
      for (DBRow r : mainRoad) {
        Object v = r.getValue("geom");
        if ((v instanceof byte[])) {
          String s = new String((byte[])v, "iso-8859-1");
          r.put("geom", s);
        }
      }
      road.add("main", mainRoad);
    }
    if (e != 0L) {
      DBRow[] locRoad = this.floorGoogleMapsMgrCc.getLocationEnteryRoadByPsid(ps_id);
      road.add("loc_entery", locRoad);
    }
    if (p != 0L) {
      DBRow[] point = this.floorGoogleMapsMgrCc.getStorageRoadPointByPsid(ps_id);
      for (DBRow r : point) {
        Object v = r.getValue("geom");
        if ((v instanceof byte[])) {
          String s = new String((byte[])v, "iso-8859-1");
          r.put("geom", s);
        }
      }
      road.add("point", point);
    }

    DBRow data = new DBRow();
    data.add("flag", "true");
    data.add("road", road);
    return data;
  }
}