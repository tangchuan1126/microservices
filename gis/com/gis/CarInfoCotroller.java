package com.gis;

import com.cwc.app.floor.api.zwb.FloorCheckInMgrZwb;
import com.cwc.app.util.DateUtil;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import com.gis.model.HisRepo;
import com.gis.model.Lasthis;
import com.gis.model.LasthisRepo;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarInfoCotroller
{

  @Autowired
  private LasthisRepo lasthisRepo;

  @Autowired
  private HisRepo hisRepo;

  @Autowired
  private FloorCheckInMgrZwb floorCheckInMgrZwb;

  private Map<String, Object> convertResultPage(Page<?> page)
  {
    PageCtrl pc = new PageCtrl();

    pc.setAllCount((int)page.getTotalElements());
    pc.setPageCount(page.getTotalPages());
    pc.setPageNo(page.getNumber() + 1);
    pc.setPageSize(page.getSize());
    pc.setRowCount(page.getNumberOfElements());

    Map result = new HashMap();
    result.put("items", page.getContent());
    result.put("pageCtrl", pc);

    return result;
  }

  @RequestMapping(value={"/carInfoCotroller/list"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Object> list(@RequestParam(value="pageNo", defaultValue="1") int pageNo, @RequestParam(value="pageSize", defaultValue="20") int pageSize)
  {
    Pageable pageReq = new PageRequest(pageNo - 1, pageSize);

    return convertResultPage(this.lasthisRepo.findAll(pageReq));
  }

  @RequestMapping(value={"/carInfoCotroller/queryLasthis"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<List<?>> queryLasthis(@RequestParam(value="aids", required=true) String aids, @RequestParam(value="isl", defaultValue="true") boolean isl)
  {
    List list = new ArrayList();
    String[] aidArray = aids.split(",");
    if (aidArray.length > 0) {
      for (int i = 0; i < aidArray.length; i++) {
        List result = this.lasthisRepo.queryLasthis(aidArray[i], isl);
        if (!result.isEmpty()) {
          list.add(this.lasthisRepo.queryLasthis(aidArray[i], isl));
        }
      }
    }

    return list;
  }

  @RequestMapping(value={"/public/gps/queryLastPosition/{imei}"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Object> queryLastPosition(HttpServletResponse res, @PathVariable("imei") String imei) throws Exception
  {
    Map map = new HashMap();

    DBRow asset = this.floorCheckInMgrZwb.findAssetByGPSNumber(imei);
    if (asset != null) {
      String aid = asset.getString("id");
      List result = this.lasthisRepo.queryLasthis(aid, true);
      if ((result != null) && (result.size() > 0)) {
        Lasthis his = (Lasthis)result.get(0);
        map.put("timezone", "UTC");
        map.put("time", DateUtil.FormatDatetime("yyyy-MM-dd HH:mm:ss", new Date(his.getT())));
        map.put("lat", Double.valueOf(his.getLat()));
        map.put("lng", Double.valueOf(his.getLon()));
        map.put("timezone", "UTC");
      } else {
        map.put("error", "No record");
      }
    } else {
      map.put("error", "Gps device not exist in SYNC");
    }
    map.put("imei", imei);
    return map;
  }

  @RequestMapping(value={"/carInfoCotroller/queryHis"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<?> queryHis(@RequestParam(value="aid", required=true) String aid, @RequestParam(value="stl", required=true) String starttime, @RequestParam(value="etl", required=true) String endtime, @RequestParam(value="timeZone", required=true) long timeZone, @RequestParam(value="isl", defaultValue="true") boolean isl)
  {
    List list = new ArrayList();
    try
    {
      int offset = Calendar.getInstance().getTimeZone().getOffset(0L);
      long tzOffset = timeZone * 60L * 60L * 1000L;
      long stl = DateUtil.getDate2LongTime(starttime) - tzOffset + offset;
      long etl = DateUtil.getDate2LongTime(endtime) - tzOffset + offset;
      list = this.hisRepo.queryHis(aid, stl, etl, isl);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return list;
  }
}