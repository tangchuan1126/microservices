package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InventoryCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/inventoryCotroller/queryContainer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyContainer(@RequestParam(value="ic_id", required=true) long ic_id, @RequestParam(value="stagingId", required=true) long stagingId)
    throws Exception
  {
    return this.floorGoogleMapsMgrCc.selectContainerPlates(ic_id, stagingId);
  }
  @RequestMapping(value={"/inventoryCotroller/queryContainerByDistinct"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyContainerByDistinct(@RequestParam(value="ic_id", required=true) long ic_id, @RequestParam(value="stagingId", required=true) long stagingId) throws Exception {
    return this.floorGoogleMapsMgrCc.distinctContainerPlates(ic_id, stagingId);
  }
}