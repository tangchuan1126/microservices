package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Transactional
@RestController
public class LocationInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/locationInfoCotroller/queryLocationGroupbyArea"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, ArrayList<DBRow>> queryLocationGroupbyArea(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    DBRow[] locations = this.floorGoogleMapsMgrCc.getLocationByPsId(ps_id);
    Map map = new HashMap();
    if (locations.length > 0) {
      for (int i = 0; i < locations.length; i++) {
        String area_id = locations[i].getString("area_id");
        if (map.containsKey(area_id)) {
          ((ArrayList)map.get(area_id)).add(locations[i]);
        } else {
          ArrayList list = new ArrayList();
          list.add(locations[i]);
          map.put(area_id, list);
        }
      }
    }
    return map;
  }

  @RequestMapping(value={"/locationInfoCotroller/queryLocationByPsId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] queryLocationByPsId(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    return this.floorGoogleMapsMgrCc.getLocationByPsId(ps_id);
  }
  public Map<String, List<Map<String, String>>> queryLocationDatas(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map rt = new HashMap();
    List list = new ArrayList();
    DBRow[] locations = this.floorGoogleMapsMgrCc.getLocationByPsId(ps_id);
    if (locations.length > 0) {
      for (int i = 0; i < locations.length; i++) {
        Map map = new HashMap();
        DBRow r = locations[i];
        map.put("latlng", r.getString("latlng", ""));
        map.put("obj_id", r.getString("obj_id"));
        map.put("obj_name", r.getString("obj_name"));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }

  @RequestMapping(value={"/locationInfoCotroller/queryLocationBoundsByPsId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Map<String, Object>> getLocationBoundsByPsId(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map map = new HashMap();
    DBRow[] rows = this.floorGoogleMapsMgrCc.getLocationByPsId(ps_id);
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        String key = rows[i].get("ps_id") + "_" + rows[i].get("obj_id");
        Map row = DBRowUtils.dbRowAsMap(rows[i]);
        map.put(key, row);
      }
    }
    return map;
  }
}