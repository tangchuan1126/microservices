package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TruckInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/truckInfoCotroller/qureyAllAsset"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyAllAssetWithGroup()
    throws Exception
  {
    return this.floorGoogleMapsMgrCc.getAllAsset();
  }
  @RequestMapping(value={"/truckInfoCotroller/qureyAllGroup"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyAllGroup() throws Exception {
    return this.floorGoogleMapsMgrCc.findAllGroup();
  }

  @RequestMapping(value={"/truckInfoCotroller/qureyAssetByGroup"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyAssetByGroup(@RequestParam(value="groupId", required=true) long groupId) throws Exception {
    return this.floorGoogleMapsMgrCc.getAllAssetByGroupId(groupId);
  }
  @RequestMapping(value={"/truckInfoCotroller/qureyAllAssetGroupByGroupId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyAllAssetGroupByGroupId() throws Exception {
    return this.floorGoogleMapsMgrCc.getAllAssetGroupByGroupId();
  }
}