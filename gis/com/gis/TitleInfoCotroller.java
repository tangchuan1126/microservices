package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import com.javaps.springboot.SessionAttr;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Transactional
@RestController
public class TitleInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/addTitleCotroller/addTitle"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow addTitle(@SessionAttr("adminSesion") Map<String, Object> alb, @RequestParam(value="area_id", required=true) long area_id, @RequestParam(value="ids", required=true) String ids)
    throws Exception
  {
    String isManager = alb.get("administrator").toString();
    if (!isManager.equals("true")) {
      throw new Exception("authError");
    }
    if ((ids == null) || (ids.isEmpty()))
      throw new Exception("AddTitleCotroller.addZoneTitle error: ids is null");
    try
    {
      this.floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(area_id);
      DBRow row = new DBRow();
      String[] title = ids.split(",");
      for (int i = 0; i < title.length; i++) {
        long titleId = Long.parseLong(title[i]);
        DBRow r = new DBRow();
        r.add("area_id", area_id);
        r.add("title_id", titleId);
        long id = this.floorGoogleMapsMgrCc.addZoneTitle(r);
        row.add(i + "", id);
      }
      return row;
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("AddTitleCotroller.addZoneTitle error:" + e);
    }
  }

  @RequestMapping(value={"titleInfoCotroller/queryTitle"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAllTitle() throws Exception
  {
    try
    {
      return this.floorGoogleMapsMgrCc.getAllTitle();
    } catch (Exception e) {
      e.printStackTrace();
      throw new Exception("AddTitleCotroller.addZoneTitle error:" + e);
    }
  }

  @RequestMapping(value={"titleInfoCotroller/deleteTitle"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public boolean deleteTitle(@SessionAttr("adminSesion") Map<String, Object> alb, @RequestParam(value="area_id", required=true) long area_id) throws Exception {
    try { String isManager = alb.get("administrator").toString();
      if (!isManager.equals("true")) {
        throw new Exception("authError");
      }
      this.floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(area_id);
      return true;
    } catch (Exception e) {
      e.printStackTrace();
    }return false;
  }

  @RequestMapping(value={"/titleInfoCotroller/qureyTitle"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getTitleByadId(@SessionAttr("adminSesion") Map<String, Object> alb, @SessionAttr("loginlicenceSesion") String lls)
    throws Exception
  {
    int adgId = ((Integer)alb.get("adgid")).intValue();
    String adId = ((Integer)alb.get("adid")).intValue() + "";
    if (adgId != 10000) {
      return this.floorGoogleMapsMgrCc.getProprietaryByadminId(Long.parseLong(adId));
    }
    return this.floorGoogleMapsMgrCc.getAllTitle();
  }
}