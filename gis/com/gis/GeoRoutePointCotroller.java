package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import com.cwc.db.PageCtrl;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GeoRoutePointCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @RequestMapping(value={"/geoRoutePointCotroller/getGeoAlarmLabel"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Object> getGeoAlarmLabel(@RequestParam(value="type", required=true) String type, @RequestParam(value="pageNo", defaultValue="0", required=false) int pageNo, @RequestParam(value="pageSize", defaultValue="0", required=false) int pageSize)
    throws Exception
  {
    PageCtrl pc = new PageCtrl();
    pc.setPageSize(pageSize == 0 ? 10 : pageSize);
    pc.setPageNo(pageNo == 0 ? 1 : pageNo);
    DBRow[] row = null;
    DBRow condition = new DBRow();
    if ("count".equals(type)) {
      row = this.floorGoogleMapsMgrCc.getGeoAlarmLabelCount();
    } else if ("geoFencing".equals(type)) {
      condition.add("geotype", "3,4,5");
      row = this.floorGoogleMapsMgrCc.getGeoAlarmLabel(condition, pc);
    } else if ("geoLine".equals(type)) {
      condition.add("geotype", "2");
      row = this.floorGoogleMapsMgrCc.getGeoAlarmLabel(condition, pc);
    } else if ("geoPoint".equals(type)) {
      condition.add("geotype", "1");
      row = this.floorGoogleMapsMgrCc.getGeoAlarmLabel(condition, pc);
    }
    Map result = new HashMap();
    result.put("datas", row);
    result.put("pageCtrl", pc);
    return result;
  }
}