package com.gis.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import us.monoid.json.JSONException;
import us.monoid.json.JSONObject;

@Document(collection="his")
public class His
{

  @Id
  private String id;
  private String aid;
  private long gt;
  private long t;
  private double lon;
  private double lat;
  private int spe;
  private int rad;
  private String cal;
  private String nam;
  private String loc;
  private int wen;
  private boolean isl;
  private boolean isa;
  private int alt;
  private String sta;

  public String toString()
  {
    try
    {
      return new JSONObject(this).toString(4);
    } catch (JSONException e) {
      e.printStackTrace();
    }return super.toString();
  }

  public String getId()
  {
    return this.id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getAid() {
    return this.aid;
  }

  public void setAid(String aid) {
    this.aid = aid;
  }

  public long getGt() {
    return this.gt;
  }

  public void setGt(long gt) {
    this.gt = gt;
  }

  public long getT() {
    return this.t;
  }

  public void setT(long t) {
    this.t = t;
  }

  public double getLon() {
    return this.lon;
  }

  public void setLon(double lon) {
    this.lon = lon;
  }

  public double getLat() {
    return this.lat;
  }

  public void setLat(double lat) {
    this.lat = lat;
  }

  public int getSpe() {
    return this.spe;
  }

  public void setSpe(int spe) {
    this.spe = spe;
  }

  public int getRad() {
    return this.rad;
  }

  public void setRad(int rad) {
    this.rad = rad;
  }

  public String getCal() {
    return this.cal;
  }

  public void setCal(String cal) {
    this.cal = cal;
  }

  public String getNam() {
    return this.nam;
  }

  public void setNam(String nam) {
    this.nam = nam;
  }

  public String getLoc() {
    return this.loc;
  }

  public void setLoc(String loc) {
    this.loc = loc;
  }

  public int getWen() {
    return this.wen;
  }

  public void setWen(int wen) {
    this.wen = wen;
  }

  public boolean isIsl() {
    return this.isl;
  }

  public void setIsl(boolean isl) {
    this.isl = isl;
  }

  public boolean isIsa() {
    return this.isa;
  }

  public void setIsa(boolean isa) {
    this.isa = isa;
  }

  public int getAlt() {
    return this.alt;
  }

  public void setAlt(int alt) {
    this.alt = alt;
  }

  public String getSta() {
    return this.sta;
  }

  public void setSta(String sta) {
    this.sta = sta;
  }
}