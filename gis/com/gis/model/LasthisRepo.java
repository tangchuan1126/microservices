package com.gis.model;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public abstract interface LasthisRepo extends MongoRepository<Lasthis, String>
{
  public abstract Page<Lasthis> findAll(Pageable paramPageable);

  @Query("{aid:?0,isl:?1}")
  public abstract List<Lasthis> queryLasthis(String paramString, boolean paramBoolean);
}