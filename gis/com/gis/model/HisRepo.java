package com.gis.model;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public abstract interface HisRepo extends MongoRepository<His, String>
{
  public abstract Page<His> findAll(Pageable paramPageable);

  @Query("{aid:?0, gt:{$gt:?1,$lte:?2}, isl:?3}")
  public abstract Page<His> queryHisPage(String paramString, long paramLong1, long paramLong2, boolean paramBoolean, Pageable paramPageable);

  @Query("{aid:?0, gt:{$gt:?1,$lte:?2}, isl:?3}")
  public abstract List<His> queryHis(String paramString, long paramLong1, long paramLong2, boolean paramBoolean);
}