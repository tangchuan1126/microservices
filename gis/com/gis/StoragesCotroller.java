package com.gis;

import com.cwc.app.floor.api.FloorOrderMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.app.key.ProductStorageTypeKey;
import com.cwc.app.util.ConfigBean;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import com.cwc.util.StringUtil;
import com.gis.exception.CustomException;
import com.javaps.springboot.SessionAttr;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Transactional
@RestController
public class StoragesCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @Autowired
  private FloorProductStoreMgr floorProductStoreMgr;

  @Autowired
  private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;

  @Autowired
  private FloorOrderMgr fom;

  @RequestMapping(value={"/storagesCotroller/storage"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAllStorageKml(@SessionAttr("adminSesion") Map<String, Object> alb, @SessionAttr("loginlicenceSesion") String lls)
    throws Exception
  {
    String isManager = alb.get("administrator").toString();
    DBRow[] rs = new DBRow[0];
    if (isManager.equals("true")) {
      rs = this.floorGoogleMapsMgrCc.getAllStorageKml();
    } else {
      int psId = ((Integer)alb.get("ps_id")).intValue();
      rs = new DBRow[] { this.floorGoogleMapsMgrCc.getStorageKmlByPsId(psId) };
    }
    return rs;
  }
  @RequestMapping(value={"/storagesCotroller/storageAndroid"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, Object> getAllStorageAndroid(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map ret = new HashMap();
    List list = new ArrayList();
    DBRow[] rows = this.floorGoogleMapsMgrCc.getAllStorageKml();
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        Map map = new HashMap();
        DBRow r = rows[i];
        map.put("ps_id", r.getString("ps_id", ""));
        map.put("title", r.getString("title", ""));
        list.add(map);
      }
    }
    Map currentData = getStorageBaseData(ps_id);
    ret.put("allStorage", list);
    ret.put("current", currentData);
    return ret;
  }
  @RequestMapping(value={"/storagesCotroller/queryPersonByPsId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getPersonByPsId(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    return this.floorGoogleMapsMgrCc.getPersonByPsId(ps_id);
  }

  @RequestMapping(value={"/storagesCotroller/querySingleLayerInfo"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] querySingleArea(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="name", required=true) String name, @RequestParam(value="type", required=true) int type) throws Exception {
    DBRow[] datas = new DBRow[0];
    switch (type) {
    case 1:
      datas = this.floorGoogleMapsMgrCc.getLoctionInfoByName(ps_id, name);
      break;
    case 2:
      datas = this.floorGoogleMapsMgrCc.getStagingInfoByName(ps_id, name);
      break;
    case 3:
      datas = this.floorGoogleMapsMgrCc.getDocksInfoByName(ps_id, name);
      break;
    case 4:
      datas = this.floorGoogleMapsMgrCc.getParkingInfoByName(ps_id, name);
      break;
    case 5:
      datas = this.floorGoogleMapsMgrCc.getAreaPositionByName(ps_id, name);
    }
    return datas;
  }
  @RequestMapping(value={"/storagesCotroller/getStorageBaseData"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, String> getStorageBaseData(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    DBRow[] bases = this.floorGoogleMapsMgrCc.getStorageBaseData(ps_id);
    Map map = new HashMap();
    String baseValue = "";
    String _base = "";
    for (int i = 0; i < bases.length; i++) {
      if (bases[i].getString("type").equals("warehouse")) {
        if ("".equals(baseValue)) {
          _base = bases[i].getString("lng") + "," + bases[i].getString("lat") + ",0.0";
        }
        String lat = bases[i].getString("lat");
        String lng = bases[i].getString("lng");
        String latlng = lng + "," + lat;
        baseValue = baseValue + latlng;
        baseValue = baseValue + ",0.0 ";
      }
    }
    baseValue = baseValue + _base;
    map.put("latlng", baseValue);
    return map;
  }
  @RequestMapping(value={"/storageCotroller/getPerson"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] qureyPersonBypsId(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    return this.floorGoogleMapsMgrCc.getPersonByPsId(ps_id);
  }

  @RequestMapping(value={"/storageCotroller/queryAllCountry"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAllCountryCode()
    throws Exception
  {
    return this.fom.getAllCountryCode();
  }

  @RequestMapping(value={"/storageCotroller/qureyStorageType"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<Map<String, String>> getProductStorageType()
    throws Exception
  {
    ProductStorageTypeKey storageTypes = new ProductStorageTypeKey();
    List typeList = storageTypes.getProductStorageType();
    List data = new ArrayList();
    for (int i = 0; i < typeList.size(); i++) {
      Map map = new HashMap();
      String name = storageTypes.getProductStorageTypeById(typeList.get(i) + "");
      map.put("id", i + "");
      map.put("name", name);
      data.add(map);
    }
    return data;
  }
  @RequestMapping(value={"/storageCotroller/saveStorage"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow saveStorage(@RequestBody Map<String, Object> data) throws Exception {
    DBRow ret = new DBRow();
    long ps_id = Long.parseLong(data.get("ps_id").toString());
    String maxX = data.get("maxX").toString();
    String maxY = data.get("maxY").toString();
    List list = (List)data.get("latlngs");
    if (list.size() == 0) {
      ret.add("flag", Boolean.valueOf(false));
      return ret;
    }
    try {
      for (int i = 0; i < list.size(); i++) {
        DBRow row = new DBRow();
        String lat = ((String)((Map)list.get(i)).get("latlng")).split(",")[0];
        String lng = ((String)((Map)list.get(i)).get("latlng")).split(",")[1];
        char A = 'A';
        int asc = A + i;
        char param = (char)asc;
        row.add("param", param + "");
        row.add("ps_id", ps_id);
        row.add("lng", lng);
        row.add("lat", lat);
        row.add("type", "warehouse");
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(row);
      }
      if (!maxX.equals("")) {
        DBRow max = new DBRow();
        max.add("param", maxX);
        max.add("type", "maxX");
        max.add("ps_id", ps_id);
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(max);
      }
      if (!maxY.equals("")) {
        DBRow max = new DBRow();
        max.add("param", maxY);
        max.add("type", "maxY");
        max.add("ps_id", ps_id);
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(max);
      }
      DBRow dt = new DBRow();
      dt.add("ps_id", ps_id);
      dt.add("kml", "");
      dt.add("update_time", System.currentTimeMillis());
      this.floorGoogleMapsMgrCc.addStorageKml(dt);
      this.floorGoogleMapsMgrCc.updateGisVersion();
      ret.add("flag", Boolean.valueOf(true));
      ret.add("ps_id", ps_id);
    } catch (Exception e) {
      e.printStackTrace();
      ret.add("flag", Boolean.valueOf(false));
    }
    return ret;
  }

  @RequestMapping(value={"/storageCotroller/qureyStorageWithoutOnMap"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getStorageWithoutOnMap() throws Exception
  {
    return this.floorGoogleMapsMgrCc.getStorageWithoutOnMap();
  }
  @RequestMapping(value={"/storageCotroller/selectContainer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<Map<String, Object>> selectContainer() throws Exception {
    List list = new ArrayList();
    DBRow[] rows = this.floorGoogleMapsMgrCc.selectContainer(3);
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        Map map = DBRowUtils.dbRowAsMap(rows[i]);
        list.add(map);
      }
    }
    return list;
  }
  @Transactional
  @RequestMapping(value={"/storageCotroller/updateModifyLayer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow updateModifyLayer(@RequestBody Map<String, Object> map, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception {
    DBRow result = new DBRow();
    String isManager = alb.get("administrator").toString();
    if (!isManager.equals("true")) {
      result.add("flag", "authError");
      return result;
    }
    try {
      int type = Integer.parseInt(map.get("location_type").toString());
      String isChanged = map.get("isChanged").toString();
      boolean positionChange = "true".equals(isChanged);
      String x = map.get("x").toString();
      String y = map.get("y").toString();
      String xPosition = map.get("height").toString();
      String yPosition = map.get("width").toString();
      String angle = map.get("angle").toString();
      if ((angle.equals("")) || (angle == "")) {
        angle = "0";
      }
      DBRow row = new DBRow();
      row.add("x", x);
      row.add("y", y);
      row.add("angle", angle);
      row.add("height", xPosition);
      row.add("width", yPosition);
      String psId = map.get("ps_id").toString();
      String cds = "";
      if ((positionChange) && (!x.equals("")) && (!y.equals("")) && (!xPosition.equals("")) && (!yPosition.equals("")) && (type != 6) && (type != 7)) {
        cds = this.floorGoogleMapsMgrCc.convertCoordinateToLatlng(psId, x, y, xPosition, yPosition, angle);
      }
      long position_id = 0L;
      String positionName = "";
      if (type == 5) {
        position_id = Long.parseLong(map.get("zone_id").toString());
        positionName = map.get("zone_name").toString();
      }
      else {
        position_id = Long.parseLong(map.get("position_id").toString());
        positionName = map.get("positionName").toString();
      }
      boolean flag = false;
      switch (type)
      {
      case 1:
        row.add("slc_area", map.get("area_id").toString());
        row.add("slc_position", positionName);
        String is3D = map.get("is_three_dimensional").toString();
        row.add("is_three_dimensional", is3D);

        if (positionChange) {
          row.add("latlng", cds);
        }

        this.floorGoogleMapsMgrCc.updateStorageLocation(position_id, row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(Long.parseLong(map.get("area_id").toString()), 1);

        Map nodeProp = new HashMap();
        nodeProp.put("is_three_dimensional", is3D);
        nodeProp.put("slc_id", Long.valueOf(position_id));
        nodeProp.put("slc_area", Long.valueOf(Long.parseLong(map.get("area_id").toString())));
        nodeProp.put("slc_position", positionName);
        nodeProp.put("ps_id", Long.valueOf(Long.parseLong(psId)));
        nodeProp.put("slc_type", Integer.valueOf(1));
        addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, nodeProp);

        createKmlNew(Long.parseLong(psId));

        deleteInvalidKml(System.getProperty("java.io.tmpdir") + "kml" + File.separator);
        break;
      case 5:
        String area_subtype = map.get("area_subtype") + "";
        if (!area_subtype.isEmpty()) {
          row.add("area_subtype", area_subtype);
        }
        row.add("area_name", positionName);
        if (positionChange) {
          row.add("latlng", cds);
        }
        this.floorGoogleMapsMgrCc.updateStorageArea(position_id, row);
        break;
      case 2:
        long sdId = 0L;
        sdId = Long.parseLong(map.get("sd_id").toString());
        row.add("sd_id", sdId);
        row.add("location_name", positionName);
        if (positionChange) {
          row.add("latlng", cds);
        }

        this.floorGoogleMapsMgrCc.updateStorageStaging(position_id, row);

        Map stg_node = new HashMap();
        stg_node.put("is_three_dimensional", Integer.valueOf(0));
        stg_node.put("slc_id", Long.valueOf(position_id));
        stg_node.put("slc_position", positionName);
        stg_node.put("ps_id", Long.valueOf(Long.parseLong(psId)));
        stg_node.put("slc_type", Integer.valueOf(2));
        addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, stg_node);
        break;
      case 4:
        long areaId = Long.parseLong(map.get("area_id").toString());
        row.add("area_id", areaId);
        row.add("yc_no", positionName);

        if (positionChange) {
          row.add("latlng", cds);
        }
        this.floorGoogleMapsMgrCc.updateStorageParking(position_id, row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(Long.parseLong(map.get("area_id").toString()), 3);
        break;
      case 3:
        long area_id = Long.parseLong(map.get("area_id").toString());
        row.add("area_id", area_id);
        row.add("doorId", positionName);
        if (positionChange) {
          row.add("latlng", cds);
        }
        this.floorGoogleMapsMgrCc.updateStorageDocks(position_id, row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 2);
      }

      result.add("data", row);
      result.add("flag", "true");
      this.floorGoogleMapsMgrCc.updateGisVersion();
      return result;
    } catch (Exception e) {
      result.add("flag", "flase");
      e.printStackTrace();
    }return result;
  }

  public void createKmlNew(long ps_id) throws Exception
  {
    DBRow[] location = this.floorGoogleMapsMgrCc.getExcelLocationData(ps_id);
    if (location.length <= 0) {
      return;
    }

    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    String filename = sdf.format(new Date()) + ".kmz";

    Document doc_2d = DocumentHelper.createDocument();
    Element kml_2d = doc_2d.addElement("kml");
    kml_2d.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
    Element document_2d = kml_2d.addElement("Document");
    document_2d.addElement("name").setText("Data from ");
    Element warehouse_2d = document_2d.addElement("Folder");
    warehouse_2d.addElement("name").setText("Location Data");

    Document doc_3d = DocumentHelper.createDocument();
    Element kml_3d = doc_3d.addElement("kml");
    kml_3d.addAttribute("xmlns", "http://www.opengis.net/kml/2.2");
    Element document_3d = kml_3d.addElement("Document");
    document_3d.addElement("name").setText("Data from ");
    Element warehouse_3d = document_3d.addElement("Folder");
    warehouse_2d.addElement("name").setText("Location Data");
    String areaName = null;
    String pName = null;
    for (int i = 0; i < location.length; i++) {
      if (location[i].getString("is_three_dimensional").equals("0")) {
        areaName = location[i].getString("area_name").trim();
        String locationName = location[i].getString("slc_position").trim();
        Element placemark = warehouse_2d.addElement("Placemark");

        pName = "location_" + locationName;
        placemark.addElement("styleUrl").setText("#style-2d_locs");
        placemark.addElement("description").setText(ps_id + "_" + filename + "_" + areaName);
        placemark.addElement("name").setText(pName);
        Element linearRing = placemark.addElement("Polygon").addElement("outerBoundaryIs").addElement("LinearRing");
        linearRing.addElement("tessellate").setText("0");
        linearRing.addElement("coordinates").setText(location[i].getString("latlng"));
      } else {
        areaName = location[i].getString("area_name").trim();
        String locationName = location[i].getString("slc_position").trim();
        Element placemark = warehouse_3d.addElement("Placemark");

        pName = "location_" + locationName;
        placemark.addElement("styleUrl").setText("#style-3d_locs");
        placemark.addElement("description").setText(ps_id + "_" + filename + "_" + areaName);
        placemark.addElement("name").setText(pName);
        Element linearRing = placemark.addElement("Polygon").addElement("outerBoundaryIs").addElement("LinearRing");
        linearRing.addElement("tessellate").setText("0");
        linearRing.addElement("coordinates").setText(location[i].getString("latlng"));
      }

    }

    Map styleMap = new HashMap();

    styleMap.put("2d_locs", "465079,1,FF82DCEE");
    styleMap.put("3d_locs", "465079,1,FFEBCE87");

    Element style = document_2d.addElement("Style");
    style.addAttribute("id", "style-2d_locs");
    Element lineStyle = style.addElement("LineStyle");
    lineStyle.addElement("color").setText("FF" + ((String)styleMap.get("2d_locs")).split(",")[0]);

    lineStyle.addElement("width").setText(((String)styleMap.get("2d_locs")).split(",")[1] + "");

    Element polyStyle = style.addElement("PolyStyle");
    polyStyle.addElement("color").setText(((String)styleMap.get("2d_locs")).split(",")[2] + "");

    polyStyle.addElement("fill").setText("1");
    polyStyle.addElement("outline").setText("1");

    style = document_3d.addElement("Style");
    style.addAttribute("id", "style-3d_locs");
    lineStyle.addElement("color").setText("FF" + ((String)styleMap.get("3d_locs")).split(",")[0]);

    lineStyle.addElement("width").setText(((String)styleMap.get("3d_locs")).split(",")[1] + "");

    polyStyle = style.addElement("PolyStyle");
    polyStyle.addElement("color").setText(((String)styleMap.get("3d_locs")).split(",")[2] + "");

    polyStyle.addElement("fill").setText("1");
    polyStyle.addElement("outline").setText("1");

    OutputFormat format = OutputFormat.createCompactFormat();

    String kmlPath = System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator;
    File f = new File(kmlPath);
    if (!f.exists()) {
      f.mkdir();
    }
    System.out.println(kmlPath);
    String _2dPath = System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator + "2d_locs_" + filename;
    String _3dPath = System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator + "3d_locs_" + filename;
    File _2file = new File(_2dPath);

    ZipOutputStream out_2d = new ZipOutputStream(new FileOutputStream(_2file));
    out_2d.putNextEntry(new ZipEntry("/"));
    out_2d.putNextEntry(new ZipEntry(filename.substring(0, filename.lastIndexOf(".")) + ".kml"));
    out_2d.write(doc_2d.asXML().getBytes());
    out_2d.closeEntry();
    out_2d.flush();
    out_2d.close();
    File _3dfile = new File(_3dPath);
    ZipOutputStream out_3d = new ZipOutputStream(new FileOutputStream(_3dfile));
    out_3d.putNextEntry(new ZipEntry("/"));
    out_3d.putNextEntry(new ZipEntry(filename.substring(0, filename.lastIndexOf(".")) + ".kml"));
    out_3d.write(doc_3d.asXML().getBytes());
    out_3d.closeEntry();
    out_3d.flush();
    out_3d.close();
    DBRow row = new DBRow();
    row.add("ps_id", ps_id);
    row.add("kml", filename);
    row.add("update_time", System.currentTimeMillis());
    this.floorGoogleMapsMgrCc.addOrUpdateStorageKml(row);
  }

  public boolean deleteInvalidKml(String path) throws Exception {
    boolean result = false;
    DBRow[] rows = this.floorGoogleMapsMgrCc.queryStorageKml();
    File file = new File(path);
    if (file.isDirectory())
    {
      String[] kmlNames = file.list();
      for (int i = 0; i < kmlNames.length; i++) {
        String kmlName = kmlNames[i];
        boolean delete = true;
        for (int j = 0; j < rows.length; j++) {
          String kml = rows[j].getString("kml");
          String _2d_loc_kml = "2d_locs_" + kml;
          String _3d_loc_kml = "3d_locs_" + kml;
          if ((_2d_loc_kml.equals(kmlName)) || (_3d_loc_kml.equals(kmlName))) {
            delete = false;
          }
        }
        if (delete) {
          String deletePath = path + "/" + kmlName;
          File deletefile = new File(deletePath);
          deletefile.delete();
          result = true;
        }
      }
    }
    return result;
  }

  @RequestMapping(value={"/storageCotroller/createKml"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Map<String, String> createKml(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map map = new HashMap();
    try {
      createKmlNew(ps_id);
      String path = System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator;
      deleteInvalidKml(path);
      map.put("flag", "true");

      return map;
    }
    catch (Exception e)
    {
      e = 
        e;

      e.printStackTrace();

      return map; } finally {  } return map;
  }

  @RequestMapping(value={"/public/getKmlUrl/{filename}"}, produces={"application/vnd.google-earth.kml+xml"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public void xmlFileAccess(HttpServletResponse res, @PathVariable("filename") String filename) throws Exception {
    res.setContentType("application/vnd.google-earth.kmz .kmz");
    InputStream is = new FileInputStream(System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator + filename + ".kmz");
    IOUtils.copy(is, res.getOutputStream());
    is.close();
  }
  @RequestMapping(value={"/storageCotroller/convertLatlngToCoordinate"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow convertLatlngToCoordinate(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="lat", required=true) double lat, @RequestParam(value="lng", required=true) double lng) throws Exception {
    DBRow row = new DBRow();
    try {
      String pointStr = this.floorGoogleMapsMgrCc.convertLatlngToCoordinate(ps_id, lat, lng);
      if (!pointStr.equals("")) {
        String x = pointStr.split(",")[0];
        String y = pointStr.split(",")[1];
        DecimalFormat df = new DecimalFormat("0.00");
        row.add("flag", "true");
        row.add("x", df.format(Double.parseDouble(x)));
        row.add("y", df.format(Double.parseDouble(y)));
      } else {
        row.add("flag", "false");
      }

      return row;
    }
    catch (Exception e)
    {
      e = 
        e;

      row.add("flag", "false");
      e.printStackTrace();

      return row; } finally {  } return row;
  }

  @RequestMapping(value={"/storageCotroller/rectanglePaths"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow convertCoordinateToLatlng(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="x", required=true) double x, @RequestParam(value="y", required=true) double y, @RequestParam(value="xPosition", required=true) double xPosition, @RequestParam(value="yPosition", required=true) double yPosition, @RequestParam(value="angle", required=true) double angle) throws Exception {
    DBRow rt = new DBRow();
    DBRow[] baseData = this.floorGoogleMapsMgrCc.getExcelBaseData(ps_id);
    double maxX = 0.0D;
    double maxY = 0.0D;

    int pCount = 0;
    char[] whChar = new char[26];
    DBRow whRow = new DBRow();
    for (int i = 0; i < baseData.length; i++) {
      DBRow r = baseData[i];
      String type = r.getString("type").toLowerCase();
      String val = r.getString("param").toLowerCase();
      if ("warehouse".equals(type)) {
        char p = val.toCharArray()[0];
        whChar[pCount] = p;
        whRow.add(p + "", r.getString("lng") + "," + r.getString("lat"));
        pCount++;
      } else if ("maxx".equals(type)) {
        maxX = Double.parseDouble(val);
      } else if ("maxy".equals(type)) {
        maxY = Double.parseDouble(val);
      }
    }

    if ((pCount < 3) || (maxX <= 0.0D) || (maxY <= 0.0D)) {
      rt.add("flag", "false");
      rt.add("paths", "");
      return rt;
    }

    whChar = Arrays.copyOfRange(whChar, 0, pCount);
    Arrays.sort(whChar);
    String pointA = whRow.getString(whChar[0] + "");
    String pointB = whRow.getString(whChar[1] + "");
    String pointD = whRow.getString(whChar[(whChar.length - 1)] + "");

    double aLng = Double.parseDouble(pointA.split(",")[0]);
    double aLat = Double.parseDouble(pointA.split(",")[1]);
    double bLng = Double.parseDouble(pointB.split(",")[0]);
    double bLat = Double.parseDouble(pointB.split(",")[1]);
    double dLng = Double.parseDouble(pointD.split(",")[0]);
    double dLat = Double.parseDouble(pointD.split(",")[1]);

    double ratio = Math.hypot(aLat - bLat, aLng - bLng) / maxX;
    double radX = Math.atan((aLat - bLat) / (aLng - bLng));
    double radY = Math.atan((aLat - dLat) / (aLng - dLng));
    if (radY <= 0.0D) {
      radY += 3.141592653589793D;
    }
    radY -= 1.570796326794897D;
    double radN = radX - radY;
    double maxY_map = Math.hypot(aLat - dLat, aLng - dLng) / ratio;

    double X = x;
    double Y = y;
    double L = xPosition;
    double W = yPosition;

    double[] XS = { X, X + L, X + L, X };
    double[] YS = { Y, Y, Y + W, Y + W };

    if (angle != 0.0D) {
      for (int j = 1; j < 4; j++) {
        XS[j] -= XS[0];
        YS[j] -= YS[0];

        double r = Math.toRadians(angle) + Math.atan(YS[j] / XS[j]);
        double l = Math.hypot(XS[j], YS[j]);
        XS[j] = (l * Math.cos(r) + XS[0]);
        YS[j] = (l * Math.sin(r) + YS[0]);
        if (Double.isNaN(XS[j])) {
          XS[j] = XS[0];
        }
        if (Double.isNaN(YS[j])) {
          YS[j] = YS[0];
        }
      }
    }
    String cds = "";

    for (int j = 0; j < 4; j++) {
      YS[j] /= maxY / maxY_map;

      XS[j] += YS[j] * Math.sin(radN) * Math.cos(radN);
      YS[j] *= Math.cos(radN);

      int sign = XS[j] > 0.0D ? 1 : -1;
      double rad = radX + Math.atan(YS[j] / XS[j]);
      double len = Math.hypot(XS[j], YS[j]);
      double lat = sign * len * ratio * Math.sin(rad) + aLat;
      double lng = sign * len * ratio * Math.cos(rad) + aLng;
      if (Double.isNaN(lat)) {
        lat = aLat;
      }
      if (Double.isNaN(lng)) {
        lng = aLng;
      }
      cds = cds + lng + "," + lat + ",0.0 ";
    }
    cds = cds + cds.split(" ")[0];
    rt.add("flag", "true");
    rt.add("paths", cds);
    return rt;
  }
  @RequestMapping(value={"/storageCotroller/convertCoordinateToLatlng"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow convertCoordinateToLatlng(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="x", required=true) double x, @RequestParam(value="y", required=true) double y) throws Exception {
    DBRow rt = new DBRow();
    DBRow[] baseData = this.floorGoogleMapsMgrCc.getExcelBaseData(ps_id);
    double maxX = 0.0D;
    double maxY = 0.0D;
    int pCount = 0;
    char[] whChar = new char[26];
    DBRow whRow = new DBRow();
    for (int i = 0; i < baseData.length; i++) {
      DBRow r = baseData[i];
      String type = r.getString("type").toLowerCase();
      String val = r.getString("param").toLowerCase();
      if ("warehouse".equals(type)) {
        char p = val.toCharArray()[0];
        whChar[pCount] = p;
        whRow.add(p + "", r.getString("lng") + "," + r.getString("lat"));
        pCount++;
      } else if ("maxx".equals(type)) {
        maxX = Double.parseDouble(val);
      } else if ("maxy".equals(type)) {
        maxY = Double.parseDouble(val);
      }
    }

    if ((pCount < 3) || (maxX <= 0.0D) || (maxY <= 0.0D)) {
      rt.add("flag", "false");
      rt.add("data", "");
      return rt;
    }

    whChar = Arrays.copyOfRange(whChar, 0, pCount);
    Arrays.sort(whChar);
    String pointA = whRow.getString(whChar[0] + "");
    String pointB = whRow.getString(whChar[1] + "");
    String pointD = whRow.getString(whChar[(whChar.length - 1)] + "");

    double aLng = Double.parseDouble(pointA.split(",")[0]);
    double aLat = Double.parseDouble(pointA.split(",")[1]);
    double bLng = Double.parseDouble(pointB.split(",")[0]);
    double bLat = Double.parseDouble(pointB.split(",")[1]);
    double dLng = Double.parseDouble(pointD.split(",")[0]);
    double dLat = Double.parseDouble(pointD.split(",")[1]);
    double ratio = Math.hypot(aLat - bLat, aLng - bLng) / maxX;
    double radX = Math.atan((aLat - bLat) / (aLng - bLng));
    double radY = Math.atan((aLat - dLat) / (aLng - dLng));
    if (radY <= 0.0D) {
      radY += 3.141592653589793D;
    }
    radY -= 1.570796326794897D;
    double radN = radX - radY;
    double maxY_map = Math.hypot(aLat - dLat, aLng - dLng) / ratio;

    double X = x;
    double Y = y;

    Y /= maxY / maxY_map;

    X += Y * Math.sin(radN) * Math.cos(radN);
    Y *= Math.cos(radN);

    int sign = X > 0.0D ? 1 : -1;
    double rad = radX + Math.atan(Y / X);
    double len = Math.hypot(X, Y);
    double lat = sign * len * ratio * Math.sin(rad) + aLat;
    double lng = sign * len * ratio * Math.cos(rad) + aLng;
    if (Double.isNaN(lat)) {
      lat = aLat;
    }
    if (Double.isNaN(lng)) {
      lng = aLng;
    }
    rt.add("flag", "true");
    rt.add("latlng", lng + "," + lat);
    return rt;
  }
  @Transactional
  @RequestMapping(value={"/storageCotroller/saveModifyLayer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow saveModifyLayer(@RequestBody Map<String, Object> map, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception { DBRow result = new DBRow();
    try {
      String isManager = alb.get("administrator").toString();
      if (!isManager.equals("true")) {
        result.add("flag", "authError");
        throw new Exception("authError");
      }
      int type = Integer.parseInt(map.get("type").toString());
      long psId = Long.parseLong(map.get("ps_id").toString());
      String x = map.get("x").toString();
      String y = map.get("y").toString();
      String xPosition = map.get("height") + "";
      String yPosition = map.get("width").toString();
      String angle = map.get("angle").toString();
      String positionName = map.containsKey("name") ? map.get("name").toString() : null;
      String storage_name = map.get("storage_name").toString();
      String cds = map.containsKey("latlng") ? map.get("latlng").toString() : null;
      long position_id = 0L;
      DBRow[] dbrow = this.floorGoogleMapsMgrCc.getLayerInfoBypsIdAndName(psId, positionName, type);
      if (dbrow.length > 0) {
        result.add("flag", "false");
        result.add("returnValue", 0);
        throw new Exception("0");
      }
      if (StringUtil.isBlank(angle)) {
        angle = "0";
      }
      DBRow row = new DBRow();
      row.add("x", x);
      row.add("y", y);
      row.add("angle", angle);
      row.add("height", xPosition);
      row.add("width", yPosition);
      long area_id = 0L;
      if (StringUtil.isBlank(cds)) {
        cds = this.floorGoogleMapsMgrCc.convertCoordinateToLatlng(psId + "", x, y, xPosition, yPosition, angle);
      }
      boolean flag = false;
      switch (type) {
      case 1:
        area_id = Long.parseLong(map.get("area_id").toString());
        String slcPositionAll = storage_name + map.get("zone").toString() + positionName;
        String is3D = map.get("is_three_dimensional") + "";
        row.add("slc_position_all", slcPositionAll);
        row.add("is_three_dimensional", is3D);
        row.add("slc_area", area_id);
        row.add("slc_position", positionName);
        row.add("latlng", cds);
        row.add("slc_psid", psId);
        row.add("slc_ps_title", storage_name);
        position_id = this.floorGoogleMapsMgrCc.addLocationCatalog(row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 1);
        createKmlNew(psId);
        deleteInvalidKml(System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator);

        Map props = new HashMap();
        props.put("slc_id", Long.valueOf(position_id));
        props.put("ps_id", Long.valueOf(psId));
        props.put("slc_area", Long.valueOf(area_id));
        props.put("is_three_dimensional", is3D);
        props.put("slc_position", positionName);
        props.put("slc_type", Integer.valueOf(1));
        addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, props);
        break;
      case 5:
        String titles = map.containsKey("title_id") ? map.get("title_id").toString() : "";
        String doors = map.containsKey("sd_id") ? map.get("sd_id").toString() : "";
        String aotulocation = map.get("aotulocations") + "";
        String childtype = map.containsKey("child_type") ? map.get("child_type").toString() : "0";

        int child_type = Integer.parseInt(childtype);

        if ((map.containsKey("area_id")) && (!"".equals(map.get("area_id").toString()))) {
          position_id = Long.parseLong(map.get("area_id").toString());
        }
        if ((position_id == 0L) && (child_type != 2)) {
          row.add("area_name", positionName);
          row.add("latlng", cds);
          row.add("area_psid", psId);
          row.add("area_type", 0);
          row.add("area_img", "");
          position_id = this.floorGoogleMapsMgrCc.addLocationArea(row);
        }

        String[] aotulocations = null;
        if ((aotulocation != null) && (!aotulocation.equals(""))) {
          aotulocations = aotulocation.split(";");
        }
        if ((aotulocation != null) && (!aotulocation.equals("")) && (aotulocations.length > 0) && (child_type > 0))
        {
          if (child_type == 1) {
            String is3DLoc = map.get("is_three_dimensional") + "";
            for (int i = 0; i < aotulocations.length; i++) {
              String[] aotuloca = aotulocations[i].split("#");
              if (aotuloca.length > 0) {
                DBRow aoturow = new DBRow();
                String auto_cds = aotuloca[7];
                String aotuname_all = storage_name + positionName + aotuloca[1];
                DBRow[] autoLocal = this.floorGoogleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 1);
                if (autoLocal.length > 0) {
                  result.add("flag", "false");
                  result.add("returnValue", -1);
                  throw new Exception("-1");
                }
                aoturow.add("x", aotuloca[4]);
                aoturow.add("y", aotuloca[5]);
                aoturow.add("angle", aotuloca[6]);
                aoturow.add("height", roundOff(aotuloca[2], 2));
                aoturow.add("width", roundOff(aotuloca[3], 2));
                aoturow.add("slc_position_all", aotuname_all.toUpperCase());
                aoturow.add("is_three_dimensional", is3DLoc);
                aoturow.add("slc_area", position_id);
                aoturow.add("slc_position", aotuloca[1].toUpperCase());
                aoturow.add("latlng", auto_cds);
                aoturow.add("slc_psid", aotuloca[0]);
                aoturow.add("slc_ps_title", storage_name);
                long slc_id = this.floorGoogleMapsMgrCc.addLocationCatalog(aoturow);

                Map nodeProp = new HashMap();
                nodeProp.put("is_three_dimensional", is3DLoc);
                nodeProp.put("slc_id", Long.valueOf(slc_id));
                nodeProp.put("slc_area", Long.valueOf(position_id));
                nodeProp.put("slc_position", aotuloca[1].toUpperCase());
                nodeProp.put("ps_id", Long.valueOf(Long.parseLong(aotuloca[0])));
                nodeProp.put("slc_type", Integer.valueOf(1));
                addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, nodeProp);
              }

            }

            this.floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 1);

            createKmlNew(psId);
            deleteInvalidKml(System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator);
          }
          if (child_type == 2) {
            for (int i = 0; i < aotulocations.length; i++) {
              String[] aotuloca = aotulocations[i].split("#");
              if (aotuloca.length > 0) {
                DBRow aoturow = new DBRow();
                String auto_cds = aotuloca[7];
                DBRow[] autoLocal = this.floorGoogleMapsMgrCc.getLayerInfoBypsIdAndName(psId, aotuloca[1], 2);
                if (autoLocal.length > 0) {
                  result.add("returnValue", -1);
                  throw new Exception("-1");
                }
                aoturow.add("x", aotuloca[4]);
                aoturow.add("y", aotuloca[5]);
                aoturow.add("angle", aotuloca[6]);
                aoturow.add("height", roundOff(aotuloca[2], 2));
                aoturow.add("width", roundOff(aotuloca[3], 2));
                aoturow.add("location_name", aotuloca[1].toUpperCase());
                aoturow.add("latlng", auto_cds);
                aoturow.add("psc_id", aotuloca[0]);
                long stag_id = this.floorGoogleMapsMgrCc.addLoadUnloadLocation(aoturow);

                Map stg_node = new HashMap();
                stg_node.put("is_three_dimensional", Integer.valueOf(0));
                stg_node.put("slc_id", Long.valueOf(stag_id));
                stg_node.put("slc_position", aotuloca[1].toUpperCase());
                stg_node.put("ps_id", Long.valueOf(Long.parseLong(aotuloca[0])));
                stg_node.put("slc_type", Integer.valueOf(2));
                addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, stg_node);
              }
            }
          }

          if (child_type == 3) {
            String[] sqls = new String[aotulocations.length];
            String[] fieldValues = new String[aotulocations.length];
            for (int i = 0; i < aotulocations.length; i++) {
              String[] aotuloca = aotulocations[i].split("#");
              if (aotuloca.length > 0) {
                fieldValues[i] = aotuloca[1];
                String sql = "INSERT INTO storage_door (x, y,angle,height,width,area_id,doorId,latlng,ps_id)  VALUES ('" + aotuloca[4] + "','" + aotuloca[5] + "','" + aotuloca[6] + "','" + roundOff(aotuloca[2], 2) + "','" + roundOff(aotuloca[3], 2) + "'," + position_id + ",'" + aotuloca[1].toUpperCase() + "','" + aotuloca[7] + "'," + aotuloca[0] + ")";

                sqls[i] = sql;
              }
            }
            DBRow[] counts = this.floorGoogleMapsMgrCc.selectInCounts(psId, fieldValues, "doorId", "storage_door");
            int count = counts[0].get("count", 0);
            if (count > 0) {
              result.add("flag", "false");
              result.add("returnValue", -1);
              throw new Exception("-1");
            }
            this.floorGoogleMapsMgrCc.batchSql(sqls);
            this.floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 2);
          }

          if (child_type == 4) {
            String[] sqls = new String[aotulocations.length];
            String[] fieldValues = new String[aotulocations.length];
            for (int i = 0; i < aotulocations.length; i++) {
              String[] aotuloca = aotulocations[i].split("#");
              if (aotuloca.length > 0) {
                fieldValues[i] = aotuloca[1];
                String sql = "INSERT INTO storage_yard_control (x, y,angle,height,width,area_id,yc_no,latlng,ps_id,yc_status)  VALUES ('" + aotuloca[4] + "','" + aotuloca[5] + "','" + aotuloca[6] + "','" + roundOff(aotuloca[2], 2) + "','" + roundOff(aotuloca[3], 2) + "'," + position_id + ",'" + aotuloca[1].toUpperCase() + "','" + aotuloca[7] + "'," + aotuloca[0] + ",0" + ")";

                sqls[i] = sql;
              }
            }
            DBRow[] counts = this.floorGoogleMapsMgrCc.selectInCounts(psId, fieldValues, "yc_no", "storage_yard_control");
            int count = counts[0].get("count", 0);
            if (count > 0) {
              result.add("flag", "false");
              result.add("returnValue", -1);
              throw new Exception("-1");
            }
            this.floorGoogleMapsMgrCc.batchSql(sqls);
            this.floorGoogleMapsMgrCc.updateStorageAreaType(position_id, 3);
          }

        }

        if (!titles.equals("")) {
          this.floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(position_id);
          String[] title = titles.split(",");
          for (int i = 0; i < title.length; i++) {
            long titleId = Long.parseLong(title[i]);
            DBRow r = new DBRow();
            r.add("area_id", position_id);
            r.add("title_id", titleId);
            this.floorGoogleMapsMgrCc.addZoneTitle(r);
          }
        }
        if (!doors.equals("")) {
          this.floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(position_id);
          String[] door = doors.split(",");
          for (int i = 0; i < door.length; i++) {
            long sdId = Long.parseLong(door[i]);
            this.floorGoogleMapsMgrCc.addStorageAreaDoor(position_id, sdId);
          }
        }
        break;
      case 2:
        long sdId = Long.parseLong(map.get("sd_id").toString());
        row.add("sd_id", sdId);
        row.add("location_name", positionName);
        row.add("latlng", cds);
        row.add("psc_id", psId);
        position_id = this.floorGoogleMapsMgrCc.addLoadUnloadLocation(row);

        Map staging_props = new HashMap();
        staging_props.put("slc_position", positionName);
        staging_props.put("slc_id", Long.valueOf(position_id));
        staging_props.put("ps_id", Long.valueOf(psId));
        staging_props.put("is_three_dimensional", "0");
        staging_props.put("slc_type", Integer.valueOf(2));
        addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, staging_props);
        break;
      case 4:
        long areaId = Long.parseLong(map.get("area_id").toString());
        row.add("area_id", areaId);
        row.add("yc_no", positionName);
        row.add("latlng", cds);
        row.add("ps_id", psId);
        row.add("yc_status", 1);
        position_id = this.floorGoogleMapsMgrCc.addStorageYardControl(row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(areaId, 3);
        break;
      case 3:
        area_id = Long.parseLong(map.get("area_id").toString());
        row.add("area_id", area_id);
        row.add("doorId", positionName);
        row.add("latlng", cds);
        row.add("ps_id", psId);
        position_id = this.floorGoogleMapsMgrCc.addStorageDoor(row);
        this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 2);
      }

      result.add("flag", "true");
      result.add("returnValue", position_id);
      this.floorGoogleMapsMgrCc.updateGisVersion();
    }
    catch (Exception e) {
      result.add("flag", "false");
      throw new Exception(e);
    }

    return result; }

  public double roundOff(String num, int len) {
    double x = Math.pow(10.0D, len);
    double d = Double.parseDouble(num);
    return (int)(d * x + 0.5D) / x;
  }

  @RequestMapping(value={"/storageCotroller/queryStorageBase"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public List<Map<String, String>> queryStorageBase(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    DBRow[] rows = this.floorGoogleMapsMgrCc.getStorageBaseData(ps_id);
    List list = new ArrayList();
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        DBRow row = rows[i];
        Map map = new HashMap();
        String type = row.getString("type");
        if (type.toLowerCase().equals("maxx")) {
          map.put("maxX", row.getString("param"));
        }
        if (type.toLowerCase().equals("maxy")) {
          map.put("maxY", row.getString("param"));
        }
        if (type.toLowerCase().equals("warehouse")) {
          String latlng = row.getString("lat") + "," + row.getString("lng");
          map.put(row.getString("param"), latlng);
        }
        list.add(map);
      }
    }
    return list;
  }
  @RequestMapping(value={"/storageCotroller/queryOwnStorage"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryOwnStorage() throws Exception {
    DBRow row = new DBRow();
    try {
      DBRow[] datas = this.floorGoogleMapsMgrCc.getStorageOnMap();
      row.add("flag", "true");
      row.add("datas", datas);
    } catch (Exception e) {
      row.add("flag", "false");
      e.printStackTrace();
    }
    return row;
  }
  @Transactional
  @RequestMapping(value={"/storageCotroller/deleteStorageBase"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public boolean deleteStorageBase(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    try { int d1 = this.floorGoogleMapsMgrCc.deleteStorageWerehouseBase(ps_id);
      int d2 = this.floorGoogleMapsMgrCc.deleteStorageKml(ps_id);
      if ((d1 != 0) && (d2 != 0)) {
        this.floorGoogleMapsMgrCc.updateGisVersion();
        return true;
      }
      throw new Exception("删除werehousebase或kml有误。");
    } catch (Exception e)
    {
      e.printStackTrace();
    }return false;
  }
  @Transactional
  @RequestMapping(value={"/storageCotroller/updateStorageBase"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow updateStorageBase(@RequestBody Map<String, Object> map, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception {
    DBRow ret = new DBRow();
    try {
      String isManager = alb.get("administrator").toString();
      if (!isManager.equals("true")) {
        ret.add("flag", "authError");
        throw new Exception("authError");
      }
      long ps_id = Long.parseLong(map.get("ps_id").toString());
      deleteStorageBase(ps_id);
      String maxX = map.get("maxX").toString();
      String maxY = map.get("maxY").toString();
      List list = (List)map.get("latlngs");
      if (list.size() == 0) {
        ret.add("flag", Boolean.valueOf(false));
        throw new Exception("no latlngs data!");
      }
      for (int i = 0; i < list.size(); i++) {
        DBRow row = new DBRow();
        String lat = ((String)((Map)list.get(i)).get("latlng")).split(",")[0];
        String lng = ((String)((Map)list.get(i)).get("latlng")).split(",")[1];
        char A = 'A';
        int asc = A + i;
        char param = (char)asc;
        row.add("param", param + "");
        row.add("ps_id", ps_id);
        row.add("lng", lng);
        row.add("lat", lat);
        row.add("type", "warehouse");
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(row);
      }
      if (!maxX.equals("")) {
        DBRow max = new DBRow();
        max.add("param", maxX);
        max.add("type", "maxX");
        max.add("ps_id", ps_id);
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(max);
      }
      if (!maxY.equals("")) {
        DBRow max = new DBRow();
        max.add("param", maxY);
        max.add("type", "maxY");
        max.add("ps_id", ps_id);
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(max);
      }
      DBRow dt = new DBRow();
      dt.add("ps_id", ps_id);
      dt.add("kml", "");
      dt.add("update_time", System.currentTimeMillis());
      this.floorGoogleMapsMgrCc.addStorageKml(dt);
      ret.add("flag", Boolean.valueOf(true));
      ret.add("ps_id", ps_id);
      this.floorGoogleMapsMgrCc.updateGisVersion();

      return ret;
    }
    catch (Exception e)
    {
      e = 
        e;

      ret.add("flag", Boolean.valueOf(false));
      e.printStackTrace();

      return ret; } finally {  } return ret;
  }

  @RequestMapping(value={"/storageCotroller/queryWebcam"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryWebcam(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception
  {
    DBRow row = new DBRow();
    try {
      DBRow[] rows = this.floorGoogleMapsMgrCc.getWebcambyPsid(ps_id);
      row.add("flag", "true");
      row.add("datas", rows);
      return row;
    } catch (Exception e) {
      row.add("flag", "false");
      e.printStackTrace();
    }throw new CustomException(row);
  }

  public Map<String, List<Map<String, String>>> getWebcamDatas(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    DBRow[] rows = this.floorGoogleMapsMgrCc.getWebcambyPsid(ps_id);
    Map rt = new HashMap();
    List list = new ArrayList();
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        Map map = new HashMap();
        DBRow r = rows[i];
        map.put("ip", r.getString("ip", ""));
        map.put("port", r.getString("port", ""));
        map.put("username", r.getString("user", ""));
        map.put("password", r.getString("password", ""));
        map.put("latlng", r.getString("latlng", ""));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }
  @RequestMapping(value={"/storageCotroller/queryPrinter"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryPrinter(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    DBRow row = new DBRow();
    try {
      DBRow[] rows = this.floorGoogleMapsMgrCc.getPrinterbyPsid(ps_id);
      row.add("flag", "true");
      row.add("datas", rows);
      return row;
    } catch (Exception e) {
      row.add("flag", "false");
      e.printStackTrace();
    }throw new CustomException(row);
  }

  @RequestMapping(value={"/storageCotroller/saveWebcam"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow modifyWebcam(@RequestBody Map<String, Object> map, @SessionAttr("adminSesion") Map<String, Object> alb) throws Exception
  {
    DBRow result = new DBRow();
    try {
      String isManager = alb.get("administrator").toString();
      if (!isManager.equals("true")) {
        result.add("flag", "authError");
        throw new Exception("authError");
      }
      String pageType = map.get("pageType") + "";
      long psId = Long.parseLong(map.get("ps_id").toString());
      String id = map.get("id").toString();
      String ip = map.get("ip").toString();
      String port = map.get("port").toString();
      String username = map.get("username").toString();
      String password = map.get("password").toString();
      double x = Double.parseDouble(map.get("x").toString());
      double y = Double.parseDouble(map.get("y").toString());
      String inner_radius = map.get("inner_radius").toString();
      String outer_radius = map.get("outer_radius").toString();
      String s_degree = map.get("s_degree").toString();
      String e_degree = map.get("e_degree").toString();

      DBRow data = new DBRow();
      String latlng = convertCoordinateToLatlng(psId, x, y).getString("latlng", "");
      if (!latlng.isEmpty()) {
        data.add("latlng", latlng);
      } else {
        result.add("flag", "false");
        throw new NullPointerException();
      }
      data.add("ps_id", psId);
      data.add("ip", ip);
      data.add("id", id);
      data.add("port", port);
      data.add("user", username);
      data.add("password", password);
      data.add("inner_radius", inner_radius);
      data.add("outer_radius", outer_radius);
      data.add("x", x);
      data.add("y", y);
      data.add("s_degree", s_degree);
      data.add("e_degree", e_degree);
      if (pageType.equals("0")) {
        long id_ = this.floorGoogleMapsMgrCc.addWebcam(data);
        result.add("id", id_);
      } else {
        this.floorGoogleMapsMgrCc.updateWebcamInfo(data);
      }
      this.floorGoogleMapsMgrCc.updateGisVersion();
      result.add("latlng", latlng);
      result.add("flag", "true");
      return result;
    } catch (Exception e) {
      e.printStackTrace();
      result.add("flag", "false");
    }throw new CustomException(result);
  }

  @RequestMapping(value={"/storageCotroller/queryPersonCounts"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow getPersonCountsGroupByArea(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception
  {
    DBRow row = new DBRow();
    try {
      DBRow[] rows = this.floorGoogleMapsMgrCc.getPersonforAreaByPsId(ps_id);
      row.add("flag", "true");
      row.add("datas", rows);
      return row;
    } catch (Exception e) {
      row.add("flag", "false");
    }throw new CustomException(row);
  }

  @RequestMapping(value={"/storageCotroller/queryMapsBoundaries"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow getBoundariesFromDB(@RequestParam(value="regionIds", required=true) String regionIds, @RequestParam(value="type", required=true) String type)
    throws Exception
  {
    DBRow res = new DBRow();
    DBRow[] boundary = null;
    if ("country".equals(type))
      boundary = this.floorGoogleMapsMgrCc.getGoogleMapsBoundaryByCountry(regionIds);
    else if ("province".equals(type)) {
      boundary = this.floorGoogleMapsMgrCc.getGoogleMapsBoundaryByProvince(regionIds);
    }
    res.add("boundary", boundary);
    return res;
  }
  @Transactional
  public void deleteAllStorageLayer(long ps_id) throws Exception {
    String dock = ConfigBean.getStringValue("storage_door");
    String dock_field = "ps_id";
    String zone = ConfigBean.getStringValue("storage_location_area");
    String zone_field = "area_psid";
    String yard = ConfigBean.getStringValue("storage_yard_control");
    String yard_field = "ps_id";
    String location = ConfigBean.getStringValue("storage_location_catalog");
    String location_field = "slc_psid";
    String staging = ConfigBean.getStringValue("storage_load_unload_location");
    String staging_field = "psc_id";
    String webcam = ConfigBean.getStringValue("webcam");
    String webcam_field = "ps_id";
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(dock_field, ps_id, dock);
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(zone_field, ps_id, zone);
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(yard_field, ps_id, yard);
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(webcam_field, ps_id, webcam);
    Set groupBy = new HashSet();
    groupBy.add("slc_id");
    groupBy.add("slc_type");
    DBRow[] loc = this.floorProductStoreMgr.productCount(ps_id, 0L, 1L, 0L, 0, null, 0L, null, null, 0L, groupBy);
    if (loc.length > 0) {
      throw new Exception("location上有库存，删除位置失败");
    }
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(location_field, ps_id, location);
    Map searchFor = new HashMap();
    searchFor.put("slc_type", Integer.valueOf(1));
    this.floorProductStoreMgr.removeNodes(ps_id, FloorProductStoreMgr.NodeType.StorageLocationCatalog, searchFor, false);

    DBRow[] stag = this.floorProductStoreMgr.productCount(ps_id, 0L, 2L, 0L, 0, null, 0L, null, null, 0L, groupBy);
    if (stag.length > 0) {
      throw new Exception("Staging上有库存，删除位置失败");
    }
    Map searchFor = new HashMap();
    searchFor.put("slc_type", Integer.valueOf(2));
    this.floorProductStoreMgr.removeNodes(ps_id, FloorProductStoreMgr.NodeType.StorageLocationCatalog, searchFor, false);
    this.floorGoogleMapsMgrCc.deleteLayerByPsId(staging_field, ps_id, staging);
  }

  @Transactional
  @RequestMapping(value={"/storageCotroller/saveExcelLayerData"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow saveExcelLayerData(@RequestParam(value="area_psid", required=true) long ps_id, @RequestParam(value="title", required=true) String title, @RequestParam(value="flag", required=true) String flag, @SessionAttr("adminSesion") Map<String, Object> alb)
    throws Exception
  {
    DBRow result = new DBRow();
    String isManager = alb.get("administrator").toString();
    if (!isManager.equals("true")) {
      throw new Exception("authError");
    }
    if (flag.equals("true")) {
      deleteAllStorageLayer(ps_id);
    }
    DBRow condition = new DBRow();
    condition.add("id", ps_id);
    DBRow data = getAllAreaInfoForStorage(condition);
    DBRow[] warehouseBase = (DBRow[])data.getValue("warehouseBase");
    DBRow[] areaData = (DBRow[])data.getValue("zones");
    DBRow[] locationData = (DBRow[])data.getValue("location");
    DBRow[] docksData = (DBRow[])data.getValue("docks");
    DBRow[] stagingData = (DBRow[])data.getValue("staging");
    DBRow[] parkingData = (DBRow[])data.getValue("parking");
    DBRow[] webcamData = (DBRow[])data.getValue("webcam");

    List titleList = new ArrayList();

    List areaDoorList = new ArrayList();

    DBRow newAreaId = new DBRow();
    DBRow newDocksId = new DBRow();

    if ((warehouseBase != null) && (warehouseBase.length > 0))
    {
      this.floorGoogleMapsMgrCc.deleteStorageWarehouseBaseByPsid(ps_id);
      for (int i = 0; i < warehouseBase.length; i++) {
        this.floorGoogleMapsMgrCc.addStorageWarehouseBase(warehouseBase[i]);
      }
    }

    if ((areaData != null) && (areaData.length > 0)) {
      for (int i = 0; i < areaData.length; i++) {
        DBRow r = areaData[i];
        String areaName = r.getString("area_name");
        long areaIdTmp = r.get("area_id", 0L);
        if ((areaIdTmp == 0L) && (newAreaId.get(areaName, 0L) == 0L)) {
          int area_type = 0;
          String area_type_str = r.getString("area_type");
          switch (area_type_str) { case "location":
            area_type = 1; break;
          case "docks":
            area_type = 2; break;
          case "parking":
            area_type = 3;
          }
          DBRow dbrow = new DBRow();
          dbrow.add("area_name", areaName);
          dbrow.add("area_psid", ps_id);
          dbrow.add("area_img", "");
          dbrow.add("area_type", area_type);
          dbrow.add("x", r.getString("x"));
          dbrow.add("y", r.getString("y"));
          dbrow.add("width", r.getString("width"));
          dbrow.add("height", r.getString("height"));
          dbrow.add("angle", r.getString("angle"));
          dbrow.add("latlng", r.getString("latlng"));
          areaIdTmp = this.floorGoogleMapsMgrCc.addLocationArea(dbrow);
          newAreaId.add(areaName, areaIdTmp);
        }
        String titleName = r.getString("title");
        if (!StringUtil.isBlank(titleName)) {
          String[] titles = titleName.split(",");
          for (int j = 0; j < titles.length; j++) {
            DBRow dbrow = new DBRow();
            dbrow.add("area_id", areaIdTmp);
            dbrow.add("title_name", titles[j].trim());
            dbrow.add("title_id", r.getString("title_id"));
            titleList.add(dbrow);
          }
        }
        String docks = r.getString("docks");
        if (!StringUtil.isBlank(docks)) {
          DBRow dbrow = new DBRow();
          dbrow.add("area_id", areaIdTmp);
          dbrow.add("docks", docks);
          areaDoorList.add(dbrow);
        }
      }
    }

    if ((locationData != null) && (locationData.length > 0)) {
      for (int i = 0; i < locationData.length; i++) {
        DBRow r = locationData[i];
        long id = r.get("slc_id", 0L);
        if (id == 0L) {
          DBRow dbrow = new DBRow();
          long area_id = r.get("area_id", 0L) == 0L ? newAreaId.get(r.getString("area_name"), 0L) : r.get("area_id", 0L);
          dbrow.add("slc_area", area_id);
          dbrow.add("slc_type", "");
          dbrow.add("slc_psid", ps_id);
          dbrow.add("slc_x", 0);
          dbrow.add("slc_y", 0);
          dbrow.add("slc_position", r.getString("placemark_name"));
          dbrow.add("slc_ps_title", title);
          dbrow.add("is_three_dimensional", r.getString("is3D"));
          dbrow.add("x", r.getString("x"));
          dbrow.add("y", r.getString("y"));
          dbrow.add("width", r.getString("width"));
          dbrow.add("height", r.getString("height"));
          dbrow.add("angle", r.getString("angle"));
          dbrow.add("latlng", r.getString("latlng"));
          id = this.floorGoogleMapsMgrCc.addLocationCatalog(dbrow);
          this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 1);

          Object nodeProp = new HashMap();
          ((Map)nodeProp).put("is_three_dimensional", Integer.valueOf(0));
          ((Map)nodeProp).put("slc_id", Long.valueOf(id));
          ((Map)nodeProp).put("slc_area", Integer.valueOf(r.get("area_id", 0) == 0 ? newAreaId.get(r.getString("area_name"), 0) : r.get("area_id", 0)));
          ((Map)nodeProp).put("slc_position", r.getString("placemark_name").trim());
          ((Map)nodeProp).put("ps_id", Long.valueOf(ps_id));
          ((Map)nodeProp).put("slc_type", Integer.valueOf(1));
          addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, (Map)nodeProp);
        }
      }
      createKmlNew(ps_id);

      deleteInvalidKml(System.getProperty("java.io.tmpdir") + File.separator + "kml" + File.separator);
    }

    if ((docksData != null) && (docksData.length > 0)) {
      for (int i = 0; i < docksData.length; i++) {
        DBRow r = docksData[i];
        long id = r.get("door_id", 0L);
        if (id == 0L) {
          DBRow doorRow = new DBRow();
          long area_id = r.get("area_id", 0L) == 0L ? newAreaId.get(r.getString("area_name"), 0L) : r.get("area_id", 0L);
          doorRow.add("doorId", r.getString("placemark_name"));
          doorRow.add("ps_id", ps_id);
          doorRow.add("area_id", area_id);
          doorRow.add("x", r.getString("x"));
          doorRow.add("y", r.getString("y"));
          doorRow.add("width", r.getString("width"));
          doorRow.add("height", r.getString("height"));
          doorRow.add("angle", r.getString("angle"));
          doorRow.add("latlng", r.getString("latlng"));
          id = this.floorGoogleMapsMgrCc.addStorageDoor(doorRow);
          this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 2);
        }
        newDocksId.add(r.getString("placemark_name"), id);
      }

    }

    if ((stagingData != null) && (stagingData.length > 0)) {
      for (int i = 0; i < stagingData.length; i++) {
        DBRow r = stagingData[i];
        long id = r.get("staging_id", 0L);
        if (id == 0L) {
          DBRow rows = new DBRow();
          long area_id = r.get("door_id", 0L) == 0L ? newDocksId.get(r.getString("door_name"), 0L) : r.get("door_id", 0L);
          rows.add("location_name", r.getString("placemark_name"));
          rows.add("psc_id", ps_id);
          rows.add("sd_id", area_id);
          rows.add("x", r.getString("x"));
          rows.add("y", r.getString("y"));
          rows.add("width", r.getString("width"));
          rows.add("height", r.getString("height"));
          rows.add("angle", r.getString("angle"));
          rows.add("latlng", r.getString("latlng"));
          id = this.floorGoogleMapsMgrCc.addLoadUnloadLocation(rows);
          this.floorGoogleMapsMgrCc.updateStorageAreaType(area_id, 2);

          Object nodeProp = new HashMap();
          ((Map)nodeProp).put("is_three_dimensional", Integer.valueOf(0));
          ((Map)nodeProp).put("slc_id", Long.valueOf(id));
          ((Map)nodeProp).put("slc_position", r.getString("placemark_name").trim());
          ((Map)nodeProp).put("ps_id", Long.valueOf(ps_id));
          ((Map)nodeProp).put("slc_type", Integer.valueOf(2));
          addNode(FloorProductStoreMgr.NodeType.StorageLocationCatalog, (Map)nodeProp);
        }
      }

    }

    if ((parkingData != null) && (parkingData.length > 0)) {
      for (int i = 0; i < parkingData.length; i++) {
        DBRow r = parkingData[i];
        long id = r.get("parking_id", 0L);
        if (id == 0L) {
          DBRow rows = new DBRow();
          rows.add("yc_no", r.getString("placemark_name"));
          rows.add("ps_id", ps_id);
          rows.add("yc_status", 1);
          rows.add("area_id", r.get("area_id", 0) == 0 ? newAreaId.get(r.getString("area_name"), 0) : r.get("area_id", 0));
          rows.add("x", r.getString("x"));
          rows.add("y", r.getString("y"));
          rows.add("width", r.getString("width"));
          rows.add("height", r.getString("height"));
          rows.add("angle", r.getString("angle"));
          rows.add("latlng", r.getString("latlng"));
          id = this.floorGoogleMapsMgrCc.addStorageYardControl(rows);
        }
      }

    }

    if (!areaDoorList.isEmpty()) {
      for (int i = 0; i < areaDoorList.size(); i++) {
        DBRow row = (DBRow)areaDoorList.get(i);
        long areaId = row.get("area_id", 0L);
        String docks = row.getString("docks");
        String[] dock = docks.split(",");
        for (int j = 0; j < dock.length; j++) {
          long dockId = newDocksId.get(dock[j].trim(), 0L);
          if (dockId != 0L) {
            this.floorGoogleMapsMgrCc.addStorageAreaDoor(areaId, dockId);
          }
        }
      }
    }

    if (webcamData.length > 0) {
      for (int i = 0; i < webcamData.length; i++) {
        DBRow webcam = new DBRow();
        webcam.add("ps_id", webcamData[i].getString("ps_id"));
        webcam.add("ip", webcamData[i].getString("ip"));
        webcam.add("port", webcamData[i].getString("port"));
        webcam.add("user", webcamData[i].getString("user"));
        webcam.add("password", webcamData[i].getString("password"));
        webcam.add("x", webcamData[i].getString("x"));
        webcam.add("y", webcamData[i].getString("y"));
        webcam.add("inner_radius", webcamData[i].getString("area_name"));
        webcam.add("outer_radius", webcamData[i].getString("placemark_name"));
        webcam.add("s_degree", webcamData[i].getString("width"));
        webcam.add("e_degree", webcamData[i].getString("height"));
        webcam.add("latlng", webcamData[i].getString("latlng"));
        long id = Long.parseLong(webcamData[i].getString("id"));
        if (id == 0L) {
          this.floorGoogleMapsMgrCc.addWebcam(webcam);
        }
      }

    }

    if (!titleList.isEmpty()) {
      savaTitle(titleList);
    }
    result.add("flag", "true");
    return result;
  }

  public DBRow getAllAreaInfoForStorage(DBRow storage) throws Exception
  {
    try {
      return this.floorLocationAreaXmlImportMgrCc.getAllAreaInfoForStorage(storage);
    } catch (Exception e) {
      throw new Exception("StoragesCotroller.getAllAreaInfoForStorage" + e);
    }
  }

  @Transactional
  public void savaTitle(List<DBRow> cam) throws Exception {
    Map titleIds = new HashMap();
    Iterator it = cam.iterator();
    while (it.hasNext()) {
      DBRow row = (DBRow)it.next();
      String title = row.getString("title_name");
      long titleId = row.get("title_id", 0L);
      long areaId = row.get("area_id", 0L);
      if (titleId == 0L) {
        if (titleIds.containsKey(title)) {
          titleId = ((Long)titleIds.get(title)).longValue();
        } else {
          DBRow[] t = this.floorGoogleMapsMgrCc.getTitle(title);
          if (t.length > 0) {
            titleId = t[0].get("title_id", 0L);
          } else {
            DBRow r = new DBRow();
            r.add("title_name", title);
            titleId = this.floorGoogleMapsMgrCc.addTitle(r);
          }
          titleIds.put(title, Long.valueOf(titleId));
        }
      }
      DBRow[] zt = this.floorGoogleMapsMgrCc.getZoneTitle(areaId, titleId);
      if (zt.length == 0) {
        DBRow r = new DBRow();
        r.add("area_id", areaId);
        r.add("title_id", titleId);
        this.floorGoogleMapsMgrCc.addZoneTitle(r);
      }
    }
  }

  public void addNode(FloorProductStoreMgr.NodeType nodeType, Map<String, Object> props) throws Exception
  {
    Map map = new HashMap();
    map.put("slc_id", props.get("slc_id"));
    map.put("ps_id", props.get("ps_id"));
    map.put("slc_type", props.get("slc_type"));
    long ps_id = Long.parseLong(props.get("ps_id").toString());
    DBRow[] rows = this.floorProductStoreMgr.searchNodes(ps_id, nodeType, map);
    if (rows.length <= 0)
      this.floorProductStoreMgr.addNode(ps_id, nodeType, props);
    else
      this.floorProductStoreMgr.updateNode(ps_id, nodeType, map, props);
  }

  @Transactional
  @RequestMapping(value={"/storageCotroller/deleteStorageLayer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Map<String, Object> deleteStorageLayer(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="location_id", required=true) long id, @RequestParam(value="type", required=true) int type) throws Exception {
    Map ret = new HashMap();
    try
    {
      int del_id = 0;
      switch (type) {
      case 5:
        this.floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(id);
        this.floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(id);
        del_id = this.floorGoogleMapsMgrCc.deleteAreaById(ps_id, id);
        break;
      case 2:
        del_id = this.floorGoogleMapsMgrCc.deleteStagingById(ps_id, id);
        deleteNeo4jSlcNode(ps_id, id, 2);
        break;
      case 3:
        del_id = this.floorGoogleMapsMgrCc.deleteDocksById(ps_id, id);
        break;
      case 4:
        del_id = this.floorGoogleMapsMgrCc.deletePrakingById(ps_id, id);
        break;
      case 1:
        del_id = this.floorGoogleMapsMgrCc.deleteLocationById(ps_id, id);
        deleteNeo4jSlcNode(ps_id, id, 1);
        break;
      case 6:
        del_id = this.floorGoogleMapsMgrCc.deleteWebcamById(ps_id, id);
        break;
      case 7:
        del_id = this.floorGoogleMapsMgrCc.deletePrinterById(ps_id, id);
        break;
      case 9:
        del_id = this.floorGoogleMapsMgrCc.deleteLightById(ps_id, id);
      case 8:
      }
      ret.put("flag", "true");
      ret.put("id", Integer.valueOf(del_id));
      return ret;
    } catch (Exception e) {
      e.printStackTrace();
      ret.put("flag", "false");
    }throw new CustomException(ret);
  }

  @RequestMapping(value={"/storageCotroller/batchDeleteStorageLayer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Map<String, Object> batchDeleteStorageLayer(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="ids", required=true) String ids, @RequestParam(value="type", required=true) int type)
    throws Exception
  {
    Map ret = new HashMap();
    try {
      String[] idArray = ids.split(",");
      long[] id = new long[idArray.length];
      for (int i = 0; i < idArray.length; i++) {
        id[i] = Long.parseLong(idArray[i]);
      }
      int count = 0;
      switch (type) {
      case 5:
        this.floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(id);
        this.floorGoogleMapsMgrCc.deleteCheckTitleByAreaId(id);
        count = this.floorGoogleMapsMgrCc.deleteAreaById(ps_id, id);
        break;
      case 2:
        count = this.floorGoogleMapsMgrCc.deleteStagingById(ps_id, id);
        batchdeleteNeo4jSlcNode(ps_id, id, 2);
        break;
      case 3:
        count = this.floorGoogleMapsMgrCc.deleteDocksById(ps_id, id);
        break;
      case 4:
        count = this.floorGoogleMapsMgrCc.deletePrakingById(ps_id, id);
        break;
      case 1:
        count = this.floorGoogleMapsMgrCc.deleteLocationById(ps_id, id);
        batchdeleteNeo4jSlcNode(ps_id, id, 1);
        break;
      case 6:
        count = this.floorGoogleMapsMgrCc.deleteWebcamById(ps_id, id);
      }

      ret.put("flag", "true");
      ret.put("count", Integer.valueOf(count));
      return ret;
    } catch (Exception e) {
      e.printStackTrace();
      ret.put("flag", "false");
      ret.put("e", e.getMessage());
    }throw new CustomException(ret);
  }

  public void deleteNeo4jSlcNode(long ps_id, long slc_id, int type) throws Exception {
    try {
      Set groupBy = new HashSet();
      groupBy.add("slc_id");
      DBRow[] rows = this.floorProductStoreMgr.productCount(ps_id, 0L, type, slc_id, 0, null, 0L, null, null, 0L, groupBy);
      if (rows.length > 0) {
        throw new Exception("位置上有库存");
      }
      Map searchFor = new HashMap();
      searchFor.put("slc_id", Long.valueOf(slc_id));
      searchFor.put("slc_type", Integer.valueOf(type));
      this.floorProductStoreMgr.removeNodes(ps_id, FloorProductStoreMgr.NodeType.StorageLocationCatalog, searchFor, false);
    }
    catch (Exception e) {
      throw new Exception("Neo4jException");
    }
  }

  public void batchdeleteNeo4jSlcNode(long ps_id, long[] slc_ids, int type) throws Exception {
    try {
      if (slc_ids.length > 0) {
        Set groupBy = new HashSet();
        groupBy.add("slc_id");
        for (int i = 0; i < slc_ids.length; i++)
        {
          long slc_id = slc_ids[i];
          DBRow[] rows = this.floorProductStoreMgr.productCount(ps_id, 0L, type, slc_id, 0, null, 0L, null, null, 0L, groupBy);
          if (rows.length > 0) {
            throw new Exception("位置上有库存");
          }
        }
        this.floorProductStoreMgr.removeSlcNodes(ps_id, slc_ids, type, true);
      }
    } catch (Exception e) {
      throw new Exception(e.getMessage());
    }
  }

  @RequestMapping(value={"/storageCotroller/queryCam"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow queryWebcamInfo(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception
  {
    return this.floorGoogleMapsMgrCc.queryCamInfo(ps_id);
  }
}