package com.gis.exception;

import javax.servlet.http.HttpServletResponse;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionHandlerController
{
  @ExceptionHandler({CustomException.class})
  public void validationExceptionOccurs(HttpServletResponse res, CustomException ex)
    throws Exception
  {
    res.setContentType("application/json");
    res.setStatus(200);
    ObjectMapper json = new ObjectMapper();
    json.writeValue(res.getOutputStream(), ex.getResult());
  }
}