package com.gis.exception;

import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;
import java.util.HashMap;
import java.util.Map;

public class CustomException extends RuntimeException
{
  private Map<String, Object> result = null;

  private CustomException()
  {
  }

  public CustomException(Map<String, Object> msg)
  {
    this.result = msg;
  }

  public CustomException(DBRow row) {
    try {
      this.result = DBRowUtils.dbRowAsMap(row);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public CustomException(String st) {
    try {
      Map rt = new HashMap();
      rt.put("e", st);
      this.result = rt;
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public Map<String, Object> getResult()
  {
    return this.result;
  }
}