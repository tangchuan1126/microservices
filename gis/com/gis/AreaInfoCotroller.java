package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.app.floor.api.cc.FloorLocationAreaXmlImportMgrCc;
import com.cwc.db.DBRow;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AreaInfoCotroller
{

  @Autowired
  private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;

  @Autowired
  private FloorLocationAreaXmlImportMgrCc floorLocationAreaXmlImportMgrCc;

  @RequestMapping(value={"/areaInfoCotroller/queryAreaPerson"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAreaPersonByPsIdandArea(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="area_id", required=true) long area_id)
    throws Exception
  {
    return this.floorGoogleMapsMgrCc.getAreaPersonByPsIdandArea(ps_id, area_id);
  }
  @RequestMapping(value={"/areaInfoCotroller/queryAreaDoor"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAreaDoorByPsIdandArea(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="area_id", required=true) long area_id) throws Exception {
    return this.floorGoogleMapsMgrCc.getAreaDoorByPsIdandArea(ps_id, area_id);
  }

  @RequestMapping(value={"/areaInfoCotroller/queryAreaByPsId"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public DBRow[] getAreaByPsId(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    return this.floorGoogleMapsMgrCc.getAreaByPsId(ps_id);
  }
  public Map<String, List<Map<String, String>>> getAreaDatas(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    Map rt = new HashMap();
    List list = new ArrayList();
    DBRow[] areas = this.floorGoogleMapsMgrCc.getAreaByPsId(ps_id);
    if (areas.length > 0) {
      for (int i = 0; i < areas.length; i++) {
        Map map = new HashMap();
        DBRow r = areas[i];
        map.put("latlng", r.getString("latlng"));
        map.put("obj_name", r.getString("obj_name"));
        map.put("obj_id", r.getString("obj_id"));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }

  @RequestMapping(value={"/areaInfoCotroller/queryAreaByPsIdAndroid"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, List<Map<String, Object>>> getAreaByPsIdAndroid(@RequestParam(value="ps_id", required=true) long ps_id)
    throws Exception
  {
    Map rt = new HashMap();
    DBRow[] rows = this.floorGoogleMapsMgrCc.getAreaByPsId(ps_id);
    List list = new ArrayList();
    if (rows.length > 0) {
      for (int i = 0; i < rows.length; i++) {
        DBRow row = rows[i];
        Map map = new HashMap();
        map.put("latlng", row.getString("latlng", ""));
        map.put("obj_name", row.getString("obj_name", ""));
        map.put("obj_id", row.getString("obj_id", ""));
        list.add(map);
      }
    }
    rt.put("datas", list);
    return rt;
  }
  @RequestMapping(value={"/areaInfoCotroller/queryTilteGroupbyArea"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.GET})
  public Map<String, ArrayList<DBRow>> getTitleGroupbyArea(@RequestParam(value="ps_id", required=true) long ps_id) throws Exception {
    DBRow[] titles = this.floorGoogleMapsMgrCc.getAreaTitleByPsId(ps_id);
    Map map = new HashMap();
    if (titles.length > 0) {
      for (int i = 0; i < titles.length; i++) {
        String area_id = titles[i].getString("area_id");
        if (map.containsKey(area_id)) {
          ((ArrayList)map.get(area_id)).add(titles[i]);
        } else {
          ArrayList list = new ArrayList();
          list.add(titles[i]);
          map.put(area_id, list);
        }
      }
    }
    return map;
  }
  @RequestMapping(value={"/areaInfoCotroller/updatePersonAreaByAdid"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow insertPersonAreaByAdid(@RequestParam(value="adid", required=true) long adid, @RequestParam(value="area_id", required=true) int area_id, @RequestParam(value="ps_id", required=true) int ps_id) throws Exception {
    DBRow data = new DBRow();
    try {
      long id = this.floorGoogleMapsMgrCc.insertPersonAreaByAdid(ps_id, area_id, adid);
      data.add("flag", "true");
      data.add("id", id);

      return data;
    }
    catch (Exception e)
    {
      e = 
        e;

      data.add("flag", "false");
      e.printStackTrace();

      return data; } finally {  } return data;
  }
  @Transactional
  @RequestMapping(value={"/areaInfoCotroller/updateZoneResource"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow updateZoneResource(@RequestParam(value="area_id", required=true) long area_id, @RequestParam(value="sdIds", required=true) String sdIds) throws Exception {
    DBRow row = new DBRow();
    try {
      this.floorGoogleMapsMgrCc.deleteAreaDoorByAreaId(area_id);
      if ((sdIds != null) && (!sdIds.equals(""))) {
        String[] sdIds_ = sdIds.split(",");
        for (int i = 0; i < sdIds_.length; i++) {
          long adid = Long.parseLong(sdIds_[i]);
          this.floorLocationAreaXmlImportMgrCc.addStorageAreaDoor(area_id, adid);
        }
      }
      row.add("flag", "true");

      return row;
    }
    catch (Exception e)
    {
      e = 
        e;

      row.add("flag", "false");
      e.printStackTrace();

      return row; } finally {  } return row;
  }

  @Transactional
  @RequestMapping(value={"/areaInfoCotroller/updateZonePerson"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow updateZonePerson(@RequestParam(value="ps_id", required=true) int ps_id, @RequestParam(value="area_id", required=true) int area_id, @RequestParam(value="adids", required=true) String adids, @RequestParam(value="oldAdids", required=true) String oldAdids) throws Exception {
    DBRow row = new DBRow();
    try {
      if ((oldAdids != null) && (!oldAdids.equals(""))) {
        String[] oldAdidStr = oldAdids.split(",");
        for (int i = 0; i < oldAdidStr.length; i++) {
          long oldAdid = Long.parseLong(oldAdidStr[i]);
          this.floorGoogleMapsMgrCc.deletePersonAreaByAdid(ps_id, area_id, oldAdid);
        }
      }
      if ((adids != null) && (!adids.equals(""))) {
        String[] adids_ = adids.split(",");
        for (int i = 0; i < adids_.length; i++) {
          long adid = Long.parseLong(adids_[i]);
          this.floorGoogleMapsMgrCc.insertPersonAreaByAdid(ps_id, area_id, adid);
        }
      }
      row.add("flag", "true");

      return row;
    }
    catch (Exception e)
    {
      e = 
        e;

      row.add("flag", "false");
      e.printStackTrace();

      return row; } finally {  } return row;
  }
}