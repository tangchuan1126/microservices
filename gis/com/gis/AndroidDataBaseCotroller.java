package com.gis;

import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import us.monoid.json.JSONObject;

@RestController
public class AndroidDataBaseCotroller {
    @Autowired
    private FloorGoogleMapsMgrCc floorGoogleMapsMgrCc;
    @Autowired
    private AreaInfoCotroller areaCotroller;
    @Autowired
    private DockInfoCotroller dockCotroller;
    @Autowired
    private StagingInfoCotroller stagingCotroller;
    @Autowired
    private StoragesCotroller storageCotroller;
    @Autowired
    private LocationInfoCotroller locationCotroller;

    public AndroidDataBaseCotroller() {
    }

    @Transactional
    @RequestMapping(
            value = {"/androidCotroller/storageDatas"},
            produces = {"application/zip"},
            method = {RequestMethod.GET}
    )
    public void androidBase(HttpServletResponse res, @RequestParam(
            value = "version",
            required = true
    ) long version) throws Exception {
        long baseVerson = Long.parseLong(this.floorGoogleMapsMgrCc.getGisVersion().getString("confvalue", ""));
        this.doZip(baseVerson);
        if (baseVerson != version) {
            res.setContentType("application/zip;charset=UTF-8");
            FileInputStream is = new FileInputStream(System.getProperty("java.io.tmpdir") + File.separator + "android" + File.separator + "storageDatas" + ".zip");
            IOUtils.copy(is, res.getOutputStream());
            is.close();
        }

    }

    public void doZip(long baseVerson) throws Exception {
        Map datas = this.getStorageData();
        Map base = this.getStorageBaseData();
        String path = System.getProperty("java.io.tmpdir") + File.separator + "android";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdir();
        }

        String fileName = path + File.separator + "storageDatas.zip";
        File deletefile = new File(fileName);
        if (deletefile.exists()) {
            deletefile.delete();
        }

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(fileName));
        out.putNextEntry(new ZipEntry("/"));

        try {
            Iterator e = datas.keySet().iterator();

            String vbs;
            String js;
            while (e.hasNext()) {
                vbs = (String) e.next();
                Map map = (Map) datas.get(vbs);
                Iterator dt = map.keySet().iterator();

                while (dt.hasNext()) {
                    js = (String) dt.next();
                    Map name = (Map) map.get(js);
                    String bs = (new JSONObject(name)).toString();
                    String name1 = vbs + "_" + js;
                    out.putNextEntry(new ZipEntry(name1 + ".txt"));
                    byte[] bs1 = bs.getBytes();
                    out.write(bs1);
                    out.closeEntry();
                }
            }

            e = base.keySet().iterator();

            while (e.hasNext()) {
                vbs = (String) e.next();
                HashMap map1 = new HashMap();
                Map dt1 = (Map) base.get(vbs);
                map1.put("datas", dt1);
                js = (new JSONObject(map1)).toString();
                String name2 = vbs + "_" + "base";
                out.putNextEntry(new ZipEntry(name2 + ".txt"));
                byte[] bs2 = js.getBytes();
                out.write(bs2);
                out.closeEntry();
            }

            String e1 = "{verson:" + baseVerson + "}";
            out.putNextEntry(new ZipEntry("version.txt"));
            byte[] vbs1 = e1.getBytes();
            out.write(vbs1);
            out.closeEntry();
        } catch (Exception var22) {
            var22.printStackTrace();
        } finally {
            out.close();
        }

    }

    public Map<String, Map<String, Map<String, List<Map<String, String>>>>> getStorageData() throws Exception {
        DBRow[] rows = this.floorGoogleMapsMgrCc.getAllStorageKml();
        HashMap rt = new HashMap();

        for (int i = 0; i < rows.length; ++i) {
            DBRow r = rows[i];
            long id = r.get("ps_id", 0L);
            HashMap oneStorageData = new HashMap();
            Map areaDatas = this.areaCotroller.getAreaDatas(id);
            Map docksingDatas = this.dockCotroller.getDocksData(id);
            Map parkingDatas = this.dockCotroller.getParkingData(id);
            Map stagingDatas = this.stagingCotroller.getStagingDatas(id);
            Map locationDatas = this.locationCotroller.queryLocationDatas(id);
            Map webcamDatas = this.storageCotroller.getWebcamDatas(id);
            oneStorageData.put("areas", areaDatas);
            oneStorageData.put("docks", docksingDatas);
            oneStorageData.put("parking", parkingDatas);
            oneStorageData.put("stagins", stagingDatas);
            oneStorageData.put("locations", locationDatas);
            oneStorageData.put("webcams", webcamDatas);
            rt.put(id + "", oneStorageData);
        }

        return rt;
    }

    public Map<String, Map<String, String>> getStorageBaseData() throws Exception {
        DBRow[] baseDatas = this.floorGoogleMapsMgrCc.getAllStroageBase();
        HashMap map = new HashMap();
        if (baseDatas.length > 0) {
            for (int i = 0; i < baseDatas.length; ++i) {
                DBRow base = baseDatas[i];
                String ps_id = base.getString("ps_id", "");
                String title = base.getString("title", "");
                String latlng;
                if (map.containsKey(ps_id)) {
                    String mm = base.getString("lng") + "," + base.getString("lat") + ",0.0 ";
                    latlng = ((Map) map.get(ps_id)).get("latlng") + "" + mm;
                    ((Map) map.get(ps_id)).put("latlng", latlng);
                } else {
                    HashMap var9 = new HashMap();
                    latlng = base.getString("lng") + "," + base.getString("lat") + ",0.0 ";
                    var9.put("latlng", latlng);
                    var9.put("name", title);
                    map.put(ps_id, var9);
                }
            }
        }

        return map;
    }
}
