package com.javaps.springboot;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

@Configuration
@Profile({"development", "production"})
public class SessionAttrConfig
{

  @Autowired
  private RequestMappingHandlerAdapter adapter;

  @PostConstruct
  public void prioritizeCustomArgumentMethodHandlers()
  {
    List argumentResolvers = new ArrayList();

    argumentResolvers.add(new SessionAttrResolver());
    argumentResolvers.addAll(this.adapter.getArgumentResolvers());

    this.adapter.setArgumentResolvers(argumentResolvers);
  }
}