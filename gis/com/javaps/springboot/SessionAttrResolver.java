package com.javaps.springboot;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

public class SessionAttrResolver
  implements HandlerMethodArgumentResolver
{
  public boolean supportsParameter(MethodParameter parameter)
  {
    return parameter.hasParameterAnnotation(SessionAttr.class);
  }

  public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory)
    throws Exception
  {
    SessionAttr ann = (SessionAttr)parameter.getParameterAnnotation(SessionAttr.class);
    if (ann == null) return WebArgumentResolver.UNRESOLVED;
    HttpServletRequest req = (HttpServletRequest)webRequest.getNativeRequest();
    HttpSession sess = req.getSession(false);
    if (sess == null) return null;
    return sess.getAttribute(ann.value());
  }
}