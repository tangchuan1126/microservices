package com.javaps.springboot;

import com.cwc.app.util.ConfigBean;
import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;
import com.cwc.init.StorageJetLagInit;
import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import com.radiadesign.catalina.session.RedisSessionHandlerValve;
import com.radiadesign.catalina.session.RedisSessionManager;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.catalina.Context;
import org.apache.catalina.Valve;
import org.codehaus.jackson.map.ObjectMapper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.tomcat.TomcatContextCustomizer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.data.authentication.UserCredentials;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan({"com.javaps.springboot", "com.gis", "com.invent"})
@EnableAutoConfiguration
@ImportResource({"classpath:applicationContext.xml"})
@EnableMongoRepositories(basePackages={"com.gis.model"})
public class MicroServiceApp
  implements Filter
{

  @Value("${redis.host}")
  private String redis_host;

  @Value("${session.maxInactiveInterval.web}")
  private int session_maxInactiveInterval_web;

  @Value("${session.maxInactiveInterval.mobile}")
  private int session_maxInactiveInterval_mobile;

  @Value("${session.timeout}")
  private int session_timeout;

  @Value("${config.path}")
  private String configPath;

  @Value("${spring.data.mongodb.host}")
  private String mongoHost;

  @Value("${spring.data.mongodb.username}")
  private String mongoUsername;

  @Value("${spring.data.mongodb.password_plain}")
  private String mongoPassword;

  @Autowired
  private DBUtilAutoTran dbUtilAutoTran;

  @Autowired
  private StorageJetLagInit storageJetLagInit;
  private RedisSessionManager rsm;

  public MicroServiceApp()
  {
    this.rsm = new RedisSessionManager();
  }
  @Bean
  public MongoDbFactory mongoDbFactory() throws Exception { UserCredentials userCredentials = new UserCredentials(this.mongoUsername.trim(), this.mongoPassword.trim());
    return new SimpleMongoDbFactory(new MongoClient(new ServerAddress(this.mongoHost)), "gps", userCredentials); }

  @Bean
  public MongoTemplate mongoTemplate() throws Exception {
    return new MongoTemplate(mongoDbFactory());
  }

  public static void main(String[] args)
  {
    ConfigBean.putBeans("admin", "admin");
    DBRow.setKeyUseUpper(false);
    DBRow.setKeyIgnoreCase(true);
    SpringApplication.run(MicroServiceApp.class, args);
  }

  @Bean
  public EmbeddedServletContainerFactory servletContainer() {
    TomcatEmbeddedServletContainerFactory factory = new TomcatEmbeddedServletContainerFactory();
    RedisSessionHandlerValve rshv = new RedisSessionHandlerValve();

    factory.addContextValves(new Valve[] { rshv });
    factory.addContextCustomizers(new TomcatContextCustomizer[] { new TomcatContextCustomizer()
    {
      public void customize(Context context)
      {
        MicroServiceApp.this.rsm.setHost(MicroServiceApp.this.redis_host);
        MicroServiceApp.this.rsm.setPort(6379);
        MicroServiceApp.this.rsm.setDatabase(0);
        MicroServiceApp.this.rsm.setMaxInactiveInterval(MicroServiceApp.this.session_maxInactiveInterval_web);
        MicroServiceApp.this.rsm.setSerializationStrategyClass("com.radiadesign.catalina.session.JsonSerializer");
        context.setManager(MicroServiceApp.this.rsm);
        context.setSessionCookiePath("/Sync10");
        context.setSessionCookiePathUsesTrailingSlash(true);
        context.setSessionTimeout(MicroServiceApp.this.session_timeout);

        MicroServiceApp.putBeans(MicroServiceApp.this.configPath);
      }
    }
     });
    return factory;
  }

  @Bean
  public FilterRegistrationBean authFilter() {
    FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
    filterRegBean.setFilter(this);
    List urlPatterns = new ArrayList();
    urlPatterns.add("/*");
    filterRegBean.setUrlPatterns(urlPatterns);
    return filterRegBean;
  }

  public void init(FilterConfig filterConfig) throws ServletException
  {
    try
    {
      this.storageJetLagInit.init(this.dbUtilAutoTran);
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  }

  private DBRow getJsonObjectMutiRequest(ServletRequest request) {
    DBRow returnRow = new DBRow();
    Enumeration enumer = request.getParameterNames();
    while ((enumer != null) && (enumer.hasMoreElements())) {
      String key = (String)enumer.nextElement();
      returnRow.add(key, request.getParameter(key));
    }
    return returnRow;
  }

  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
    throws IOException, ServletException
  {
    HttpServletRequest req = (HttpServletRequest)request;
    HttpServletResponse res = (HttpServletResponse)response;
    res.setCharacterEncoding("UTF-8");
    HttpSession sess = req.getSession(false);
    DBRow datas = getJsonObjectMutiRequest(request);
    if (sess == null) sess = getSessionByHeader(req);
    Map m = sess == null ? null : (Map)sess.getAttribute("adminSesion");
    if ((!req.getRequestURI().startsWith("/public/")) && ((m == null) || (!Boolean.TRUE.equals(m.get("login")))))
    {
      m = new HashMap();
      m.put("error", "未认证的访问");
      res.setStatus(401);
      res.setContentType("application/json; charset=UTF-8");
      new ObjectMapper().writeValue(res.getWriter(), m);
      return;
    }
    chain.doFilter(new CustomHttpServletRequest(req, sess), response);
  }

  private HttpSession getSessionByHeader(HttpServletRequest req) throws IOException {
    String sessionId = req.getHeader("JSESSIONID");

    if (sessionId == null) return null;
    HttpSession sess = this.rsm.loadSessionFromRedis(sessionId);

    if (sess != null)
    {
      sess.setMaxInactiveInterval(this.session_maxInactiveInterval_mobile);
    }

    return sess;
  }

  public void destroy()
  {
  }

  public static void putBeans(String urlMppingPath)
  {
    InputStream is = MicroServiceApp.class.getResourceAsStream(urlMppingPath);

    SAXReader reader = new SAXReader();
    Document document = null;
    try
    {
      document = reader.read(is);
    }
    catch (DocumentException e)
    {
      e.printStackTrace();
    }

    Element root = document.getRootElement();

    String tagName = null;
    String tagValue = null;

    ArrayList elementAl = new ArrayList();

    for (Iterator i = root.elementIterator(); i.hasNext(); )
    {
      Element element = (Element)i.next();

      tagName = element.attributeValue("name");
      tagValue = element.attributeValue("value");

      if (ConfigBean.getStringValue(tagName) == null)
      {
        ConfigBean.putBeans(tagName, tagValue);
      }
    }
  }

  class CustomHttpServletRequest extends HttpServletRequestWrapper
  {
    private HttpSession session;

    CustomHttpServletRequest(HttpServletRequest req, HttpSession sess)
    {
      super();
      this.session = sess;
    }

    public HttpSession getSession(boolean create)
    {
      return this.session;
    }

    public HttpSession getSession() {
      return this.session;
    }
  }
}