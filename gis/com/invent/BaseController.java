package com.invent;

import com.cwc.app.floor.api.FloorProductStoreMgr;
import com.cwc.app.floor.api.FloorProductStoreMgr.NodeType;
import com.cwc.app.floor.api.cc.FloorGoogleMapsMgrCc;
import com.cwc.db.DBRow;
import com.gis.exception.CustomException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

@RestController
public class BaseController
{

  @Autowired
  private FloorProductStoreMgr productStoreMgr;

  @Autowired
  private FloorGoogleMapsMgrCc googleMapsMgr;

  @RequestMapping(value={"/BaseController/putAway"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow putAway(@RequestBody Map<String, Object> map)
    throws Exception
  {
    DBRow rt = new DBRow();
    try {
      Map slc_props = new HashMap();
      long ps_id = Long.parseLong(map.get("ps_id").toString());
      ArrayList cons = (ArrayList)map.get("con_ids");
      Long[] con_ids = new Long[cons.size()];
      for (int i = 0; i < cons.size(); i++) {
        Number l = (Number)cons.get(i);
        con_ids[i] = Long.valueOf(l.longValue());
      }
      slc_props.put("slc_id", map.get("slc_id").toString());
      slc_props.put("slc_type", map.get("slc_type").toString());
      this.productStoreMgr.putAway(ps_id, slc_props, con_ids);
      rt.add("ret", "1");
      rt.add("err", "0");
      return rt;
    } catch (Exception e) {
      rt.add("ret", "0");
      rt.add("e", e.getMessage());
      rt.add("err", "1");
      e.printStackTrace();
    }throw new CustomException(rt);
  }

  @Transactional
  @RequestMapping(value={"/BaseController/deleteContainer"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow deleteContainer(@RequestParam(value="ps_id", required=true) long ps_id, @RequestParam(value="con_id", required=true) long con_id) throws Exception {
    DBRow row = new DBRow();
    try {
      Map searchFor = new HashMap();
      searchFor.put("con_id", Long.valueOf(con_id));
      int counts = this.productStoreMgr.removeNodes(ps_id, FloorProductStoreMgr.NodeType.Container, searchFor, true);
      row.add("err", "0");
      row.add("ret", "1");
      row.add("counts", counts);
      return row;
    } catch (Exception e) {
      row.add("err", "1");
      row.add("ret", "0");
      row.add("e", e.getMessage());
    }throw new CustomException(row);
  }

  @RequestMapping(value={"/BaseController/copyTree"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow copyTree(@RequestBody Map<String, Object> maps)
    throws Exception
  {
    DBRow ret = new DBRow();
    try {
      List list = (List)maps.get("data");
      for (int i = 0; i < list.size(); i++) {
        JSONObject con = new JSONObject();
        JSONArray products = new JSONArray();
        JSONObject product = new JSONObject();
        Map map = (Map)list.get(i);
        long ps_id = Long.parseLong(map.get("ps_id").toString());
        Map con_props = (Map)map.get("con_props");
        Map product_props = (Map)map.get("product_props");
        Map rel_props = (Map)map.get("rel_props");

        Map slc_props = map.get("slc_props") == null ? null : (Map)map.get("slc_props");
        Map locates_props = (Map)map.get("locates_props");
        Map locProps = new HashMap();
        if (slc_props != null) {
          DBRow[] rows = this.productStoreMgr.searchNodes(ps_id, FloorProductStoreMgr.NodeType.StorageLocationCatalog, slc_props);
          if (rows.length == 0)
          {
            locProps.put("slc_id", Integer.valueOf(1));
            locProps.put("slc_type", Integer.valueOf(1));
          } else {
            locProps.put("slc_id", slc_props.get("slc_id"));
            locProps.put("slc_type", slc_props.get("slc_type"));
          }
        } else {
          locProps.put("slc_id", Integer.valueOf(1));
          locProps.put("slc_type", Integer.valueOf(1));
        }
        int[] catalogs = getCatalogs(Long.parseLong(product_props.get("pc_id").toString()));
        con.put("con_id", con_props.get("con_id"));
        con.put("container", con_props.get("container"));
        con.put("container_type", con_props.get("container_type"));
        con.put("is_full", con_props.get("is_full"));
        con.put("is_has_sn", con_props.get("is_has_sn"));
        con.put("type_id", con_props.get("type_id"));
        product.put("catalogs", catalogs);
        product.put("p_name", product_props.get("p_name"));
        product.put("pc_id", product_props.get("pc_id"));
        product.put("product_line", product_props.get("product_line"));
        product.put("quantity", rel_props.get("quantity"));
        product.put("title_id", product_props.get("title_id"));
        products.put(product);
        con.put("products", products);

        locProps.put("pc_id", locates_props.get("pc_id"));
        locProps.put("title_id", locates_props.get("title_id"));
        locProps.put("lot_number", locates_props.get("lot_number"));
        locProps.put("time_number", locates_props.get("time_number"));
        this.productStoreMgr.copyTree(con, ps_id, locProps);
      }
      ret.add("err", 0);
      ret.add("ret", 1);
      return ret;
    } catch (Exception e) {
      ret.add("err", 1);
      ret.add("ret", 0);
      ret.add("e", e.getMessage());
      e.printStackTrace();
    }throw new CustomException(ret);
  }

  public int[] getCatalogs(long pc_id)
    throws Exception
  {
    int[] catalogs = null;
    DBRow row = this.googleMapsMgr.queryCatalogs(pc_id);
    if (row == null) {
      catalogs = new int[0];
    } else {
      String ids = row.getString("catalog_id");
      if (!ids.equals("")) {
        String[] c_ids = ids.split(",");
        catalogs = new int[c_ids.length];
        for (int j = 0; j < c_ids.length; j++) {
          catalogs[j] = Integer.parseInt(c_ids[j]);
        }
      }
    }
    return catalogs;
  }

  @RequestMapping(value={"/BaseController/qureyInvenByslc"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public String qureyInvenByslc(@RequestBody Map<String, Object> map)
    throws Exception
  {
    long ps_id = Long.parseLong(map.get("ps_id").toString());
    Map searchForSlc = new HashMap();
    if (map.get("slc_id") != null) {
      searchForSlc.put("slc_id", map.get("slc_id"));
    }
    if (map.get("slc_type") != null) {
      searchForSlc.put("slc_type", map.get("slc_type"));
    }
    Long[] title_ids = null;
    if (map.get("title_ids") != null) {
      String[] titles = map.get("title_ids").toString().split(",");
      title_ids = new Long[titles.length];
      for (int i = 0; i < titles.length; i++) {
        title_ids[i] = Long.valueOf(Long.parseLong(titles[i]));
      }
    }
    long product_line = 0L;
    if (map.get("product_line") != null) {
      product_line = Long.parseLong(map.get("product_line").toString());
    }
    long catalog = 0L;
    if (map.get("catalog") != null) {
      catalog = Long.parseLong(map.get("catalog").toString());
    }
    String[] p_names = null;
    if (map.get("p_names") != null) {
      p_names = map.get("p_names").toString().split(",");
    }
    String[] lot_numbers = null;
    if (map.get("lot_numbers") != null) {
      lot_numbers = map.get("lot_numbers").toString().split(",");
    }
    int container_type = 0;
    if (map.get("container_type") != null) {
      container_type = Integer.parseInt(map.get("container_type").toString());
    }
    long type_id = 0L;
    if (map.get("type_id") != null) {
      type_id = Long.parseLong(map.get("type_id").toString());
    }
    String[] model_numbers = null;
    if (map.get("model_numbers") != null) {
      p_names = map.get("model_numbers").toString().split(",");
    }
    List lists = this.productStoreMgr.locationTree(ps_id, searchForSlc, title_ids, product_line, catalog, p_names, lot_numbers, container_type, type_id, model_numbers);

    return new JSONArray(lists).toString();
  }

  @RequestMapping(value={"/BaseController/queryHasInventLocationss"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public DBRow[] queryHasInventLocations(@RequestBody Map<String, Object> map)
    throws Exception
  {
    long ps_id = Long.parseLong(map.get("ps_id").toString());
    long[] title_ids = null;
    if (map.get("title_ids") != null) {
      String[] titles = map.get("title_ids").toString().split(",");
      title_ids = new long[titles.length];
      for (int i = 0; i < titles.length; i++) {
        title_ids[i] = Long.parseLong(titles[i]);
      }
    }
    long product_line = 0L;
    if (map.get("product_line") != null) {
      product_line = Long.parseLong(map.get("product_line").toString());
    }
    long catalog = 0L;
    if (map.get("catalog") != null) {
      catalog = Long.parseLong(map.get("catalog").toString());
    }
    String[] p_names = null;
    if (map.get("p_names") != null) {
      p_names = map.get("p_names").toString().split(",");
    }
    String[] lot_numbers = null;
    if (map.get("lot_numbers") != null) {
      lot_numbers = map.get("lot_numbers").toString().split(",");
    }
    int container_type = 0;
    if (map.get("container_type") != null) {
      container_type = Integer.parseInt(map.get("container_type").toString());
    }
    long type_id = 0L;
    if (map.get("type_id") != null) {
      type_id = Long.parseLong(map.get("type_id").toString());
    }
    int union_flag = -1;
    if (map.get("union_flag") != null) {
      type_id = Integer.parseInt(map.get("union_flag").toString());
    }

    return this.productStoreMgr.queryHasInventLocations(ps_id, title_ids, product_line, catalog, p_names, lot_numbers, container_type, type_id, union_flag);
  }

  @RequestMapping(value={"/BaseController/queryProductLine"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Set<Long> queryProductLine(@RequestBody Map<String, Object> map)
    throws Exception
  {
    long ps_id = Long.parseLong(map.get("ps_id").toString());
    long[] title_ids = null;
    if (map.get("title_ids") != null) {
      String[] titles = map.get("title_ids").toString().split(",");
      title_ids = new long[titles.length];
      for (int i = 0; i < titles.length; i++) {
        title_ids[i] = Long.parseLong(titles[i]);
      }
    }
    long catalog = 0L;
    if (map.get("catalog") != null) {
      catalog = Long.parseLong(map.get("catalog").toString());
    }
    String[] model_number = null;
    if (map.get("model_number") != null) {
      model_number = map.get("model_number").toString().split(",");
    }
    String[] p_names = null;
    if (map.get("p_names") != null) {
      p_names = map.get("p_names").toString().split(",");
    }
    return this.productStoreMgr.queryProductLine(ps_id, title_ids, catalog, p_names, model_number);
  }

  @RequestMapping(value={"/BaseController/querylotNumbers"}, produces={"application/json; charset=UTF-8"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
  public Set<String> querylotNumbers(@RequestBody Map<String, Object> map)
    throws Exception
  {
    long ps_id = Long.parseLong(map.get("ps_id").toString());
    long[] title_ids = null;
    if (map.get("title_ids") != null) {
      String[] titles = map.get("title_ids").toString().split(",");
      title_ids = new long[titles.length];
      for (int i = 0; i < titles.length; i++) {
        title_ids[i] = Long.parseLong(titles[i]);
      }
    }
    long product_line = 0L;
    if (map.get("product_line") != null) {
      product_line = Long.parseLong(map.get("product_line").toString());
    }
    long catalog = 0L;
    if (map.get("catalog") != null) {
      catalog = Long.parseLong(map.get("catalog").toString());
    }
    String[] p_names = null;
    if (map.get("p_names") != null) {
      p_names = map.get("p_names").toString().split(",");
    }
    String[] model_number = null;
    if (map.get("model_number") != null) {
      model_number = map.get("model_number").toString().split(",");
    }

    return this.productStoreMgr.lotNumbers(ps_id, title_ids, product_line, catalog, p_names, model_number);
  }
}