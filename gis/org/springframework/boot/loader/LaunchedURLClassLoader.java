package org.springframework.boot.loader;

import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.AccessController;
import java.security.PrivilegedActionException;
import java.security.PrivilegedExceptionAction;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import org.springframework.boot.loader.jar.Handler;
import org.springframework.boot.loader.jar.JarFile;

public class LaunchedURLClassLoader extends URLClassLoader
{
  private final ClassLoader rootClassLoader;

  public LaunchedURLClassLoader(URL[] urls, ClassLoader parent)
  {
    super(urls, parent);
    this.rootClassLoader = findRootClassLoader(parent);
  }

  private ClassLoader findRootClassLoader(ClassLoader classLoader) {
    while (classLoader != null) {
      if (classLoader.getParent() == null) {
        return classLoader;
      }
      classLoader = classLoader.getParent();
    }
    return null;
  }

  public URL getResource(String name)
  {
    URL url = null;
    if (this.rootClassLoader != null) {
      url = this.rootClassLoader.getResource(name);
    }
    return url == null ? findResource(name) : url;
  }

  public URL findResource(String name)
  {
    try {
      if ((name.equals("")) && (hasURLs())) {
        return getURLs()[0];
      }
      return super.findResource(name);
    } catch (IllegalArgumentException ex) {
    }
    return null;
  }

  public Enumeration<URL> findResources(String name)
    throws IOException
  {
    if ((name.equals("")) && (hasURLs())) {
      return Collections.enumeration(Arrays.asList(getURLs()));
    }
    return super.findResources(name);
  }

  private boolean hasURLs() {
    return getURLs().length > 0;
  }

  public Enumeration<URL> getResources(String name) throws IOException
  {
    if (this.rootClassLoader == null) {
      return findResources(name);
    }

    final Enumeration rootResources = this.rootClassLoader.getResources(name);
    final Enumeration localResources = findResources(name);

    return new Enumeration()
    {
      public boolean hasMoreElements()
      {
        return (rootResources.hasMoreElements()) || (localResources.hasMoreElements());
      }

      public URL nextElement()
      {
        if (rootResources.hasMoreElements()) {
          return (URL)rootResources.nextElement();
        }
        return (URL)localResources.nextElement();
      }
    };
  }

  protected Class<?> loadClass(String name, boolean resolve)
    throws ClassNotFoundException
  {
    synchronized (this) {
      Class loadedClass = findLoadedClass(name);
      if (loadedClass == null) {
        Handler.setUseFastConnectionExceptions(true);
        try {
          loadedClass = doLoadClass(name);
        }
        finally {
          Handler.setUseFastConnectionExceptions(false);
        }
      }
      if (resolve) {
        resolveClass(loadedClass);
      }
      return loadedClass;
    }
  }

  private Class<?> doLoadClass(String name) throws ClassNotFoundException
  {
    try
    {
      if (this.rootClassLoader != null) {
        return this.rootClassLoader.loadClass(name);
      }
    }
    catch (Exception ex)
    {
    }
    try
    {
      findPackage(name);
      return findClass(name);
    }
    catch (Exception ex)
    {
    }

    return super.loadClass(name, false);
  }

  private void findPackage(String name) throws ClassNotFoundException {
    int lastDot = name.lastIndexOf('.');
    if (lastDot != -1) {
      String packageName = name.substring(0, lastDot);
      if (getPackage(packageName) == null)
        try {
          definePackageForFindClass(name, packageName);
        }
        catch (Exception ex)
        {
        }
    }
  }

  private void definePackageForFindClass(final String name, final String packageName)
  {
    try
    {
      AccessController.doPrivileged(new PrivilegedExceptionAction()
      {
        public Object run() throws ClassNotFoundException {
          String path = name.replace('.', '/').concat(".class");
          for (URL url : LaunchedURLClassLoader.this.getURLs()) {
            try {
              if ((url.getContent() instanceof JarFile)) {
                JarFile jarFile = (JarFile)url.getContent();

                if ((jarFile.getJarEntryData(path) != null) && (jarFile.getManifest() != null))
                {
                  LaunchedURLClassLoader.this.definePackage(packageName, jarFile.getManifest(), url);
                  return null;
                }
              }
            }
            catch (IOException ex)
            {
            }
          }

          return null;
        }
      }
      , AccessController.getContext());
    }
    catch (PrivilegedActionException ex)
    {
    }
  }
}