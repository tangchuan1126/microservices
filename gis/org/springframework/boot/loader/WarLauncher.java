package org.springframework.boot.loader;

import java.io.IOException;
import java.util.List;
import org.springframework.boot.loader.archive.Archive;
import org.springframework.boot.loader.archive.Archive.Entry;
import org.springframework.boot.loader.archive.Archive.EntryRenameFilter;
import org.springframework.boot.loader.util.AsciiBytes;

public class WarLauncher extends ExecutableArchiveLauncher
{
  private static final AsciiBytes WEB_INF = new AsciiBytes("WEB-INF/");

  private static final AsciiBytes META_INF = new AsciiBytes("META-INF/");

  private static final AsciiBytes WEB_INF_CLASSES = WEB_INF.append("classes/");

  private static final AsciiBytes WEB_INF_LIB = WEB_INF.append("lib/");

  private static final AsciiBytes WEB_INF_LIB_PROVIDED = WEB_INF.append("lib-provided/");

  public boolean isNestedArchive(Archive.Entry entry)
  {
    if (entry.isDirectory()) {
      return entry.getName().equals(WEB_INF_CLASSES);
    }

    return (entry.getName().startsWith(WEB_INF_LIB)) || (entry.getName().startsWith(WEB_INF_LIB_PROVIDED));
  }

  protected void postProcessClassPathArchives(List<Archive> archives)
    throws Exception
  {
    archives.add(0, getFilteredArchive());
  }

  protected Archive getFilteredArchive()
    throws IOException
  {
    return getArchive().getFilteredArchive(new Archive.EntryRenameFilter()
    {
      public AsciiBytes apply(AsciiBytes entryName, Archive.Entry entry) {
        if ((entryName.startsWith(WarLauncher.META_INF)) || (entryName.startsWith(WarLauncher.WEB_INF))) {
          return null;
        }
        return entryName;
      }
    });
  }

  public static void main(String[] args) {
    new WarLauncher().launch(args);
  }
}