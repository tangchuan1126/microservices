#!/bin/bash
if [ -z "$1" ] || [ -z "$2" ] || [ ! -f "$2" ]
then
	echo "USAGE:  post_json.sh <JSESSIONID Value> <JSON file path>"
	exit 1
fi
curl -v \
-b "JSESSIONID=$1" \
--request POST \
--header "Content-Type: application/json" \
--data @$2 \
http://localhost/Sync10/springboot/