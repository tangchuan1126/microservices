基于Spring Boot的微服务工程模板
============================

请先浏览：

[微服务培训幻灯片](http://192.168.1.12/micro-services.pdf)

# 1. 项目设置

## 1.1 克隆项目

使用Git命令行工具或图形界面工具克隆本项目到开发机。

## 1.1 拷贝依赖包

克隆项目后，先在项目根目录下建一个lib子目录，从以下链接下载那三个jar文件放进去：

[点此下载依赖包](http://192.168.1.15/~frank/springbootdemo/)

如果上述地址的jar已经不是最新的，可以点击以下地址来重新构建最新版jar包：

[点此构建依赖包](http://192.168.1.15/~frank/springbootdemo.html)

注意：以上页面在请求时，会实时去从版本服务器中拉取并构建库的最新版本，所以***请不要频繁访问这个网页***！！！

## 1.2 在STS中导入项目

选择Import，选择导入Gradle Project，选中上述克隆目录，点按`Build Model`按钮，然后勾选项目并完成

## 1.3 刷新依赖

在STS中，右键点击SpringBootDemo项目，选择`Gradle -> Refresh Dependencies`

此操作第一次可能会时间久一些，因为它会解析并下载所有的依赖包


## 1.4 设置自动类重新载入（Class Reloading）

做此设置后，在开发中如果微服务处于运行状态，修改代码后，会自动重载而无需重启。

1. 在执行按钮菜单中选择`Run Configurations...`

	![](http://192.168.1.12/springbootdemo/pics/001.png)

2. 左侧选择Java Application下应用名称，右侧选择Arguments面板，在`VM arguments`处增加一行：

	![](http://192.168.1.12/springbootdemo/pics/002.png)

# 2. 执行单元测试

单元测试代码应该放在`src/test/java`目录下，其中已经放置了演示单元测试代码。执行方法同常规单元测试。

# 3. 运行

右键选择项目，选择`Run As -> Spring Boot App`

# 4. 生成发布包

右键选择项目，选择`Run As -> Gradle Build...`，在任务窗口中设置如下：

![](http://192.168.1.12/springbootdemo/pics/003.png)

然后点击`Run`按钮，此设置只需做一次，以后直接选择`Run As -> Gradle Build`即可。生成出来的jar位于`build/libs`目录下。

# 5. 在测试/生产环境启动微服务

假如生成的jar名为`SpringBootDemo.jar`，此jar是**all-in-one**的，包含了所有依赖，所以启动微服务只需要：

	java -jar SpringBootDemo.jar

如果要后台执行，可以：

	java -jar SpringBootDemo.jar > out.log 2> err.log &

# 6. 修改默认端口

此微服务默认监听8080端口，如果需要修改监听端口，可以在项目根目录下增加文件gradle.properties，里面写入一个属性值，例如：

	server.port=9090
	
如果在运行发布包时，临时需要修改端口，可以加命令行参数，例如：

	java -jar SpringBootDemo.jar --server.port=9090
	
# 7. nginx常用配置技巧

## 7.2 下载nginx

已经配置好Server端/Sync10等代理路径指向开发服务器的nginx绿色安装包（Windows）：

- [点此下载](http://192.168.1.15/~frank/nginx.zip)


## 7.2 配置代理路径

所谓代理路径是指，指定一个URI路径前缀，将这个前缀开头的所有HTTP请求，不是nginx自己处理，而是用反向代理方式转发给另一个外部服务器去处理，并抓取其响应并返回给请求端。例如：

	#先定义一个上游服务器（外部服务器）
	upstream tomcat {
		server 192.168.1.15:8000;
	}
	…………
	server {
		……
		location /Sync10/ {
			index index.jsp index.html index.htm;
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $remote_addr;
			proxy_set_header X-Real-IP $remote_addr;
			#将/Sync10/*的请求代理给名为tomcat的上游服务器的/Sync10/*
			proxy_pass http://tomcat/Sync10/;
			proxy_next_upstream error;
		}
		……
	}


如需集成其他的后端服务（微服务），前端开发员可以自行添加这类的映射，但请注意，为了能在对服务的请求中携带会话令牌Cookie，需要将这些Sync10主模块之外的服务映射为Sync10的下级路径，例如：

	upstream microservice {
        server 127.0.0.1:8080;
    }
    ……
    server {
    	……
		location /Sync10/ {
			……
		}
		……
		location /Sync10/_some_service/ {
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $remote_addr;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_pass http://microservice/;
            proxy_next_upstream error;
        }
        ……
    }
    
这样，所有到`http://localhost/Sync10/_some_service/*`的请求会被反向代理到`http://127.0.0.1:8080/*`，而且如果已经在`http://localhost/Sync10/`主模块登录的，这些对微服务的请求同样会带有会话的Cookie，从而可以让微服务通过这些会话ID最终读取到当前认证用户的信息。  
