package com.cwc.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import us.monoid.json.JSONArray;
import us.monoid.json.JSONObject;

import com.cwc.db.DBRow;

public class DBRowUtils {
	public static String dbRowAsString(DBRow para) throws Exception {
		StringBuilder sb = new StringBuilder();
		sb.append("{\n");
		String indent = "    ";
		for (Object field : para.getFieldNames()) {
			sb.append(indent).append(field).append("=")
					.append(para.getValue(field.toString())).append("\n");
		}
		sb.append("}");
		return sb.toString();
	}
	
	public static Map<String, Object> dbRowAsMap(DBRow para) throws Exception {
		
		Map<String, Object> result = new HashMap<String, Object>();
		for (Object field : para.getFieldNames()) {
			result.put(field.toString().toLowerCase(),
					para.getValue(field.toString()));
		}

		return result;
	}
	
	public static DBRow convertToDBRow(JSONObject jo) throws Exception {
		DBRow r = new DBRow();
		for (Iterator<String> itr = jo.keys(); itr.hasNext();) {
			String k = itr.next();
			r.add(k, jo.get(k));
		}
		return r;
	}
	
	public static DBRow convertToDBRow(JSONArray ja, String... keys) throws Exception {
		DBRow r = new DBRow();
		for(int i=0; i<keys.length && i<ja.length(); i++){
			if(keys[i] == null) continue; //skip field
			r.add(keys[i], ja.get(i));
		}
		return r;
	}

	public static JSONArray dbRowArrayAsJSON(DBRow[] rows) throws Exception {
		JSONArray a = new JSONArray();
		for(DBRow row:rows){
			a.put(dbRowAsMap(row));
		}
		return a;
	}
	
	public static Map<String,Object>  jsonObjectAsMap(JSONObject jo) throws Exception{
		Map<String,Object> m = new HashMap<String,Object>();
		for(Iterator<String> i=jo.keys(); i.hasNext(); ){
			String k = i.next();
			m.put(k, jo.get(k));
		}
		return m;
	}

	public static DBRow[] jsonArrayAsDBRowArray(JSONArray data, String... fieldNames) throws Exception {
		DBRow[] rows = new DBRow[data.length()];
		for(int i=0; i<data.length(); i++){
			rows[i] = convertToDBRow(data.getJSONArray(i), fieldNames);
		}
		return rows;
	}
	
	/**
	 * JSON 转换   DBRow内包含 DBRow[] 
	 * @param JSONObject
	 * @return DBRow
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public static DBRow convertToMultipleDBRow(JSONObject parameter) throws Exception {
		
		DBRow row = new DBRow();
		
		for (Iterator<String> result = parameter.keys(); result.hasNext();) {
			
			String key = result.next();
			
			if(parameter.get(key) instanceof JSONArray){
				
				row.add(key,jsonArrayAsDBRowArray((JSONArray)parameter.get(key)));
				continue;
			}
			
			row.add(key,parameter.get(key));
			
		}
		return row;
	}
	
	public static DBRow[] jsonArrayAsDBRowArray(JSONArray parameter) throws Exception {
		
		DBRow[] rows = new DBRow[parameter.length()];
		
		for(int i=0; i<parameter.length(); i++){
			
			rows[i] = convertToMultipleDBRow(parameter.getJSONObject(i));
			
		}
		return rows;
	}
	
	/**
	 * DBRow[]大写转小写
	 * @param DBRow数组 可包含DBRow数组 可包含DBRow 可包含普通值
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public static List<Object> dbrowToLowercase(DBRow[] param) throws Exception {
		
		List<Object> result = new ArrayList<Object>();
		
		if(param != null){
			
			for(DBRow oneResult:param){
				
				Map<String, Object> oneMap = new LinkedHashMap<String, Object>();
				
				for (Object field : oneResult.getFieldNames()) {
					
					//包含DBRow[]
					if(oneResult.getValue(field.toString()) instanceof DBRow[]){
						
						oneMap.put(field.toString().toLowerCase(), DBRowUtils.dbrowToLowercase((DBRow[])oneResult.getValue(field.toString())));
					//包含DBRow
					}else if(oneResult.getValue(field.toString()) instanceof DBRow){
						
						oneMap.put(field.toString().toLowerCase(), DBRowUtils.dbRowAsMap((DBRow)oneResult.getValue(field.toString())));
					//普通值
					}else{
						oneMap.put(field.toString().toLowerCase(),oneResult.getValue(field.toString()));
					}
				}
				result.add(oneMap);
			}
		}
		
		return result;
	}
	
	/**
	 * DBRow[]内包含无限DBRow,DBRow[] 转换为 JSON
	 * @param DBRow[]
	 * @return JSONArray
	 * @since Sync10-ui 1.0
	 * @author subin
	**/
	public static JSONArray multipleDBRowArrayAsJSON(DBRow[] rows) throws Exception {
		
		JSONArray a = new JSONArray();
		
		if(rows != null){
			
			for(DBRow row:rows){
				
				a.put(multipleDBRowAsMap(row));
			}
		}
		
		return a;
	}
	
	public static Map<String, Object> multipleDBRowAsMap(DBRow para) throws Exception {
		
		Map<String, Object> result = new LinkedHashMap<String, Object>();
		
		for (Object field : para.getFieldNames()) {
			
			//包含DBRow[]
			if(para.getValue(field.toString()) instanceof DBRow[]){
				//递归方法
				result.put(field.toString().toLowerCase(), DBRowUtils.multipleDBRowArrayAsJSON((DBRow[])para.getValue(field.toString())));
			//包含DBRow
			}else if(para.getValue(field.toString()) instanceof DBRow){
				
				result.put(field.toString().toLowerCase(), DBRowUtils.dbRowAsMap((DBRow)para.getValue(field.toString())));
			//普通值
			}else{
				result.put(field.toString().toLowerCase(),para.getValue(field.toString()));
			}
		}
		return result;
	}

	 public static DBRow mapConvertToDBRow(Map<String,Object> map)
		throws Exception
	 {
	   	DBRow dbrow = new DBRow();
	   	
	   	//dbrow.putAll(map);
	   	Set<String> mapKeys = map.keySet();
	   	for (String key : mapKeys) 
	   	{
			dbrow.add(key,map.get(key));
		}
	   	return dbrow;
	}
}
