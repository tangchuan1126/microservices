package com.cwc.app.floor.api.sbb;

import com.cwc.db.DBRow;
import com.cwc.db.DBUtilAutoTran;

public class FloorSystemConfigMgrSbb{
	
	private DBUtilAutoTran dbUtilAutoTran;
	
	public void setDbUtilAutoTran(DBUtilAutoTran dbUtilAutoTran){
		this.dbUtilAutoTran = dbUtilAutoTran;
	}
	
	/**
	 * 查询系统配置所有分类[递归]
	 * @param 分类的父ID
	 * @return 所有分类信息
	 * @author subin
	 * */
	public DBRow[] getSystemConfigClassify(long parentId) throws Exception{
		
		try {
			
			String sql = "select tab_id,tab_name,tab_parent_id,order_tab from system_config_tab where tab_parent_id = "+ parentId +" order by order_tab";
			
			DBRow[] systemConfig = dbUtilAutoTran.selectMutliple(sql);
			
			for(DBRow oneResult:systemConfig){
				
				DBRow[] systemConfigItem = getSystemConfigClassify(oneResult.get("tab_id", -1));
				
				if(systemConfigItem.length>0){
					
					oneResult.add("children", systemConfigItem);
				}
			}
			
			return systemConfig;
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigMenu(long parentId) error:" + e);
		}
	}
	
	/**
	 * 查询系统配置所有分类[树形结构]
	 * @param null
	 * @return 所有分类信息[树形结构]
	 * @author subin
	 * */
	public DBRow[] getSystemConfigClassifyTree() throws Exception{
		
		try {
			
			String sql = "select tab_id as id,tab_name as text,IF(tab_parent_id = 0,'#',CONVERT(tab_parent_id, CHAR))AS parent from system_config_tab order by tab_parent_id,order_tab";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigClassifyTree() error:" + e);
		}
	}
	
	/**
	 * 查询系统配置所有分类[树形结构][all]
	 * @param null
	 * @return 所有分类信息[树形结构][all]
	 * @author subin
	 * */
	public DBRow[] getSystemConfigClassifyAllTree() throws Exception{
		
		try {
			
			String sql = "select tab_id as id,tab_name as text,tab_parent_id AS parent from system_config_tab order by tab_parent_id,order_tab";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigClassifyAllTree() error:" + e);
		}
	}
	
	/**
	 * 查询系统配置所有tabID
	 * @param null
	 * @return 数组
	 * @author subin
	 * */
	public DBRow[] getSystemConfigTabId() throws Exception{
		
		try {
			
			String sql = "select belong_tab_id as tab_id from config group by belong_tab_id";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigTabId() error:" + e);
		}
	}
	
	/**
	 * 查询系统配置所有tab页下的配置信息
	 * @param tab_id
	 * @return 数组
	 * @author subin
	 * */
	public DBRow[] getSystemConfigContent(long id) throws Exception{
		
		try {
			
			String sql = "select id, confname as config_id,confvalue as config_value,description as config_name,config_type,order_config,pre_description,post_description from config where belong_tab_id = "+ id +" order by order_config;";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigContent(long id) error:" + e);
		}
	}
	
	/**
	 * 查询系统配置所有tab页下的配置信息的选项
	 * @param config_id
	 * @return 数组
	 * @author subin
	 * */
	public DBRow[] getSystemConfigOption(long id) throws Exception{
		
		try {
			
			String sql = "select option_key,option_value from system_config_option where belong_system_config_id = "+id;
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigOption(long id) error:" + e);
		}
	}
	
	/**
	 * 获取系统配置分类顺序+1
	 * @param 父分类ID
	 * @return 顺序值
	 * @author subin
	 * */
	public long getSystemConfigClassifyOrder(long id) throws Exception{
		
		try {
			
			String sql = "select max(order_tab)+1 as classify_order from system_config_tab where tab_parent_id = "+id;
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			return result.get("classify_order", 1);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigClassifyOrder(long id) error:" + e);
		}
	}
	
	/**
	 * 添加系统配置分类
	 * @param 配置分类信息
	 * @return ID
	 * @author subin
	 * */
	public long insertSystemConfigClassify(DBRow param) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("system_config_tab", param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.insertSystemConfigClassify(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 在此分类下,是否存在系统配置项
	 * @param 分类ID
	 * @return 存在与否
	 * @author subin
	 * */
	public boolean existSystemConfigInClassify(long id) throws Exception{
		
		try {
			boolean returnValue = false;
			
			String sql = "select * from config where belong_tab_id = "+id;
			
			DBRow[] result = dbUtilAutoTran.selectMutliple(sql);
			
			if(result.length > 0){
				returnValue = true;
			}
			
			return returnValue;
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.existSystemConfigInClassify(long id) error:" + e);
		}
	}
	
	/**
	 * 更新配置项的所属分类ID
	 * @param 旧所属分类ID,新所属分类ID
	 * @return null
	 * @author subin
	 * */
	public void updateClassifyForSystemConfig(long parentId,long id) throws Exception{
		
		try {
			String sql = "where belong_tab_id = "+parentId;
			
			String tableName = "config";
			
			DBRow param = new DBRow();
			param.add("belong_tab_id", id);
			
			dbUtilAutoTran.update(sql, tableName, param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.updateClassifyForSystemConfig(long parentId,long id) error:" + e);
		}
	}
	
	/**
	 * 是否存在此分类
	 * @param 分类ID
	 * @return 存在与否
	 * @author subin
	 * */
	public boolean existClassify(long id) throws Exception{
		
		try {
			boolean returnValue = false;
			
			if(id==0){
				
				returnValue = true;
			}else{
				
				String sql = "select * from system_config_tab where tab_id = "+id;
				
				if(dbUtilAutoTran.selectSingle(sql) != null){
					returnValue = true;
				}
			}
			
			return returnValue;
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.existClassify(long id) error:" + e);
		}
	}
	
	/**
	 * 根据[字符串 用逗号分隔的ID]查询
	 * @param 多个ID
	 * @return 系统配置项信息
	 * @author subin
	 * */
	public DBRow[] getSystemConfigItemByManyItemId(String id) throws Exception{
		
		try {
			
			String sql = "select * from config where id in ("+id+")";
			
			return dbUtilAutoTran.selectMutliple(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigItemByManyItemId(String id) error:" + e);
		}
	}
	
	/**
	 * 根据所属分类ID查询分类下的下一个顺序号
	 * @param 分类ID
	 * @return 顺序号
	 * @author subin
	 * */
	public long getSystemConfigItemNextOrder(long id) throws Exception{
		
		try {
			
			String sql = "select max(order_config)+1 as order_config from config where belong_tab_id = "+id;
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			return result.get("order_config",1);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigItemNextOrder(long id) error:" + e);
		}
	}
	
	/**
	 * 更新系统配置项信息
	 * @param 配置项ID,修改项
	 * @return null
	 * @author subin
	 * */
	public void updateSystemConfigItem(long id,DBRow param) throws Exception{
		
		try {
			
			String sql = "where id = "+id;
			
			dbUtilAutoTran.update(sql, "config", param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.updateSystemConfigItem(long id,DBRow param) error:" + e);
		}
	}
	
	/**
	 * 更新系统配置项信息[name]
	 * @param 配置项name,修改项
	 * @return null
	 * @author subin
	 * */
	public void updateSystemConfigItemByName(String name,DBRow param) throws Exception{
		
		try {
			
			String sql = "where confname = '"+name+"'";
			
			dbUtilAutoTran.update(sql, "config", param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.updateSystemConfigItemByName(String name,DBRow param) error:" + e);
		}
	}
	
	/**
	 * 查询系统配置项
	 * @param 配置项ID
	 * @return 配置信息
	 * @author subin
	 * */
	public DBRow getSystemConfigItemById(long id) throws Exception{
		
		try {
			
			String sql = " select * from config where id = "+id;
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigItemById(long id) error:" + e);
		}
	}
	
	/**
	 * 查询配置项信息[根据排序 查询上一个或者下一个]
	 * @param 配置项ID,上下区分
	 * @return 配置项信息
	 * @author subin
	 * */
	public DBRow getSystemConfigItemByOrder(long id,String flag) throws Exception{
		
		try {
			
			String sql = "";
			
			if(flag.equals("up")){
				
				sql = "select * from config where belong_tab_id = (select belong_tab_id from config where id="+id+") and order_config = (select max(order_config) from config where belong_tab_id = (select belong_tab_id from config where id="+id+") and order_config < (select order_config from config where id="+id+") order by order_config )";
			}else{
				
				sql = "select * from config where belong_tab_id = (select belong_tab_id from config where id="+id+") and order_config > (select order_config from config where id="+id+") order by order_config LIMIT 0,1";
			}
			
			return dbUtilAutoTran.selectSingle(sql);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.getSystemConfigItemByOrder(long id,String flag) error:" + e);
		}
	}
	
	/**
	 * insert system config item 
	 * @param config info
	 * @return ID
	 * @author subin
	 * */
	public long insertSystemConfigClassifyItem(DBRow param) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("config", param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.insertSystemConfigClassifyItem(DBRow param) error:" + e);
		}
	}
	
	/**
	 * insert system config item checkbox option
	 * @param option info
	 * @return ID
	 * @author subin
	 * */
	public long insertClassifyItemOption(DBRow param) throws Exception{
		
		try {
			
			return dbUtilAutoTran.insertReturnId("system_config_option", param);
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.insertClassifyItemOption(DBRow param) error:" + e);
		}
	}
	
	/**
	 * 是否存在此配置项ID
	 * @param 配置项ID
	 * @return 存在与否
	 * @author subin
	 * */
	public boolean existSystemConfigItemId(String id) throws Exception{
		
		try {
			boolean returnValue = false;
			
			String sql = "select * from config where confname = '"+id+"'";
			
			DBRow result = dbUtilAutoTran.selectSingle(sql);
			
			if(result==null){
				returnValue = true;
			}
			
			return returnValue;
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.existSystemConfigInClassify(String id) error:" + e);
		}
	}
	
	/**
	 * 删除配置项的选项
	 * @param 配置项ID
	 * @return null
	 * @author subin
	 * */
	public void deleteSystemConfigItemOption(long id) throws Exception{
		
		try {
			
			String sql = "where belong_system_config_id = "+id;
			
			dbUtilAutoTran.delete(sql, "system_config_option");
			
		}catch (Exception e){
			throw new Exception("FloorSystemConfigMgrSbb.deleteSystemConfigItemOption(long id) error:" + e);
		}
	}
}