package com.micro.system.manage.config;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.sbb.FloorSystemConfigMgrSbb;
import com.cwc.db.DBRow;

@RestController
public class SystemConfigItemMoveController{
	
	@Autowired
	private FloorSystemConfigMgrSbb floorSystemConfigMgr;
	
	/**
	 * 获取需要移动的配置项信息
	 * @param 配置项ID
	 * @return success
	 * @author subin
	 * */
	@RequestMapping(value="/systemConfigItemMove",produces="application/json;charset=UTF-8",method=RequestMethod.GET)
	public Map<String,Object> systemConfigItemMoveInfo(
			
			@RequestParam(value="moveItemId",required=true) String moveItemId
		) throws Exception {
		
		String itemId[] = moveItemId.split(",");
		
		DBRow[] item = new DBRow[itemId.length];
		
		for(int i=0;i<itemId.length;i++){
			
			item[i] = floorSystemConfigMgr.getSystemConfigItemById(Long.parseLong(itemId[i]));
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configInfo", item);
		
		return result;
	}
	
	/**
	 * 改变配置项所属的分类
	 * @param 多个配置项ID[逗号分隔ID],分类ID
	 * @return success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/systemConfigItemMove",produces="application/json;charset=UTF-8",method=RequestMethod.POST)
	public Map<String,Object> modifyClassifyForBelong(
			//配置项ID
			@RequestParam(value="configItemId",required=true) String configItemId,
			//分类ID
			@RequestParam(value="classifyId",required=true) long classifyId
		) throws Exception {
		
		//查询配置项ID,按原方式排序
		DBRow[] configItem = floorSystemConfigMgr.getSystemConfigItemByManyItemId(configItemId);
		
		//改变所属分类ID,跟据原ID顺序重新排序
		for(DBRow oneResutl:configItem){
			
			DBRow param = new DBRow();
			param.add("belong_tab_id", classifyId);
			param.add("order_config", floorSystemConfigMgr.getSystemConfigItemNextOrder(classifyId));
			
			floorSystemConfigMgr.updateSystemConfigItem(oneResutl.get("id",-1),param);
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", true);
		
		return result;
	}
	
	/**
	 * 变换系统配置信息顺序
	 * @param 配置项ID,交换排序区分[up/down]
	 * @return success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/exchangeConfigItemOrder",produces="application/json;charset=UTF-8",method=RequestMethod.POST)
	public Map<String,Object> exchangeConfigItemOrder(
			//配置项ID
			@RequestParam(value="configItemId",required=true) long configItemId,
			//交换排序区分
			@RequestParam(value="exchangeFlag",required=true) String exchangeFlag
		) throws Exception {
		
		//查询需要交换的配置项
		DBRow first = floorSystemConfigMgr.getSystemConfigItemById(configItemId);
		DBRow second = floorSystemConfigMgr.getSystemConfigItemByOrder(configItemId,exchangeFlag);
		
		if(second!=null){
			
			//更新配置项顺序
			DBRow param = new DBRow();
			param.add("order_config", second.get("order_config",1));
			floorSystemConfigMgr.updateSystemConfigItem(first.get("id",-1), param);
			
			param.add("order_config", first.get("order_config",1));
			floorSystemConfigMgr.updateSystemConfigItem(second.get("id",-1), param);
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", true);
		
		return result;
	}
}