package com.micro.system.manage.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.sbb.FloorSystemConfigMgrSbb;
import com.cwc.db.DBRow;

@RestController
public class SystemConfigItemController{
	
	@Autowired
	private FloorSystemConfigMgrSbb floorSystemConfigMgr;
	
	/**
	 * 验证系统配置项ID是否重复
	 * @param 配置项ID
	 * @return success
	 * @author subin
	 * */
	@RequestMapping(value="/existSystemConfigItemIdValidator",produces="application/json;charset=UTF-8",method=RequestMethod.GET)
	public Map<String,Object> existSystemConfigItemIdValidator(
			
			@RequestParam(value="classifyId",required=true) String classifyId
		) throws Exception {
		
		boolean flag = floorSystemConfigMgr.existSystemConfigItemId(classifyId);

		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", flag);
		
		return result;
	}
	
	/**
	 * 查询配置项
	 * @param 配置项信息
	 * @return success
	 * @author subin
	 * */
	@RequestMapping(value="/systemConfigItem",produces="application/json;charset=UTF-8",method=RequestMethod.GET)
	public Map<String,Object> getSystemConfigItem(
			
			@RequestParam(value="id",required=true) long id
		) throws Exception {
		
		DBRow item = floorSystemConfigMgr.getSystemConfigItemById(id);
			
		String type = item.get("config_type","-1");
			
		//组装选项数据
		if(type.equals("radio") || type.equals("checkbox") || type.equals("select")){
			
			DBRow[] option = floorSystemConfigMgr.getSystemConfigOption(id);
			
			//组装复选框已选择项
			if(type.equals("checkbox")){
				
				String[] checkOption = item.get("confvalue", "").split(",");
				
				for(DBRow checkResult:option){
					for(String optionResult:checkOption){
						
						if((checkResult.get("option_key","-1")).equals(optionResult)){
							
							checkResult.add("checked", "checked");
						}
					}
				}
			}else{
				
				for(DBRow checkResult:option){
						
					if((checkResult.get("option_key","-1")).equals(item.get("confvalue", ""))){
						
						checkResult.add("checked", "checked");
					}
				}
			}
			
			if(option.length>0){
				
				item.add("option", option);
			}
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configItem", item);
		
		return result;
	}
	
	/**
	 * 查询系统配置所有的配置信息
	 * @param null
	 * @return 所有的配置信息
	 * @author subin
	 * */
	@RequestMapping(value = "/systemConfigInfo", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> getSystemConfigContent() throws Exception {
		
		//组装有配置的tabId
		DBRow[] sysConfig = floorSystemConfigMgr.getSystemConfigTabId();
		
		for(DBRow oneResult:sysConfig){
			
			DBRow[] sysContent = floorSystemConfigMgr.getSystemConfigContent(oneResult.get("tab_id", -1));
			
			//组装配置数据
			if(sysContent.length>0){
				
				for(DBRow result:sysContent){
					
					String type = result.get("config_type","-1");
					
					//组装选项数据
					if(type.equals("radio") || type.equals("checkbox") || type.equals("select")){
						
						DBRow[] option = floorSystemConfigMgr.getSystemConfigOption(result.get("id",-1));
						
						//组装复选框已选择项
						if(type.equals("checkbox")){
							
							String[] checkOption = result.get("config_value", "").split(",");
							
							for(DBRow checkResult:option){
								for(String optionResult:checkOption){
									
									if((checkResult.get("option_key","-1")).equals(optionResult)){
										
										checkResult.add("checked", "checked");
									}
								}
							}
						}
						if(option.length>0){
							
							result.add("option", option);
						}
					}
				}
				
				oneResult.add("content", sysContent);
			}
		}
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configInfo", sysConfig);
		
		return result;
	}
	
	/**
	 * 更新系统配置项的值
	 * @param 配置项的ID,value
	 * @return success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/updateSystemConfigValue",produces="application/json;charset=UTF-8",method=RequestMethod.POST)
	public Map<String,Object> updateSystemConfigValue(
			
			@RequestBody Map<String, Object> parameter
		) throws Exception {
		
		//更新配置项
		for (String oneResutl:parameter.keySet()){
			
			DBRow param = new DBRow();
			param.add("confvalue", ((String) parameter.get(oneResutl)).replaceAll("(^\\s{1,})|(\\s{1,}$)",""));
			
			floorSystemConfigMgr.updateSystemConfigItemByName(oneResutl,param);
		}
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", true);
		
		return result;
	}
	
	/**
	 * 添加配置项
	 * @param 配置项信息
	 * @return success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/systemConfigItem",produces="application/json;charset=UTF-8",method=RequestMethod.POST)
	public Map<String,Object> addSystemConfigItem(
			
			@RequestBody Map<String, Object> parameter
		) throws Exception {
		
		String classifyType = parameter.get("classifyType").toString();
		String belongClassifyId = parameter.get("belongClassifyId").toString();
		
		DBRow param = new DBRow();
		param.put("confname",parameter.get("classifyId"));
		//maybe null
		param.put("confvalue",parameter.get("classifyValue"));
		param.put("description",parameter.get("classifyName"));
		param.put("config_type",parameter.get("classifyType"));
		param.put("belong_tab_id",parameter.get("belongClassifyId"));
		param.put("order_config", floorSystemConfigMgr.getSystemConfigItemNextOrder(Long.parseLong(belongClassifyId)));
		param.put("pre_description",parameter.get("preDescription"));
		param.put("post_description",parameter.get("postDescription"));
		
		//插入config返回id
		long id = floorSystemConfigMgr.insertSystemConfigClassifyItem(param);
		
		if(classifyType.equals("radio") || classifyType.equals("select") || classifyType.equals("checkbox")){
			
			@SuppressWarnings("unchecked")
			List<Map<String,String>> optionList = (ArrayList<Map<String,String>>) parameter.get("classifyTypeContent");

			for (Map<String,String> oneResult:optionList){
				
				DBRow option = new DBRow();
				option.put("belong_system_config_id", id);
				option.put("option_key", oneResult.get("choiseKey"));
				option.put("option_value", oneResult.get("choiseValue"));
				
				//插入config_option
				floorSystemConfigMgr.insertClassifyItemOption(option);
			}
		}

		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", true);
		
		return result;
	}
	
	/**
	 * 修改配置项
	 * @param 配置项信息
	 * @return success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/systemConfigItem",produces="application/json;charset=UTF-8",method=RequestMethod.PUT)
	public Map<String,Object> modifySystemConfigItem(
			
			@RequestBody Map<String, Object> parameter
		) throws Exception {
		
		long id = Long.parseLong(parameter.get("id").toString());
		String classifyType = parameter.get("classifyType").toString();
		
		DBRow param = new DBRow();
		//maybe null
		param.put("confvalue",parameter.get("classifyValue"));
		param.put("description",parameter.get("classifyName"));
		param.put("config_type",classifyType);
		param.put("pre_description",parameter.get("preDescription"));
		param.put("post_description",parameter.get("postDescription"));
		
		//update config
		floorSystemConfigMgr.updateSystemConfigItem(id,param);
		
		if(classifyType.equals("radio") || classifyType.equals("select") || classifyType.equals("checkbox")){
			
			//删除原有的选项
			floorSystemConfigMgr.deleteSystemConfigItemOption(id);
			
			@SuppressWarnings("unchecked")
			List<Map<String,String>> optionList = (ArrayList<Map<String,String>>) parameter.get("classifyTypeContent");

			for (Map<String,String> oneResult:optionList){
				
				DBRow option = new DBRow();
				option.put("belong_system_config_id", id);
				option.put("option_key", oneResult.get("choiseKey"));
				option.put("option_value", oneResult.get("choiseValue"));
				
				//插入config_option
				floorSystemConfigMgr.insertClassifyItemOption(option);
			}
		}

		Map<String,Object> result = new HashMap<String,Object>();
		result.put("success", true);
		
		return result;
	}
}