package com.micro.system.manage.config;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cwc.app.floor.api.sbb.FloorSystemConfigMgrSbb;
import com.cwc.app.util.DBRowUtils;
import com.cwc.db.DBRow;

@RestController
public class SystemConfigClassifyController {
	
	@Autowired
	private FloorSystemConfigMgrSbb floorSystemConfigMgr;
	
	/**
	 * 查询系统配置所有分类信息
	 * @param null
	 * @return 所有分类信息
	 * @author subin
	 * */
	@RequestMapping(value = "/systemConfigClassify", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> getSystemConfigClassify() throws Exception {
		
		//递归查询系统配置所有分类 最高父ID为[0]
		DBRow[] configClassify = floorSystemConfigMgr.getSystemConfigClassify(0);
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configClassify", configClassify);
		
		return result;
	}
	
	/**
	 * 查询系统配置所有分类信息[树形结构]
	 * @param null
	 * @return 所有分类信息[树形结构]
	 * @author subin
	 * */
	@RequestMapping(value = "/systemConfigClassifyTree", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> systemConfigClassifyTree() throws Exception {
		
		DBRow[] configClassify = floorSystemConfigMgr.getSystemConfigClassifyTree();
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configClassify", DBRowUtils.dbrowToLowercase(configClassify));
		
		return result;
	}
	
	/**
	 * 查询系统配置所有分类信息[all][树形结构]
	 * @param null
	 * @return 所有分类信息[树形结构][all]
	 * @author subin
	 * */
	@RequestMapping(value = "/systemConfigClassifyAllTree", produces = "application/json; charset=UTF-8", method = RequestMethod.GET)
	public Map<String,Object> systemConfigClassifyAllTree() throws Exception {
		
		DBRow[] configClassify = floorSystemConfigMgr.getSystemConfigClassifyAllTree();
		
		Map<String, Object> oneMap = new LinkedHashMap<String, Object>();
		oneMap.put("id", 0);
		oneMap.put("text", "ALL");
		oneMap.put("parent", "#");
		
		List<Object> params = DBRowUtils.dbrowToLowercase(configClassify);
		params.add(oneMap);
		
		Map<String,Object> result = new HashMap<String,Object>();
		result.put("configClassify", params);
		
		return result;
	}
	
	/**
	 * 添加系统配置分类信息
	 * @param 分类名,分类所属ID (默认最高级)
	 * @return ID,success
	 * @author subin
	 * */
	@Transactional
	@RequestMapping(value="/systemConfigClassify",produces="application/json;charset=UTF-8",method=RequestMethod.POST)
	public Map<String,Object> addSystemConfigClassify(
			
			@RequestBody Map<String, Object> parameter
		) throws Exception {
		
		String classifyName = parameter.get("classifyName").toString();
		long parentClassifyId = Long.parseLong(parameter.get("parentClassifyId").toString());
		
		Map<String,Object> result = new HashMap<String,Object>();
		
		//验证是否存在此分类ID
		if(!floorSystemConfigMgr.existClassify(parentClassifyId)){
			
			result.put("success", false);
		}else{
			
			//查询分类顺序
			long classifyOrder = floorSystemConfigMgr.getSystemConfigClassifyOrder(parentClassifyId);
			
			//添加分类
			DBRow param = new DBRow();
			param.put("tab_name", classifyName);
			param.put("tab_parent_id", parentClassifyId);
			param.put("order_tab", classifyOrder);
			
			long id = floorSystemConfigMgr.insertSystemConfigClassify(param);
			
			//查询分类父级下是否有配置项,如果有,则转移到子级分类
			if(floorSystemConfigMgr.existSystemConfigInClassify(parentClassifyId)){
				
				floorSystemConfigMgr.updateClassifyForSystemConfig(parentClassifyId,id);
			}
			
			result.put("id", id);
			result.put("success", true);
		}
		
		return result;
	}
}